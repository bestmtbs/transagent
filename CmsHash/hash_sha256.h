/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/
#ifndef _HASH_SHA256_H_
#define _HASH_SHA256_H_

#include "hash_global.h"
#include "hash.h"


#define	SHA256_K1		0x428a2f98;
#define	SHA256_K2		0x71374491;
#define	SHA256_K3		0xb5c0fbcf;
#define	SHA256_K4		0xe9b5dba5;
#define	SHA256_K5		0x3956c25b;
#define	SHA256_K6		0x59f111f1;
#define	SHA256_K7		0x923f82a4;
#define	SHA256_K8		0xab1c5ed5;
#define	SHA256_K9		0xd807aa98;
#define	SHA256_K10		0x12835b01;
#define	SHA256_K11		0x243185be;
#define	SHA256_K12		0x550c7dc3;
#define	SHA256_K13		0x72be5d74;
#define	SHA256_K14		0x80deb1fe;
#define	SHA256_K15		0x9bdc06a7;
#define	SHA256_K16		0xc19bf174;
#define	SHA256_K17		0xe49b69c1;
#define	SHA256_K18		0xefbe4786;
#define	SHA256_K19		0x0fc19dc6;
#define	SHA256_K20		0x240ca1cc;
#define	SHA256_K21		0x2de92c6f;
#define	SHA256_K22		0x4a7484aa;
#define	SHA256_K23		0x5cb0a9dc;
#define	SHA256_K24		0x76f988da;
#define	SHA256_K25		0x983e5152;
#define	SHA256_K26		0xa831c66d;
#define	SHA256_K27		0xb00327c8;
#define	SHA256_K28		0xbf597fc7;
#define	SHA256_K29		0xc6e00bf3;
#define	SHA256_K30		0xd5a79147;
#define	SHA256_K31		0x06ca6351;
#define	SHA256_K32		0x14292967;
#define	SHA256_K33		0x27b70a85;
#define	SHA256_K34		0x2e1b2138;
#define	SHA256_K35		0x4d2c6dfc;
#define	SHA256_K36		0x53380d13;
#define	SHA256_K37		0x650a7354;
#define	SHA256_K38		0x766a0abb;
#define	SHA256_K39		0x81c2c92e;
#define	SHA256_K40		0x92722c85;
#define	SHA256_K41		0xa2bfe8a1;
#define	SHA256_K42		0xa81a664b;
#define	SHA256_K43		0xc24b8b70;
#define	SHA256_K44		0xc76c51a3;
#define	SHA256_K45		0xd192e819;
#define	SHA256_K46		0xd6990624;
#define	SHA256_K47		0xf40e3585;
#define	SHA256_K48		0x106aa070;
#define	SHA256_K49		0x19a4c116;
#define	SHA256_K50		0x1e376c08;
#define	SHA256_K51		0x2748774c;
#define	SHA256_K52		0x34b0bcb5;
#define	SHA256_K53		0x391c0cb3;
#define	SHA256_K54		0x4ed8aa4a;
#define	SHA256_K55		0x5b9cca4f;
#define	SHA256_K56		0x682e6ff3;
#define	SHA256_K57		0x748f82ee;
#define	SHA256_K58		0x78a5636f;
#define	SHA256_K59		0x84c87814;
#define	SHA256_K60		0x8cc70208;
#define	SHA256_K61		0x90befffa;
#define	SHA256_K62		0xa4506ceb;
#define	SHA256_K63		0xbef9a3f7;
#define	SHA256_K64		0xc67178f2;

// 아래 두개는 Sha1 에서도 쓰인다.
#define blk0(i)				(W[i] = data[i])
#define blk1(i)				(W[i&15] = ROTL32(W[(i+13)&15]^W[(i+8)&15]^W[(i+2)&15]^W[i&15],1))


#define blk2(i)				(W[i&15]+=SHA256s1(W[(i-2)&15])+W[(i-7)&15]+SHA256s0(W[(i-15)&15]))
#define Ch(x,y,z)			(z^(x&(y^z)))
#define Maj(x,y,z)			((x&y)|(z&(x|y)))

#define a(i)				T[(0-i)&7]
#define b(i)				T[(1-i)&7]
#define c(i)				T[(2-i)&7]
#define d(i)				T[(3-i)&7]
#define e(i)				T[(4-i)&7]
#define f(i)				T[(5-i)&7]
#define g(i)				T[(6-i)&7]
#define h(i)				T[(7-i)&7]

#define R(i,j,l)			h(i)+=SHA256S1(e(i))+Ch(e(i),f(i),g(i))+SHA256_K##l; h(i)+=(j?blk2(i):blk0(i));\
							d(i)+=h(i);\
							h(i)+=SHA256S0(a(i))+Maj(a(i),b(i),c(i))
// 앞의 h(i)와 뒤의 h(i)를 묶으면 에러 발생. --> i, j의 위치와 관계된 것으로 보임.


#define SHA256S0(x)			(ROTR32(x,2)^ROTR32(x,13)^ROTR32(x,22))
#define SHA256S1(x)			(ROTR32(x,6)^ROTR32(x,11)^ROTR32(x,25))
#define SHA256s0(x)			(ROTR32(x,7)^ROTR32(x,18)^(x>>3))
#define SHA256s1(x)			(ROTR32(x,17)^ROTR32(x,19)^(x>>10))




#ifdef __cplusplus
extern "C" {
#endif

void CmsSha256Init(	HASH_CTX * psCtx);
void CmsSha256Update( HASH_CTX *	psCtx, unsigned char * pInput, unsigned int nInputByteLength);
void CmsSha256Final( HASH_CTX * psCtx, unsigned char * pHashValue);
void CmsSha256(	unsigned char	* pHashValue, unsigned char * pInput,	unsigned int	nInputByteLength);

#ifdef __cplusplus
} 
#endif

#endif