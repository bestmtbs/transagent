
/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/

/**
@file       hash.c 
@brief     hash
@author   jhlee
@date      create 2011.09.06
*/
#include "hash_global.h"
#include "hash.h"

#include "hash_md5.h"			
#include "hash_sha1.h"			// sha0 & sha1
#include "hash_sha256.h"


void
CmsHashBin (
			unsigned char *		pOutData,		// [O]

			unsigned char *		pInData,		// [I]
			unsigned int		nInLen,			// [I]
			unsigned int			nAlgId			// [I]
)
{
	
	switch( nAlgId) {


		case HASH_ALGID_MD5 :			// MD5

			CmsMd5( pOutData, pInData, nInLen);
			break;

		case HASH_ALGID_SHA1 :			// SHA-1

			CmsSha1( pOutData, pInData, nInLen);
			break;

		case HASH_ALGID_SHA256 :			// SHA-256

			CmsSha256( pOutData, pInData, nInLen);
			break;

// 		case DK_HASH_ALGID_RIPEMD160 :		// RipeMD-160
// 
// 			DK_Ripemd( pOutData, pInData, nInLen);
// 			break;

// 		case DK_HASH_ALGID_HAS160 :			// HAS-160
// 			DK_Has160( pOutData, pInData, nInLen);
// 			break;

		default :							// Default : SHA-1
			CmsSha1( pOutData, pInData, nInLen);

	} // switch
}



/*[!] 아래 함수는 연접 해쉬에 사용한다.  */
 
HASH_CTX *
CmsCreateHashCtx (
				  unsigned int		nHashId		// [I]
)
{


	HASH_CTX *			psCtx = NULL;

	psCtx = /*ExAllocatePool(NonPagedPool ,sizeof( DK_HASH_CTX));*/calloc( 1, sizeof( HASH_CTX));
	memset(psCtx,0x00,sizeof( HASH_CTX));
	psCtx->nAlgId = nHashId;

	CmsGetHashLen( &psCtx->nHashLen, nHashId);

	return psCtx;
}


/*void
DK_Hash_Determine_Length
	)*/

void
CmsGetHashLen (
			   unsigned int *		pnHashLen,		// [O]

			   unsigned int			nHashId
)
{

	switch( nHashId) {

		case HASH_ALGID_MD2 :
		case HASH_ALGID_MD5 :

			* pnHashLen = 16;
			break;

		case HASH_ALGID_RIPEMD160 :
		case HASH_ALGID_SHA1 :
		case HASH_ALGID_HAS160 :

			* pnHashLen = 20;
			break;

		case HASH_ALGID_SHA256 :

			* pnHashLen = 32;
	} // switch
}

void
CmsInitHash (
			 HASH_CTX *		psCtx
)
{

	switch( psCtx->nAlgId) {

// 		case DK_HASH_ALGID_MD2 :		// MD2
// 
// 			DK_Md2Init( psCtx);
// 			break;

		case HASH_ALGID_MD5 :		// MD5

			CmsMd5Init( psCtx);
			break;

		case HASH_ALGID_SHA1 :		// SHA-1

			CmsSha1Init( psCtx);
			break;

		case HASH_ALGID_SHA256 :		// SHA-256

			CmsSha256Init( psCtx);
			break;

// 		case DK_HASH_ALGID_RIPEMD160 :	// RipeMD-160
// 
// 			DK_RipemdInit( psCtx);
// 			break;
// 
// 		case DK_HASH_ALGID_HAS160 :		// HAS-160
// 
// 			DK_Has160Init( psCtx);
// 			break;

		default :						// Default : SHA-1

			psCtx->nAlgId = HASH_ALGID_SHA1;

			CmsSha1Init( psCtx);
				
	} // switch

}


void
CmsUpdateHash (
			   HASH_CTX *		psCtx,		// [O]

			   unsigned char *			pcInData,	// [I]
			   unsigned int				nInLen		// [I]
)
{

	switch( psCtx->nAlgId) {

// 		case DK_HASH_ALGID_MD2 :		// MD2
// 
// 			DK_Md2Update( psCtx, pcInData, nInLen);
// 			break;

		case HASH_ALGID_MD5 :		// MD5

			CmsMd5Update( psCtx, pcInData, nInLen);
			break;

		case HASH_ALGID_SHA1 :		// SHA-1

			CmsSha1Update( psCtx, pcInData, nInLen);
			break;

		case HASH_ALGID_SHA256 :		// SHA-256

			CmsSha256Update( psCtx, pcInData, nInLen);
			break;

// 		case DK_HASH_ALGID_RIPEMD160 :	// RipeMD-160
// 
// 			DK_RipemdUpdate( psCtx, pcInData, nInLen);
// 			break;
// 
// 		case DK_HASH_ALGID_HAS160 :		// HAS-160
// 
// 			DK_Has160Update( psCtx, pcInData, nInLen);
// 			break;

		default :						// Default hash is SHA-1

			CmsSha1Update( psCtx, pcInData, nInLen);

	} // switch

}

void
CmsFinalHash (
			 unsigned char *			pOutData,	// [O]

			  HASH_CTX *		psCtx		// [I/O]
)
{

	switch( psCtx->nAlgId) {

// 		case DK_HASH_ALGID_MD2 :		// MD2
// 
// 			DK_Md2Final( psCtx, pOutData);
// 			break;

		case HASH_ALGID_MD5 :		// MD5

			CmsMd5Final( psCtx, pOutData);
			break;

		case HASH_ALGID_SHA1 :		// SHA-1

			CmsSha1Final( psCtx, pOutData);
			break;

		case HASH_ALGID_SHA256 :		// SHA-256

			CmsSha256Final( psCtx, pOutData);
			break;

// 		case DK_HASH_ALGID_RIPEMD160 :	// RipeMD-160
// 
// 			DK_RipemdFinal( psCtx, pOutData);
// 			break;
// 
// 		case DK_HASH_ALGID_HAS160 :		// HAS-160
// 
// 			DK_Has160Final( psCtx, pOutData);
// 			break;

		default :						// Default hash is SHA-1

			CmsSha1Final( psCtx, pOutData);

	} // switch
}

void
CmsFreeHashCtx (
				HASH_CTX *		psCtx
)
{
	if( psCtx == NULL)
		return;

	memset( psCtx->caBuf, 0x00, 64 );
	memset( psCtx->caStat, 0x00, 8 * DK_WORD_BYTELENGTH );

	/*ExFreePool(psCtx);*/
	free( psCtx);
}
