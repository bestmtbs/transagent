/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/


#ifndef _HASH_SHA1_H_
#define _HASH_SHA1_H_

#include "hash_global.h"
#include "hash.h"

// start of Steve Reid's code
#define blk0(i)				(W[i] = data[i])
#define blk1(i)				(W[i&15] = ROTL32(W[(i+13)&15]^W[(i+8)&15]^W[(i+2)&15]^W[i&15],1))

#define f1(x,y,z)			(z^(x&(y^z)))
#define f2(x,y,z)			(x^y^z)
#define f3(x,y,z)			((x&y)|(z&(x|y)))
#define f4(x,y,z)			(x^y^z)

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i)		z+=f1(w,x,y)+blk0(i)+0x5A827999+ROTL32(v,5);w=ROTL32(w,30);
#define R1(v,w,x,y,z,i)		z+=f1(w,x,y)+blk1(i)+0x5A827999+ROTL32(v,5);w=ROTL32(w,30);
#define R2(v,w,x,y,z,i)		z+=f2(w,x,y)+blk1(i)+0x6ED9EBA1+ROTL32(v,5);w=ROTL32(w,30);
#define R3(v,w,x,y,z,i)		z+=f3(w,x,y)+blk1(i)+0x8F1BBCDC+ROTL32(v,5);w=ROTL32(w,30);
#define R4(v,w,x,y,z,i)		z+=f4(w,x,y)+blk1(i)+0xCA62C1D6+ROTL32(v,5);w=ROTL32(w,30);

#ifdef __cplusplus
extern "C" {
#endif

void CmsSha1Init( HASH_CTX *	psCtx);
void CmsSha1Update( HASH_CTX * psCtx, unsigned char * pInput, unsigned int	 nInputByteLength);
void CmsSha1Final( HASH_CTX * psCtx, unsigned char	* pHashValue);
void CmsSha1( unsigned char * pHashValue,	unsigned char	* pInput, unsigned int	 nInputByteLength);

#ifdef __cplusplus
} 
#endif

#endif