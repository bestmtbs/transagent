/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/

#include "hash_global.h"
#include "hash.h"
#include "hash_sha256.h"


void
CmsSha256Transform (
					unsigned int *		pState,		// [O]
					unsigned char *		pInput		// [I]
)
{
	unsigned int W[16]={0,};
	unsigned int T[8]={0,};
	unsigned int	data[16]={0,};
	unsigned int nIdx=0, j=0;

	for (nIdx = 0; nIdx < 16; nIdx++ )
	data[nIdx] = (((unsigned int )pInput[4 * nIdx]  << 24 )
		  | (((unsigned int )pInput[4 * nIdx + 1] ) << 16 )
		  |	(((unsigned int )pInput[4 * nIdx + 2] ) << 8 )
		  | ((unsigned int )pInput[4 * nIdx + 3] ) );

    /* Copy context->state[] to working vars */
	memcpy(T, pState, sizeof(T));
    /* 64 operations, partially loop unrolled */

	R( 0,0,1); R( 1,0,2); R( 2,0,3); R( 3,0,4); R( 4,0,5); R( 5,0,6); R( 6,0,7); R( 7,0,8); 
	R( 8,0,9); R( 9,0,10); R(10,0,11); R(11,0,12); R(12,0,13); R(13,0,14); R(14,0,15); R(15,0,16);

	R( 0,16,17); R( 1,16,18); R( 2,16,19); R( 3,16,20);	R( 4,16,21); R( 5,16,22); R( 6,16,23); R( 7,16,24);
	R( 8,16,25); R( 9,16,26); R(10,16,27); R(11,16,28); R(12,16,29); R(13,16,30); R(14,16,31); R(15,16,32);

	R( 0,32,33); R( 1,32,34); R( 2,32,35); R( 3,32,36);	R( 4,32,37); R( 5,32,38); R( 6,32,39); R( 7,32,40);
	R( 8,32,41); R( 9,32,42); R(10,32,43); R(11,32,44);	R(12,32,45); R(13,32,46); R(14,32,47); R(15,32,48);

	R( 0,48,49); R( 1,48,50); R( 2,48,51); R( 3,48,52);	R( 4,48,53); R( 5,48,54); R( 6,48,55); R( 7,48,56);
	R( 8,48,57); R( 9,48,58); R(10,48,59); R(11,48,60);	R(12,48,61); R(13,48,62); R(14,48,63); R(15,48,64);

    /* Add the working vars back into context.state[] */
    pState[0] += a(0);
    pState[1] += b(0);
    pState[2] += c(0);
    pState[3] += d(0);
    pState[4] += e(0);
    pState[5] += f(0);
    pState[6] += g(0);
    pState[7] += h(0);
    /* Wipe variables */
	memset(W, 0, sizeof(W));
	memset(T, 0, sizeof(T));
}

void
CmsSha256Init (
			   HASH_CTX *		psCtx	// [O]
)
{
	psCtx->caStat[0] = 0x6a09e667;
	psCtx->caStat[1] = 0xbb67ae85;
	psCtx->caStat[2] = 0x3c6ef372;
	psCtx->caStat[3] = 0xa54ff53a;
	psCtx->caStat[4] = 0x510e527f;
	psCtx->caStat[5] = 0x9b05688c;
	psCtx->caStat[6] = 0x1f83d9ab;
	psCtx->caStat[7] = 0x5be0cd19;

	psCtx->nDataLen[0] = psCtx->nDataLen[1] = 0L;
}


void
CmsSha256Update (
			  	 HASH_CTX *		psCtx,				// [O]

				 unsigned char *			pInput,				// [I]
				 unsigned int			nInputByteLength	// [I]
)
{
	unsigned int			nIdx = 0;
	unsigned int			nIndex = 0;
	unsigned int			nPartLength = 0;

	
	/* Compute number of bytes mod 64 */
	nIndex = (unsigned int)((psCtx->nDataLen[0] >> 3 ) & 0x3f);

	/* update number of sBits */
	if ((psCtx->nDataLen[0] += nInputByteLength << 3 ) < (nInputByteLength << 3 ) )
		psCtx->nDataLen[1]++;
	
	psCtx->nDataLen[1] += (nInputByteLength >> 29 );
	nPartLength = 64 - nIndex;
	
	/* Transform as many times as possible */
	if(nInputByteLength >= nPartLength ) {
		memcpy(psCtx->caBuf + nIndex, pInput, nPartLength );
		if (psCtx->nAlgId == HASH_ALGID_SHA256 )

			CmsSha256Transform( psCtx->caStat, psCtx->caBuf);
		else
			//exit(1 );
			return ;
	
		for (nIdx = nPartLength; nIdx + 63 < nInputByteLength; nIdx+= 64) {
			if (psCtx->nAlgId == HASH_ALGID_SHA256 )

				CmsSha256Transform ( psCtx->caStat, pInput + nIdx);
		}
		nIndex = 0;
	}
	else
		nIdx = 0;

	/* buffer remaining pInput */
	memcpy(psCtx->caBuf + nIndex, pInput + nIdx, nInputByteLength - nIdx );
}


void
CmsSha256Final (
				HASH_CTX *		psCtx,			// [I/O]

				unsigned char	*			pHashValue		// [O]
)
{
	unsigned char					sBits[8];
	unsigned char					sPad[64] = {
																0x80, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0
																};
	unsigned int					nIndex, nPadLength;
	int						nIdx;
	
	for (nIdx = 7; nIdx >= 0; nIdx-- )
		sBits[7 - nIdx] = (unsigned char )(psCtx->nDataLen[nIdx >> 2] >> ((nIdx & 3 ) << 3 ) );

	/*Pad out to 56 mod 64 */
	nIndex = (psCtx->nDataLen[0] >> 3 ) & 0x3f;
	nPadLength = (nIndex < 56 ) ? (56 - nIndex ) : (120 - nIndex );
	
	CmsSha256Update( psCtx, sPad, nPadLength);
	
	/* Append length (before Padding) */
	CmsSha256Update( psCtx,	sBits, 8);
	
	/* Store pState in digest */
	for (nIdx = 0; nIdx < 8; nIdx++ ) {
		pHashValue[(nIdx << 2 )] = (unsigned char )(psCtx->caStat[nIdx] >> 24 );
		pHashValue[(nIdx << 2 ) + 1] = (unsigned char )(psCtx->caStat[nIdx] >> 16 );
		pHashValue[(nIdx << 2 ) + 2] = (unsigned char )(psCtx->caStat[nIdx] >> 8 );
		pHashValue[(nIdx << 2 ) + 3] = (unsigned char )psCtx->caStat[nIdx];
	}

	memset(psCtx->caBuf, 0, 64 );
	memset(psCtx->caStat, 0, 32 );
}


void
CmsSha256 (
		   unsigned char *		pHashValue,			// [O]

		   unsigned char *		pInput,				// [I]
		   unsigned int			nInputByteLength	// [I]
)
{
	HASH_CTX *			psCtx = NULL;


 	psCtx =/* ExAllocatePool(NonPagedPool ,sizeof(DK_HASH_CTX ) );*/calloc(1, sizeof(HASH_CTX) );
	memset(psCtx,0x00,sizeof(HASH_CTX ));
	psCtx->nAlgId = HASH_ALGID_SHA256;

	CmsSha256Init(psCtx);

	CmsSha256Update(psCtx,pInput,nInputByteLength);

	CmsSha256Final(	psCtx,	pHashValue	);

	/*ExFreePool(psCtx);*/
	free(psCtx );
}