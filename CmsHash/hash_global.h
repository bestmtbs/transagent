
/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>		// strlwr strupr
#include <memory.h>
#include <malloc.h>
#include <time.h>
#include <stdarg.h>		// 가변인자
#include <ctype.h>		// 대소문자 변화 tolower etc..
//#include <winsock2.h>  //ws2_32.lib도 링크해야 함

#define DK_OK				0	
#define DK_ERR				-1

#define DK_TRUE 			1
#define DK_FALSE			0

#define DK_WORD_BYTELENGTH			4
#define DK_WORD_BITLENGTH			32

#define MASK8           ( 0xffL		 )			//  8 bit mask
#define MASK16          ( 0xffffL	 )			// 16 bit mask
#define MASK32          ( 0xffffffffL )			// 32 bit mask
#define MASK64          ( 0xffffffffffffffffL )	// 64 bit mask
#define MASKL32         ( 0x00000000ffffffffL )	// for cutting lower 32 bit
#define MASKH32         ( 0xffffffff00000000L )	// for cutting lower 32 bit
#define	MASKMSB			( 0x80000000L )			// MSB of DK_WORD
#define	MASK_ALG_TYPE	( 0xff000000L )			// extract algorithm type

/* ================================================================================= macro === */
#define MAX(x,y)		( ( (x) >= (y) ) ?   (x) : (y)         )
#define ROTL16(x, n)	( ( (x) << (n) ) | ( (x) >> (16 - (n))))	//x : 16bit circular rotation
#define ROTR16(x, n)	( ( (x) >> (n) ) | ( (x) << (16 - (n))))	//x : 16bit	circular rotation
#define ROTL32(x, y)	( ( (x) << (y) ) | ( (x) >> (32 - (y))))	//x : 32bit	circular rotation
#define ROTR32(x, y)	( ( (x) >> (y) ) | ( (x) << (32 - (y))))	//x : 32bit	circular rotation

#define	GETBYTE(x, n)	(DK_BYTE )((x ) >> (n << 3 ) )		//n<<3 : 1byte (8bit)
															//get n-th byte from x
															//n*8bit만큼 x를 오른족으로 shift and trucate one byte	


#ifdef  __cplusplus
extern "C" {
#endif

#ifdef  __cplusplus
}
#endif

#endif