/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/

#include "hash_global.h"
#include "hash.h"
#include "hash_sha1.h"

void
CmsSha1Transform (
				  unsigned int *		pState,		// [O]

				  unsigned char *		pInput		// [I]
)
{
	unsigned int W[16];
	unsigned int					a, b, c, d, e;
	unsigned int					data[16];
	int						nIdx;

    /* Copy context->state[] to working vars */
	a = pState[0];
	b = pState[1];
	c = pState[2];
	d = pState[3];
	e = pState[4];

	for (nIdx = 0; nIdx < 16; nIdx++ )
		data[nIdx] = (((unsigned int )pInput[4 * nIdx]  << 24 )
			  | (((unsigned int )pInput[4 * nIdx + 1] ) << 16 )
			  |	(((unsigned int )pInput[4 * nIdx + 2] ) << 8 )
			  | ((unsigned int )pInput[4 * nIdx + 3] ) );

    /* 4 rounds of 20 operations each. Loop unrolled. */
    R0(a,b,c,d,e, 0); R0(e,a,b,c,d, 1); R0(d,e,a,b,c, 2); R0(c,d,e,a,b, 3);
    R0(b,c,d,e,a, 4); R0(a,b,c,d,e, 5); R0(e,a,b,c,d, 6); R0(d,e,a,b,c, 7);
    R0(c,d,e,a,b, 8); R0(b,c,d,e,a, 9); R0(a,b,c,d,e,10); R0(e,a,b,c,d,11);
    R0(d,e,a,b,c,12); R0(c,d,e,a,b,13); R0(b,c,d,e,a,14); R0(a,b,c,d,e,15);
    R1(e,a,b,c,d,16); R1(d,e,a,b,c,17); R1(c,d,e,a,b,18); R1(b,c,d,e,a,19);
    R2(a,b,c,d,e,20); R2(e,a,b,c,d,21); R2(d,e,a,b,c,22); R2(c,d,e,a,b,23);
    R2(b,c,d,e,a,24); R2(a,b,c,d,e,25); R2(e,a,b,c,d,26); R2(d,e,a,b,c,27);
    R2(c,d,e,a,b,28); R2(b,c,d,e,a,29); R2(a,b,c,d,e,30); R2(e,a,b,c,d,31);
    R2(d,e,a,b,c,32); R2(c,d,e,a,b,33); R2(b,c,d,e,a,34); R2(a,b,c,d,e,35);
    R2(e,a,b,c,d,36); R2(d,e,a,b,c,37); R2(c,d,e,a,b,38); R2(b,c,d,e,a,39);
    R3(a,b,c,d,e,40); R3(e,a,b,c,d,41); R3(d,e,a,b,c,42); R3(c,d,e,a,b,43);
    R3(b,c,d,e,a,44); R3(a,b,c,d,e,45); R3(e,a,b,c,d,46); R3(d,e,a,b,c,47);
    R3(c,d,e,a,b,48); R3(b,c,d,e,a,49); R3(a,b,c,d,e,50); R3(e,a,b,c,d,51);
    R3(d,e,a,b,c,52); R3(c,d,e,a,b,53); R3(b,c,d,e,a,54); R3(a,b,c,d,e,55);
    R3(e,a,b,c,d,56); R3(d,e,a,b,c,57); R3(c,d,e,a,b,58); R3(b,c,d,e,a,59);
    R4(a,b,c,d,e,60); R4(e,a,b,c,d,61); R4(d,e,a,b,c,62); R4(c,d,e,a,b,63);
    R4(b,c,d,e,a,64); R4(a,b,c,d,e,65); R4(e,a,b,c,d,66); R4(d,e,a,b,c,67);
    R4(c,d,e,a,b,68); R4(b,c,d,e,a,69); R4(a,b,c,d,e,70); R4(e,a,b,c,d,71);
    R4(d,e,a,b,c,72); R4(c,d,e,a,b,73); R4(b,c,d,e,a,74); R4(a,b,c,d,e,75);
    R4(e,a,b,c,d,76); R4(d,e,a,b,c,77); R4(c,d,e,a,b,78); R4(b,c,d,e,a,79);
    /* Add the working vars back into context.state[] */
    pState[0] += a;
    pState[1] += b;
    pState[2] += c;
    pState[3] += d;
    pState[4] += e;
    /* Wipe variables */
    a = b = c = d = e = 0;
	memset(W, 0, sizeof(W));
}


/* ============================================================================ function body === */
void
CmsSha1Init (
			 HASH_CTX *			psCtx		// [I]
)
{
	psCtx->caStat[0] = 0x67452301;
	psCtx->caStat[1] = 0xefcdab89;
	psCtx->caStat[2] = 0x98badcfe;
	psCtx->caStat[3] = 0x10325476;
	psCtx->caStat[4] = 0xc3d2e1f0;

	psCtx->nDataLen[0] = psCtx->nDataLen[1] = 0L;
}


/* ============================================================================ function body === */
void
CmsSha1Update (
			   HASH_CTX *		psCtx,				// [I/O]

			   unsigned char *			pInput,				// [I]
			   unsigned int				nInputByteLength	// [I]
)
{
	unsigned int			nIdx = 0;
	unsigned int			nIndex = 0;
	unsigned int			nPartLength = 0;

	
	/* Compute number of bytes mod 64 */
	nIndex = (unsigned int)((psCtx->nDataLen[0] >> 3 ) & 0x3f);

	/* update number of sBits */
	if ((psCtx->nDataLen[0] += nInputByteLength << 3 ) < (nInputByteLength << 3 ) )
		psCtx->nDataLen[1]++;
	
	psCtx->nDataLen[1] += (nInputByteLength >> 29 );
	nPartLength = 64 - nIndex;
	
	/* Transform as many times as possible */
	if(nInputByteLength >= nPartLength ) {

		memcpy(psCtx->caBuf + nIndex, pInput, nPartLength );

		if (psCtx->nAlgId == HASH_ALGID_SHA1 )
			CmsSha1Transform( psCtx->caStat, psCtx->caBuf);
		else
			//exit(1 ); 
			return ;
	
		for (nIdx = nPartLength; nIdx + 63 < nInputByteLength; nIdx+= 64) {

			if (psCtx->nAlgId == HASH_ALGID_SHA1 )

				CmsSha1Transform( psCtx->caStat, pInput + nIdx);
		}
		nIndex = 0;
	}
	else
		nIdx = 0;

	/* buffer remaining pInput */
	memcpy(psCtx->caBuf + nIndex, pInput + nIdx, nInputByteLength - nIdx );
}


/* ============================================================================ function body === */
void
CmsSha1Final (
			  HASH_CTX *		psCtx,			// [I/O]

			  unsigned char *			pHashValue		// [O]	
)
{
	unsigned char					sBits[8];
	unsigned char					sPad[64] = {
																0x80, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0, 
																   0, 0, 0, 0, 0, 0, 0, 0
																								};
	unsigned int					nIndex, nPadLength;
	int						nIdx;
	
	for (nIdx = 7; nIdx >= 0; nIdx-- )
		sBits[7 - nIdx] = (unsigned char )(psCtx->nDataLen[nIdx >> 2] >> ((nIdx & 3 ) << 3 ) );

	/*Pad out to 56 mod 64 */
	nIndex = (psCtx->nDataLen[0] >> 3 ) & 0x3f;
	nPadLength = (nIndex < 56 ) ? (56 - nIndex ) : (120 - nIndex );
	
	CmsSha1Update( psCtx, sPad, nPadLength);
	
	/* Append length (before Padding) */
	CmsSha1Update( psCtx, sBits, 8);
	
	/* Store pState in digest */
	for (nIdx = 0; nIdx < 5; nIdx++ ) {
		pHashValue[(nIdx << 2 )] = (unsigned char )(psCtx->caStat[nIdx] >> 24 );
		pHashValue[(nIdx << 2 ) + 1] = (unsigned char )(psCtx->caStat[nIdx] >> 16 );
		pHashValue[(nIdx << 2 ) + 2] = (unsigned char )(psCtx->caStat[nIdx] >> 8 );
		pHashValue[(nIdx << 2 ) + 3] = (unsigned char )psCtx->caStat[nIdx];
	}

	memset(psCtx->caBuf, 0, 64 );
	memset(psCtx->caStat, 0, 20 );
}


/* ============================================================================ function body === */
void
CmsSha1 (
		 unsigned char *		pHashValue,			// [O]

		 unsigned char *		pInput,				// [I]
		 unsigned int		nInputByteLength	// [I]
)
{
	HASH_CTX *				psCtx = NULL;

 	psCtx = /*ExAllocatePool(NonPagedPool , sizeof(DK_HASH_CTX ) );*/calloc(1, sizeof(HASH_CTX) );
	memset(psCtx,0x00,sizeof(HASH_CTX ) );
	psCtx->nAlgId = HASH_ALGID_SHA1;

	CmsSha1Init( psCtx);

	CmsSha1Update( psCtx, pInput, nInputByteLength);

	CmsSha1Final( psCtx, pHashValue);

 	/*ExFreePool(psCtx);*/free(psCtx );
}


