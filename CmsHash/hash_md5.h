/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/
#ifndef _HASH_MD5_H_
#define _HASH_MD5_H_

#include "hash_global.h"
#include "hash.h"

#ifdef __cplusplus
extern "C" {
#endif 

void CmsMd5Init( HASH_CTX * psCtx);
void CmsMd5Update( HASH_CTX * psCtx,	unsigned char * pInput, unsigned int nInputByteLength);
void CmsMd5Final( HASH_CTX *	psCtx, unsigned char * pHashValue);
void CmsMd5( unsigned char * pHashValue, unsigned char * pInput, unsigned int nInputByteLength);


#ifdef __cplusplus
} 
#endif 

#endif