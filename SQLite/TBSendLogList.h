#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBSendLogList :public CDBConnect
{
public:
	CTBSendLogList(void);
	virtual ~CTBSendLogList(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
