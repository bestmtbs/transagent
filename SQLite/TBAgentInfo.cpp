#include "StdAfx.h"
#include "TBAgentInfo.h"

CTBAgentInfo::CTBAgentInfo(void)
{
}

CTBAgentInfo::~CTBAgentInfo(void)
{
}
/**
@brief     QueryAllSelect
@author   JHLEE
@date      2011.09.28
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@note		실질적인 쿼리 처리는 GetAllMessage함수에서 한다.
@return	bool(true/false)
*/
bool CTBAgentInfo::QueryAllSelect(TCHAR* _szQuery, CStringArray* _parrData)
{

	bool bSelect = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bSelect = GetAllMessage(strQuery, _parrData);


	return bSelect;
}

/**
@brief     GetAllMessage
@author   JHLEE
@date      2011.09.28
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@note		select 쿼리만을 전담하는 함수
@return	bool(true/false)
*/
bool CTBAgentInfo::GetAllMessage(CString _strQuery, CStringArray* _parrData)
{
	bool bRet = false;
	CString strDevice = _T(""), strDeviceUUID = _T(""), strVer = _T(""), \
		strDepartmentName = _T(""), strAgentmentName = _T(""), strDepartmentID = _T(""), \
		strLicenseCode = _T(""), strCorperationName = _T(""), strRegisterDate = _T("");
	CString strTotal  = _T("");

	CDbDataSet *pDataSet = new CSQLiteDataSet();

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 lock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//ezLog(_T("[DBConnect] CTBConfig::GetAllMessage. before LockCMSDBMutex(). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//HANDLE hMutexForCMSDB = LockCMSDBMutex();
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//ezLog(_T("[DBConnect] CTBConfig::GetAllMessage. after LockCMSDBMutex(). hMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), hMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_

	if( ExecQuery(_strQuery, pDataSet) == true )
	{
		while ( !pDataSet->IsEof() )
		{   
			//full_path, thread_idx, file_size, current_offset, last_write_date
			strTotal = _T("");
			strDevice.Format(_T("%s"), pDataSet->GetStringField(_T("device")));
			strDeviceUUID.Format(_T("%s"), pDataSet->GetStringField(_T("device_uuid")));
			strVer.Format(_T("%s"),  pDataSet->GetStringField(_T("ver")));
			strDepartmentName.Format(_T("%s"),  pDataSet->GetStringField(_T("department_name")));
			strAgentmentName.Format(_T("%s"), pDataSet->GetStringField(_T("agent_name")));
			strDepartmentID.Format(_T("%d"), pDataSet->GetIntField(_T("department_id")));
			strLicenseCode.Format(_T("%s"), pDataSet->GetStringField(_T("license_code")));
			strCorperationName.Format(_T("%s"), pDataSet->GetStringField(_T("corp_name")));
			strRegisterDate.Format(_T("%s"), pDataSet->GetStringField(_T("regist_date")));
			strTotal.Format(_T("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|"), strDevice, strDeviceUUID, strVer, strDepartmentName, strAgentmentName, strDepartmentID, \
				strLicenseCode, strCorperationName, strRegisterDate, strRegisterDate);

			_parrData->Add(strTotal);

			pDataSet->Next();
		}

		bRet = true;
	}

	if( pDataSet != NULL )
	{
		delete pDataSet;
		pDataSet =NULL;
	}

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//ezLog(_T("[DBConnect] CTBConfig::GetAllMessage. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//UnlockCMSDBMutex(hMutexForCMSDB);

	return bRet;

// 	if( _parrData->GetSize() <= 0 )
// 		return false;
// 
// 	return true;
}
