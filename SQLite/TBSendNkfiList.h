#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBSendNkfiList :public CDBConnect
{
public:
	CTBSendNkfiList(void);
	virtual ~CTBSendNkfiList(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
