#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBNotice :public CDBConnect
{
public:
	CTBNotice(void);
	virtual ~CTBNotice(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
