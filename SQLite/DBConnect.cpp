
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      CDBConnect.cpp 
 @brief     CDBConnect  구현 파일  
 @author   jhlee
 @date      create 2011.06.22
 */
#include "StdAfx.h"
#include "DBConnect.h"
#include <Aclapi.h.>
#include "PathInfo.h"

#include "UtilsUnicode.h"
#include "UtilsFile.h"
#include "UtilParse.h"
#include <yvals.h>
//#include "CommonDefine.h"


const CString CDBConnect::FILENAME = WTIOR_SETTING_DATABASE;
//HANDLE CDBConnect::mhMutexForCMSDB = INVALID_HANDLE_VALUE;	// 2017-03-30 kh.choi C:\dsntech\TiorSaver\DB\CMSDB.db 파일에 동시 접속을 막기위한 동기화용 뮤텍스

//#define _CMSDB_MUTEX_SHOW_LOG_	// 2017-04-19 kh.choi 로컬디비 뮤텍스 디버깅을 위한 선언

/********************************************************************************
 @class     CDBConnect
 @brief     로컬 디비 사용을 전담하는 클래스(sqlite)
 @note     사용자 인증 정보, 정책, 로그, 관리자 알림 메세지 등 사용자의 정보를 
                로컬 디비를 통해 정보를 공유 ( create, select, insert, update, delete)
*********************************************************************************/

/**
 @brief     생성자
 @author   JHLEE
 @date      2011.06.22
 @note      로컬 디비 연결
*/
CDBConnect::CDBConnect(void)
{
	//CreateDBFile();
	m_strFilePath = _T("");
	//m_bFromGetMessage = FALSE;
	UM_WRITE_LOG(_T("[DBConnect] 1"));
	//Open();
}

/**
 @brief     소멸자
 @author   JHLEE
 @date      2011.06.22
*/
CDBConnect::~CDBConnect(void)
{
//#ifdef _CMSDB_MUTEX_SHOW_LOG_
//	////ezLog(_T("[DBConnect] ~CDBConnect(). mhMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), mhMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-04-19 kh.choi 파일로그
//#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	Close();

//	// 2017-03-30 kh.choi mhMutexForCMSDB 닫기
	//if (mhMutexForCMSDB && mhMutexForCMSDB != INVALID_HANDLE_VALUE) {
		//CloseHandle(mhMutexForCMSDB);
//#ifdef _CMSDB_MUTEX_SHOW_LOG_
//		////ezLog(_T("[DBConnect] CloseHandle(mhMutexForCMSDB). mhMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), mhMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-04-19 kh.choi 파일로그
//#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//}
	//mhMutexForCMSDB = INVALID_HANDLE_VALUE;
}

/**
 @brief     SQLite로 CMSDB.db  파일 오픈
 @author   JHLEE
 @date      2011.06.22
 @return	bool(true/false)
*/
bool CDBConnect::Open()
{
	UM_WRITE_LOG(_T("[DBConnect] 2"));

	if( m_strFilePath.IsEmpty() )
		m_strFilePath.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), FILENAME);
	else
		m_strFilePath.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), m_strFilePath);
	m_SQLiteConnection.SetConnectionString(m_strFilePath);

	UM_WRITE_LOG(_T("[DBConnect] 3"));
	CString strLog = _T("");
	strLog.Format(_T("[DBConnect] m_strFilePath : %s [line: %d, function: %s]"), m_strFilePath, __LINE__, __FUNCTIONW__);
	UM_WRITE_LOG(strLog);
	
	return m_SQLiteConnection.Open();
}

/**
 @brief     SQLite가 연결되어 있는지 확인한다.
 @author   JHLEE
 @date      2011.06.22
 @note	    연결되어 있지 않는 경우 연결을 시도한다.
*/
bool CDBConnect::IsConnected()
{
	m_ConSection.Lock();

	bool bRet = m_SQLiteConnection.IsConnected();
	if (!bRet)
	{
		bRet = m_SQLiteConnection.Open();
	}

	m_ConSection.Unlock();

	return bRet;
}

/**
 @brief     메세지 파일을 모든 사용자가 접근할수있는 권한으로 생성한다. 
 @author    jhlee
 @date      2011.06.22
 @note      SQLite가 사용할 Db파일을 생성한다.
 @note      생성위치 : program files/ITCMS/DB/
 @note      모든 사용자가 접근가능하게 권한을 설정하여 생성한다.
*/
bool CDBConnect::CreateDBFile()
{
	if( FileExists(m_strFilePath) )
		return true;

	PSID pEveryoneSID = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_LOCAL_SID_AUTHORITY;
	SECURITY_ATTRIBUTES sa;
	EXPLICIT_ACCESS ea;
	DWORD dwRes;
	PSECURITY_DESCRIPTOR pSD = NULL;
	PACL pACL = NULL;

	::AllocateAndInitializeSid( &SIDAuthWorld, 1, SECURITY_IUSER_RID,0, 0, 0, 0, 0, 0, 0,
		&pEveryoneSID);

	::ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));
	ea.grfAccessPermissions = 0x001FFFFF;
	ea.grfAccessMode = SET_ACCESS;
	ea.grfInheritance= SUB_CONTAINERS_AND_OBJECTS_INHERIT; 
	ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea.Trustee.ptstrName = reinterpret_cast<LPTSTR>(pEveryoneSID);

	dwRes = ::SetEntriesInAcl(10, &ea, NULL, &pACL);
	pSD = reinterpret_cast<PSECURITY_DESCRIPTOR>(::LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH)); 

	::InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION);
	::SetSecurityDescriptorDacl(pSD, TRUE, pACL, FALSE);

	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = pSD;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);

	HANDLE hFile = ::CreateFile( m_strFilePath , GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, &sa, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);        
	bool bRet = false;
	bRet = (INVALID_HANDLE_VALUE == hFile);
	::CloseHandle(hFile);
	::LocalFree(pSD);
	return bRet;
}
/**
 @brief     SQL 문장을 실행한다.
 @author    JHLEE
 @date      2011.06.22
 @param     [in] _sSql      실행할 SQL문장
 @param     [in] _pDataSet  실행결과를 저장할 DataSet 포인터(기본값 = NULL)
 @return    true/false
 @note      _pDataSet은 기본이 NULL값이기 때문에 필요하지 않으면 빈공간으로 두면 된다.
*/
bool CDBConnect::ExecQuery(const CString _sSql,CDbDataSet *_pDataSet)
{
	/*  //원래꺼
	if (!IsConnected())
			return false;
	
		if( _sSql.IsEmpty() )
			return false;
	
		m_Section.Lock();
	
		CDbCommand *pCommand = NULL;
		pCommand  = new CSQLiteCommand(&m_SQLiteConnection, reinterpret_cast<CSQLiteDataSet *>(_pDataSet));
		if (NULL == pCommand)
			return false;
	
		int nRecord = 0;
		pCommand->SetCommandText(_sSql);
		
		bool bResult = pCommand->Execute(nRecord);
	
		if (!bResult)
		{
			CString strMsg;
			strMsg.Format(_T("CMSDB:ExecQuery ERROR(%s)(%d)"), _sSql,GetLastError());
	//		UM_WRITE_LOG(strMsg);
			//ERROR 코드 찍을 필요있음
		}
	
		if( pCommand != NULL)
		{
			delete pCommand;
			pCommand = NULL;
		}
	
		m_Section.Unlock();
	
		return bResult;*/

	//BOOL bSkipCloseMutexHandle = FALSE;	// 2017-04-19 kh.choi GetMessage() 에서 호출된 경우 뮤텍스 CloseHandle 하지 않도록
	//if (m_bFromGetMessage) {
	//	bSkipCloseMutexHandle = TRUE;
	//}
	//m_bFromGetMessage = FALSE;

	CString strLog = _T("");
	strLog.Format(_T("[DBCONNECT] ExecQuery - %s [line: %d, function: %s, file: %s]"), _sSql, __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);
	////ezLog(_T("[DBConnect] ExecQuery. _sSql: %s [line: %d, function: %s, file: %s]\r\n"), _sSql, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 lock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] ExecQuery. before LockCMSDBMutex(). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//HANDLE hMutexForCMSDB = LockCMSDBMutex();
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] ExecQuery. after LockCMSDBMutex(). hMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), hMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_

	if (!IsConnected())
	{
		UM_WRITE_LOG(_T("IsConnected FAILED"));
		////ezLog(_T("[DBConnect] [ERROR] IsConnected() return FAIL. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
		
		// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
		////ezLog(_T("[DBConnect] ExecQuery. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
		//UnlockCMSDBMutex(hMutexForCMSDB);

		return false;
	}

	if( _sSql.IsEmpty() )
	{
		UM_WRITE_LOG(_T("_sSql.IsEmpty()"));
		////ezLog(_T("[DBConnect] [ERROR] _sSql.IsEmpty() return TRUE. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그

		// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
		////ezLog(_T("[DBConnect] ExecQuery. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
		//UnlockCMSDBMutex(hMutexForCMSDB);

		return false;
	}

	bool bResult;

	m_Section.Lock();
	
	CDbCommand *pCommand = NULL;

	try
	{
		//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 2"));
		pCommand  = new CSQLiteCommand(&m_SQLiteConnection, reinterpret_cast<CSQLiteDataSet *>(_pDataSet));
		if (NULL == pCommand)
		{
			m_Section.Unlock();

			// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
			////ezLog(_T("[DBConnect] ExecQuery. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
			//UnlockCMSDBMutex(hMutexForCMSDB);

			UM_WRITE_LOG(_T("NULL == pCommand"));
			////ezLog(_T("[DBConnect] [ERROR] NULL == pCommand [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
			//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 3"));
			return false;
		}

		//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 4"));
		int nRecord = 0;
		pCommand->SetCommandText(_sSql);
		int nErr = pCommand->GetError();
		BOOL bErrorLogPrinted = FALSE;
		if(nErr != SQLITE_OK)
		{
			TCHAR strErr[MAX_PATH];
			wsprintf(strErr, L"nError = %d", nErr);
			UM_WRITE_LOG(strErr);
			////ezLog(_T("[DBConnect] [ERROR] pCommand->GetError() return %d [line: %d, function: %s, file: %s]\r\n"), nErr, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
			bErrorLogPrinted = TRUE;
		}		
		//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 5"));
		bResult = pCommand->Execute(nRecord);
		//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 6"));

		if (!bResult)
		{
			DWORD dwError = pCommand->GetError();
			CString strMsg;
			strMsg.Format(_T("CMSDB:ExecQuery[sqllite Err = %d](%s)"), dwError, _sSql);
			UM_WRITE_LOG(strMsg);

			// 2017-03-30 kh.choi 파일 로그가 출력되지 않았거나, 에러코드가 바뀌었을 경우에만 파일 로그 출력하도록 수정.
			if (!bErrorLogPrinted || nErr != (int)dwError) {
				////ezLog(_T("[DBConnect] [ERROR] pCommand->Execute(nRecord) return FALSE. pCommand->GetError() return %d. nRecord: %d, _sSql: %s [line: %d, function: %s, file: %s]\r\n"), dwError, nRecord, _sSql, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
			}

			//ERROR 코드 찍을 필요있음	// 2017-03-30 kh.choi 이전부터 있던 주석. 내용 파악 안됨.
		}

		if( pCommand != NULL)
		{
			delete pCommand;
			pCommand = NULL;
		}

		// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
		////ezLog(_T("[DBConnect] ExecQuery. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
		//UnlockCMSDBMutex(hMutexForCMSDB);

		m_Section.Unlock();

	}
	catch (CException* e)
	{
		if( pCommand != NULL)
			delete pCommand;
		
		m_Section.Unlock();

		// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
		TCHAR szMsg[1024] = { 0, };
		e->GetErrorMessage(szMsg, 1024);
		CString strMsg = _T("");
		strMsg.Format(_T("[DBConnect]%s[line: %d, function: %s, file: %s]\r\n"), szMsg, __LINE__, __FUNCTIONW__, __FILEW__);
		DBGLOG((LPWSTR)(LPCWSTR)strMsg);
		//UnlockCMSDBMutex(hMutexForCMSDB);

		// 2017-03-30 kh.choi Unlock 아래로 순서 바꿈. 뮤텍스 release 먼저 하고나서 파일 로그 남기도록
		//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 7"));
		////ezLog(_T("[DBConnect] [ERROR] in catch(). GetLastError(): %d, _sSql: %s [line: %d, function: %s, file: %s]\r\n"), GetLastError(), _sSql, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그

		return false;
	}

	//UM_WRITE_LOG(_T("[CmsTray] ExecQuery - 8"));
	return bResult;
	
}


/**
 @brief      CMSDB.db에 테이블을 생성한다.
 @author    jhlee
 @date      2011.06.22
 @note      CMSDB.db에 파일로 빠져있는 테이블들을 생성한다.
                 테이블 생성 쿼리 파일은 클라이언트 설치 경로인 program files/ITCMS/DB 폴더안에 CreateTable.sql로 존재한다.
*/
bool CDBConnect::CreateTable()
{
	if (!IsConnected())
		return false;

	CString strFilePath = _T(""), strQuery = _T("");
	if (-1 != m_strFilePath.Find(WTIOR_SETTING_DATABASE)) {
		strQuery.Format(_T("create table if not exists db_agent_info (\
			device nvarchar(50),\
			device_uuid varchar(260) primary key,\
			ver nvarchar(20),\
			department_name nvarchar(30),\
			agent_name nvarchar(50),\
			department_id int,\
			license_code nvarchar(50),\
			corp_name nvarchar(50),\
			regist_date datetime);\
\
		create table if not exists db_server_info(\
			server_host_url	 varchar(260) primary key\
		);\
\
		create table if not exists db_pc_info(\
			device_uuid varchar(260) primary key,\
			agent_ip nvarchar(20),\
			mac nvarchar(30),\
			os nvarchar(50),\
			program text);\
\
		create table if not exists db_collect_policy(\
			device_uuid	 varchar(260),\
			collect nvarchar(30) primary key);\
\
		create table if not exists db_notice(\
			device_uuid	 varchar(260),\
			notice nvarchar(50) primary key\
		);\
\
		create table if not exists db_state_policy(\
			target nvarchar(20) default \"all\",\
			target_id varchar(260) primary key,\
			state	 bool default 1,\
			init_screen_idx int default - 1,\
			thumnail_start bool default 0\
		);\
\
		create table if not exists db_url_policy(\
			device_uuid	 varchar(260),\
			domain text);"));
	} else if (-1 != m_strFilePath.Find(WTIOR_NCVI_DATABASE)) {
		strQuery.Format(_T("create table if not exists db_send_ncvi_list (\
			full_path nvarchar(1024),\
			thread_idx int,\
			file_size int,\
			current_offset int default 0,\
			last_write_date datetime,\
			last_scan_time datetime,\
			each_scan_delay int default 5);"));
	} else if (-1 != m_strFilePath.Find(WTIOR_NKFI_DATABASE)) {
		strQuery.Format(_T("create table if not exists db_send_nkfi_list (\
			full_path nvarchar(1024),\
			thread_idx int,\
			file_size int,\
			current_offset int default 0,\
			last_write_date datetime,\
			last_scan_time datetime,\
			each_scan_delay int default 5);"));
	} else if (-1 != m_strFilePath.Find(WTIOR_LOG_DATABASE)) {
		strQuery.Format(_T("create table if not exists db_send_log_list (\
			full_path nvarchar(1024),\
			thread_idx int,\
			file_size int,\
			current_offset int default 0,\
			last_write_date datetime,\
			last_scan_time datetime,\
			each_scan_delay int default 5);"));
	}
	if (strQuery.IsEmpty())  return false;

	CStringArray arrList;
	arrList.RemoveAll();
	CParseUtil::ParseStringToStringArray(_T(";"), strQuery,&arrList);

	for( int i=0; i< arrList.GetSize(); i++)
	{
		CString strValue = arrList.GetAt(i);
		strValue = strValue + _T(";");
		UM_WRITE_LOG(_T("CreateDb : ") + strValue);

		if (!ExecQuery(strValue))
		{
			CString strError; 
			strError.Format(_T("%d"), GetLastError());
			//AfxMessageBox(strError);

			return false;
		}
	}
	return true;
}


/**
 @brief     SQLite Connection을 닫는다.
 @author   jhlee
 @date      2011.06.22
 @return   bool(true/false)
*/
void CDBConnect::Close()
{
	m_strFilePath.ReleaseBuffer();

	m_SQLiteConnection.Close();
}


/**
 @brief      데이터베이스 및 테이블 생성
 @author   jhlee
 @date      2011.06.22
 @return   bool(true/false)
*/
bool CDBConnect::QueryCreate()
{
	bool bCreate  = false;
	CreateDBFile();
	bCreate = CreateTable();

	return bCreate;
}

/**
@brief     QueryUpdate
@author   JHLEE  -> hhh수정
@date      2011.06.22
@param   [in] _szQuery - 실행될 쿼리문
@param   [in] nTryCnt - 실패시 재시도할 카운트, 만약 -1이라면 무한 재시도(반드시 업데이트해야할 데이터 같은 경우)

@note	    update 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::QueryUpdate(TCHAR *_szQuery)
{
	bool bUpdate = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bUpdate = ExecQuery(strQuery);
	return bUpdate;
}

/**
@brief     QueryInsert
@author   JHLEE
@date      2011.06.22
@param   [in] _szQuery - 실행될 쿼리문
@note	    insert 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::QueryInsert(TCHAR *_szQuery)
{
	bool bInsert =  false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bInsert = ExecQuery(strQuery);

	return bInsert;
}

/**
@brief     QueryDelete
@author   JHLEE
@date      2011.06.22
@param   [in] _szQuery - 실행될 쿼리문
@note	    delete 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::QueryDelete(TCHAR *_szQuery)
{
	bool bDelete = false;

	CString strQuery =_T("");
	strQuery.Format(_T("%s"), _szQuery);

	bDelete = ExecQuery(strQuery);

	return bDelete;
}

/**
@brief     QuerySelect
@author   JHLEE
@date       2011.09.01
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in]_strField - 원하는 값의 컬럼명
@note		원하는 컬럼값만 가져올 수 있다. 실질적인 쿼리 처리는 GetMessage함수에서 한다.
@return	bool(true/false)
*/
bool CDBConnect::QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField)
{

	bool bSelect = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bSelect = GetMessage(strQuery, _parrData, _strField);


	return bSelect;
}


/**
@brief     QuerySelect
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in]_strField - 원하는 값의 컬럼명
@note		원하는 컬럼값만 가져올 수 있다. 실질적인 쿼리 처리는 GetMessage함수에서 한다.
@return	bool(true/false)
*/
bool CDBConnect::QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField, CString _strField2)
{

	bool bSelect = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bSelect = GetMessage(strQuery, _parrData, _strField,_strField2);


	return bSelect;
}


/**
@brief     QuerySelect
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in]_strField - 원하는 값의 컬럼명
@note		원하는 컬럼값만 가져올 수 있다. 실질적인 쿼리 처리는 GetMessage함수에서 한다.
@return	bool(true/false)
*/
bool CDBConnect::QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField,CString _strField2,CString _strField3)
{

	bool bSelect = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bSelect = GetMessage(strQuery, _parrData, _strField,_strField2,_strField3);


	return bSelect;
}


/**
@brief     QuerySelect
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in]_strField - 원하는 값의 컬럼명
@note		원하는 컬럼값만 가져올 수 있다. 실질적인 쿼리 처리는 GetMessage함수에서 한다.
@return	bool(true/false)
*/
bool CDBConnect::QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField,CString _strField2,CString _strField3, CString _strField4)
{

	bool bSelect = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bSelect = GetMessage(strQuery, _parrData, _strField,_strField2,_strField3, _strField4);


	return bSelect;
}



/**
@brief     GetMessage
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in] _strField - 값을 없을 컬럼값
@note		select 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::GetMessage(CString _strQuery,  CStringArray* _parrData, CString _strField)
{

	bool bRet = false;
	CString strData = _T("");
	CDbDataSet *pDataSet = new CSQLiteDataSet();

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 lock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 1st. before LockCMSDBMutex(). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//HANDLE hMutexForCMSDB = LockCMSDBMutex();
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 1st. after LockCMSDBMutex(). hMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), hMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_

	//m_bFromGetMessage = TRUE;

	if( ExecQuery(_strQuery, pDataSet) == true )
	{
		while ( !pDataSet->IsEof() )
		{   
			strData.Format(_T("%s"),pDataSet->GetStringField(_strField));
			_parrData->Add(strData);

			pDataSet->Next();
		}
		bRet = true;
	}

	if( pDataSet != NULL )
	{
		delete pDataSet;
		pDataSet =NULL;
	}

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 1st. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//UnlockCMSDBMutex(hMutexForCMSDB);

	return bRet;

	//if( _parrData->GetSize() <=0 )
	//	return false;

	//return true;
}

/**
@brief     GetMessage
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in] _strField - 값을 없을 컬럼값
@note		select 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::GetMessage(CString _strQuery,  CStringArray* _parrData, CString _strField, CString _strField2)
{

	bool bRet = false;
	CString strData = _T(""), strData2 = _T(""), strTotal = _T("");
	CDbDataSet *pDataSet = new CSQLiteDataSet();

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 lock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 2nd. before LockCMSDBMutex(). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//HANDLE hMutexForCMSDB = LockCMSDBMutex();
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 2nd. after LockCMSDBMutex(). hMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), hMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_

	//m_bFromGetMessage = TRUE;

	if( ExecQuery(_strQuery, pDataSet) == true )
	{
		while ( !pDataSet->IsEof() )
		{   
			strTotal = _T("");
			strData.Format(_T("%s"),pDataSet->GetStringField(_strField));
			strData2.Format(_T("%s"), pDataSet->GetStringField(_strField2));

			strTotal.Format(_T("%s|%s|"), strData,strData2);

			_parrData->Add(strTotal);

			pDataSet->Next();
		}
		bRet = true;
	}

	if( pDataSet != NULL )
	{
		delete pDataSet;
		pDataSet =NULL;
	}

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 2nd. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//UnlockCMSDBMutex(hMutexForCMSDB);

	return bRet;

	//if( _parrData->GetSize() <=0 )
	//	return false;

	//return true;
}


/**
@brief     GetMessage
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in] _strField - 값을 없을 컬럼값
@note		select 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::GetMessage(CString _strQuery,  CStringArray* _parrData, CString _strField,CString _strField2, CString _strField3)
{

	bool bRet = false;
	CString strData = _T(""), strData2 = _T(""), strData3 = _T(""),strTotal = _T("");
	CDbDataSet *pDataSet = new CSQLiteDataSet();

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 lock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 3rd. before LockCMSDBMutex(). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//HANDLE hMutexForCMSDB = LockCMSDBMutex();
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 3rd. after LockCMSDBMutex(). hMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), hMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_

	//m_bFromGetMessage = TRUE;

	if( ExecQuery(_strQuery, pDataSet) == true )
	{
		while ( !pDataSet->IsEof() )
		{   
			strTotal = _T("");
			strData.Format(_T("%s"),pDataSet->GetStringField(_strField));
			strData2.Format(_T("%s"), pDataSet->GetStringField(_strField2));
			strData3.Format(_T("%s"), pDataSet->GetStringField(_strField3));

			strTotal.Format(_T("%s|%s|%s|"), strData,strData2, strData3);

			_parrData->Add(strTotal);

			pDataSet->Next();
		}
		bRet = true;
	}

	if( pDataSet != NULL )
	{
		delete pDataSet;
		pDataSet =NULL;
	}

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	////ezLog(_T("[DBConnect] GetMessage 3rd. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//UnlockCMSDBMutex(hMutexForCMSDB);

	return bRet;

//	if( _parrData->GetSize() <=0 )
//		return false;

//	return true;
}

/**
@brief     GetMessage
@author   JHLEE
@date      2011.08.27
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@param	[in] _strField - 값을 없을 컬럼값
@note		select 쿼리만을 전담하는 함수
@return		bool(true/false)
*/
bool CDBConnect::GetMessage(CString _strQuery,  CStringArray* _parrData, CString _strField,CString _strField2, CString _strField3, CString _strField4)
{

	bool bRet = false;
	CString strData = _T(""), strData2 = _T(""), strData3 = _T(""), strData4 = _T(""), strTotal = _T("");
	CDbDataSet *pDataSet = new CSQLiteDataSet();

	if( ExecQuery(_strQuery, pDataSet) == true )
	{
		while ( !pDataSet->IsEof() )
		{   
			strTotal = _T("");
			strData.Format(_T("%s"),pDataSet->GetStringField(_strField));
			strData2.Format(_T("%s"), pDataSet->GetStringField(_strField2));
			strData3.Format(_T("%s"), pDataSet->GetStringField(_strField3));
			strData4.Format(_T("%s"), pDataSet->GetStringField(_strField4));
			strTotal.Format(_T("%s|%s|%s|%s|"), strData,strData2, strData3,strData4);

			_parrData->Add(strTotal);

			pDataSet->Next();
		}
		bRet = true;
	}

	if( pDataSet != NULL )
	{
		delete pDataSet;
		pDataSet =NULL;
	}
	return bRet;
}
