#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBCollectPolicy :public CDBConnect
{
public:
	CTBCollectPolicy(void);
	virtual ~CTBCollectPolicy(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
