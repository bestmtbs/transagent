

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      DBConnect.h
 @brief     CDBConnect  정의 파일
 @author   jhlee
 @date      create 2011.06.22
 */
#pragma once

#include <afxmt.h>

#include "sqlite.h"

class   CDBConnect
{
public:
	CDBConnect(void);
	virtual ~CDBConnect(void);

	bool Open();
	void Close();
	bool IsConnected();
	bool CreateDBFile();
	bool ExecQuery(const CString _sSql,CDbDataSet *_pDataSet = NULL);
	bool CreateTable();

	virtual bool QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField);
	virtual bool QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField, CString _strField2);
	virtual bool QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField, CString _strField2, CString _strField3);
	virtual bool QuerySelect(TCHAR*_szQuery,  CStringArray* _parrData ,CString _strField, CString _strField2, CString _strField3,  CString _strField4);
	virtual bool QueryAllSelect(TCHAR*_szQuery, CStringArray* _parrData)=0;
	virtual bool QueryUpdate(TCHAR*_szQuery) ;
	virtual bool QueryInsert(TCHAR*_szQuery);
	virtual bool QueryDelete(TCHAR*_szQuery) ;

	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData)=0;	
	virtual bool GetMessage(CString _strQuery, CStringArray* _parrData, CString _strField);
	virtual bool GetMessage(CString _strQuery, CStringArray* _parrData, CString _strField,CString _strField2);
	virtual bool GetMessage(CString _strQuery, CStringArray* _parrData, CString _strField,CString _strField2, CString _strField3);
	virtual bool GetMessage(CString _strQuery, CStringArray* _parrData, CString _strField,CString _strField2, CString _strField3, CString _strField4);
	bool QueryCreate();
	
public:
	CSQLiteConnection        m_SQLiteConnection;

	CString						  m_strFilePath;

	CCriticalSection m_Section;
	CCriticalSection m_ConSection;

	static const CString FILENAME;
	//static HANDLE LockCMSDBMutex(VOID);	// 2017-05-01 kh.choi 로컬디비 접속용 뮤텍스 lock
	//static VOID UnlockCMSDBMutex(HANDLE hMutexForCMSDB);	// 2017-05-01 kh.choi 로컬디비 접속용 뮤텍스 unlock

private:
	//static HANDLE mhMutexForCMSDB;	// 2017-03-30 kh.choi C:\dsntech\TiorSaver\DB\CMSDB.db 파일에 하나씩 접근하게 하기 위한 동기화용 뮤텍스
};
