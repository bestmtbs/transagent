#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBSendNcviList :public CDBConnect
{
public:
	CTBSendNcviList(void);
	virtual ~CTBSendNcviList(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
