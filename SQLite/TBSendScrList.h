#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBSendScrList :public CDBConnect
{
public:
	CTBSendScrList(void);
	virtual ~CTBSendScrList(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
