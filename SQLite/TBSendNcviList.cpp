#include "StdAfx.h"
#include "TBSendNcviList.h"

CTBSendNcviList::CTBSendNcviList(void)
{
}

CTBSendNcviList::~CTBSendNcviList(void)
{
}
/**
@brief     QueryAllSelect
@author   JHLEE
@date      2011.09.28s
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@note		실질적인 쿼리 처리는 GetAllMessage함수에서 한다.
@return	bool(true/false)
*/
bool CTBSendNcviList::QueryAllSelect(TCHAR* _szQuery, CStringArray* _parrData)
{

	bool bSelect = false;

	CString strQuery = _T("");
	strQuery.Format(_T("%s"), _szQuery);

	bSelect = GetAllMessage(strQuery, _parrData);


	return bSelect;
}

/**
@brief     GetAllMessage
@author   JHLEE
@date      2011.09.28
@param   [in] _szQuery - 실행될 쿼리문
@param   [out] _parrData - select  쿼리 실행 후 결과값을 배열에 넣어 클라이언트 모듈에 전달
@note		select 쿼리만을 전담하는 함수
@return	bool(true/false)
*/
bool CTBSendNcviList::GetAllMessage(CString _strQuery, CStringArray* _parrData)
{
	bool bRet = false;
	CString strFullPath = _T(""), strLogCycle = _T(""), strThreadIdx = _T(""), strFileSize = _T(""), strCurrentOffset = _T("");
	CString strLastWriteDate = _T("");
	CString strTotal  = _T("");

	CDbDataSet *pDataSet = new CSQLiteDataSet();

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 lock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//ezLog(_T("[DBConnect] CTBConfig::GetAllMessage. before LockCMSDBMutex(). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//HANDLE hMutexForCMSDB = LockCMSDBMutex();
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//ezLog(_T("[DBConnect] CTBConfig::GetAllMessage. after LockCMSDBMutex(). hMutexForCMSDB: %p [line: %d, function: %s, file: %s]\r\n"), hMutexForCMSDB, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_

	if( ExecQuery(_strQuery, pDataSet) == true )
	{
		while ( !pDataSet->IsEof() )
		{   
			//full_path, thread_idx, file_size, current_offset, last_write_date
			strTotal = _T("");
			strFullPath.Format(_T("%s"), pDataSet->GetStringField(_T("full_path")));
			strThreadIdx.Format(_T("%d"), pDataSet->GetIntField(_T("thread_idx")));
			strFileSize.Format(_T("%d"),  pDataSet->GetIntField(_T("file_size")));
			strCurrentOffset.Format(_T("%d"),  pDataSet->GetIntField(_T("current_offset")));
			strLastWriteDate.Format(_T("%s"), pDataSet->GetStringField(_T("last_write_date")));
			strLastWriteDate.Format(_T("%s"), pDataSet->GetStringField(_T("last_scan_time")));
			strLastWriteDate.Format(_T("%d"), pDataSet->GetIntField(_T("each_scan_delay")));

			strTotal.Format(_T("%s|%s|%s|%s|%s|"), strFullPath, strThreadIdx,strFileSize,strCurrentOffset, strLastWriteDate);

			_parrData->Add(strTotal);

			pDataSet->Next();
		}

		bRet = true;
	}

	if( pDataSet != NULL )
	{
		delete pDataSet;
		pDataSet =NULL;
	}

	// 2017-05-01 kh.choi 로컬디비 접속 뮤텍스 unlock
#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//ezLog(_T("[DBConnect] CTBConfig::GetAllMessage. will call UnlockCMSDBMutex(hMutexForCMSDB). [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-22 kh.choi 파일로그
#endif //#ifdef _CMSDB_MUTEX_SHOW_LOG_
	//UnlockCMSDBMutex(hMutexForCMSDB);

	return bRet;

// 	if( _parrData->GetSize() <= 0 )
// 		return false;
// 
// 	return true;
}
