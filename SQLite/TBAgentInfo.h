#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBAgentInfo :public CDBConnect
{
public:
	CTBAgentInfo(void);
	virtual ~CTBAgentInfo(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
