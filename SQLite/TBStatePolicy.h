#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBStatePolicy :public CDBConnect
{
public:
	CTBStatePolicy(void);
	virtual ~CTBStatePolicy(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
