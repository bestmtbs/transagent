#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBServerInfo :public CDBConnect
{
public:
	CTBServerInfo(void);
	virtual ~CTBServerInfo(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
