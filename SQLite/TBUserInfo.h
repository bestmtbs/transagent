#pragma once
#include "dbconnect.h"

class AFX_EXT_CLASS CTBUserInfo :public CDBConnect
{
public:
	CTBUserInfo(void);
	virtual ~CTBUserInfo(void);
public:	
	virtual bool QueryAllSelect(TCHAR *_szQuery,CStringArray* _parrData) ;
	virtual bool GetAllMessage(CString _strQuery, CStringArray* _parrData);

};
