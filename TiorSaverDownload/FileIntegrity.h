#pragma once

#include <atlcoll.h>
#include <atlstr.h>
#include "OfsIntegrity.h"
#include "IntegrityDB.h"




typedef  CAtlMap<CAtlString,CAtlString,CStringElementTraits<CAtlString>> FILE_LIST;


int Check_File_Integrity(int _nSetNo, int _nAction,CString _strTime, CString& _strErr);
//INI 파일의 해쉬값을 읽어온다. 
int Get_FileList(FILE_LIST& _mapList,CString _strIniPath,CString& _strErr);
int Check_Integrity_List(FILE_LIST& _mapList,CStringArray& _saList, CString _strTargetPath,BOOL _bSubPath);

//파일 해쉬값 확인 
int Check_FileIntegrity(CString _strTargetPath,CString _strHash);
int Check_IniIntegrity(CString _strIniPath);
//
int Check_File_Status(CString _strIniPath, /*CString _strSection,*/CString _strKeyName,CString _strTargetPath,CString _sValidKey ,CString& _sHash, CString& _strErr);

int Check_Integrity_DRV(FILE_LIST& mList,CStringArray& _saList,CString _strIniPath,CString& _strErr);
int Check_Integrity_APP(FILE_LIST& mList,CStringArray& _saList,CString _strIniPath,BOOL bSubPath,CString& _strErr);
void RedirectionOnOff(BOOL bFlag);



CString  GetIniHashInfo();
//CString  GetDbInfo_Config(CString _strQuery,CString _StrField);
CString   GetIniDrvSection();

CString GetFileHash_MD5(CString _strFilePath);

//CString MakeDecodeData(CString _strKeyData);

BOOL IsPeFormat(CString _strFilePath);

BOOL Check_CodeSign(LPCWSTR pwszSourceFile);