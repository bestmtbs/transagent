#pragma once
#include <atlcoll.h>
#include "FileIntegrity.h"



#define FW_STANDARD_SET			_T("SYSTEM\\CurrentControlSet\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile\\AuthorizedApplications\\List\\")
#define VALUSE_FORMAT		        _T("%s:*:Enabled:%s")
#define ADD_FIREWALL_FORMAT   _T("cmd.exe /c  netsh advfirewall firewall add rule name=\"%s\" dir=in action=allow program=\"%s\" enable=yes")
#define DEL_FIREWALL_FORMAT   _T("cmd.exe /c  netsh advfirewall firewall delete rule name=\"%s\" program=\"%s\"")



#define OS_XP										0
#define OS_32										1
#define OS_64										2
#define OS_64_IA									3
#define TRY_COUNT									3
#define DISAPPEAR_TIME								5

#define STR_CMS_DB_SECTION						_T("DB Command")


#define STR_FILE_BACKUP							_T("Backup_OFS_")		
#define STR_UPDATE_DOWN_FOLDER					_T("UDownload")
#define STR_UPDATE_BACKUP_FOLDER				_T("UBackup")

#define STR_DB_CMD_TXT									_T("DB_CMD.txt")

#define STR_CMSVER_INI							_T("TSVersion.ini")
#define STR_CMS_VER_SECTION						_T("TSVersion")

#define STR_VALID_KEY						_T("VGlvclNhdmVy")												//base64 encode : itcms_update
#define SE_ENCKEY								_T("KB2ItCms#!R2")

#define STR_CMS_OPTION							_T("OPTION")

const int UPDATE_SUCCESS = 0;
const int UPDATE_ERROR_SERVER_INFO  = -1;
const int UPDATE_ERROR_CONNECT  = -2;
const int UPDATE_ERROR_INFO_FILE  = -3;
const int UPDATE_ERROR_DOWN_INFO  = -4;
const int UPDATE_ERROR_DOWNLOAD  = -5;
const int UPDATE_ERROR_UPGRADE  = -6;


const int UPDATE_ERROR_INTERGRITY  = -7;
const int UPDATE_ERROR_VERSION_INI  = -8;
const int UPDATE_ERROR_DB		  = -9;
const int UPDATE_ERROR_CHANGE_APP  = -10;
const int UPDATE_ERROR_CHANGE_DRV  = -11;
const int UPDATE_ERROR_INI_VERSION  = -12;
const int UPDATE_ERROR_INTEGRIY_CHANGE = -13;
const int UPDATE_ERROR_INTEGRIY_CHANGE_APP = -14;
const int UPDATE_ERROR_INTEGRIY_CHANGE_DRV = -15;
const int UPDATE_ERROR_INI_EXIST = -16;
const int UPDATE_ERROR_TARGET_VERSION  = -17;
const int UPDATE_ERROR_INI_HASH_KEY = -18;



typedef  CAtlMap<CAtlString,CAtlString,CStringElementTraits<CAtlString>> UPDATE_LIST;


class CUpdateReport
{
public:
	CUpdateReport(void);
	~CUpdateReport(void);

	virtual void Report(int nTotal, int nCurrent, CString _strReport);
};




class CDownload
{
public:
	CDownload(void);
	~CDownload(void);

	void Init();

	int Start(CString strVersion);
	int Start_Setup();

	void	SetReport(CUpdateReport* pClass){m_pReport = pClass;}
	//CString GetErrMsg();
	

protected:
	CString DecodeBase64(CString strEncData);
	BOOL ChangeFile(CString strTargetPath,CString strSrcPath,CString strFileName,BOOL bDelete = FALSE);
	
	


	//static void RedirectionOnOff(BOOL bFlag);
	//BOOL CheckFile(CString strFile,CString strValue);
//	CString GetFileMD5(CString	_strFilePath);
//	CString MakeDecodeData(CString _strKeyData);

protected:
	
	int Get_SetupVersion();
	int Get_DownloadInfo();
	int Add_Download_List(FILE_LIST& mInfo, CStringArray& sList,CString strAdd);
	int Check_DownloadInfo(CString strIni);
	//int Connect_Server();
	int Download();
	BOOL CurlDownload(CString RemoteFile, CString LocalFile);
	int UpgradeExcute(CString _strVersion);
	BOOL DownLoadFile(CString strRemotFile, CString strDowFullnPath);
   //BOOL Check_FileHash(CString strVersionPath , CString _strSection,BOOL bSub=TRUE, BOOL bIntegrity=FALSE);

//	BOOL Check_FileHash(CString strVersionPath , CString _strSection,CString strRootPath,BOOL bSub = TRUE);
//	BOOL Check_FileHash(CString strTargetPath,CString strHash);
	


	BOOL Get_UrlInfo();

	void CheckStatus();
	void CheckStatus_FireWall();

	void CheckFilefilterDownloadSkip();

protected:
	CUpdateReport*        m_pReport;

	CString m_TargetVersion;
	BOOL m_bSetUp;


	UPDATE_LIST         m_List;
	BOOL				m_bConnect;

	CString m_sFtpIP;
	CString m_sFtpID;
	CString m_sFtpPW;
	int     m_nFTPPort;

	CString m_strErr;
	int		m_nErr;

	CString m_sDownRootPath;
	//CString m_sRemotePath;
	int		m_nOSType;

	CString m_strValidKey;
	BOOL	m_bSkipDownloadFileFilter;	// 2016-01-27 sy.choi 레지스트리에 XPSkipUpdate 값이 1이면 파일필터, 네트웍필터 sys 파일 업데이트 스킵하기위해 flag set

};
