// TiorSaverDownload.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "TiorSaverDownload.h"
//#include "Patch.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#include "WTSSession.h"
#include "pipe.h"
#include "C_Update_S_Agent/PTOAPipeClient.h"
#include "UtilsProcess.h"
#include "Download.h" 
#include "MinidumpHelp.h"


// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;

//VOID closeEzLog(void)
//{
//	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
//	SingletonForEzLog::DeleteSingleton();
//	CloseLogFile();
//}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	//SingletonForEzLog::SetStrLogFilenamePrefix(_T("TiorSaverDownload"));	// 2017-01-20 kh.choi 파일로그 파일명 prefix 등록. [FileLogSet]

	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd,SW_HIDE);
	
	MinidumpHelp MiniDump;
	MiniDump.install_self_mini_dump();

	// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
		DWORD dwReturnValue = 0;

		LPWSTR* pStr = NULL;
		int iCnt;
		pStr = CommandLineToArgvW( GetCommandLine(), &iCnt);
		CString strMode = _T("");
		CString strVersion = _T("");
		bool bUpdate =true;
		if( 3 == iCnt ){
			strMode = pStr[1];
			strVersion =  pStr[2];	
		}
		LocalFree(pStr);
		
		if( iCnt != 3 ) {
			//closeEzLog();	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
			return -1;
		}


		if( CProcess::ProcessCreateMutex(WTIOR_DOWNLOAD_AGENT_MUTEX_NAME) == false )
		{
			UM_WRITE_LOG(_T("Already Start CmsUpdate.exe..")) 
			UM_WRITE_LOG(_T("업데이트 모듈이 이미 동작중입니다.."));
			//closeEzLog();	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
			exit(0);
		}
		g_cDbgLog.SetLogFileName(L"TiorSaverDownload.log");
		g_cDbgLog.InitLog();
		int nRst;

		CDownload download;

		nRst = download.Start(strVersion);
		if (UPDATE_SUCCESS != nRst) {
			SHARE_DATA* pShareData = new SHARE_DATA;
			CPTOAPipeClient PTOAPipeClient(PTOA_PIPE_NAME);
			pShareData->dwAct = ACT_AGENT_UPDATE_FAIL;
			if (TRUE == PTOAPipeClient.OnSetSendData(pShareData)) {
				if (FALSE == PTOAPipeClient.PipeClientStartUp()) {
					//return FALSE;
				} /*else
					return TRUE;*/
			} else {
				//return FALSE;
			}
		}
	}
	return nRetCode;
}
