#include "StdAfx.h"
#include "Download.h"
#include "PathInfo.h"
#include "IniFileEx.h"
//#include "FileEncrypt.h"
#include "Crypto/MD5.h"
#include "UtilsFile.h"
#include "Crypto/Base64.h"
#include "UtilsProcess.h"
#include "UtilsUnicode.h"
#include "UtilsReg.h"
#include "Impersonator.h"
#include "UtilParse.h"
#include "yvals.h"
#include <wininet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "curl\curl.h"
#pragma comment(lib, "wldap32.lib")

#ifndef _DEBUG
#pragma comment (lib, "../BuildEnv/Lib/x86/libcurl.lib")
#else	// #ifdef _DEBUG
#pragma  comment (lib, "../BuildEnv/Lib/x86/libcurld.lib")
#endif	// #else	// #ifdef _DEBUG
#pragma comment(lib, "urlmon.lib")
#pragma comment(lib, "wininet.lib")



CUpdateReport::CUpdateReport()
{

}

CUpdateReport::~CUpdateReport()
{

}

void CUpdateReport::Report(int nTotal, int nCurrent,  CString _strReport)
{
}

CDownload::CDownload(void)
{
	Init();
}

CDownload::~CDownload(void)
{
}



void CDownload::Init()
{
	m_bSetUp = FALSE;
	m_sFtpIP = _T("");
	m_sFtpID = _T("");
	m_sFtpPW = _T("");
	m_TargetVersion = _T("");
	m_pReport = NULL;
	m_nFTPPort = 0;
	m_bConnect = FALSE;
	m_sDownRootPath.Format( _T("%s%s\\"),  CPathInfo::GetClientInstallPath(), STR_UPDATE_DOWN_FOLDER);
	m_bSkipDownloadFileFilter = FALSE;

	CWinOsVersion osVer;

	if(  osVer.Is32bit() ){		
			m_nOSType = OS_32;
	
	}else{
		m_nOSType = OS_64;
	}


}




int CDownload::Start(CString strVersion)
{

	DWORD nErr = 0;

	//m_TargetVersion = strVersion;
	//m_sRemotePath = _T("/")+m_TargetVersion+_T("/");

	CheckStatus_FireWall();

	// 다운로드 리스트 수집

	//m_sDownRootPath+STR_CMSVER_INI
	if(UPDATE_SUCCESS != Check_DownloadInfo(m_sDownRootPath+STR_CMSVER_INI)){
		if(NULL != m_pReport)
			m_pReport->Report(0,0,L"Fail : Check File Infomation");
		return m_nErr;
	}
	
	CheckFilefilterDownloadSkip();
	if(0< m_List.GetCount() ){
		//5. 파일 다운로드
		//if(FALSE == Download(arDownList)){
		if(UPDATE_SUCCESS != Download()){
			ForceDeleteDir(m_sDownRootPath);

			if(NULL != m_pReport)
				m_pReport->Report(0,0,L"Fail : Download file");


			return UPDATE_ERROR_DOWNLOAD;			
		}		
	}


	if( FALSE == m_bSetUp){
		//6.업그레이드 
		
		if(TRUE != UpgradeExcute(strVersion)){

			if(NULL != m_pReport)
				m_pReport->Report(0,0,L"Fail : Excute Upgrade");
			return m_nErr;		
		}
	}
	return 0;
}

void CDownload::CheckFilefilterDownloadSkip()
{
	///레지스트리 읽어서 파일필터 다운로드 여부 결정
	DWORD dwSkipLoadFilefilter = 0;
	DWORD dwChangeFile = 0;
	HKEY hKey;
	if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,  _T("SOFTWARE\\dsntech\\TiorSaver"), 0, KEY_READ, &hKey))
	{
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);

		RegQueryValueEx(hKey, _T("XPSkipUpdate"), NULL, (LPDWORD)&value_type, (LPBYTE)&dwSkipLoadFilefilter, &value_length);
		RegCloseKey(hKey);
	}
	if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,  _T("SOFTWARE\\dsntech\\TiorSaver"), 0, KEY_READ, &hKey))
	{
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);

		RegQueryValueEx(hKey, _T("change"), NULL, (LPDWORD)&value_type, (LPBYTE)&dwChangeFile, &value_length);
		RegCloseKey(hKey);
	}
	if (1 == dwSkipLoadFilefilter && 1 == dwChangeFile) {
		m_bSkipDownloadFileFilter = TRUE;
	}
}

int CDownload::Check_DownloadInfo(CString strIni)
{
	FILE_LIST mList;
	CStringArray saList;
	CString _strErr;

	int nResult = 0;
	// yjLee(2015-04-24) : Img 폴더 내의 리소스도 무결성 검사
	// nResult = Check_Integrity_APP(mList,saList,strIni,FALSE,_strErr);
	nResult = Check_Integrity_APP(mList,saList,strIni,TRUE,_strErr);

	// 2015-08-27 kh.choi 추가
	if (OFS_CHECK_INI_VALUE_HASH_KEY == nResult) {
		return UPDATE_ERROR_INI_HASH_KEY;
	}	// 2015-08-27

	Add_Download_List(mList,saList,m_TargetVersion);

	mList.RemoveAll();
	saList.RemoveAll();
	return 0;
}


int CDownload::Add_Download_List(FILE_LIST& mInfo, CStringArray& sList,CString strAdd)
{

	int nCnt = sList.GetCount();

	if(nCnt<1)
		return 0;

	for(int nIdx =0; nIdx<nCnt; nIdx++){

		CAtlString strSearch = sList[nIdx];
		CAtlString strValue;
		
		if(mInfo.Lookup(strSearch,strValue)){
			//CAtlString strTaregt = strAdd;
			//strTaregt+=strSearch;
			CAtlString strTaregt = strSearch;
			m_List.SetAt(strTaregt,strValue);
		}
	}

	return 0;
}


int CDownload::Download()
{

	CString sRemotePath = _T("");
	CString sTargetPath;
	CString strTarget;
	CString sDownFolder;

	POSITION pos =   m_List.GetStartPosition(); 

	int nTotal = m_List.GetCount();
	int nCurCnt = 0;
	

	BOOL bSuccess = FALSE;
	while ( pos != NULL ) 
	{
		int nErr = -1;	// 2016-02-12 kh.choi m_nErr 에 UPDATE_ERROR_INTERGRITY 가 들어가야 하는 경우를 위해 선언

		CAtlString strDownFileName;
		CAtlString strValue;
		m_List.GetNextAssoc(pos, strDownFileName, strValue);

		strTarget = ExtractFileName(strDownFileName);
		sTargetPath = m_sDownRootPath+strTarget;
		sRemotePath = strDownFileName;

		int nCnt = 0;

		if(NULL != m_pReport)
			m_pReport->Report(nTotal,nCurCnt+1,strTarget);

		CString strLog = _T("");
		do{ //실패시 10초 기다렸다가 10회 반복시도

			if(TRUE == DownLoadFile(sRemotePath,sTargetPath)){
				// 다운로드 받은 파일 해쉬 검사 
				if(  OFS_CHECK_SUCCESS != Check_FileIntegrity(sTargetPath,  strValue) ){
					//m_nErr = UPDATE_ERROR_INTERGRITY;
					nErr = UPDATE_ERROR_INTERGRITY;
					//return m_nErr;	// 2016-02-12 kh.choi Check_FileIntegrity 에 실패하더라도 nCnt 10이 될 때까지 재시도 하도록 바로 리턴 안 하게 수정
				} else {
					bSuccess = TRUE;
					nCurCnt++;
					break;
				}
			}
			bSuccess = FALSE;
		}while(nCnt++ < 10);

		if(FALSE == bSuccess){
			if (UPDATE_ERROR_INTERGRITY == nErr) {
				m_nErr = UPDATE_ERROR_INTERGRITY;
			} else {
				m_nErr = UPDATE_ERROR_DOWNLOAD;
			}
			return m_nErr;
		}
	}

	{
		CString strLog = _T("");
		strLog.Format(_T("[Download] Download() will return SUCCESS. [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
	}

	return UPDATE_SUCCESS;
}

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}

CStringA /*CDownload::*/Utf8_Encode(CStringW strData)
{
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed+1, 0);
	WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), &strTo[0], size_needed, NULL, NULL);
	return strTo.c_str();
}

CStringW /*CDownload::*/Utf8_Decode(CStringA strData)
{
	int size_needed = MultiByteToWideChar(CP_UTF8, 0,strData.GetString(), -1, NULL, 0);
	std::wstring wstrTo( size_needed+1, 0 );
	wstrTo.clear();
	MultiByteToWideChar (CP_UTF8, 0, strData.GetString(),-1, &wstrTo[0], size_needed+1);
	return wstrTo.c_str();
}

BOOL CDownload::CurlDownload(CString RemoteFile, CString LocalFile)
{
	if(RemoteFile.GetLength()<3)
		return FALSE;

	CStringA strUrlA = Utf8_Encode(RemoteFile);
	CStringA strOutFileName = Utf8_Encode(LocalFile);
	CString strLog = _T("");
	DWORD dwBuffSize = 0;

	CURL *curl;
	FILE *fp;
	CURLcode res;
	curl = curl_easy_init();
	if (curl) {
		fp = fopen(strOutFileName,"wb");
		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		res = curl_easy_perform(curl);
		/* always cleanup */
		curl_easy_cleanup(curl);
		fclose(fp);
	}
	if (CURLE_OK == res) {
		strLog = _T("");
		strLog.Format(_T("[DownLoad] success"));
		UM_WRITE_LOG(strLog);
		return TRUE;
	} else {
		strLog = _T("");
		strLog.Format(_T("[DownLoad] fail %d, GetLastError(): %d"), res, GetLastError());
		UM_WRITE_LOG(strLog);
	}
	return FALSE;
}

BOOL CDownload::DownLoadFile(CString strRemotFile, CString strDowFullnPath)
{
	CString sDownPath;
	int iPos = strDowFullnPath.ReverseFind( _T('\\') );
	sDownPath = strDowFullnPath.Left( iPos);

	//다운받을 폴더가 존재하는지 체크
	if(  FALSE == FileExists(sDownPath))
	{

		if(  FALSE== ForceCreateDir(sDownPath) ){
			
			CString sLog;
			sLog.Format( _T("DownLoadFile Fail(ForceCreateDir) - Filename : %s"),strDowFullnPath);			
			UM_WRITE_LOG(sLog);		
			return FALSE;
		}
	}

	if( FileExists( strDowFullnPath) )
	{
		DWORD dwError;
		if( !ForceDeleteFile(strDowFullnPath, &dwError) )
		{
			CString sLog;
			sLog.Format( _T("DownLoadFile Fail - Filename : %s"),  strDowFullnPath);
			UM_WRITE_LOG(sLog) 
			UM_WRITE_LOG(sLog);
			return FALSE;
		}
	}
	
	if( FALSE == CurlDownload(strRemotFile, strDowFullnPath) )
		return FALSE;

	return TRUE;

}


CString CDownload::DecodeBase64(CString strEncData)
{

	CString str_Ret = _T("");
	CBase64 base;
	char	szAnsiBuff[MAX_PATH]={0,};
	char	szDecData[MAX_PATH]={0,};
	LPWSTR pRet =NULL ;

	UnicodeToAnsi(szAnsiBuff, strEncData, MAX_PATH);
	base.base64decode(szAnsiBuff, szDecData);
	pRet =AnsiCodeToUniCode(szDecData);
	str_Ret.Format( _T("%s"),  pRet);
	
	if(pRet)
		delete pRet;

	return str_Ret;
}


BOOL CDownload::UpgradeExcute(CString _strVersion)
{

	CString strAppPath = m_sDownRootPath+_T("\\");
	
	//1. 교체
	if(FileExists(strAppPath+WTIOR_UPDATE_AGENT_NAME)){
		if(FALSE == ChangeFile(CPathInfo::GetClientInstallPath(),strAppPath,WTIOR_UPDATE_AGENT_NAME))
			return FALSE;

	}
	
	//2. 실행 
	strAppPath = CPathInfo::GetClientInstallPath()+WTIOR_UPDATE_AGENT_NAME+_T(" -u ")+_strVersion;

	BOOL bSuccess = FALSE;
	int nCnt = 0;

	do{

		HINSTANCE hInstShell = ShellExecute(NULL, TEXT("open"), CPathInfo::GetClientInstallPath()+WTIOR_UPDATE_AGENT_NAME,_T("-u"), NULL, SW_HIDE);	

		if((HINSTANCE)32< hInstShell){
			bSuccess = TRUE;
			break;
		}

		nCnt++;
		Sleep(1000);
	}while(nCnt <5 );

	return bSuccess;
}






BOOL CDownload::ChangeFile(CString strTargetPath,CString strSrcPath,CString strFileName,BOOL bDelete)

{
	CString strBackupPath = strTargetPath+STR_FILE_BACKUP;
	CString strBackupFile;

	//이름 바꾸기 
	int nCnt = 0;			
	do{
		CString strNoSubFolderFileName = strFileName;
		strNoSubFolderFileName.Replace(_T("\\"), _T("_"));
		strBackupFile.Format( _T("%s%d_%s"), strBackupPath,nCnt,strNoSubFolderFileName);	// 2016-02-19 kh.choi 설치 폴더에 백업파일을 떨구기위해 strFileName 에 있는 '\' 문자를 '_' 문자로 교체
		
		if(FALSE == PathFileExists( strBackupFile)){
			break;				
		}
		nCnt++;

	}while(nCnt < 100);

	// 2016-02-17 kh.choi 100개 모두 있는 경우 실패처리
	if (100 <= nCnt) {
		return FALSE;
	}

	{
		//CString strLog = _T("");
		//strLog.Format(_T("[Download] strBackupPath: %s, strBackupFile: %s [line: %d, function : %s, file: %s]"), strBackupPath, strBackupFile, __LINE__, __FUNCTIONW__, __FILEW__);
		//UM_WRITE_LOG(strLog);
	}

	try{

		if(TRUE == PathFileExists( strTargetPath+strFileName))
		{
			{
				CString strLog = _T("");
				strLog.Format(_T("[Download] CFile::Rename(%s, %s) [line: %d, function : %s, file: %s]"), strTargetPath + strFileName, strBackupFile, __LINE__, __FUNCTIONW__, __FILEW__);
				UM_WRITE_LOG(strLog);
			}
			//CFile::Rename(strTargetPath+strFileName, strBackupFile);	// 2016-02-19 kh.choi 폴더가 다른 경우 Rename 으로는 안 되어서, MoveFileEx 로 교체
			if (FALSE == MoveFileEx(strTargetPath + strFileName, strBackupFile, MOVEFILE_REPLACE_EXISTING)) {
				{
					CString strLog = _T("");
					strLog.Format(_T("[Download] MoveFileEx(%s, %s) return FALSE. [line: %d, function : %s, file: %s]"), strTargetPath + strFileName, strBackupFile, __LINE__, __FUNCTIONW__, __FILEW__);
					UM_WRITE_LOG(strLog);
				}
				//return FALSE;
			}
		}

		if(bDelete){
			if( FALSE==MoveFileEx( strSrcPath+strFileName ,strTargetPath+strFileName, MOVEFILE_REPLACE_EXISTING) ){
				{
					CString strLog = _T("");
					strLog.Format(_T("[Download] MoveFileEx(%s, %s) return FALSE. [line: %d, function : %s, file: %s]"), strSrcPath+strFileName, strTargetPath+strFileName, __LINE__, __FUNCTIONW__, __FILEW__);
					UM_WRITE_LOG(strLog);
				}
				return FALSE;		
			}
		}
		else{
			{
				CString strLog = _T("");
				strLog.Format(_T("[Download] CopyFile(%s, %s) [line: %d, function : %s, file: %s]"), strSrcPath + strFileName, strTargetPath + strFileName, __LINE__, __FUNCTIONW__, __FILEW__);
				UM_WRITE_LOG(strLog);
			}
			if( FALSE==CopyFile( strSrcPath+strFileName ,strTargetPath+strFileName,TRUE)){	// 2016-02-19 kh.choi CopyFile 위에서 원본파일이 이동되었기 때문에 마지막 인자가 TRUE 이어도 됨
				{
					CString strLog = _T("");
					strLog.Format(_T("[Download] CopyFile(%s, %s) will return FALSE. [line: %d, function : %s, file: %s]"), strSrcPath + strFileName, strTargetPath + strFileName, __LINE__, __FUNCTIONW__, __FILEW__);
					UM_WRITE_LOG(strLog);
				}
				return FALSE;
			}
		}
	}
	catch(CFileException* pEx ){
		{
			CString strLog = _T("");
			strLog.Format(_T("[Download] catch pEx->m_cause: %d [line: %d, function : %s, file: %s]"), pEx->m_cause, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}

		pEx->Delete();

		return FALSE;
	}
	return TRUE;
}

void CDownload::CheckStatus_FireWall()
{


	CString strCmsUpdateFullPath= _T(""),  strCmsUpdateVal =  _T("");
	CString StrTiorSaverUpdatePath = _T(""),StrTiorSaverUpdateVal = _T("");
	CString strCmd = _T(""), strRundir = _T("");
	CWinOsVersion osVer;
	CImpersonator imp;

	
	strCmsUpdateFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_DOWNLOAD_AGENT_NAME );
	StrTiorSaverUpdatePath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_UPDATE_AGENT_NAME );

	
	strCmsUpdateVal.Format( VALUSE_FORMAT,  strCmsUpdateFullPath, _T("ITCMS_DOWNLOAD"));
	StrTiorSaverUpdateVal.Format( VALUSE_FORMAT,  StrTiorSaverUpdatePath, _T("ITCMS_UPGRADE"));
		
	strRundir.Format( _T("%s"), CPathInfo::GetSystem32() ) ;

	if( osVer.GetProduct() <=   osWinServer2003 )		//win2003 이하 일경우
	{

	
		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsUpdateFullPath, strCmsUpdateVal, true);
		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, StrTiorSaverUpdatePath, StrTiorSaverUpdateVal, true);

		

	}
	else{		// 비스타 이상부터는 레지스트리로 방화벽 예외처리 정보 없음(커맨드 사용)

		DWORD dwRet;
		//CmsAgent.exe 예외 처리 해제

		strCmd.Format(DEL_FIREWALL_FORMAT, _T("ITCMS_DOWNLOAD"), CPathInfo::GetClientInstallPath() + WTIOR_DOWNLOAD_AGENT_NAME );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, _T("ITCMS_UPGRADE"), CPathInfo::GetClientInstallPath() + WTIOR_UPDATE_AGENT_NAME );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );



		strCmd.Format(ADD_FIREWALL_FORMAT, _T("ITCMS_DOWNLOAD"), CPathInfo::GetClientInstallPath() + WTIOR_DOWNLOAD_AGENT_NAME );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );	
		strCmd.Format(ADD_FIREWALL_FORMAT, _T("ITCMS_UPGRADE"), CPathInfo::GetClientInstallPath() + WTIOR_UPDATE_AGENT_NAME );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );	


	}
}
int CDownload::Start_Setup()
{
	m_bSetUp = TRUE;
	CString strVersion;

	if(UPDATE_SUCCESS != Get_SetupVersion()){		
		return UPDATE_ERROR_TARGET_VERSION;
	}


	return Start(m_TargetVersion);
}

int CDownload::Get_SetupVersion()
{
	DWORD nErr = 0;

	CString strSeupIni,strRemotePath;

	strSeupIni.Format( _T("%s%s"),  CPathInfo::GetClientInstallPath(), STR_CMSVER_INI);

	//ini파일에 version정보를 읽어 비교
	CIniFileEx	CmsVerIni(strSeupIni);
	m_TargetVersion.Format(_T("%s"), CmsVerIni.ReadString(STR_CMS_VER_SECTION, STR_CMS_VER_SECTION, _T("")));	
	CmsVerIni.Clear();
	return UPDATE_SUCCESS;
}
