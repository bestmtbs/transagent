#include "stdafx.h"
#include "FileIntegrity.h"
#include <xutility>


//#include "OfsIntegrity.h"


#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <Softpub.h>
#include <wincrypt.h>
#include <wintrust.h>


///////
//#include "FileEncrypt.h"
#include "Crypto/Base64.h"
#include "Crypto/MD5.h"
#include "UtilsUnicode.h"
#include "IniFileEx.h"
#include "PathInfo.h"
//#include "TBAgentInfo.h"
#include "WinOsVersion.h"
#include "UtilsFile.h"
////
#define STR_DRV_NF						_T("dsntechnf.sys")
#define STR_DRV_NF_RENAME				_T("dsntechnf_n.sys")


//#include "CommonDefine.h"

// Link with the Wintrust.lib file.
#pragma comment (lib, "wintrust")


#define STR_VALID_KEY						_T("VGlvclNhdmVy")												//base64 encode : itcms_update
#define SE_ENCKEY								_T("KB2ItCms#!R2")


#define STR_CMSVER_INI							_T("CmsVer.ini")


#define STR_CMS_APP_SECTION						_T("APP")
#define STR_CMS_DRVXP_SECTION					_T("DRV_XP")
#define STR_CMS_DRV32_SECTION					_T("DRV_32")
#define STR_CMS_DRV32_7_SECTION					_T("DRV_32_7")
#define STR_CMS_DRV64_SECTION					_T("DRV_64")
#define STR_CMS_DRV64_7_SECTION					_T("DRV_64_7")
#define STR_CMS_DRV64_SECTION_IA				_T("DRV_64_IA")

#define OS_XP										0
#define OS_32										1
#define OS_64										2
#define OS_64_IA									3





//extern "C" OFSINTEGRITY_API int Check_Integrity(int _nSetNo, int _nAction,CString _strTime, CString& _strErr)


int Check_File_Integrity(int _nSetNo, int _nAction,CString _strTime, CString& _strErr, CString _strVersion)
{
	CString cTime = _strTime;
	CString strReport;

	int nSuccess = 1;
	

	//UM_WRITE_LOG(_T("Start"));
	{
		CString strLog = _T("");
		strLog.Format(_T("[FileIntegrity] Check_File_Integrity Start. [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
	}

	//return 1;
	CString strINI=    CPathInfo::GetClientInstallPath()+STR_CMSVER_INI;

	int nResult = 0;
	//int nResult = Check_IniIntegrity(strINI);

	//if(OFS_CHECK_SUCCESS != nResult ){
	//	//DB에 로그 생성
	//	_strErr.Format(_T("INI파일의 무결성이 올바르지 않습니다.(%d) %s"),nResult,strINI);		
	//	UM_WRITE_LOG(_strErr);		
	//	WriteIntegrityLog(_nSetNo,0,_nAction,STR_CODE_INTEGRITY_CHECK_FILE,0,_strErr,_strTime);
	//	nSuccess = 0;
	//	return   nResult;
	//}

	FILE_LIST mList;
	CStringArray saList;

	//nResult = Check_Integrity_APP(mList,saList,strINI,FALSE,_strErr);
	nResult = Check_Integrity_APP(mList,saList,strINI,TRUE,_strErr);	// 2016-02-12 kh.choi 매번 하위폴더까지 무결성 검사하도록 수정

	if(OFS_CHECK_SUCCESS != nResult ){
		nSuccess = 0;
		CString strLog = _T("");
		strLog.Format(_T("[OfsSetup] %s [line: %d, function : %s, file: %s]"), _strErr, __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
			int nCnt  = saList.GetCount();
		if(0< nCnt){
			int nIdx=0;
			while(nIdx< nCnt){
				strReport += saList.GetAt(nCnt);
				strReport += _T(",");
				nIdx++;
			}
		}else{
			strReport += _strErr;
		}
	}

	mList.RemoveAll();
	saList.RemoveAll();

	return nResult ;
}





int Check_Integrity_APP(FILE_LIST& mList,CStringArray& _saList,CString _strIniPath,BOOL bSubPath,CString& _strErr)
{
	UM_WRITE_LOG(_T("Check_Integrity_APP"));
	
	CString strErr;

	int nResult = Get_FileList(mList,_strIniPath,strErr);

	if(OFS_CHECK_SUCCESS != nResult ){
		_strErr.Format(_T("INI의 APP섹션의 값이 올바르지 않습니다.(%s)"),_strIniPath);
		CString strLog = _T("");
		strLog.Format(_T("[OfsSetup] %s [line: %d, function : %s, file: %s]"), _strErr, __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
		return nResult;
	}

	nResult = Check_Integrity_List(mList,_saList,CPathInfo::GetClientInstallPath(),bSubPath);

	if(OFS_CHECK_SUCCESS != nResult ){
		_strErr.Format(_T("INI의 APP섹션의 무결성 위반 항목을 발견하였습니다."));
		//TODO 여기서 위반항목을 DB에 작성 

		CString strLog = _T("");
		strLog.Format(_T("[OfsSetup] %s [line: %d, function : %s, file: %s]"), _strErr, __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
	}

	return nResult ;
}

int Check_Integrity_DRV(FILE_LIST& mList,CStringArray& _saList,CString _strIniPath,CString& _strErr)
{

	UM_WRITE_LOG(_T("Check_Integrity_DRV"));
	

	CString strErr;

	CString strSection = GetIniDrvSection();

	CString strTargetPath;
	strTargetPath.Format(_T("%sdrivers\\"), CPathInfo::GetSystem32());


	int nResult = Get_FileList(mList,_strIniPath,strErr);

	if(OFS_CHECK_SUCCESS != nResult ){
		_strErr.Format(_T("INI의 %s섹션의 값이 올바르지 않습니다.(%s)"), strSection, _strIniPath);	// 2015-08 kh.choi %s 2개이고, 인자 하나였던 것을 수정
		CString strLog = _T("");
		strLog.Format(_T("[OfsSetup] %s [line: %d, function : %s, file: %s]"), _strErr, __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
		return nResult;
	}


	RedirectionOnOff(FALSE);

	nResult = Check_Integrity_List(mList,_saList,strTargetPath,FALSE);	
	RedirectionOnOff(TRUE);

	if(OFS_CHECK_SUCCESS != nResult ){
		_strErr.Format(_T("INI의 %s섹션의 무결성 위반 항목을 발견하였습니다."),strSection);
		//TODO 여기서 위반항목을 DB에 작성 
		UM_WRITE_LOG(_strErr);

		{
			CString strLog = _T("");
			strLog.Format(_T("[FileIntegrity] %s [line: %d, function : %s, file: %s]"), _strErr, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
	}

	return nResult;
}




CString GetIniDrvSection()
{
	CWinOsVersion osVer;

	CString strSection;
	
	if(  osVer.Is32bit() ){					
		if( osVer.GetProduct() <=   osWinVista ){		//win2003 이하 일경우 xP로 취급			
			strSection = STR_CMS_DRV32_SECTION;
		}else{
			strSection = STR_CMS_DRV32_7_SECTION;		
		}
	
	}else{		
		if( osVer.GetProduct() <=   osWinVista ){		//win2003 이하 일경우 xP로 취급			
			strSection = STR_CMS_DRV64_SECTION;
		}else{
			strSection = STR_CMS_DRV64_7_SECTION;		
		}
	}

	return strSection;
}


int Check_File_Status(CString _strIniPath, /*CString _strSection,*/CString _strKeyName,CString _strTargetPath,CString _sValidKey ,CString& _sHash, CString& _strErr)
{

	CString strDbg;

	UM_WRITE_LOG(_T("Get_FileList"));
	UM_WRITE_LOG(_strIniPath);


	if(FALSE ==	PathFileExists(_strIniPath)){
		_strErr.Format(_T("설정한 위치에 INI파일이 없습니다. (%s)"),_strIniPath);
		return OFS_CHECK_INI_FILE;
	}
	return OFS_CHECK_SUCCESS;

	//CBase64 base;
	//char	szAnsiBuff[MAX_PATH]={0,};
	//char	szDecData[MAX_PATH]={0,};
	//LPWSTR pRet =NULL ;



	//CString strValidKey;
	//if(0 == _sValidKey.GetLength()){
	//	UnicodeToAnsi(szAnsiBuff, STR_VALID_KEY, MAX_PATH);
	//	base.base64decode(szAnsiBuff, szDecData);
	//	pRet =AnsiCodeToUniCode(szDecData);		
	//	strValidKey.Format( _T("%s"),  pRet);
	//}
	//else
	//	strValidKey = _sValidKey;

	//
	//if(strValidKey.IsEmpty()){
	//	_strErr.Format(_T("설정한 위치에 INI의 해쉬키가 없습니다.. (%s)"),_strIniPath);
	//	return OFS_CHECK_INI_HASH_KEY;
	//}


	//CString	strKeyName = _strKeyName, strValue, sLog;
	//CStringList listApp_AllKeys;

	//CIniFileEx	CmsVerIni(_strIniPath);	

	////strValue = CmsVerIni.ReadString(_strSection,strKeyName,L"");
	////CmsVerIni.ReadSection(_strSection, listApp_AllKeys);
	//int nCnt =strValue.GetLength();

	//if(nCnt<1)
	//	return OFS_CHECK_INI_VALUE;

	//CString sTargetFile = _strTargetPath+strKeyName;

	//return OFS_CHECK_SUCCESS;



}



int Get_FileList(FILE_LIST& _mapList,CString _strIniPath,CString& _strErr)
{

	CString strDbg;
	CString _strSection = _T("");

	UM_WRITE_LOG(_T("Get_FileList"));
	UM_WRITE_LOG(_strIniPath);

	if(FALSE ==	PathFileExists(_strIniPath)){
		_strErr.Format(_T("설정한 위치에 INI파일이 없습니다. (%s)"),_strIniPath);
		return OFS_CHECK_INI_FILE;
	}


	CBase64 base;
	char	szAnsiBuff[MAX_PATH]={0,};
	char	szDecData[MAX_PATH]={0,};
	LPWSTR pRet =NULL ;

	UnicodeToAnsi(szAnsiBuff, STR_VALID_KEY, MAX_PATH);
	base.base64decode(szAnsiBuff, szDecData);
	pRet =AnsiCodeToUniCode(szDecData);
	CString strValidKey;
	strValidKey.Format( _T("%s"),  pRet);

	
	if(strValidKey.IsEmpty()){
		_strErr.Format(_T("설정한 위치에 INI의 해쉬키가 없습니다.. (%s)"),_strIniPath);
		return OFS_CHECK_INI_HASH_KEY;
	}


	CString	strKeyName, strValue, sLog;
	CString strPath = _T("");
	CStringList listApp_AllKeys;

	CIniFileEx	CmsVerIni(_strIniPath);		
	CmsVerIni.ReadSections(listApp_AllKeys);
	int nCnt = listApp_AllKeys.GetCount();

	if(nCnt<1)
		return OFS_CHECK_INI_SECTION;

	POSITION pos = listApp_AllKeys.GetHeadPosition();
	listApp_AllKeys.GetNext(pos);
	
	while(pos != NULL){
		
		strKeyName = listApp_AllKeys.GetNext(pos);
		
		strDbg.Format(L"Get_FileList - [%s] = %s  ",_strSection,strKeyName);
		UM_WRITE_LOG(strDbg);


		if( !strKeyName.IsEmpty() ){
			strPath = CmsVerIni.ReadString(strKeyName, _T("path"), _T("") );
			strValue = CmsVerIni.ReadString(strKeyName, _T("hash"), _T("") );
			if( strValue.IsEmpty()){
				_strErr = strKeyName;
				_strErr.Format(_T("%s 세션 %s의  값이 비어 있습니다."),_strSection,_T("hash"));
				UM_WRITE_LOG(_strErr);
				return OFS_CHECK_INI_VALUE;
			} else {
				strValue.Remove('\"');
			}
			if( strPath.IsEmpty()){
				_strErr = strKeyName;
				_strErr.Format(_T("%s 세션 %s의  값이 비어 있습니다."),_strSection,_T("path"));
				UM_WRITE_LOG(_strErr);
				return OFS_CHECK_INI_VALUE;
			} else {
				strPath.Remove('\"');
				strPath.Remove('\\');
			}
			_mapList.SetAt(strPath,strValue);	
		}//if( !strKeyName.IsEmpty() ){		
	}//while(pos != NULL){

	return OFS_CHECK_SUCCESS;
}


int Check_Integrity_List(FILE_LIST& _mapList,CStringArray& _saList, CString _strTargetPath,BOOL _bSubPath)
{

	UM_WRITE_LOG(_T("Check_Integrity_List"));

	if(FALSE ==	PathFileExists(_strTargetPath))
		return OFS_CHECK_TARGET_PATH;

	int  nResult = OFS_CHECK_SUCCESS;


	POSITION pos =   _mapList.GetStartPosition(); 

	BOOL bSuccess = FALSE;

	CString strTarget;
	CString strDbg;


	while ( pos != NULL ) 
	{

		CAtlString strKey;
		CAtlString strHash;
		_mapList.GetNextAssoc(pos, strKey, strHash); 	

		strTarget =_strTargetPath;
		strTarget += ExtractFileName(strKey);

		BOOL bCheck = TRUE;

		if(FALSE == _bSubPath){
			 if(-1 != strKey.Find('\\'))
				bCheck = FALSE;
		}

		strDbg.Format(L"Check_Integrity_List - %s",strTarget);
		UM_WRITE_LOG(strDbg);
		
		if(TRUE == bCheck){
			if(OFS_CHECK_SUCCESS != Check_FileIntegrity(strTarget,strHash)){
				_saList.Add(strKey.GetString());				
				nResult = OFS_CHECK_FILE_INTEGRITY;

				strDbg.Format(L"Check_Integrity_List ADD- %s",strKey);
				UM_WRITE_LOG(strDbg);
			}
		}else{

			if(FALSE == PathFileExists(strTarget )){
				_saList.Add(strKey.GetString());
				nResult = OFS_CHECK_FILE_INTEGRITY;

				strDbg.Format(L"Check_Integrity_List ADD- %s",strKey);
				UM_WRITE_LOG(strDbg);
			}

		}
			
	}


	UM_WRITE_LOG(_T("Check_Integrity_List -End - "));
	return nResult;
}





int Check_FileIntegrity(CString _strTargetPath,CString _strHash)
{

	CString strTargetHash = GetFileHash_MD5(_strTargetPath);

	CString strDbg;

	strDbg.Format(L"Check_FileIntegrity - %s",_strTargetPath);
	UM_WRITE_LOG(strDbg);


	if(strTargetHash.GetLength() <1 ){
		strDbg.Format(L"Check_FileIntegrity OFS_CHECK_GET_FILE_HASH");
		UM_WRITE_LOG(strDbg);
		return OFS_CHECK_GET_FILE_HASH;
	}

	if(strTargetHash == _strHash)
		return OFS_CHECK_SUCCESS;
	else {
		strDbg.Format(L"[FileIntegrity] Check_FileIntegrity OFS_CHECK_HASH_ELSE. [line: %d, function: %s, file: %s]\
						n_strTargetPath: %s\nstrTargetHash : %s\n_strHash : %s "
						, __LINE__, __FUNCTIONW__, __FILEW__, _strTargetPath, strTargetHash, _strHash);
		UM_WRITE_LOG(strDbg);

		return OFS_CHECK_HASH_ELSE;
	}
}




int Check_IniIntegrity(CString _strIniPath)
{
//	CString strIni = CPathInfo::GetClientInstallPath()+STR_CMSVER_INI;

	UM_WRITE_LOG(_T("Check_IniIntegrity"));

	CString strIniHash = GetIniHashInfo();


	return Check_FileIntegrity(_strIniPath,strIniHash);
}



CString  GetIniHashInfo()
{
	CString strHash = _T("");
	//strQuery.Format(_T("select * from %s"), DB_AGENT_INFO);
	//strHash = GetDbInfo_Config(strQuery,_T("etc_1"));
	return strHash;


}


//CString  GetDbInfo_Config(CString _strQuery,CString _StrField)
//{
//
//	CDBConnect* pDB  = new  CTBAgentInfo();
//	CString strQuery = _T(""), strResult = _T("");
//
//	CStringArray arrData;
//	arrData.RemoveAll();
//
//	pDB->QuerySelect((TCHAR*)_strQuery.GetString(), &arrData,(TCHAR*)_StrField.GetString());
//
//	if( arrData.GetSize() > 0 )
//	{
//		strResult = arrData.GetAt(0);
//	}
//
//	SE_MemoryDelete(pDB);
//
//
//	return strResult;
//
//
//}
//
//
//int Check_FileIntegrity_Enc(CString _strTargetPath,CString _strEncHash)
//{
//
//	CString strHash = MakeDecodeData(_strEncHash);
//
//	if(strHash.GetLength()<1)
//		OFS_CHECK_DECODE_HASH;
//
//	return Check_FileIntegrity(_strTargetPath,_strEncHash);
//
//}


//
//int Check_FileIntegrity_All(CStringArray& _saList , CString _strTargetPath,CString _strIniPath, CString _strSection,BOOL _bSubPath)
//{
//
///*
//	strTargetPath 경로 폴더확인 
//	1. INI 파일 존재 확인 
//	2. INI의 파일 해쉬 구하기 
//	3. 
//*/
//
//	
//	if(FALSE ==	PathFileExists(_strTargetPath))
//		return OFS_CHECK_TARGET_PATH;
//
//
//	if(FALSE ==	PathFileExists(_strIniPath))
//		return OFS_CHECK_INI_FILE;
//
//	CString	strKeyName, strValue, sLog;
//	CStringList listApp_AllKeys;
//
//	CIniFileEx	CmsVerIni(_strIniPath);		
//	CmsVerIni.ReadSection(_strSection, listApp_AllKeys);
//	int nCnt = listApp_AllKeys.GetCount();
//
//	if(nCnt<1)
//		return OFS_CHECK_INI_SECTION;
//
//	POSITION pos = listApp_AllKeys.GetHeadPosition();
//	
//	while(pos != NULL){
//		
//		strKeyName = listApp_AllKeys.GetNext(pos);
//		if( !strKeyName.IsEmpty() ){
//
//			strValue = CmsVerIni.ReadString(_strSection, strKeyName, _T("") );
//			if( strValue.IsEmpty()){
//				return OFS_CHECK_INI_VALUE;
//			}
//
//			CString strTarget = _strTargetPath+strKeyName;
//
//			if(OFS_CHECK_SUCCESS != Check_FileIntegrity_Enc(strTarget,strValue )){
//				_saList.Add(strKeyName);			
//			}
//
//		}//if( !strKeyName.IsEmpty() ){		
//	}//while(pos != NULL){
//
//
//
//
//	return OFS_CHECK_SUCCESS;
//
//}


int Check_File_CodeSign(CString _strTargetPath)
{

	//1. 파일존재여부 확인
	if(FALSE ==	PathFileExists(_strTargetPath))
		return OFS_CHECK_FILE_EXIST;

	//2. PE파일 여부 판단
	if(FALSE ==	IsPeFormat(_strTargetPath))
		return OFS_CHECK_PE_FILE;
	
	//3. 코드사인 확인 
	if(FALSE ==	Check_CodeSign(_strTargetPath))
		return OFS_CHECK_CODESIGN;


	return OFS_CHECK_SUCCESS;
}



CString GetFileHash_MD5(CString _strFilePath)
{


	CFile			cfile;
	CMD5			md5;	
	BYTE			caHash[ 16] = { 0,};
	char			szMD5Val[40]={0,};
	LPWSTR			pRet =NULL ;
	CString			strRet = _T(""), sLog;
	
	if( cfile.Open(_strFilePath, CFile::modeRead|CFile::typeBinary) )
	{
		int file_size = (int)cfile.GetLength() ;
		char *szTemp = "TiorSaver";
		BYTE *pTemp = (BYTE*)szTemp;
		int nTempSize = strlen(szTemp)*sizeof(BYTE);
		//unsigned char* pBuff = new unsigned char[file_size + 1];
		BYTE *pBuff = new BYTE[file_size + nTempSize + 1];
		//BYTE *pResult = new BYTE[file_size + strTemp.GetLength() + 1];
		memset(pBuff, 0, file_size +1+nTempSize) ;
		cfile.Read(pBuff, file_size );
		cfile.Close();
		//ustrcat(pBuff, pTemp);
		memcpy(pBuff+file_size, pTemp, nTempSize);
		md5.MD5Update((unsigned char*)pBuff, file_size + nTempSize);
		md5.MD5Final(caHash);

		for(int i = 0; i < sizeof(caHash) ; i++)
			sprintf(szMD5Val + (i * 2), "%02x", caHash[i]);

		delete pBuff;

		pRet = AnsiCodeToUniCode(szMD5Val);
		strRet.Format( _T("%s"),  pRet);
		if(pRet)
			delete pRet;
	}
	else{
		sLog.Format( _T("[FileIntegrity] IsSameMD5 Error: File Open fail (name: %s, %d"),  _strFilePath, GetLastError() );
		UM_WRITE_LOG(sLog);
	}

	return strRet;



}

//
//
//CString MakeDecodeData(CString _strKeyData)
//{
//	CMD5 md5;
//	md5.Decode(_strKeyData);
//
//}


//
//int CheckIniFileInfo(FILE_LIST& mapList, CString strIniPath, CString strSection)
//{
//
//
//
//}
//


BOOL Check_CodeSign(LPCWSTR pwszSourceFile)
{
	LONG lStatus;
    //DWORD dwLastError;

    // Initialize the WINTRUST_FILE_INFO structure.

    WINTRUST_FILE_INFO FileData;
    memset(&FileData, 0, sizeof(FileData));
    FileData.cbStruct = sizeof(WINTRUST_FILE_INFO);
    FileData.pcwszFilePath = pwszSourceFile;
    FileData.hFile = NULL;
    FileData.pgKnownSubject = NULL;

    /*
    WVTPolicyGUID specifies the policy to apply on the file
    WINTRUST_ACTION_GENERIC_VERIFY_V2 policy checks:
   
    1) The certificate used to sign the file chains up to a root
    certificate located in the trusted root certificate store. This
    implies that the identity of the publisher has been verified by
    a certification authority.
   
    2) In cases where user interface is displayed (which this example
    does not do), WinVerifyTrust will check for whether the 
    end entity certificate is stored in the trusted publisher store, 
    implying that the user trusts content from this publisher.
   
    3) The end entity certificate has sufficient permission to sign
    code, as indicated by the presence of a code signing EKU or no
    EKU.
    */

    GUID WVTPolicyGUID = WINTRUST_ACTION_GENERIC_VERIFY_V2;
    WINTRUST_DATA WinTrustData;

    // Initialize the WinVerifyTrust input data structure.

    // Default all fields to 0.
    memset(&WinTrustData, 0, sizeof(WinTrustData));

    WinTrustData.cbStruct = sizeof(WinTrustData);
   
    // Use default code signing EKU.
    WinTrustData.pPolicyCallbackData = NULL;

    // No data to pass to SIP.
    WinTrustData.pSIPClientData = NULL;

    // Disable WVT UI.
    WinTrustData.dwUIChoice = WTD_UI_NONE;

    // No revocation checking.
    WinTrustData.fdwRevocationChecks = WTD_REVOKE_NONE;

    // Verify an embedded signature on a file.
    WinTrustData.dwUnionChoice = WTD_CHOICE_FILE;

    // Default verification.
    WinTrustData.dwStateAction = 0;

    // Not applicable for default verification of embedded signature.
    WinTrustData.hWVTStateData = NULL;

    // Not used.
    WinTrustData.pwszURLReference = NULL;

    // This is not applicable if there is no UI because it changes
    // the UI to accommodate running applications instead of
    // installing applications.
    WinTrustData.dwUIContext = 0;

    // Set pFile.
    WinTrustData.pFile = &FileData;

    // WinVerifyTrust verifies signatures as specified by the GUID
    // and Wintrust_Data.
    lStatus = WinVerifyTrust(
        NULL,
        &WVTPolicyGUID,
        &WinTrustData);

	if(ERROR_SUCCESS == lStatus)
	    return true;


	return false;
}


/////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

BOOL IsPeFormat(CString _strFilePath)
{

	if(!PathFileExists(_strFilePath))
		return FALSE;


	HANDLE hFile = NULL;
	BOOL bResult = FALSE;



	hFile=CreateFile(_strFilePath,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);	

	if(NULL == hFile)
		return bResult;

	IMAGE_DOS_HEADER stHeader;

	memset(&stHeader, 0, sizeof(IMAGE_DOS_HEADER));

	DWORD read = 1;

	UINT nSize = GetFileSize(hFile,NULL);

	if( nSize < sizeof(IMAGE_DOS_HEADER)+1 ){
		CloseHandle(hFile);
		return bResult;
	}

	if(!ReadFile(hFile,&stHeader,sizeof(IMAGE_DOS_HEADER),&read,NULL) ){
		CloseHandle(hFile);
		return bResult;
	}
	//MZ시그너쳐 확인  
	if(stHeader.e_magic != IMAGE_DOS_SIGNATURE){
		CloseHandle(hFile);
		return bResult;
	}
	//PE시그너쳐 위치 확인
	LONG nTaregPoint = stHeader.e_lfanew;	
	if(nSize<nTaregPoint+sizeof(DWORD)+1){
		CloseHandle(hFile);
		return bResult;
	}
	//PE시그너쳐 위치로 이동 

	//INVALID_SET_FILE_POINTER
	DWORD dwPtr = SetFilePointer(hFile,nTaregPoint,NULL,FILE_BEGIN);
	if(INVALID_SET_FILE_POINTER == dwPtr){
		CloseHandle(hFile);
		return bResult;
	}
	
	DWORD nPE = 0;
	if(!ReadFile(hFile,&nPE,sizeof(DWORD),&read,NULL) ){
		CloseHandle(hFile);
		return bResult;
	}
	//PE시그너쳐 확인
	if(nPE == IMAGE_NT_SIGNATURE)
		bResult = TRUE;
	
	CloseHandle(hFile);	


	return bResult;

}



void RedirectionOnOff(BOOL bFlag)
{
	static PVOID oldValue ;
	
	typedef BOOL (WINAPI * LPWOW64DISABLEWOW64FSREDIRECTION)(PVOID *);
	typedef BOOL (WINAPI * LPWOW64REVERTWOW64FSREDIRECTION)(PVOID *);
	HMODULE  hK32lib;
	LPWOW64DISABLEWOW64FSREDIRECTION lpWow64DisableWow64FsRedirection;
	LPWOW64REVERTWOW64FSREDIRECTION lpWow64RevertWow64FsRedirection;

	hK32lib = ::LoadLibrary( _T("kernel32.dll") );
	lpWow64DisableWow64FsRedirection = NULL;

	if (hK32lib != NULL)
	{
		lpWow64DisableWow64FsRedirection = (LPWOW64DISABLEWOW64FSREDIRECTION)GetProcAddress(hK32lib, "Wow64DisableWow64FsRedirection");
		lpWow64RevertWow64FsRedirection = (LPWOW64REVERTWOW64FSREDIRECTION)GetProcAddress(hK32lib, "Wow64RevertWow64FsRedirection");
	}
	else
		return ;
	
	if(bFlag) //Redirection 켜기
	{
		if (lpWow64RevertWow64FsRedirection != NULL)
			 lpWow64RevertWow64FsRedirection(&oldValue);
	}
	else //끄기
	{
		if (lpWow64DisableWow64FsRedirection != NULL)
			 lpWow64DisableWow64FsRedirection(&oldValue);
	}
}