#include "StdAfx.h"
#include "IntegrityDB.h"


void WriteIntegrityLog(int _nSetNo,int _nMode ,int _nAction,CString _strCode,int _nResult,CString _strTemp,CString _strTime,int _nEnd )
{

	if(0< _strTemp.GetLength())
		UM_WRITE_LOG(_strTemp);

#if _CC
	CDBConnect* pDB  = new  CTBConfig();
	CString strQuery = _T("");

	CString strReport = _strTemp;

	if(2047<strReport.GetLength())
		strReport = _strTemp.Mid(0,2047);


	strQuery.Format(_T("insert into %s(setNo, mode, type, testItemsCode,result,report,startTime,isEnd,time) values(%d,%d,%d,'%s',%d,'%s','%s',%d ,datetime('now','localtime'))"),DB_INTEGRITY_CHECK_LOG, 
		                  _nSetNo,_nMode,_nAction,_strCode,_nResult,strReport,_strTime,_nEnd);

	bool bResult = 	WrapQyery(pDB,(TCHAR*)strQuery.GetString());

	SE_MemoryDelete(pDB);
#endif _CC
}




bool WrapQyery(CDBConnect* _pDB, CString _strQuery, int _nCnt)
{
	if(NULL == _pDB)
		return false;

	if(_strQuery.GetLength()<5)
		return false;

	int nIdx = 0;
	bool bResult = false;

	do{
		 bResult= _pDB->QueryInsert((TCHAR*)_strQuery.GetString());

		 if(true == bResult)
			 break;
		 Sleep(1000);

		nIdx++;
	}while(nIdx < _nCnt);


	return bResult;

}



bool WrapQyery_Insert(CDBConnect* _pDB, CString _strQuery, int _nCnt)
{
	if(NULL == _pDB)
		return false;

	if(_strQuery.GetLength()<5)
		return false;

	int nIdx = 0;
	bool bResult = false;

	do{
		 bResult= _pDB->QueryInsert((TCHAR*)_strQuery.GetString());

		 if(true == bResult)
			 break;
		 Sleep(1000);

		nIdx++;
	}while(nIdx < _nCnt);


	return bResult;

}

bool WrapQyery_Select(CDBConnect* _pDB, CString _strQuery,CStringArray* _arrData,CString _strFild ,int _nCnt )
{

	if(NULL == _pDB)
		return false;

	if(_strQuery.GetLength()<5)
		return false;


	int nIdx = 0;
	bool bResult = false;

	do{
		 bResult= _pDB->QuerySelect(_strQuery.GetBuffer(0),_arrData, _strFild );

		 if(true == bResult)
			 break;
		 Sleep(1000);

		nIdx++;
	}while(nIdx < _nCnt);


	return bResult;


}

bool WrapQyery_Select(CDBConnect* _pDB, CString _strQuery,CStringArray* _arrData,CString _strFild, CString _strFild2, int _nCnt)
{
	
	if(NULL == _pDB)
		return false;

	if(_strQuery.GetLength()<5)
		return false;


	int nIdx = 0;
	bool bResult = false;

	do{
		 bResult= _pDB->QuerySelect(_strQuery.GetBuffer(0),_arrData, _strFild,_strFild2 );

		 if(true == bResult)
			 break;

		 Sleep(1000);

		nIdx++;
	}while(nIdx < _nCnt);


	return bResult;


}


