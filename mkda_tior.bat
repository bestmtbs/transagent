IF "%X64BUILD%" == "" SET X64BUILD=0

SET MKROOTPATH=%CD%
SET X64BUILD=0

REM ####################################################################
SET SLN_PATH=CmsHash
REM ####################################################################

SET MA_SOLUTION_PATH=CmsHash
SET MA_SOLUTION_NAME=CmsHash
SET MA_PROJECT_NAME=OfsHash
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"


REM ####################################################################
SET SLN_PATH=SQLite
REM ####################################################################

SET MA_SOLUTION_PATH=SQLite
SET MA_SOLUTION_NAME=SQLite
SET MA_PROJECT_NAME=SQLite
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

REM ####################################################################
SET SLN_PATH=TiorSaverDownload
REM ####################################################################

SET MA_SOLUTION_PATH=TiorSaverDownload
SET MA_SOLUTION_NAME=TiorSaverDownload
SET MA_PROJECT_NAME=TiorSaverDownload
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

REM ####################################################################
SET SLN_PATH=TiorSaverUpdate
REM ####################################################################

SET MA_SOLUTION_PATH=TiorSaverUpdate
SET MA_SOLUTION_NAME=TiorSaverUpdate
SET MA_PROJECT_NAME=TiorSaverUpdate
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

REM ####################################################################
SET SLN_PATH=TiorSaverService
REM ####################################################################

SET MA_SOLUTION_PATH=TiorSaverService
SET MA_SOLUTION_NAME=TiorSaverService
SET MA_PROJECT_NAME=TiorSaverService
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

REM ####################################################################
SET SLN_PATH=TiorSaverDummyProcess
REM ####################################################################

SET MA_SOLUTION_PATH=TiorSaverDummyProcess
SET MA_SOLUTION_NAME=TiorSaverDummyProcess
SET MA_PROJECT_NAME=TiorSaverDummy
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

REM ####################################################################
SET SLN_PATH=TiorSaverTransfer
REM ####################################################################

SET MA_SOLUTION_PATH=TiorSaverTransfer
SET MA_SOLUTION_NAME=TiorSaverTransfer
SET MA_PROJECT_NAME=TiorSaverTransfer
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

call %CD%\dsntech_TiorSaver.bat

REM ####################################################################
SET SLN_PATH=TiorSaverTray
REM ####################################################################

SET MA_SOLUTION_PATH=TiorSaverTray
SET MA_SOLUTION_NAME=TiorSaverTray
SET MA_PROJECT_NAME=TiorSaverTray
SET MA_CONFIG_NAME=Release
call "%MAROOTPATH%\mk.bat"

call %CD%\dsntech_TiorSaver.bat