// CommonClass.h: interface for the CCommonClass class.
//
//////////////////////////////////////////////////////////////////////
/***********************Class Product*******************************
1. Class Name		: CommonClass
2. Author			: Jung Min
3. Last Update		: 2002.11.21
*********************************************************************/
#if !defined(AFX_COMMONCLASS_H__9494DCD4_1BE9_446C_8B24_D2379D71725D__INCLUDED_)
#define AFX_COMMONCLASS_H__9494DCD4_1BE9_446C_8B24_D2379D71725D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#pragma comment(lib, "winmm.lib")				// For CPU Clock
#pragma comment(lib, "Iphlpapi.lib")			// For IP Address, MAC Address
#pragma comment(lib, "version.lib")				// For File Version
#pragma comment(lib, "Netapi32.lib")			// For NetBios

#include <Nb30.h>
#include "tlhelp32.h"

#include <winsock2.h>
#include <afxsock.h>
/************NetBios Struct**************/
typedef struct _ASTAT_
{
	ADAPTER_STATUS adapt;
	NAME_BUFFER    NameBuff [30];
}ASTAT, * PASTAT;
/***********ShortCut Struct***************/
struct SHORTCUTSTRUCT
{
	LPTSTR pszTarget;
	LPTSTR pszDesc;
	WORD wHotKey;
	LPTSTR pszIconPath;
	WORD wIconIndex;
};

/* add start by wsjang 20071205 */
typedef struct secuusb_policy{
 	int			nDevPassUse;
	int			nExtPassView;
} SECUUSB_POLICY;
/* add end by wsjang 20071205 */





typedef BOOL (WINAPI *fpConvertStringSidToSid)(LPCTSTR, PSID*);


////////////////////////////////////////////////////////////////////////
//                                                                    //
//                          User and Group related SID attributes     //
//                                                                    //
////////////////////////////////////////////////////////////////////////

//
// Group attributes
//

#define SE_GROUP_MANDATORY                 (0x00000001L)
#define SE_GROUP_ENABLED_BY_DEFAULT        (0x00000002L)
#define SE_GROUP_ENABLED                   (0x00000004L)
#define SE_GROUP_OWNER                     (0x00000008L)
#define SE_GROUP_USE_FOR_DENY_ONLY         (0x00000010L)
#define SE_GROUP_INTEGRITY                 (0x00000020L)
#define SE_GROUP_INTEGRITY_ENABLED         (0x00000040L)
#define SE_GROUP_LOGON_ID                  (0xC0000000L)
#define SE_GROUP_RESOURCE                  (0x20000000L)

#define SE_GROUP_VALID_ATTRIBUTES          (SE_GROUP_MANDATORY          | \
                                            SE_GROUP_ENABLED_BY_DEFAULT | \
                                            SE_GROUP_ENABLED            | \
                                            SE_GROUP_OWNER              | \
                                            SE_GROUP_USE_FOR_DENY_ONLY  | \
                                            SE_GROUP_LOGON_ID           | \
                                            SE_GROUP_RESOURCE           | \
                                            SE_GROUP_INTEGRITY          | \
                                            SE_GROUP_INTEGRITY_ENABLED)



#define MESSAGE_WINDOWS_DIR			"%Windows%"
#define MESSAGE_SYSTEM_DIR			"%System%"
#define MESSAGE_PROGRAM_DIR			"%Program%"
#define MESSAGE_TEMP_DIR			"%Temp%"
#define SERVER_UPLOAD_PATH			"c:\\temp\\"
#define MESSAGE_APPDATA_DIR			"%APPDATA%"
#define MESSAGE_ROOT_DIR			"%ROOT%"



class CCommonClass  
{
public:
	CCommonClass();
	virtual ~CCommonClass();

public:

	int ParserString( CString target, CStringArray * pData, CString parser );

	// server 옵션화.
	BOOL IsSafePCOption( CString strOption );

	BOOL SetRegMultiCharValue(HKEY hRootKey, LPTSTR pszKey, LPTSTR pszValue,CStringArray &array);
	BOOL GetRegMultiCharValue(HKEY hkey, LPTSTR pszKey, LPTSTR pszValue,CStringArray &array);

	static BOOL SetRegistryIntValueUser(LPTSTR pszKey, LPTSTR pszValue, DWORD dwInput);
	static BOOL SetRegistryValueDefaultUser(LPTSTR pszKey, LPTSTR pszValue, LPTSTR pszInput);
	BOOL GetRealIPMAC(CString& strIPAddress, CString& strMacAddress);
	BOOL CCommonClass::GetRealIPMAC(CString &strIPAddress, CString &strMacAddress,CString strTargetIP);
	static int GetUserListFromReg(CStringArray &arrUser);
	BOOL DelRegistryValue(LPTSTR pszKey, LPTSTR pszValue);
	void	SetFilePath(CString &pFilePath);
	BOOL	RemoveFile(CString strDestFile);
	BOOL	ExecuteFile(CString strDestFile);
	BOOL	SaveFile(CString strDestFile, LPBYTE pbyBuffer, DWORD dwBufferSize);
	CString GetFileVersion(CString strFilePath);
	static DWORD FindProcessID(CString ProcessName);	// shmoon. 2009.10.13. 프로세스 이름으로 PID구하기
	
	/************************File Function*******************************/
	BOOL	CreateFileEx (WORD iResource, TCHAR *pszPath);
	static	CString GetFileName(CString& strFilePath);
	CString GetDirectory(CString& strFilePath);
	BOOL	MakeFile(CString& strFilePath, LPTSTR pszBuffer = NULL, CStringArray* lpstrArray = NULL);
	CString GetWindowsPath();
	CString GetSystemPath();
	CString GetProgramFilePath();
	CString GetTemporaryPath();
	CString GetRootPath();

	BOOL SetRegistryValueUser(LPTSTR pszKey, LPTSTR pszValue, LPTSTR pszInput);

	/************************Process Function****************************/
	DWORD	CreateProcessEx(CString& strCommand, USHORT iShowMode = SW_SHOW, BOOL bWait = FALSE);
	DWORD	CreateProcessEx(LPTSTR pszCommand, USHORT iShowMode = SW_SHOW, BOOL bWait = FALSE);
	DWORD	CreateProcessEx(CString strCommand, USHORT iShowMode, BOOL bWait,CString strDesktop);
	BOOL	CreateProcessEx(CString &strCommand, USHORT iShowMode,PROCESS_INFORMATION *pi);
	BOOL	TerminateProgram(CString& strProgramPath, BOOL bWait = FALSE);
	BOOL	CreateProcessIL(TCHAR *pFilePath, BOOL bSessionID, DWORD dwInSessionID, DWORD dwILevel, DWORD dwWait = 0);
	static BOOL KillingProcess(CString strProc);
	/************************Network Function****************************/
	void	SetDefaultMacAddress(CString& strMacAddress);
	void	SetDefaultIPAddress(CString& strIPAddress);
	CString GetMacAddress(CStringArray *pstrArray = NULL);
	CString GetLocalIPAddress(CStringArray *pstrArray = NULL);
	BOOL	IsValidIP(CString& strIP);
	CString GetLogonId();
	//kw 20090216 우리금융 설치유도 관련 함수
	void GetAllIPList(CStringArray &array);
	//kw end
	/***************Registry Function****************************************/
	static BOOL		SetRegistryValue(LPTSTR pszKey, LPTSTR pszValue, LPTSTR pszInput);
	static BOOL		SetRegistryValue(LPTSTR pszKey, LPTSTR pszValue, DWORD dwInput);
	CString GetRegistryCharValue(LPTSTR pszKey, LPTSTR pszValue, HKEY hRootKey = HKEY_LOCAL_MACHINE);
	static DWORD	GetRegistryNumericValue(LPTSTR pszKey, LPTSTR pszValue, HKEY hRootKey = HKEY_LOCAL_MACHINE);
	BOOL SetRegistryValueDWORD(HKEY hkey,LPTSTR pszkey,LPTSTR pszValue,DWORD dwValue);
	/***************Shell Function*******************************************/
   
private:
	char * strtok_r(char *s1, const char *s2, char **lasts);
	HRESULT SHCreateShortcutEx(LPCTSTR szLnkFile, SHORTCUTSTRUCT *lpss);
};

#endif // !defined(AFX_COMMONCLASS_H__9494DCD4_1BE9_446C_8B24_D2379D71725D__INCLUDED_)
