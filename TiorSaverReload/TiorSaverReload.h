
// TiorSaverReload.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"		// 주 기호입니다.
#include "Impersonator.h"
#include "UtilsProcess.h"
//#include "Pipe/C_TransAgent_S_Reload/ATORPipeServer.h"
//#include "pipe/C_Tray_S_Reload/TTORPipeServer.h"
// CTiorSaverReloadApp:
// 이 클래스의 구현에 대해서는 TiorSaverReload.cpp을 참조하십시오.
//
//typedef DWORD (WINAPI *fpWTSGetActiveConsoleSessionId)();
//typedef HANDLE (WINAPI *fpCreateToolhelp32Snapshot) (DWORD, DWORD);
//typedef BOOL (WINAPI *fpProcess32First) (HANDLE, LPPROCESSENTRY32);
//typedef BOOL (WINAPI *fpProcess32Next) (HANDLE, LPPROCESSENTRY32);

class CTiorSaverReloadApp : public CWinAppEx
{
public:
	CTiorSaverReloadApp();
	virtual ~CTiorSaverReloadApp();

	//static UINT  PipeServerThread(LPVOID lpParam);
	//CTTORPipeServer*		m_pPipeServer;
	//void	UninstallStart(CString _strUnkey=L"");
	//void	StartUserClearModule();
	CProcess m_Process;
	BOOL				m_bStopFlag;
	//HANDLE	m_hATORPipeServer;
	//CATORPipeServer* m_pATORPipeServer;
	//BOOL m_bThreadATORTPipeServerExit;

private:	
	BOOL	m_UseFilter;
	BOOL	m_bSkipLoadFilefilter;
	BOOL	m_bIs64bit;

	HMODULE m_hMod;
	//fpWTSGetActiveConsoleSessionId m_WTSGetActiveConsoleSessionId;
	//fpCreateToolhelp32Snapshot m_pCreateToolhelp32Snapshot ;
	//fpProcess32First   m_pProcess32First ;
	//fpProcess32Next    m_pProcess32Next ;

	void Execute();
	//void CheckTrayProcess(CString _strPath, CString _strMutexName);
	//void CheckProcMgrProcess(CString strProcMgrNamePerBit, CString strMutexName); // 2016-10-13 sy.choi OfsProcMgr을 실행할 더미 실행
	//int IsGoodSessionID(DWORD dwSessionID, CString _strCompareProcessName);
	//BOOL IsProcessExist(CString _strCompareProcessName);	// 2017-05-08 kh.choi 프로세스 존재유무 체크
	//BOOL StartProcessAsUser(DWORD pid, CString strExePath, CString _strParam=L"");
	//DWORD GetRemoteCurrentSessionId();
	//BOOL LaunchAppIntoDifferentSession(DWORD _dwPid, CString _strPath, DWORD _dwSessionID);
	void	Set_CurrentUserSession(DWORD _dwSessionID);
	void	ServiceCheck();
	//BOOL	IsWindowLogin();		//윈도우에 로그인하여 explorer.exe가 생성되었는지
	BOOL	StopAT_Suspend();			//지정된 레지스트리 파일에 값이 존재하면 ResumeThread 처리르 하지 않도록 한다.
	//void	CheckNewDrvCtrl();			//새로 추가된 드라이버 필터 연동모듈을 구동할것인지 확인
	//void CheckFilefilterLoadSkip(void);	// 2015-12-30 sy.choi 레지스트리를 체크하여 m_bSkipLoadFilefilter 값을 설정한다.
	//VOID UpdateLicenseKey(VOID);		// 2017-05-18 kh.choi license.cms 파일의 key 를 ltb_config 테이블 etc_3 에 업데이트한다. license.cms 파일에서 읽어오기에 실패하는 경우가 간간히 있음
// 재정의입니다.
public:
	virtual BOOL InitInstance();
	BOOL KillTiorProcess(CString _strFIlePath);
	BOOL ChangeFileName(CString _strChangeFileName);
	BOOL SetPrivilege(HANDLE hToken, LPCTSTR Privilege, BOOL bEnablePrivilege);
	BOOL DeleteService();
	BOOL CheckVaildServiceName(const CString &_sServiceName);
	BOOL ExistsService(const CString &_sSvcName);
	friend BOOL WINAPI StartATORThread(LPVOID lpParam);
	
// 구현입니다.

	DECLARE_MESSAGE_MAP()
	virtual int ExitInstance();
};

extern CTiorSaverReloadApp theApp;