
// TiorSaverReload.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.


#include "stdafx.h"
#include "TiorSaverReload.h"
#include "TiorSaverReloadDlg.h"

//#include "CommonDefine.h"
#include "UtilsProcess.h"
#include "WinOsVersion.h"
//#include "WTSSession.h"
#include "PathInfo.h"
//#include "UtilsProcess.h"
//#include "CommonDefine.h"
//#include "pipe.h"
//#include "C_Agent_S_Tray/ATOTPipeClient.h"
#include "UtilsFile.h"
#include "GeneralUtil.h"
#include "UtilsReg.h"

//#include <Userenv.h>
//#include <Wtsapi32.h>
#include <winsvc.h>
#include "yvals.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma  comment(lib, "Wtsapi32.lib")

// CTiorSaverReloadApp

BEGIN_MESSAGE_MAP(CTiorSaverReloadApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTiorSaverReloadApp 생성

CTiorSaverReloadApp::CTiorSaverReloadApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	//m_WTSGetActiveConsoleSessionId = NULL;
	//m_pCreateToolhelp32Snapshot = NULL;
	//m_pProcess32First = NULL;
	//m_pProcess32Next =NULL;
	m_hMod = NULL;
	//m_pPipeServer = NULL;
	m_bStopFlag = FALSE;
	m_bSkipLoadFilefilter = FALSE;
	m_bIs64bit = FALSE;
}

/**
 @brief     소멸자
 @author    kh.choi
 @date      2017-05-17
*/
CTiorSaverReloadApp::~CTiorSaverReloadApp(void)
{
	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
	//SingletonForEzLog::DeleteSingleton();
	//CloseLogFile();
}


// 유일한 CTiorSaverReloadApp 개체입니다.

CTiorSaverReloadApp theApp;


// CTiorSaverReloadApp 초기화

BOOL CTiorSaverReloadApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	SetRegistryKey(_T("로컬 응용 프로그램 마법사에서 생성된 응용 프로그램"));
	CWinOsVersion osVer;
	m_bIs64bit = osVer.Is64bit();

	// 	//뮤텍스 생성
	if( false == CProcess::ProcessCreateMutex(WTIOR_RELOAD_AGENT_MUTEX_NAME))
	{
		{
			CString strLog = _T("");
			strLog.Format(_T("[TiorSaverReload] TiorSaverReload_RUNNING_NOW mutex create failed."));	// 2017-05-25 kh.choi
			UM_WRITE_LOG(strLog);
		}
		exit(0);
	}

	g_cDbgLog.SetLogFileName(L"TiorSaverReload.log");
	g_cDbgLog.InitLog();
	CString strLog;
	 m_hMod = LoadLibrary(_T("kernel32.dll"));
	if (m_hMod != NULL)
	{
		m_Process.m_pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(m_hMod, "CreateToolhelp32Snapshot");
		m_Process.m_pProcess32First = (fpProcess32First) ::GetProcAddress(m_hMod, PROCESS32FIRST);
		m_Process.m_pProcess32Next  = (fpProcess32Next) ::GetProcAddress(m_hMod, PROCESS32NEXT);		
	}

	BOOL bFirstStartFlag= TRUE;
	while(TRUE)
	{
		CString strUninstallMutexName = _T("");	// 2017-04-28 sy.choi OfsUninstall실행시에는 Reload가 프로세스들을 다시 실행하지 않도록 수정
		strUninstallMutexName.Format(_T("%s"), WTIOR_UNINSTALL_AGENT_MUTEX_NAME);
		CString strUninstallReadyFile = _T("");
		strUninstallReadyFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("uninstall.ready"));
		if (FileExists(strUninstallReadyFile)){
			CString strUninstall = _T("");
			strUninstall.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_UNINSTALL_AGENT_NAME);
			if(false == CProcess::IsExistMutex(strUninstallMutexName) ) {
				//m_Process.CheckTiorProcess(strUninstall, strUninstallMutexName);
				//WinExec((LPCSTR)(LPCTSTR)strUninstall, SW_HIDE);
				CImpersonator imp;
				imp.CreateProcessEx(strUninstall);
				imp.Free();
			}
		}
		if(m_bStopFlag)
			break;
		UM_WRITE_LOG(_T("[OfsRelaod] Checking.."));
		if (!FileExists(strUninstallReadyFile))
		//CString strUninstallMutexName = _T("");	// 2017-04-28 sy.choi OfsUninstall실행시에는 Reload가 프로세스들을 다시 실행하지 않도록 수정
		//strUninstallMutexName.Format(_T("%s"), WTIOR_UNINSTALL_AGENT_MUTEX_NAME);
		//if(false == CProcess::IsExistMutex(strUninstallMutexName) )
		{
			Execute();
		}
		//else
		//{
		//	CString strLog = _T("");
		//	strLog.Format(_T("[TiorSaverReload] IsExistMutex(UNINSTALL_MUTEX_NAME) return TRUE. [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		//	UM_WRITE_LOG(strLog);
		//}
		Sleep(2000);
	}
	return FALSE;
}
/**
@brief      윈도우에 로그인하여 explorer.exe 생겨난 상태인지 체크
@author		hhh
@date		2013.06.11
*/
//BOOL CTiorSaverReloadApp::IsWindowLogin()
//{
//	DWORD dwCSID;
//	dwCSID = GetRemoteCurrentSessionId();
//
//	if(dwCSID == -1)	//원격이 아닐때
//		dwCSID = WTSGetActiveConsoleSessionId();		//현재 활성화된 세션ID체크
//		
//		
//	//strLog.Format(_T("[TiorSaverReload] ActiveconsoleSession Id: %d"), dwCSID);
//	//UM_WRITE_LOG(strLog);
//	if(dwCSID == -1)
//	{
//		return FALSE;
//	}
//		
//	DWORD dwProcessID = m_Process.IsGoodSessionID(dwCSID, EXPLORER_PROC_NAME);	//인자로 들어가는 dwCSID는 현재 Actice된 세션ID	
//	if( dwProcessID > 0)
//	{
//		return TRUE;
//	}
//	
//	return FALSE;
//}

void CTiorSaverReloadApp::Execute()
{
	CString strSysAgent = _T("");
	CString strSysAgentMutex = _T("");
	CString strUsrAgent = _T("");
	CString strUsrAgentMutex = _T("");
	CString strTransAgentPath = _T("");
	CString strTransAgentMutex = _T("");

	CImpersonator imp;

	if(m_bStopFlag)
		return;

	static BOOL bFirst = TRUE;
	static BOOL bFirstUpgrade = TRUE;

	//나머지 프로세스들은 윈도우에 로그인하면 구동시키도록한다.
	if( m_Process.IsWindowLogin() )
	{
		CString strCheckFirstUser = _T("");
		strCheckFirstUser.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("register.success"));
		if (!FileExists(strCheckFirstUser)) {
			CString strTrayName = _T("");
			CString strTrayMutexName = _T("");
			strTrayName = CPathInfo::GetClientInstallPath() + WTIOR_TRAY_NAME;
			strTrayMutexName.Format(_T("%s"), WTIOR_TRAY_MUTEX_NAME);
			m_Process.CheckTiorProcess(strTrayName, strTrayMutexName);
		}
		else {
			CString strTrayMutex = _T("");

			if (m_bIs64bit) {
				strUsrAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_USR_AGENT_64_NAME);
				strUsrAgentMutex.Format(_T("%s"), WTIOR_USR_AGENT_64_MUTEX_NAME);
			} else {
				strUsrAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_USR_AGENT_32_NAME);
				strUsrAgentMutex.Format(_T("%s"), WTIOR_USR_AGENT_32_MUTEX_NAME);
			}
			m_Process.CheckTiorProcess(strUsrAgent, strUsrAgentMutex);  //hhh: test
			if(m_bStopFlag)
				return;

			strTransAgentPath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_TRANS_AGENT_NAME);
			strTransAgentMutex.Format(_T("%s"), WTIOR_TRANS_AGENT_MUTEX_NAME);

			if( false == CProcess::IsExistMutex(strTransAgentMutex) )	{
				imp.CreateProcessEx(strTransAgentPath);
				Sleep(500);
			}

			if(m_bStopFlag)
				return;

			if( bFirst )
			{
				bFirst =FALSE; 
				Sleep(10000);
			}

			if (m_bIs64bit) {
				strSysAgentMutex.Format(_T("%s"), WTIOR_SYS_AGENT_64_MUTEX_NAME);
				strSysAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_SYS_AGENT_64_NAME);    
			} else {
				strSysAgentMutex.Format(_T("%s"), WTIOR_SYS_AGENT_32_MUTEX_NAME);
				strSysAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_SYS_AGENT_32_NAME);    
			}

			if( false == CProcess::IsExistMutex(strSysAgentMutex) )	{
				imp.CreateProcessEx(strSysAgent);
				Sleep(500);
			}

			if(m_bStopFlag)
				return;

			if(bFirstUpgrade)
			{
				CString strUpdate = _T("");
				CString strUpdateMutex = _T("");
				strUpdate.Format(L"%s%s -a", CPathInfo::GetClientInstallPath(), WTIOR_UPDATE_AGENT_NAME);
				strUpdateMutex.Format(_T("%s"), WTIOR_UPDATE_AGENT_MUTEX_NAME);

				if( false == CProcess::IsExistMutex(strUpdateMutex) )	{
					BOOL bExecute = imp.CreateProcessEx(strUpdate);
					if(  bExecute )			
						bFirstUpgrade = FALSE;		
				}	
			}
		}
	}

	if(m_bStopFlag)
		return;
	
	ServiceCheck();
	imp.Free();
}


//활성화된 모든 세션을 체크하고 각세션에 tray가 구동중인지 체크
//void CTiorSaverReloadApp::CheckTrayProcess(CString _strPath, CString _strMutexName)
//{
//	CString strTrayMutex = _T("");
//
//	PWTS_SESSION_INFO ppSessionInfo = NULL;
//	DWORD     pCount = 0, dwRetSessionID = -1;
//	WTS_SESSION_INFO  wts;
//	CString strStationName, strLog;
//	WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0, 1, &ppSessionInfo, &pCount );
//
//	for( DWORD i = 0; i < pCount; i++ )
//	{
//
//		wts = ppSessionInfo[i];
//		LPTSTR  ppBuffer        = NULL;
//		DWORD   pBytesReturned  = 0;
//		if(wts.SessionId == RDP_LISTENING_SESSION)			//해당 세션은 리모드 리스닝 세션이므로 무시.
//			continue;
//
//		//해당세션 id를 가진 explorer.exe가 존재하는지 체크,
//		DWORD dwProcessID = IsGoodSessionID(wts.SessionId, EXPLORER_PROC_NAME);
//		
//		//해당세션에 explorer.exe가 돌고있음
//		if(dwProcessID != -1)
//		{
//			strTrayMutex.Format(_T("%s_%d"), _strMutexName, wts.SessionId);
//			//Process가 작동중이 아니라면
//			if(false == CProcess::IsExistMutex(strTrayMutex) )	// 2017-05-08 kh.choi 윈도우 업데이트 후 모조리 false 로 리턴. 해서 프로세스 명을 확인하도록 아래로 바꿈 -> 다시 뮤텍스 사용
//			{
//				//인자로 들어가는 프로세스의 권한으로 프로세를 실행시킨다.
//				if( StartProcessAsUser(dwProcessID, _strPath) )	//test
//				{
//					UM_WRITE_LOG(_T("[TiorSaverReload] Process Start Success : ")+ _strPath );
//				}					
//				else
//					UM_WRITE_LOG(_T("[TiorSaverReload] Process Start Fail : ")+ _strPath );
//			}
//		}
//		
//	}
//}

int CTiorSaverReloadApp::ExitInstance()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if(m_hMod)
		FreeLibrary(m_hMod);
	return CWinAppEx::ExitInstance();
}

//인자로 들어온 세션ID를 가진 _strCompareProcessName가 존재하는지 체크 
//존재 한다면 그 pid를 리턴 
//int CTiorSaverReloadApp::IsGoodSessionID(DWORD dwSessionID, CString _strCompareProcessName)
//{
//
//	BOOL bGet = FALSE;  
//	
//	HANDLE hSnapshot;  
//	PROCESSENTRY32 ppe;     
//	CWTSSession cWtsession;
//	hSnapshot = m_pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
//	ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  
//
//	CString strProcessName =_T(""), strLog;
//	DWORD dwGetSessionID, dwRetPid = -1;
//
//	bGet = m_pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  
//
//	while (bGet)  
//	{  
//		strProcessName.Format(_T("%s"), ppe.szExeFile);
//		strProcessName.MakeLower();
//		
//		//세션을 확인할 프로세스 이름 발견시
//		if( strProcessName ==_strCompareProcessName ) 
//		{	
//			dwGetSessionID = cWtsession.GetProcessSessionId(ppe.th32ProcessID);
//			strLog.Format(_T("[TiorSaverReload] Pid:%d(SeesionID: %d) - _InSessionID:%d"), ppe.th32ProcessID, dwGetSessionID, dwSessionID);
//			UM_WRITE_LOG(strLog);
//			if(dwGetSessionID == dwSessionID)
//			{
//				dwRetPid = ppe.th32ProcessID;
//				break;
//			}
//		}
//	
//		bGet = m_pProcess32Next(hSnapshot, &ppe);  
//	}  
//
//	CloseHandle(hSnapshot);  
//
//	return dwRetPid;  
//}

// 2017-05-08 kh.choi
// 전체 세션에서 해당 프로세스가 있는지를 체크하여 있으면 TRUE, 없으면 FALSE 를 리턴
// int CTiorSaverReloadApp::IsGoodSessionID 와 BOOL CImpersonator::GetProcessExist 함수를 참조
//BOOL CTiorSaverReloadApp::IsProcessExist(CString _strCompareProcessName)
//{
//	BOOL bGet = FALSE;
//	BOOL bReturn = FALSE;
//
//	HANDLE hSnapshot;  
//	PROCESSENTRY32 ppe;     
//
//	hSnapshot = m_pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다
//	if (INVALID_HANDLE_VALUE == hSnapshot)
//	{
//		//	DBGLOG(_T("GetProcessExist - Error 1"));
//		{
//			CString strLog = _T("");
//			strLog.Format(_T("[TiorSaverReload] [ERROR] hSnapshot is INVALID_HANDLE_VALUE!! in IsProcessExist. _strCompareProcessName: %s, bReturn: %d [line: %d, function: %s, file: %s]"), _strCompareProcessName, bReturn, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-25 kh.choi 에러 로그 추가
//			UM_WRITE_LOG(strLog);
//		}
//		return FALSE;
//	}
//	ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  
//
//	CString strProcessName =_T(""), strLog;
//	//DWORD dwGetSessionID;
//	DWORD dwRetPid = -1;
//
//	bGet = m_pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.
//
//	while (bGet)  
//	{
//		strProcessName.Format(_T("%s"), ppe.szExeFile);
//		strProcessName.MakeLower();
//
//		//세션을 확인할 프로세스 이름 발견시
//		if (0 == strProcessName.CompareNoCase(_strCompareProcessName)) {
//			bReturn = TRUE;
//			break;
//		}
//
//		bGet = m_pProcess32Next(hSnapshot, &ppe);  
//	}  
//
//	CloseHandle(hSnapshot);  
//
//	return bReturn;  
//}

/**
@brief      Process 실행
@author    hhh
@param	pid : 해당토큰을 가져올 프로세스 ID
@param	strExePath : 실행할 exe경로
@param	_strParam : 실행시 인자값
@date		 2013.05.06
*/
//BOOL CTiorSaverReloadApp::StartProcessAsUser(DWORD pid, CString strExePath, CString _strParam)
//{
//	CString strLog;
//	HANDLE hd = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
//	if(hd == NULL)
//		return FALSE;
//
//	HANDLE token = NULL;
//	if(OpenProcessToken(hd, TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE, &token) == FALSE)
//	{
//		strLog.Format(_T("[TiorSaverReload] OpenProcessToken Error - %d"), GetLastError() );
//		UM_WRITE_LOG(strLog);
//		CloseHandle(hd);
//		return FALSE;
//	}
//
//	HANDLE newtoken = NULL;
//	if(::DuplicateTokenEx(token, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &newtoken) == FALSE)
//	{
//		strLog.Format(_T("[TiorSaverReload] DuplicateTokenEx Error - %d"), GetLastError() );
//		UM_WRITE_LOG(strLog);
//		CloseHandle(hd);
//		CloseHandle(token);
//		return FALSE;
//	}
//
//	CloseHandle(token);
//
//	void* EnvBlock = NULL;
//	CreateEnvironmentBlock(&EnvBlock, newtoken, FALSE);
//
//	STARTUPINFO si = {0};
//	PROCESS_INFORMATION pi = {0};
//
//	if(::CreateProcessAsUser(newtoken,
//		strExePath,
//		_strParam.GetBuffer(0),
//		NULL,
//		NULL,
//		FALSE,
//		NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT |
//		CREATE_NEW_CONSOLE  | CREATE_SEPARATE_WOW_VDM |
//		CREATE_NEW_PROCESS_GROUP,
//		EnvBlock,
//		NULL,
//		&si,
//		&pi) == TRUE)
//	{
//	
//		CloseHandle(hd);
//		CloseHandle(newtoken);
//		return TRUE;
//	}
//
//	strLog.Format(_T("[ofsMgr] CreateProcessAsUser %s Error - %d"), strExePath, GetLastError() );
//	UM_WRITE_LOG(strLog);
//
//	CloseHandle(hd);
//	CloseHandle(newtoken);
//	return FALSE;
//
//}

/**
@brief      원격세션일때 원격세션이름을 가진 explorer.exe의 세션 ID를 반환
@author    hhh
@date      2013.05.09

*/
//DWORD CTiorSaverReloadApp::GetRemoteCurrentSessionId()
//{
//	PWTS_SESSION_INFO ppSessionInfo = NULL;
//	DWORD     pCount = 0, dwRetSessionID = -1;
//	WTS_SESSION_INFO  wts;
//	CString strStationName, strLog;
//	WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0, 1, &ppSessionInfo, &pCount );
//	//strLog.Format(_T("[TiorSaverReload] SessionCnt: %d"), pCount);
//	//UM_WRITE_LOG(strLog);
//	for( DWORD i = 0; i < pCount; i++ )
//	{
//
//		wts = ppSessionInfo[i];
//		LPTSTR  ppBuffer        = NULL;
//		DWORD   pBytesReturned  = 0;
//
//		//strLog.Format(_T("[TiorSaverReload] SessionID: %d"), wts.SessionId);
//		//UM_WRITE_LOG(strLog);
//		if(wts.SessionId == RDP_LISTENING_SESSION)			//해당 세션은 리모드 리스닝 세션이므로 무시.
//			continue;
//
//		if( WTSQuerySessionInformation( WTS_CURRENT_SERVER_HANDLE,
//			wts.SessionId,
//			WTSWinStationName,
//			&ppBuffer,
//			&pBytesReturned) )
//		{
//			strStationName.Format(_T("%s"), ppBuffer);
//			//UM_WRITE_LOG(_T("[TiorSaverReload] SessionName : ") +  strStationName);
//			if(strStationName.Find(_T("RDP")) != -1)		//원격데스크톱 session-name를 가진 세션ID를 리턴
//			{			
//				WTSFreeMemory( ppBuffer );
//				dwRetSessionID = wts.SessionId;
//				break;
//			}
//
//		}   
//		WTSFreeMemory( ppBuffer );
//
//	}
//	
//	return dwRetSessionID;
//}

void CTiorSaverReloadApp::ServiceCheck()
{

	//서비스의 프로세스의 Thread를 resumeThread로 전환한다.
	DWORD dwOfsSvcPid = GetProcessID(WTIOR_AGENT_SERVICE_NAME);
	if(dwOfsSvcPid > 0)
	{
		if(FALSE == StopAT_Suspend() )
			ResumeProcess(dwOfsSvcPid);
	}
	

	CString strLog;
	CImpersonator imp;
	CString strServicePath = _T("");
	strServicePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);

	UM_WRITE_LOG(_T("[TiorSaverReload_s] --ServiceCheck start"));	

	SC_HANDLE hScManage_Handle=  NULL, hService = NULL;
	hScManage_Handle = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

	if(hScManage_Handle != NULL)
	{
		UM_WRITE_LOG(_T("[TiorSaverReload_s] --OpenSCManager is Good"));	
		DWORD dwError;
		hService = OpenService(hScManage_Handle, TS_SERVICE_NAME, SERVICE_ALL_ACCESS);
		
		if (hService == NULL) 
		{
			dwError = GetLastError();
			
			//서비스가 설치되어 있지 않다면 재설치를 진행한다.
			if(dwError == ERROR_SERVICE_DOES_NOT_EXIST)
			{									
				if( imp.CreatProcessAndCompareStr(strServicePath,  "ok", _T("i")) )
				{
					strLog.Format(_T("[TiorSaverReload_s] Service Reinstall start.."));
					UM_WRITE_LOG(strLog);			
				}
				else
				{
					strLog.Format(_T("[TiorSaverReload_s] Service Reinstall start Fail : %d"), GetLastError() );
					UM_WRITE_LOG(strLog);									
				}						
			}					
		}
		else			//서비스가 있다면 동작상태 체크
		{
			strLog.Format(_T("[TiorSaverReload_s] OpenService sSuccess") );
			UM_WRITE_LOG(strLog);	

			SERVICE_STATUS ss;
			if( ControlService(hService, SERVICE_CONTROL_INTERROGATE, &ss) )
			{
				strLog.Format(_T("[TiorSaverReload_s] ControlService sSuccess: %d"), ss.dwCurrentState );
				UM_WRITE_LOG(strLog);	

				if(ss.dwCurrentState == SERVICE_PAUSED)		//서비스가 일시중지 라면
				{
					strLog.Format(_T("[TiorSaverReload_s] Service -- SERVICE_PAUSED") );
					UM_WRITE_LOG(strLog);	

					if( false == imp.CreatProcessAndCompareStr(strServicePath,  "ok", _T("c")) )
					{
						strLog.Format(_T("[TiorSaverReload_s] Service continueFail : %d"), GetLastError() );
						UM_WRITE_LOG(strLog);	
					}
				}
			}
			else
			{	
				strLog.Format(_T("[TiorSaverReload_s] ControlService Fail") );
				UM_WRITE_LOG(strLog);	

				DWORD dwError =GetLastError();
				if(dwError == ERROR_SERVICE_NOT_ACTIVE)		//서비스가 중지된 상태
				{
					strLog.Format(_T("[TiorSaverReload_s] Service -- SERVICE_STOPPED") );
					UM_WRITE_LOG(strLog);	
					//strServicePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), SVC_PROC_NAME);
					if( false == imp.CreatProcessAndCompareStr(strServicePath,  "ok", _T("s")) )
					{
						strLog.Format(_T("[TiorSaverReload_s] Service start Fail : %d"), GetLastError() );
						UM_WRITE_LOG(strLog);	
					}				
				}
				
			}

			LPQUERY_SERVICE_CONFIG lpsc; 
			BYTE ServeConfig[400] ={0,};
			DWORD dwBytesNeeded, cbBufSize, dwError; 
			cbBufSize = 400;
			lpsc = (LPQUERY_SERVICE_CONFIG)&ServeConfig;
			//서비스의 시작 상태값 확인
			/*if( !QueryServiceConfig(hService, NULL, 0, &dwBytesNeeded))
			{
				strLog.Format(L"[TiorSaverReload_s] dwBytesNeeded : %d", dwBytesNeeded);
				UM_WRITE_LOG(strLog);
				dwError = GetLastError();
				if( ERROR_INSUFFICIENT_BUFFER == dwError )
				{
					cbBufSize = dwBytesNeeded;
					lpsc = (LPQUERY_SERVICE_CONFIG) LocalAlloc(LMEM_FIXED, cbBufSize);
				}				
			}*/
  
			if( !QueryServiceConfig( hService, lpsc, cbBufSize, &dwBytesNeeded) ) 
			{				
				dwError = GetLastError();
				strLog.Format(L"[TiorSaverReload_s] QueryServiceConfig  2 Error : %d", dwError);
				UM_WRITE_LOG(strLog);	
			}

			strLog.Format(L"[TiorSaverReload_s] - QueryServiceConfig Result: %d", lpsc->dwStartType);
			UM_WRITE_LOG(strLog);

			//시작 유형이 바꼈다면 원래대로 돌려놓는다.
			if(lpsc->dwStartType != SERVICE_AUTO_START)
			{
				CString strServicePath;
				strServicePath.Format(L"%s%s", CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
				if(	FALSE == ChangeServiceConfig(hService, 
										SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
										SERVICE_AUTO_START,
										SERVICE_ERROR_NORMAL,
										strServicePath.GetBuffer(0),
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										L"TiorSaver SERVICE") )
				{
					dwError = GetLastError();
					strLog.Format(L"[TiorSaverReload_s] ChangeServiceConfig  Error : %d", dwError);
					UM_WRITE_LOG(strLog);	
				}



			}

		}

		imp.Free();		
		if(hScManage_Handle)
			CloseServiceHandle(hScManage_Handle);
		if(hService)
			CloseServiceHandle(hService);
	}
	else
	{
		strLog.Format(_T("[TiorSaverReload_s] OpenSCManager Fail.. Error: %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
	}



}

/**
@brief     ResumeThread 기능을 동작할건지 여부 체크
@author    hhh
@date      2013.07.26
@return	   BOOL
*/
BOOL CTiorSaverReloadApp::StopAT_Suspend()
{
	DWORD dwDefalut = 0, dwRet = 0;
	dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, DBG_LOG_DEFAULT_KEY, L"ATSuspend", dwDefalut);
	if(dwRet == 1)
		return TRUE;
	else
		return FALSE;
}

BOOL CTiorSaverReloadApp::ChangeFileName(CString _strChangeFileName)
{
	BOOL bResult = FALSE;
	CString strFileFullpath, strTempName, strLog;
	try
	{
		strFileFullpath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), _strChangeFileName) ;

		strTempName.Format( _T("%s_%s"), CPathInfo::GetClientInstallPath(), _strChangeFileName) ;  //_가 붙은 파일이름으로 변경
		//이미 백업된 파일이 있다면 삭제 후 ReName한다.

		UM_WRITE_LOG(strFileFullpath + _T("  ->") + strTempName);

		CFile::Rename( strFileFullpath, strTempName);
		bResult = TRUE;
	}
	catch (CFileException* pEx)
	{
		strLog.Format(_T("ChangeFileName Error : %d"), pEx->m_cause);
		UM_WRITE_LOG(strLog);
		pEx->Delete();
		bResult = FALSE;
	}
	return bResult;
}

BOOL CTiorSaverReloadApp::KillTiorProcess(CString _strFIlePath)
{
	CString strLog = _T("");

	HANDLE hToken;
	OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
		&hToken);

	SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);

	//BOOL bRet = CProcess::KillProcess(_strFIlePath);	// 2017-09-15 sy.choi 프로세스가 실행중이 아닐때에도 TRUE 리턴하므로
	BOOL bRet = CProcess::KillProcessByName(_strFIlePath);
	strLog.Format(_T("Kill ret : %d"), bRet);
	UM_WRITE_LOG(strLog);

	SetPrivilege(hToken, SE_DEBUG_NAME, FALSE);
	CloseHandle(hToken);
	return bRet;
}

BOOL CTiorSaverReloadApp::DeleteService()
{
	if (FALSE == CheckVaildServiceName(TS_SERVICE_NAME)) {
		return TRUE;
	}
	if (FALSE == ExistsService(TS_SERVICE_NAME)) {
		return TRUE;
	}
	CImpersonator imp;
	CString strServiceName = _T("");
	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
	if( imp.CreatProcessAndCompareStr(strServiceName,  "ok", _T("u")) )
	{
		imp.Free();
		return TRUE;
	}
	else
	{
		imp.Free();
		return FALSE;
	}
}
BOOL CTiorSaverReloadApp::SetPrivilege(
							  HANDLE hToken,   // 토큰 핸들
							  LPCTSTR Privilege,   // 활성/비활성화할 권한
							  BOOL bEnablePrivilege  // 권한 활성화 여부?
							  )
{
	DWORD dwErr = 0;
	TOKEN_PRIVILEGES tp;
	LUID luid;
	TOKEN_PRIVILEGES tpPrevious;
	DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);

	if (!LookupPrivilegeValue( NULL, Privilege, &luid ))
		return FALSE;

	// 현재의 권한 설정 얻기
	tp.PrivilegeCount   = 1;
	tp.Privileges[0].Luid   = luid;
	tp.Privileges[0].Attributes = 0;

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		&tpPrevious,
		&cbPrevious
		);

	dwErr = GetLastError();
	if (dwErr != ERROR_SUCCESS)
		return FALSE;

	// 이전의 권한 설정에 따라 권한 설정하기
	tpPrevious.PrivilegeCount       = 1;
	tpPrevious.Privileges[0].Luid   = luid;

	if (bEnablePrivilege) {
		tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
	}
	else {
		tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
			tpPrevious.Privileges[0].Attributes);
	}

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tpPrevious,
		cbPrevious,
		NULL,
		NULL
		);

	dwErr = GetLastError();
	if (dwErr != ERROR_SUCCESS)
		return FALSE;

	return TRUE;
}

#define MAXLEN_SERVICENAME              182        /**< 서비스 이름 최대 길이NT이상OS (NULL 종료문자를 제외한 최대 길이)      */
#define MAXLEN_NTSERVICENAME            80         /**< 서비스 이름 최대 길이 NT에서만(NULL 종료문자를 제외한 최대 길이)      */
BOOL CTiorSaverReloadApp::CheckVaildServiceName(const CString &_sServiceName)
{    
	if (_T("") == _sServiceName) 
		return false;

	CWinOsVersion OSVersion;
	if (osWinNT == OSVersion.GetProduct()) 
	{
		if (MAXLEN_NTSERVICENAME < _sServiceName.GetLength()) 
			return false;
	}
	else
	{
		if (MAXLEN_SERVICENAME < _sServiceName.GetLength()) 
			return false;
	}

	if (-1 != _sServiceName.Find(_T("/"))) 
		return false;    
	if (-1 != _sServiceName.Find(_T("\\")))
		return false;

	return true;
}

BOOL CTiorSaverReloadApp::ExistsService(const CString &_sSvcName)
{

	SC_HANDLE schService = NULL;
	SC_HANDLE schSCManager = NULL;

	if (!CheckVaildServiceName(_sSvcName)) 
		return false;    

	schSCManager = ::OpenSCManager(NULL, NULL, GENERIC_READ);
	if (NULL == schSCManager)
		return false;

	schService = ::OpenService(schSCManager, _sSvcName, SERVICE_QUERY_STATUS);        
	if (NULL == schService)    
	{
		::CloseServiceHandle(schSCManager);
		return false;
	}

	::CloseServiceHandle(schService);
	::CloseServiceHandle(schSCManager);

	return true;
}