// CommonClass.cpp: implementation of the CCommonClass class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "CommonClass.h"

/*********Additional Include File**************************/
#include <iphlpapi.h>
#include <Shlobj.h>
/**********************************************************/

#include <Sddl.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCommonClass::CCommonClass()
{

}

CCommonClass::~CCommonClass()
{

}
/***************************SHELL*********************************************/

/*************************Registry********************************************/
CString CCommonClass::GetRegistryCharValue(LPTSTR pszKey, LPTSTR pszValue, HKEY hRootKey)
{
	HKEY hKey = NULL;

	if (ERROR_SUCCESS != RegOpenKeyEx(hRootKey, pszKey, 0, KEY_READ, &hKey))
	{
		return _T("");
	}
	
	TCHAR szResult[1024] = {0};
	DWORD dwBufferSize = sizeof(szResult) * sizeof(TCHAR);

	if (ERROR_SUCCESS != RegQueryValueEx (hKey, pszValue, NULL, NULL, (LPBYTE)szResult, &dwBufferSize))
	{
		if (hKey != NULL) RegCloseKey(hKey);

		hKey = NULL;

		return _T("");
	}
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;

	CString strResult = szResult;

	return strResult;
}

BOOL CCommonClass::SetRegistryValue(LPTSTR pszKey, LPTSTR pszValue, DWORD dwInput)
{
	HKEY hKey = NULL;

	if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, pszKey, 0, KEY_ALL_ACCESS, &hKey))
	{
		return FALSE;
	}

	RegSetValueEx(hKey, pszValue, 0, REG_DWORD, (LPBYTE)&dwInput, sizeof(DWORD));
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;

	return TRUE;
}

BOOL CCommonClass::SetRegistryValue(LPTSTR pszKey, LPTSTR pszValue, LPTSTR pszInput)
{
	HKEY hKey = NULL;

	if(ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, pszKey, 0, KEY_ALL_ACCESS, &hKey))
	{
		return FALSE;
	}
	
	RegSetValueEx(hKey, pszValue, 0, REG_SZ, (const LPBYTE)pszInput, lstrlen(pszInput) + 1);
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;

	return TRUE;
}

CString CCommonClass::GetLogonId()
{
	TCHAR szLogonID[256] = {'\0'};
	DWORD dwBufferSize = sizeof(szLogonID) * sizeof(TCHAR);
	if(!::GetUserName(szLogonID, &dwBufferSize)) return _T("");

	CString strResult = szLogonID;

	return strResult;
}

DWORD CCommonClass::CreateProcessEx(LPTSTR pszCommand, USHORT iShowMode, BOOL bWait)
{
	STARTUPINFO			si;
	PROCESS_INFORMATION pi;
	
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));
	
	si.cb			= sizeof(si);

	if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;
	
	si.wShowWindow	= iShowMode;
	

/*	SECURITY_ATTRIBUTES saPipe;
	
	saPipe.lpSecurityDescriptor = (PSECURITY_DESCRIPTOR) malloc(SECURITY_DESCRIPTOR_MIN_LENGTH);
	
	InitializeSecurityDescriptor(saPipe.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
	
	// Access Control list is asiigned as NULL inorder to allow all access to the object.
	
	SetSecurityDescriptorDacl(saPipe.lpSecurityDescriptor, TRUE, (PACL) NULL,FALSE);
	
	saPipe.nLength = sizeof(saPipe);
	
	saPipe.bInheritHandle = TRUE;
*/

	// Start the child process. 
	BOOL bResult = CreateProcess(NULL,				// No module name (use command line). 
								 pszCommand,		// Command line. 
								 NULL,             // Process handle not inheritable. 
								 NULL,             // Thread handle not inheritable. 
								 FALSE,            // Set handle inheritance to FAL
								 NORMAL_PRIORITY_CLASS,
								 NULL,             // Use parent's environment block. 
								 NULL,             // Use parent's starting directory. 
								 &si,              // Pointer to STARTUPINFO structure.
								 &pi);             // Pointer to PROCESS_INFORMATION structure.
	
	if (!bResult) return FALSE;

	// Wait until child process exits.
	if (bWait)
	{
		if (pi.hProcess != NULL && pi.hProcess != INVALID_HANDLE_VALUE)
			WaitForSingleObject (pi.hProcess, INFINITE);
	}

	DWORD dwExit=0;
	GetExitCodeProcess(pi.hProcess,&dwExit); 

	// Close process and thread handles.
	CloseHandle (pi.hProcess);
	CloseHandle	(pi.hThread);

	return dwExit;
}

DWORD CCommonClass::CreateProcessEx(CString &strCommand, USHORT iShowMode, BOOL bWait)
{
	STARTUPINFO			si;
	PROCESS_INFORMATION pi;
	
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));
	
	si.cb			= sizeof(si);

	if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;
	
	si.wShowWindow	= iShowMode;
	
	// Start the child process. 
	BOOL bResult = CreateProcess(NULL,				// No module name (use command line). 
								 (LPTSTR)(LPCTSTR)strCommand,		// Command line. 
								 NULL,             // Process handle not inheritable. 
								 NULL,             // Thread handle not inheritable. 
								 FALSE,            // Set handle inheritance to FAL
								 NORMAL_PRIORITY_CLASS,
								 NULL,             // Use parent's environment block. 
								 NULL,             // Use parent's starting directory. 
								 &si,              // Pointer to STARTUPINFO structure.
								 &pi);             // Pointer to PROCESS_INFORMATION structure.
	
	if (!bResult) return FALSE;

	// Wait until child process exits.
	if (bWait)
	{
		if (pi.hProcess != NULL && pi.hProcess != INVALID_HANDLE_VALUE)
			WaitForSingleObject (pi.hProcess, INFINITE);
	}

	DWORD dwExit=0;
	GetExitCodeProcess(pi.hProcess,&dwExit); 

	// Close process and thread handles.
	CloseHandle (pi.hProcess);
	CloseHandle	(pi.hThread);
	
	return dwExit;
}

DWORD CCommonClass::CreateProcessEx(CString strCommand, USHORT iShowMode, BOOL bWait,CString strDesktop)
{
	STARTUPINFO			si;
	PROCESS_INFORMATION pi;
	
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));
	
	si.cb			= sizeof(si);

	if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;
	
	si.lpDesktop = (TCHAR*)(LPCTSTR)strDesktop;			

	si.wShowWindow	= iShowMode;

	
	
	// Start the child process. 
	BOOL bResult = CreateProcess(NULL,				// No module name (use command line). 
								 (LPTSTR)(LPCTSTR)strCommand,		// Command line. 
								 NULL,             // Process handle not inheritable. 
								 NULL,             // Thread handle not inheritable. 
								 FALSE,            // Set handle inheritance to FAL
								 NORMAL_PRIORITY_CLASS,
								 NULL,             // Use parent's environment block. 
								 NULL,             // Use parent's starting directory. 
								 &si,              // Pointer to STARTUPINFO structure.
								 &pi);             // Pointer to PROCESS_INFORMATION structure.
	
	if (!bResult) return FALSE;

	// Wait until child process exits.
	if (bWait)
	{
		if (pi.hProcess != NULL && pi.hProcess != INVALID_HANDLE_VALUE)
			WaitForSingleObject (pi.hProcess, INFINITE);
	}

	DWORD dwExit=0;
	GetExitCodeProcess(pi.hProcess,&dwExit); 

	// Close process and thread handles.
	CloseHandle (pi.hProcess);
	CloseHandle	(pi.hThread);

	return dwExit;
}

BOOL CCommonClass::CreateProcessEx(CString &strCommand, USHORT iShowMode,PROCESS_INFORMATION *pi)
{
	STARTUPINFO			si;
	
	ZeroMemory(&si, sizeof(si));
	ZeroMemory(pi, sizeof(PROCESS_INFORMATION));
	
	si.cb			= sizeof(si);

	if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;
	
	si.wShowWindow	= iShowMode;

	
	
	// Start the child process. 
	BOOL bResult = CreateProcess(NULL,				// No module name (use command line). 
								 (LPTSTR)(LPCTSTR)strCommand,		// Command line. 
								 NULL,             // Process handle not inheritable. 
								 NULL,             // Thread handle not inheritable. 
								 FALSE,            // Set handle inheritance to FAL
								 CREATE_SUSPENDED|NORMAL_PRIORITY_CLASS,
								 NULL,             // Use parent's environment block. 
								 NULL,             // Use parent's starting directory. 
								 &si,              // Pointer to STARTUPINFO structure.
								 pi);             // Pointer to PROCESS_INFORMATION structure.
	
	if (!bResult) return FALSE;
	
	return TRUE;
}

CString CCommonClass::GetDirectory(CString &strFilePath)
{
	int iFind = strFilePath.ReverseFind('\\');

	if (iFind == -1) return _T("");

	return strFilePath.Left(iFind);
}

CString CCommonClass::GetFileName(CString &strFilePath)
{
	int iFind = strFilePath.ReverseFind('\\');

	if (iFind == -1) return _T("");
	
	return strFilePath.Mid(iFind + 1);
}

BOOL CCommonClass::MakeFile(CString &strFilePath, LPTSTR pszBuffer, CStringArray* lpstrArray)
{
	CStdioFile stdFile;

	if (!stdFile.Open(strFilePath, CFile::modeCreate | CFile::modeWrite, NULL))
	{
		CreateDirectory(GetDirectory(strFilePath), NULL);

		if (!stdFile.Open(strFilePath, CFile::modeCreate | CFile::modeWrite, NULL))
		{
			return FALSE;
		}
	}
	
	if (pszBuffer != NULL)
	{
		stdFile.Write(pszBuffer, lstrlen(pszBuffer));
	}
	else if (lpstrArray != NULL)
	{
		for (int i = 0; i < lpstrArray->GetSize(); i++)
		{
			if		(lpstrArray->GetAt(i).Right(1) == '\n') stdFile.WriteString(lpstrArray->GetAt(i));
			else	stdFile.WriteString(lpstrArray->GetAt(i) + '\n');
		}
	}
	
	stdFile.Close();

	return TRUE;
}
/*****************************Get Path*****************************************/
CString CCommonClass::GetRootPath()
{
	UINT iBufferSize = GetWindowsDirectory(NULL, 0);
		
	TCHAR *pszWindowsPath = new TCHAR[iBufferSize];
	ZeroMemory(pszWindowsPath, iBufferSize);
		
	GetWindowsDirectory(pszWindowsPath, iBufferSize);

	CString strResult = pszWindowsPath;
	if( !strResult.IsEmpty() )
	{
		strResult = strResult.Left(strResult.Find(':')+1);
	}

	delete[] pszWindowsPath;

	return strResult;
}

CString CCommonClass::GetWindowsPath()
{
	UINT iBufferSize = GetWindowsDirectory(NULL, 0);
		
	TCHAR *pszWindowsPath = new TCHAR[iBufferSize];
	ZeroMemory(pszWindowsPath, iBufferSize);
		
	GetWindowsDirectory(pszWindowsPath, iBufferSize);

	CString strResult = pszWindowsPath;

	delete[] pszWindowsPath;

	return strResult;
}

CString CCommonClass::GetSystemPath()
{
	TCHAR szSystemDirectory[1024]={0};
	GetSystemDirectory(szSystemDirectory, sizeof(szSystemDirectory));
	
	CString strResult = szSystemDirectory;

	return strResult;
}

CString CCommonClass::GetProgramFilePath()
{
	HKEY hKey = NULL;

	if (ERROR_SUCCESS != RegOpenKeyEx (HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion"), 0, KEY_READ, &hKey))
	{
		return _T("");
	}

	TCHAR szPath[1024] = {0};
	DWORD dwSize = sizeof(szPath) * sizeof(TCHAR);

	if(ERROR_SUCCESS != RegQueryValueEx (hKey, TEXT("ProgramFilesDir"), NULL, NULL, (LPBYTE)szPath, &dwSize))
	{
		if (hKey != NULL) RegCloseKey(hKey);
		return _T("");
	}

	CString strResult = szPath;

	if(hKey != NULL) RegCloseKey(hKey);

	return strResult;
}

BOOL CCommonClass::TerminateProgram(CString &strProgramPath, BOOL bWait)
{
	HMODULE hModule = GetModuleHandle(strProgramPath);
	
	BOOL bResult = TerminateProcess(hModule, 0);
	
	if (!bWait) return bResult;
	
	WaitForSingleObject(hModule, INFINITE);
	
	return bResult;
}

BOOL CCommonClass::IsValidIP(CString &strIP)
{
	int iSubFind = 0, iCount = 0, iMask = 0, iTotalCount = 0;
	const TCHAR chParse = '.';
	
	while (((iSubFind = strIP.Find(chParse, iCount)) != -1))
	{
		CString strFind = strIP.Mid (iCount, iSubFind - iCount);
		
		if (strFind.IsEmpty()) return FALSE;
		
		for (int i = 0; i < strFind.GetLength(); i++)
		{
			if (!isdigit(strFind.GetAt(i))) return FALSE;
		}
		
		iMask = _ttoi(strFind);

		if (iMask < 0 || iMask > 255) return FALSE;

		iCount = iSubFind + 1;
		
		iTotalCount++;
	}

	if (iSubFind == -1)
	{
		CString strFind = strIP.Mid(iCount, strIP.GetLength() - iCount);
		
		if (strFind.IsEmpty()) return FALSE;
		
		for (int i = 0; i < strFind.GetLength(); i++)
		{
			if (!isdigit(strFind.GetAt(i))) return FALSE;
		}

		iMask = _ttoi(strFind);
		
		if (iMask < 0 || iMask > 255) return FALSE;

		iTotalCount++;
	}
	
	if (iTotalCount != 4) return FALSE;

	return TRUE;
}

CString CCommonClass::GetTemporaryPath()
{
	DWORD dwBufferSize = GetTempPath(0, NULL);
	
	TCHAR *pszTempPath = new TCHAR[dwBufferSize];
	ZeroMemory(pszTempPath, dwBufferSize);
	
	GetTempPath(dwBufferSize, pszTempPath);
	
	CString strResult = pszTempPath;
	
	delete[] pszTempPath;
	
	return strResult;
}

int CCommonClass::GetUserListFromReg(CStringArray &arrUser)
{

	DWORD dwIndex=0,cbBuffer=MAX_PATH;
	TCHAR szKeyName[MAX_PATH]={0,};
	HKEY  hKey=0;

	arrUser.RemoveAll();

	if(RegOpenKey(HKEY_USERS,_T(""),&hKey)!=ERROR_SUCCESS)
	{
		return 0;
	}
	while(ERROR_SUCCESS==RegEnumKey(hKey,dwIndex,szKeyName,cbBuffer))
	{
		arrUser.Add(szKeyName);
		dwIndex ++;
	}

	RegCloseKey(hKey);

	return arrUser.GetSize();

}

CString CCommonClass::GetLocalIPAddress(CStringArray *pstrArray)
{
	DWORD dwBufferSize	= 0;
	
	IP_ADAPTER_INFO *lpGateware = NULL, *lpTemp = NULL;

	CString strGateway, strIPAddress, strRealIPAddress;

	GetAdaptersInfo(NULL, &dwBufferSize);

	if (dwBufferSize == 0)
	{
		SetDefaultIPAddress(strRealIPAddress);
		return strRealIPAddress;
	}
	
	lpGateware = (IP_ADAPTER_INFO *) GlobalAlloc(GPTR, dwBufferSize);

	if (ERROR_SUCCESS != GetAdaptersInfo(lpGateware, &dwBufferSize))
	{
		SetDefaultIPAddress(strRealIPAddress);
		return strRealIPAddress;
	}
	
	lpTemp = lpGateware;
	
	while(TRUE)
	{
		strGateway		= lpTemp->GatewayList.IpAddress.String;
		strIPAddress	= lpTemp->IpAddressList.IpAddress.String;
		
		/***** This Macaddress And IP Address is passed default Gateway****/
		if (!strGateway.IsEmpty() && (lpTemp->Type == MIB_IF_TYPE_ETHERNET)) strRealIPAddress = strIPAddress;
		
		if (pstrArray == NULL)
		{
			if (!strRealIPAddress.IsEmpty()) break;
		}
		else
		{
			pstrArray->Add(strIPAddress);
		}
		
		lpTemp = lpTemp->Next;
		
		if (lpTemp == NULL) break;
	}

	GlobalFree(lpGateware);

	if (strRealIPAddress.IsEmpty()) SetDefaultIPAddress(strRealIPAddress);

	return strRealIPAddress;
}

CString CCommonClass::GetMacAddress(CStringArray *pstrArray)
{
	DWORD dwBufferSize	= 0;
	
	IP_ADAPTER_INFO *lpGateware = NULL, *lpTemp = NULL;
	CString strGateway, strMacAddress, strRealMacAddress;

	GetAdaptersInfo(NULL, &dwBufferSize);

	if (dwBufferSize == 0)
	{
		SetDefaultMacAddress(strRealMacAddress);

		return strRealMacAddress;
	}
	
	lpGateware = (IP_ADAPTER_INFO *) GlobalAlloc(GPTR, dwBufferSize);

	if (ERROR_SUCCESS != GetAdaptersInfo(lpGateware, &dwBufferSize))
	{
		SetDefaultMacAddress(strRealMacAddress);
		
		return strRealMacAddress;
	}

	lpTemp = lpGateware;
	
	while(TRUE)
	{
		strGateway = lpTemp->GatewayList.IpAddress.String;
		
		strMacAddress.Format("%02X:%02X:%02X:%02X:%02X:%02X",
							  lpTemp->Address[0], lpTemp->Address[1],
							  lpTemp->Address[2], lpTemp->Address[3],
							  lpTemp->Address[4], lpTemp->Address[5]);
		
		/***** This Macaddress And IP Address is passed default Gateway****/
		if (!strGateway.IsEmpty() && (lpTemp->Type == MIB_IF_TYPE_ETHERNET)) strRealMacAddress = strMacAddress;
		
		if (pstrArray == NULL)
		{
			if (!strRealMacAddress.IsEmpty()) break;
		}
		else
		{
			pstrArray->Add(strMacAddress);
		}
		
		lpTemp = lpTemp->Next;
		
		if (lpTemp == NULL) break;
	}
	
	GlobalFree(lpGateware);
	
	if (strRealMacAddress.IsEmpty()) SetDefaultMacAddress(strRealMacAddress);
	
	return strRealMacAddress;
}

void CCommonClass::SetDefaultIPAddress(CString& strIPAddress)
{
	WORD		wVersionRequested;
	WSADATA		wsaData;
	char		name[50] = {0};
	PHOSTENT	hostinfo;
	wVersionRequested = MAKEWORD(2, 0);
	
	if (!WSAStartup( wVersionRequested, &wsaData))
	{
		if (!gethostname (name, sizeof(name)))
		{
			if ((hostinfo = gethostbyname(name)) != NULL)
			{
				strIPAddress.Format("%s", inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list));
			}
		}
		
		WSACleanup();
	}
}

void CCommonClass::SetDefaultMacAddress(CString& strMacAddress)
{
	ASTAT		Adapter;
    NCB			Ncb;
	UCHAR		uRetCode;
	TCHAR		NetName[50] = {0};
	LANA_ENUM   lenum;
	
	memset(&Ncb, 0, sizeof(Ncb));
    Ncb.ncb_command	= NCBENUM;
    Ncb.ncb_buffer	= (UCHAR *)&lenum;
    Ncb.ncb_length	= sizeof(lenum);

    uRetCode		= Netbios( &Ncb );

	CString strMac;

    for (int i = 0; i < lenum.length; i++)
    {
		memset( &Ncb, 0, sizeof(Ncb) );
        Ncb.ncb_command = NCBRESET;
        Ncb.ncb_lana_num = lenum.lana[i];

        uRetCode = Netbios( &Ncb );

        memset( &Ncb, 0, sizeof (Ncb) );
        Ncb.ncb_command = NCBASTAT;
        Ncb.ncb_lana_num = lenum.lana[i];

        strcpy((char*)Ncb.ncb_callname, "*               ");
        Ncb.ncb_buffer = (unsigned char *) &Adapter;
        Ncb.ncb_length = sizeof(Adapter);

        uRetCode = Netbios( &Ncb );
		
        if (uRetCode == 0)
        {
			strMac.Format("%02X:%02X:%02X:%02X:%02X:%02X",
							      Adapter.adapt.adapter_address[0],
								  Adapter.adapt.adapter_address[1],
								  Adapter.adapt.adapter_address[2],
								  Adapter.adapt.adapter_address[3],
								  Adapter.adapt.adapter_address[4],
								  Adapter.adapt.adapter_address[5]);

#ifdef _WOONGJIN
			// 웅진 VPN MAC 제외
			if ( strMac.CompareNoCase( "44:45:53:54:42:00" ) != 0)
				strMacAddress = strMac;
#else
			strMacAddress = strMac;
#endif

		}
	}
}

BOOL CCommonClass::CreateFileEx (WORD iResource, TCHAR *pszPath)
{
	HRSRC hRes = NULL;
	
	hRes = FindResource(NULL, MAKEINTRESOURCE(iResource), TEXT("ExecuteFile"));
	
	if (hRes == NULL) return FALSE;

	LPVOID mem = LoadResource (NULL, hRes);
	
	if (mem == NULL) return FALSE;

	DWORD dwBufferSize = SizeofResource(NULL, hRes);
	
	HANDLE handle = CreateFile (pszPath, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	
	if (handle == NULL || handle == INVALID_HANDLE_VALUE) 
	{
		return FALSE;
	}
	
	/* Error Handler ///////////////////////////////////////
	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{
		CloseHandle(handle);
		return TRUE;
	}*//////////////////////////////////////////////////////
	
	DWORD dwWriteSize = 0;

	BOOL bResult = WriteFile(handle, mem, dwBufferSize, &dwWriteSize, NULL);
		
	CloseHandle(handle);
	
	return bResult;
}

CString CCommonClass::GetFileVersion(CString strFilePath)
{
	DWORD vSize=0, vLen=0;
	LPVOID SoftVI=NULL;

	vSize = GetFileVersionInfoSize((LPSTR)(LPCSTR)strFilePath, &vLen);
		
	// Get None SoftWareInfo /////////////////////
	if (!vSize) return "";

	// Get Buffer
	SoftVI = malloc(vSize+1);

	if (SoftVI==NULL)
	{
		return "";
	}

	if (GetFileVersionInfo((LPSTR)(LPCSTR)strFilePath, NULL, vSize, SoftVI)) 
	{
		// Get Software Version
		VS_FIXEDFILEINFO *FFI = NULL;
		
		if (VerQueryValue (SoftVI, _TEXT("\\"), (LPVOID*)&FFI, (UINT *)&vLen))
		{
			if (FFI != NULL)
			{
				DWORD dwVer1 = FFI->dwFileVersionMS;
				DWORD dwVer2 = FFI->dwFileVersionLS;
				
				CString strVersion;
				strVersion.Format("%d.%d.%d.%d", HIWORD(dwVer1), LOWORD(dwVer1), HIWORD(dwVer2), LOWORD(dwVer2));
			
				// Buffer Free
				if(SoftVI != NULL) free(SoftVI);

				return strVersion;
			}
		}
	}

	// Buffer Free
	if(SoftVI != NULL) free(SoftVI);
	
	return "";
}

BOOL CCommonClass::SaveFile(CString strDestFile, LPBYTE pbyBuffer, DWORD dwBufferSize)
{
	if (dwBufferSize == 0 || pbyBuffer == NULL) return FALSE;

	SetFilePath(strDestFile);
	
	CFile			writer;
	CFileException	ex;
	
	int iFind = strDestFile.ReverseFind('\\');
	
	if (iFind > 0) CreateDirectory(strDestFile.Left(iFind), NULL);
	
	if (!writer.Open(strDestFile, CFile::modeWrite|CFile::shareExclusive|CFile::modeCreate, &ex)) return FALSE;
	
	writer.Write(pbyBuffer, dwBufferSize);
	
	writer.Close();

	return TRUE;
}

BOOL CCommonClass::ExecuteFile(CString strDestFile)
{
	BOOL bResult = FALSE;
	
	SetFilePath(strDestFile);

	/* Executable File */
	bResult = CreateProcessEx(strDestFile);
	CString strText=getenv("ProgramFiles");

	
	/* Non-Executable File */
	if (!bResult)
	{
		(32 < (int)ShellExecute(NULL, TEXT("open"), strDestFile, NULL, NULL, SW_SHOWNORMAL)) ? bResult = TRUE : bResult = FALSE;
	}
	
	return bResult;
}

BOOL CCommonClass::RemoveFile(CString strDestFile)
{
	BOOL bResult = FALSE;

	SetFilePath(strDestFile);

	bResult = DeleteFile(strDestFile);

	return bResult;
}

void CCommonClass::SetFilePath(CString &pFilePath)
{
	CString strPath;
	strPath.Format("%s", pFilePath);
	if (strPath.Find('%') != -1)
	{
		if		(strPath.Find(MESSAGE_WINDOWS_DIR) != -1)
			strPath.Replace(MESSAGE_WINDOWS_DIR, GetWindowsPath());
		else if	(strPath.Find(MESSAGE_SYSTEM_DIR) != -1)
			strPath.Replace(MESSAGE_SYSTEM_DIR, GetSystemPath());
		else if	(strPath.Find(MESSAGE_PROGRAM_DIR) != -1)
			strPath.Replace(MESSAGE_PROGRAM_DIR, GetProgramFilePath());
		else if	(strPath.Find(MESSAGE_TEMP_DIR) != -1)
			strPath.Replace(MESSAGE_TEMP_DIR, GetTemporaryPath());
		else if( strPath.Find(MESSAGE_ROOT_DIR) != -1 )
			strPath.Replace(MESSAGE_ROOT_DIR, GetRootPath());
	}
	pFilePath.Format("%s", strPath);
}

BOOL CCommonClass::DelRegistryValue(LPTSTR pszKey, LPTSTR pszValue)
{
	HKEY hKey = NULL;

	if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, pszKey, 0, KEY_ALL_ACCESS, &hKey)) return FALSE;
	
	BOOL bResult = FALSE;
	
	RegDeleteValue(hKey, pszValue) == ERROR_SUCCESS ? bResult = TRUE : bResult = FALSE;
	
	if (hKey != NULL) RegCloseKey(hKey);

	return bResult;
}

BOOL CCommonClass::GetRealIPMAC(CString &strIPAddress, CString &strMacAddress)
{
	DWORD dwBufferSize	= 0;
	
	IP_ADAPTER_INFO *lpGateware = NULL, *lpTemp = NULL;
	
	CString strGateway;

	GetAdaptersInfo(NULL, &dwBufferSize);

	if (dwBufferSize == 0)
	{
		SetDefaultIPAddress(strIPAddress);
		SetDefaultMacAddress(strMacAddress);
		return TRUE;
	}
	
	lpGateware = (IP_ADAPTER_INFO *) GlobalAlloc(GPTR, dwBufferSize);

	if (ERROR_SUCCESS != GetAdaptersInfo(lpGateware, &dwBufferSize))
	{
		SetDefaultIPAddress(strIPAddress);
		SetDefaultMacAddress(strMacAddress);
		return TRUE;
	}
	
	lpTemp = lpGateware;



	
	while(TRUE)
	{
		strGateway		= lpTemp->GatewayList.IpAddress.String;
		strIPAddress	= lpTemp->IpAddressList.IpAddress.String;

		strMacAddress.Format("%02X:%02X:%02X:%02X:%02X:%02X",
							  lpTemp->Address[0], lpTemp->Address[1],
							  lpTemp->Address[2], lpTemp->Address[3],
							  lpTemp->Address[4], lpTemp->Address[5]);
		
		/***** This Macaddress And IP Address is passed default Gateway****/
		if (!strGateway.IsEmpty() && (lpTemp->Type == MIB_IF_TYPE_ETHERNET) &&
			!strIPAddress.IsEmpty() && !strMacAddress.IsEmpty())
		{
			break;
		}
	
		lpTemp = lpTemp->Next;
		
		if (lpTemp == NULL) break;
	}

	GlobalFree(lpGateware);

	if (strIPAddress.IsEmpty())		SetDefaultIPAddress(strIPAddress);
	if (strMacAddress.IsEmpty())	SetDefaultMacAddress(strMacAddress);
	

	return TRUE;
}

BOOL CCommonClass::GetRealIPMAC(CString &strIPAddress, CString &strMacAddress,CString strTargetIP)
{
	DWORD dwBufferSize	= 0;
	
	IP_ADAPTER_INFO *lpGateware = NULL, *lpTemp = NULL;
	
	CString strGateway;
	BOOL bFound=FALSE;

	GetAdaptersInfo(NULL, &dwBufferSize);

	if (dwBufferSize == 0)
	{
		SetDefaultIPAddress(strIPAddress);
		SetDefaultMacAddress(strMacAddress);
		return TRUE;
	}
	
	lpGateware = (IP_ADAPTER_INFO *) GlobalAlloc(GPTR, dwBufferSize);

	if (ERROR_SUCCESS != GetAdaptersInfo(lpGateware, &dwBufferSize))
	{
		SetDefaultIPAddress(strIPAddress);
		SetDefaultMacAddress(strMacAddress);
		return TRUE;
	}
	
	lpTemp = lpGateware;

	DWORD	dwInterface=0;
	GetBestInterface(inet_addr(strTargetIP),&dwInterface);

	CStringArray arrMac;
	
	while(TRUE)
	{
		strGateway		= lpTemp->GatewayList.IpAddress.String;
		strIPAddress	= lpTemp->IpAddressList.IpAddress.String;

		strMacAddress.Format("%02X:%02X:%02X:%02X:%02X:%02X",
							  lpTemp->Address[0], lpTemp->Address[1],
							  lpTemp->Address[2], lpTemp->Address[3],
							  lpTemp->Address[4], lpTemp->Address[5]);


		arrMac.Add(strMacAddress); // vpn mac 일경우 제거
				

		if(dwInterface==lpTemp->Index)
		{
					/***** This Macaddress And IP Address is passed default Gateway****/
			if(!strIPAddress.IsEmpty() && !strMacAddress.IsEmpty())
			{
				bFound=TRUE;
				break;
			}

		}
	
		lpTemp = lpTemp->Next;
		
		if (lpTemp == NULL) break;
	}

	GlobalFree(lpGateware);

	// dwindex로 부터 얻어온 mac이 vpn일경우
	// 다른 mac address로 교체한다.

#ifndef _WOONGJIN
	if(strMacAddress.CompareNoCase("46:53:4C:32:54:50")==0)
	{
		for(int i=0;i<arrMac.GetSize();i++)
		{
			if(arrMac.GetAt(i).CompareNoCase("46:53:4C:32:54:50")!=0)
			{
				strMacAddress=arrMac.GetAt(i);
				break;
			}
		}

	}
#else
	gclDbgLog.WriteTraceLog(_APPLY_LOG, "COMMON:RealIpMac-VPN MAC Check BEGIN ****");
	gclDbgLog.WriteTraceLog(_APPLY_LOG, "COMMON:RealIpMac-MAC(%s)", strMacAddress);
	if(strMacAddress.CompareNoCase("44:45:53:54:42:00")==0)
	{

		for(int i=0;i<arrMac.GetSize();i++)
		{
			gclDbgLog.WriteTraceLog(_APPLY_LOG, "COMMON:RealIpMac- It's VPN MAC");

			if(arrMac.GetAt(i).CompareNoCase("44:45:53:54:42:00")!=0)
			{
				strMacAddress=arrMac.GetAt(i);

				break;
			}
		}
		
		if ( strMacAddress.CompareNoCase("44:45:53:54:42:00") == 0) strMacAddress = _T("");
	}

	gclDbgLog.WriteTraceLog(_APPLY_LOG, "COMMON:RealIpMac- Real Macs(%s)", strMacAddress);
	gclDbgLog.WriteTraceLog(_APPLY_LOG, "COMMON:RealIpMac-VPN MAC Check END ****", strMacAddress);
#endif

	if(!bFound)
	{
		strIPAddress="";
	}
	if (strIPAddress.IsEmpty())		SetDefaultIPAddress(strIPAddress);
	if (strMacAddress.IsEmpty())	SetDefaultMacAddress(strMacAddress);
	
	return TRUE;
}

//wank
BOOL CCommonClass::SetRegistryValueUser(LPTSTR pszKey, LPTSTR pszValue, LPTSTR pszInput)
{
	HKEY hKey = NULL;

	if(ERROR_SUCCESS != RegOpenKeyEx(HKEY_CURRENT_USER, pszKey, 0, KEY_ALL_ACCESS, &hKey))
	{
		TRACE("CCommonClass: SetSafePCPath() RegOpen Error!\n");
		return FALSE;
	}
	
	RegSetValueEx(hKey, pszValue, 0, REG_SZ, (const LPBYTE)pszInput, lstrlen(pszInput) + 1);
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;

	return TRUE;
}
BOOL CCommonClass::SetRegistryValueDefaultUser(LPTSTR pszKey, LPTSTR pszValue, LPTSTR pszInput)
{
	HKEY hKey = NULL;

	if(ERROR_SUCCESS != RegOpenKeyEx(HKEY_USERS, pszKey, 0, KEY_ALL_ACCESS, &hKey))
	{
		TRACE("CCommonClass: SetSafePCPath() RegOpen Error!\n");
		return FALSE;
	}
	
	RegSetValueEx(hKey, pszValue, 0, REG_SZ, (const LPBYTE)pszInput, lstrlen(pszInput) + 1);
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;

	return TRUE;
}

BOOL CCommonClass::SetRegistryValueDWORD(HKEY hkey, LPTSTR pszKey, LPTSTR pszValue,DWORD dwValue)
{
	HKEY hKey = NULL;

	if (ERROR_SUCCESS != RegOpenKeyEx(hkey, pszKey, 0, KEY_ALL_ACCESS, &hKey))
	{
		TRACE("CCommonClass: SetSafePCPath() RegOpen Error!\n");
		return FALSE;
	}

	RegSetValueEx(hKey, pszValue, 0, REG_DWORD, (LPBYTE)&dwValue, sizeof(DWORD));
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;

	return TRUE;	
}
BOOL CCommonClass::GetRegMultiCharValue(HKEY hRootKey, LPTSTR pszKey, LPTSTR pszValue,CStringArray &array)
{

	HKEY hKey = NULL;

	if (ERROR_SUCCESS != RegOpenKeyEx(hRootKey, pszKey, 0, KEY_READ, &hKey))
	{
		return FALSE;
	}
	
	BYTE szResult[1024] = {0};
	TCHAR cData[5];
	DWORD dwBufferSize = sizeof(szResult) * sizeof(BYTE);

	if (ERROR_SUCCESS != RegQueryValueEx (hKey, pszValue, NULL, NULL, (LPBYTE)szResult, &dwBufferSize))
	{
		if (hKey != NULL) RegCloseKey(hKey);

		hKey = NULL;

		return FALSE;
	}
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	hKey = NULL;
	CString strTmp;

	for(int i=0;i<(int)dwBufferSize;i++)
	{
		if(!szResult[i])
		{
			if(!strTmp.IsEmpty())
				array.Add(strTmp);
			strTmp="";	
			continue;
		}

		sprintf(cData,"%c",(char*)szResult[i]);

		strTmp+=cData;

	}

//	CString strResult = szResult;

	return TRUE;
}

BOOL CCommonClass::SetRegMultiCharValue(HKEY hRootKey, LPTSTR pszKey, LPTSTR pszValue, CStringArray &array)
{

	HKEY	hKey = NULL;
	BYTE	szResult[1024] = {0};
	DWORD	dwBufferSize = sizeof(szResult) * sizeof(BYTE);
	int		Seek=0;

	if (ERROR_SUCCESS != RegOpenKeyEx(hRootKey, pszKey, 0, KEY_WRITE, &hKey))
	{
		return FALSE;
	}

	for(int i=0;i<array.GetSize();i++)
	{
		CString strText=array.GetAt(i);
		memcpy(szResult+Seek,(char *)(LPCTSTR)strText,strText.GetLength());

		Seek=Seek+strText.GetLength()+1;
		
	}
	
	
	if (ERROR_SUCCESS != RegSetValueEx(hKey, pszValue, 0, REG_MULTI_SZ, (LPBYTE)szResult, Seek))
	{
		if (hKey != NULL) RegCloseKey(hKey);

		hKey = NULL;

		return FALSE;
	}
	
	if (hKey != NULL) RegCloseKey(hKey);
	
	return TRUE;
}

//kw 20090216 우리금융 설치유도 관련.
void CCommonClass::GetAllIPList(CStringArray &array)
{
	DWORD dwBufferSize	= 0;
	  
	IP_ADAPTER_INFO *lpGateware = NULL, *lpTemp = NULL;
	
	CString strGateway;

	GetAdaptersInfo(NULL, &dwBufferSize);

	if (dwBufferSize == 0)
	{
		return ;
	}
	
	lpGateware = (IP_ADAPTER_INFO *) GlobalAlloc(GPTR, dwBufferSize);

	if (ERROR_SUCCESS != GetAdaptersInfo(lpGateware, &dwBufferSize))
	{
		return ;
	}

	lpTemp = lpGateware;
	
	while(TRUE)
	{
		CString strIPAddress	= lpTemp->IpAddressList.IpAddress.String;
		
		array.Add(strIPAddress);
		
		lpTemp = lpTemp->Next;
		
		if (lpTemp == NULL) break;
	}
	
	GlobalFree(lpGateware);
	

	return ;	
}

BOOL CCommonClass::CreateProcessIL(TCHAR *pFilePath ,BOOL bSessionID,  DWORD dwInSessionID, DWORD dwILevel, DWORD dwWait)
{
	HANDLE hOwnerToken=NULL;
	HANDLE hSeOwToken=NULL;
	DWORD dwSize=0;
	BOOL bRet;
	PSID pIntegritySid = NULL;
	TOKEN_MANDATORY_LABEL TIL = {0};
	PROCESS_INFORMATION ProcInfo = {0};
	STARTUPINFO StartupInfo = {0};
	ULONG ExitCode = 0;
	TCHAR szTokenIntegrityLevel[30]={0,};
	DWORD dwSessionId=0;

	if(bSessionID == FALSE)
	{
		dwSessionId=0;
	}
	else if(bSessionID == TRUE)
	{
		if(dwInSessionID == 0)
		{
			DWORD	(__stdcall *pActiveSessionId)();
			
			HINSTANCE hInst;
			hInst = ::LoadLibrary("Kernel32.dll");
		
			if(hInst != NULL)
			{
				pActiveSessionId = (DWORD(__stdcall *)())::GetProcAddress(hInst, "WTSGetActiveConsoleSessionId");
				dwSessionId = pActiveSessionId();
			}
			
			if(hInst != NULL)
				::FreeLibrary(hInst);

//			dwSessionId = WTSGetActiveConsoleSessionId();
		}
		else
		{
			dwSessionId = dwInSessionID;
		}
	}

	if(dwILevel == 0)		// system
	{
		_tcscpy(szTokenIntegrityLevel, _T("S-1-16-16384"));
	}
	else if(dwILevel ==1)	// low
	{
		_tcscpy(szTokenIntegrityLevel, _T("S-1-16-4096"));		
	}
	else if(dwILevel == 2)	// medium
	{
		_tcscpy(szTokenIntegrityLevel, _T("S-1-16-8192"));		
	}
	else if(dwILevel == 3)	// high
	{
		_tcscpy(szTokenIntegrityLevel, _T("S-1-16-12288"));		
	}

	if(::OpenProcessToken(GetCurrentProcess(), MAXIMUM_ALLOWED, &hOwnerToken))
	{			
		HINSTANCE hInst = NULL;
		hInst = ::LoadLibrary("Advapi32.dll");
		
		
		if(hInst != NULL)
		{
			fpConvertStringSidToSid pConvertStringSidToSid = NULL;
			pConvertStringSidToSid = (fpConvertStringSidToSid)::GetProcAddress(hInst, "ConvertStringSidToSidA");

			if(pConvertStringSidToSid != NULL)
			{
				if(pConvertStringSidToSid(szTokenIntegrityLevel, &pIntegritySid))
				{
					TIL.Label.Attributes = SE_GROUP_INTEGRITY;
					TIL.Label.Sid = pIntegritySid;
					
					DuplicateTokenEx(hOwnerToken, MAXIMUM_ALLOWED, NULL,SecurityImpersonation, TokenPrimary, &hSeOwToken);
										
					// 세션 권한 변경시.			
					if(SetTokenInformation(hSeOwToken, TokenSessionId, &dwSessionId, sizeof(DWORD)))
					{
						// SACL 권한 변경시.
						if(SetTokenInformation(hSeOwToken, (TOKEN_INFORMATION_CLASS)25, &TIL, sizeof(TOKEN_MANDATORY_LABEL) + GetLengthSid(pIntegritySid)))
						{
							TCHAR szDir[MAX_PATH]={0,};
							//_tcscat(szDir, pFilePath);
							_tcscpy(szDir, pFilePath);
							
							/// FilePath 실행.
							memset(&StartupInfo,0,sizeof(StartupInfo));
							memset(&ProcInfo,0,sizeof(ProcInfo));
							
							bRet = CreateProcessAsUser(hSeOwToken, NULL, szDir, NULL, NULL, FALSE,0, NULL, NULL, &StartupInfo, &ProcInfo);
							if(bRet)
							{
								WaitForInputIdle(ProcInfo.hProcess,INFINITE);
								if(dwWait == 1)
								{
									WaitForSingleObject( ProcInfo.hProcess, INFINITE );
								}
								
								LocalFree(pIntegritySid);						
								CloseHandle(hSeOwToken);						
								CloseHandle(hOwnerToken);						
								CloseHandle(ProcInfo.hProcess);						
								CloseHandle(ProcInfo.hThread);

								FreeLibrary(hInst);
								
								return bRet;						
							}
						}
						LocalFree(pIntegritySid);
					}
					CloseHandle(hSeOwToken);
				}					
			}
		}		
		CloseHandle(hOwnerToken);		
		FreeLibrary(hInst);
	}
	return FALSE;
}

DWORD CCommonClass::FindProcessID(CString ProcessName)
{
	HANDLE			hProcessSnap = NULL; 
	BOOL			Return       = FALSE; 
    PROCESSENTRY32	pe32         = {0};
	DWORD dwProcID;

	ProcessName.MakeLower();

    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0); 

    if (hProcessSnap == INVALID_HANDLE_VALUE) 
	{
        return FALSE; 
	}
 
    pe32.dwSize = sizeof(PROCESSENTRY32);
	

    if (Process32First(hProcessSnap, &pe32))
    { 
		DWORD Code = 0;
        DWORD dwPriorityClass; 
       
        do 
        { 
            HANDLE hProcess; 

            // Get the actual priority class. 
            hProcess = OpenProcess (PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID); 
            dwPriorityClass = GetPriorityClass (hProcess); 
             
			CString Temp = pe32.szExeFile;
			Temp.MakeLower();

			if(Temp == ProcessName)
			{
				dwProcID = pe32.th32ProcessID;
				break;
			}
			CloseHandle(hProcess);
        } 
        while(Process32Next(hProcessSnap, &pe32));        
    }

    CloseHandle(hProcessSnap);

	return dwProcID;
}

BOOL CCommonClass::KillingProcess(CString strProc)
{
	strProc.MakeUpper();
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    if((int)hSnapshot != -1 )
    {
        PROCESSENTRY32 pe32;
        pe32.dwSize = sizeof(PROCESSENTRY32);
        BOOL bContinue;
        CString strProcessName;

        if(Process32First(hSnapshot, &pe32))
        {
            do
            {
                strProcessName = pe32.szExeFile; 
                strProcessName.MakeUpper();

                if((strProcessName.Find(strProc, 0) != -1))
                {
                    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, pe32.th32ProcessID);

                    if(hProcess)
                    {
                        DWORD dwExitCode;
                        GetExitCodeProcess(hProcess, &dwExitCode);
                        TerminateProcess(hProcess, dwExitCode);

                        CloseHandle(hProcess);
                        CloseHandle(hSnapshot);

						return TRUE;
                    }
//	                 return FALSE;
				}
                bContinue = Process32Next(hSnapshot, &pe32);
            }
			while(bContinue);
        }
        CloseHandle( hSnapshot );
    }
    return FALSE;
}

char * CCommonClass::strtok_r(char *s1, const char *s2, char **lasts)
{
 	char *ret;

	if (s1 == NULL)
		s1 = *lasts;

	while(*s1)
	{
		char * p = strchr(s2, *s1);
		if( !p )
			break;
		if(*s1 == *s2)
		{
			++s1;
			*lasts = s1;
			return "";
		}
		++s1;
	}

	if(*s1 == '\0')
		return NULL;

	if(*s1 == *s2)
		return "";

	ret = s1;
	while(*s1 && !strchr(s2, *s1))
		++s1;

	if(*s1)
		*s1++ = '\0';

	*lasts = s1;
  return ret;
}

//dev_choi	2010.03.22 string parsor
int CCommonClass::ParserString( CString target, CStringArray * pData, CString parser )
{
	int     j = 0;
	char    *pCh;

	CString strTarget;
	strTarget.Format("%s", target );

	char * p = strtok_r((char*)(LPCTSTR)strTarget, parser, &pCh);
	pData->Add( p );
	while(p != NULL)
	{
		++j;
		p = strtok_r(NULL,parser, &pCh);
		if( p )
		{
			if( strlen(p) > 0 )
				pData->Add( p );
		}
	}
	
	return (j);
}

// SE_CompileOption.h 참조
BOOL CCommonClass::IsSafePCOption( CString strOption )
{
	CString strData;
	CString strIniFile;
	TCHAR szData[MAX_PATH] = {0,};

	strIniFile.Format("%s\\SE_Options.ini", GetWindowsPath() );

	GetPrivateProfileString( _T("FUNCTION_OPTIONS"), _T("OPTION_LIST"), _T(""), szData, sizeof( szData ), strIniFile );

	strData.Format("%s", szData);

	if( strData.IsEmpty() )
		return FALSE;

	CStringArray aryOptins;
	ParserString( strData, &aryOptins, ",");

	for( int i =0 ; i < aryOptins.GetSize(); i++ )
	{
		CString str = aryOptins.GetAt(i);
		str.TrimLeft(); str.TrimRight();

		if( strOption.CompareNoCase(str) == 0 )
			return TRUE;
	}

	return FALSE;
}