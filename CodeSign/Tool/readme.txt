타임스탬프 URL
http://timestamp.verisign.com/scripts/timstamp.dll

패스워드
/logsaver432

OFFICESAFER Installer (service)
http://www.OfficeSafer.com
OFFICESAFER 외부복호화 파일

OFFICESAFER Installer (SILIOSS)
http://www.silioss.com

OFFICESAFER Installer (managed)
https://www-officesafer.skbroadband.com

OFFICESAFER Installer (XN)
https://pharmsecurity-www.skbroadband.com

OFFICESAFER Installer (GuardOne)
https://web.GuardOne.kr

GUARD1 DEFENDER Installer
http://web.guard1.kr
GUARD1 DEFENDER 외부복호화 파일

BizSafer Installer
http://www.BizSafer.co.kr
BizSafer 외부복호화 파일


From: 써트코리아 [mailto:support@certkorea.co.kr]
Sent: Thursday, June 25, 2015 3:15 PM
To: dsntech@dsntech.com
Subject: [써트코리아] Dsntech Inc. 의 인증서 발급이 완료되었습니다.




안녕하세요. 써트코리아 정주영입니다.

요청하신 인증서가 발급 완료 되었습니다.
인증서 파일 첨부하여 보내드립니다.
(인증서 패스워드는 /logsaver432 로 처리했습니다.)

인증서가 첨부되지 않은 경우에는 아래의 사이트로 접속 하셔서 다운 받으세요.

<인증서 상태조회>
http://www.certkorea.co.kr/html/my/my_login.asp

<인증서 설치 가이드>
http://www.certkorea.co.kr/html/tech/csrv_link.asp?link=codesignv_spc.asp

※ 인증서상에 오류가 있을경우 발급일로 부터 30일 안에 반드시 연락을 주셔야 처리가 가능합니다.




[ CAB 파일로 배포하시는 경우 ]
- cab 파일로 프로그램을 배포하시는 경우,
  cab 파일에 패키징 되는 ocx, dll, exe 파일을 먼저 개별 사이닝을 하셔야 합니다.
  각각의 파일을 개별 사이닝 하신후 cab 파일로 패키징 하시고,
  cab 파일을 다시 사이닝 하셔야 합니다.



다른 궁금한 점 있으시면 연락 주세요.

감사합니다.
