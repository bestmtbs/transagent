#include "StdAfx.h"
#include "Upgrade.h"
#include "IniFileEx.h"
//#include "FileEncrypt.h"
#include "Crypto/MD5.h"
#include "PathInfo.h"
#include "Crypto/Base64.h"
#include "UtilsUnicode.h"
#include "UtilsFile.h"
#include "TBAgentInfo.h"
#include "pipe.h"
#include "C_Update_S_Agent/PTOAPipeClient.h"
#include "UtilsProcess.h"
#include "WTSSession.h"
#include "FileIntegrity.h"

#ifndef _SETUP
#include "C_Update_S_Agent/PTOAPipeClient.h"
#endif

#include "ConnectParamInfo.h"
//#include "HttpsTConnect.h"

#include <yvals.h>

//#define STR_CMS_DB_SECTION						_T("DB Command")
//#define STR_DB_CMD_TXT							_T("DB_CMD.txt")
//
//#define STR_UPDATE_DOWN_FOLDER					_T("UDownload")
//#define STR_UPDATE_BACKUP_FOLDER				_T("UBackup")
//#define STR_CMSVER_INI							_T("CmsVer.ini")

#include <strsafe.h>	// 2016-02-16 kh.choi 내 프로세스 명 찍기. 파일이 여기 저기 쓰임





BOOL SetPrivilege(
        HANDLE hToken,   // 토큰 핸들
        LPCTSTR Privilege,   // 활성/비활성화할 권한
        BOOL bEnablePrivilege  // 권한 활성화 여부?
        )
    {
        TOKEN_PRIVILEGES tp;
        LUID luid;
        TOKEN_PRIVILEGES tpPrevious;
        DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);
 
        if (!LookupPrivilegeValue( NULL, Privilege, &luid ))
            return FALSE;
 
        // 현재의 권한 설정 얻기
        tp.PrivilegeCount   = 1;
        tp.Privileges[0].Luid   = luid;
        tp.Privileges[0].Attributes = 0;
 
        AdjustTokenPrivileges(
                hToken,
                FALSE,
                &tp,
                sizeof(TOKEN_PRIVILEGES),
                &tpPrevious,
                &cbPrevious
                );
 
        if (GetLastError() != ERROR_SUCCESS)
            return FALSE;
 
        // 이전의 권한 설정에 따라 권한 설정하기
        tpPrevious.PrivilegeCount       = 1;
        tpPrevious.Privileges[0].Luid   = luid;
 
        if (bEnablePrivilege) {
            tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
        }
        else {
            tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
                tpPrevious.Privileges[0].Attributes);
        }
 
        AdjustTokenPrivileges(
                hToken,
                FALSE,
                &tpPrevious,
                cbPrevious,
                NULL,
                NULL
                );
 
        if (GetLastError() != ERROR_SUCCESS)
            return FALSE;
 
        return TRUE;
    }





CUpgrade::CUpgrade(void)
{
	//m_nCurChange = 0;
	m_dKill = -1;
	m_bBackUp = false;
	m_sTargetVerion = _T("");
}

CUpgrade::~CUpgrade(void)
{
}



int CUpgrade::Start_Change(CString _strVersion)
{

	//다운 받은 파일 무결성 체크는 이미 다운로드 하면서 다 한거임
	m_bIntergrity = TRUE;


	CString strIni = CPathInfo::GetClientInstallPath()+STR_UPDATE_DOWN_FOLDER+_T("\\")+STR_CMSVER_INI;
		
	//1. INI 파일의 옵션값을 읽는다.  [OPTION] change= 1(즉시교체) 0=부팅후 교체 
	{		
		CIniFileEx	CmsVerIni(strIni);	
		//m_nCurChange = CmsVerIni.ReadInt(STR_CMS_VER_SECTION, STR_CMS_OPTION, 0);		
		m_sTargetVerion.Format(_T("%s"), CmsVerIni.ReadString(STR_CMS_VER_SECTION, STR_CMS_VER_SECTION, _T("")));	
		CmsVerIni.Clear();
	}

	CString strVersion = _strVersion;


	if( 0!=m_sTargetVerion.Compare(strVersion))
			m_bBackUp = true;


	//2. 파일 교체 
	m_nErr = FileUpgrade();
		
	if(UPDATE_SUCCESS!=m_nErr){		
		CString strLog;
		strLog.Format(_T("FileUpgrade  : %d "),m_nErr);
		//PatchLog(strLog);
		return m_nErr;

	}


	return UPDATE_SUCCESS;
}

int CUpgrade::Start_Change2()
{
	//if(2==m_nCurChange)
	m_dKill = KILL_ALL;
	
	CheckStatus();

	Kill_Module();

	m_bBackUp = true;
	Backup();

	//if(TRUE== m_bSetUp)
	//	Change_CurrentVersion();


	ForceDeleteDir(m_sDownRootPath);

	return UPDATE_SUCCESS;
}
 


int CUpgrade::Start_Integrity()
{
	CString strErr;
	CString strIniPath = _T("");
	 ///레지스트리 읽어서 무결성체크 하지 않게 하기
	DWORD nIntegrityCheck = 0;
    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,  _T("SOFTWARE\\dsntech\\TiorSaver"), 0, KEY_READ, &hKey))
    {
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);
		
		RegQueryValueEx(hKey, _T("change"), NULL, (LPDWORD)&value_type, (LPBYTE)&nIntegrityCheck, &value_length);
		RegCloseKey(hKey);

		if(0 != nIntegrityCheck)
			return UPDATE_SUCCESS;
	}


	{
		strIniPath = CPathInfo::GetClientInstallPath() + STR_CMSVER_INI;			
		CIniFileEx	CmsVerIni(strIniPath);
		CString strVersionFromIni = _T("");
		strVersionFromIni = CmsVerIni.ReadString(STR_CMS_VER_SECTION, STR_CMS_VER_SECTION, _T(""));
		m_sTargetVerion = strVersionFromIni;
		CmsVerIni.Clear();
	}

	FileIntegrity();
	Backup();
	CheckBackupFile(CPathInfo::GetClientInstallPath());	
	RedirectionOnOff(FALSE);
	CheckBackupFile(L"Driver");
	RedirectionOnOff(TRUE);
	//return nResult;
	return UPDATE_SUCCESS;
}


//int CUpgrade::Start_Integrity_Reload()
//{
//
//	CString strErr;
//
//	{
//		CString strLog = _T("");
//		strLog.Format(_T("[Upgrade] Start_Integrity_Reload Start. [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
//		UM_WRITE_LOG(strLog);
//	}
//
//	 ///레지스트리 읽어서 무결성체크 하지 않게 하기
//	DWORD nIntegrityCheck = 0;
//    HKEY hKey;
//    if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,  _T("SOFTWARE\\dsntech\\TiorSaver"), 0, KEY_READ, &hKey))
//    {
//		DWORD value_type = REG_DWORD;
//		DWORD value_length = sizeof(DWORD);
//		
//		RegQueryValueEx(hKey, _T("change"), NULL, (LPDWORD)&value_type, (LPBYTE)&nIntegrityCheck, &value_length);
//		RegCloseKey(hKey);
//
//		if(0 != nIntegrityCheck)
//			return UPDATE_SUCCESS;
//	}
//	m_bConnect = TRUE;
//
//	CString sInstallPath = CPathInfo::GetClientInstallPath();
//    CString sIniPath = sInstallPath+STR_CMSVER_INI;
//	{	
//		
//		CIniFileEx	CmsVerIni(CPathInfo::GetClientInstallPath()+STR_CMSVER_INI);			
//		m_sTargetVerion = CmsVerIni.ReadString(STR_CMS_VER_SECTION, STR_CMS_VER_SECTION, _T("") );	
//		CmsVerIni.Clear();
//	}
//
//	//m_sRemotePath = _T("/")+m_sTargetVerion+_T("/");
//
//
//	CTime cTime = CTime::GetCurrentTime();  
//	CString strTime =cTime.Format(_T("%Y-%m-%d %H:%M:%S")) ;
//
//	
//	int nResult = Check_IniIntegrity(sIniPath); //INI 파일 무결성 체크..
//	if(OFS_CHECK_SUCCESS != nResult ){
//
//		strErr.Format(_T("INI파일의 무결성이 올바르지 않습니다.(%d) %s"),nResult,sIniPath);	
//	
//
//		UM_WRITE_LOG(strErr);		
//	
//
//		//if(FALSE == DownLoadFile(m_sRemotePath+STR_CMSVER_INI,sIniPath) ){
//		//	WriteIntegrityLog(0,0,0,STR_CODE_INTEGRITY_CHECK_FILE,0,strErr,strTime);	
//		//	return UPDATE_ERROR_INI_EXIST;
//		//}
//
//		if(FALSE == SetIniHash()){
//			WriteIntegrityLog(0,0,0,STR_CODE_INTEGRITY_CHECK_FILE,0,strErr,strTime);		
//			return UPDATE_ERROR_INI_EXIST;		
//		}
//	}
//
//
//
//	//CString strVersion = GetCurVersion();
//	//if( 0!=m_sTargetVerion.Compare(strVersion)){
//	//	Set_UpdateVersion();
//	//}
//
//	//Change_CurrentVersion();	
//	CString sErr;
//	CString strBackupPath = CPathInfo::GetClientInstallPath()+STR_UPDATE_BACKUP_FOLDER+_T("\\");		
//	CString strBackupPathApp = strBackupPath;
////	CString strBackupPathDrv = strBackupPath+m_strDrvSection+_T("\\");
//
//	CString sKeyName = L"OfsReload.exe";
//
//	CString sHash;
//	
//
//	//Reload
//	if(UPDATE_SUCCESS != Check_File_Status(sIniPath,sKeyName,sInstallPath,_T(""),sHash,sErr) && 0< sHash.GetLength()){
//		
//			//CString strPath =  sInstallPath +sKeyName;
//			//iPos = strPath.ReverseFind( _T('\\') );
//		//	strPath = strPath.Left( iPos);	
//			
//		//	if(  FALSE == FileExists(strPath))
//		//	   ForceCreateDir(strPath);
//			BOOL bCopy = FALSE;
//			CString strBackupAppPath  = strBackupPathApp+sKeyName;
//
//			if(TRUE == FileExists(strBackupAppPath)){
//				//1. 백업폴더에서 파일 찾기
//				//2. 해쉬값 비교
//				//strBackupAppPath = TargetFolder+strDownFileName;
//				if(OFS_CHECK_SUCCESS ==Check_FileIntegrity(strBackupAppPath,sHash)){
//					bCopy = ChangeFile(sInstallPath,strBackupPathApp,sKeyName,FALSE);
//				}
//			}
//			//2. 없으면 다운로드 
//			if(FALSE == bCopy && TRUE == m_bConnect ){
//				CString strReomteFile = m_sRemotePath+sKeyName.GetString();
//				CString strDowFullnPath = m_sDownRootPath+sKeyName.GetString();
//
//				strReomteFile.Replace('\\','/');
//				if(TRUE == DownLoadFile(strReomteFile, strDowFullnPath)){
//					if(OFS_CHECK_SUCCESS ==Check_FileIntegrity(strDowFullnPath,sHash))
//						bCopy = ChangeFile(sInstallPath,m_sDownRootPath,sKeyName);
//				}				
//			}
//
//			if(FALSE == bCopy)
//				//무결성 체크 에러
//				return UPDATE_ERROR_INTEGRIY_CHANGE;
//		
//
//	}
//
//
//
//	return UPDATE_SUCCESS;
//
//
//}



int CUpgrade::FileUpgrade()
{
	//STR_UPDATE_DOWN_FOLDER 폴더가 있는경우 파일들의 무결성을 체크
	if(FALSE == PathFileExists(m_sDownRootPath))
		return UPDATE_SUCCESS;


	//MakeDBcommand();

	//CMSVer.ini 파일을 복사한다.
	if(FALSE == ChangeFile(CPathInfo::GetClientInstallPath(),m_sDownRootPath,STR_CMSVER_INI))
		return UPDATE_ERROR_VERSION_INI;

	//SetIniHash();

	if(m_sTargetVerion.GetLength()<1)
		return UPDATE_ERROR_INI_VERSION;
	
	//파일 교체
	if(FALSE == UpgradePath(m_sDownRootPath,CPathInfo::GetClientInstallPath()/*,false*/))
		return UPDATE_ERROR_CHANGE_APP;
	//if(UPDATE_SUCCESS != Set_UpdateVersion())
	//	return UPDATE_ERROR_DB;
		
	//STR_UPDATE_DOWN_FOLDER 폴더 삭제
	ForceDeleteDir(m_sDownRootPath);	

	return UPDATE_SUCCESS;
}







void CUpgrade::CheckStatus()
{

	CString strVersion = /*GetCurVersion()*/_T("");


	if( 0 != m_sTargetVerion.Compare(strVersion)){
		
		 HKEY hKey;
		if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE,  _T("SOFTWARE\\dsntech\\TiorSaver"), 0, KEY_ALL_ACCESS, &hKey))
		{
			DWORD value_type = 0;
			DWORD value_length = sizeof(DWORD);			
			RegSetValueEx(hKey, _T("change"), NULL, REG_DWORD, (LPBYTE)&value_type, value_length);
			RegCloseKey(hKey);		

		}

	}


	



}

//void CUpgrade::MakeDBcommand()
//{
//	int i = 1;
//	CString strValue, strKeyName, DBTxtPath, strWriteData = _T("");
//	CFile	DbCmd_file;
//	CIniFileEx	CmsVerIni(m_sTargetVerion);
//	USHORT nShort = 0xfeff;  // 유니코드 바이트 오더마크.
//
//	while(TRUE)
//	{	
//		strKeyName.Format( _T("DB_%d"), i);
//		strValue = CmsVerIni.ReadString(STR_CMS_DB_SECTION,  strKeyName, _T("") );
//		if( strValue.IsEmpty() ) break;
//			
//		strWriteData += strValue;
//		strWriteData += _T("\r\n");		
//		i++;
//	}
//	
//	if( FALSE == strWriteData.IsEmpty() )
//	{
//		DBTxtPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(),  STR_DB_CMD_TXT);
//		if( !DbCmd_file.Open(DBTxtPath, CFile::modeCreate | CFile::modeWrite) )
//		{
//			CString sLog;
//			sLog.Format( _T("MakeDBcommna File open fail : %d"), GetLastError() );
//			UM_WRITE_LOG(sLog);
//			return;	
//		}
//
//		DbCmd_file.Write(&nShort,2);  	
//		DbCmd_file.Write(strWriteData, strWriteData.GetLength() * 2);
//		DbCmd_file.Close();	
//	}
//}

//BOOL CUpgrade::SetIniHash()
//{
//
//	BOOL bResult = FALSE;
//
//	int nCnt = 0;
//
//		CString	strCurrentMd_Hash;		
//		//strCurrentMd_Hash =	GetFileMD5(CPathInfo::GetClientInstallPath()+STR_CMSVER_INI);
//		strCurrentMd_Hash =	GetFileHash_MD5(CPathInfo::GetClientInstallPath()+STR_CMSVER_INI);
//		CString strQuery;
//		strQuery.Format(_T("update %s set etc_1 = '%s'"), DB_AGENT_INFO, strCurrentMd_Hash);
//		// 2017-09-06 sy.choi TODO
//		//if(TRUE == SetDBInfo(strQuery)){
//		//	bResult = TRUE;			
//		//}		
//
//	return bResult;
//}
//
//
//BOOL 	CUpgrade::SetDBInfo(CString _strQuery)
//{
//	BOOL bSuccess = FALSE;
//
//	int nCnt = 0;
//	
//	do{
//		CDBConnect* pDB  = new  CTBAgentInfo();
//
//		UM_WRITE_LOG(_strQuery);
//		
//		if(TRUE == pDB->QueryUpdate((TCHAR*)_strQuery.GetString()))	{
//			bSuccess = TRUE;
//			//Sleep(1000);		
//			//if(TRUE == pDB->QueryUpdate((TCHAR*)_strQuery.GetString()))
//			//{
//			//	//UM_WRITE_LOG(_T("UpdateQuery Fail.."));
//			//	bSuccess = TRUE;
//			//}
//		}
//
//		SE_MemoryDelete(pDB);
//
//		if(TRUE ==  bSuccess)
//			break;
//		nCnt++;
//
//		Sleep(3000);		
//	}while(nCnt<10);
//
//	return bSuccess;

//}




BOOL CUpgrade::UpgradePath(CString _strSrcPath,CString _srcDstPath/*,bool bDrv*/)
{


	BOOL bRst = TRUE;

	//if(false == bDrv)
	ForceCreateDir(_srcDstPath);

	CString		strFindPath;
	strFindPath.Format( _T("%s*.*"),_strSrcPath);

	int nRoot = CPathInfo::GetClientInstallPath().GetLength();
	int nPath = _srcDstPath.GetLength();

	CFileFind finder;
	BOOL working = finder.FindFile( strFindPath );
	
		
	while ( working )
	{
		working = finder.FindNextFile();
		if ( finder.IsDots() ) continue;

		CString  strName = finder.GetFileName();
  
		if( finder.IsDirectory())	//파일
		{
			/*if(FALSE == bDrv)*/{
				CString  strDst = _srcDstPath+strName ;
				strDst +=_T("\\");

				CString  strSrc = _strSrcPath+strName ;
				strSrc +=_T("\\");

				if(FALSE == UpgradePath(strSrc,strDst/*,bDrv*/)){
					bRst = FALSE;
					break;
				}
			}		
		}//if( !finder.IsDirectory())	//파일
		else
		{

			if(/*FALSE == bDrv && */nRoot == nPath ){
				//m_strChangeList.Add(strName); //프로세스 종료시 필요
				CheckChangeModule(strName);
			}

			//if(TRUE == bDrv){
			//	if(0== strName.Compare(STR_DRV_NF)){
			//		CString sCheckFile = _srcDstPath+STR_DRV_NF_RENAME;
			//		if(FileExists(sCheckFile))
			//			continue;		
			//	
			//	}			
			//}

			if(FALSE ==ChangeFile(_srcDstPath,_strSrcPath,strName)){
				bRst = FALSE;
					break;
			}
			

		}
	}//while ( working )
	finder.Close();


	return bRst;
}
//
//CString		CUpgrade::GetDBInfo(CString _strQuery,CString _StrField)
//{
//	CDBConnect* pDB  = new  CTBAgentInfo();
//	CString strQuery = _T(""), strResult = _T("");
//
//	CStringArray arrData;
//	arrData.RemoveAll();
//
//	pDB->QuerySelect((TCHAR*)_strQuery.GetString(), &arrData,(TCHAR*)_StrField.GetString());
//
//	if( arrData.GetSize() > 0 )
//	{
//		strResult = arrData.GetAt(0);
//	}
//
//	SE_MemoryDelete(pDB);
//	return strResult;
//}






void CUpgrade::CheckChangeModule(CString strName)
{

	if(KILL_ALL == m_dKill)
		return;

	if(-1< strName.Find(WTIOR_SYS_AGENT_32_NAME))	
	  	m_dKill |= KILL_COLLECT_AGENT;
	else if(-1< strName.Find(WTIOR_USR_AGENT_32_NAME))	
		m_dKill |= KILL_COLLECT_AGENT;
	//else if(-1< strName.Find(WTIOR_SYS_AGENT_32_RE_NAME))	
	//  	m_dKill |= KILL_COLLECT_AGENT;
	//else if(-1< strName.Find(WTIOR_USR_AGENT_32_RE_NAME))	
	//  	m_dKill |= KILL_COLLECT_AGENT;
	else if(-1< strName.Find(WTIOR_SYS_AGENT_64_NAME))	
	  	m_dKill |= KILL_COLLECT_AGENT;
	else if(-1< strName.Find(WTIOR_USR_AGENT_64_NAME))	
	  	m_dKill |= KILL_COLLECT_AGENT;
	//else if(-1< strName.Find(WTIOR_SYS_AGENT_64_RE_NAME))	
	//  	m_dKill |= KILL_COLLECT_AGENT;
	//else if(-1< strName.Find(WTIOR_USR_AGENT_64_RE_NAME))	
	//  	m_dKill |= KILL_COLLECT_AGENT;
	else if(-1< strName.Find(WTIOR_TRANS_AGENT_NAME))	
		m_dKill |= KILL_TRANS_AGENT_NAME;
	else if(-1< strName.Find(WTIOR_UPDATE_AGENT_NAME))	
		m_dKill |= KILL_UPDATE_AGENT_NAME;
	else if(-1< strName.Find(WTIOR_DOWNLOAD_AGENT_NAME))	
		m_dKill |= KILL_DOWNLOAD_AGENT_NAME;
	else if(-1< strName.Find(WTIOR_UNINSTALL_AGENT_NAME))	
		m_dKill |= KILL_UNINSTALL_AGENT_NAME;
}

int CUpgrade::Kill_Module()
{
	//if(/*0< m_nCurChange && */-1 != m_dKill ){

		//Change_CurrentVersion();

	HANDLE hToken;
	OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
	SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);

	//프로세스 종료 메세지 날리고 1분만 대기
	//if((m_dKill & KILL_RELOAD )|| (m_dKill == KILL_ALL)){
		//if(TRUE == CProcess::IsExistMutex(WTIOR_RELOAD_AGENT_MUTEX_NAME)){
		if(FALSE == CProcess::KillProcess( CPathInfo::GetClientInstallPath() + WTIOR_AGENT_SERVICE_NAME)) {
			UM_WRITE_LOG(_T("Kill Reload Fail"));
		}
	SetPrivilege(hToken, SE_DEBUG_NAME, FALSE);
	CloseHandle(hToken);
	//}
	return UPDATE_SUCCESS;
}

//int CUpgrade::Change_CurrentVersion()
//{
//
//	CString strQuery = _T("") ;
//	strQuery.Format(_T("update %s set version = updateVersion"), DB_AGENT_INFO);
//
//	int nCnt =0;
//	BOOL bSuccess = FALSE;
//
//	do{
//
//		 bSuccess = SetDBInfo(strQuery);
//		if(TRUE == bSuccess)
//			break;
//
//		Sleep(1000);
//		nCnt++;
//	}while(nCnt < 10);
//
//
//	return bSuccess? UPDATE_SUCCESS: -1;
//
//}

//int CUpgrade::Set_UpdateVersion()
//{
//
//	CString strQuery;
//	strQuery.Format(_T("update %s set updateVersion = '%s' , updateTime = datetime('now', 'localtime')"), DB_AGENT_INFO, m_sTargetVerion);
//
//	int nCnt =0;
//	BOOL bSuccess = FALSE;
//
//	do{
//
//		 bSuccess = SetDBInfo(strQuery);
//
//		if(TRUE == bSuccess)
//			break;
//
//		Sleep(1000);
//		nCnt++;
//	}while(nCnt < 10);
//
//
//	
//	return bSuccess? UPDATE_SUCCESS: -1;
//}



int CUpgrade::FileIntegrity()
{

	int nRst = UPDATE_SUCCESS;


	//m_sTargetVerion = GetCurVersion();

	CString strIni = _T("");
	CString strBackupPath = _T("");		
	CString strBackupPathApp = strBackupPath;
	//m_sRemotePath = _T("/")+m_sTargetVerion+_T("/");

	strIni.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), STR_CMSVER_INI);
	strBackupPath.Format(_T("%s%s\\"), CPathInfo::GetClientInstallPath(), STR_UPDATE_BACKUP_FOLDER);
	CString strErrTemp = _T("");
	m_bConnect = FALSE;
	m_bConnect = TRUE;
	
	CTime cTime = CTime::GetCurrentTime();  
	CString strTime =cTime.Format(_T("%Y-%m-%d %H:%M:%S")) ;

	CString strErr;

	int nSuccess = 1;
	m_List.RemoveAll();

	nRst = IntegrityCheckFile(strIni,m_TargetVersion,CPathInfo::GetClientInstallPath(),strBackupPath,strErrTemp);
	if(UPDATE_SUCCESS != nRst){
		strErr += strErrTemp;
		{
			CString strLog = _T("");
			strLog.Format(_T("[Upgrade] %s [line: %d, function : %s, file: %s]"), strErr, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
		WriteIntegrityLog(0,0,0,STR_CODE_INTEGRITY_CHECK_FILE,0,strErr,strTime);		
		return UPDATE_ERROR_INTEGRIY_CHANGE_APP;
	}

	if(0< m_List.GetCount()){
		nSuccess = 0;
		strErr += strErrTemp;
	}


	m_List.RemoveAll();
	//RedirectionOnOff(FALSE);
	//nRst = IntegrityCheckFile(strIni,m_strDrvSection,CPathInfo::GetSystem32() +_T("drivers\\"),strBackupPath,strErrTemp);
	//RedirectionOnOff(TRUE);
	if(UPDATE_SUCCESS != nRst){
		strErr += strErrTemp;
		WriteIntegrityLog(0,0,0,STR_CODE_INTEGRITY_CHECK_FILE,0,strErr,strTime);
		return UPDATE_ERROR_INTEGRIY_CHANGE_DRV;
	}

	if( 0< m_List.GetCount()){
		nSuccess = 0;
		strErr += strErrTemp;
	}

	if( 1 == nSuccess){
		WriteIntegrityLog(0,0,0,STR_CODE_INTEGRITY_CHECK_FILE,1,_T(""),strTime);
	}
	else
		WriteIntegrityLog(0,0,0,STR_CODE_INTEGRITY_CHECK_FILE,0,strErr,strTime);

	return nRst;
}


int CUpgrade::IntegrityCheckFile(CString strIni, CString strSesstion,CString strTargetFolder,CString strBackUpFolder,CString& _strErr)
{

	int nRst = UPDATE_SUCCESS;
	//무결성체크

	m_List.RemoveAll();
	FILE_LIST mIniList;	
	CStringArray saList;
	CString strErr;
	CString strReport;

//	FillTheUpdateList(strIni , strSesstion,FALSE,TRUE);

	// 2016-02-16 kh.choi 내 프로세스 명 찍기. 파일이 여기 저기 쓰임
	{
		WCHAR			wcModuleName[MAX_PATH] = {0};
		WCHAR			*pwcModuleName=NULL;
		WCHAR			strProcess[MAX_PATH] = {0};
		char			strProcessA[MAX_PATH] = {0};

		memset( wcModuleName, 0x00, sizeof( wcModuleName ) );
		::GetModuleFileNameW(NULL, wcModuleName, sizeof(wcModuleName));
		if(lstrlen(wcModuleName) > 0)
		{
			pwcModuleName = (WCHAR*)wcsrchr( wcModuleName, L'\\' );
			pwcModuleName++;
			WCHAR *pEnd = wcsstr(pwcModuleName, L".exe");
			*pEnd = L'\0';
			StringCchCopy(strProcess, MAX_PATH, pwcModuleName);

			ULONG cbAnsi=0, cCharacters = MAX_PATH;
			int nRet = MAX_PATH;
			// Convert to ANSI.
			cCharacters = MAX_PATH;
			WideCharToMultiByte(CP_ACP, 0, strProcess, cCharacters, strProcessA, nRet, NULL, NULL);
			//return TRUE;
			CString strLog = _T("");
			strLog.Format(_T("[Upgrade] IntegrityCheckFile Start. process name: %s [line: %d, function: %s, file: %s]"), strProcess, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		} else {
			//return FALSE;
			CString strLog = _T("");
			strLog.Format(_T("[Upgrade] IntegrityCheckFile Start. [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
	}

	int nResult = Get_FileList(mIniList,strIni,strErr);
	if(OFS_CHECK_SUCCESS != nResult ){
		strReport.Format(_T("INI의 %s섹션의 값이 올바르지 않습니다.[%d](%s)"),strSesstion,nResult,strIni);
		_strErr +=strReport;
		_strErr +=strErr;
	
		return nResult;
	}

	//nResult = Check_Integrity_List(mIniList,saList,strTargetFolder,FALSE);
	nResult = Check_Integrity_List(mIniList,saList,strTargetFolder,TRUE);	// 2016-02-15 kh.choi 매번 하위폴더까지 무결성 검사하도록 수정

	if(OFS_CHECK_SUCCESS != nResult ){
		strErr.Format(_T("INI의 %s섹션의 무결성 위반 항목을 발견하였습니다."),strSesstion);
		_strErr += strErr;


		//TODO 여기서 위반항목을 DB에 작성 
		//UM_WRITE_LOG(strErr);
		{
			CString strLog = _T("");
			strLog.Format(_T("[Upgrade] strErr: %s [line: %d, function : %s, file: %s]"), strErr, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
	//	nResult += strErr;
		
		int nCnt  = saList.GetCount();
		if(0< nCnt){
			int nIdx=0;
			while(nIdx< nCnt){
				//strReport.Format(_T("파일무결성 위반 : %s (%d)"),saList.GetAt(nCnt),nResult);	
				strReport = saList.GetAt(nIdx);
				_strErr += strReport;
			//	WriteIntegrityLog(0,0,STR_TITLE_INTEGRITY_CHECK_FILE,STR_CODE_INTEGRITY_CHECK_FILE,0,strReport,_strTime);
				nIdx++;
			}
		}	


		Add_Download_List(mIniList,saList,strSesstion+_T("\\"));

	}

	if(0<m_List.GetCount()){
	
		CString strBackupAppPath;		
		CString strBackup;		
		POSITION pos =   m_List.GetStartPosition(); 
	
		while(NULL != pos){
			CAtlString strDownFileName;
			CAtlString strValue;
			BOOL bCopy = FALSE;

			m_List.GetNextAssoc(pos, strDownFileName, strValue); 

			CString strFileName = strDownFileName.GetString();
			CString strSub = _T("");

			int iPos = 0;
			iPos = strFileName.Find(_T('\\'));
			if(0<iPos){
				strSub = strFileName.Left(iPos+1);
				strFileName = strFileName.Mid(iPos+1);

			}

			CString strPath =  strTargetFolder + ExtractFileName(strFileName);
			iPos = strPath.ReverseFind( _T('\\') );
			strPath = strPath.Left( iPos);				
			if(  FALSE == FileExists(strPath))
			   ForceCreateDir(strPath);

			strBackupAppPath  = strBackUpFolder+ExtractFileName(strFileName);

			if(TRUE == FileExists(strBackupAppPath)){
				//1. 백업폴더에서 파일 찾기
				//2. 해쉬값 비교
				//strBackupAppPath = TargetFolder+strDownFileName;
				if(OFS_CHECK_SUCCESS ==Check_FileIntegrity(strBackupAppPath,strValue)){
					bCopy = ChangeFile(strTargetFolder,strBackUpFolder+strSub,ExtractFileName(strFileName),FALSE);
				}
			}
			//2. 없으면 다운로드 
			if(FALSE == bCopy && TRUE == m_bConnect ){
				CString strReomteFile = strFileName;
				CString strDowFullnPath = m_sDownRootPath + ExtractFileName(strFileName);

				strReomteFile.Remove('\\');

				{
					//CString strLog = _T("");
					//strLog.Format(_T("[Upgrade] DownLoadFile(%s, %s) [line: %d, function : %s, file: %s]"), strReomteFile, strDowFullnPath, __LINE__, __FUNCTIONW__, __FILEW__);
					//UM_WRITE_LOG(strLog);
				}

				if(TRUE == DownLoadFile(strReomteFile, strDowFullnPath)){
					if (OFS_CHECK_SUCCESS ==Check_FileIntegrity(strDowFullnPath,strValue)) {
						{
							//CString strLog = _T("");
							//strLog.Format(_T("[Upgrade] ChangeFile(%s, %s, %s) [line: %d, function : %s, file: %s]"), strTargetFolder, m_sDownRootPath + strSub, strFileName, __LINE__, __FUNCTIONW__, __FILEW__);
							//UM_WRITE_LOG(strLog);
						}
						bCopy = ChangeFile(strTargetFolder,m_sDownRootPath+strSub,ExtractFileName(strFileName));
					}
				}				
			}
			if(FALSE == bCopy){
				//무결성 체크 에러
				nRst = UPDATE_ERROR_INTEGRIY_CHANGE;
				break;
			}else
				m_bBackUp=true;
		}
	}
	return nRst;
}


int	 CUpgrade::Backup()
{

	if(false == m_bBackUp)
		return 0;
		

	CString strRoot = CPathInfo::GetClientInstallPath();
	CString strIni = strRoot+STR_CMSVER_INI;	
	CString strBackupPath = strRoot+STR_UPDATE_BACKUP_FOLDER+_T("\\");		
	CString strBackupPathApp = strBackupPath;

	ForceDeleteDir(strBackupPath);	
	ForceCreateDir(strBackupPathApp);

	//INI파일 복사 
	ChangeFile(strBackupPath,strRoot,STR_CMSVER_INI,FALSE);

	CStringList listApp_AllKeys;
	CIniFileEx	CmsVerIni(strIni);		
	CmsVerIni.ReadSections(listApp_AllKeys);
	int nCnt = listApp_AllKeys.GetCount();

	if(nCnt<1)
		return OFS_CHECK_INI_SECTION;

	POSITION pos = listApp_AllKeys.GetHeadPosition();
	listApp_AllKeys.GetNext(pos);

	while(pos != NULL)
	{
		CString strFileName = listApp_AllKeys.GetNext(pos);
		CString strPath =  strBackupPathApp +strFileName;
		int iPos = strPath.ReverseFind( _T('\\') );
		strPath = strPath.Left( iPos);				
		if(  FALSE == FileExists(strPath))
		   ForceCreateDir(strPath);
		ChangeFile(strBackupPathApp,strRoot,strFileName,FALSE);
		Sleep(5);
	}

	listApp_AllKeys.RemoveAll();
	

	return UPDATE_SUCCESS;
}





void CUpgrade::CheckBackupFile(CString _strSection,bool bSub )
{

	//DWORD		dwMoveFileFlag;
	//BOOL			bIsDrvFile= FALSE;
	CString		strRootPath,strFindPath,  strDstPath, strSrcPath, strSrcFileFullpath;
	
	//
	//if(_strSection == _T("Driver"))																		
	//{
	//	dwMoveFileFlag = MOVEFILE_REPLACE_EXISTING;													//드라이버 모듈은 바로 변경하는 옵션으로
	//	strRootPath.Format( _T("%s\\%s\\"), CPathInfo::GetSystem32() , _T("drivers"));		
	//	bIsDrvFile = TRUE;	
	//}
	//else			
	{
		strRootPath = _strSection;	
	}

	//if(TRUE == bIsDrvFile)
	//	strFindPath.Format( _T("%s%s*.*"),strRootPath,STR_FILE_BACKUP);
	//else
		strFindPath.Format( _T("%s*.*"),strRootPath);


	UM_WRITE_LOG(strFindPath);

	
	CFileFind finder;
	BOOL working = finder.FindFile( strFindPath );

	while ( working )
	{
		working = finder.FindNextFile();
		if ( finder.IsDots() ) continue;
  
		if( !finder.IsDirectory())	//파일
		{
			try
			{
				if(0== finder.GetFileName().Find(STR_FILE_BACKUP)){
					CString File  = strRootPath +finder.GetFileName();								
					DeleteFile(File);
					UM_WRITE_LOG(File);
				}
			
			}
			catch(CFileException* pEx )
			{
				if (pEx->m_cause == CFileException::fileNotFound)
					UM_WRITE_LOG(_T("ERROR: File not found\n"));
				pEx->Delete();
			}
		}//if( !finder.IsDirectory())	//파일
		else
		{
			CString strSubPath = strRootPath+finder.GetFileName()+_T("\\");
			CheckBackupFile(strSubPath,true);
		}
	}//while ( working )
	finder.Close();

}