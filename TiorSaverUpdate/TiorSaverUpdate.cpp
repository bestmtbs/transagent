// TiorSaverUpdate.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "TiorSaverUpdate.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#include "WTSSession.h"
#include "pipe/C_Update_S_Agent/PTOAPipeClient.h"
#include "UtilsProcess.h"
#include "Upgrade.h"
#include "MiniDumpHelp.h"

// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;
//
//VOID closeEzLog(void)
//{
//	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
//	SingletonForEzLog::DeleteSingleton();
//	CloseLogFile();
//}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;
	CString strVersion = _T("");
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd,SW_HIDE);

	MinidumpHelp MiniDump;
	MiniDump.install_self_mini_dump();

	// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
		nRetCode = 1;
	}
	else
	{
		// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
		//SingletonForEzLog::SetStrLogFilenamePrefix(_T("TiorSaverUpdate"));	// 2017-01-20 kh.choi 파일로그 파일명 prefix 등록. [FileLogSet]
 
		//뮤텍스 

//		CPatch  download;
		CUpgrade  download;
		
		DWORD dwReturnValue = 0;
		int nKill = -1;

		LPWSTR* pStr = NULL;
		int iCnt;
		pStr = CommandLineToArgvW( GetCommandLine(), &iCnt);
		CString strMode = _T("");
		int  nType =0;		
		if( 2== iCnt ){
			 strMode  =  pStr[1];	
		}
		if( 3== iCnt ){
			strMode  =  pStr[1];			 
			strVersion  =  pStr[2];			 
		}

		LocalFree(pStr);

		if (2!= iCnt  && 3!= iCnt) {
			//closeEzLog();	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
			return -1;
		}

				

		if(strMode.MakeLower() == _T("-c") )
			nType = 2;
		else if(strMode.MakeLower() == _T("-u") ) {
			nType = 0;
		}
		else if(strMode.MakeLower() == _T("-t") )
			nKill = _ttoi(strVersion);
		else if(strMode.MakeLower() == _T("-a") )
			nType = 1;
		else {
			//closeEzLog();	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
			return -1;
		}

		if( CProcess::ProcessCreateMutex(WTIOR_UPDATE_AGENT_MUTEX_NAME) == false )
		{
			UM_WRITE_LOG(_T("Already Start CmsUpdate.exe..")) 
			UM_WRITE_LOG(_T("업데이트 모듈이 이미 동작중입니다.."));
			//closeEzLog();	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
			exit(0);
		}
		g_cDbgLog.SetLogFileName(L"TiorSaverUpdate.log");
		g_cDbgLog.InitLog();
		int nRst;
		if(-1< nKill){
			return 0;
		}
		else{
			if( 0 == nType){
				nRst = download.Start_Change(strVersion);
				SHARE_DATA* pShareData = new SHARE_DATA;
				CPTOAPipeClient PTOAPipeClient(PTOA_PIPE_NAME);
				if (UPDATE_SUCCESS == nRst) {
					pShareData->dwAct = ACT_UPDATE_COMPLETE_RESTART;
					pShareData->dwMsg = download.m_dKill;
					pShareData->dwBufferSize = strVersion.GetLength() * sizeof(TCHAR);
					memcpy(pShareData->szBuffer, strVersion.GetBuffer(0), pShareData->dwBufferSize);
				} else {
					pShareData->dwAct = ACT_AGENT_UPDATE_FAIL;
				}
				if (TRUE == PTOAPipeClient.OnSetSendData(pShareData)) {
					if (FALSE == PTOAPipeClient.PipeClientStartUp()) {
						//return FALSE;
					} /*else
						return TRUE;*/
				} else {
					//return FALSE;
				}
			} else if(1 == nType){
				nRst=	download.Start_Integrity();	
			}
		}

		if(0 == nType){
			Sleep(2000);
			download.Start_Change2();		
		}
	}

	return nRetCode;
}
