#pragma once

#include "../TiorSaverDownload/Download.h"


#define STR_DRV_NF						_T("dsntechnf.sys")
#define STR_DRV_NF_RENAME				_T("dsntechnf_n.sys")
class CUpgrade:public CDownload
{
public:
	CUpgrade(void);
	~CUpgrade(void);

	int Start_Change(CString _strVersion);
	int Start_Change2();
	int Start_Integrity();
	//int Start_Integrity_Reload();

	

	//int  m_nCurChange;
	BOOL m_bIntergrity;
	int		m_dKill;
private:

	int Kill_Module();
	int FileUpgrade();
	//int KillModule();
	BOOL SetIniHash();
	//BOOL SetDBInfo(CString _strQuery);
	
	BOOL UpgradePath(CString _strSrcPath,CString _srcDstPath/*,bool bDrv*/);
	
	//CString	GetDBInfo(CString _strQuery,CString _StrField);
	void CheckChangeModule(CString strName);
	void CheckBackupFile(CString _strSection,bool bSub = FALSE );

	int Change_CurrentVersion();
	int Set_UpdateVersion();
//	BOOL SetIniHash();

	int	FileIntegrity();
	int Backup();
	int IntegrityCheckFile(CString strIni, CString strSesstion,CString strTargetFolder,CString strBackUpFolder, CString& _strErr);

	void MakeDBcommand();

private:

	CString m_sTargetVerion;
	bool  m_bBackUp;

	void 		CheckStatus();

};
