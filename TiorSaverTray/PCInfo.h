#pragma once

#pragma comment(lib, "Iphlpapi.lib")			// For IP Address, MAC Address
#pragma comment(lib, "Netapi32.lib")			// For NetBios
#include <Nb30.h>
#include "wbemcli.h"     // WMI interface declarations
typedef struct _ASTAT_
{
	ADAPTER_STATUS adapt;
	NAME_BUFFER    NameBuff [30];
}ASTAT, * PASTAT;

class CPCInfo
{
public:
	CPCInfo(void);
	virtual ~CPCInfo(void);

	static void SetDefaultIPAddress(CString& _strIPAddress);
	static void SetDefaultMacAddress(CString& _strMacAddress);
	static BOOL GetRealIPMAC(CString& _strIPAddress, CString& _strMacAddress);
	static void GetPCName(CString& _strPCName);
	static void GetPCID(CString& _strPCID);
	static void GetOSName(CString& _strOSName);
	static void GetCpuName(CString& _strCpuName);
	static int   GetRamSize();
	static void GetWorkgroup(CString& _strGroup);
	//BOOL GetDiskVolume(CStringArray* _pArrDevice);
	CString GetDeviceSerial(CString strVolume);

private:
	CString m_strNamespace;
	IWbemServices *m_pIWbemServices;
	
	BOOL Connect();

};
