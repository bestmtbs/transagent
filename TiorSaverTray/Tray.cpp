
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      Tray.cpp 
 @brief     트레이 아이콘 기능 구현 파일
 @author    odkwon 
 @date      create 2011.05.20 
*/

#include "StdAfx.h"
#include "Tray.h"

/********************************************************************************
 @class     CTray
 @brief     CTray Class

            트레이 아이콘 기능을 클래스화하여 제사용,적용하기 쉽게한다.
*********************************************************************************/


BEGIN_MESSAGE_MAP(CTrayMsgRecevier, CWnd)
    ON_MESSAGE(WM_TRAY_NOTIFY, OnTrayNotification)
END_MESSAGE_MAP()


/**
 @brief     트레이 아이콘 등록하기
 @author    odkwon
 @date      2008-05-15
 @param     [in]_pTray       [in]트레이아이콘 참조 포인터
 @return    true/false
 */
bool CTrayMsgRecevier::CreateWnd(CTray *_pTray)
{
    if (_pTray == NULL) return false;
    bool bCreate = false;

     LPCTSTR pstrOwnerClass = ::AfxRegisterWndClass(0);    
     bCreate = (TRUE == CWnd::CreateEx(0, pstrOwnerClass, _T(""), WS_POPUP, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, NULL, 0));

    m_nShellRestart  = ::RegisterWindowMessage(_T("TaskbarCreated"));
    m_pTray = _pTray;
    
    return bCreate;
}

CTrayMsgRecevier::CTrayMsgRecevier()
{
    m_pTray = NULL;
	m_nShellRestart = 0;
}

/**
 @brief     CTrayMsgRecevier파괴자 생성했던 윈도우를 파괴한다.
 @author    odkwon
 @date      2008-05-20
 @return 
 */
CTrayMsgRecevier::~CTrayMsgRecevier()
{
     CWnd::DestroyWindow();
}


/**
 @brief     윈도우 프로시저 함수 재정의 explorer 제시작시 메세지를 처리한다.
 @author    odkwon
 @date      2008-05-15
 @param     [in]message 메세지 ID
 @param     [in]wParam  wParam
 @param     [in]lParam  lParam
 @return    true/false
 */
LRESULT CTrayMsgRecevier::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
   
	if (message == m_nShellRestart) {
		if (m_pTray) {
			m_pTray->ShowTray();
		} else {
			DefWindowProc(message, wParam, lParam);
		}
	} else {
		DefWindowProc(message, wParam, lParam);
	}

    return CWnd::WindowProc(message, wParam, lParam);
}


/**
 @brief     트레이아이콘 콜백함수수신부, 마우스 클릭에 대한 처리를 한다.
 @author    odkwon
 @date      2008-05-20
 @param     [in]wParam  wParam
 @param     [in]lParam  lParam
 @return 
 */
LRESULT CTrayMsgRecevier::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
    UNUSED_ALWAYS(wParam);
    switch(lParam)
    {
        case WM_LBUTTONDBLCLK:
            m_pTray->OnTrayLButtonDbClick();            
        break;
        case WM_RBUTTONDOWN:
            m_pTray->OnTrayRButtonDown();
        break;
        case WM_LBUTTONDOWN:
            m_pTray->OnTrayLButtonDown();
        break;
        case WM_RBUTTONUP:
            m_pTray->OnTrayRButtonUp();
        break;
        case WM_LBUTTONUP:
            m_pTray->OnTrayLButtonUp();
        break;
    }
    return 0;
}


/**
 @brief     트레이아이콘 생성자. 내부적응로 윈도우를 생성한다.
 @author    odkwon
 @date      2008-05-20
 */
CTray::CTray(void)
{
   m_TrayMsgRecevier.CreateWnd(this);
}
/**
 @brief     CTray 파괴자. 파괴시 트레이 아이콘 삭제를 한다.
 @author    odkwon
 @date      2008-05-15
 @param void 
 @return 
 */
CTray::~CTray(void)
{
    RemoveTrayIcon();

}

/**
 @brief     트레이 아이콘 등록하기
 @author    odkwon
 @date      2008-05-15
 @param     [in]_pWnd        [in]트레이아이콘에서 발생하는 메세지를 수신받는 윈도우
 @param     [in]_hIcon       [in]트레이 아이콘에 등록할 아이콘 이미지
 @param     [in]_szToolTip   [in]툴팁 메세지
 @return    true/false
 */
bool CTray::AddIcon(HICON _hIcon, LPCTSTR _szToolTip /* = _T("") */)
{
	

//     ::ZeroMemory(&m_NotiIconData, sizeof(NOTIFYICONDATA));
// 
//     // NOTIFYICONDATA 구조체의 값 설정
//     m_NotiIconData.cbSize  = sizeof(NOTIFYICONDATA);
	::ZeroMemory(&m_NotiIconData, NOTIFYICONDATA_V3_SIZE);
	m_NotiIconData.cbSize = NOTIFYICONDATA_V3_SIZE;
    m_NotiIconData.hWnd = m_TrayMsgRecevier.GetSafeHwnd();
    m_NotiIconData.uID = 0;
    m_NotiIconData.hIcon = _hIcon;
    m_NotiIconData.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP | NIF_INFO;
    m_NotiIconData.uCallbackMessage = WM_TRAY_NOTIFY;
    _tcscpy_s(m_NotiIconData.szTip, _countof(m_NotiIconData.szTip), _szToolTip);
    
	//RemoveTrayIcon();
    // 트레이 아이콘 추가
    return ShowTray();
}


/**
 @brief     설정되어 있는 아이콘 정보를 이용하여 트레이 아이콘을 추가한다.
 @author    odkwon
 @date      2008-05-15
 @return    true/false
 */
bool CTray::ShowTray()
{
	// 2016-01-18 sy.choi m_NotiIconData.hWnd == NULL 인 경우 false 리턴
	if (NULL == m_NotiIconData.hWnd) {
		return false;
	}

    if (INVALID_HANDLE_VALUE == m_NotiIconData.hWnd) 
	{
		return false;
	}
    if (0 == m_NotiIconData.uFlags)
	{
		return false;
	}

    if (0 == m_NotiIconData.uCallbackMessage)
	{
		return false;
	}
    if (0 == m_NotiIconData.hIcon)
	{
		return false;
	}
    
	
    return (TRUE == ::Shell_NotifyIcon(NIM_ADD, &m_NotiIconData));
}

/**
 @brief     트레이 아이콘을 변경한다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_hIcon  [in]트레이 아이콘에 등록할 아이콘 이미지 핸들
 @return    true/false
 */
bool CTray::SetIcon(HICON _hIcon)
{
    m_NotiIconData.uFlags = NIF_ICON;
    m_NotiIconData.hIcon = _hIcon;
    return (TRUE == ::Shell_NotifyIcon(NIM_MODIFY, &m_NotiIconData));
}
/**
 @brief     트레이 아이콘을 변경한다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_lpszIconName  [in]트레이 아이콘에 등록할 아이콘 이미지 이름
 @return    true/false
 */
bool CTray::SetIcon(LPCTSTR _lpszIconName)
{
    HICON hIcon = ::AfxGetApp()->LoadIcon(_lpszIconName);
    return (TRUE == SetIcon(hIcon));
}

/**
 @brief     트레이 아이콘을 변경한다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_nIDResource  [in]트레이 아이콘에 등록할 아이콘 리소스 아이디
 @return    true/false
 */
bool CTray::SetIcon(UINT _nIDResource)
{
    HICON hIcon = ::AfxGetApp()->LoadIcon(_nIDResource);
    return (TRUE == SetIcon(hIcon));
}


/**
 @brief     툴팁 텍스트를 변경한다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_pszTip  [in]변경할 툴팁 Text
 @return    true/false
 */
bool CTray::SetTooltipText(LPCTSTR _pszTip)
{
    m_NotiIconData.uFlags = NIF_TIP;
    _tcscpy_s(m_NotiIconData.szTip, _countof(m_NotiIconData.szTip), _pszTip);   

    return (TRUE == ::Shell_NotifyIcon(NIM_MODIFY, &m_NotiIconData));
}

/**
 @brief     툴팁 텍스트를 변경한다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_pszTip  [in]변경할 툴팁 리소스 스트링 아이디
 @return    true/false
 */
bool CTray::SetTooltipText(UINT _nID)
{
    CString strText;
    if (FALSE == strText.LoadString(_nID)) return false;
    return (TRUE == SetTooltipText(strText));
}

/**
 @brief     트레이 아이콘을 삭제한다.
 @author    odkwon
 @date      2008-05-15 
 */
void CTray::RemoveTrayIcon()
{
    m_NotiIconData.uFlags = 0;
    ::Shell_NotifyIcon(NIM_DELETE, &m_NotiIconData);
}

/**
 @brief     풍선 툴팁을 뛰운다.\n
            9x,NT에서는 지원하지 않는다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_szBalloonTitle  [in]풍선 툴팁 타이틀
 @param     [in]_szBalloonMsg    [in]풍선 툴팁 메세지
 @param     [in]_dwIcon          [in]풍선 툴팁 아이콘(NIIF_WARNING,NIIF_ERROR,NIIF_INFO,NIIF_NONE)
 @param     [in]_nTimeOut        [in]풍선 툴팁을 띄울 시간(단위 초)
 @return    true/false
 */
bool CTray::ShowBalloon(LPCTSTR _szBalloonTitle, LPCTSTR _szBalloonMsg, DWORD _dwIcon /* = NIIF_NONE */, UINT _nTimeOut /* = 10 */)
{

    // 파라메터 체크
    if (NULL == _szBalloonTitle) return false;
    if (NULL == _szBalloonMsg) return false;
    if (NULL == _szBalloonMsg) return false;

    if (false == (_dwIcon == NIIF_WARNING || _dwIcon == NIIF_ERROR || _dwIcon == NIIF_INFO || _dwIcon == NIIF_NONE)) return false;
    
    _nTimeOut *= 1000;
    if (10000 > _nTimeOut) return false;
    
	m_NotiIconData.dwInfoFlags = _dwIcon;
	m_NotiIconData.uFlags |= NIF_INFO;    
	m_NotiIconData.uTimeout = _nTimeOut;
	_tcscpy_s(m_NotiIconData.szInfoTitle, _countof(m_NotiIconData.szInfoTitle), _szBalloonTitle);
	_tcscpy_s(m_NotiIconData.szInfo, _countof(m_NotiIconData.szInfo), _szBalloonMsg);
	return (TRUE == ::Shell_NotifyIcon(NIM_MODIFY, &m_NotiIconData));  
}

/**
 @brief     트레이 메뉴를 띄운다.
 @author    odkwon
 @date      2008-05-15 
 @param     [in]_pContextMenu  [in]메뉴 포인터
  @return   true/false
 */
bool CTray::ShowMenu(CMenu *_pContextMenu)
{
    if (NULL == _pContextMenu) return false;            

    CPoint aPos = 0;
	::AfxGetMainWnd()->SetForegroundWindow();
	::GetCursorPos(&aPos);
	return (TRUE == _pContextMenu->TrackPopupMenu(TPM_RIGHTALIGN | TPM_RIGHTBUTTON, aPos.x, aPos.y, AfxGetMainWnd(), 0));
}

void CTray::SetWnd(HWND _hWnd)
{
	m_TrayMsgRecevier.m_hWnd = _hWnd;
}