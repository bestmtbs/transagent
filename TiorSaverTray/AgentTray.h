
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file       CAgentTray.h 
 @brief     AgentTray  ���� ����
 @author   jhlee
 @date      create 2011.06.15
 */

#pragma once

#include "Tray.h"

class CAgentTray :
	public CTray
{
public:
	CAgentTray(void);
	virtual ~CAgentTray(void);

protected:
	virtual void OnTrayLButtonDbClick() ;
	virtual void OnTrayLButtonDown() ;
	virtual void OnTrayLButtonUp()  ;
	virtual void OnTrayRButtonDown();
	virtual void OnTrayRButtonUp();
};
