
// HTMLDlg.cpp : 구현 파일
//

#include "stdafx.h"
//#include "HTML.h"
#include "NotifyByBrowserDlg.h"
#include "afxdialogex.h"
#include "Resource.h"
#include "TiorSaverTray.h"
//#include "CWebBrowser2.h"
#include "HTMLWriter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHTMLDlg 대화 상자

extern CTiorSaverTrayApp theApp;
IMPLEMENT_DYNAMIC(CNotifyByBrowserDlg, CDialogEx)


CNotifyByBrowserDlg::CNotifyByBrowserDlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CNotifyByBrowserDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bClose = TRUE;
}

void CNotifyByBrowserDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange( pDX );
	DDX_Control( pDX, IDC_EXPLORER1, m_WebNotify );
}

BEGIN_MESSAGE_MAP(CNotifyByBrowserDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	//ON_WM_SIZE()
END_MESSAGE_MAP()


// CHTMLDlg 메시지 처리기

BOOL CNotifyByBrowserDlg::OnInitDialog()
{
	m_bClose = FALSE;
	CDialogEx::OnInitDialog();

	//CenterWindow();
	//m_bmpBackground.LoadBitmap(IDB_NT_MSG_BG);
	//BITMAP bm;
	//m_bmpBackground.GetBitmap(&bm);
	MoveWindow(0,0,620,900);		//이미지 크기에 맞게 다이얼로그 사이즈 조정
	//this->CenterWindow(GetDesktopWindow());
	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
	m_WebNotify.put_Silent(TRUE);
	if (0 == m_strMessageType.CompareNoCase(_T("url"))) {
		CComVariant var(m_strMessage);
		m_WebNotify.Navigate2(&var, NULL, NULL, NULL, NULL);
		//m_WebNotify.get_Height();

	} else if (0 == m_strMessageType.CompareNoCase(_T("html"))) {
		m_WebNotify.Navigate( _T("about:blank"), NULL, NULL, NULL, NULL );

		CHTMLWriter htmlWriter( m_WebNotify.GetControlUnknown() );

		htmlWriter.Write(m_strMessage);
	}
	m_WebNotify.put_Resizable(VARIANT_TRUE);
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNotifyByBrowserDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	BITMAP bm;
	CDC memDC;

	//dc.FillSolidRect(rc, RGB(255,255,255));

	CBitmap * bitmap = NULL;
	m_bmpBackground.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_bmpBackground);

	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
	m_WebNotify.put_Resizable(VARIANT_TRUE);
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CNotifyByBrowserDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//void CNotifyByBrowserDlg::OnAdd() 
//{
//	UpdateData( TRUE );
//
//	CHTMLWriter htmlWriter( m_WebNotify.GetControlUnknown() );
//
//	htmlWriter.Add( m_sHTML );
//}

void CNotifyByBrowserDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	m_bClose = TRUE;
	if (theApp) {
		if (theApp.m_pFrame) {
			if (0 <theApp.m_pFrame->m_arrNotifyByBrowserDlg.GetSize()) {
				theApp.m_pFrame->m_arrNotifyByBrowserDlg.RemoveAt(m_nIndex);
				for (int i = m_nIndex; i < theApp.m_pFrame->m_arrNotifyByBrowserDlg.GetSize(); i++) {
					theApp.m_pFrame->m_arrNotifyByBrowserDlg.GetAt(i)->m_nIndex--;
				}
			}
		}
	}
	CDialogEx::OnClose();
}
//
//void CNotifyByBrowserDlg::OnSize(UINT nType, int cx, int cy)
//{
//	m_WebNotify.MoveWindow(0,0,cx,cy);
//	CDialogEx::OnSize(nType, cx, cy);
//	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
//}
