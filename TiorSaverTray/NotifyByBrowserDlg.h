
// HTMLDlg.h : 헤더 파일
//

#pragma once
#include "CWebBrowser2.h"
#include "Resource.h"

// CHTMLDlg 대화 상자
class CNotifyByBrowserDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CNotifyByBrowserDlg)
private:
	CWebBrowser2 m_WebNotify;

	// 생성입니다.
public:
	CNotifyByBrowserDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	BOOL m_bClose;
	CString m_strMessage;
	CString m_strTitle;
	CString m_strMessageType;
	CBitmap m_bmpBackground;
	int m_nIndex;

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_NOTIFY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


	// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	//afx_msg void OnSize(UINT nType, int cx, int cy);
};
