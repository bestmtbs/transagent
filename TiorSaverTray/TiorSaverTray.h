
// TiorSaverTray.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <locale>
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "curl\curl.h"
#pragma comment(lib, "wldap32.lib")
#pragma comment (lib, "crypt32")

#ifndef _DEBUG
#pragma comment (lib, "../BuildEnv/Lib/x86/libcurl.lib")
#else	// #ifdef _DEBUG
#pragma  comment (lib, "../BuildEnv/Lib/x86/libcurld.lib")
#endif	// #else	// #ifdef _DEBUG
#if LIBCURL_VERSION_NUM < 0x070c03
#error "upgrade your libcurl to no less than 7.12.3"
#endif
#include "resource.h"		// main symbols
//#include "../BuildEnv/common/HttpsTConnect.h"
#include "../TiorSaverTransfer/MemberData.h"
//#include "TiorSaverTrayWnd.h"
#include "MainFrm.h"
#include "MinidumpHelp.h"
#include "CurlTConnect.h"

// CTiorSaverTrayApp:
// See TiorSaverTray.cpp for the implementation of this class
//

class CTiorSaverTrayApp : public CWinAppEx
{
public:
	CTiorSaverTrayApp();
	virtual ~CTiorSaverTrayApp();

	//CHttpsTConnect* m_logSend;
	MinidumpHelp m_Minidump;
	CMemberData* m_pMemberData;
	CMainFrame *m_pFrame;
	CString m_strToken;
	CString m_strUrl;
	CString m_strTimeFromServer;
	//BOOL m_bComplete;
	//INT m_nRestartProcessType;
	INT m_nSendType;
	//BOOL m_bIsRandom;
	//BOOL m_bIsCollecting;
	//BOOL m_bIsFirstUser;
	INT m_nCurrNotice;
	CCurlTConnect m_curl;
	//CString m_strNewVersion;
	//INT	m_nScanDelay;
	virtual BOOL InitInstance();
	//void CurlJSON();

	//BOOL CreateDB();
	//CString CurlPost(CString _strParam, CString _strUrl, INT nErr);
	//CString CurlGET(CString _strUrl, INT nErr);
	//CString CurlDelete(CString _strParam, CString _strUrl, INT nErr);
	//CString HttpsPost(CString _strToken, CString _strParam, CString _strUrl);
	//CString HttpsGET(CString _strToken, CString _strUrl);
	//BOOL SendData(CString _strTargetFile, UINT& _nCurrentOffset);
	//BOOL SendBinaryData(CString _strTargetFile, UINT& _nCurrentOffset);
	void SetToken(CString _strToken);
	CString GetToken();
	//CStringA Utf8_Encode(CStringW strData);
	//CStringW Utf8_Decode(CStringA strData);
	BOOL NoticeRequest();
	BOOL PipeServerTime();
	//void CheckNotify();
	NOTICE_SET* m_pNoticeSet;
	UINT m_nNoticeSetSize;
	
//	CRandomDataGenerateThread *m_pRandomDataGenerateThread;
	void Write_Index(CString _strPath, CString _strHash, CString _strVersion);

	DECLARE_MESSAGE_MAP()
	friend BOOL WINAPI PipeServerTimeThread(LPVOID lpParam);
};

extern CTiorSaverTrayApp theApp;