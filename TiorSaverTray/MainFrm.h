
// MainFrm.h : CMainFrame 클래스의 인터페이스
#include "AgentTray.h"
//#include "TiorSaverTrayPopupDlg.h"
#include "NotifyByBrowserDlg.h"


#pragma once

class CMainFrame : public CFrameWndEx
{
	
public: // serialization에서만 만들어집니다.
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:

// 작업입니다.
public:

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);

// 구현입니다.
public:
	virtual ~CMainFrame();
	CAgentTray				m_Tray;
	//CTiorSaverTrayPopupDlg* m_pMainDlg;
	//CNotifyByBrowserDlg* m_pMainDlg;
	CTypedPtrArray <CPtrArray, CNotifyByBrowserDlg*> m_arrNotifyByBrowserDlg;
	BOOL m_bSendToCollect_Kill;
	void ShowPopUp();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	CMFCMenuBar       m_wndMenuBar;
	CMFCToolBar       m_wndToolBar;
	CMFCStatusBar     m_wndStatusBar;
	CMFCToolBarImages m_UserImages;

// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnViewCustomize();
	afx_msg void OnPopUpStart();
	afx_msg LRESULT OnToolbarCreateNew(WPARAM wp, LPARAM lp);
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()
private:
	BOOL					CreateTrayIcon();

//	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
public:
	virtual void ActivateFrame(int nCmdShow = -1);
};


