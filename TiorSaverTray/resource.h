//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TiorSaverTray.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDQUERY                         101
#define IDD_REGISTER_DIALOG             103
#define IDR_HTML_NOTIFYBYBROWSER        112
#define IDR_MAINFRAME                   128
#define IDR_MENU2                       130
//#define IDB_NT_MSG_BG                   132
#define IDD_DIALOG_NOTIFY               138
//#define IDB_REGISTER_BG                 140
#define IDC_STATIC_SRC                  1015
#define IDC_BUTTON_CURL                 1016
#define IDC_BUTTON_HTTP                 1017
#define IDC_BUTTON_STOP                 1019
#define IDC_BUTTON_REGISTER             1020
#define IDC_EDIT_LICENSE_KEY            1021
#define IDC_BUTTON_GETTOKEN_HTTP        1022
#define IDC_BUTTON_HIDE                 1023
#define IDC_BUTTON_REGISTER_CURL        1024
#define IDC_BUTTON_REGISTER_HTTP        1025
#define IDC_BUTTON_REGISTER_CURL2       1025
#define IDC_BUTTON_CANCEL               1025
#define IDC_EDIT_LICENSE_KEY2           1027
#define IDC_EDIT_SERVER_HOST_URL        1027
#define IDC_EDIT_AGENT_NAME             1028
#define IDC_COMBOBOX_DEPARTMENT         1030
#define IDC_EXPLORER1                   1032
#define IDC_BUTTON_GETTOKEN_CURL        1033
#define IDC_BUTTON_CLOSE                1034
#define IDC_STATIC_DOMAIN               1035
#define IDC_STATIC_LICENSE              1036
#define IDC_STATIC_STEP1                1037
#define IDC_STATIC_STEP2                1038
#define IDC_STATIC_USERNAME				1039
#define IDC_STATIC_DEPARTMENT			1040
#define IDC_RADIO_HTTP                  1041
#define IDC_RADIO_HTTPS                 1042
#define IDR_MENU1                       32788
#define ID_MENU_ITCMS_TOTAL             32789
#define ID_MENU_HELP                    32790
#define ID_MENU_HOMEPAGE                32791
#define ID_MENU_PRODUCT_INFO            32792
#define ID_MENU_SEARCH                  32793
#define ID_MENU_TRAY_LOGOUT             32794

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1043
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
