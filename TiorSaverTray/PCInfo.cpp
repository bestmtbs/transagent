
/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.05.29		
********************************************************************************/
/**
@file       CAgentTray.cpp
@brief     AgentTray  구현 파일
@author   jhlee
@date      create 2011.06.15
*/
#include "StdAfx.h"
#include "PCInfo.h"
#include "hash_global.h"
#include "hash.h"

//#include "Base64.h"
#include "Crypto/MD5.h"
#include "UtilsUnicode.h"
#include "PathInfo.h"
#include "wbemidl.h"
#include <comdef.h>
#include <comutil.h>
#pragma comment (lib, "Wbemuuid.lib")
#include "WinOsVersion.h"
#include "UtilsReg.h"
#include <lm.h>  

#define CPU_REGPATH	_T("HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0")

#ifndef UM_WRITE_LOG
#define UM_WRITE_LOG	DBGLOG
#endif

CPCInfo::CPCInfo(void)
{
}

CPCInfo::~CPCInfo(void)
{
}

BOOL CPCInfo::GetRealIPMAC(CString &_strIPAddress, CString &_strMacAddress)
{
	DWORD dwBufferSize	= 0;
	IP_ADAPTER_INFO *lpGateware = NULL, *lpTemp = NULL;
	GetAdaptersInfo(NULL, &dwBufferSize);
	if (dwBufferSize == 0)
	{
		SetDefaultIPAddress(_strIPAddress);
		SetDefaultMacAddress(_strMacAddress);

		return TRUE;
	}

	CString strGateway = _T(""), strDesc = _T("");
	BOOL bFound=FALSE;
	lpGateware = (IP_ADAPTER_INFO *) GlobalAlloc(GPTR, dwBufferSize);
	if (ERROR_SUCCESS != GetAdaptersInfo(lpGateware, &dwBufferSize))
	{
		SetDefaultIPAddress(_strIPAddress);
		SetDefaultMacAddress(_strMacAddress);
		if( lpGateware != NULL )
			GlobalFree(lpGateware);// Kevin(2013-8-1)
		//UM_WRITE_LOG(_T("-------------- GetRealIPMAC -end 1"));
		return TRUE;
	}

	lpTemp = lpGateware;

	while(TRUE)
	{
		strGateway		= lpTemp->GatewayList.IpAddress.String;
		_strIPAddress	= lpTemp->IpAddressList.IpAddress.String;
		strDesc			= lpTemp->Description;

		_strMacAddress.Format( _T("%02X:%02X:%02X:%02X:%02X:%02X"),
			lpTemp->Address[0], lpTemp->Address[1],
			lpTemp->Address[2], lpTemp->Address[3],
			lpTemp->Address[4], lpTemp->Address[5]);

		/*arrMac.Add(_strMacAddress); // vpn mac 일경우 제거*/

		strDesc.MakeUpper();

		if( strDesc.Find( _T("BLUETOOTH")) < 0)
		{
			if(!_strIPAddress.IsEmpty() && !_strMacAddress.IsEmpty() && _strIPAddress != _T("0.0.0.0"))
			{
				bFound=TRUE;
				break;
			}
		}

		lpTemp = lpTemp->Next;

		if (lpTemp == NULL) 
		{
			break;
		}
	}

	if( lpGateware != NULL )
		GlobalFree(lpGateware);

	if (_strIPAddress.IsEmpty())		SetDefaultIPAddress(_strIPAddress);
	if (_strMacAddress.IsEmpty())	SetDefaultMacAddress(_strMacAddress);

	return TRUE;

}

void CPCInfo::SetDefaultIPAddress(CString& _strIPAddress)
{
	WORD		wVersionRequested;
	WSADATA		wsaData;
	char		name[50] = {0};
	PHOSTENT	hostinfo;
	wVersionRequested = MAKEWORD(2, 0);

	if (!WSAStartup( wVersionRequested, &wsaData))
	{
		if (!gethostname (name, sizeof(name)))
		{
			if ((hostinfo = gethostbyname(name)) != NULL)
			{
				_strIPAddress.Format( _T("%S"), inet_ntoa (*(struct in_addr *)*hostinfo->h_addr_list)); //2016-05-20 sy.choi 와이드 문자열을 지정하는 %s대신 %S로 단일 바이트 또는 멀티바이트 문자열을 지정
			}
		}

		WSACleanup();
	}
}


void CPCInfo::SetDefaultMacAddress(CString& _strMacAddress)
{
	ASTAT		Adapter;
	NCB			Ncb;
	UCHAR		uRetCode;
	TCHAR		NetName[50] = {0};
	LANA_ENUM   lenum;

	memset(&Ncb, 0, sizeof(Ncb));
	Ncb.ncb_command	= NCBENUM;
	Ncb.ncb_buffer	= (UCHAR *)&lenum;
	Ncb.ncb_length	= sizeof(lenum);
	
	uRetCode		= Netbios( &Ncb );
	if(NRC_GOODRET != Ncb.ncb_retcode)	//hhh:(2013.06.04)
		return;
	
	CString strMac;

	for (int i = 0; i < lenum.length; i++)
	{
		memset( &Ncb, 0, sizeof(Ncb) );
		Ncb.ncb_command = NCBRESET;
		Ncb.ncb_lana_num = lenum.lana[i];

		uRetCode = Netbios( &Ncb );

		memset( &Ncb, 0, sizeof (Ncb) );
		Ncb.ncb_command = NCBASTAT;
		Ncb.ncb_lana_num = lenum.lana[i];

		strcpy_s((char*)Ncb.ncb_callname,NCBNAMSZ, "*               ");
		Ncb.ncb_buffer = (unsigned char *) &Adapter;
		Ncb.ncb_length = sizeof(Adapter);

		uRetCode = Netbios( &Ncb );

		if (uRetCode == 0)
		{
			strMac.Format( _T("%02X:%02X:%02X:%02X:%02X:%02X"),
				Adapter.adapt.adapter_address[0],
				Adapter.adapt.adapter_address[1],
				Adapter.adapt.adapter_address[2],
				Adapter.adapt.adapter_address[3],
				Adapter.adapt.adapter_address[4],
				Adapter.adapt.adapter_address[5]);

			_strMacAddress = strMac;

		}
	}

}

void CPCInfo::GetPCName(CString& _strPCName)
{
	TCHAR szComputerName[64] = {0,};
	DWORD dwSize = sizeof(szComputerName);

	HKEY hKey = NULL;

	if(ERROR_SUCCESS != RegOpenKeyEx (HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters"), 0, KEY_READ, &hKey))
	{			
		return ;
	}	

	if (ERROR_SUCCESS != RegQueryValueEx (hKey, TEXT("NV Hostname"), NULL, NULL, (LPBYTE)szComputerName, &dwSize))
	{
		if(hKey != NULL) RegCloseKey(hKey);
		return ;
	}


	if (hKey != NULL) RegCloseKey(hKey);

	_strPCName = szComputerName;

}

void CPCInfo::GetPCID(CString& _strPCID)
{

	CString strIP = _T(""), strMacAddress = _T(""), strFile = _T(""), strGetPCID = _T("");

	strFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("pcid.sys"));

	CFile cFile;

	if( cFile.Open(strFile, CFile::modeRead | CFile::shareDenyNone, NULL))
	{
		DWORD dwLength = (DWORD)cFile.GetLength();

		char* pszBuffer = new char[dwLength+1];
		ZeroMemory(pszBuffer, dwLength+1);

		cFile.Read(pszBuffer, dwLength);

		strGetPCID = pszBuffer;

		delete[] pszBuffer;

		cFile.Close();
	}

	if( strGetPCID.IsEmpty() )
	{
		if( cFile.Open(strFile,   CFile::modeCreate|CFile::modeWrite, NULL ))
		{
			GetRealIPMAC(strIP, strMacAddress);

			CPCInfo info;
			CString strDiskID = _T("");
			strDiskID = info.GetDeviceSerial(CPathInfo::GetRoot());

			CString strPCID = _T("");

			strPCID = strDiskID + strMacAddress;

			UCHAR szSha[MAX_PATH]={0,};

			char szAnsiBuff[MAX_PATH] = {0,};
			memset(szAnsiBuff, 0x00, sizeof(szAnsiBuff));

			UnicodeToAnsi(szAnsiBuff, strPCID, MAX_PATH);

			CmsHashBin(szSha, (UCHAR*)szAnsiBuff, strlen(szAnsiBuff), HASH_ALGID_SHA256);

			//CBase64 base;
			//strGetPCID.Format(_T("%s"), base.base64encode(szSha,20));

			CMD5 md5;
			BYTE caHash[ 16] = { 0,};
			char szMD5Val[40]={0,};
			LPWSTR pRet =NULL ;
			md5.MD5Update((unsigned char*)szSha, 20);
			md5.MD5Final(caHash);

			for(int i = 0; i < sizeof(caHash) ; i++)
				sprintf(szMD5Val + (i * 2), "%02x", caHash[i]);

			pRet = AnsiCodeToUniCode(szMD5Val);
			strGetPCID.Format( _T("%s"),  pRet);
			if(pRet)
				delete pRet;

			UnicodeToAnsi(szAnsiBuff, strGetPCID, MAX_PATH);
			cFile.Write(szAnsiBuff, strGetPCID.GetLength());
	
			cFile.Close();
		}
	}

	_strPCID = strGetPCID;


}

CString CPCInfo::GetDeviceSerial(CString strVolume)
{

	CString strMsg;

	HRESULT hRes;

	CString  strDeviceID=_T(""), strSerialNumber = _T(""), strDeviceInfo = _T("");
	if ( Connect() == TRUE )
	{
		if(m_pIWbemServices == NULL)
		{
			/*gclDbgLog.WriteTraceLog(_FLOW_LOG, "CHddDeviceThread::WMI NULL");*/
			return strSerialNumber;
		}

		bstr_t bstrquery;
		bstrquery = TEXT( "select * From win32_LogicalDisk where DriveType = 3 and DeviceID='"+strVolume+"'");
		IEnumWbemClassObject* pEnumerator = NULL;
		hRes = m_pIWbemServices->ExecQuery(
																		bstr_t("WQL"),
																		bstrquery,
																		0,
																		NULL, 
																		&pEnumerator);

		if(FAILED(hRes))
		{

			/*gclDbgLog.WriteTraceLog(_FLOW_LOG, _T("CHddDeviceThread::WbemClass query error (%d)"), GetLastError());	*/	
			m_pIWbemServices->Release();

			return strSerialNumber;

		}

		IWbemClassObject *pclsObj;
		ULONG uReturn = 0;

		while (pEnumerator)
		{
			HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);

			if(uReturn == 0)
			{
				/*	gclDbgLog.WriteTraceLog(_FLOW_LOG, "CHddDeviceThread::WbemClass RETURN NULL");		*/

				break;
			}

			VARIANT vtProp1;

			hr = pclsObj->Get(L"VolumeSerialNumber", 0, &vtProp1,0,0);

			if(vtProp1.vt != VT_NULL)
			{
				strSerialNumber.Format(_T("%s"), CString(V_BSTR(&vtProp1)));
			}

			/*gclDbgLog.WriteTraceLog(_FLOW_LOG, _T("CHddDeviceThread::GetDeviceSerial (%s)"), strSerialNumber);*/

			VariantClear(&vtProp1);

		}

		m_pIWbemServices->Release();

		CoUninitialize();
	}
	return strSerialNumber;

}

BOOL CPCInfo::Connect()
{
	// Create an instance of the WbemLocator interface.
	m_strNamespace = _T("\\\\.\\root\\cimv2");
	m_pIWbemServices = NULL;

	IWbemLocator *pIWbemLocator = NULL;

	BSTR bstrNamespace = m_strNamespace.AllocSysString();
	if (!bstrNamespace)
	{
		//AfxMessageBox(_T("AllocSysString() failed. "));
		return FALSE;
	}

	HRESULT hRes;
	//hRes=CoInitialize(NULL);
	hRes = CoInitializeEx(0, COINIT_MULTITHREADED);

	//Security needs to be initialized in XP first and this was the major problem 
	//why it was not working in XP.

	hRes=CoInitializeSecurity( 
		NULL,	
		-1,	
		NULL,	
		NULL,	
		//RPC_C_AUTHN_LEVEL_PKT,	
		RPC_C_AUTHN_LEVEL_DEFAULT,
		RPC_C_IMP_LEVEL_IMPERSONATE,
		NULL,	
		EOAC_NONE,	
		NULL	);

	//if(hRes != S_OK)
	if(FAILED(hRes))
	{
	
	}

	if(CoCreateInstance(CLSID_WbemLocator,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IWbemLocator,
		(LPVOID *) &pIWbemLocator) == S_OK)
	{
		// If already connected, release m_pIWbemServices.
		if (m_pIWbemServices)  m_pIWbemServices->Release();

		// Using the locator, connect to CIMOM in the given namespace.

		if(pIWbemLocator->ConnectServer(bstrNamespace,
			NULL,   //using current account for simplicity
			NULL,	//using current password for simplicity
			0L,		// locale
			0L,		// securityFlags
			NULL,	// authority (domain for NTLM)
			NULL,	// context
			&m_pIWbemServices) == S_OK) 
		{	
			// Indicate success.
	
		}
		else 
		{
			//AfxMessageBox(_T("Bad namespace"));
		}

		// Done with pNamespace.
		SysFreeString(bstrNamespace);

		// Done with pIWbemLocator. 
		pIWbemLocator->Release(); 
		return TRUE;
	}
	else 
	{
		//AfxMessageBox("Failed to create IWbemLocator object ");
		CoUninitialize();
		return FALSE;
	}

	return TRUE;

}

void CPCInfo::GetOSName(CString& _strOSName)
{
	CWinOsVersion osVer;

	CString strProductName = _T(""), strEdition = _T("");
	strProductName = osVer.GetProductName();
	strEdition = osVer.GetEdition();
	_strOSName.Format(_T("%s %s"), strProductName, strEdition);

}

//cpu정보를 가져온다.
void CPCInfo::GetCpuName(CString& _strCpuName)
{
	_strCpuName = GetRegStringValue(HKEY_LOCAL_MACHINE, CPU_REGPATH, _T("ProcessorNameString"), _T("") );	
}

//램 용량을 가져온다.
int CPCInfo::GetRamSize()
{
	MEMORYSTATUSEX memoryStatus;
	memset(&memoryStatus, 0, sizeof(MEMORYSTATUSEX));
	memoryStatus.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memoryStatus);
	
	int iRamMb = (int)memoryStatus.ullTotalPhys / (1024 * 1024);  //MB바이트로 환산
	return iRamMb;
}

//작업그룹명을 가져온다. 작업그룹이 없다면 도메인 이름을 가져옴
void CPCInfo::GetWorkgroup(CString& _strGroup)
{
	TCHAR* lpNameBuff;
	NETSETUP_JOIN_STATUS njsBuffType;  

	if (NetGetJoinInformation(NULL, &lpNameBuff, &njsBuffType) == NERR_Success) {  
		switch (njsBuffType) {  
		case NetSetupWorkgroupName:  
			_strGroup.Format(_T("%s"), lpNameBuff);
			break;  

		case NetSetupDomainName:  
			_strGroup.Format(_T("%s"), lpNameBuff);
			break;  
		}  
		NetApiBufferFree(lpNameBuff);  
	}  
}

/*
BOOL CPCInfo::GetDiskVolume(CStringArray* _pArrDevice)
{


	HRESULT hRes;

	DWORD dwRealFixedDrives = 0;

	CString  strDeviceID=_T(""), strMsg = _T("");
	

 	if(Connect() == TRUE )
 	{

		if(m_pIWbemServices == NULL)
		{
			DBGLOG(_T("CPCInfo::GetDiskVolume() WMI NULL"));
			return  FALSE;
		}

		bstr_t bstrquery;
		bstrquery = "select * From win32_LogicalDisk where DriveType = 3";
		IEnumWbemClassObject* pEnumerator = NULL;
		hRes = m_pIWbemServices->ExecQuery(
			bstr_t("WQL"),
			bstrquery,
			0,
			NULL, 
			&pEnumerator);


		if(FAILED(hRes))
		{
			strMsg.Format(_T("CPCInfo::WbemClass query error (%d)  "), GetLastError());
			DBGLOG(strMsg);
			return FALSE;

		}

		IWbemClassObject *pclsObj;
		ULONG uReturn = 0;

		strMsg.Format(_T("CPCInfo::WbemClass query success  "));
		DBGLOG(strMsg);
		while (pEnumerator)
		{
			HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);

			if(uReturn == 0)
			{
				DBGLOG(_T("CPCInfo::WbemClass RETURN NULL"));
				break;
			}

			VARIANT vtProp1;

			hr = pclsObj->Get(L"DeviceID", 0, &vtProp1,0,0);

			if(vtProp1.vt != VT_NULL)
			{
				strDeviceID.Format(_T("%s"), CString(V_BSTR(&vtProp1)));
				DBGLOG(strDeviceID);
				_pArrDevice->Add(strDeviceID);
			}

			VariantClear(&vtProp1);
		}

	}

	
	return TRUE;
}
*/