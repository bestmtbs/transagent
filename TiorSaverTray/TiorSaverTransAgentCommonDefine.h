#ifndef _SEND_AGENT__COMMON_DEFINE_H
#define _SEND_AGENT__COMMON_DEFINE_H

//#define CPathInfo::GetClientInstallPath()	_T("C:\\DSNTECH\\WinTior\\Log\\")
//Process Name
#define	WTIOR_SYS_AGENT_32_NAME			_T("TiorSaverSysAgent_x86.exe")
#define	WTIOR_USR_AGENT_32_NAME			_T("TiorSaverUsrAgent_x86.exe")
//#define	WTIOR_SYS_AGENT_32_RE_NAME		_T("TiorSaverSysAgent32_RE.exe")
//#define	WTIOR_TRANS_AGENT_NAME			_T("TiorSaverUsrAgent32_RE.exe")
#define	WTIOR_SYS_AGENT_64_NAME			_T("TiorSaverSysAgent_x64.exe")
#define	WTIOR_USR_AGENT_64_NAME			_T("TiorSaverUsrAgent_x64.exe ")
//#define	WTIOR_SYS_AGENT_64_RE_NAME		_T("TiorSaverSysAgent64_RE.exe")
//#define	WTIOR_USR_AGENT_64_RE_NAME		_T("TiorSaverUsrAgent64_RE.exe")
#define	WTIOR_TRANS_AGENT_NAME			_T("TiorSaverTray.exe")
#define	WTIOR_AGENT_SERVICE_NAME		_T("TiorSaverService.exe")
#define	WTIOR_AGENT_RELOAD_NAME			_T("TiorSaverReload.exe")
#define WTIOR_AGENT_DUMMY_NAME			_T("TiorSaverDummy")
#define	WTIOR_UPDATE_AGENT_NAME			_T("TiorSaverUpdate.exe")
#define	WTIOR_DOWNLOAD_AGENT_NAME		_T("TiorSaverDownload.exe")
#define	WTIOR_UNINSTALL_AGENT_NAME		_T("TiorSaverUninstall.exe")
#define EXPLORER_PROC_NAME				_T("explorer.exe")

//Mutex Name
#define	WTIOR_SYS_AGENT_32_MUTEX_NAME		_T("Global\\BBSAVER_CSA_x86")
#define	WTIOR_USR_AGENT_32_MUTEX_NAME		_T("Global\\BBSAVER_CUA_x86")
#define	WTIOR_SYS_AGENT_64_MUTEX_NAME		_T("Global\\BBSAVER_CSA_x64")
#define	WTIOR_USR_AGENT_64_MUTEX_NAME		_T("Global\\BBSAVER_CUA_x64")
#define	WTIOR_TRANS_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_TA")
#define	WTIOR_UPDATE_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_UPA")
#define	WTIOR_DOWNLOAD_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_DWNA")
#define	WTIOR_UNINSTALL_AGENT_MUTEX_NAME	_T("Global\\BBSAVER_UINSA")
#define	WTIOR_UNINSTALL_AGENT_MUTEX_NAME	_T("Global\\BBSAVER_INSA")
#define	WTIOR_RELOAD_AGENT_MUTEX_NAME	_T("Global\\BBSAVER_REA")

#define CURL_REQUEST_TIMEOUT	30L
#define SEND_TYPE_CURL	0
#define SEND_TYPE_HTTPSTCONNECT 1
#define QUERY_TRY_MAX_LIMIT 5

#define TS_SERVICE_NAME _T("TiorSaver SERVICE")
#define STR_CMS_VER_SECTION						_T("TSVersion")
//#define RANDOMLOG _T("random4.log")
#define SEND_MAX_SIZE 10485760
//struct MemoryStruct {
//	BYTE *memory;
//	size_t size;
//};

#ifndef SE_MemoryFree
#define SE_MemoryFree( _X) {		\
	if( _X != NULL)	{		\
	free( _X);			    \
	_X = NULL;			\
	}						        \
}
#endif
#ifndef SE_MemoryDelete
#define SE_MemoryDelete( _X) {		\
	if( _X != NULL)	{		\
	delete  _X;			    \
	_X = NULL;			\
	}						        \
}
#endif

#define CMSDB_ACCESS_MUTEX_NAME_BY_ANY	_T("Global\\MUTEX_FOR_CMSDB_ACCESS_BY_ANY")	// 2017-03-30 kh.choi C:\dsntech\TiorSaver\DB\CMSDB.db 파일에 동시 접속을 막기위한 동기화용 뮤텍스. TRAY 에서 BY_TRAY 뮤텍스가 만들어지기 이전에 사용
#define CMSDB_ACCESS_MUTEX_NAME_BY_TRAY	_T("Global\\MUTEX_FOR_CMSDB_ACCESS_BY_TRAY")	// 2017-03-30 kh.choi C:\dsntech\TiorSaver\DB\CMSDB.db 파일에 동시 접속을 막기위한 동기화용 뮤텍스. 기본적으로 TRAY 에서 만들어진 뮤텍스를 사용

#define AGENT_ACTION_UPDATE 1
#define AGENT_ACTION_FORCE_UPDATE 2
#define AGENT_ACTION_DELETE 3

#define AGENT_STATE_START 1
#define AGENT_STATE_STOP 0

#define DB_AGENT_INFO		_T("db_agent_info")
#define DB_PC_INFO			_T("db_pc_info")
#define DB_SEND_LIST		_T("db_send_list")
#define DB_COLLECT_POLICY	_T("db_collect_policy")
#define DB_NOTICE			_T("db_notice")
#define DB_STATE_POLICY		_T("db_state_policy")
#define DB_URL_POLICY		_T("db_url_policy")

#define KILL_ALL					0
#define	KILL_COLLECT_AGENT			1
#define	KILL_TRANS_AGENT_NAME		2
#define	KILL_UPDATE_AGENT_NAME		4
#define	KILL_DOWNLOAD_AGENT_NAME	8
#define	KILL_UNINSTALL_AGENT_NAME	16

typedef struct _TIME_SET
{
	CTime ctTimeFromServer;
	int nScanDelay;
	CTime ctScanDate;
	CTime ctDeleteDate;
	int nPolicyRetry;
	_TIME_SET() {
		nScanDelay = 0;
		nPolicyRetry = 0;
	}

}TIME_SET;

typedef struct _UPDATE_SET
{
	//CString strDevice;
	CString strCurrVersion;
	CString strNewVersion;
	//CString strTransPath;
	//CString strTransHash;
	//CString strCollectPath;
	//CString strCollectHash;
	//CString strUpdatePath;
	//CString strUpdateHash;
	//CString  strUpdateMessage;
	//_UPDATE_SET() {
	//	strDevice = _T("");
	//	strCurrVersion = _T("");
	//	strNewVersion = _T("");
	//	strTransPath = _T("");
	//	strTransHash = _T("");
	//	strCollectPath = _T("");
	//	strCollectHash = _T("");
	//	strUpdatePath = _T("");
	//	strUpdateHash = _T("");
	//	strUpdateMessage = _T("");
	//}
	CStringArray arrPath;
	CStringArray arrVersion;
	CStringArray arrHash;
	CString strMessage;
	_UPDATE_SET() {
		strCurrVersion = _T("");
		strNewVersion = _T("");
		arrPath.RemoveAll();
		arrVersion.RemoveAll();
		arrHash.RemoveAll();
		strMessage = _T("");
	}
}UPDATE_SET;

typedef struct _POLICY_SET
{
	BOOL bStart;
	BOOL bDelete;
	CStringArray arrCollect;
	int nAction;
	CStringArray arrDomain;
	CStringArray arrNotice;
	TIME_SET* TimeSet;
	UPDATE_SET* UpdateSet;
	UINT nDiskFree;
	_POLICY_SET() {
		bStart = AGENT_STATE_START;
		bDelete = FALSE;
		arrCollect.RemoveAll();
		nAction = 0;
		arrDomain.RemoveAll();
		arrNotice.RemoveAll();
		TimeSet = new TIME_SET;
		UpdateSet = new UPDATE_SET;
		nDiskFree = 0;
	}
}POLICY_SET;

typedef struct _LOG_DATA
{
	CString strPostData;
	_LOG_DATA() {
		strPostData = _T("");
	}

}LOG_DATA;

typedef struct _DEPARTMENT
{
	CString strDepartmentName;
	int nDepartmentID;
	_DEPARTMENT() {
		strDepartmentName = _T("");
		nDepartmentID = 0;
	}

}DEPARTMENT;

enum OFS_CHECK_ERR
{
	OFS_CHECK_SUCCESS = 0,
	OFS_CHECK_GET_FILE_HASH	 =1,
	OFS_CHECK_HASH_ELSE = 2,
	OFS_CHECK_DECODE_HASH = 3,
	OFS_CHECK_FILE_EXIST = 4,
	OFS_CHECK_PE_FILE = 5,
	OFS_CHECK_INI_FILE= 6,
	OFS_CHECK_TARGET_PATH = 7,
	OFS_CHECK_INI_SECTION = 8,
	OFS_CHECK_INI_VALUE = 9,
	OFS_CHECK_CODESIGN = 10,
	OFS_CHECK_INI_VALUE_HASH = 11,
	OFS_CHECK_FILE_INTEGRITY = 12,
	OFS_CHECK_INI_EXIST = 13,
	OFS_CHECK_INI_HASH_INFO = 14,
	OFS_CHECK_INI_DRV_SECTION = 15,
	OFS_CHECK_INI_HASH_KEY = 16,
	OFS_CHECK_INI_VALUE_HASH_KEY = 17,

};

#define STR_CODE_INTEGRITY_CHECK_POLICY		_T("200001")				
#define STR_CODE_INTEGRITY_CHECK_FILE		_T("200002")

#endif	//#ifndef _SEND_AGENT__COMMON_DEFINE_H

//TIMER
#define	TIMER_CREATE_TRAY_ICON			4
#define TIMER_SEND_TO_EXIT_CMD			10
#define TIMER_EXIT_AGENT_PROCESS		11
#define TIMER_ALL_THREAD_EXIT_CHECK		12
#define EXIT_COLLECT_AGENT_RETRY_TIME	1000 * 1				//TransAgent 종료시 모든 쓰레드가 정상 종료되었는지 체클할 타이머 시간
#define CHECK_ALL_THREAD_END			1000 * 6				//TransAgent 종료시 모든 쓰레드들이 정상적으로 빠져나왔는지 체크   
#define EXIT_AGENT_RETRY_TIME			1000 * 3			//TransAgent 종료시 모든 쓰레드가 정상 종료되었는지 체클할 타이머 시간

//MSG
#define MSG_UPDATE_COMPLETE_RESTART		WM_USER + 100
#define MSG_DELETE_START				WM_USER + 101