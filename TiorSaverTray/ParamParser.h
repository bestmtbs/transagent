#pragma once
//#include "CommonDefine.h"

class CParamParser
{
public:
	CParamParser(void);
	virtual ~CParamParser(void);
	BOOL ParserGetOffsetReturn(CString _strRecvData, UINT& nCurrentOffset, CString& strToken, int& nDelay);
	BOOL ParserOnlyToken(CString _strRecvData, /*UINT& nCurrentOffset, */CString& strToken);
	BOOL ParserIdentifyLicense(CString _strRecvData, CTypedPtrArray <CPtrArray, DEPARTMENT*>* arrDepartment, CString& strToken);
	//CString CreateJSONRequest(CString _strText, CString _strTrid, CString _strLicenseKey);
	BOOL ParserNoticeSet(CString _strRecvData, CString& strToken);
	CString CreateKeyValueRequest();
	CString CreateKeyValueLogRequest(CString _strFileName);
	CString CreateAgentRegisterRequest(int _nDepartmentID, CString _strAgentName, CString _strAppList);
	CString CreateKeyValueScreenRequest(CString _strFileName);
	CString CreateOffsetKeyValueRequest(CString _strFileName);
	CString CreateInstallKeyValueRequest(CString _strLicenseCode);
	int GetErrorCheck(CString _strRecvData, CString strMessage);
};
