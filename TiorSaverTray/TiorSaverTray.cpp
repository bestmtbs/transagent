
// TiorSaverTray.cpp : Defines the class behaviors for the application.
//
#include "stdafx.h"
#include "TiorSaverTray.h"
//#include "TiorSaverTrayPopupDlg.h"
#include "..\BuildEnv\common\UtilsFile.h"
#include "../BuildEnv/common/UtilParse.h"
#include "../BuildEnv/common/UtilsUnicode.h"
#include "Impersonator.h"
#include "ParamParser.h"
#include "PathInfo.h"
#include "Crypto/Base64.h"
#include "Sntp.h"
//#include "CmsDBManager.h"
#include <fstream>
#include "pipe/C_SendAgent_S_CollectAgent/STOCPipeClient.h"
//#include "TiorSaverTrayRegisterDlg.h"
#include "UtilsProcess.h"
#include "WTSSession.h"
#include "MainFrm.h"
#include "NotifyByBrowserDlg.h"
//#include "SendAgentCommonDefine.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTiorSaverTrayApp

BEGIN_MESSAGE_MAP(CTiorSaverTrayApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
	//ON_WM_TIMER()
END_MESSAGE_MAP()

CTiorSaverTrayApp::CTiorSaverTrayApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_pMemberData = new CMemberData;
	//m_strUrl = STR_SEND_URL;
	m_strUrl = _T("");
	//m_pPTOAPipeServer = NULL;
	//m_bIsFirstUser = TRUE;
	m_pNoticeSet = NULL;
	m_nNoticeSetSize = 0;
	m_nCurrNotice = 0;
	m_strTimeFromServer = _T("");
}

/**
 @brief     소멸자
 @author    kh.choi
 @date      2017-03-02
*/
CTiorSaverTrayApp::~CTiorSaverTrayApp(void)
{
	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
	//SingletonFor//ezLog::DeleteSingleton();
	//CloseLogFile();
}


// The one and only CTiorSaverTrayApp object

CTiorSaverTrayApp theApp;


// CTiorSaverTrayApp initialization

BOOL CTiorSaverTrayApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));
	m_Minidump.install_self_mini_dump();

	//SingletonFor//ezLog::SetStrLogFilenamePrefix(_T("TiorSaverTray"));	// 2017-01-20 kh.choi 파일로그 파일명 prefix 등록. [FileLogSet] 생성자가 아닌 InitInstance() 에서 등록해야한다. 생성자에서는 SingletonFor//ezLog 클래스의 static 맴버 m_strLogFilenamePrefix 가 아직 초기화되기 이전일 수 있다.

	//if (m_pRandomDataGenerateThread)
	//{
	//	m_pRandomDataGenerateThread->ResumeThread();
	//}
	CWTSSession wts;

	DWORD dwLogonSessionID = 0;
	DWORD dwProcessId = GetCurrentProcessId();

	int nTryCnt = 0;
	DWORD dwCurrentLoginSessionID = -1;
	while(TRUE)
	{
		if(nTryCnt > 6)	{	//현재 세션id값을 가져오지 못한다면 현재 활성화된 세션값을 얻어온다.
			exit(0);
		}

		if(ProcessIdToSessionId(dwProcessId,&dwLogonSessionID)) {// 로그온 세션 ID 를 구한다
			dwCurrentLoginSessionID = dwLogonSessionID;
			break;
		}
		nTryCnt++;
		Sleep(500);			
	}

	if(dwCurrentLoginSessionID < 0) {
		UM_WRITE_LOG(_T("[Error] GetCurrent Logon Session Id is -1 "));
		exit(0);
	}

	CString strMutextname = _T("");
	strMutextname.Format(_T("%s_%d"), WTIOR_TRAY_MUTEX_NAME, dwCurrentLoginSessionID);
	if( CProcess::ProcessCreateMutex(strMutextname) == false ) {
		CString strLog = _T("");
		strLog.Format(_T("createmutex fail %d"), GetLastError());
		//MessageBox(NULL, strLog, _T("TiorSaver"), MB_OK);
		exit(0);
	}

	g_cDbgLog.SetLogFileName(L"TiorSaverTray.log");
	g_cDbgLog.InitLog();
	//CheckNotify();
	INT nCnt = 0;
	while (TRUE)
	{
		if (TRUE == NoticeRequest())
			break;
		if (5 <= nCnt)
			return FALSE;
		nCnt++;
	}

	//CString strCheckFirstUser = _T("");
	//strCheckFirstUser.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("register.success"));
	//CString strTestDBFileName = _T("");
	//strTestDBFileName.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), _T("test.db"));
	//if (!FileExists(strCheckFirstUser)) {
	//	m_bIsFirstUser = TRUE;
	//	CreateDB();
	//	CTiorSaverTrayRegisterDlg registerDlg;
	//	registerDlg.DoModal();
	//} else {
		//m_bIsFirstUser = FALSE;
		if (NULL != m_pNoticeSet) {
			if (0 < m_nNoticeSetSize) {
				m_pFrame = new CMainFrame;
				//CNotifyByBrowserDlg dlg;
				m_pMainWnd = m_pFrame;
				//m_pFrame->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL, NULL);
				//m_pFrame->m_pMainDlg = &dlg;
				m_pFrame->CreateEx(0, AfxRegisterWndClass(NULL, NULL, NULL, NULL), _T("TIORTRAY"), WS_POPUP, 0, 0, 100, 100, NULL, NULL);
				m_pFrame->ShowWindow(SW_HIDE);
				m_pMainWnd->UpdateWindow();
				if (m_pFrame) {
					m_pFrame->PostMessage(MSG_POP_UP_START);
				}
			}
		}
	 
	return TRUE;
}

//static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
//	((std::string *)userp)->append((char*)contents, size * nmemb);
//	return size * nmemb;
//}

//CStringA CTiorSaverTrayApp::Utf8_Encode(CStringW strData)
//{
//	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), NULL, 0, NULL, NULL);
//	std::string strTo( size_needed+1, 0 );
//	WideCharToMultiByte                  (CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), &strTo[0], size_needed, NULL, NULL);
//	return strTo.c_str();
//}
//
//CStringW CTiorSaverTrayApp::Utf8_Decode(CStringA strData)
//{
//	int size_needed = MultiByteToWideChar(CP_UTF8, 0,strData.GetString(), -1, NULL, 0);
//	std::wstring wstrTo( size_needed+1, 0 );
//	wstrTo.clear();
//	MultiByteToWideChar (CP_UTF8, 0, strData.GetString(),-1, &wstrTo[0], size_needed+1);
//	return wstrTo.c_str();
//}

//void CTiorSaverTrayApp::CurlJSON()
//{
//	if (!m_bComplete) {
//		return;
//	} else {
//		m_bComplete = FALSE;
//	}
//	std::string readBuffer;
//	CURL *curl;
//	CURLcode res;
//
//	CString strFilePath = _T("");
//	CString strCurrentDay = _T("");
//	CTime cTime = CTime::GetCurrentTime();
//	strCurrentDay = cTime.Format(_T("%Y%m%d"));
//
//	strFilePath.Format(_T("http://192.168.100.112/v1/trans/event"), strCurrentDay);
//	CFile cFile;
//	if( !cFile.Open(strFilePath, CFile::modeRead) )
//	{
//		return;
//	}
//	DWORD dwFileSize = (DWORD)cFile.GetLength();
//	char *pszBuffer = new char[dwFileSize +1];
//	ZeroMemory(pszBuffer, dwFileSize+1);
//
//	cFile.Read(pszBuffer, dwFileSize);
//
//	//strBuffer = pszBuffer;
//	cFile.Close();
//	//char *post = pszBuffer;
//	char * post = "0DCC-713E-D831-C47A";
//
//	//char *url = "http://localhost/index.php/curl";
//	char *install_url = "http://192.168.100.112/v1/agent/license";
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//	if(curl)
//	{
//		struct curl_slist *slist = NULL;
//
//		slist = curl_slist_append(slist, "Accept: application/json");
//		slist = curl_slist_append(slist, "charsets: utf-8");
//		slist = curl_slist_append(slist, "Content-Type: application/json");
//
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
//		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
//		curl_easy_setopt(curl, CURLOPT_HEADER, 0);
//		curl_easy_setopt(curl, CURLOPT_USERAGENT,  "libcurl");
//		//curl_easy_setopt(curl, CURLOPT_URL, url);
//		curl_easy_setopt(curl, CURLOPT_URL, install_url);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30);
//		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post);
//		res = curl_easy_perform(curl);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if(res != CURLE_OK)
//			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
//		else {
//			CString strFileName = _T("");
//			strFileName.Format(_T("backseul.log"));
//			char * hi = "D:\\DSNTECH\\WinTior\\Log\\backseul.log";
//			char * writable = new char[readBuffer.size() + 1];
//			std::copy(readBuffer.begin(), readBuffer.end(), writable);
//			writable[readBuffer.size()] = '\0'; // don't forget the terminating 0
//
//			// don't forget to free the string after finished using it
//			FileWrite(hi, writable);
//			if (writable) {
//				delete[] writable;
//			}
//		}
//
//		/* always cleanup */ 
//		curl_easy_cleanup(curl);
//		curl_slist_free_all(slist);
//	}
//	if (pszBuffer) {
//		delete[] pszBuffer;
//	}
//	curl_global_cleanup();
//	m_bComplete = TRUE;
//}
//
//CString CTiorSaverTrayApp::CurlDelete(CString _strParam, CString _strUrl, INT nErr)
//{
//	BOOL bResult = FALSE;
//	std::string readBuffer;
//	readBuffer.clear();
//	CURL *curl;
//	CURLcode res;
//	//html_context_data data = {0, 0};
//
//	CStringA strUrlA = Utf8_Encode(_strUrl);
//	CStringA strReadBufferA;
//	CString strReadBufferW = _T("");
//
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//	if (curl)
//	{
//		CString strTokenHeader = _T("");
//		curl_slist* header = NULL ;
//		strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//		CStringA strDataA = Utf8_Encode(_strParam);
//		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//		header = curl_slist_append( header ,  strTokenHeaderA);
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
//		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strDataA);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if (readBuffer.size() > 0)
//		{
//			strReadBufferA = readBuffer.c_str();
//			if (strReadBufferA.GetLength() > 0) {
//				strReadBufferW = Utf8_Decode(strReadBufferA);
//			}
//			//SetToken((CString)cs);
//		}
//		if(res != CURLE_OK) {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("postfail"));
//			strCount.Format(_T("[CurlPost]URL: %s, Param: %s res: %d [line: %d, function: %s, file: %s]"), _strUrl, _strParam, res, __LINE__, __FUNCTIONW__, __FILEW__);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			DROP_TRACE_LOG(_T("TiorSaver_postfail"), strCount);
//			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
//		}
//		curl_slist_free_all(header) ;
//		curl_easy_cleanup(curl);
//	}
//	curl_global_cleanup();
//	return strReadBufferW;
//}
//
//CString CTiorSaverTrayApp::CurlGET(CString _strUrl, INT nErr)
//{
//	BOOL bResult = FALSE;
//	std::string readBuffer;
//	readBuffer.clear();
//	CURL *curl;
//	CURLcode res;
//	//html_context_data data = {0, 0};
//
//	CStringA strUrlA = Utf8_Encode(_strUrl);
//	CStringA strReadBufferA;
//	CString strReadBufferW = _T("");
//
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//	if(curl)
//	{
//		CString strTokenHeader = _T("");
//		curl_slist* header = NULL ;
//		strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//		header = curl_slist_append( header ,  strTokenHeaderA);
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if (readBuffer.size() > 0)
//		{
//			strReadBufferA = readBuffer.c_str();
//			strReadBufferW = Utf8_Decode(strReadBufferA);
//			//SetToken((CString)cs);
//		}
//		if(res != CURLE_OK) {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("TiorSaver_getfail"));
//			strCount.Format(_T("[CurlGET]%s res: %d [line: %d, function: %s, file: %s]"), _strUrl, res, __LINE__, __FUNCTIONW__, __FILEW__);
//			DROP_TRACE_LOG(_T("TiorSaver_getfail"), strCount);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
//		}
//		curl_slist_free_all(header) ;
//		curl_easy_cleanup(curl);
//	}
//	curl_global_cleanup();
//	return strReadBufferW;
//}
//
//CString CTiorSaverTrayApp::CurlPost(CString _strParam, CString _strUrl, INT nErr)
//{
//	BOOL bResult = FALSE;
//	std::string readBuffer;
//	readBuffer.clear();
//	CURL *curl;
//	CURLcode res;
//	//html_context_data data = {0, 0};
//
//	CStringA strUrlA = Utf8_Encode(_strUrl);
//	CStringA strReadBufferA;
//	CString strReadBufferW = _T("");
//
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//    if (curl)
//	{
//		CString strTokenHeader = _T("");
//		curl_slist* header = NULL ;
//		strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//		CStringA strDataA = Utf8_Encode(_strParam);
//		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//		header = curl_slist_append( header ,  strTokenHeaderA);
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//		curl_easy_setopt(curl, CURLOPT_POST, 1);
//		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strDataA);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if (readBuffer.size() > 0)
//		{
//			strReadBufferA = readBuffer.c_str();
//			if (strReadBufferA.GetLength() > 0) {
//				strReadBufferW = Utf8_Decode(strReadBufferA);
//			}
//			//SetToken((CString)cs);
//		}
//		if(res != CURLE_OK) {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("postfail"));
//			strCount.Format(_T("[CurlPost]URL: %s, Param: %s res: %d [line: %d, function: %s, file: %s]"), _strUrl, _strParam, res, __LINE__, __FUNCTIONW__, __FILEW__);
//			DROP_TRACE_LOG(_T("TiorSaver_postfail"), strCount);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
//		}
//		curl_slist_free_all(header) ;
//		curl_easy_cleanup(curl);
//	}
//	curl_global_cleanup();
//	return strReadBufferW;
//}

//CString CTiorSaverTrayApp::HttpsPost(CString _strToken, CString _strParam, CString _strUrl)
//{
//	BOOL bResult = FALSE;
//	CString strPolicy = _T("");
//	CString strLog = _T("");
//	CHttpsTConnect logSend;
//	logSend.SetServerInfo(_strUrl);
//	logSend.SetTokenFromAgent(_strToken);
//	logSend.m_bGet = FALSE;
//	CString strCountFile = _T("");
//	CString strCount = _T("");
//	DWORD dwBytesWritten = 0;
//	if( logSend.HttpConnect())
//	{
//		strLog.Format(_T("[curltest]%s %s [line: %d, function: %s, file: %s]"), _strParam, strPolicy, __LINE__, __FUNCTIONW__, __FILEW__);
//		UM_WRITE_LOG(strLog);
//	} else {
//		strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("fail"));
//		strCount.Format(_T("[%d] error code: %d [line: %d, function: %s, file: %s]"), ::GetCurrentProcessId(), GetLastError(), __LINE__, __FUNCTIONW__, __FILEW__);
//	}
//	WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//	return strPolicy;
//}
//CString CTiorSaverTrayApp::HttpsGET(CString _strToken, CString _strUrl)
//{
//	BOOL bResult = FALSE;
//	CString strPolicy = _T("");
//	CString strLog = _T("");
//	CHttpsTConnect logSend;
//	logSend.SetServerInfo(_strUrl);
//	logSend.SetTokenFromAgent(_strToken);
//	logSend.m_bGet = TRUE;
//	if( logSend.HttpConnect(_T("")))
//	{
//		strPolicy = logSend.HttpReadRequest(_T(""));
//		strLog.Format(_T("[curltest]%s [line: %d, function: %s, file: %s]"), strPolicy, __LINE__, __FUNCTIONW__, __FILEW__);
//		UM_WRITE_LOG(strLog);
//	}
//	return strPolicy;
//}



void CTiorSaverTrayApp::SetToken(CString _strToken)
{
	CString strValue = _strToken;
	if (strValue.GetLength() <= 0) {
		CString strLog = _T("");
		strLog.Format(_T("[curltest]invalid token [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
		return;
	}
	if (0 != m_strToken.Compare(_strToken))
	{
		m_strToken = _strToken;
		CString strTokenFilePath = _T("");
		strTokenFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("my.token"));
		DWORD dwBytesWritten = 0;
		//if (!WriteFileTxt(strTokenFilePath, m_strToken, nFlag)) {
		
		//CStringA strToWriteA = m_curl.Utf8_Encode(_strToken);
		int nCnt = 0;
		while(!WriteFileExample(strTokenFilePath, _strToken, dwBytesWritten) || 10 == nCnt) {
			nCnt++;
			CString strLog = _T("");
			strLog.Format(_T("[curltest]invalid token file [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
	}
}

CString CTiorSaverTrayApp::GetToken()
{
	if (m_strToken.IsEmpty())
	{
		CString strTokenFilePath = _T("");
		CString strReadData = _T("");
		strTokenFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("my.token"));
		ReadAnsiFileTxt(strTokenFilePath, strReadData);
		m_strToken = strReadData;
	}
	return m_strToken;
}

//BOOL CTiorSaverTrayApp::SendData(CString _strTargetFile, UINT& _nCurrentOffset)
//{
//	if (!m_bComplete) {
//		return FALSE;
//	} else {
//		m_bComplete = FALSE;
//	}
//	BOOL bResult = FALSE;
//	CString strLog = _T("");
//	CString strFilePath = _T("");
//	CString strBuffer = _T("");
//	CString strCurrentDay = _T("");
//	CStringArray arrData;
//	arrData.RemoveAll();
//	CTime cTime = CTime::GetCurrentTime();
//	strCurrentDay = cTime.Format(_T("%Y%m%d"));
//
//	//if (m_bIsRandom) {
//	//	strFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), RANDOMLOG);
//	//} else {
//	//	strFilePath.Format(_T("D:\\DSNTECH\\WinTior\\Log\\%s\\WtiorAgent_LOG.log"), strCurrentDay);
//	//}
//	strFilePath = _strTargetFile;
//	//CFile cFile;
//	HANDLE hRead = CreateFile( strFilePath , GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
//	if(INVALID_HANDLE_VALUE == hRead) {
//		bResult = FALSE;
//	} else {
//		DWORD dwFileSize = GetFileSize(hRead,  NULL);
//		DWORD nNumberOfBytesToRead = dwFileSize + 1 - _nCurrentOffset;
//		DWORD lpNumberOfBytesRead;
//		if (dwFileSize + 1 <= _nCurrentOffset) {
//			CloseHandle(hRead);
//			m_bComplete = TRUE;
//			return FALSE;
//		}
//		char *pszBuffer = new char[nNumberOfBytesToRead];
//		memset(pszBuffer, 0x00, nNumberOfBytesToRead);
//
//		//cFile.Seek(m_nCurrentPos, CFile::begin);
//		DWORD dwPtr = SetFilePointer( hRead, _nCurrentOffset, NULL, FILE_BEGIN ); 
//		if (dwPtr == INVALID_SET_FILE_POINTER) { 
//			// Obtain the error code. 
//			DWORD dwError = GetLastError() ; 
//			// TODO : Deal with failure 
//			// . . . 
//
//		} // End of error handler 
//		//m_nTempCurrentPos = cFile.Read(pszBuffer, dwFileSize - m_nCurrentPos);
//		BOOL bReadResult = ReadFile(hRead, pszBuffer, nNumberOfBytesToRead, &lpNumberOfBytesRead, NULL);
//		if (!bReadResult) {
//			//CloseHandle(hRead);
//			//return;
//			strLog.Format(_T("[curltest]failed to read file"));
//			OutputDebugString(strLog);
//		}
//		m_nTempCurrentPos = _nCurrentOffset + lpNumberOfBytesRead;
//		CloseHandle(hRead);
//		strLog.Format(_T("[curltest]m_nCurrentPos: %d m_nTempCurrentPos: %d, dwFileSize: %d"), _nCurrentOffset, m_nTempCurrentPos, dwFileSize);
//		OutputDebugString(strLog);
//		//cFile.Close();
//		strBuffer = pszBuffer;
//		if (pszBuffer) {
//			delete[] pszBuffer;
//		}
//		CParamParser parser;
//		CString strParam = _T("");
//		strParam = parser.CreateKeyValueRequest(_T("NOYE5HO"), _T("uuid2"), _T("1.0"), strFilePath);
//		CString strToken = GetToken();
//		CString strReturn = _T("");
//		strLog.Format(_T("[curltest]%s | %s"), strParam, strBuffer);
//		OutputDebugString(strLog);
//		if (SEND_TYPE_CURL == m_nSendType) {
//			strReturn = CurlPost(strToken, strParam, m_strUrl);
//		} else if (SEND_TYPE_HTTPSTCONNECT == m_nSendType) {
//			strReturn = HttpsPost(strToken, strParam, m_strUrl);
//		}
//		UINT nCurrentOffset = 0;
//		CString strTokenFromReturn = _T("");
//		if (parser.ParserAfterLogSend(strReturn, nCurrentOffset, strTokenFromReturn)) {
//			SetToken(strTokenFromReturn);
//			//CString strCountFile = _T("");
//			//CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("success.count"));
//			//m_nSuccessCount++;
//			//strCount.Format(_T("[%d]%d"), ::GetCurrentProcessId(), m_nSuccessCount);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten);
//			_nCurrentOffset = m_nTempCurrentPos;
//			bResult = TRUE;
//		} else {
//			//m_bComplete = TRUE;
//			//MessageBox(NULL, strReturn, _T("send failed"), MB_OK);
//			bResult = FALSE;
//		}
//	}
//	m_bComplete = TRUE;
//	return bResult;
//}

//static size_t
//WriteMemoryCallback(BYTE *contents, size_t size, size_t nmemb, BYTE *userp)
//{
//	size_t realsize = size * nmemb;
//	struct MemoryStruct *mem = (struct MemoryStruct *)userp;
//
//	mem->memory = (BYTE*)realloc(mem->memory, mem->size + realsize + 1);
//	if(mem->memory == NULL) {
//		/* out of memory! */ 
//		printf("not enough memory (realloc returned NULL)\n");
//		return 0;
//	}
//
//	memcpy(&(mem->memory[mem->size]), contents, realsize);
//	mem->size += realsize;
//	mem->memory[mem->size] = 0;
//
//	return realsize;
//}


//BOOL CTiorSaverTrayApp::SendBinaryData(CString _strTargetFile, UINT& _nCurrentOffset)
//{
//	//BOOL bResult = FALSE;
//	//if (!m_bComplete) {
//	//	return FALSE;
//	//} else {
//	//	m_bComplete = FALSE;
//	//}
//	BOOL bResult= FALSE;
//	CFile cFile(_strTargetFile, CFile::modeRead|CFile::typeBinary);
//	UINT nFileLen = (UINT)cFile.GetLength();
//	//UINT nBytesToRead = nFileLen - _nCurrentOffset;
//	UINT nBytesRead = 1;
//	//BYTE* pEncodedFileData;
//	CString strUrl= _T("http://192.168.100.112/v1/trans/screen");
//	CString strReturn = _T("");
//	cFile.Seek(_nCurrentOffset, CFile::begin);
//	do
//	{
//		BYTE* pFileData= new BYTE[SEND_MAX_SIZE];
//		nBytesRead = cFile.Read(pFileData, SEND_MAX_SIZE);
//		CParamParser parser;
//		CString strParam = parser.CreateKeyValueScreenRequest(_T("NOYE5HO"), _T("uuid2"), _T("1.0"), _strTargetFile);
//		CBase64 base64;
//		CString strEncodedFileData = base64.base64encode(pFileData, nBytesRead);
//		if (NULL != pFileData) {
//			delete[] pFileData;
//			pFileData = NULL;
//		}
//		if (0 <= nBytesRead) {
//			strParam = strParam + strEncodedFileData;
//			if (SEND_TYPE_CURL == m_nSendType) {
//				strReturn = CurlPost(GetToken(), strParam, strUrl);
//			} else if (SEND_TYPE_HTTPSTCONNECT == m_nSendType) {
//				strReturn = HttpsPost(GetToken(), strParam, strUrl);
//			}
//			UINT nCurrentOffset = 0;
//			CString strTokenFromReturn = _T("");
//			if (parser.ParserAfterLogSend(strReturn, nCurrentOffset, strTokenFromReturn)) {
//				//m_nCurrentPos += m_nTempCurrentPos;
//				SetToken(strTokenFromReturn);
//				//CString strCountFile = _T("");
//				//CString strCount = _T("");
//				//DWORD dwBytesWritten = 0;
//				//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("success.count"));
//				//m_nSuccessCount++;
//				//strCount.Format(_T("[%d]%d"), ::GetCurrentProcessId(), m_nSuccessCount);
//				//WriteFileExample(strCountFile, strCount, dwBytesWritten);
//				_nCurrentOffset += nBytesRead;
//				bResult = TRUE;
//			} else {
//				//m_bComplete = TRUE;
//				//MessageBox(NULL, strReturn, _T("send failed"), MB_OK);
//				bResult = FALSE;
//			}
//		}
//	}
//	while (nBytesRead > 0 || TRUE == bResult);
//	cFile.Close();
//	//CStringA strReadBufferA;
//	//CString strReadBufferW = _T("");
//	//CStringA strEncodedFileDataA = Utf8_Encode(strEncodedFileData);
//	//UINT nEncodedFileDataA = strEncodedFileDataA.GetLength();
//	//CStringA strData = Utf8_Encode(strParam);
//	//std::string readBuffer;
//	//CURL *curl;
//	//CURLcode res;
//	//struct MemoryStruct chunk;
//	//UINT nPostDataLen= strData.GetLength() + 1 + nFileLen;
//	//UINT nSizeData = strData.GetLength() + 1;
//	//BYTE *postthis = new BYTE[nPostDataLen];
//	//memset(postthis, 0x00, nPostDataLen);
//	//memcpy_s(postthis, nSizeData, strData, nSizeData);
//	//memcpy_s((postthis + nSizeData - 1), nFileLen, pFileData, nFileLen);
//	//long lPostSize = (long)_msize(postthis);
//
//	//chunk.memory = (BYTE*)malloc(1);  /* will be grown as needed by realloc above */ 
//	//chunk.size = 0;    /* no data at this point */ 
//
//	//curl_global_init(CURL_GLOBAL_ALL);
//	//curl = curl_easy_init();
//	//if(curl) {
//	//	CString strTokenHeader = _T("");
//	//	curl_slist* header = NULL ;
//	//	strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//	//	CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//	//	header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//	//	header = curl_slist_append( header ,  strTokenHeaderA);
//	//	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//	//	curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//	//	curl_easy_setopt(curl, CURLOPT_POST, 1);
//	//	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//	//	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//	//	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strData);
// //		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strData.GetLength());
//	//	res = curl_easy_perform(curl);
//	//	if (readBuffer.size() > 0)
//	//	{
//	//		strReadBufferA = readBuffer.c_str();
//	//		strReadBufferW = Utf8_Decode(strReadBufferA);
//	//	}
//	//	if(res != CURLE_OK) {
//	//		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
//	//	} else {
//	//		DWORD dwBytesWritten = 0;
//	//		CString strFileName = _T("");
//	//		strFileName.Format(_T("backseul.log"));
//	//		char * hi = "D:\\dsntech\\WinTior\\Log\\backseul.log";
//	//		char * writable = new char[readBuffer.size() + 1];
//	//		std::copy(readBuffer.begin(), readBuffer.end(), writable);
//	//		writable[readBuffer.size()] = '\0'; // don't forget the terminating 0
//
//	//		// don't forget to free the string after finished using it
//	//		//WriteFileExample((CString)hi, writable, dwBytesWritten);
//	//		if (writable) {
//	//			delete[] writable;
//	//		}
//	//	}
//	//curl_slist_free_all(header);
//	//curl_easy_cleanup(curl);
//
//	////free(chunk.memory);
//
//	///* we're done with libcurl, so clean it up */ 
//	//curl_global_cleanup();
//	//}
//	//m_bComplete = TRUE;
//	return bResult;
//}

void CTiorSaverTrayApp::Write_Index(CString _strPath, CString _strHash, CString _strVersion)
{
	BOOL bRet ;
	CString strIni_Path=L"", strIndex, strSection = _T("");
	strSection.Format( L"%s", ExtractFileName(_strPath));
	strSection.Remove(_T('\"'));
	strIni_Path.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("UDownload\\TSVersion.ini"));
	bRet = WritePrivateProfileString( strSection, _T("path"), _strPath.GetBuffer(0) ,strIni_Path);
	bRet = WritePrivateProfileString( strSection, _T("hash"), _strHash.GetBuffer(0) ,strIni_Path);
	bRet = WritePrivateProfileString( strSection, _T("ver"), _strVersion.GetBuffer(0) ,strIni_Path);
	if( !bRet )
	{
		//필요시 로그를 남길것
	}

}

//void CTiorSaverTrayApp::CheckNotify()
//{
//	if (NoticeRequest()) {
//		if (NULL != m_pNoticeSet) {
//			if (0 < m_nNoticeSetSize) {
//				if (m_pFrame) {
//					m_pFrame->PostMessage(MSG_POP_UP_START);
//				}
//			}
//		}
//	
//	}
//}

BOOL CTiorSaverTrayApp::PipeServerTime()
{
	if (m_curl.m_strTimeFromServer.IsEmpty())
		return FALSE;
	if (m_strTimeFromServer.IsEmpty())
		return FALSE;
	HANDLE hThread;
	DWORD dwUIThreadID = 0;
	hThread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PipeServerTimeThread, this, 0, &dwUIThreadID);
	CloseHandle(hThread);
	return TRUE;
}

BOOL CTiorSaverTrayApp::NoticeRequest()
{
	BOOL bResult = FALSE;
	CParamParser parser;
	CString strParam = _T("");
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strToken = _T("");
	strParam = parser.CreateKeyValueRequest();
	strUrl.Format(_T("%s/v1/popup?%s"), theApp.m_pMemberData->GetServerHostURL(), strParam);
	int nErr = 0;
	int nCnt = 0;

	while (TRUE) {
		strReturn = theApp.m_curl.CurlGET(strUrl, GetToken());

		if (parser.ParserNoticeSet(strReturn, strToken)) {
			bResult = TRUE;
			theApp.SetToken(strToken);
			if (!theApp.m_strTimeFromServer.IsEmpty()) {
				HANDLE hThread;
				DWORD dwUIThreadID = 0;
				hThread= ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PipeServerTimeThread, this, 0, &dwUIThreadID);
				CloseHandle(hThread);
			}
			break;
		} else {
			if (strToken.IsEmpty()) {
				if (0 == nErr) {
					strToken.Format(_T("%s"), strReturn);
				} else {
					strToken.Format(_T("%d"), nErr);
				}
			}
			if (m_pFrame) {
				m_pFrame->SetTimer(TIMER_POP_UP_PROCESS, POP_UP_PROCESS_RETRY_TIME, NULL);
			}
			if (nCnt >= 10) {
				AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
			}
			nCnt++;
		}
		Sleep(1000);
	}
	return bResult;
}

BOOL WINAPI PipeServerTimeThread(LPVOID lpParam)
{
	CTiorSaverTrayApp* pWnd = reinterpret_cast<CTiorSaverTrayApp*>(lpParam);
	BOOL bResult = TRUE;
	SHARE_DATA_WITH_COLLECT_AGENT* pShareCollectData = new SHARE_DATA_WITH_COLLECT_AGENT;
	CWTSSession wts;
	CString strSTOCSysPipeName = _T("");
	CString strSTOCUsrPipeName = _T("");
	strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
	strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, wts.GetCurrentLogonSessionID());
	CSTOCPipeClient STOCSysPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
	CSTOCPipeClient STOCUsrPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
	pShareCollectData->dwAct = ACT_SYNC_SERVER_TIME;
	//memcpy(pShareCollectData->szTimeOnServer, pWnd->m_strTimeFromServer.GetBuffer(0), SERVERTIME_SIZE);
	_tcscpy_s(pShareCollectData->szTimeOnServer, pWnd->m_strTimeFromServer.GetString());
	if (TRUE == STOCSysPipeClient.OnSetSendData(pShareCollectData)) {
		if (FALSE == STOCSysPipeClient.PipeClientStartUp()) {
			bResult = FALSE;
		}
	} else {
		bResult = FALSE;
	}
	if (TRUE == STOCUsrPipeClient.OnSetSendData(pShareCollectData)) {
		if (FALSE == STOCUsrPipeClient.PipeClientStartUp()) {
			bResult = FALSE;
		}
	} else {
		bResult = FALSE;
	}
	if (bResult) {
	} else {
	}
	pWnd->m_strTimeFromServer = _T("");
	return bResult;
}