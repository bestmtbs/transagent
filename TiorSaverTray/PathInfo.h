
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
@file      PathInfo.h
@brief     CPathInfo 정의 파일
@author    hang ryul lee
@date      create 2011.04.01
*/
#pragma once

/************************registry 경로*********************************************************/
#define SOFTWARE_VMS_SITESERVER_GENERAL         _T("SOFTWARE\\VMS\\3.0\\SITESERVER\\GENERAL")
#define SOFTWARE_VMS_SITECLIENT_GENERAL         _T("SOFTWARE\\VMS\\3.0\\SITECLIENT\\GENERAL")
#define INSTALL_DIR                             _T("InstallDir")
#define DOWN_DIR                                _T("DownDir")
#define DEPLOY_DIR                              _T("DeployDir")
#define BACKUP_DEPLOY_DIR                       _T("BackupDeployDir")
#define SOFTWARE_HAURI_COMMON_UPDATE            _T("SOFTWARE\\HAURI\\COMMON\\UPDATE")

/************************사용자 입력 Windows format********************************************/
#define WINDOWS_DIR_FORMAT                       _T("%WINDIR%")
#define WINDOWS_SYSTEM_DIR_FORMAT                _T("%WINSYSDIR%")
#define WINDOWS_SYSTEM86_DIR_FORMAT              _T("%WINSYSDIRX86%")
#define COMMON_FILES_DIR_FORMAT                  _T("%COMMONFILESDIR%")
#define PROGRAM_FILES_DIR_FORMAT                 _T("%PROGRAMFILESDIR%")
#define PROGRAM_FILES_X86_DIR_FORMAT             _T("%PROGRAMFILESDIRX86%")
#define COMMON_DESKTOP_FORMAT                    _T("%DESKTOP%")
#define COMMON_PROGRAM_FORMAT                    _T("%PROGRAMMENU%")
#define COMMON_START_MENU_FORMAT                 _T("%STARTMENU%")
#define COMMON_STARTUP_FORMAT                    _T("%STARTUP%")
#define ROOT_DIR_FORMAT									_T("%ROOT%")
#define WINDOWS_SYSTEM32_DIR_FORMAT                _T("%WINSYSDIR32%")
#define WINDOWS_RECYCLE_BIN								_T("%RECYCLE%")
#define INTERNET_TEMP_FILES									_T("%TEMP%")
#define INTERNET_COOKIES										_T("%COOKIES%")


/************************사용자 입력 Hauri format***********************************************/
#define INSTALL_DIR_FORMAT                       _T("%INSTALLDIR%")
#define AGENT_DIR_FORMAT                         _T("%AGENTDIR%")
#define DEPLOY_DIR_FORMAT                        _T("%DEPLOYDIR%")
#define DEPLOY_BACKUP_DIR_FORMAT                 _T("%BACKUPDEPLOYDIR%")
#define AGENT_DOWN_DIR_FORMAT                    _T("%AGENTDOWNDIR%")
#define AGENT_UPDATE_DIR_FORMAT                  _T("%AGENTUPDATEDIR%")
#define TEMP_DIR_FORMAT                          _T("%TEMPDIR%")
#define PATTERN_DIR_FORMAT                       _T("%PATTERNDIR%")
#define OTHDOWN_DIR_FORMAT                       _T("%OTHDOWNDIR%")
#define ENGINE_DIR_FORMAT                        _T("%ENGINEDIR%")
#define COMPONENTS_DIR_FORMAT                    _T("%COMPONENTSDIR%")
#define VISMS_DIR_FORMAT                         _T("%VISMSDIR%")
#define DOWN_DIR_FORMAT                          _T("%DOWNDIR%")
#define PATCH_DIR_FORMAT                         _T("%PATCHDIR%")
#define HAURI_UPDATE_DIR_FORMAT                  _T("%HAURIUPDATE%")
#define ACTIVE_REPAIR_DIR_FORMAT                 _T("%ACTIVEREPAIR%")
#define ALERT_DIR_FORMAT                         _T("%ALERTDIR%")
#define PLUGIN_DIR_FORMAT                        _T("%PLUGINDIR%")

/************************ 기본 DEFAULT PATH******************************************************/
#define DEFAULT_DISK_PATH                        _T("c:\\")
#define DEFAULT_WINDOWS_PATH                     _T("Windows\\")
#define DEFAULT_PROGRAM_PAHT                     _T("Program Files\\")
#define DEFAULT_PROGRAM_X86_PATH                 _T("Program Files (X86)\\")
#define DEFAULT_SYSTEM_FILES_PATH                _T("System32\\")
#define DEFAULT_SYSTE_X86_FILES_PATH             _T("WowSys\\")
#define DEFAULT_COMMON_PATH                      _T("Common Files\\")
#define DEFAULT_ALL_USERS_PATH                   _T("Documents and Setting\\All Users\\")
#define DEFAULT_APPLICATION_PATH                 _T("ApplicationData\\")
#define DEFAULT_START_MENU_PATH                  _T("ProgramData\\Microsoft\\Windows\\Start Menu\\")

/*-----------------------하우리 DEFAULT PATH*********************************************************/
#define DEFAULT_HAURI_SERVER_INSTALL_PATH        _T("Hauri\\SiteServer\\")
#define DEFAULT_HAURI_SERVER_DOWN_PATH           _T("Hauri\\SiteServer\\Down\\")
#define DEFAULT_HAURI_CLIENT_INSTALL_PATH        _T("Hauri\\SiteClient\\")
#define DEFAULT_HAURI_CLIENT_DOWN_PATH           _T("Hauri\\SiteClient\\Down\\")
#define DEFAULT_HAURI_SERVER_DEPLOY_PATH         _T("Hauri\\SiteServer\\Deploy\\")
#define DEFAULT_HAURI_SERVER_BACKUP_DEPLOY_PATH  _T("Hauri\\SiteServer\\BackupDeploy\\")
#define DEFAULT_HAURI_COMMON_UPDATE_PATH         _T("Hauri\\Common\\Update\\")

/************************폴더 생성길이 최대 제한***************************************/
#define MAXLEN_PATH                                                                    260

typedef HRESULT (STDAPICALLTYPE *SHGETFOLDERPATH)(HWND, int, HANDLE, DWORD, LPTSTR);
typedef UINT ( WINAPI *GETSYSTEMWINDOWSDIRECTORY)(LPTSTR, UINT);


/**    
@class     CPathInfo 
@brief     현재 시스템 경로 및 클라이언트/서버 설치 경로을 가능하게 한다 
@author    Hang Ryul Lee
@date      create 2011.4.1 

@note      현재 시스템 경로를 가져올수 있다
@note      현재 설치된 클라이언트 경로를 가져올수 있다
@note       현재 설치된 서버 경로를 가져올수 있다
@note       폴더 경로를 구하는 메소드를 static 함수로 선언하여 참조 \n

@note      CSystemPathInfo 클래스 주요 메서드 를 static으로 선언 한 이유 
@note       static 함수로 선언한 이유는 클래스 스코프 영역에 포함되지 않아도(클래스를 생성하지 않아도) \n
@note      되는 utility 개념의 메소드 이므로 static 으로 선언하여 사용 \n
@note      static 으로 선언된 함수는 컴파일 시점에 전역데이타에 바인딩 되기 때문에 객체가 생성되서\n
@note      run time 에만 그 위치를 결정 할수 있는 static 으로 선언되지 않은 멤버에 접근 제한 \n
@note       멤버 함수 인자로 this 포인터가 넘어오지 않기 때문에 인자 한개가 줄어드는 효과가 발생하여 \n
@note      빈번 하게 호출되는 함수의 경우 성능 향상 도모
*/

class CPathInfo
{        
public:

    CPathInfo(void);
    virtual ~CPathInfo(void);

public:
    /************************Windows File Path Return Function*******************************************/
    static CString GetAdminTool();                /** administrative tools directory for an individual user */
    static CString GetCommonAdminTool();          /** administrative tools directory for all users */
    static CString GetCommonProgram();            /** directory  contains on the Start menu for all users */
    static CString GetCommonStartMenu();          /** on the Start Menu for all users */
    static CString GetCommonStartUp();            /** on the Startup folder for all users */
	static CString GetRoot();							/** root path */
    static CString GetProgram();                  /** the program files directory */
    static CString GetProgramX86();               /**64bit the program files directory */
    static CString GetAppData();                  /** the file system directory  that serves as a common repository for application-specific data */
    static CString GetCommonAppData();            /** the file system directory that contains application data for all users */
    static CString GetCommonDeskTop();            /** The file system directory that contains files and folders that appear on the desktop for all users */
    static CString GetInternetCache();            /** The file system directory that serves as a common repository for temporary Internet files */
    static CString GetLocalAppData();             /** The file system directory that serves as a data repository for local (nonroaming) applications */
    static CString GetDocument();                 /** The file system directory that contains documents for an individual user*/
    static CString GetCommonDocument();           /** The file system directory that contains documents that are common to all users*/
    static CString GetCommonTemplate();           /** The file system directory that contains the templates that are available to all users.*/
    static CString GetTemplate();                 /** The file system directory that contains the templates for an individual user*/
    static CString GetCookie();                   /** The file system directory that serves as a common repository for Internet cookies.*/
    static CString GetDeskTop();                  /** The file system directory used to physically store file objects on the desktop */
    static CString GetHistory();                  /** The file system directory that serves as a common repository for Internet history items.*/
    static CString GetSystem();                   /** The Windows System folder */
	static CString GetSystem32();                   /** The Windows System32 folder */
    static CString GetSystemX86();                /** 64bit The Windows System folder */
    static CString GetCommon();                   /** The file system directory that contains common File */
    static CString GetCommonX86();                /** The file system directory that contains common File under 64bit */
    static CString GetProfile();                  /** The user's profile folder */
    static CString GetWindows();                  /** The Windows folder */
    static CString GetDisk();                     /** Disk Volume Name */
	static CString GetRecycle();
	static CString GetWow64Program();
	static CString GetOfficeConfigPath(BOOL bLFC=TRUE);		//hhh : 2013. 05.28 

    static void ConvertWindowsFilePath(CString &_sFilePath);
    static void ConvertHauriFilePath(CString &_sFilePath);
    static void ConvertDestPath(CString &_sFilePath);
    static bool IsProperFilePath(CString &_sSource); /** 파일 적절성 검사 */

    /************************Harui File Path Return Function*******************************************/
    static CString GetServerInstall();         /**Hauri Server Install Path */
    static CString GetServerDown();            /** Hauri Server DownLoad Path */
    static CString GetClientInstall();         /** Hauri Client DownLoad Path */
    static CString GetClientDown();            /** Hauri Client DownLoad Path */
    static CString GetServerDeploy();          /** Hauri Server Deploy Path */
    static CString GetServerBackupDeploy();    /** Hauri Client Backup Deploy Path */
    static CString GetClientUpdate();          /** Hauri Client Update Path */
    static CString GetServerPattern();         /** Hauri Server Pattern Path */
    static CString GetServerOthDown();         /** Hauri Server OthDown Path */
    static CString GetServerEngine();          /** Hauri Server Engine Path */
    static CString GetServerComponent();       /** Hauri Server Component Path */
    static CString GetServerVISMS();           /** Hauri Server VISMS Path */
    static CString GetServerPatch();           /** Hauri Server Patch Path */
    static CString GetClientHauriUpdate();     /** Hauri ICR업데이트시 사용하는 Client Update폴더 */
    static CString GetServerAcRepair();        /** Hauri Server Active Repair Path */
    static CString GetServerAlert();           /** Hauri Server Alert Path */
    static CString GetServerPlugin();          /** Hauri Server Plugin Path */

	static CString GetClientInstallPath(BOOL bLFC=TRUE);	    /** ITCMS Client Install Path */	
	static CString GetClientInstallPath64(); /** ITCMS Client Install Path 64 */	
	static CString GetLogFilePath();		   /** ITCMS Client Log File Path */	
	static CString GetDBFilePath();           /** ITCMS Client DB File Path */	
	static CString GetWinUpdatePath();    /** ITCMS Win update folder path*/
	static CString GetKeyBackupPath();      /**ITCMS Key backup folder path*/
	static CString GetClientUpdatePath();  /**ITCMS  Client update folder path*/
	static CString GetOfficeRootPath(BOOL bLFC=TRUE); /**OfficeSafer intall root folder path*/

	static CString GetOfficeConfig_PTP_Path(BOOL bLFC=TRUE);		/** OfficeSafer install root folder PTP path* hhh:2013.10.01**/			
	static CString GetOfficeSaferZone_Path(BOOL bLFC=TRUE);		/** OfficeSaferZone Path -  hhh:2013.10.01**/			
    static CString GetAllUserTempPath(BOOL bLFC=TRUE);					/** Get All Users Path hhh:2013.10.28**/
private:
    /************************File Path apposite Inspect Function*******************************/
    static void GetFolderPath(UINT _nType, CString  &_sFilePath); /** 타입별 file path */
    static CString GetFilesFolder(CString _sDllName, UINT _nType);
    static CString GetWindowsFolder98();
    static CString GetWindowsFolder2000();
};
