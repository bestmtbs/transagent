/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file       CAgentTray.cpp
 @brief     AgentTray  구현 파일
 @author   jhlee
 @date      create 2011.06.15
 */
#include "StdAfx.h"
#include "AgentTray.h"
#include "TiorSaverTray.h"
#include "IniFileEx.h"
#include "PathInfo.h"
//#include "resource.h"

/********************************************************************************
 @class     CAgentTray
 @brief     클라이언트 트레이 등록
 @note     CTray  클래스 상속 받아 구현 
*********************************************************************************/

/**
 @brief     생성자
 @author   JHLEE
 @date      2011.06.15
 @note      초기화 작업
*/
CAgentTray::CAgentTray(void)
{
}


/**
 @brief     소멸자
 @author   JHLEE
 @date      2011.06.15
*/
CAgentTray::~CAgentTray(void)
{
}

/**
@brief     마우스 왼쪽 더블클릭 이벤트
@author   JHLEE
@date      2011.12.23
@note      트레이 메뉴에서 마우스 왼쪽 더블클릭시의 액션
				메인페이지 출력함
*/

void CAgentTray::OnTrayLButtonDbClick()
{
}

void CAgentTray::OnTrayLButtonDown()
{

}


void CAgentTray::OnTrayLButtonUp()
{

}

void CAgentTray::OnTrayRButtonDown()
{

}

/**
 @brief     마우스 오른쪽 클릭 이벤트
 @author   JHLEE
 @date      2011.06.15
 @note      트레이 메뉴에서 마우스 오른쪽 클릭시의 액션
                 메뉴 출력함
*/
void CAgentTray::OnTrayRButtonUp()
{
	//if(theApp.m_pFrame->m_bMainEnable == FALSE)
	//	return;


	CString strIni = CPathInfo::GetClientInstallPath()+_T("CmsVer.ini");

	CIniFileEx	CmsVerIni(strIni);	
	int nVal = CmsVerIni.ReadInt(_T("CmsVersion"), _T("MAINMENU"), 0 );	
	CmsVerIni.Clear();

	//if( theApp.m_bFirstInstall == TRUE )
	//{
	//	CMenu menu;
	//	if( FALSE == menu.LoadMenu(IDR_MENU1) )
	//		return;


	//	menu.ModifyMenuW(ID_MENU_ITCMS_TOTAL, MF_BYCOMMAND, ID_MENU_ITCMS_TOTAL, _T("Open OFFICSSAFER"));
	////	menu.ModifyMenuW(ID_Menu_SEARCH, MF_BYCOMMAND, ID_Menu_SEARCH, GetTxt(TRAY_MENU_SEARCH,_T("Smart Search"))); //2014.08.18 - 스마트서취 사용안함
	//	menu.ModifyMenuW(ID_MENU_HELP, MF_BYCOMMAND, ID_MENU_HELP, _T("Help"));
	//	menu.ModifyMenuW(ID_MENU_PRODUCT_INFO, MF_BYCOMMAND, ID_MENU_PRODUCT_INFO, _T("Product information"));
	//	menu.ModifyMenuW(ID_MENU_HOMEPAGE, MF_BYCOMMAND, ID_MENU_HOMEPAGE, _T("OFFICESAFER website"));
	//	menu.ModifyMenuW(ID_MENU_TRAY_LOGOUT, MF_BYCOMMAND, ID_MENU_TRAY_LOGOUT, _T("Log out"));
	////	ON_COMMAND_RANGE(ID_USER_SCAN ,ID_USER_SCAN + 1 ID_USER_SCAN , NULL);
	//	//sETmENU

	//	CMenu *pPopMenu = NULL;
	//	pPopMenu = menu.GetSubMenu(0);
	//	
	//	if( pPopMenu == NULL )
	//		return;

	//	ShowMenu(pPopMenu);	
	//

	//}
	//else
	{
		CMenu menu;
		if( FALSE == menu.LoadMenu(IDR_MENU1) )
			return;

		menu.ModifyMenuW(ID_MENU_ITCMS_TOTAL, MF_BYCOMMAND, ID_MENU_ITCMS_TOTAL, _T("Open TIORSAVER"));
		menu.ModifyMenuW(ID_MENU_PRODUCT_INFO, MF_BYCOMMAND, ID_MENU_PRODUCT_INFO, _T("Product information"));
		menu.ModifyMenuW(ID_MENU_HOMEPAGE, MF_BYCOMMAND, ID_MENU_HOMEPAGE, _T("TIORSAVER website"));

		CMenu *pPopMenu = NULL;
		pPopMenu = menu.GetSubMenu(0);

		if( pPopMenu == NULL )
			return;
		ShowMenu(pPopMenu);	

	}

	
}

