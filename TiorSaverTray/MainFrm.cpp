//MainFrm.cpp : CMainFrame 클래스의 구현


#include "stdafx.h"
#include "TiorSaverTray.h"
//#include "TiorSaverTrayCommonDefine.h"
#include "pipe.h"
#include "UtilsProcess.h"
#include "UtilsFile.h"
#include "MainFrm.h"
#include "PathInfo.h"

#include <winsvc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWndEx)

const int  iMaxUserToolbars = 10;
const UINT uiFirstUserToolBarId = AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId = uiFirstUserToolBarId + iMaxUserToolbars - 1;

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWndEx)
	ON_WM_CREATE()
	ON_REGISTERED_MESSAGE(AFX_WM_CREATETOOLBAR, &CMainFrame::OnToolbarCreateNew)
	ON_MESSAGE_VOID( MSG_POP_UP_START , OnPopUpStart) ///업데이트 완료옴, 종료 시켜할 것들은 종료한다.
	//ON_MESSAGE_VOID( MSG_DELETE_START , OnDeleteStart) ///업데이트 완료옴, 종료 시켜할 것들은 종료한다.
	ON_WM_TIMER()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
	//m_pMainDlg = NULL;
	m_arrNotifyByBrowserDlg.RemoveAll();
}

CMainFrame::~CMainFrame()
{
	CString strLog = _T("");
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CMenu *pMenu = GetMenu();

	if (pMenu != NULL)
	{
		SetMenu(NULL);
		::DestroyMenu(pMenu->GetSafeHmenu());
	}

	//Tray아이콘 생성
	//m_Tray.RemoveTrayIcon();
	if( FALSE == CreateTrayIcon() )
	{
		SetTimer(TIMER_CREATE_TRAY_ICON, 2500, NULL );
	}

	//m_pMainDlg = new CNotifyByBrowserDlg;

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWndEx::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return TRUE;
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWndEx::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWndEx::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 메시지 처리기

void CMainFrame::OnViewCustomize()
{
	CMFCToolBarsCustomizeDialog* pDlgCust = new CMFCToolBarsCustomizeDialog(this, TRUE /* 메뉴를 검색합니다. */);
	pDlgCust->EnableUserDefinedToolbars();
	pDlgCust->Create();
}

void CMainFrame::OnPopUpStart()
{
	//if (for < theApp.m_nNoticeSetSize) {
	for (theApp.m_nCurrNotice = 0; theApp.m_nCurrNotice < (INT)theApp.m_nNoticeSetSize; theApp.m_nCurrNotice++)
	{
		ShowPopUp();
	}
	SetTimer(TIMER_POP_UP_PROCESS, POP_UP_PROCESS_RETRY_TIME, NULL);
	//SetTimer(TIMER_POP_UP_PROCESS, POP_UP_PROCESS_RETRY_TIME, NULL);
}

LRESULT CMainFrame::OnToolbarCreateNew(WPARAM wp,LPARAM lp)
{
	LRESULT lres = CFrameWndEx::OnToolbarCreateNew(wp,lp);
	if (lres == 0)
	{
		return 0;
	}

	CMFCToolBar* pUserToolbar = (CMFCToolBar*)lres;
	ASSERT_VALID(pUserToolbar);

	//BOOL bNameValid;
	//CString strCustomize;
	//bNameValid = strCustomize.LoadString(IDS_TOOLBAR_CUSTOMIZE);
	//ASSERT(bNameValid);

	//pUserToolbar->EnableCustomizeButton(TRUE, ID_VIEW_CUSTOMIZE, strCustomize);
	return lres;
}

//Tray 아이콘 생성
BOOL	 CMainFrame::CreateTrayIcon()
{
	CString strLog;
	HICON hIcon = NULL;
	hIcon = theApp.LoadIcon(IDR_MAINFRAME);
	if(hIcon != NULL)	//실패시 1초후에 다시 시도 
	{
		if( false == m_Tray.AddIcon( hIcon, _T("B2SAVER")) )
		{
			strLog.Format(_T("[Tray Icon] Error - LoadIcon Fail : %d"), GetLastError())  ;
			UM_WRITE_LOG(strLog);
			return FALSE;	
		}

	}
	else		
	{
		strLog.Format(_T("[Tray Icon] Error - LoadIcon Fail : %d"), GetLastError())  ;
		UM_WRITE_LOG(strLog);
		return FALSE;
	}

	::LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE (IDR_MENU1));
	return TRUE;
}

void CMainFrame::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	switch (nIDEvent)
	{

	case TIMER_POP_UP_PROCESS:
		{
			KillTimer(TIMER_POP_UP_PROCESS);
			if (0 < m_arrNotifyByBrowserDlg.GetSize()) {
				SetTimer(TIMER_POP_UP_PROCESS, POP_UP_PROCESS_RETRY_TIME, NULL);
			}
			else {
				AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
			}
			//if(NULL != m_pMainDlg) {
			//	if (TRUE == m_pMainDlg->m_bClose) {
			//		delete m_pMainDlg;
			//		m_pMainDlg = NULL;

					//} else {
					//	AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
					//}
				//} else {
				//	SetTimer(TIMER_POP_UP_PROCESS, POP_UP_PROCESS_RETRY_TIME, NULL);
				//}
				

			//} else {
			//	if (theApp.m_nCurrNotice < theApp.m_nNoticeSetSize) {
			//		theApp.m_nCurrNotice++;
			//		ShowPopUp();
			//	} else {
			//		AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
			//	}
			//}
		}
	//case TIMER_CREATE_TRAY_ICON:
	//	{
	//		KillTimer(TIMER_CREATE_TRAY_ICON);
	//		if( FALSE == CreateTrayIcon() )
	//		{
	//			SetTimer(TIMER_CREATE_TRAY_ICON, 2500, NULL);
	//		}
	//	}
	}
}

void CMainFrame::ShowPopUp()
{
	//if(m_pMainDlg == NULL) {
		//m_pMainDlg = new CNotifyByBrowserDlg;
		CNotifyByBrowserDlg* pNotifyDlg = new CNotifyByBrowserDlg;
		//m_pMainDlg->m_strTitle = theApp.m_pNoticeSet[theApp.m_nCurrNotice].strTitle;
		//m_pMainDlg->m_strMessage = theApp.m_pNoticeSet[theApp.m_nCurrNotice].strDescr;
		//m_pMainDlg->m_strMessageType = theApp.m_pNoticeSet[theApp.m_nCurrNotice].strType;
		//m_pMainDlg->Create(IDD_TATTLING_DIALOG, this);
		//SetTimer(TIMER_POP_UP_PROCESS, POP_UP_PROCESS_RETRY_TIME, NULL);
		pNotifyDlg->m_strTitle = theApp.m_pNoticeSet[theApp.m_nCurrNotice].strTitle;
		pNotifyDlg->m_strMessage = theApp.m_pNoticeSet[theApp.m_nCurrNotice].strDescr;
		pNotifyDlg->m_strMessageType = theApp.m_pNoticeSet[theApp.m_nCurrNotice].strType;
		pNotifyDlg->m_nIndex = theApp.m_nCurrNotice;
		m_arrNotifyByBrowserDlg.Add(pNotifyDlg);
		if (pNotifyDlg->Create(IDD_DIALOG_NOTIFY, GetDesktopWindow())) {
		//if (pNotifyDlg->DoModal() == IDOK) {
			pNotifyDlg->ShowWindow(SW_SHOW);
		} else {
			AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
		}
}

void CMainFrame::ActivateFrame(int nCmdShow)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	nCmdShow = SW_HIDE;
	CFrameWnd::ActivateFrame(nCmdShow);
}
