#include "StdAfx.h"
#include "ParamParser.h"
#include "../BuildEnv/common/json/JSON.h"
#include "../TiorSaverTransfer/MemberData.h"
#include "TiorSaverSetup.h"

#define	 HEADER			_T("header")
#define  SESSION			_T("Issession")
#define  CRYPTO			_T("Iscrypto")
#define  ENCRYPT			_T("Isencrypt")
#define  DEVICEID			_T("Deviceid")
#define  TRID				_T("Trid")
#define  MESSAGE			_T("message")
#define  CURRENT_OFFSET				_T("size")
#define  LOGFILE			_T("file")
#define	 TOKEN				_T("token")
#define  DATA		        _T("data")
#define  STATUS			_T("status")
#define  DEVICE			_T("device")
#define	 DEVICE_UUID		_T("device_uuid")
#define	 DEVICE_VERSION	_T("ver")
#define  EVENTLOG_FILENAME	_T("filename")
#define	 EVENTLOG_CONTENTS	_T("logs")
#define	 LICENSE_CODE _T("license_code")

CParamParser::CParamParser(void)
{
}

CParamParser::~CParamParser(void)
{
}

//CString CParamParser::CreateJSONRequest(CString _strText, CString _strTrid, CString _strLicenseKey)
//{
//	JSONObject root, root2, root3;
//
//	CString strReturn = _T("") , strTrid = _T("");
//
//	strTrid = _strTrid;
//
//	root3[SESSION] = new JSONValue(_T("0"));
//	root3[CRYPTO] = new JSONValue(_T("0"));
//	root3[DEVICEID] = new JSONValue(_T(""));
//	root3[TRID] =  new JSONValue(strTrid);
//	root3[TXID] = new JSONValue(_T("1"));
//	root3[VERSION] = new JSONValue(_T(""));
//
//	root[HEADER] = new JSONValue(root3);
//
//	root2[_T("text")] =  new JSONValue(_strText);
//
//	root[BODY]  = new JSONValue(root2);
//
//	JSONValue *value = new JSONValue(root);
//
//	strReturn.Format(_T("%s"), value->Stringify().c_str());
//
//	//DBGLOG(strReturn);
//
//	delete value;
//
//	return strReturn;
//}

CString CParamParser::CreateKeyValueRequest()
{
	//CString strReturn = _T("");
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion());
	//strReturn.Format(_T("headers: { Authorization: Bearer %s} %s"), _strToken, strData);
	return strData;
}
CString CParamParser::CreateKeyValueLogRequest(CString _strFileName)
{
	//CString strReturn = _T("");
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s&%s="), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName, EVENTLOG_CONTENTS);
	DBGLOG(_T("[CreateKeyValueLogRequest] %s"), strData);
	return strData;
}
CString CParamParser::CreateOffsetKeyValueRequest(CString _strFileName)
{
	//CString strReturn = _T("");
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName);
	//strReturn.Format(_T("headers: { Authorization: Bearer %s} %s"), _strToken, strData);
	return strData;
}
CString CParamParser::CreateInstallKeyValueRequest(CString _strLicenseCode)
{
	CString strReturn = _T("");
	strReturn.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(), \
		LICENSE_CODE, _strLicenseCode);
	return strReturn;
}
CString CParamParser::CreateKeyValueScreenRequest(CString _strFileName)
{
	CString strReturn = _T("");
	strReturn.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s&%s="),  DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName, DATA);
	return strReturn;
}

CString CParamParser::CreateFirstValueData()
{
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID());
	return strData;
}

int CParamParser::GetErrorCheck(CString _strRecvData, CString& strMessage)
{
	BOOL bStatus = 0;
	BOOL bIsInspect = FALSE;

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root;

		if( value->IsObject() != false )
		{
			root = value->AsObject();

			if(root.find(STATUS) != root.end() && root[STATUS]->IsBool())
			{
				bStatus = root[STATUS]->AsBool();
				if (FALSE == bStatus) {
					if( root.find(_T("inspect")) != root.end() && root[_T("inspect")]->IsBool() )
					{
						bIsInspect = root[_T("inspect")]->AsBool();
						if (!bIsInspect) {
							if( root.find(MESSAGE) != root.end() && root[MESSAGE]->IsString() )
							{
								strMessage.Format(_T("%s"), root[MESSAGE]->AsString().c_str());
							}
						} else {
							strMessage = _T("");
						}
					}
				}
			}//header
		}
		delete value;
	}
	return bStatus;
}

BOOL CParamParser::ParserGetOffsetReturn(CString _strRecvData, UINT& nCurrentOffset, CString& strToken, int& nDelay)
{
	if (_strRecvData.IsEmpty()) {
		return FALSE;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	int nStatus = 0;
	BOOL bResult = FALSE;

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken = strMessage;
		return bResult;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				//JSONValue *value2 = JSON::Parse(root[DATA]->AsString().c_str());
				root2 = root[DATA]->AsObject();
				if( root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString() ) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());

					if( root2.find(_T("file")) != root2.end() && root2[_T("file")]->IsObject() ) {
						root3 = root2[_T("file")]->AsObject();
						if(root3.find(_T("size")) != root3.end() && root3[_T("size")]->IsNumber())
						{
							nCurrentOffset = (int)(root3[_T("size")]->AsNumber());
							bResult = TRUE;
						}
					}
				} else {
					bResult = FALSE;
				}

				if(root2.find(_T("delay")) != root2.end() && root2[_T("delay")]->IsNumber()) {
					nDelay = (INT)root2[_T("delay")]->AsNumber();
				}

			} else {
				bResult = FALSE;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
		}
		delete value;
	}
	return bResult;
}

BOOL CParamParser::ParserOnlyToken(CString _strRecvData, /*UINT& nCurrentOffset, */CString& strToken)
{
	if (_strRecvData.IsEmpty()) {
		return FALSE;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	int nStatus = 0;
	BOOL bResult = FALSE;

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken.Format(_T("%s"), strMessage);
		return bResult;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3, root5;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				//JSONValue *value2 = JSON::Parse(root[DATA]->AsString().c_str());
				root2 = root[DATA]->AsObject();
				if(root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString()) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());

					//if( root2.find(_T("file")) != root2.end() && root2[_T("file")]->IsObject() ) {
					//	root3 = root2[_T("file")]->AsObject();
					//	if(root3.find(_T("size")) != root3.end() && root3[_T("size")]->IsNumber())
					//	{
					//		nCurrentOffset = (int)(root3[_T("size")]->AsNumber());
					bResult = TRUE;
					//	}
					//}
				} else {
					bResult = FALSE;
				}
				if(root2.find(_T("screen_index")) != root2.end() && root2[_T("screen_index")]->IsNumber()) {
					theApp.m_nScreenIdx = (INT)root2[_T("screen_index")]->AsNumber();
				}
			} else {
				bResult = FALSE;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
			if( root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject())
			{
				root3 = root2[_T("policy")]->AsObject();							
				if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
					root5 = root3[_T("time")]->AsObject();
					if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
						CString strTimeFromServer = _T("");
						strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
						//theApp.m_strTimeFromServer = strTimeFromServer;
					}
				}
			}
		}
		delete value;
	}
	return bResult;
}

BOOL CParamParser::ParserIdentifyLicense(CString _strRecvData, CTypedPtrArray <CPtrArray, DEPARTMENT*>* arrDepartment, CString& strToken)
{
	if (_strRecvData.IsEmpty()) {
		return FALSE;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	int nStatus = 0;
	BOOL bResult = FALSE;
	arrDepartment->RemoveAll();

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken.Format(_T("%s"), strMessage);
		return bResult;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3, root5;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				//JSONValue *value2 = JSON::Parse(root[DATA]->AsString().c_str());
				root2 = root[DATA]->AsObject();
				if(root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString()) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());
					bResult = TRUE;

					if (root2.find(_T("department")) != root2.end() && root2[_T("department")]->IsArray()) {
						JSONArray subArray = root2[_T("department")]->AsArray();
						for( UINT i = 0; i < subArray.size(); i++)
						{
							DEPARTMENT* department = new DEPARTMENT;
							root3 = subArray[i]->AsObject();
							if (root3.find(_T("dep_id")) != root3.end() && root3[_T("dep_id")]->IsNumber()) {
								department->nDepartmentID = (int)root3[_T("dep_id")]->AsNumber();
							}
							if (root3.find(_T("dep_name")) != root3.end() && root3[_T("dep_name")]->IsString()) {
								department->strDepartmentName = root3[_T("dep_name")]->AsString().c_str();
							}
							arrDepartment->Add(department);
							//if (NULL != department) {
							//	delete department;
							//	department = NULL;
							//}
						}
					}
					if (root2.find(_T("corp_name")) != root2.end() && root2[_T("corp_name")]->IsString()) {
						CString strCorpName = _T("");
						strCorpName.Format(_T("%s"), root2[_T("corp_name")]->AsString().c_str());
						theApp.m_pMemberData->SetCorpName(strCorpName);
					}
				} else {
					bResult = FALSE;
				}
				if( root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject())
				{
					root3 = root2[_T("policy")]->AsObject();							
					if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
						root5 = root3[_T("time")]->AsObject();
						if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
							CString strTimeFromServer = _T("");
							strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
							//theApp.m_strTimeFromServer = strTimeFromServer;
						}
					}
				}
			} else {
				bResult = FALSE;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
		}
		delete value;
	}
	return bResult;
}

CString CParamParser::CreateAgentRegisterRequest(int _nDepartmentID, CString _strAgentName, CString _strAppList)
{
	CString strReturn = _T("");
	strReturn.Format(_T("%s=%s&%s=%s&%s=%s&%s=%d&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		_T("dep_id"), _nDepartmentID, _T("agent_name"), _strAgentName, _T("agent_ip"), theApp.m_pMemberData->GetPCAddress(TRUE), _T("program"), _strAppList);
	return strReturn;
}

//BOOL CParamParser::ParserNoticeSet(CString _strRecvData, CString& strToken)
//{
//	if (_strRecvData.IsEmpty()) {
//		return FALSE;
//	}
//	CString strResult = _T(""), strTempIndex=L"";
//	CString strMessage = _T("");
//	int nStatus = 0;
//	BOOL bResult = FALSE;
//
//	nStatus = GetErrorCheck(_strRecvData, strMessage);
//	if( nStatus != 1)
//	{
//		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
//		strToken = strMessage;
//		return bResult;
//	}
//
//	JSONValue *value = JSON::Parse(_strRecvData);
//
//	if( value != NULL )
//	{
//		JSONObject root, root2, root10, root3, root4, root5, root6, root7;
//
//		if( value->IsObject() != false )
//		{
//			root = value->AsObject();
//			CString strBody = _T("");
//			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
//			{
//				root2 = root[DATA]->AsObject();
//				if(root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString()) {
//					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());
//					bResult = TRUE;
//
//					if (root2.find(_T("notice")) != root2.end() && root2[_T("notice")]->IsArray()) {
//						JSONArray subArray = root2[_T("notice")]->AsArray();
//						theApp.m_nNoticeSetSize = subArray.size();
//						theApp.m_pNoticeSet = new NOTICE_SET[theApp.m_nNoticeSetSize];
//						for( UINT i = 0; i < theApp.m_nNoticeSetSize; i++)
//						{
//								root10 = subArray[i]->AsObject();
//								if(root10.find(_T("id")) != root10.end() && root10[_T("id")]->IsString()) {
//									theApp.m_pNoticeSet[i].strID.Format(_T("%s"), root10[_T("id")]->AsString().c_str());
//								}
//								if(root10.find(_T("title")) != root10.end() && root10[_T("title")]->IsString()) {
//									theApp.m_pNoticeSet[i].strTitle.Format(_T("%s"), root10[_T("title")]->AsString().c_str());
//								}
//								if(root10.find(_T("descr")) != root10.end() && root10[_T("descr")]->IsString()) {
//									theApp.m_pNoticeSet[i].strDescr.Format(_T("%s"), root10[_T("descr")]->AsString().c_str());
//								}
//								if(root10.find(_T("type")) != root10.end() && root10[_T("type")]->IsString()) {
//									theApp.m_pNoticeSet[i].strType.Format(_T("%s"), root10[_T("type")]->AsString().c_str());
//								}
//								if(root10.find(_T("is_show")) != root10.end() && root10[_T("is_show")]->IsBool()) {
//									theApp.m_pNoticeSet[i].bIsShow = root10[_T("is_show")]->AsBool();
//								}
//								if(root10.find(_T("corp_name")) != root10.end() && root10[_T("corp_name")]->IsString()) {
//									theApp.m_pNoticeSet[i].strCorpName.Format(_T("%s"), root10[_T("corp_name")]->AsString().c_str());
//								}
//								if(root10.find(_T("device_uuid")) != root10.end() && root10[_T("device_uuid")]->IsString()) {
//									theApp.m_pNoticeSet[i].strDeviceUUID.Format(_T("%s"), root10[_T("device_uuid")]->AsString().c_str());
//								}
//								if(root10.find(_T("created_at")) != root10.end() && root10[_T("created_at")]->IsString()) {
//									theApp.m_pNoticeSet[i].strCreateAt.Format(_T("%s"), root10[_T("created_at")]->AsString().c_str());
//								}
//								if(root10.find(_T("updated_at")) != root10.end() && root10[_T("updated_at")]->IsString()) {
//									theApp.m_pNoticeSet[i].strUpdateAt.Format(_T("%s"), root10[_T("updated_at")]->AsString().c_str());
//								}
//						}
//					}
//				} else {
//					bResult = FALSE;
//				}
//				if( root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject())
//				{
//					root3 = root2[_T("policy")]->AsObject();							
//					if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
//						root5 = root3[_T("time")]->AsObject();
//						if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
//							CString strTimeFromServer = _T("");
//							strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
//							theApp.m_strTimeFromServer = strTimeFromServer;
//						}
//					}
//				}
//			} else {
//				bResult = FALSE;
//				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
//			}
//		}
//		delete value;
//	}
//	return bResult;
//}

BOOL CParamParser::ParserInstallUpdateSet(CString _strRecvData, CString& strMessage, UPDATE_SET* pUpdateSet)
{
	if (_strRecvData.IsEmpty()) {
		return FALSE;
	}
	CString strResult = _T(""), strTempIndex=L"";
	int nStatus = 0;
	BOOL bResult = FALSE;

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		return bResult;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3, root4, root5, root6, root7;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				root2 = root[DATA]->AsObject();
				if(root2.find(_T("version")) != root2.end() && root2[_T("version")]->IsString()) {
					pUpdateSet->strNewVersion.Format(_T("%s"), root2[_T("version")]->AsString().c_str());
					bResult = TRUE;

					if (root2.find(_T("files")) != root2.end() && root2[_T("files")]->IsObject()) {
						root3 = root2[_T("files")]->AsObject();
						if (root3.find(_T("path")) != root3.end() && root3[_T("path")]->IsArray()) {
							JSONArray subArray = root3[_T("path")]->AsArray();
							for( UINT i = 0; i < subArray.size(); i++)
							{
								CString strSubArr = _T("");
								strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
								pUpdateSet->arrPath.Add(strSubArr);
							}
						}
						if (root3.find(_T("version")) != root3.end() && root3[_T("version")]->IsArray()) {
							JSONArray subArray = root3[_T("version")]->AsArray();
							for( UINT i = 0; i < subArray.size(); i++)
							{
								CString strSubArr = _T("");
								strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
								pUpdateSet->arrVersion.Add(strSubArr);
							}
						}
						if (root3.find(_T("hash")) != root3.end() && root3[_T("hash")]->IsArray()) {
							JSONArray subArray = root3[_T("hash")]->AsArray();
							for( UINT i = 0; i < subArray.size(); i++)
							{
								CString strSubArr = _T("");
								strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
								pUpdateSet->arrHash.Add(strSubArr);
							}
						}
					} else {
						pUpdateSet = NULL;
					}
					if (root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject()) {
						root3 = root2[_T("policy")]->AsObject();
						if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
							root5 = root3[_T("time")]->AsObject();
							if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
								CString strTimeFromServer = _T("");
								strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
								//theApp.m_strTimeFromServer = strTimeFromServer;
							}
						}
					}
				} else {
					pUpdateSet = NULL;
				}
			}
		}
		delete value;
	}
	return bResult;
}
