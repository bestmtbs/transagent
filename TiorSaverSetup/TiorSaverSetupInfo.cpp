#include "StdAfx.h"
#include "TiorSaverSetup.h"
#include "TiorSaverSetupInfo.h"

#include "PathInfo.h"
#include "UtilsProcess.h"
#include "Impersonator.h"
#include "UtilsReg.h"
#include "UtilsService.h"
#include "RunRegistry.h"
//#include "PipeClient.h"
#include "UtilsReg.h"
#include "UtilCab.h"
#include "UtilsUnicode.h"
#include "CmsDBManager.h"
//#include "Log.h"

//#include "DBConnect.h"
//#include "CmsDBManager.h"

#define	ITCMS_AGENT_FILE			_T("TiorSaverTransfer.exe")
#define	ITCMS_UPGRADE_FILE			_T("TiorSaverUpdate.exe")
#define	ITCMS_UPDATE_FILE		_T("TiorSaverDownload.exe")
#define	ITCMS_TRAY_FILE					_T("TiorSaverTray.exe")

#define ITCMS_AGENT							_T("ITCMS_AGENT")
#define ITCMS_TRAY								_T("ITCMS_TRAY")
#define ITCMS_UPDATE							_T("ITCMS_DOWNLOAD")
#define ITCMS_UPGRADE							_T("ITCMS_UPGRADE")

#define	 ITCMS_CLIENT_PATH		_T("Software\\dsntech")
#define FW_DOMAIN_SET 				_T("SYSTEM\\CurrentControlSet\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\DomainProfile\\AuthorizedApplications\\List\\")
#define FW_STANDARD_SET			_T("SYSTEM\\CurrentControlSet\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile\\AuthorizedApplications\\List\\")
#define FW_PUBLIC_SET				_T("SYSTEM\\CurrentControlSet\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\PublicProfile\\AuthorizedApplications\\List\\")


#define FW_DOMAIN_SET001			_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\DomainProfile\\AuthorizedApplications\\List\\")
#define FW_STANDARD_SET001		_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\StandardProfile\\AuthorizedApplications\\List\\")
#define	FW_PUBLIC_SET001			_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\PublicProfile\\AuthorizedApplications\\List\\")


#define FW_VISTA_SET					_T("SYSTEM\\CurrentControlSet\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\FirewallRules\\")
#define FW_VISTA_SET001			_T("SYSTEM\\ControlSet001\\Services\\SharedAccess\\Parameters\\FirewallPolicy\\FirewallRules\\")

#define VALUSE_FORMAT		        _T("%s:*:Enabled:%s")
#define ADD_FIREWALL_FORMAT   _T("cmd.exe /c  netsh advfirewall firewall add rule name=\"%s\" dir=in action=allow program=\"%s\" enable=yes")
#define DEL_FIREWALL_FORMAT   _T("cmd.exe /c  netsh advfirewall firewall delete rule name=\"%s\" program=\"%s\"")

#define FW_VISTA_VALUE				 _T("v2.0|Action=Allow|Active=TRUE|Dir=In|Protocol=6|Profile=Private|Profile=Public|Profile=Domain|App=%s|Name=Csrss|Desc=Csrss|Edge=FALSE|")
#define TCP_FW_CMSTRAY			_T("TCP Query User{B9213F1D-417D-454F-A539-0E3533054053}%s")
#define UDP_FW_CMSTRAY            _T("UDP Query User{7C44B0C7-10A2-4012-A43F-38DBA0C7C1A3}%s")
#define TCP_FW_CMSAGENT          _T("TCP Query User{B9213F1D-417D-454F-A539-0E3533054053}%s")
#define UDP_FW_CMSAGENT			_T("UDP Query User{7C44B0C7-10A2-4012-A43F-38DBA0C7C1A3}%s")

#define WINLOGON_REGPATH			_T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon")
#define UAC_REGPATH					_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Policies\\System")

#define  VCREDIST_X86				 _T("vcredist_x86_s1.exe")
#define  VCREDIST_X64				 _T("vcredist_x64.exe")			

typedef BOOL (WINAPI* LPWOW64FSREDIRECTION)(PVOID *);
LPWOW64FSREDIRECTION lpfWow64DisableWow64FsRedirection = NULL;
BOOL bDisableRedirection;
HMODULE hModule;

#pragma comment(lib,"Version.lib")       // version.lib를 Link

/**
 @brief     생성자
 @author   JHLEE
 @date      2011.06.08
 @note      초기화 작업
*/
CTiorSaverSetupInfo::CTiorSaverSetupInfo(void)
{

}

CTiorSaverSetupInfo::~CTiorSaverSetupInfo(void)
{

}

/**
 @brief     기존 클라이언트 설치 여부 판단
 @author   JHLEE
 @date      2011.06.08 
 @return    true / false 
 @note       CmsAgent.exe  실행여부로 판단
*/
bool CTiorSaverSetupInfo::ClientInstallCheck()
{
	bool bExist = false;

	bExist = CProcess::IsProcessExist(_T("CMSAGENT_RUNNING_NOW"));

    return bExist;
}

	

/**
 @brief     클라이언트 모듈 설치
 @author   JHLEE
 @date      2011.06.08 
 @return    true / false 
 @note      경로는 (Program Files/Itcms, windows/system32/drivers)
 */
void CTiorSaverSetupInfo::FileExtract()
{

	CWinOsVersion osVer;
	CWinOsProduct  osProduct = osVer.GetProduct();

	CString   strClientPath = _T(""), strDriverPath = _T(""), strLogPath = _T(""), strDestPath = _T(""), strExtractionFile = _T(""), strWinPatchPath = _T(""),strTiorsaverZone = _T("");
	CString strClientModule = _T(""), strDriverModule = _T(""), strTemp, strUpdate = _T("");

	strClientPath = CPathInfo::GetClientInstallPath();

	DWORD err ;
	ForceCreateDir(strClientPath, &err);

	strWinPatchPath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("Update"));
	::CreateDirectory(strWinPatchPath, NULL);

	DropFile(IDR_ZIP_ITCMS,strClientPath+_T("Itcms.7z") ,_T("ZIP"));
	DeCompressFile(strClientPath+_T("Itcms.7z") ,strClientPath);
}

/**
 @brief     레지스트리 
 @author   JHLEE
 @date      2011.06.09 
 @return    
 @note      클라이언트에 필요한 레지스트리 생성
                 HKEY_LOCAL_MACHINE\SOFTWARE\Itcms 

 */
void CTiorSaverSetupInfo::CreateRegistry()
{
	bool bRegi = false;

	bRegi = CreateRegKey(HKEY_LOCAL_MACHINE, ITCMS_CLIENT_PATH);

}


/**
 @brief     서비스등록 
 @author   JHLEE
 @date      2011.06.09 
 @return    
 @note      서비스 모듈 등록 및 실행
          
 */
bool CTiorSaverSetupInfo::InstallServices()
{
	CImpersonator imp;
	CString strDbg;
	

	CString strServiceName = _T("");

	bool bService = true;

	strServiceName.Format(_T("%s%s i"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
	//if( imp.CreatProcessAndCompareStr(strServiceName,  "ok", _T("i")) )
	if(imp.CreateProcess(strServiceName, SW_HIDE))
	{
		imp.Free();

		//안전모드에서 서비스가 올라올수 있도록 등록
		CString strSafeMode_Mini, strSafeMode_Network;
		strSafeMode_Mini.Format(_T("SYSTEM\\CurrentControlSet\\Control\\SafeBoot\\Minimal\\%s"), TS_SERVICE_NAME);
		strSafeMode_Network.Format(_T("SYSTEM\\CurrentControlSet\\Control\\SafeBoot\\Network\\%s"), TS_SERVICE_NAME);
		SetRegStringValue(HKEY_LOCAL_MACHINE, strSafeMode_Mini, _T(""), _T("Service"), TRUE);
		SetRegStringValue(HKEY_LOCAL_MACHINE, strSafeMode_Network, _T(""), _T("Service"), TRUE);
		return true;
	}
	else
	{	
		strDbg.Format(_T("[TiorSaverSetup] Services Install Fail (%s)"),strServiceName);
		
		OutputDebugString(strDbg);
		imp.Free();
		return false;
	}
		
}


/**
 @brief     서비스등록 
 @author   JHLEE
 @date      2011.06.09 
 @return    
 @note      서비스 모듈 등록 및 실행
          
 */
bool CTiorSaverSetupInfo::StartServices()
{
	CImpersonator imp;

	CString strServiceName = _T("");

	bool bService = true;

	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
	strServiceName  +=_T(" s");
	if( imp.CreateProcess(strServiceName))
	{
		imp.Free();
		return true;
	}
	else
	{
		
		imp.Free();
		return false;
	}
		
}


bool  CTiorSaverSetupInfo::StartAgent()
{

	CImpersonator imp;

	CString strServiceName = _T("");

	bool bService = true;
	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_TRANS_AGENT_NAME);
	//if (CProcess::ProcessCreateMutex(strServiceName)) {
	int nCnt = 0;
	while (TRUE) {
		if (imp.CreateProcessEx(strServiceName)) {
			imp.Free();
			return true;
		}
		if (nCnt > 5)
			break;
		Sleep(1000);
	}	
	imp.Free();
	return false;
	//}
}







/**
 @brief     드라이버 설치 
 @author   JHLEE
 @date       
 @return    bool(true/false)
 @note      드라이버 설치

 */
/*bool CTiorSaverSetupInfo::InstallDriver()
{

	BOOL  bResult =FALSE ;
	CString  strDrvPath = _T(""), strExecute = _T("");

	CImpersonator imp;

	SetCurrentDirectory(CPathInfo::GetClientInstallPath());

	while( FileExists(_T("CmsFMonDrv.sys")) == FALSE )
	{
		Sleep(500);
	}
	
	

	while( FileExists(_T("NMonDrv.sys")) == FALSE )
	{
		Sleep(500);
	}	
	

	CString strNewFMon = _T(""), strNewNMon = _T("");

	hModule = ::LoadLibrary(_T("Kernel32.dll"));

	if(hModule)
	{
		lpfWow64DisableWow64FsRedirection = (LPWOW64FSREDIRECTION)GetProcAddress(hModule, ("Wow64DisableWow64FsRedirection"));

	}

	BOOL bRedirectEnable = TRUE;
	if(lpfWow64DisableWow64FsRedirection)
	{
		 bDisableRedirection = 	(*lpfWow64DisableWow64FsRedirection)(reinterpret_cast<PVOID*>(&bRedirectEnable));
	}

	
	strNewFMon.Format(_T("%s%s\\%s"), CPathInfo::GetSystem32(), _T("drivers"), _T("CmsFMonDrv.sys"));
	//OutputDebugString(strNewFMon);
	//UM_WRITE_LOG(strNewFMon)

	strNewNMon.Format(_T("%s%s\\%s"), CPathInfo::GetSystem32(), _T("drivers"), _T("NMonDrv.sys"));
	//OutputDebugString(strNewNMon);
	//UM_WRITE_LOG(strNewNMon)
	ForceCopyFile(_T("CmsFMonDrv.sys"), strNewFMon);
	ForceCopyFile(_T("NMonDrv.sys"), strNewNMon);


	bRedirectEnable= FALSE;
	if(lpfWow64DisableWow64FsRedirection)
	{
		 bDisableRedirection = 	(*lpfWow64DisableWow64FsRedirection)(reinterpret_cast<PVOID*>(&bRedirectEnable));
	}

	if(hModule)
	{
		FreeLibrary(hModule);
	}



	ShellExecute(NULL, TEXT("open"),_T("NMonDrvInstall.exe"), _T("install"), NULL, SW_HIDE);
	ShellExecute(NULL, TEXT("open"),_T("FMonDrvInstall.exe"), _T("install"), NULL, SW_HIDE);

	return TRUE;

}
*/

/**
@brief     드라이버 설치 
@author   hyo haeng heo
@date       2013.03.27
@return    bool(true/false)
@note      드라이버 설치

*/
//bool CTiorSaverSetupInfo::InstallDriver()
//{
//
//	CImpersonator imp;
//	CString strServiceName = _T("");
//
//	bool bService = true;
//	
//	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), INSTALLFILTER_PROC_NAME);
//
//	// 2017-04-11 kh.choi 파일 필터 드라이버와 네트웍 드라이버를 설치시 블루스크린이 발생하는 경우가 있어서 먼저 삭제한 후 설치하도록 조치
//	CString strUnregister = _T("");	// 삭제에 사용할 명령어 저장
//	strUnregister.Format(_T("%s %s"), strServiceName, _T("-unregister"));
//	if (imp.CreatProcessAndCompareStr(strUnregister, "ok")) {
//		//ezLog(_T("[TiorSaverSetup] imp.CreatProcessAndCompareStr(%s, \"ok\") return TRUE. [line: %d, function: %s, file: %s]\r\n"), strUnregister, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-04-11 kh.choi 파일로그
//	} else {	// 명령어 실행 결과 "ok" 문자열이 없더라도 문제없을 듯. 기존에 드라이버가 설치 완료된 상태가 아니기때문에. 일단 파일 로그만 남긴다.
//		//ezLog(_T("[TiorSaverSetup] imp.CreatProcessAndCompareStr(%s, \"ok\") return FALSE. [line: %d, function: %s, file: %s]\r\n"), strUnregister, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-04-11 kh.choi 파일로그
//	}
//	
//	CString strInstall = _T("");	// 설치에 사용할 명령어 저장
////#ifdef _TEST_RELEASE_		// 2017-06-30 kh.choi 모든 경우에 네트웍 필터는 설치되지 않고 파일 필터만 설치되도록 "-If" 옵션 사용
//	strInstall.Format(_T("%s %s"), strServiceName, _T("-If"));	
////#else //#ifdef _TEST_RELEASE_
////	strInstall.Format(_T("%s"), strServiceName);
////#endif //#else //#ifdef _TEST_RELEASE_
//	if( imp.CreatProcessAndCompareStr(strInstall, "ok") )
//	{
//		//ezLog(_T("[TiorSaverSetup] imp.CreatProcessAndCompareStr(%s, \"ok\") return TRUE. [line: %d, function: %s, file: %s]\r\n"), strInstall, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-04-11 kh.choi 파일로그
//		imp.Free();
//		return true;
//	}
//	else
//	{
//		//ezLog(_T("[TiorSaverSetup] imp.CreatProcessAndCompareStr(%s, \"ok\") return FALSE. [line: %d, function: %s, file: %s]\r\n"), strInstall, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-04-11 kh.choi 파일로그
//		imp.Free();
//		return false;
//	}
//
///*
//
//	BOOL flag;
//	HANDLE hwrite, hread;
//	SECURITY_ATTRIBUTES sa;
//	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
//	sa.lpSecurityDescriptor = NULL;
//	sa.bInheritHandle = true;
//	CString strLog, strDriverModulePath;
//	// 어노니머스 파이프 생성
//	flag = CreatePipe(&hread, &hwrite, &sa, 0);
//	if (!flag)
//	{
//		strLog.Format(_T("Driver Install Fail.. CreatePipeError : %d"), GetLastError() );
//		UM_WRITE_LOG(strLog);
//		return false;
//	}
//
//	// 콘솔어플리케이션 프로세스 실행을 위한 준비
//	STARTUPINFO si;
//	memset(&si, 0, sizeof(STARTUPINFO));
//	si.cb = sizeof(STARTUPINFO);
//	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
//	si.hStdOutput = hwrite;  // 표준출력(stdout) 리다이렉션
//	si.hStdError = hwrite;   // 표준에러(stderr) 리다이렉션
//	PROCESS_INFORMATION pi;
//	memset(&pi, 0, sizeof(PROCESS_INFORMATION));
//
//	// 콘솔어플리케이션 프로세스 실행
//	strDriverModulePath.Format(_T("%s"), CPathInfo::GetClientInstallPath() + INSTALLFILTER_PROC_NAME);
//	flag = CreateProcess(strDriverModulePath, NULL, NULL, NULL, TRUE, DETACHED_PROCESS,  //DETACHED_PROCESS
//		NULL, NULL, &si, &pi);
//	if(!flag)
//	{
//		strLog.Format(_T("Driver Install Fail.. CreatePipeError : %d"), GetLastError() );
//		UM_WRITE_LOG(strLog);
//		return false ;
//	}
//
//	CloseHandle(hwrite);//이것을 하지 않으면 프로세스가 block된다
//
//	char buffer[512]={0,};
//	DWORD BytesRead;
//	CString ResultString;
//	while(ReadFile(hread, buffer, sizeof(buffer)-1, &BytesRead, NULL) && BytesRead)
//	{
//		buffer[BytesRead] = '\0';
//		ResultString.Format(_T("%s"), buffer);
//	
//	}
//	CloseHandle(hread);
//
//	if(strstr(buffer, "ok") != NULL)
//	{
//		UM_WRITE_LOG(_T("Driver Install Ok..........."));
//		return true;
//	}
//	else 
//	{
//		UM_WRITE_LOG(_T("Driver Install Fail..........."));
//		return false;
//	}
//		*/
//	
//	
//}



/**
 @brief     방화벽 예외 등록 
 @author   hyo haeng heo
 @date      2012.10.15 
 @return    
 @note     기존 방화벽 코드 수정
				 
 */
void CTiorSaverSetupInfo::FirewallException()
{

//	CString strFullPath = _T(""), strPath = _T(""), strFileName = _T(""), strData = _T("");

//----	
	CString strCmsAgentFullPath = _T(""), strCmsTaryFullPath= _T(""), strCmsUpdateFullPath= _T(""), strCmsAgentVal =  _T(""), strCmsTrayVal =  _T(""),  strCmsUpdateVal =  _T(""),strSetupFullPath,strTiorSaverSetupVal =  _T("");
	CString StrOfsUpgradePath = _T(""),StrOfsUpgradeVal = _T("");
	CString strCmd = _T(""), strRundir = _T("");
	CWinOsVersion osVer;
	CImpersonator imp;

	strCmsAgentFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_TRANS_AGENT_NAME );
	strCmsTaryFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_TRAY_NAME );
	strCmsUpdateFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_UPDATE_AGENT_NAME );
	StrOfsUpgradePath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_DOWNLOAD_AGENT_NAME );
	strSetupFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_INSTALL_AGENT_NAME);


	strCmsAgentVal.Format( VALUSE_FORMAT,  strCmsAgentFullPath, ITCMS_AGENT);
	strCmsTrayVal.Format( VALUSE_FORMAT,  strCmsTaryFullPath, ITCMS_TRAY);
	strCmsUpdateVal.Format( VALUSE_FORMAT,  strCmsUpdateFullPath, ITCMS_UPDATE);
	StrOfsUpgradeVal.Format( VALUSE_FORMAT,  StrOfsUpgradePath, ITCMS_UPGRADE);
	
	strTiorSaverSetupVal.Format( VALUSE_FORMAT,  strSetupFullPath, L"ITCMS_SETUP");
	strRundir.Format( _T("%s"), CPathInfo::GetSystem32() ) ;
	
	if( osVer.GetProduct() <=   osWinServer2003 )		//win2003 이하 일경우
	{
		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsAgentFullPath, strCmsAgentVal, true);
		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsTaryFullPath, strCmsTrayVal, true);
		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsUpdateFullPath, strCmsUpdateVal, true);
		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, StrOfsUpgradePath, StrOfsUpgradeVal, true);

		SetRegStringValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strSetupFullPath, strTiorSaverSetupVal, true);
	}
	else{		// 비스타 이상부터는 레지스트리로 방화벽 예외처리 정보 없음(커맨드 사용)
	
		//기존에 이미 등록되어 있을수도 있으니 해제
		DWORD dwRet;

		//CmsAgent.exe 예외 처리 해제
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_AGENT, CPathInfo::GetClientInstallPath() + ITCMS_AGENT_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_TRAY, CPathInfo::GetClientInstallPath() + ITCMS_TRAY_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_UPDATE, CPathInfo::GetClientInstallPath() + ITCMS_UPDATE_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_UPGRADE, CPathInfo::GetClientInstallPath() + ITCMS_UPGRADE_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT,  L"ITCMS_SETUP", CPathInfo::GetClientInstallPath() +  L"TiorSaverSetup.exe"  );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );

		//방화벽 예외처리 등록
		strCmd.Format(ADD_FIREWALL_FORMAT, ITCMS_AGENT, CPathInfo::GetClientInstallPath() + ITCMS_AGENT_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(ADD_FIREWALL_FORMAT, ITCMS_TRAY, CPathInfo::GetClientInstallPath() + ITCMS_TRAY_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );		
		strCmd.Format(ADD_FIREWALL_FORMAT, ITCMS_UPDATE, CPathInfo::GetClientInstallPath() + ITCMS_UPDATE_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );	
		strCmd.Format(ADD_FIREWALL_FORMAT, ITCMS_UPGRADE, CPathInfo::GetClientInstallPath() + ITCMS_UPGRADE_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );	

		strCmd.Format(ADD_FIREWALL_FORMAT,  L"ITCMS_SETUP", CPathInfo::GetClientInstallPath() +  L"TiorSaverSetup.exe"  );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
	}
	
	
}

/**
@brief     방화벽 예외 정책 삭제
@author   hyo haeng heo
@date      2012.10.15 
@return    

*/
void CTiorSaverSetupInfo::DeleteFirewallPolicy()
{
	CString strCmsAgentFullPath = _T(""), strCmsTaryFullPath= _T(""), strCmsUpdateFullPath= _T(""), strCmsAgentVal =  _T(""), strCmsTrayVal =  _T(""),  strCmsUpdateVal =  _T(""),strSetupFullPath,strTiorSaverSetupVal =  _T("");
	CString strCmd = _T(""), strRundir = _T("");
	CString StrOfsUpgradePath = _T(""),StrOfsUpgradeVal = _T("");
	CWinOsVersion osVer;
	CImpersonator imp;

	strCmsAgentFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), ITCMS_AGENT_FILE );
	strCmsTaryFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), ITCMS_TRAY_FILE );
	strCmsUpdateFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), ITCMS_UPDATE_FILE );
	StrOfsUpgradePath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), ITCMS_UPGRADE_FILE );
	strSetupFullPath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), L"TiorSaverSetup.exe" );

	strRundir.Format( _T("%s"), CPathInfo::GetSystem32() ) ;

	if( osVer.GetProduct() <=   osWinServer2003 )		//win2003 이하 일경우
	{
		DelRegValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsAgentFullPath);
		DelRegValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsTaryFullPath);
		DelRegValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strCmsUpdateFullPath);
		DelRegValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, strSetupFullPath);
		DelRegValue(HKEY_LOCAL_MACHINE, FW_STANDARD_SET, StrOfsUpgradePath);

		

	}
	else{		// 비스타 이상부터는 레지스트리로 방화벽 예외처리 정보 없음(커맨드 사용)

		DWORD dwRet;
		//CmsAgent.exe 예외 처리 해제
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_AGENT, CPathInfo::GetClientInstallPath() + ITCMS_AGENT_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_TRAY, CPathInfo::GetClientInstallPath() + ITCMS_TRAY_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_UPDATE, CPathInfo::GetClientInstallPath() + ITCMS_UPDATE_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
		strCmd.Format(DEL_FIREWALL_FORMAT, ITCMS_UPGRADE, CPathInfo::GetClientInstallPath() + ITCMS_UPGRADE_FILE );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );

		strCmd.Format(DEL_FIREWALL_FORMAT,  L"ITCMS_SETUP", CPathInfo::GetClientInstallPath() +  L"TiorSaverSetup.exe"  );
		imp.RunProcessAndWait(strCmd, strRundir,  &dwRet );
	}
}


/**
 @brief     CmsTray.exe run key등록
 @author   JHLEE
 @date      2011.06.13 
 @note      vista  이하 os버전에서는 설치시 CmsTray.exe를 Run키에 등록 시키고 실행시켜줌 
				 
 */
void CTiorSaverSetupInfo::SetRunKey() //효효
{
	CWinOsVersion osVer;
	CWinOsProduct osProduct = osVer.GetProduct();
	

// 	if( osProduct < osWinVista )
// 	{
// 		CString strTray = _T(""), strUserInit = _T(""), strKeyValue = _T("");
// 		strTray.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("CmsTray.exe"));
// 
// 		CRunRegistry::SetRun(HKEY_LOCAL_MACHINE,_T("CmsTray.exe"), strTray);	
// 
// 		strUserInit = GetRegStringValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon"), _T("Userinit"));
// 
// 		strKeyValue.Format(_T("%s%s,"), strUserInit, strTray);
// 		SetRegStringValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon"), _T("Userinit"), strKeyValue);
// 
// 	}
// 	
	
	CString strTray = _T(""), strUserInit = _T(""), strKeyValue = _T(""), strAgent = _T("");
	strTray.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_TRAY_NAME);
	strAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_TRANS_AGENT_NAME);

	CRunRegistry::SetRun(HKEY_LOCAL_MACHINE,WTIOR_TRAY_NAME, strTray);	
	CRunRegistry::SetRun(HKEY_LOCAL_MACHINE,WTIOR_TRANS_AGENT_NAME, strAgent);	

	//strUserInit = GetRegStringValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon"), _T("Userinit"));

	//등록되어 있는지 확인
	//int nFind = 
	
	if( osVer.Is32bit() )		//32bit환경일때 
	{
		strUserInit = GetRegStringValue(HKEY_LOCAL_MACHINE, WINLOGON_REGPATH, _T("Userinit"));
		
		//이미 등록되어 있는 경우 발생대비
		strUserInit.Replace(  strTray +_T(",")  , _T("") );
		strUserInit.Replace(  strAgent +_T(",")  , _T("") );
		strKeyValue.Format(_T("%s%s,%s,"), strUserInit, strTray, strAgent);
		SetRegStringValue(HKEY_LOCAL_MACHINE, WINLOGON_REGPATH, _T("Userinit"), strKeyValue);
	}
	else{

		strUserInit = GetRegStringValueWow64(HKEY_LOCAL_MACHINE, WINLOGON_REGPATH, _T("Userinit"));
		strUserInit.Replace(  strTray +_T(",")  , _T("") );
		strUserInit.Replace(  strAgent +_T(",")  , _T("") );

		strKeyValue.Format(_T("%s%s,%s,"), strUserInit, strTray, strAgent);
		SetRegStringValueWow64(HKEY_LOCAL_MACHINE, WINLOGON_REGPATH, _T("Userinit"), strKeyValue);
	}
}

/**
@brief    로컬 데이타베이스 생성
@author   JHLEE
@date      2011.06.13 
@return    bool(true/false)
@note      CreateTable.sql 파일에 있는 쿼리문을 실행하여 필요한 테이블을 생성해준다.

*/
BOOL CTiorSaverSetupInfo::CreateDB()
{
	BOOL bCreate = FALSE;

	//db폴더가 생길때까지 기다려야한다..
	CString strDBFolder = _T("");
	strDBFolder.Format(_T("%s"), CPathInfo::GetDBFilePath());
	if (FileExists(strDBFolder + WTIOR_SETTING_DATABASE)) {
		ForceDeleteFile(strDBFolder+_T("test.db"));
	}
	while( DirectoryExists(strDBFolder) == FALSE )
	{
		Sleep(500);

	}

	CCmsDBManager dbManage(DB_AGENT_INFO);
	bCreate = dbManage.CreateQuery();

	dbManage.Free();

	return bCreate;
	
}

/**
@brief     클라이언트 OS 체크
@author   JHLEE
@date      2011.06.08 
@return    true / false 
@note      미지원 os (98, NT, VISTA 64BIT)
*/
bool CTiorSaverSetupInfo::IsOSCheck()
{

	CWinOsVersion osVer;
	CWinOsProduct osProduct = osVer.GetProduct();

	CString strMsg;
	strMsg.Format(_T("TiorSaverSetup : OS (%d)"), osProduct);
	//OutputDebugString(strMsg);
	UM_WRITE_LOG(strMsg)

	if( osWin2k > osProduct )
	{
		return false;
	}
/*	else if( (osWinVista == osProduct)  &&  osVer.Is64bit() )
	{
		return false;
	}*/

	return true;
}

/**
@brief    vs2008 재배포 패키지 설치
@author   hhheo
@date      2012.10.09 
@return    true / false 

*/
bool CTiorSaverSetupInfo::Install_RedistributablePackage()
{
	CString sPackagePath =_T(""), sRunningDir = _T("");
	CWinOsVersion osVer;
	CImpersonator imp;
	DWORD dwRet = 0;

		
	sPackagePath.Format(_T("\"%s%s\" /q"), CPathInfo::GetClientInstallPath(), VCREDIST_X86);
	sRunningDir.Format( _T("%s\\"),  CPathInfo::GetClientInstallPath());
		//imp.CreateProcess((LPTSTR)(LPCTSTR)sPackagePath, SW_HIDE);
	if( FALSE == imp.RunProcessAndWait(sPackagePath, sRunningDir,  &dwRet ) )
	{
		
		return false;
	}
		

	return true;
}



//bool CTiorSaverSetupInfo::GetVersionInfo( CString strFilePath, CString& strProductVersion, CString& strFileVersion )
//{
//	// 파일버전 정보를 읽기위한 버퍼크기 결정
//	DWORD dummy;
//	DWORD dwSize = GetFileVersionInfoSize( (LPCTSTR)strFilePath, &dummy );
//	if (dwSize == 0)
//	{
//		TRACE("GetFileVersionInfoSize failed with error %d\n", GetLastError());
//		return false;
//	}
//	BYTE*	pData = new BYTE[ dwSize ];
//
//
//	// 파일버전 정보 읽기
//	if ( !GetFileVersionInfo( (LPCTSTR)strFilePath, NULL, dwSize, pData ) )
//	{
//	//	TRACE("GetFileVersionInfo failed with error %d\n", GetLastError());
//		delete pData;
//		return false;
//	}
//
//	// 파일버전 정보를 읽기위한 변수설정 ( 파일버전 정보는 언어 리소스별로 구분되어 저장됨. )
//	LPVOID pvProductVersion = NULL;
//	LPVOID pvFileVersion = NULL;
//
//	struct LANGANDCODEPAGE 
//	{
//		WORD wLanguage;
//		WORD wCodePage;
//	} *lpTranslate;
//	UINT		cbTranslate;
//	CString		strSubBlock;
//
//	VerQueryValue( pData, _T("\\VarFileInfo\\Translation"), (LPVOID*)&lpTranslate, (PUINT)&cbTranslate);
//
//	// 파일버전 정보에서 첫번째 언어에 해당하는 data를 읽어옴
//	// 다중언어 리소스에서는 정확한 동작을 위해서는 data를 읽기 전에 언어코드, 코드페이지 정보를 지정하여야 함.
//	// 버전에 관한 언어, 코드페이지 정보는 리소스파일의 "Block Header"에 기입되어 있음.
//	strSubBlock.Format( _T("\\StringFileInfo\\%04x%04x\\ProductVersion"), lpTranslate[0].wLanguage, lpTranslate[0].wCodePage );
//	
//	if ( !VerQueryValue(pData, (LPCTSTR)strSubBlock, &pvProductVersion, (PUINT)&dummy) )
//	{
//	//	TRACE("Can't obtain ProductVersion from resources\n");
//		delete pData;
//		return false;
//	}
//
//	strSubBlock.Format( _T("\\StringFileInfo\\%04x%04x\\FileVersion"), lpTranslate[0].wLanguage,
//		lpTranslate[0].wCodePage );
//	if ( !VerQueryValue(pData, (LPCTSTR)strSubBlock, &pvFileVersion, (PUINT)&dummy) )
//	{
//	//	TRACE("Can't obtain FileVersion from resources\n");
//		delete pData;
//		return false;
//	}
//
//	strProductVersion = (LPCTSTR)pvProductVersion;
//	strFileVersion = (LPCTSTR)pvFileVersion;
//
//	delete pData;
//	return true;
//}

void CTiorSaverSetupInfo::UacOff()
{
	CWinOsVersion osVer;
	CWinOsProduct osProduct = osVer.GetProduct();
	
	if(osProduct >= osWinVista) //비스타 이상 환경에서
	{
		if( osVer.Is32bit() )		//32bit환경일때 
		{
			SetRegDWORDValue(HKEY_LOCAL_MACHINE, UAC_REGPATH, _T("EnableLUA"), 0);
		}
		else
		{
			SetRegDWORDValueWow64(HKEY_LOCAL_MACHINE, UAC_REGPATH, _T("EnableLUA"), 0);
		}
	}
	

}

