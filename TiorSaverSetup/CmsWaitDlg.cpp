// CmsWaitDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "CmsWaitDlg.h"


// CCmsWaitDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCmsWaitDlg, CDialog)

CCmsWaitDlg::CCmsWaitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCmsWaitDlg::IDD, pParent)
{

}

CCmsWaitDlg::~CCmsWaitDlg()
{
}

void CCmsWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_WAIT_GIF, m_PictureProgressBar);
	DDX_Control(pDX, IDC_STATIC_WAIT_MAIN, m_Static_Main);
}


BEGIN_MESSAGE_MAP(CCmsWaitDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CCmsWaitDlg 메시지 처리기입니다.

BOOL CCmsWaitDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	m_hBitBack.LoadBitmap(IDB_BMP_BKG);

	//m_titleFont.CreateFont(13,0,0,0,FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
	//									CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("MS Shell Dlg")); 

	//m_Static_Main.SetFont(&m_titleFont);

	SetDlgItemText(IDC_STATIC_WAIT_MAIN, _T("잠시만 기다려 주십시오."));

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CCmsWaitDlg::StartProgressBar()
{

	if(m_PictureProgressBar.Load(MAKEINTRESOURCE(IDR_GIF_PROGRESS), _T("GIF")))
		m_PictureProgressBar.Draw();

}

void CCmsWaitDlg::StopProgressBar()
{
	m_PictureProgressBar.Stop();

}

void CCmsWaitDlg::SetProgressMsg(CString strMsg)
{
	SetDlgItemText(IDC_STATIC_WAIT, strMsg);
}


void CCmsWaitDlg::OnPaint()
{

	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rc;
	GetClientRect(&rc);
	BITMAP bm;
	CDC memDC;
	
	dc.FillSolidRect(rc, RGB(255,255,255));
		// client
	CBitmap * bitmap = NULL;
	m_hBitBack.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_hBitBack);
	
	dc.BitBlt(0, 0, rc.Width(), rc.Height(), &memDC, 0, 0, SRCCOPY);

	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
}

HBRUSH CCmsWaitDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	UINT uid = pWnd->GetDlgCtrlID();
	
	if (uid == IDC_STATIC_WAIT )
	{
 		pDC->SetBkMode(TRANSPARENT);
		hbr = CreateSolidBrush(RGB(255,255, 255));
 		return hbr;
	}
	else if (uid == IDC_STATIC_WAIT_MAIN )
	{
 		pDC->SetBkMode(TRANSPARENT);
		hbr = CreateSolidBrush(RGB(248,248, 248));
 		return hbr;
	}
	return hbr;

}
