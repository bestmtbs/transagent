#pragma once
#include "afxcmn.h"
//#include "CmsWaitDlg.h"
#include "Control/Button/ImageButton.h"
#include "Control/Progress/ProgressEx.h"
#include "Control/Static/ColorStatic.h"
#include "afxwin.h"

//#include "CmsUpdateProc.h"
//#include "Download.h"
#include "Upgrade.h"



// CCmsOneLevelDlg 대화 상자입니다.

class CCmsOneLevelDlg : public CDialog,public CUpdateReport
{
	DECLARE_DYNAMIC(CCmsOneLevelDlg)

public:
	CCmsOneLevelDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCmsOneLevelDlg();

	CListBox	 m_ListBox;
	CProgressEx		m_Progress;
	HWND m_hwndWinPcapHWND;
	HANDLE m_hWinPcapHandle;

	BOOL				m_bSetupThreadStop;
	BOOL				m_bStartUninst;
	BOOL				m_bAgentAlreadyExist;
	CBitmap				m_hBitBack;
	CImageButton		m_btInstallPrev;
	CImageButton		m_btInstallNext;
	CImageButton		m_btnClose;
	CImageButton		m_btnMin;
	CImageButton		m_btnSetupCancel;
	CColorStatic			m_StaticInstall;
	
	HANDLE					m_hThread;				//hhh: 셋업 설치 쓰레드 핸들

	bool		IsOsCheck();
	BOOL		Register();
	//void		IsFileExtract();
	//bool		IsFileCheck();
	bool		IsFirewallException();
	bool		IsCreateDB();
	//bool        IsCheckServerInfo();
	CString     Base64Enc(CString _strData);
	bool		IsService();
	bool		IsCreateRegistry();
	//bool		IsInstallDriver();
	BOOL		Install_Vcredist();										// hhh: 재배포 패키지 설치 
	bool        IsCurrentFile();

	void		Install();
	void		FailBtnSet();												//hhh: 셋업중 실패시 종료 버튼 설정 
	void		Set_InfoText(CString _strTxt);
	BOOL		CheckThreadStopFlag();
	void		SetProgressPos(int _iPos);
	void		BringWindowToFront(HWND parm_dest_wnd);
	BOOL		IsWinPcapThere();
	void		ExtractAndCopyInstallFiles();
	virtual		void Report(int nTotal, int nCurrent, CString _strReport);
	BOOL		m_bEnd;
	//BOOL		m_bJustAfterSelectPath;
	
	//BOOL m_bSetupThreadRunning;

//	CCmsWaitDlg*	m_pCmsWaitDlg;

//	void WaitEnd();

	void		ReLoad();

	void		RenameNetworkSysFile();
//	void RefreshControl();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	friend BOOL WINAPI StartProgressThread(LPVOID lpParam);
	friend BOOL WINAPI SetupThread(LPVOID lpParam);
	friend BOOL WINAPI ExtractThread(LPVOID lpParam);
// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_ONE_LEVEL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG * pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonNext();
	afx_msg void OnBnClickedButtonPrev();
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnPaint();

private:
	void ComponentLineUp();

public:
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	
	afx_msg void OnBnClickedBtnMin();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnClose();
};
