#pragma once
#include "afxwin.h"
#include "Control/Button/ImageButton.h"


// CCmsAgreeDlg 대화 상자입니다.

class CCmsAgreeDlg : public CDialog
{
	DECLARE_DYNAMIC(CCmsAgreeDlg)

public:
	CCmsAgreeDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCmsAgreeDlg();

	CWnd *m_pWnd;

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_AGREE };

	CButton		m_btAgree;
	CBitmap				m_hBitBack;
	CImageButton		m_btInstallPrev;
	CImageButton		m_btInstallNext;
	CImageButton		m_btnMin;
	CImageButton		m_btnClose;

	void				ReLoad();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	afx_msg BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedRadioAgree();
	afx_msg void OnBnClickedRadioDisagree();
	CButton m_btnNext;
	afx_msg void OnBnClickedCheckAgree();

	afx_msg void OnPaint();
	afx_msg void OnBnClickedButtonInstallNext();
	afx_msg void OnBnClickedButtonInstallCancel();



private:
	void ComponentLineUp();
	void WriteTermsIntheEditCtrl();
	virtual BOOL PreTranslateMessage(MSG * pMsg);
public:
	CEdit m_CtrlEdit_Terms;
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	
	afx_msg void OnBnClickedButtonMin();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnDestroy();
};
