// TiorSaverTrayPopupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "VerifyDlg.h"
#include "ParamParser.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include "RegistryQueryForAppList.h"
#include "pipe/C_SendAgent_S_CollectAgent/STOCPipeClient.h"
#include "WTSSession.h"
#include "Impersonator.h"
#include "CmsDBManager.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib, "UxTheme.lib")

// CAboutDlg dialog used for App About

extern CTiorSaverSetupApp theApp;
IMPLEMENT_DYNAMIC(CVerifyDlg, CDialog)


CVerifyDlg::CVerifyDlg(CWnd* pParent /*=NULL*/)
: CDialog(CVerifyDlg::IDD, pParent)
,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, _T(""),0)
,m_btnMin(IDB_BMP_MIN_N, IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, 0, _T(""),0)
,m_btnVerifyLicense(IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, _T(""),0)
//,m_btRegister(IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, _T(""),0)
,m_btInstallPrev(IDB_BMP_PREV_N, IDB_BMP_PREV_N, IDB_BMP_PREV_O, 0, 0, _T(""),0)
//,m_btInstallNext(IDB_BMP_NEXT_N,IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, IDB_BMP_NEXT_N, _T(""),0)
{
	//m_cbDepartment.ResetContent();
	//m_strLicenseCode = _T("");
	//m_nIsHttp = 1;
	m_bButtonVerify = FALSE;
}

void CVerifyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX,IDC_COMBOBOX_DEPARTMENT, m_cbDepartment);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	//((CButton*)GetDlgItem(IDC_RADIO_HTTP))->SetCheck(1);
	DDX_Control(pDX, IDC_BUTTON_GETTOKEN_CURL, m_btnVerifyLicense);
	//DDX_Control(pDX, IDC_BUTTON_REGISTER_CURL, m_btRegister);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_btInstallPrev);
	//DDX_Control(pDX, IDC_BUTTON_NEXT, m_btInstallNext);
}

BEGIN_MESSAGE_MAP(CVerifyDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_BUTTON_GETTOKEN_CURL, &CVerifyDlg::OnBnClickedButtonGettokenCurl)
	ON_BN_CLICKED(IDC_BUTTON_PREV, &CVerifyDlg::OnBnClickedButtonPreview)
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CVerifyDlg::OnBnClickedBtnMin)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CVerifyDlg::OnBnClickedButtonClose)
	ON_WM_CTLCOLOR()
ON_WM_NCHITTEST()
END_MESSAGE_MAP()

// CTiorSaverTrayPopupDlg message handlers
BOOL CVerifyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CenterWindow();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ComponentLineUp();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CVerifyDlg::ComponentLineUp()
{
	m_bmpBackground.LoadBitmap(IDB_BITMAP_BKG_LICENSE);
	m_hInputDomain.LoadBitmap(IDB_BMP_LICENSE_INPUT_N);
	m_hInputLicense.LoadBitmap(IDB_BMP_LICENSE_INPUT_N);
	BITMAP bm;
	m_bmpBackground.GetBitmap(&bm);
	MoveWindow(0,0,bm.bmWidth, bm.bmHeight);		//이미지 크기에 맞게 다이얼로그 사이즈 조정
	//this->CenterWindow(GetParent());
	SetWindowTheme(GetDlgItem(IDC_BUTTON_PREV)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_GETTOKEN_CURL)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_CLOSE)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_MIN)->m_hWnd, _T(""), _T(""));

	CFont fnt;
	LOGFONT lf;
	::ZeroMemory(&lf, sizeof(lf) );
	lf.lfHeight = 14;
	lf.lfWeight =FW_THIN;
	wcscpy(lf.lfFaceName, _T("SkiCargo"));
	wcscpy_s(lf.lfFaceName, theApp.m_ttfNotoSansRegular.GetFontFamilyName().c_str());
	fnt.CreateFontIndirect(&lf);
	GetDlgItem(IDC_EDIT_SERVER_HOST_URL)->SetFont(&fnt);
	GetDlgItem(IDC_EDIT_LICENSE_KEY)->SetFont(&fnt);
	fnt.Detach();

	GetDlgItem(IDC_EDIT_SERVER_HOST_URL)->MoveWindow(104, 106, 300, 15);
	GetDlgItem(IDC_EDIT_LICENSE_KEY)->MoveWindow(104, 144, 300, 15);

	m_btInstallPrev.SizeToContent();
	m_btnVerifyLicense.SizeToContent();

	m_btInstallPrev.MoveWindow(385, 327, m_btnVerifyLicense.GetWidth(), m_btnVerifyLicense.GetHeight());
	m_btnVerifyLicense.MoveWindow(485, 327, m_btInstallPrev.GetWidth(), m_btInstallPrev.GetHeight());
	m_btnMin.SizeToContent();
	m_btnClose.SizeToContent();
	m_btnClose.MoveWindow(550, 4, m_btnClose.GetWidth(), m_btnClose.GetHeight());
	m_btnMin.MoveWindow(520, 4, m_btnMin.GetWidth(), m_btnMin.GetHeight());
	GetDlgItem(IDC_EDIT_SERVER_HOST_URL)->SetFocus();
}

void CVerifyDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	BITMAP bm;
	CDC memDC;

	CBitmap * bitmap = NULL;
	m_bmpBackground.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_bmpBackground);

	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
	m_hInputDomain.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);

	bitmap = memDC.SelectObject(&m_hInputDomain);
	dc.BitBlt(100, 102, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
	m_hInputLicense.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);

	bitmap = memDC.SelectObject(&m_hInputLicense);
	dc.BitBlt(100, 140, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CVerifyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CVerifyDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CVerifyDlg::OnBnClickedButtonGettokenCurl()
{
	if (m_bButtonVerify) {
		return;
	}
	m_bButtonVerify = TRUE;
	CStringArray arrData;
	arrData.RemoveAll();
	CParamParser parser;

	CString strLog = _T("");
	CString strParam = _T("");
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strToken = _T("");

	CString strLicenseCode = _T("");
	CString strServerHostURL = _T("");
	GetDlgItemText(IDC_EDIT_LICENSE_KEY, strLicenseCode);
	GetDlgItemText(IDC_EDIT_SERVER_HOST_URL, strServerHostURL);
	strLicenseCode.Remove(_T(' '));
	strServerHostURL.Remove(_T(' '));
	if (strLicenseCode.IsEmpty() || strServerHostURL.IsEmpty()) {
		m_bButtonVerify = FALSE;
		MessageBox(_T("라이센스 코드와 서버 주소를 모두 입력해주세요."), _T("TiorSaver"), MB_OK);
		return;
	}
	if (-1 == strServerHostURL.Find(_T("http"))) {
		strServerHostURL = _T("https://") + strServerHostURL;
	}
	theApp.m_pMemberData->m_strServerHostURL = strServerHostURL;
	strParam = parser.CreateInstallKeyValueRequest(strLicenseCode);
	strUrl.Format(_T("%s/v1/agent/license"), theApp.m_pMemberData->GetServerHostURL());
	if (theApp) {
		strReturn = theApp.m_curl.CurlPost(strParam, strUrl, theApp.GetToken());
	}

	if (TRUE == parser.ParserIdentifyLicense(strReturn, &theApp.m_arrDepartment, strToken)) {
		//m_cbDepartment.ResetContent();
		theApp.SetToken(strToken);
		theApp.m_pMemberData->SetLicenseCode(strLicenseCode);
		CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

		if( pWnd )
		{
			ShowWindow(FALSE);

			pWnd->SendMessage(MESSAGE_REGISTER_DIALOG, 0,0);
		}
	} else {
		if (strToken.IsEmpty()) {
				strToken.Format(_T("통신 에러. 다시 시도해주세요."));
		}
		MessageBox(strToken, _T("TiorSaver"), MB_OK);
	}
	m_bButtonVerify = FALSE;
}

void CVerifyDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

BOOL CVerifyDlg::PreTranslateMessage(MSG* pMsg)
{
	// ESC 키
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	{
		return true;
	}

	// 엔터키
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		return true;
	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CVerifyDlg::OnBnClickedButtonPreview()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

		//theApp.DeleteRequest();
	if (theApp.m_pMemberData) {
		theApp.m_pMemberData->ResetAgentInformation();
	}
	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

	if( pWnd )
	{
		ShowWindow(FALSE);

		pWnd->SendMessage(MESSAGE_REGISTER_PREV, 0,0);
	}
}

HBRUSH CVerifyDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	UINT nID = pWnd->GetDlgCtrlID();

	if((IDC_EDIT_SERVER_HOST_URL == nID) || (IDC_EDIT_LICENSE_KEY == nID) )
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(50, 51, 53));
		hbr = CreateSolidBrush(RGB(255, 255, 255)); //255,255,255s
		//return hbr;
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CVerifyDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialog::OnMouseMove(nFlags, point);
}

void CVerifyDlg::OnBnClickedBtnMin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}

void CVerifyDlg::OnBnClickedButtonClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());

	::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
}
LRESULT CVerifyDlg::OnNcHitTest(CPoint point)
{
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
	{
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		pDlg->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));  
	}
	return CDialog::OnNcHitTest(point);
}
