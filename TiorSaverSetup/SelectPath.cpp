// SelectPath.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "SelectPath.h"
#include "UtilsFile.h"


// CSelectPath 대화 상자입니다.

IMPLEMENT_DYNAMIC(CSelectPath, CDialog)

CSelectPath::CSelectPath(CWnd* pParent /*=NULL*/)
: CDialog(CSelectPath::IDD, pParent)
,m_btInstallPrev(IDB_BMP_PREV_N,IDB_BMP_PREV_N, IDB_BMP_PREV_O, 0, 0, _T(""),0)
,m_btInstallNext(IDB_BMP_NEXT_N,IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, IDB_BMP_NEXT_N, _T(""),0)
,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O,  0, 0, _T(""),0)
,m_btnMin(IDB_BMP_MIN_N, IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, 0, _T(""), 0)
,m_btFind(IDB_BMP_FIND_N, IDB_BMP_FIND_N, IDB_BMP_FIND_O, 0, 0, _T(""), 0)
{
	m_bFindStart = FALSE;
	m_bButtonNext = FALSE;
}

CSelectPath::~CSelectPath()
{
}

void CSelectPath::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PATH, m_editPath);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_btInstallPrev);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_btInstallNext);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_btnClose);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_FIND, m_btFind);
}


BEGIN_MESSAGE_MAP(CSelectPath, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_FIND, &CSelectPath::OnBnClickedButtonFind)
	ON_BN_CLICKED(IDC_BUTTON_PREV, &CSelectPath::OnBnClickedButtonPrev)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CSelectPath::OnBnClickedButtonNext)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CSelectPath::OnBnClickedButtonExit)
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CSelectPath::OnBnClickedBtnMin)
	ON_WM_MOUSEMOVE()
	ON_WM_NCHITTEST()
	ON_WM_PAINT()
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

int CALLBACK BrowseCallbackProc(HWND hwnd,
								UINT uMsg,
								LPARAM lParam,
								LPARAM lpData)
{
	switch (uMsg) {
 case BFFM_INITIALIZED :
	 {
		 // 해당 Control ID는 shell32.dll version 5.0 이상에서 사용된다.
		 // 하위 버전에서는 현재 tree control이 id가 다르며
		 // 새폴더 버튼을 생성할 수 없다.
		 HWND hShell = GetDlgItem(hwnd, 0);  // 0x00000000(Shell Class)
		 HWND hTree = GetDlgItem(hShell, 100); // 0x00000064(Tree Control)
		 HWND hNew = GetDlgItem(hwnd, 14150); // 0x00003746(New Folder Button)
		 HWND hOK = GetDlgItem(hwnd, 1);  // 0x00000001(OK Button)
		 HWND hCancel= GetDlgItem(hwnd, 2);  // 0x00000002(Cancel Button)
		 HWND hStatic= GetDlgItem(hwnd, 14146); // 0x00003742(Static Control)

		 // 하나라도 못가져오면 기본 구성으로 처리한다.
		 if (!hShell || !hTree || !hNew || !hOK || !hCancel)
			 return 0;

		 // 상단 표시줄 삭제
		 if (hStatic) {
			 EnableWindow(hStatic, FALSE);
			 ShowWindow(hStatic, SW_HIDE);
		 }

		 CRect rectWnd;
		 CRect rectNew;
		 CRect rectOK;
		 CRect rectCancel;
		 GetClientRect(hwnd, &rectWnd);
		 GetClientRect(hNew, &rectNew);
		 GetClientRect(hOK, &rectOK);
		 GetClientRect(hCancel, &rectCancel);

		 // Tree 크기조정
		 // Tree를 가지는 shell의 크기조정
		 MoveWindow(hShell,
			 rectWnd.left+10,
			 rectWnd.top+10,
			 rectWnd.Width()-22, // 20이지만 실제로 보면 작아 보인다. 따라서 2를 추가로 빼준다.
			 rectWnd.Height()-rectNew.Height()-30,
			 TRUE);
		 // 새 폴더 Button 변경
		 MoveWindow(hNew,
			 rectWnd.left+10,
			 rectWnd.bottom-rectNew.Height()-10,
			 rectNew.Width(),
			 rectNew.Height(),
			 TRUE);
		 // 확인 Button
		 MoveWindow(hOK,
			 rectWnd.right-10-rectCancel.Width()-5-rectOK.Width(),
			 rectWnd.bottom-rectOK.Height()-10,
			 rectOK.Width(),
			 rectOK.Height(),
			 TRUE);
		 // 취소 Button
		 MoveWindow(hCancel,
			 rectWnd.right-10-rectCancel.Width(),
			 rectWnd.bottom-rectCancel.Height()-10,
			 rectCancel.Width(),
			 rectCancel.Height(),
			 TRUE);
	 }
	 break;
 case BFFM_SELCHANGED :
 case BFFM_VALIDATEFAILED :
	 break;
	}
	return 0;
}

// CSelectPath 메시지 처리기입니다.

BOOL CSelectPath::OnInitDialog()
{
	CDialog::OnInitDialog();
	//CenterWindow();
	ComponentLineUp();

	m_hInputBack.LoadBitmap(IDB_BMP_PATH_INPUT_N);
	CFont fnt;
	LOGFONT lf;
	::ZeroMemory(&lf, sizeof(lf) );
	lf.lfHeight = 14;
	wcscpy(lf.lfFaceName, _T("SkiCargo"));
	wcscpy_s(lf.lfFaceName, theApp.m_ttfNotoSansRegular.GetFontFamilyName().c_str());
	fnt.CreateFontIndirect(&lf);
	GetDlgItem(IDC_EDIT_PATH)->SetFont(&fnt);
	fnt.Detach();
	m_editPath.SetWindowText(_T("C:\\TiorSaver"));

	GetDlgItem(IDC_EDIT_PATH)->SetFocus();

	return TRUE;
}

void CSelectPath::ComponentLineUp()
{
	BITMAP bm;
	m_hBitBack.LoadBitmap(IDB_BITMAP_BKG_PATH);
	m_hBitBack.GetBitmap(&bm);
	this->MoveWindow(0,0, bm.bmWidth, bm.bmHeight);

	SetWindowTheme(GetDlgItem(IDC_BUTTON_PREV)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_FIND)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_NEXT)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_EXIT)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_MIN)->m_hWnd, _T(""), _T(""));
	
	CFont fnt;
	LOGFONT lf;
	::ZeroMemory(&lf, sizeof(lf) );
	lf.lfHeight = 14;
	lf.lfWeight =FW_THIN;
	wcscpy(lf.lfFaceName, _T("SkiCargo"));
	wcscpy_s(lf.lfFaceName, theApp.m_ttfNotoSansRegular.GetFontFamilyName().c_str());
	fnt.CreateFontIndirect(&lf);
	fnt.Detach();

	m_btInstallNext.SizeToContent();
	m_btInstallPrev.SizeToContent();
	m_btnClose.SizeToContent();
	m_btnMin.SizeToContent();
	m_btFind.SizeToContent();

	m_btInstallPrev.MoveWindow(385, 327, m_btInstallPrev.GetWidth(), m_btInstallPrev.GetHeight());
	m_btInstallNext.MoveWindow(485, 327, m_btInstallNext.GetWidth(), m_btInstallNext.GetHeight());
	m_btFind.MoveWindow(425, 222, m_btFind.GetWidth(), m_btFind.GetHeight());
	m_editPath.MoveWindow(44, 224, 350, 15);

	m_btnClose.MoveWindow(550, 4, m_btnClose.GetWidth(),m_btnClose.GetHeight());
	m_btnMin.MoveWindow(520, 4, m_btnMin.GetWidth(), m_btnMin.GetHeight());

}


void CSelectPath::OnBnClickedButtonFind()
{
	if (m_bFindStart) {
		return;
	}
	m_bFindStart = TRUE;
	CString  strFolder = _T("");
	ITEMIDLIST* pidlBrowse;
	TCHAR  achPath[MAX_PATH];
	BROWSEINFO brInfo;

	memset(&brInfo, 0x00, sizeof(BROWSEINFO));
	wcscpy(achPath, _T("폴더 선택"));
	//brInfo.hwndOwner  = this;
	brInfo.pidlRoot   = NULL;
	brInfo.pszDisplayName = achPath;
	brInfo.lpszTitle  = _T("");
	brInfo.ulFlags   = BIF_NEWDIALOGSTYLE|BIF_RETURNONLYFSDIRS|BIF_DONTGOBELOWDOMAIN;
	brInfo.lpfn    = BrowseCallbackProc;
	pidlBrowse    = ::SHBrowseForFolder( &brInfo );

	if(pidlBrowse != NULL) {
		SHGetPathFromIDList( pidlBrowse, achPath );

		// pszPathname에 선택한 폴더가 들어있습니다.
		strFolder = achPath;
		m_bFindStart = FALSE;
		m_editPath.SetWindowText(strFolder);
	}
}
void CSelectPath::OnBnClickedButtonPrev()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

	if( pWnd )
	{
		ShowWindow(FALSE);

		pWnd->SendMessage(MESSAGE_SELECTPATH_PREV, 0,0);
	}
}

void CSelectPath::OnBnClickedButtonNext()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	if (m_bButtonNext) {
		return;
	}
	m_bButtonNext = TRUE;
	m_editPath.GetWindowText(theApp.m_strInstallPath);
	if (theApp.m_strInstallPath.IsEmpty()) {
		MessageBox(_T("올바른 경로를 지정해 주세요"), _T("TiorSaver"), MB_OK);
		m_bButtonNext = FALSE;
		return;
	}
	if (FALSE == ForceCreateDir(theApp.m_strInstallPath)){
		CString strLog = _T("");
		strLog.Format(_T("올바른 경로를 지정해 주세요 %d"), GetLastError());
		MessageBox(strLog, _T("TiorSaver"), MB_OK);
		m_bButtonNext = FALSE;
		return;
	}

	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

	if( pWnd )
	{
		ShowWindow(FALSE);
		theApp.m_bJustAfterSelectPath = TRUE;
		pWnd->SendMessage(MESSAGE_ONE_DIALOG, 0,0);
	}
	m_bButtonNext = FALSE;
}

void CSelectPath::OnBnClickedButtonExit()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	//CCmsUninstall uninstall;
	//uninstall.SetupExit();

	::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
}

void CSelectPath::OnBnClickedBtnMin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}


BOOL CSelectPath::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE )
		{
			//엔터를 Dialog로 넘겨주지 않음
			return FALSE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CSelectPath::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	CWnd *pWnd = FindWindow(NULL, _T("폴더 찾아보기"));
	if (pWnd) {
		::SetFocus(pWnd->GetSafeHwnd());
	}
	CDialog::OnMouseMove(nFlags, point);
}

LRESULT CSelectPath::OnNcHitTest(CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
	{
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		pDlg->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));  
	}
	return CDialog::OnNcHitTest(point);
}

void CSelectPath::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rc;
	GetClientRect(&rc);
	BITMAP bm;
	CDC memDC;

	dc.FillSolidRect(rc, RGB(255,255,255));
	// client
	CBitmap * bitmap = NULL;
	m_hBitBack.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_hBitBack);

	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
	m_hInputBack.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);

	bitmap = memDC.SelectObject(&m_hInputBack);
	dc.BitBlt(40, 220, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
}

HBRUSH CSelectPath::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	UINT nID = pWnd->GetDlgCtrlID();

	if((nID == IDC_EDIT_PATH) )
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(50, 51, 53));
		hbr = CreateSolidBrush(RGB(255, 255, 255)); //255,255,255s
		//return hbr;
	} else if(nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(50, 51, 53));
		return (HBRUSH)::GetStockObject(NULL_BRUSH);		
	}
	else if (CTLCOLOR_STATIC == nCtlColor || CTLCOLOR_BTN == nCtlColor) {
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)::GetStockObject(NULL_BRUSH);	

	} else {
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)::GetStockObject(NULL_BRUSH);	
	}
	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}