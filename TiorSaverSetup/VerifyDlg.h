
// TiorSaverTrayPopupDlg.h : header file
//

//#pragma once
//#include "TiorsaverUserInfo.h"
#include "Control/Button/ImageButton.h"


#include "afxwin.h"
class CVerifyDlg : public CDialog
{
	DECLARE_DYNAMIC(CVerifyDlg)
	// Construction
public:
	CVerifyDlg(CWnd* pParent = NULL);	// standard constructor
	//virtual ~CTiorSaverTrayPopupDlg();

	// Dialog Data
	enum { IDD = IDD_VERIFY_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


	// Implementation
protected:
	HICON m_hIcon;
	CStatusBar m_wndStatusBar;
	CBitmap m_bmpBackground;
	CBitmap m_hInputDomain;
	CBitmap m_hInputLicense;
	BOOL m_bButtonVerify;
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedBtnMin();
	//afx_msg void OnBnClickedButtonRegisterCurl();
	//afx_msg void OnBnClickedButtonRegisterHttp();
	afx_msg void OnBnClickedButtonGettokenCurl();
	//afx_msg void OnBnClickedButtonGettokenHttp();
	//afx_msg void OnBnClickedButtonHide();
	afx_msg void OnBnClickedButtonClose();
	//afx_msg void OnBnClickedButtonStop();
	BOOL PreTranslateMessage(MSG* pMsg);
	void ComponentLineUp();
	//CComboBox m_cbDepartment;
	CImageButton		m_btnMin;
	CImageButton		m_btnClose;
	//afx_msg void OnCbenInsertitemComboboxexDepartment(NMHDR *pNMHDR, LRESULT *pResult);
	//afx_msg void OnBnClickedRadioHttps();
	//afx_msg void OnBnClickedRadioHttp();
protected:
	virtual void OnOK();
public:
	afx_msg void OnBnClickedButtonPreview();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
//	afx_msg void OnCbnEditchangeComboboxDepartment();
	CImageButton m_btnVerifyLicense;
	//CImageButton m_btRegister;
	CImageButton m_btInstallPrev;
	//CImageButton m_btInstallNext;
	afx_msg LRESULT OnNcHitTest(CPoint point);
};
