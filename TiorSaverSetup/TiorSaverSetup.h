
// TiorSaverSetup.h : PROJECT_NAME 응용 프로그램에 대한 주 헤더 파일입니다.
//

#pragma once

#ifndef __AFXWIN_H__
#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <locale>
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "curl\curl.h"
#include "CurlTConnect.h"
#pragma comment(lib, "wldap32.lib")
#pragma comment(lib, "crypt32")

#ifndef _DEBUG
#pragma comment (lib, "../BuildEnv/Lib/x86/libcurl.lib")
#else	// #ifdef _DEBUG
#pragma  comment (lib, "../BuildEnv/Lib/x86/libcurld.lib")
#endif	// #else	// #ifdef _DEBUG
#if LIBCURL_VERSION_NUM < 0x070c03
#error "upgrade your libcurl to no less than 7.12.3"
#endif

#include "resource.h"		// 주 기호입니다.

//#include "TiorSaverSetupWnd.h"
#include "../TiorSaverTransfer/MemberData.h"
#include "MinidumpHelp.h"
#include "TTF.h"


#define AGREE_DIALOG			100
#define ONE_DIALOG				101
#define TWO_DIALOG				102
#define THREE_DIALOG			103
#define VERIFY_DIALOG			104
#define ONE_DIALOG_PREV			105
#define COMPLETE_DIALOG			106
#define SELECTPATH_DIALOG			107	// 2015-10-28 kh.choi 설치 전 점검 다이얼로그


//#define MESSAGE_AGREE_DIALOG		WM_USER + 100	// 2015-11-23 kh.choi 쓰이는 곳 없음 -> 주석처리
#define MESSAGE_SELECTPATH_DIALOG		WM_USER + 100	// 2015-11-23 kh.choi 설치 전 점검 다이얼로그 생성 메세지
#define MESSAGE_ONE_DIALOG				WM_USER + 101
#define MESSAGE_REGISTER_DIALOG			WM_USER + 102
#define MESSAGE_SELECTPATH_PREV			WM_USER + 103
#define MESSAGE_VERIFY_DIALOG			WM_USER + 104
#define MESSAGE_SETUP_SUCCESS			WM_USER + 105
#define MESSAGE_CREATE_TRAY				WM_USER + 106
#define MESSAGE_SETUP_CANCLE			WM_USER + 107
#define MESSAGE_AGREE_PREV				WM_USER + 108
#define MESSAGE_ONE_PREV				WM_USER + 109
#define MESSAGE_REGISTER_PREV			WM_USER  +110
#define MESSAGE_COMPLETE				WM_USER + 111
#define MESSAGE_PROCESS_EXIT			WM_USER + 112
#define MESSAGE_MINUMUM					WM_USER + 113
#define MESSAGE_HIDE					WM_USER + 114

// CTiorSaverSetupApp:
// 이 클래스의 구현에 대해서는 TiorSaverSetup.cpp을 참조하십시오.
//

class CTiorSaverSetupApp : public CWinAppEx
{
public:
	CTiorSaverSetupApp();
	virtual ~CTiorSaverSetupApp();
public:
//	CTiorSaverSetupWnd* m_pWnd;
	MinidumpHelp m_Minidump;
	CMemberData* m_pMemberData;
	CString m_strToken;
	//CString m_strTimeFromServer;
	CString m_strInstallPath;
	INT m_nScreenIdx;
	INT m_nDepartmentID;
	BOOL m_bJustAfterSelectPath;
	CTypedPtrArray <CPtrArray, DEPARTMENT*> m_arrDepartment;
	CCurlTConnect m_curl;

	//BOOL SendMsg(CString _strBuffer, int _iType);
	//CString CurlPost(CString _strParam, CString _strUrl, INT& nErr);
	//CString CurlGET(CString _strUrl, INT& nErr);
	//CString CurlDelete(CString _strParam, CString _strUrl, INT& nErr);
	void SetToken(CString _strToken);
	CString GetToken();
	//CStringA Utf8_Encode(CStringW strData);
	//CStringW Utf8_Decode(CStringA strData);
	CString GetRcPath(CString _strFile);
	CString GetUserId();
	void DeleteMyself(CString _strPath);
	BOOL MoveFile();
	BOOL DeleteRequest();
	void Write_Index(CString _strPath, CString _strHash, CString _strVersion);
	BOOL WriteVersionFile();
	TTF m_ttfNotoSansBold;
	TTF m_ttfNotoSansRegular;
	HANDLE m_fonthandle;
	//void StartTray();

	CString GetRootAddress(){return m_sRootAddress;}
	
	//BOOL	GetNetworkFlt_UseFlag() { return m_bUseNetworkFlt; };
	BOOL CreateDB();


//	void WaitStart(CWnd* _pWnd, int _nType);
//	void WaitEnd();
	CDialog * m_pDlg;
	BOOL m_bWait;
	BOOL CreateFileEx (WORD _iResource, CString& _strFilePath, CString _strType	);

	DWORD		CreateProcessEx(LPTSTR pszCommand, USHORT iShowMode, BOOL bWait);
// 재정의입니다.
	public:
	virtual BOOL InitInstance();

// 구현입니다.

private:

	//BOOL	Init();

	CString m_sRootAddress;
	CString m_strUserId;
	BOOL	m_bUseNetworkFlt;

	DECLARE_MESSAGE_MAP()
};

extern CTiorSaverSetupApp theApp;