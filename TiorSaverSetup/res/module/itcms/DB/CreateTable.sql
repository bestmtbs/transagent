create table if not exists db_agent_info (
device nvarchar(50),
device_uuid varchar(260) primary key,
ver nvarchar(20),
department_name nvarchar(30),
agent_name nvarchar(50),
department_id int,
license_code nvarchar(50),
corp_name nvarchar(50),
regist_date datetime);

create table if not exists db_server_info (
server_host_url	 varchar(260) primary key
);

create table if not exists db_pc_info (
device_uuid varchar(260) primary key,
agent_ip nvarchar(20),
mac nvarchar(30), 
os nvarchar(50),
program text);

create table if not exists db_collect_policy	(
device_uuid	 varchar(260),
collect nvarchar(30) primary key);

create table if not exists db_notice (
device_uuid	 varchar(260),
notice nvarchar(50) primary key		
);

create table if not exists db_state_policy (
target nvarchar(20) default "all",
target_id varchar(260) primary key,		
state	 bool default 1,
init_screen_idx int default -1,
thumnail_start bool default 0
);

create table if not exists db_url_policy (
device_uuid	 varchar(260),
domain text);