											// CmsAgreeDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "CmsAgreeDlg.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include <locale.h>
//#include "CmsUninstall.h"
#pragma comment(lib, "UxTheme.lib") 

// CCmsAgreeDlg 대화 상자입니다.
#define		TERMS_FILE	_T("itcms_licence.txt")


IMPLEMENT_DYNAMIC(CCmsAgreeDlg, CDialog)

CCmsAgreeDlg::CCmsAgreeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCmsAgreeDlg::IDD, pParent)
	,m_btInstallPrev(IDB_BMP_PREV_N, IDB_BMP_PREV_N,  IDB_BMP_PREV_O, 0, IDB_BMP_PREV_N, _T(""),0)
	,m_btInstallNext(IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, IDB_BMP_NEXT_N, _T(""),0)
	,m_btnMin(IDB_BMP_MIN_N, IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, IDB_BMP_MIN_N, _T(""),0)
	,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O, 0, IDB_BMP_CLOSE_N, _T(""),0)
	//,m_btAgree(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O, 0, IDB_BMP_CLOSE_N, _T(""),0)
{

}

CCmsAgreeDlg::~CCmsAgreeDlg()
{
}

void CCmsAgreeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_CANCEL, m_btInstallPrev);
	DDX_Control(pDX, IDC_BUTTON_INSTALL_NEXT, m_btInstallNext);
	DDX_Control(pDX, IDC_CHECK_AGREE, m_btAgree);
	DDX_Control(pDX, IDC_EDIT_TERMS, m_CtrlEdit_Terms);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
}
BEGIN_MESSAGE_MAP(CCmsAgreeDlg, CDialog)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_CHECK_AGREE, &CCmsAgreeDlg::OnBnClickedCheckAgree)
	ON_WM_PAINT()
	
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_NEXT, &CCmsAgreeDlg::OnBnClickedButtonInstallNext)
	ON_BN_CLICKED(IDC_BUTTON_INSTALL_CANCEL, &CCmsAgreeDlg::OnBnClickedButtonInstallCancel)
	ON_WM_NCHITTEST()
	ON_WM_MOUSEMOVE()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CCmsAgreeDlg::OnBnClickedButtonMin)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CCmsAgreeDlg::OnBnClickedButtonClose)
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CCmsAgreeDlg 메시지 처리기입니다.
BOOL CCmsAgreeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	SetWindowTheme(m_btAgree.m_hWnd, L"", L"");
	ComponentLineUp();
	WriteTermsIntheEditCtrl();

	ReLoad();


	//	SetWindowPos( NULL , 0 , 0 , bmpinfo.bmWidth , bmpinfo.bmHeight , SWP_NOMOVE|SWP_NOZORDER ) ;


	return TRUE;
}

void	CCmsAgreeDlg::ReLoad()
{
	SetDlgItemText(IDC_CHECK_AGREE,_T("동의함"));

	m_btInstallPrev.ReLoadImage(theApp.GetRcPath(_T("0_btn_prev_n.bmp")), theApp.GetRcPath(_T("0_btn_prev_n.bmp")), theApp.GetRcPath(_T("0_btn_prev_o.bmp")), _T(""), theApp.GetRcPath(_T("0_btn_prev_n.bmp")));
	m_btInstallNext.ReLoadImage(theApp.GetRcPath(_T("0_btn_next_n.bmp")), theApp.GetRcPath(_T("0_btn_next_n.bmp")), theApp.GetRcPath(_T("0_btn_next_o.bmp")), _T(""), theApp.GetRcPath(_T("0_btn_next_n.bmp")));
}


//버튼 정렬 및 다이얼로그 크기 및 위치 설정
void CCmsAgreeDlg::ComponentLineUp()
{
	BITMAP bm;
	m_hBitBack.LoadBitmap(IDB_BITMAP_BKG_AGREE);
	m_hBitBack.GetBitmap(&bm);
	this->MoveWindow(0,0, bm.bmWidth, bm.bmHeight);
	
	CFont fnt;
	LOGFONT lf;
	::ZeroMemory(&lf, sizeof(lf) );
	lf.lfHeight = 14;
	wcscpy(lf.lfFaceName, _T("SkiCargo"));
	wcscpy_s(lf.lfFaceName, theApp.m_ttfNotoSansRegular.GetFontFamilyName().c_str());
	fnt.CreateFontIndirect(&lf);
	GetDlgItem(IDC_CHECK_AGREE)->SetFont(&fnt);
	GetDlgItem(IDC_EDIT_TERMS)->SetFont(&fnt);
	fnt.Detach();
	GetDlgItem(IDC_CHECK_AGREE)->MoveWindow(270, 297, 300, 15);

	m_btInstallPrev.SizeToContent();
	m_btInstallNext.SizeToContent();
	m_btnMin.SizeToContent();
	m_btnClose.SizeToContent();
	
	m_btInstallPrev.MoveWindow(385, 327, m_btInstallPrev.GetWidth(),   m_btInstallPrev.GetHeight());
	m_btInstallNext.MoveWindow(485, 327, m_btInstallNext.GetWidth(),   m_btInstallNext.GetHeight());
	m_btnClose.MoveWindow(550, 4, m_btnClose.GetWidth(),   m_btnClose.GetHeight());
	m_btnMin.MoveWindow(520, 4, m_btnMin.GetWidth(),   m_btnMin.GetHeight());
	m_CtrlEdit_Terms.MoveWindow(30, 100, 520, 170);
	m_btAgree.MoveWindow(260, 285, 200, 15);
	
}

void CCmsAgreeDlg::WriteTermsIntheEditCtrl()
{
	//CStdioFile stf;
	CString strTermsPath = _T("") ;
	CString display_str, read_str;
	DWORD dwFileSize ;

//	strTermsPath.Format( _T("%s%s"), CPathInfo::GetSystem32(), TERMS_FILE ) ;

	strTermsPath.Format( _T("%s%s") , GetCurrentModulePath(), TERMS_FILE );
	int nRcFile = IDR_TXT_TERMS;

	theApp.CreateFileEx(nRcFile, strTermsPath, _T("txt"));

	HANDLE hFile =  ::CreateFile( strTermsPath , GENERIC_READ , FILE_SHARE_READ , NULL , OPEN_EXISTING , FILE_ATTRIBUTE_NORMAL , NULL ) ;


	if( hFile == INVALID_HANDLE_VALUE )
		return ;

	dwFileSize = ::GetFileSize( hFile , NULL ) ;
	CloseHandle(hFile);

	display_str.GetBuffer( (dwFileSize + 1) * 2);
	
	CString strTmp =_T(""), strTmp2;
	int iLimiteReadLine = 52;

	FILE *fStream;
	errno_t e = _tfopen_s(&fStream, strTermsPath.GetBuffer(),  _T("rt,ccs=UNICODE"));
	if (e == 0) {		
		CStdioFile stf(fStream);  // open the file from this stream
		
		while( stf.ReadString(read_str) )
		{
			//display_str += strTmp2;	
			display_str += read_str;	
			display_str += _T("\r\n");
			strTmp2 = _T("");
		}
		SetDlgItemText(IDC_EDIT_TERMS, display_str);
		stf.Close();
	}
	display_str.ReleaseBuffer();

	GetDlgItem(IDC_EDIT_TERMS)->SetFocus();

	DeleteFile(strTermsPath); //이용약관 파일 삭제 

}

HBRUSH CCmsAgreeDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	UINT nID = pWnd->GetDlgCtrlID();
	
/*	if(nID == IDC_CHECK_AGREE)
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(50, 51, 53));
		return (HBRUSH)GetStockObject(RGB(240, 240, 240));
	}*/

	if(nCtlColor == CTLCOLOR_STATIC)
	{
	
		if(nID == IDC_EDIT_TERMS )
		{
			pDC->SetBkColor(RGB(240, 240, 240));
			return (HBRUSH)::GetStockObject(RGB(240, 240, 240));
		}
		if (nID == IDC_CHECK_AGREE)
		{

			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(50, 51, 53));
			return (HBRUSH)::GetStockObject(NULL_BRUSH);	
		}
		
	}
	else{
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)::GetStockObject(NULL_BRUSH);
	}
	return hbr;
}



void CCmsAgreeDlg::OnBnClickedCheckAgree()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CWnd *pWnd = GetDlgItem(IDC_BUTTON_INSTALL_NEXT);
	CRect Rect; 
	GetDlgItem(IDC_CHECK_AGREE)->GetWindowRect(&Rect);
	ScreenToClient(&Rect);
	InvalidateRect(Rect);

	if(m_btAgree.GetCheck())
	{
		m_btInstallNext.SetCheck(TRUE);
	}
	else
	{
		m_btInstallNext.SetCheck(FALSE);
	}
}


void CCmsAgreeDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rc;
	GetClientRect(&rc);
	BITMAP bm;
	CDC memDC;
	
	dc.FillSolidRect(rc, RGB(255,255,255));
	// client
	CBitmap * bitmap = NULL;
	m_hBitBack.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_hBitBack);
	
	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();

	CDC* pDC = &dc;

	pDC->SetBkMode(TRANSPARENT);
}

void CCmsAgreeDlg::OnBnClickedButtonInstallNext()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	if(!m_btInstallNext.GetCheck())
	{
		
		//MessageBox(_T("사용자 동의 버튼을 체크해 주십시요."), Tiorsaver_TITLE, MB_OK);
		MessageBox(_T("Please check the \"I agree\" checkbox"), _T("Tiorsaver"), MB_OK);
		return;
	}

	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer")); 

	if( pWnd != NULL )
	{
		ShowWindow(FALSE);

		pWnd->SendMessage(MESSAGE_SELECTPATH_DIALOG, 0,0);
	}
}


void CCmsAgreeDlg::OnBnClickedButtonInstallCancel()
{
	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

	if( pWnd != NULL )
	{
		ShowWindow(FALSE);
		pWnd->SendMessage(MESSAGE_AGREE_PREV, 0,0);
	}
}

LRESULT CCmsAgreeDlg::OnNcHitTest(CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
	{
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		pDlg->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));  
	}
	return CDialog::OnNcHitTest(point);
}

void CCmsAgreeDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
//*	*/PostMessage( WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM( point.x, point.y));
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CCmsAgreeDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

void CCmsAgreeDlg::OnBnClickedButtonMin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}

void CCmsAgreeDlg::OnBnClickedButtonClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());

	//::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
	exit(0);
}

BOOL CCmsAgreeDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE )
		{
			//엔터를 Dialog로 넘겨주지 않음
			return FALSE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CCmsAgreeDlg::OnDestroy()
{
	CDialog::OnDestroy();
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
