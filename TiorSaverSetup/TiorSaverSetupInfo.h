#pragma once

class CTiorSaverSetupInfo
{
public:
	CTiorSaverSetupInfo(void);
	~CTiorSaverSetupInfo(void);

public:
	static bool	ClientInstallCheck();
	static void	FileExtract();
	static void  CreateRegistry();
	static void  SetRunKey();
	static void  FirewallException();
	static bool  InstallServices();
	static BOOL CreateDB();
	//static bool InstallDriver();
	static bool  StartServices();
	static bool  StartAgent();
	static bool IsOSCheck();
	static bool Install_RedistributablePackage();
	static void DeleteFirewallPolicy();
	//static bool GetVersionInfo( CString strFilePath, CString& strProductVersion, CString& strFileVersion );
	static void UacOff();
};
