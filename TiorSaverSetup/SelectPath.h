#pragma once
#include "afxwin.h"
#include "Control/Button/ImageButton.h"


// CSelectPath 대화 상자입니다.

class CSelectPath : public CDialog
{
	DECLARE_DYNAMIC(CSelectPath)

public:
	CImageButton		m_btInstallPrev;
	CImageButton		m_btInstallNext;
	CImageButton		m_btnClose;
	CImageButton		m_btnMin;
	CBitmap				m_hInputBack;
	CImageButton m_btFind;
	BOOL m_bFindStart;
	BOOL m_bButtonNext;

	CSelectPath(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CSelectPath();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_PATH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG * pMsg);
	afx_msg void OnBnClickedButtonFind();
	afx_msg void OnBnClickedButtonPrev();
	afx_msg void OnBnClickedButtonNext();
	CEdit m_editPath;
	CBitmap m_hBitBack;
	void ComponentLineUp();
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnBnClickedBtnMin();
//	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
