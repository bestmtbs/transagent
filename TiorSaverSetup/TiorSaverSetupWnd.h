#pragma once

// CTiorSaverSetupWnd


class CTiorSaverSetupWnd : public CWnd
{
	DECLARE_DYNAMIC(CTiorSaverSetupWnd)

public:
	CTiorSaverSetupWnd();
	virtual ~CTiorSaverSetupWnd();


protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnCreateTray();
	DECLARE_MESSAGE_MAP()
};


