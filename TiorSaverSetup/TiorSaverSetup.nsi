/*
  HM NIS Edit (c) 2003-2005 H?tor Mauricio Rodr?uez Segura <ranametal@users.sourceforge.net>
  For conditions of distribution and use, see license.txt

  Installation script

*/
!include "MUI.nsh"

!define PRODUCT_NAME "B2Saver"
!define PRODUCT_VERSION "1.2.1706.3001"
!define PRODUCT_PUBLISHER "<DSNTECH>.  All rights reserved."
!define PRODUCT_WEB_SITE "http://www.B2Saver.com"
!define APP_NAME "B2Saver Installer"
!define APP_VERSION "1.3.98.08.13"
!define APP_HOME_PAGE "http://www.B2Saver.com"

 
VIProductVersion "${PRODUCT_VERSION}"
VIAddVersionKey "ProductVersion" "${APP_VERSION}"
VIAddVersionKey "ProductName" "${PRODUCT_NAME}"
VIAddVersionKey "CompanyName" "${PRODUCT_PUBLISHER}"
VIAddVersionKey "FileVersion" "${PRODUCT_VERSION}"
VIAddVersionKey "InternalName" "${APP_NAME}"
VIAddVersionKey "FileDescription" "${APP_NAME}"
VIAddVersionKey "LegalCopyright" "${PRODUCT_PUBLISHER}"


!define MUI_ICON "res\TiorSaverSetup.ico"
; Language files
!insertmacro MUI_LANGUAGE "Korean"



OutFile    "..\Bin\x86\bin\TiorSaverInstaller.exe"

Function .onInit
    SetSilent silent
         SetOutPath "$TEMP"
        File "..\Bin\x86\bin\TiorSaverSetup.exe"
        File "vc_redist.x86.exe"
	File "vc_redist.x64.exe"
        File "WinPcap_4_1_3.exe"
        File "..\Bin\x86\bin\libcrypto-1_1.dll"
        File "..\Bin\x86\bin\libssl-1_1.dll"

        ;ReadRegStr $R4 HKLM "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{e2803110-78b3-4664-a479-3611a381656a}" ""
        ;IfErrors done 0
      
        ;ReadRegDword $R0 HKLM "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{BBF2AC74-720C-3CB3-8291-5E34039232FA}" "Version"
        ;IfErrors  0 done

        ;ReadRegDword $R0 HKLM "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{69BCE4AC-9572-3271-A2FB-9423BDA36A43}" "Version"
        ;IfErrors  0 done
        
				${If} ${RunningX64} != 0
				        Exec  "$TEMP\vc_redist.x86.exe /q /norestart"
					Exec  "$TEMP\vc_redist.x64.exe /q /norestart"
				${Else}
				        Exec  "$TEMP\vc_redist.x86.exe /q /norestart"
				${EndIf}       
done:

        Exec  "$TEMP\TiorSaverSetup.exe"
    
    
FunctionEnd



SecTion


SecTionEnd