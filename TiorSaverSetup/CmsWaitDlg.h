#pragma once
#include "Control/PictureEx.h"


// CCmsWaitDlg 대화 상자입니다.

class CCmsWaitDlg : public CDialog
{
	DECLARE_DYNAMIC(CCmsWaitDlg)

public:
	CCmsWaitDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCmsWaitDlg();

	void StartProgressBar();
	void StopProgressBar();

	void SetProgressMsg(CString strMsg);

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_NEW_WAIT };

	CBitmap m_hBitBack;
	CPictureEx		m_PictureProgressBar;
	CStatic			m_Static_Main;
	CFont			m_titleFont;


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
};
