#pragma once
#include "Control/Button/ImageButton.h"
#include "Control/Static/ColorStatic.h"
#include "afxwin.h"


// CCmsCompleteDlg 대화 상자입니다.

class CCmsCompleteDlg : public CDialog
{
	DECLARE_DYNAMIC(CCmsCompleteDlg)

public:
	CCmsCompleteDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CCmsCompleteDlg();

	CBitmap					m_hBitBack;
	CImageButton		m_btComplete;
	CImageButton		m_btnMin;
	CImageButton		m_btnClose;

	BOOL MySystemReboot();
	void ReLoad();


// 대화 상자 데이터입니다.
	enum { IDD = IDD_DIALOG_COMPLETE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonReboot();
	afx_msg void OnBnClickedButtonNoReboot();
	afx_msg void OnPaint();
private:
	void ComponentLineUp();
public:
	afx_msg LRESULT OnNcHitTest(CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedBtnMin();
	afx_msg void OnBnClickedBtnClose();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//afx_msg void OnBnClickedButtonCancel();
	afx_msg void OnClose();
};
