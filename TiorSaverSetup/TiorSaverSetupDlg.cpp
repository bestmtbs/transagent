
// TiorSaverSetupDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "TiorSaverSetupDlg.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include "UtilsReg.h"

#include <atlimage.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CTiorSaverSetupDlg 대화 상자




CTiorSaverSetupDlg::CTiorSaverSetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTiorSaverSetupDlg::IDD, pParent)
	,m_btInstallCancle(IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_O, IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_N, _T(""),0)
	,m_btInstallNext(IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, 0, _T(""),0)
	//,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, _T(""),0)
	//,m_btnMin(IDB_BMP_MIN_N, IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, 0, _T(""),0)

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pSelectPathDlg = NULL;	// 2015-11-24 kh.choi 설치 전 점검 다이얼로그 추가
	m_pAgreeDlg = NULL;
	m_pOneDlg = NULL;
	m_pTwoDlg = NULL;
	m_pVerifyDlg = NULL;
	m_pCompleteDlg = NULL;
	m_brush.CreateSolidBrush(GRAY_BRUSH);
}

CTiorSaverSetupDlg::~CTiorSaverSetupDlg()
{
	m_brush.DeleteObject();
	Close();
}

void CTiorSaverSetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_SETUP_INSTALL_CALCEL, m_btInstallCancle);
	DDX_Control(pDX, IDC_BUTTON_SETUP_INSTALL_NEXT, m_btInstallNext);
	//DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	//DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	//DDX_Control(pDX, IDC_CB_LANG, m_cb_Lang);
	//DDX_Control(pDX, IDC_ST_LANG, m_ST_Lang);
}

BEGIN_MESSAGE_MAP(CTiorSaverSetupDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
	//ON_MESSAGE_VOID(MESSAGE_AGREE_DIALOG, OnAgreeDlg)			// 2015-11-23 kh.choi 쓰이는 곳 없음 -> 주석처리
	ON_MESSAGE_VOID(MESSAGE_SELECTPATH_DIALOG, OnSelectPathDlg)
	ON_MESSAGE_VOID(MESSAGE_ONE_DIALOG, OnOneLevelDlg)
	ON_MESSAGE_VOID(MESSAGE_REGISTER_DIALOG, OnRegisterLevelDlg)
	//ON_MESSAGE_VOID(MESSAGE_THREE_DIALOG, OnThreeLevelDlg)
	ON_MESSAGE_VOID(MESSAGE_VERIFY_DIALOG, OnVerifyLevelDlg)
	ON_MESSAGE_VOID(MESSAGE_COMPLETE, OnSetupSuccess)
	ON_MESSAGE_VOID(MESSAGE_SETUP_CANCLE, OnSetupCancle)
	ON_MESSAGE_VOID(MESSAGE_AGREE_PREV, OnAgreePrev)
	ON_MESSAGE_VOID(MESSAGE_ONE_PREV, OnOnePrev)
	ON_MESSAGE_VOID(MESSAGE_REGISTER_PREV, OnRegisterPrev)
	ON_MESSAGE_VOID(MESSAGE_SELECTPATH_PREV, OnSelectPathPrev)
	ON_MESSAGE_VOID(MESSAGE_PROCESS_EXIT, OnExit)

	ON_MESSAGE_VOID(MESSAGE_MINUMUM, OnMinimum)
	//ON_MESSAGE_VOID(MESSAGE_HIDE, OnHide)
	
//	ON_WM_MOUSEMOVE()
	//ON_BN_CLICKED(IDC_BUTTON_SETUP_REBOOT, &CTiorSaverSetupDlg::OnBnClickedButtonSetupReboot)
	//ON_BN_CLICKED(IDC_BUTTON_SETUP_REBOOT_CANCLE, &CTiorSaverSetupDlg::OnBnClickedButtonSetupRebootCancle)
	ON_BN_CLICKED(IDC_BUTTON_SETUP_CANCEL, &CTiorSaverSetupDlg::OnBnClickedButtonSetupCancel)
	ON_STN_CLICKED(IDC_STATIC_INSTALL_STEP_GUIDE, &CTiorSaverSetupDlg::OnStnClickedStaticInstallStepGuide)
	ON_BN_CLICKED(IDC_BUTTON_SETUP_INSTALL_CALCEL, &CTiorSaverSetupDlg::OnBnClickedButtonSetupInstallCalcel)
	ON_BN_CLICKED(IDC_BUTTON_SETUP_INSTALL_NEXT, &CTiorSaverSetupDlg::OnBnClickedButtonSetupInstallNext)
	ON_WM_ERASEBKGND()
	ON_WM_NCHITTEST()
	//ON_CBN_SELCHANGE(IDC_CB_LANG, &CTiorSaverSetupDlg::OnCbnSelchangeCbLang)
	//ON_BN_CLICKED(IDC_BUTTON_MIN, &CTiorSaverSetupDlg::OnBnClickedButtonMin)
END_MESSAGE_MAP()


// CTiorSaverSetupDlg 메시지 처리기

BOOL CTiorSaverSetupDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다. 응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	ComponentLineUp();

	CWnd *pWnd = GetDlgItem(IDC_BUTTON_SETUP_SUCCESS);
	pWnd->ShowWindow(SW_HIDE);
	pWnd->EnableWindow(FALSE);

	SetWindowText(TIORSAVER_INSTALL);  //-> 문구 영어로 변경해야함.!!

	//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->SetFocus();	
	ReLoad();
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}


void CTiorSaverSetupDlg::ReLoad()
{
	BOOL bResult = FALSE;
	m_btInstallCancle.SizeToContent();
	m_btInstallNext.SizeToContent();
}



//버튼 정렬 및 다이얼로그 크기 및 위치 설정
void CTiorSaverSetupDlg::ComponentLineUp()
{
	BITMAP bm;
	m_hBitBack.LoadBitmap(IDB_BITMAP_BKG_WELCOME);
	m_hBitBack.GetBitmap(&bm);
	this->MoveWindow(0,0, bm.bmWidth, bm.bmHeight);
	CenterWindow(GetDesktopWindow());
	
	m_btInstallCancle.SizeToContent();
	m_btInstallNext.SizeToContent();

	m_btInstallCancle.MoveWindow(385, 327, m_btInstallCancle.GetWidth(),   m_btInstallCancle.GetHeight());
	m_btInstallNext.MoveWindow(485, 327, m_btInstallNext.GetWidth(),   m_btInstallNext.GetHeight());
	
}

HBRUSH CTiorSaverSetupDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	UINT uid = pWnd->GetDlgCtrlID();

	if(nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(39, 39, 39));
		return (HBRUSH)::GetStockObject(NULL_BRUSH);	
	}
	return hbr;

}

void CTiorSaverSetupDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다. 문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CTiorSaverSetupDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this); // device context for painting
		// TODO: 여기에 메시지 처리기 코드를 추가합니다.
		// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

		CRect rc;
		GetClientRect(&rc);
		BITMAP bm;
		CDC memDC;
	
		dc.FillSolidRect(rc, RGB(255,255,255));
		// client
		CBitmap * bitmap = NULL;

		m_hBitBack.GetBitmap(&bm);
		memDC.CreateCompatibleDC(&dc);
		bitmap = memDC.SelectObject(&m_hBitBack);
		dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
		memDC.SelectObject(bitmap);
		memDC.DeleteDC();

		//m_hBitWelcomTxt.GetBitmap(&bm);
		//memDC.CreateCompatibleDC(&dc);
		//bitmap = memDC.SelectObject(&m_hBitWelcomTxt);
		//dc.BitBlt(200, 130, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
		//memDC.SelectObject(bitmap);
		//memDC.DeleteDC();

		//m_hBitLogo.GetBitmap(&bm);
		//memDC.CreateCompatibleDC(&dc);
		//bitmap = memDC.SelectObject(&m_hBitLogo);
		//dc.BitBlt(40, 120, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
		//memDC.SelectObject(bitmap);
		//memDC.DeleteDC();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CTiorSaverSetupDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTiorSaverSetupDlg::CreateDlg(int iType)
{
	CRect rect;
//	Close(); 
	switch(iType)
	{
		case AGREE_DIALOG:
			{	
				if(m_pAgreeDlg)
				{
					delete m_pAgreeDlg;
					m_pAgreeDlg = NULL;
				}
					
				m_pAgreeDlg= new CCmsAgreeDlg;
				m_pAgreeDlg->Create(IDD_DIALOG_AGREE, this); 
				m_pAgreeDlg->ShowWindow(SW_SHOW);

				m_btInstallCancle.ShowWindow(FALSE);
				m_btInstallNext.ShowWindow(FALSE);
				//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_HIDE);
			}
			break;

		//// 2015-10-28 kh.choi 설치 전 점검 다이얼로그 추가
		case SELECTPATH_DIALOG:
			{
				if (m_pSelectPathDlg) {
					delete m_pSelectPathDlg;
					m_pSelectPathDlg = NULL;
				}

				m_pSelectPathDlg = new CSelectPath;
				m_pSelectPathDlg->Create(IDD_DIALOG_PATH, this);
				m_pSelectPathDlg->ShowWindow(SW_SHOW);

				m_btInstallCancle.ShowWindow(FALSE);
				m_btInstallNext.ShowWindow(FALSE);
				//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_HIDE);
			}
			break;

		case ONE_DIALOG:
			{
				if(m_pOneDlg)
				{
					delete m_pOneDlg;
					m_pOneDlg = NULL;
				}

				m_pOneDlg= new CCmsOneLevelDlg;
				m_pOneDlg->Create(IDD_DIALOG_ONE_LEVEL, this); 
				m_pOneDlg->ShowWindow(SW_SHOW);

				m_btInstallCancle.ShowWindow(FALSE);
				m_btInstallNext.ShowWindow(FALSE);
				//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_HIDE);
			}
			break;

		case TWO_DIALOG:
			{
				if(m_pTwoDlg)
				{
					delete m_pTwoDlg;
					m_pTwoDlg = NULL;
				}

				m_pTwoDlg= new CRegisterDlg;
				m_pTwoDlg->Create(IDD_REGISTER_DIALOG, this); 
				m_pTwoDlg->ShowWindow(SW_SHOW); 

				m_btInstallCancle.ShowWindow(FALSE);
				m_btInstallNext.ShowWindow(FALSE);
				//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_HIDE);
			}
			break;

		case VERIFY_DIALOG:
			{
				if(m_pVerifyDlg)
				{
					delete m_pVerifyDlg;
					m_pVerifyDlg = NULL;
				}
				m_pVerifyDlg = new CVerifyDlg;
				m_pVerifyDlg->Create(IDD_VERIFY_DIALOG, this);
				m_pVerifyDlg->ShowWindow(SW_SHOW);
			
				m_btInstallCancle.ShowWindow(FALSE);
				m_btInstallNext.ShowWindow(FALSE);
				//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_HIDE);
			}
			break;

		case COMPLETE_DIALOG:
			{
				if(m_pCompleteDlg)
				{
					delete m_pCompleteDlg;
					m_pCompleteDlg = NULL;
				}

				m_pCompleteDlg = new CCmsCompleteDlg;
				m_pCompleteDlg->Create(IDD_DIALOG_COMPLETE, this);
				m_pCompleteDlg->ShowWindow(SW_SHOW);
							
				m_btInstallCancle.ShowWindow(FALSE);
				m_btInstallNext.ShowWindow(FALSE);
				//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_HIDE);
			}
			break;

		default:
			break;

	}

}

void CTiorSaverSetupDlg::OnOneLevelDlg()
{
	CreateDlg(ONE_DIALOG);
	//CreateDlg(THREE_DIALOG);
}

void CTiorSaverSetupDlg::OnSelectPathDlg()
{
	CreateDlg(SELECTPATH_DIALOG);		// 2015-11-23 kh.choi 설치 전 점검 다이얼로그 생성
}

void CTiorSaverSetupDlg::OnRegisterLevelDlg()
{
	CreateDlg(TWO_DIALOG);
}
//
//void CTiorSaverSetupDlg::OnThreeLevelDlg()
//{
//	CreateDlg(THREE_DIALOG);
//}
//
void CTiorSaverSetupDlg::OnVerifyLevelDlg()
{
	CreateDlg(VERIFY_DIALOG);
}

void CTiorSaverSetupDlg::AllWindowHide()
{
	//if (m_pPrecheckDlg) {
	//	m_pPrecheckDlg->ShowWindow(SW_HIDE);
	//}
	if(m_pAgreeDlg)
		m_pAgreeDlg->ShowWindow(SW_HIDE);
	if(m_pOneDlg)
		m_pOneDlg->ShowWindow(SW_HIDE);
	if(m_pTwoDlg)
		m_pTwoDlg->ShowWindow(SW_HIDE);
	//if(m_pThreeDlg)
	//	m_pThreeDlg->ShowWindow(SW_HIDE);
}

void CTiorSaverSetupDlg::Close()
{
	if( m_pAgreeDlg != NULL )
	{
		delete m_pAgreeDlg;
		m_pAgreeDlg = NULL;
	}

	if( m_pOneDlg != NULL)
	{
		delete m_pOneDlg;
		m_pOneDlg = NULL;
	}

	if( m_pTwoDlg != NULL )
	{
		delete m_pTwoDlg;
		m_pTwoDlg = NULL;
	}

	//if( m_pThreeDlg != NULL )
	//{
	//	delete m_pThreeDlg;
	//	m_pThreeDlg = NULL;
	//}

	if(m_pCompleteDlg != NULL)
	{
		delete m_pCompleteDlg;
		m_pCompleteDlg = NULL;
	}
	
	
}

void CTiorSaverSetupDlg::OnSetupSuccess()
{
	
	CreateDlg(COMPLETE_DIALOG);
}

void CTiorSaverSetupDlg::OnSetupCancle()
{
}

void CTiorSaverSetupDlg::OnExit()
{
	CString strCheckPath =  CPathInfo::GetClientInstallPath()+ _T("register.success");
	if(FALSE == FileExists(strCheckPath))
	{
		theApp.DeleteRequest();
	}
	theApp.MoveFile();
	CDialog::OnCancel();
	
	HWND hWnd= ::FindWindow(NULL, TIORSAVER_INSTALL);
	if( hWnd )
		::SendMessage(hWnd,WM_CLOSE, 0,0);
}

void CTiorSaverSetupDlg::OnAgreePrev()
{
	m_btInstallCancle.ShowWindow(SW_SHOW);
	m_btInstallNext.ShowWindow(SW_SHOW);

	//GetDlgItem(IDC_STATIC_OFFCICE_MARK_N)->ShowWindow(SW_SHOW);
}

void CTiorSaverSetupDlg::OnOnePrev()
{

	CreateDlg(TWO_DIALOG);
	//CreateDlg(AGREE_DIALOG);
}

void CTiorSaverSetupDlg::OnSelectPathPrev()
{
	CreateDlg(AGREE_DIALOG);
}

void CTiorSaverSetupDlg::OnRegisterPrev()
{
	CreateDlg(SELECTPATH_DIALOG);
}


void CTiorSaverSetupDlg::OnCancel()
{
	int iResult = 0;

	iResult =MessageBox(_T("Are you sure you want to cancel the installation?"), _T("TiorSaver"), MB_YESNO);

	if( iResult == IDYES )
	{
		// TODO 이부분에  드라이버 관련 삭제 부분도 들어가야함
		//CCmsUninstall uninstall;
		//uninstall.SetupExit();

		theApp.MoveFile();
		CDialog::OnCancel();
	}
}

void CTiorSaverSetupDlg::OnBnClickedButtonSetupCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	exit(0);
}

void CTiorSaverSetupDlg::OnStnClickedStaticInstallStepGuide()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CTiorSaverSetupDlg::OnBnClickedButtonSetupInstallCalcel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//CCmsUninstall uninstall;
	//uninstall.SetupExit();
	theApp.MoveFile();
	exit(0);
}

void CTiorSaverSetupDlg::OnBnClickedButtonSetupInstallNext()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	//m_cb_Lang.ShowWindow(SW_HIDE);
	//m_ST_Lang.ShowWindow(SW_HIDE);
	CreateDlg(AGREE_DIALOG);
}

BOOL CTiorSaverSetupDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

void CTiorSaverSetupDlg::OnMinimum()
{
	this->ShowWindow(SW_MINIMIZE);
}

//void CTiorSaverSetupDlg::OnHide()
//{
//	this->ShowWindow(SW_HIDE);
//}

LRESULT CTiorSaverSetupDlg::OnNcHitTest(CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
		return HTCAPTION;

	return CDialog::OnNcHitTest(point);
}

BOOL CTiorSaverSetupDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE )
		{
			//엔터를 Dialog로 넘겨주지 않음
			return FALSE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CTiorSaverSetupDlg::OnBnClickedButtonMin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}
