#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "TiorSaverSetupDlg.h"
#include "UtilsProcess.h"
#include "PathInfo.h"
#include "Impersonator.h"
//#include "HttpsTConnect.h"
#include "ConnectParamInfo.h"
#include "ParamParser.h"
#include "UtilCab.h"
#include "UtilsUnicode.h"
#include "CmsDBManager.h"
#include "UtilsFile.h"
#include "TiorSaverSetupInfo.h"


//#include "WaitDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

/********************************************************************************
 @class     CTiorSaverSetup
 @brief      클라이언트 설치 
 @note      
*********************************************************************************/
// CTiorSaverSetupApp

BEGIN_MESSAGE_MAP(CTiorSaverSetupApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CTiorSaverSetupApp 생성
/**
 @brief     생성자
 @author   JHLEE
 @date      2011.06.08
 @note      초기화 작업
*/
CTiorSaverSetupApp::CTiorSaverSetupApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	m_pMemberData = new CMemberData;
	m_strInstallPath = _T("");
	m_nScreenIdx = -1;
	m_bJustAfterSelectPath = TRUE;
	m_nDepartmentID = 0;
	m_fonthandle = INVALID_HANDLE_VALUE;
}

/**
 @brief     소멸자
 @author    kh.choi
 @date      2017-01-20
*/
CTiorSaverSetupApp::~CTiorSaverSetupApp(void)
{
	if(m_fonthandle)
	{
		RemoveFontMemResourceEx(m_fonthandle);
	}
}

// 유일한 CTiorSaverSetupApp 개체입니다.

CTiorSaverSetupApp theApp;

// CTiorSaverSetupApp 초기화

BOOL CTiorSaverSetupApp::InitInstance()
{
	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.

	m_Minidump.install_self_mini_dump();


	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	if( CProcess::ProcessCreateMutex(WTIOR_INSTALL_AGENT_MUTEX_NAME) == false )
	{
		MessageBox(NULL,_T("Installation is in progress"), _T("TiorSaver"), MB_OK);
		exit(0);
	}

	if( FALSE == IsUserAnAdmin() )  //관리자 계정이 아니면 설치를 하지 않는다.
	{	
		MessageBox(NULL,_T("Please install through the administrator account"), _T("TiorSaver"), MB_OK);
		exit(0);
	}

	HINSTANCE hResInstanceBold = AfxGetResourceHandle( );
	HINSTANCE hResInstanceRegular = AfxGetResourceHandle( );

	HRSRC res = FindResource(hResInstanceBold,
		MAKEINTRESOURCE(IDR_NOTOSAN_BOLD_FONT),L"BINARY");
	if (res) 
	{
		HGLOBAL mem = LoadResource(hResInstanceBold, res);
		void *data = LockResource(mem);
		size_t len = SizeofResource(hResInstanceBold, res);

		DWORD nFonts;
		m_fonthandle = AddFontMemResourceEx(
			data,       // font resource
			len,       // number of bytes in font resource 
			NULL,          // Reserved. Must be 0.
			&nFonts      // number of fonts installed
			);

		m_ttfNotoSansBold.Parse((BYTE*)(data), len);

		if(m_fonthandle==0)
		{
			MessageBox(NULL, L"Font add fails 1", L"TiorSaver", MB_OK);
		}
	}
	res = FindResource(hResInstanceRegular,
		MAKEINTRESOURCE(IDR_NOTOSAN_REGULAR_FONT),L"BINARY");
	if (res) 
	{
		HGLOBAL mem = LoadResource(hResInstanceRegular, res);
		void *data = LockResource(mem);
		size_t len = SizeofResource(hResInstanceRegular, res);

		DWORD nFonts;
		m_fonthandle = AddFontMemResourceEx(
			data,       // font resource
			len,       // number of bytes in font resource 
			NULL,          // Reserved. Must be 0.
			&nFonts      // number of fonts installed
			);

		m_ttfNotoSansRegular.Parse((BYTE*)(data), len);

		if(m_fonthandle==0)
		{
			MessageBox(NULL, L"Font add fails 2", L"TiorSaver", MB_OK);
		}
	}

	CTiorSaverSetupDlg dlg;

	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
		//  코드를 배치합니다.
	}

	
	// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	// 반환합니다.
	return FALSE;
}

//static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
//	((std::string *)userp)->append((char*)contents, size * nmemb);
//	return size * nmemb;
//}

CString CTiorSaverSetupApp::GetRcPath(CString _strFile)
{
	return _T("res/")+_strFile;
}
//
//CString CTiorSaverSetupApp::CurlGET(CString _strUrl, INT& nErr)
//{
//	BOOL bResult = FALSE;
//	std::string readBuffer;
//	readBuffer.clear();
//	CURL *curl;
//	CURLcode res;
//	//html_context_data data = {0, 0};
//
//	CStringA strUrlA = Utf8_Encode(_strUrl);
//	CStringA strReadBufferA;
//	CString strReadBufferW = _T("");
//
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//	if(curl)
//	{
//		CString strTokenHeader = _T("");
//		curl_slist* header = NULL ;
//		strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//		header = curl_slist_append( header ,  strTokenHeaderA);
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if (readBuffer.size() > 0)
//		{
//			strReadBufferA = readBuffer.c_str();
//			strReadBufferW = Utf8_Decode(strReadBufferA);
//			//SetToken((CString)cs);
//		}
//		if(res != CURLE_OK) {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_getfail"));
//			strCount.Format(_T("[CurlGET]%s res: %d [line: %d, function: %s, file: %s]"), _strUrl, res, __LINE__, __FUNCTIONW__, __FILEW__);
//			DROP_TRACE_LOG(_T("TiorSaver_getfail"), strCount);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
//		}
//		curl_slist_free_all(header) ;
//		curl_easy_cleanup(curl);
//	}
//	curl_global_cleanup();
//	return strReadBufferW;
//}
//
//CString CTiorSaverSetupApp::CurlPost(CString _strParam, CString _strUrl, INT& nErr)
//{
//	BOOL bResult = FALSE;
//	std::string readBuffer;
//	readBuffer.clear();
//	CURL *curl;
//	CURLcode res;
//	//html_context_data data = {0, 0};
//
//	CStringA strUrlA = Utf8_Encode(_strUrl);
//	CStringA strReadBufferA;
//	CString strReadBufferW = _T("");
//
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//	if (curl)
//	{
//		CString strTokenHeader = _T("");
//		curl_slist* header = NULL ;
//		strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//		CStringA strDataA = Utf8_Encode(_strParam);
//		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//		header = curl_slist_append( header ,  strTokenHeaderA);
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//		curl_easy_setopt(curl, CURLOPT_POST, 1);
//		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strDataA);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		//curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if (readBuffer.size() > 0)
//		{
//			strReadBufferA = readBuffer.c_str();
//			if (strReadBufferA.GetLength() > 0) {
//				strReadBufferW = Utf8_Decode(strReadBufferA);
//			}
//			//SetToken((CString)cs);
//		}
//		if(res != CURLE_OK) {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_postfail"));
//			strCount.Format(_T("[CurlPost]URL: %s, Param: %s res: %d [line: %d, function: %s, file: %s]"), _strUrl, _strParam, res, __LINE__, __FUNCTIONW__, __FILEW__);
//			DROP_TRACE_LOG(_T("TiorSaver_postfail"), strCount);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
//		}
//		curl_slist_free_all(header) ;
//		curl_easy_cleanup(curl);
//	}
//	curl_global_cleanup();
//	return strReadBufferW;
//}
//
//CString CTiorSaverSetupApp::CurlDelete(CString _strParam, CString _strUrl, INT& nErr)
//{
//	BOOL bResult = FALSE;
//	std::string readBuffer;
//	readBuffer.clear();
//	CURL *curl;
//	CURLcode res;
//	//html_context_data data = {0, 0};
//
//	CStringA strUrlA = Utf8_Encode(_strUrl);
//	CStringA strReadBufferA;
//	CString strReadBufferW = _T("");
//
//	curl_global_init(CURL_GLOBAL_ALL);
//	curl = curl_easy_init();
//
//	if (curl)
//	{
//		CString strTokenHeader = _T("");
//		curl_slist* header = NULL ;
//		strTokenHeader.Format(_T("Authorization: Bearer %s"), GetToken());
//		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
//		CStringA strDataA = Utf8_Encode(_strParam);
//		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
//		header = curl_slist_append( header ,  strTokenHeaderA);
//		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
//		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
//		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
//		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strDataA);
//		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
//		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
//		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
//		curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
//
//		/* Now run off and do what you've been told! */ 
//		res = curl_easy_perform(curl);
//		/* Check for errors */ 
//		if (readBuffer.size() > 0)
//		{
//			strReadBufferA = readBuffer.c_str();
//			if (strReadBufferA.GetLength() > 0) {
//				strReadBufferW = Utf8_Decode(strReadBufferA);
//			}
//			//SetToken((CString)cs);
//		}
//		if(res != CURLE_OK) {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_deletefail"));
//			strCount.Format(_T("[CurlDelete]URL: %s, Param: %s res: %d [line: %d, function: %s, file: %s]"), _strUrl, _strParam, res, __LINE__, __FUNCTIONW__, __FILEW__);
//			DROP_TRACE_LOG(_T("TiorSaver_deletefail"), strCount);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
//		} else {
//			nErr = res;
//			//CString strCountFile = _T("");
//			CString strCount = _T("");
//			//DWORD dwBytesWritten = 0;
//			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_deletesuccess"));
//			strCount.Format(_T("[CurlDelete]URL: %s, Param: %s, Return: %s [line: %d, function: %s, file: %s]"), _strUrl, _strParam, strReadBufferW, __LINE__, __FUNCTIONW__, __FILEW__);
//			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
//			DROP_TRACE_LOG(_T("TiorSaver_deletesuccess"), strCount);
//		}
//		curl_slist_free_all(header) ;
//		curl_easy_cleanup(curl);
//	}
//	curl_global_cleanup();
//	return strReadBufferW;
//}
//
//CStringA CTiorSaverSetupApp::Utf8_Encode(CStringW strData)
//{
//	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), NULL, 0, NULL, NULL);
//	std::string strTo( size_needed+1, 0 );
//	WideCharToMultiByte                  (CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), &strTo[0], size_needed, NULL, NULL);
//	return strTo.c_str();
//}
//
//CStringW CTiorSaverSetupApp::Utf8_Decode(CStringA strData)
//{
//	int size_needed = MultiByteToWideChar(CP_UTF8, 0,strData.GetString(), -1, NULL, 0);
//	std::wstring wstrTo( size_needed+1, 0 );
//	wstrTo.clear();
//	MultiByteToWideChar (CP_UTF8, 0, strData.GetString(),-1, &wstrTo[0], size_needed+1);
//	return wstrTo.c_str();
//}

void CTiorSaverSetupApp::SetToken(CString _strToken)
{
	CString strValue = _strToken;
	if (strValue.GetLength() <= 0) {
		CString strLog = _T("");
		strLog.Format(_T("[curltest]invalid token [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
		return;
	}
	if (0 != m_strToken.Compare(_strToken))
	{
		m_strToken = _strToken;
		CString strTokenFilePath = _T("");
		strTokenFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("my.token"));
		DWORD dwBytesWritten = 0;
		//if (!WriteFileTxt(strTokenFilePath, m_strToken, nFlag)) {

		//CStringA strToWriteA = Utf8_Encode(_strToken);
		int nCnt = 0;
		while(!WriteFileExample(strTokenFilePath, _strToken, dwBytesWritten) || 10 == nCnt) {
			nCnt++;
			CString strLog = _T("");
			strLog.Format(_T("[curltest]invalid token file [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
	}
}

CString CTiorSaverSetupApp::GetToken()
{
	if (m_strToken.IsEmpty())
	{
		CString strTokenFilePath = _T("");
		CString strReadData = _T("");
		strTokenFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("my.token"));
		ReadAnsiFileTxt(strTokenFilePath, strReadData);
		m_strToken = strReadData;
	}
	return m_strToken;
}

BOOL CTiorSaverSetupApp::CreateFileEx (WORD _iResource, CString& _strFilePath, CString _strType)
{
	HRSRC hRes = NULL;

	hRes = FindResource(NULL, MAKEINTRESOURCE(_iResource), _strType);

	if (hRes == NULL) return FALSE;

	LPVOID mem = LoadResource (NULL, hRes);

	if (mem == NULL) return FALSE;

	DWORD dwBufferSize = SizeofResource(NULL, hRes);

	HANDLE handle = CreateFile (_strFilePath, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (handle == NULL || handle == INVALID_HANDLE_VALUE) 
	{
		return FALSE;
	}

	/* Error Handler ///////////////////////////////////////
	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{
	CloseHandle(handle);
	return TRUE;
	}*//////////////////////////////////////////////////////

	DWORD dwWriteSize = 0;

	BOOL bResult = WriteFile(handle, mem, dwBufferSize, &dwWriteSize, NULL);

	CloseHandle(handle);

	return bResult;
}

DWORD CTiorSaverSetupApp::CreateProcessEx(LPTSTR pszCommand, USHORT iShowMode, BOOL bWait)
{
	STARTUPINFO			si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));

	si.cb			= sizeof(si);

	if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;

	si.wShowWindow	= iShowMode;


		SECURITY_ATTRIBUTES saPipe;

	saPipe.lpSecurityDescriptor = (PSECURITY_DESCRIPTOR) malloc(SECURITY_DESCRIPTOR_MIN_LENGTH);

	InitializeSecurityDescriptor(saPipe.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);

	// Access Control list is asiigned as NULL inorder to allow all access to the object.

	SetSecurityDescriptorDacl(saPipe.lpSecurityDescriptor, TRUE, (PACL) NULL,FALSE);

	saPipe.nLength = sizeof(saPipe);

	saPipe.bInheritHandle = TRUE;
	

	// Start the child process. 
	BOOL bResult = CreateProcess(NULL,				// No module name (use command line). 
		pszCommand,		// Command line. 
		&saPipe,             // Process handle not inheritable. 
		NULL,             // Thread handle not inheritable. 
		FALSE,            // Set handle inheritance to FAL
		NORMAL_PRIORITY_CLASS,
		NULL,             // Use parent's environment block. 
		NULL,             // Use parent's starting directory. 
		&si,              // Pointer to STARTUPINFO structure.
		&pi);             // Pointer to PROCESS_INFORMATION structure.

	if (!bResult) return FALSE;

	// Wait until child process exits.
	if (bWait)
	{
		if (pi.hProcess != NULL && pi.hProcess != INVALID_HANDLE_VALUE)
			WaitForSingleObject (pi.hProcess, INFINITE);
	}

	DWORD dwExit=0;
	GetExitCodeProcess(pi.hProcess,&dwExit); 

	// Close process and thread handles.
	CloseHandle (pi.hProcess);
	CloseHandle	(pi.hThread);

	return dwExit;
}

CString CTiorSaverSetupApp::GetUserId()
{
	return m_strUserId;
}

BOOL CTiorSaverSetupApp::CreateDB()
{
	BOOL bCreate;
	CCmsDBManager dbManage(DB_AGENT_INFO);
	bCreate = dbManage.CreateQuery();

	dbManage.Free();

	return bCreate;
}

void CTiorSaverSetupApp::DeleteMyself(CString _strPath)
{
	CString strFilePath = _T("");

	CFileFind finder;

	BOOL bWorking = finder.FindFile(_strPath + _T("\\*.*"));

	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		if (finder.IsDots())
			continue;

		if (finder.IsDirectory()) {
			strFilePath = finder.GetFilePath();
			DeleteMyself(strFilePath);
		} else {
			strFilePath = finder.GetFilePath();
			ForceDeleteFile(strFilePath);
		}
	}
	ForceDeleteFile(_strPath);
}

BOOL CTiorSaverSetupApp::MoveFile()
{
	CString strRootDir = _T("");
	strRootDir = CPathInfo::GetClientInstallPath();
	DWORD dwErrorCode = 0;
	DWORD dwError = 0;
	CString strCfgPath = _T("");
	CString strDBPath = _T("");
	CString strLogPath = _T("");
	CString strOffsetPath = _T("");
	CString strUpdatePath = _T("");
	CString strBackupPath = _T("");
	CString strDownloadPath = _T("");
	strCfgPath.Format(_T("%scfg"), strRootDir);
	strDBPath.Format(_T("%sDB"), strRootDir);
	strLogPath.Format(_T("%slog"), strRootDir);
	strOffsetPath.Format(_T("%soffset"), strRootDir);
	strUpdatePath.Format(_T("%sUpdate"), strRootDir);
	strDownloadPath.Format(_T("%sUDownload"), strRootDir);
	strBackupPath.Format(_T("%sUBackup"), strRootDir);


	CString strCmd = _T("");
	strCmd.Format(_T("taskkill /F /T /IM TiorSaver*\r\n"));
	WinExec((LPCSTR)(LPCTSTR)strCmd, SW_HIDE);

	DeleteMyself(strRootDir);
	UM_WRITE_LOG(_T("MoveFile() Start..1"))
		ForceDeleteDir(strCfgPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strDBPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strLogPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strOffsetPath, &dwErrorCode);
	dwError += dwErrorCode;
	UM_WRITE_LOG(_T("MoveFile() Start..2"))
	ForceDeleteDir(strUpdatePath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strDownloadPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strBackupPath, &dwErrorCode);
	dwError += dwErrorCode;
	UM_WRITE_LOG(_T("MoveFile() Start..3"))
		ForceDeleteDir(strRootDir, &dwErrorCode);
	dwError += dwErrorCode;
	UM_WRITE_LOG(_T("MoveFile() Start..4"));

	CFile unFile;
	CString strFile = CPathInfo::GetTransferLogFilePath() + _T("TiorSaver_Unsuccess.cms");
	if (unFile.Open(strFile, CFile::modeCreate | CFile::modeWrite)) {
		unFile.Close();
	}
	ForceDeleteFile(strFile);

	if (dwError != 0) {
		return FALSE;
	}
	else {
		return TRUE;
	}
}

BOOL CTiorSaverSetupApp::DeleteRequest()
{
	BOOL bResult = FALSE;
	CString strLog = _T("");
	CString strUninstall = _T("");
	strUninstall.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_UNINSTALL_AGENT_NAME);
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strParam = _T("");
	CString strTokenFromReturn = _T("");
	CParamParser parser;
	INT nErr = 0;

	strParam = parser.CreateKeyValueRequest();	// 2017-10-12 sy.choi 삭제 완료 서버로 전송. 서버의 return과 상관없이 삭제해야 함.
	strUrl.Format(_T("%s/v1/agent/remove.dsntech"), theApp.m_pMemberData->GetServerHostURL());
	strReturn = theApp.m_curl.CurlDelete(strParam, strUrl, GetToken());
	if (parser.ParserOnlyToken(strReturn, strTokenFromReturn)) {
		theApp.SetToken(strTokenFromReturn);
		CString strCheckPath =  CPathInfo::GetClientInstallPath()+ _T("register.success");
		ForceDeleteFile(strCheckPath);
		bResult = TRUE;
	} else {
		if (strTokenFromReturn.IsEmpty()) {
			if (0 == nErr) {
				strTokenFromReturn.Format(_T("%s"), strReturn);
			} else {
				strTokenFromReturn.Format(_T("%d"), nErr);
			}
		}
	}
	return bResult;
}

void CTiorSaverSetupApp::Write_Index(CString _strPath, CString _strHash, CString _strVersion)
{
	BOOL bRet ;
	CString strIni_Path=L"", strIndex, strSection = _T("");
	strSection.Format( L"%s", ExtractFileName(_strPath));
	strSection.Remove(_T('\"'));
	strIni_Path.Format(_T("%sUDownload\\%s"), CPathInfo::GetClientInstallPath(), _T("TSVersion.ini"));
	bRet = WritePrivateProfileString( strSection, _T("path"), _strPath.GetBuffer(0) ,strIni_Path);
	bRet = WritePrivateProfileString( strSection, _T("hash"), _strHash.GetBuffer(0) ,strIni_Path);
	bRet = WritePrivateProfileString( strSection, _T("ver"), _strVersion.GetBuffer(0) ,strIni_Path);
	if( !bRet )
	{
		//필요시 로그를 남길것
	}
}

BOOL CTiorSaverSetupApp::WriteVersionFile()
{
	BOOL bResult = FALSE;
	INT nErr = 0;
	CParamParser parser;
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strMessage = _T("");
	UPDATE_SET * pUpdateSet = new UPDATE_SET;
	strUrl.Format(_T("%s/v1/agent/install.dsntech?%s"), theApp.m_pMemberData->GetServerHostURL(), parser.CreateFirstValueData());
	strReturn = theApp.m_curl.CurlGET(strUrl, GetToken());
	if (parser.ParserInstallUpdateSet(strReturn, strMessage, pUpdateSet)) {
		if (NULL != pUpdateSet) {
			bResult = TRUE;
			CString strQuery = _T("");
			CString strNewVer = _T("");
			CString strIni_Path = _T("");
			CString strCurVersion = _T("");
			strIni_Path.Format(_T("%sUDownload\\%s"), CPathInfo::GetClientInstallPath(), _T("TSVersion.ini"));
			//다운받을 폴더가 존재하는지 체크
			int iPos = strIni_Path.ReverseFind( _T('\\') );
			CString sDownPath = strIni_Path.Left( iPos);
			if(FALSE == FileExists(sDownPath)) {
				if(FALSE == ForceCreateDir(sDownPath)) {
					CString sLog;
					sLog.Format( _T("Fail(ForceCreateDir) - Filename : %s"),sDownPath);			
					UM_WRITE_LOG(sLog);	
					SE_MemoryDelete(pUpdateSet);	
					return FALSE;
				}
			} else if (FileExists(strIni_Path)) {
				ForceDeleteFile(strIni_Path);
			}
			strNewVer = pUpdateSet->strNewVersion.GetBuffer(0);
			strQuery.Format(_T("update %s set ver = \"%s\" where device_uuid = \"%s\""), DB_AGENT_INFO, pUpdateSet->strNewVersion, theApp.m_pMemberData->GetDeviceUUID());
			CCmsDBManager dbManage(DB_AGENT_INFO);
			dbManage.UpdateQuery(strQuery);
			dbManage.Free();
			WritePrivateProfileString( STR_CMS_VER_SECTION, STR_CMS_VER_SECTION, pUpdateSet->strNewVersion.GetBuffer(0), strIni_Path);

			for (int i =0; i < pUpdateSet->arrPath.GetSize(); i++)
			{
				CString strUpdateVersion = _T("");
				CString strUpdateHash = _T("");
				CString strUpdatePath = _T("");
				if (0 < pUpdateSet->arrPath.GetSize()) {
					strUpdatePath = pUpdateSet->arrPath.GetAt(i);
				} else {
					return FALSE;
				}
				if (0 <pUpdateSet->arrHash.GetSize()) {
					strUpdateHash = pUpdateSet->arrHash.GetAt(i);
				} else {
					return FALSE;
				}
				if (0 < pUpdateSet->arrVersion.GetSize()) {
					strUpdateVersion = pUpdateSet->arrVersion.GetAt(i);
				}
				if (strUpdateVersion.IsEmpty())
					strUpdateVersion = pUpdateSet->strNewVersion;
				theApp.Write_Index(strUpdatePath, strUpdateHash, strUpdateVersion);
			}
		}
	} else {
		if (strMessage.IsEmpty()) {
			if (0 == nErr) {
				strMessage.Format(_T("%s"), strReturn);
			} else {
				strMessage.Format(_T("%d"), nErr);
			}
		}
	}

	return bResult;
}
//
//void CTiorSaverSetupApp::StartTray()
//{
//	INT nCnt = 0;
//	CImpersonator imp;
//	CString strTrayName = CPathInfo::GetClientInstallPath() + WTIOR_TRAY_NAME;
//	while (!imp.CreateProcessEx(strTrayName)) {
//		nCnt++;
//		if (nCnt > 5)
//			break;
//		Sleep(1000);
//	}
//	imp.Free();
//}