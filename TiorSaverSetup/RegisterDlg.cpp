// TiorSaverTrayPopupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "RegisterDlg.h"
#include "ParamParser.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include "pipe/C_SendAgent_S_CollectAgent/STOCPipeClient.h"
#include "WTSSession.h"
#include "Impersonator.h"
#include "CmsDBManager.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#pragma comment(lib, "UxTheme.lib")

// CAboutDlg dialog used for App About

extern CTiorSaverSetupApp theApp;
IMPLEMENT_DYNAMIC(CRegisterDlg, CDialog)


CRegisterDlg::CRegisterDlg(CWnd* pParent /*=NULL*/)
: CDialog(CRegisterDlg::IDD, pParent)
,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, _T(""),0)
,m_btnMin(IDB_BMP_MIN_N, IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, 0, _T(""),0)
//,m_btnVerifyLicense(IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, _T(""),0)
,m_btRegister(IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, IDB_BMP_NEXT_N, IDB_BMP_NEXT_N, _T(""),0)
,m_btInstallPrev(IDB_BMP_PREV_N, IDB_BMP_PREV_N, IDB_BMP_PREV_O, 0, 0, _T(""),0)
//,m_btInstallNext(IDB_BMP_NEXT_N,IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, IDB_BMP_NEXT_N, _T(""),0)
{
	m_cbDepartment.ResetContent();
	//m_strLicenseCode = _T("");
	//m_nIsHttp = 1;
	m_bButtonRegister = FALSE;
}

void CRegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX,IDC_COMBOBOX_DEPARTMENT, m_cbDepartment);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
	//((CButton*)GetDlgItem(IDC_RADIO_HTTP))->SetCheck(1);
	//DDX_Control(pDX, IDC_BUTTON_GETTOKEN_CURL, m_btnVerifyLicense);
	DDX_Control(pDX, IDC_BUTTON_REGISTER_CURL, m_btRegister);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_btInstallPrev);
	//DDX_Control(pDX, IDC_BUTTON_NEXT, m_btInstallNext);
}

BEGIN_MESSAGE_MAP(CRegisterDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_BUTTON_REGISTER_CURL, &CRegisterDlg::OnBnClickedButtonRegisterCurl)
	ON_NOTIFY(CBEN_INSERTITEM, IDC_COMBOBOX_DEPARTMENT, &CRegisterDlg::OnCbenInsertitemComboboxexDepartment)
	ON_BN_CLICKED(IDC_BUTTON_PREV, &CRegisterDlg::OnBnClickedButtonPreview)
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CRegisterDlg::OnBnClickedButtonMin)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CRegisterDlg::OnBnClickedButtonClose)
	ON_WM_CTLCOLOR()
ON_WM_NCHITTEST()
END_MESSAGE_MAP()


// CTiorSaverTrayPopupDlg message handlers
BOOL CRegisterDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CenterWindow();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	m_bmpInputName.LoadBitmap(IDB_BMP_NAME_INPUT_N);
	ComponentLineUp();
	CString strClientPath = CPathInfo::GetClientInstallPath();

	m_cbDepartment.ResetContent();
	for (int i = 0; i < theApp.m_arrDepartment.GetSize(); i++)
	{
		DEPARTMENT* department = theApp.m_arrDepartment.GetAt(i);
		//if (department->strDepartmentName.LoadString(IDC_COMBOBOXEX_DEPARTMENT)) {
			m_cbDepartment.AddString(department->strDepartmentName);
		//}
	}
	m_cbDepartment.SetCurSel(0);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CRegisterDlg::ComponentLineUp()
{
	m_bmpBackground.LoadBitmap(IDB_BITMAP_BKG_NAME);
	BITMAP bm;
	m_bmpBackground.GetBitmap(&bm);
	MoveWindow(0,0,bm.bmWidth, bm.bmHeight);		//이미지 크기에 맞게 다이얼로그 사이즈 조정
	this->CenterWindow(GetParent());
	SetWindowTheme(GetDlgItem(IDC_BUTTON_PREV)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_REGISTER_CURL)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_CLOSE)->m_hWnd, _T(""), _T(""));
	SetWindowTheme(GetDlgItem(IDC_BUTTON_MIN)->m_hWnd, _T(""), _T(""));	
	CFont fnt;
	LOGFONT lf;
	::ZeroMemory(&lf, sizeof(lf) );
	lf.lfHeight = 14;
	lf.lfWeight =FW_THIN;
	wcscpy(lf.lfFaceName, _T("SkiCargo"));
	wcscpy_s(lf.lfFaceName, theApp.m_ttfNotoSansRegular.GetFontFamilyName().c_str());
	fnt.CreateFontIndirect(&lf);
	GetDlgItem(IDC_EDIT_AGENT_NAME)->SetFont(&fnt);
	GetDlgItem(IDC_COMBOBOX_DEPARTMENT)->SetFont(&fnt);
	fnt.Detach();

	m_btRegister.SizeToContent();
	m_btInstallPrev.SizeToContent();

	GetDlgItem(IDC_EDIT_AGENT_NAME)->MoveWindow(107, 144, 200, 15);
	GetDlgItem(IDC_COMBOBOX_DEPARTMENT)->MoveWindow(104, 104, 210, 17);
	m_btInstallPrev.MoveWindow(385, 327, m_btRegister.GetWidth(), m_btRegister.GetHeight());
	m_btRegister.MoveWindow(485, 327, m_btInstallPrev.GetWidth(), m_btInstallPrev.GetHeight());
	m_btnMin.SizeToContent();
	m_btnClose.SizeToContent();
	m_btnClose.MoveWindow(550, 4, m_btnClose.GetWidth(),   m_btnClose.GetHeight());
	m_btnMin.MoveWindow(520, 4, m_btnMin.GetWidth(),   m_btnMin.GetHeight());

}

void CRegisterDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting

	BITMAP bm;
	CDC memDC;

	//dc.FillSolidRect(rc, RGB(255,255,255));

	CBitmap * bitmap = NULL;
	m_bmpBackground.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_bmpBackground);

	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
	m_bmpInputName.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);

	bitmap = memDC.SelectObject(&m_bmpInputName);
	dc.BitBlt(100, 140, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CRegisterDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CRegisterDlg::OnDestroy()
{
	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CRegisterDlg::OnBnClickedButtonRegisterCurl()
{
	if (m_bButtonRegister) {
		return;
	}
	m_bButtonRegister = TRUE;
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strDepartmentName = _T("");
	CString strAgentName = _T("");
	BOOL bResult = FALSE;

	m_cbDepartment.GetLBText(m_cbDepartment.GetCurSel(), strDepartmentName);
	//theApp.m_pMemberData->SetDepartmentID(nDepartmentID);
	
	GetDlgItemText(IDC_EDIT_AGENT_NAME, strAgentName);
	strAgentName.Remove(_T(' '));
	if (strAgentName.IsEmpty()) {
		m_bButtonRegister = FALSE;
		MessageBox(_T("사용자 이름을 입력해주세요"), _T("TiorSaver"), MB_OK);
		return;
	}

	for (int i = 0; i < theApp.m_arrDepartment.GetSize(); i++)
	{
		CString strDepartmentNameFromList = _T("");
		strDepartmentNameFromList = theApp.m_arrDepartment.GetAt(i)->strDepartmentName;
		if (0 == strDepartmentNameFromList.CompareNoCase(strDepartmentName)) {
			theApp.m_pMemberData->ResetAgentInformation();
			theApp.m_pMemberData->SetDepartmentID(theApp.m_arrDepartment.GetAt(i)->nDepartmentID);
			theApp.m_pMemberData->SetAgentName(strAgentName);
			bResult = theApp.m_pMemberData->SetAgentInformation();
			bResult &= theApp.m_pMemberData->SetServerHostURL(theApp.m_pMemberData->m_strServerHostURL);

			if (bResult) {
				theApp.m_nDepartmentID = theApp.m_arrDepartment.GetAt(i)->nDepartmentID;
				CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer")); 

				if( pWnd != NULL )
				{
					ShowWindow(FALSE);

					// 2015-11-23 kh.choi 기존 ONE_DIALOG 생성하던 것을 PRECHECK_DIALOG 생성하도록 변경
					//pWnd->SendMessage(MESSAGE_ONE_DIALOG, 0,0);
					theApp.m_bJustAfterSelectPath = FALSE;
					pWnd->SendMessage(MESSAGE_ONE_DIALOG, 0, 0);
				}
			} else {
				theApp.m_pMemberData->ResetAgentInformation();
				//m_cbDepartment.ResetContent();
				MessageBox(_T("db setting fail. 다시 시도해주세요."), _T("TiorSaver"), MB_OK);
			}

			break;
		}
	}
	m_bButtonRegister = FALSE;
}


void CRegisterDlg::OnCbenInsertitemComboboxexDepartment(NMHDR *pNMHDR, LRESULT *pResult)
{
	PNMCOMBOBOXEX pCBEx = reinterpret_cast<PNMCOMBOBOXEX>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	UpdateData(FALSE);

	*pResult = 0;
}

void CRegisterDlg::OnOK()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	//CDialog::OnOK();
}

BOOL CRegisterDlg::PreTranslateMessage(MSG* pMsg)
{
	// ESC 키
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE))
	{
		return true;
	}

	// 엔터키
	if((pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN))
	{
		return true;
	}
	return CDialog::PreTranslateMessage(pMsg);
}
void CRegisterDlg::OnBnClickedButtonPreview()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strCheckPath =  CPathInfo::GetClientInstallPath()+ _T("register.success");
	if(FALSE == FileExists(strCheckPath))
	{
		theApp.DeleteRequest();
		if (theApp.m_pMemberData) {
			theApp.m_pMemberData->ResetAgentInformation();
		}
	}
	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

	if( pWnd )
	{
		ShowWindow(FALSE);

		pWnd->SendMessage(MESSAGE_REGISTER_PREV, 0,0);
	}
}

HBRUSH CRegisterDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	UINT nID = pWnd->GetDlgCtrlID();

	if(IDC_EDIT_AGENT_NAME == nID || IDC_COMBOBOX_DEPARTMENT)
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(50, 51, 53));
		hbr = CreateSolidBrush(RGB(255, 255, 255)); //255,255,255s
	}

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}
void CRegisterDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialog::OnMouseMove(nFlags, point);
}

void CRegisterDlg::OnBnClickedButtonMin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}
void CRegisterDlg::OnBnClickedButtonClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	//CCmsUninstall uninstall;
	//uninstall.SetupExit();

	::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
}
LRESULT CRegisterDlg::OnNcHitTest(CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
	{
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		pDlg->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));  
	}
	return CDialog::OnNcHitTest(point);
}
