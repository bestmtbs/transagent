
// TiorSaverSetupDlg.h : 헤더 파일
//

#pragma once
#include "afxcmn.h"

#include "CmsAgreeDlg.h"
#include "CmsOneLevelDlg.h"
#include "RegisterDlg.h"
#include "VerifyDlg.h"
#include "SelectPath.h"
#include "CmsCompleteDlg.h"
#include "Control/Button/ImageButton.h"
#include "afxwin.h"

// CTiorSaverSetupDlg 대화 상자
class CTiorSaverSetupDlg : public CDialog
{
// 생성입니다.
public:
	CTiorSaverSetupDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	~CTiorSaverSetupDlg();
	void CreateDlg(int iType);
	void AllWindowHide();

	CSelectPath *m_pSelectPathDlg;	// 2015-11-24 kh.choi 설치 전 점검 다이얼로그 추가
	CCmsAgreeDlg *m_pAgreeDlg;
	CCmsOneLevelDlg *m_pOneDlg;
	CRegisterDlg *m_pTwoDlg;
	CVerifyDlg *m_pVerifyDlg;
	CCmsCompleteDlg*	m_pCompleteDlg;

	void Close();
	//BOOL MySystemReboot();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_TIORSAVERSETUP_DIALOG };

	CBitmap m_hBitBack;
	//CBitmap m_hBitWelcomTxt;
	//CBitmap m_hBitLogo;

	CBrush			m_brush;
	CStatic			m_Static_Main;
	CStatic			m_Static_Install_Step1_Guide;
	CFont			m_titleFont;
	CFont			m_GuideFont;

	CImageButton		m_btInstallCancle;
	CImageButton		m_btInstallNext;
	//CImageButton		m_btnClose;
	//CImageButton		m_btnMin;

	void ReLoad();



	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//afx_msg void OnAgreeDlg();
	afx_msg void OnSelectPathDlg();
	afx_msg void OnOneLevelDlg();
	afx_msg void OnRegisterLevelDlg();
	//afx_msg void OnThreeLevelDlg();
	afx_msg void OnVerifyLevelDlg();
	afx_msg void OnSetupSuccess();
	afx_msg void OnSetupCancle();
	afx_msg void OnAgreePrev();
	afx_msg void OnOnePrev();
	afx_msg void OnSelectPathPrev();
	afx_msg void OnRegisterPrev();
	afx_msg void OnExit();
	afx_msg void OnMinimum();
	afx_msg void OnHide();
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedButtonSetupSuccess();
//	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	//afx_msg void OnBnClickedButtonSetupReboot();
	//afx_msg void OnBnClickedButtonSetupRebootCancle();
	afx_msg void OnBnClickedButtonSetupCancel();
	afx_msg void OnStnClickedStaticInstallStepGuide();
	afx_msg void OnBnClickedButtonSetupInstallCalcel();
	afx_msg void OnBnClickedButtonSetupInstallNext();

private:
	void ComponentLineUp();
	virtual BOOL PreTranslateMessage(MSG * pMsg);
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg LRESULT OnNcHitTest(CPoint point);
	//CComboBox m_cb_Lang;
	//CStatic m_ST_Lang;
	//afx_msg void OnCbnSelchangeCbLang();
	afx_msg void OnBnClickedButtonMin();
};
