// CmsOneLevelDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "CmsOneLevelDlg.h"
#include "TiorSaverSetupInfo.h"
//#include "CmsUninstall.h"
#include "Impersonator.h"
#include "UtilsFile.h"
#include "PathInfo.h"
#include "WinOsVersion.h"
#include "UtilsProcess.h"
#include "UtilParse.h"
#include "UtilsUnicode.h"
#include "Crypto/Base64.h"
#include "RegistryQueryForAppList.h"
#include "CmsDBManager.h"

#include "ConnectParamInfo.h"
//#include "CmsUserAuth.h"
//#include "HttpsTConnect.h"
#include "ParamParser.h"
#include <MMSystem.h>

//#include "CmsDBManager.h"
// CCmsOneLevelDlg 대화 상자입니다.

#define TIMER_PREV_PAGE		0
#define TIMER_WINPCAP_INSATLL_CHECK 1
#define TIMER_WINPCAP_INSATLL_COMPLETE 2
#define WINPCAP_INSATLL_CHECK_RETRY_TIME	1000 * 1
#define WINPCAP_PROC_NAME _T("WinPcap 4.1.3")

//UINT	UnInstallThreadProc(void *lParam);		

IMPLEMENT_DYNAMIC(CCmsOneLevelDlg, CDialog)

CCmsOneLevelDlg::CCmsOneLevelDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCmsOneLevelDlg::IDD, pParent)
	,m_btInstallPrev(IDB_BMP_PREV_N,IDB_BMP_PREV_N, IDB_BMP_PREV_O, 0, 0, _T(""),0)
	,m_btInstallNext(IDB_BMP_NEXT_N,IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, IDB_BMP_NEXT_N, _T(""),0)
	,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O,  0, 0, _T(""),0)
	,m_btnMin(IDB_BMP_MIN_N, IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, 0, _T(""), 0)
	,m_btnSetupCancel(IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_O, 0, 0, _T(""), 0)
	
{
	//m_bSetupThreadRunning = FALSE;
	m_bSetupThreadStop = FALSE;
	m_bStartUninst = FALSE;
	m_bAgentAlreadyExist = FALSE;
	m_hWinPcapHandle = INVALID_HANDLE_VALUE;
	m_hwndWinPcapHWND = NULL;
}

CCmsOneLevelDlg::~CCmsOneLevelDlg()
{

}

void CCmsOneLevelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS_ONE, m_Progress);
	DDX_Control(pDX, IDC_LIST_RESULT, m_ListBox);
	DDX_Control(pDX, IDC_BUTTON_PREV, m_btInstallPrev);
	DDX_Control(pDX, IDC_BUTTON_NEXT, m_btInstallNext);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_btnClose);
	DDX_Control(pDX, IDC_STATIC_TEXT, m_StaticInstall);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btnSetupCancel);
}


BEGIN_MESSAGE_MAP(CCmsOneLevelDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_NEXT, &CCmsOneLevelDlg::OnBnClickedButtonNext)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_PREV, &CCmsOneLevelDlg::OnBnClickedButtonPrev)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CCmsOneLevelDlg::OnBnClickedButtonExit)
	ON_WM_PAINT()
	ON_WM_NCHITTEST()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CCmsOneLevelDlg::OnBnClickedBtnMin)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CCmsOneLevelDlg::OnBnClickedButtonCancel)
	ON_WM_MOUSEMOVE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

ULONG ProcIDFromWnd(HWND hwnd) // 윈도우 핸들로 프로세스 아이디 얻기
{
	ULONG idProc;
	GetWindowThreadProcessId( hwnd, &idProc );
	return idProc;
}

HWND GetWinHandle(ULONG pid) // 프로세스 아이디로 윈도우 핸들 얻기
{
	HWND tempHwnd = FindWindow(NULL,NULL); // 최상위 윈도우 핸들 찾기

	while( tempHwnd != NULL )
	{
		if( GetParent(tempHwnd) == NULL ) // 최상위 핸들인지 체크, 버튼 등도 핸들을 가질 수 있으므로 무시하기 위해
			if( pid == ProcIDFromWnd(tempHwnd) )
				return tempHwnd;
		tempHwnd = GetWindow(tempHwnd, GW_HWNDNEXT); // 다음 윈도우 핸들 찾기
	}
	return NULL;
}

// CCmsOneLevelDlg 메시지 처리기입니다.
BOOL CCmsOneLevelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ComponentLineUp();

	if (theApp.m_bJustAfterSelectPath) {
		Set_InfoText(_T("설치 파일 복사중..."));
		SetTimer(TIMER_WINPCAP_INSATLL_CHECK, WINPCAP_INSATLL_CHECK_RETRY_TIME, NULL);
	}
	else {
		Install(); //hhh:test
	}


	
//	HFONT  hfont;
//	hfont = CreateFont(12,0, 0, 0,FW_BOLD, FALSE, FALSE, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
//		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, _T("돋움"));
	
//	m_StaticInstall.SendMessage(WM_SETFONT, (WPARAM)hfont, (LPARAM)TRUE);
//	m_StaticInstall.SetBkColor((COLORREF)RGB(45,48,59));
//	m_StaticInstall.SetTextColor((COLORREF)RGB(255,255,255));


	ReLoad();

	return FALSE;
}

void CCmsOneLevelDlg::ReLoad()
{

	//GetDlgItem(IDC_STATIC_OFFICE_TXT_M)->SetWindowText(_T("TiorSaver-Installing"));


	m_btInstallPrev.ReLoadImage(theApp.GetRcPath(_T("0_btn_prev_n.bmp")), theApp.GetRcPath(_T("0_btn_prev_n.bmp")), theApp.GetRcPath(_T("0_btn_prev_o.bmp")), _T(""), theApp.GetRcPath(_T("0_btn_prev_n.bmp")));
	m_btInstallNext.ReLoadImage(theApp.GetRcPath(_T("0_btn_next_n.bmp")), theApp.GetRcPath(_T("0_btn_next_n.bmp")), theApp.GetRcPath(_T("0_btn_next_o.bmp")), _T(""), theApp.GetRcPath(_T("0_btn_next_n.bmp")));
	m_btnSetupCancel.ReLoadImage(theApp.GetRcPath(_T("0_btn_setup_cancel_n.bmp")), theApp.GetRcPath(_T("0_btn_setup_cancel_n.bmp")), theApp.GetRcPath(_T("0_btn_setup_cancel_o.bmp")), theApp.GetRcPath(_T("0_btn_setup_cancel_n.bmp")), theApp.GetRcPath(_T("0_btn_setup_cancel_n.bmp")));

	m_btInstallPrev.SizeToContent();
	m_btInstallNext.SizeToContent();
	m_btnSetupCancel.SizeToContent();


//	m_btInstallPrev(IDB_BMP_PREV_N,IDB_BMP_PREV_N, IDB_BMP_PREV_O, 0, 0, _T(""),0)
//	m_btInstallNext(IDB_BMP_NEXT_N,IDB_BMP_NEXT_N, IDB_BMP_NEXT_O, 0, IDB_BMP_NEXT_N, _T(""),0)

//	m_btnSetupCancel(IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_N, IDB_BMP_SETUP_CANCEL_O, 0, 0, _T(""), 0)
//
}



void CCmsOneLevelDlg::ComponentLineUp()
{
	BITMAP bm;
	if (theApp.m_bJustAfterSelectPath)
		m_hBitBack.LoadBitmap(IDB_BITMAP_BKG_PROGRESS);
	else
		m_hBitBack.LoadBitmap(IDB_BITMAP_BKG_PROGRESS_2);
	m_hBitBack.GetBitmap(&bm);
	this->MoveWindow(0,0, bm.bmWidth, bm.bmHeight);

	m_Progress.SetPercent(FALSE, RGB(84, 86, 98));
	m_Progress.SetColors(RGB(213,213,213),RGB(68, 122, 183), RGB(239, 240, 241));
	CString strTiorsaverInstaller = _T("Tiorsaver Installer");
	m_Progress.SetFile((TCHAR*)strTiorsaverInstaller.GetString());
	m_Progress.SetFLen(100);
	m_Progress.SetRange(0, 100);
	m_Progress.StepIt(0);
	m_Progress.MoveWindow(30, 115, 520, 14);

	m_btInstallPrev.SizeToContent();
	m_btInstallNext.SizeToContent();
	m_btnClose.SizeToContent();
	m_btnMin.SizeToContent();
	m_btnSetupCancel.SizeToContent();
	
	
	CFont fnt;
	LOGFONT lf;

	::ZeroMemory(&lf, sizeof(lf) );
	lf.lfHeight = 15;
	wcscpy(lf.lfFaceName, _T("SkiCargo") );
	wcscpy_s(lf.lfFaceName, theApp.m_ttfNotoSansRegular.GetFontFamilyName().c_str());
	fnt.CreateFontIndirect(&lf);
	GetDlgItem(IDC_STATIC_TEXT)->SetFont(&fnt);
	//GetDlgItem(IDC_STATIC_PERCENT)->SetFont(&fnt);
	fnt.Detach();
	GetDlgItem(IDC_STATIC_TEXT)->MoveWindow(30, 95, 450, 30);
	//GetDlgItem(IDC_STATIC_PERCENT)->MoveWindow(420, 123, 30, 13);
	
	m_btInstallPrev.MoveWindow(385, 327, m_btInstallPrev.GetWidth(),   m_btInstallPrev.GetHeight());
	m_btInstallNext.MoveWindow(485, 327, m_btInstallNext.GetWidth(),   m_btInstallNext.GetHeight());
	m_btnClose.MoveWindow(550, 4, m_btnClose.GetWidth(),   m_btnClose.GetHeight());
	m_btnMin.MoveWindow(520, 4, m_btnMin.GetWidth(),   m_btnMin.GetHeight());
	m_btnSetupCancel.MoveWindow(485, 327, m_btnSetupCancel.GetWidth(),   m_btnSetupCancel.GetHeight());
	GetDlgItem(IDC_BUTTON_PREV)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BUTTON_NEXT)->ShowWindow(SW_HIDE); 
//	GetDlgItem(IDC_BUTTON_NEXT)->ShowWindow(SW_SHOW);  //test : 지울것!

	GetDlgItem(IDC_PROGRESS_ONE)->SetFocus();
}

void CCmsOneLevelDlg::Install()
{

	DWORD dwUIThreadID = 0;
//	HANDLE hThread;
		m_hThread= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)SetupThread, this, 0, &dwUIThreadID);
}



HBRUSH CCmsOneLevelDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(56, 56, 56));
		return (HBRUSH)::GetStockObject(NULL_BRUSH);	
	}
	
	return hbr;

}


void CCmsOneLevelDlg::OnBnClickedButtonNext()
{
	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));
	if (theApp.m_bJustAfterSelectPath) {
		if( pWnd )
		{
			ShowWindow(FALSE);
			pWnd->SendMessage(MESSAGE_VERIFY_DIALOG, 0,0);
		}
	} else {
		if( pWnd )
		{
			ShowWindow(FALSE);
			pWnd->SendMessage(MESSAGE_COMPLETE, 0,0);
		}
	}
}

bool CCmsOneLevelDlg::IsOsCheck()
{
	bool bResult = false;

	Set_InfoText(_T("Checking whether the OS can be installed "));	
	bResult = CTiorSaverSetupInfo::IsOSCheck();
	
	if( bResult == true )
	{	
		Set_InfoText(_T("Tiorsaver agent can be installed in this OS....(1/8)"));
	}
	else
	{
		
		Set_InfoText(_T("Tiorsaver installation is impossible in this OS"));
		return false;
	}
	return true;
	
}

bool CCmsOneLevelDlg::IsFirewallException()
{
	Set_InfoText(_T("A Tiorsaver's exemption process is being registered"));
	CTiorSaverSetupInfo::FirewallException() ; //test	
	Set_InfoText(_T("Exemption processing in the Tiorsaver module is proceeding normally....(3/8)"));
	return true;
}

bool CCmsOneLevelDlg::IsCreateRegistry()
{
	//SetDlgItemText(IDC_STATIC_TEXT, _T("레지스트리 생성 작업이 진행중입니다"));
	Set_InfoText(_T("Registry creation is in progress")); 


	//hThread= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)StartProgressThread, this, 0, &dwUIThreadID);

 	CTiorSaverSetupInfo::CreateRegistry();

	//SetDlgItemText(IDC_STATIC_TEXT, _T("레지스트리가 정상적으로 등록되었습니다....(4/7)"));
	Set_InfoText(_T("Registry has been registered properly....(5/8)"));
     return true;
}


bool CCmsOneLevelDlg::IsCreateDB()
{
	m_bEnd  = FALSE;

	BOOL bResult = FALSE;
	
	Set_InfoText(_T("Local database creation is in progress"));
	DWORD dwUIThreadID = 0;
	HANDLE hThread;

	hThread= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)StartProgressThread, this, 0, &dwUIThreadID);


	bResult = CTiorSaverSetupInfo::CreateDB();

	m_bEnd = TRUE;
	if( TRUE == bResult )
	{
		Set_InfoText(_T("Local database has been created successfully."));

	}
	else
	{
		Set_InfoText(_T("Local database creation failed"));
		return false;
	}

	return true;
}

CString    CCmsOneLevelDlg::Base64Enc(CString _strData)
{


	char	szAnsiBuff[300]={0,};
	char	TestBuffer2[300]={0,};
	char	TestBuffer[300]={0,};

	CBase64 base;
	wsprintfA(szAnsiBuff, "%S", _strData);
	CString strTestRet = base.base64encode(szAnsiBuff, _strData.GetLength());
	
	return strTestRet;
}


BOOL CCmsOneLevelDlg::Install_Vcredist()
{
	BOOL bRet = FALSE;
	
	Set_InfoText(_T("Installed file configuration is in progress"));

	if( CTiorSaverSetupInfo::Install_RedistributablePackage() )
	{
		Set_InfoText(_T("Installed file configuration has been completed.....(7/8)"));
		bRet =  TRUE;
	}
	else{
	
		Set_InfoText(_T("Installed file configuration has failed"));
	}	
	
	return bRet;

}
bool CCmsOneLevelDlg::IsCurrentFile()
{
	bool bResult = false;	
	//Set_InfoText(_T("최신파일로 업데이트 중입니다"));
	Set_InfoText(_T("Installing a file"));
	Sleep(1000);

    CUpgrade cUpdate;

	cUpdate.SetReport(this);
	//bResult = cUpdate.Mode_Update();

	bResult = cUpdate.Start_Setup();

	if( bResult == 0 )
	{		
		Set_InfoText(_T("Completed file installation ....(7/8)"));
		cUpdate.Start_Change(_T("0.0.1"));
	
	}
	return bResult;

}



//설치중 실패가 나면'이전', '다음' 버튼을 숨기고 종료버튼을 보이게 함
void CCmsOneLevelDlg::FailBtnSet()
{
	//GetDlgItem(IDC_BUTTON_PREV)->ShowWindow(SW_SHOW);
	GetDlgItem(IDC_BUTTON_CANCEL)->ShowWindow(SW_SHOW);
	m_bStartUninst = TRUE;
}

BOOL WINAPI   StartProgressThread(LPVOID lpParam)
{
	CCmsOneLevelDlg* pWnd = reinterpret_cast<CCmsOneLevelDlg*>(lpParam);

	while(pWnd->m_bEnd == FALSE)
	{
		pWnd->m_Progress.SetRange32(0,100);
		pWnd->m_Progress.SetPos(0);
		pWnd->m_Progress.SetPos(100);

		Sleep(1000);

		pWnd->m_Progress.Invalidate();
	}

	return TRUE;
}


bool CCmsOneLevelDlg::IsService()
{
	bool bResult = false;
	m_bEnd  = FALSE;
	Set_InfoText(_T("Service registration is in progress"));

	DWORD dwUIThreadID = 0;

	bResult = CTiorSaverSetupInfo::InstallServices();

	//if(true==bResult)
		//CTiorSaverSetupInfo::StartAgent();

	m_bEnd  = TRUE;

	if( bResult == true )
	{
		Set_InfoText(_T("Service registration has been successfully done....(8/8)"));
	}
	else
	{
		Set_InfoText(_T("Failed to register the service"));
	}
	return bResult;
}


BOOL CCmsOneLevelDlg::CheckThreadStopFlag()
{
	if(m_bSetupThreadStop)
	{
		m_bStartUninst = TRUE;
		return TRUE;
	}

	return FALSE;
}

BOOL WINAPI ExtractThread(LPVOID lpParam)
{
	//CWnd* pWnd;
	CProgressEx * pCp = NULL;
	CCmsOneLevelDlg* pOneLevelDlg = reinterpret_cast<CCmsOneLevelDlg*>(lpParam);
	{
		pOneLevelDlg->SetProgressPos(10);
		pOneLevelDlg->SetProgressPos(20);
		Sleep(1000);

		CString strClientPath = CPathInfo::GetClientInstallPath();

		DWORD err ;
		ForceCreateDir(strClientPath, &err);
		pOneLevelDlg->SetProgressPos(30);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		//DropFile(IDR_TXT_XML,strClientPath+_T("OfsLang.xml"),_T("txt"));	
		DropFile(IDR_EXE_7Z,strClientPath+_T("7z.exe") ,_T("EXE"));
		pOneLevelDlg->SetProgressPos(40);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		DropFile(IDR_DLL_7Z,strClientPath+_T("7z.dll") ,_T("DLL"));
		pOneLevelDlg->SetProgressPos(50);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		CTiorSaverSetupInfo::FileExtract();
		pOneLevelDlg->SetProgressPos(60);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		CString strCheckFirstUser = _T("");
		strCheckFirstUser.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("register.success"));
		if(FileExists(strCheckFirstUser))
		{
			pOneLevelDlg->m_bAgentAlreadyExist = TRUE;
			pOneLevelDlg->Set_InfoText(_T("It is already installed on this PC. Installation will be stopped"));
			pOneLevelDlg->FailBtnSet();
			return 0;
		}
		pOneLevelDlg->SetProgressPos(70);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		if (!pOneLevelDlg->IsCreateDB()) {
			pOneLevelDlg->FailBtnSet();
			return 0;
		}
		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		pOneLevelDlg->SetProgressPos(90);
		pOneLevelDlg->Set_InfoText(_T("설치파일 복사 완료"));
		pOneLevelDlg->SetProgressPos(100);
	}
	Sleep(1000);

	pOneLevelDlg->GetDlgItem(IDC_BUTTON_NEXT)->ShowWindow(SW_SHOW);
	pOneLevelDlg->GetDlgItem(IDC_BUTTON_PREV)->ShowWindow(SW_SHOW);

	return TRUE;
}

BOOL WINAPI SetupThread(LPVOID lpParam)
{
	//CWnd* pWnd;
	CString strCountFile = _T("");
	strCountFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("reboot.require"));
	if (FileExists(strCountFile)) {
		ForceDeleteFile(strCountFile);
	}
	CProgressEx * pCp = NULL;
	CCmsOneLevelDlg* pOneLevelDlg = reinterpret_cast<CCmsOneLevelDlg*>(lpParam);
	
	{
	pOneLevelDlg->m_Progress.SetRange32(0,100);
	if( pOneLevelDlg->IsOsCheck() == false )
		{
			pOneLevelDlg->Set_InfoText(_T("This OS is not supported. Installation will be stopped"));
			pOneLevelDlg->FailBtnSet(); 
			return 0;
		}
		

		pOneLevelDlg->SetProgressPos(10);
		Sleep(1000);	
	

		//pOneLevelDlg->IsFileExtract();  //test

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		pOneLevelDlg->SetProgressPos(20);
		pOneLevelDlg->IsFirewallException();

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;
		
		pOneLevelDlg->SetProgressPos(30);
		Sleep(1000);
		pOneLevelDlg->SetProgressPos(40);
		
		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		if( pOneLevelDlg->IsCreateRegistry() == false ) 
		{
			pOneLevelDlg->Set_InfoText(_T("Failed to create a registry. Installation will be stopped."));
			pOneLevelDlg->FailBtnSet();
 			return 0;
 		}

		Sleep(1000);


		pOneLevelDlg->SetProgressPos(50);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

		Sleep(1000);
		 if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			 return 0;
		
		 Sleep(1000);	


		 if (theApp.WriteVersionFile()) {
			 if (pOneLevelDlg->IsCurrentFile()) {
				 pOneLevelDlg->Set_InfoText(_T("Failed to download files. Installation will be stopped."));
				 pOneLevelDlg->FailBtnSet();
				 return 0;
			 }
		 } else {
			 pOneLevelDlg->Set_InfoText(_T("Failed to create the version. Installation will be stopped."));
			 pOneLevelDlg->FailBtnSet();
			 return 0;
		 }
		 if (pOneLevelDlg->CheckThreadStopFlag())  //---------------ThreadStop Check
			 return 0;
		 pOneLevelDlg->SetProgressPos(60);
		pOneLevelDlg->IsFirewallException();

		if (pOneLevelDlg->CheckThreadStopFlag())  //---------------ThreadStop Check
			return 0;
		pOneLevelDlg->SetProgressPos(70);
		/////////////////////////////////////에이전트 실행시켜준다////////////////////////////
		if (pOneLevelDlg->CheckThreadStopFlag())  //---------------ThreadStop Check
			return 0;

		Sleep(1000);

		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;



		pOneLevelDlg->SetProgressPos(80);
		if (pOneLevelDlg->CheckThreadStopFlag())  //---------------ThreadStop Check
			return 0;
		//서비스를 등록한다.
		CString strCount = _T(" ");
		DWORD dwBytesWritten = 0;
		UM_WRITE_LOG(strCount);
		WriteFileExample(strCountFile, strCount, dwBytesWritten);
		if( pOneLevelDlg->IsService() == false)
		{
			pOneLevelDlg->FailBtnSet();
			return 0;
		}
		if (pOneLevelDlg->CheckThreadStopFlag())  //---------------ThreadStop Check
			return 0;
		pOneLevelDlg->SetProgressPos(90);
		if (!pOneLevelDlg->Register()) {
			pOneLevelDlg->Set_InfoText(_T("이미 등록된 사용자입니다. 설치를 종료합니다."));
			pOneLevelDlg->FailBtnSet();
			return 0;
		}
		//else {
		//	theApp.StartTray();
		//}
		pOneLevelDlg->SetProgressPos(100);
		if( pOneLevelDlg->CheckThreadStopFlag() )  //---------------ThreadStop Check
			return 0;

	}
	Sleep(1000);
	return TRUE;
}

void CCmsOneLevelDlg::SetProgressPos(int _iPos)
{
	CString strPercent;
	strPercent.Format( _T("%d%%"), _iPos );
	m_Progress.StepIt(_iPos);
	CRect r;
	//GetDlgItem(IDC_STATIC_PERCENT)->SetWindowText(strPercent);
	//tDlgItem(IDC_STATIC_PERCENT)->GetWindowRect(&r);
	ScreenToClient(&r);
	RedrawWindow(&r ,NULL,RDW_INVALIDATE | RDW_UPDATENOW | RDW_NOERASE);
}

void CCmsOneLevelDlg::Set_InfoText(CString _strTxt)
{
	CRect r;
	GetDlgItem(IDC_STATIC_TEXT)->SetWindowText(_strTxt);
	GetDlgItem(IDC_STATIC_TEXT)->GetWindowRect(&r);
	ScreenToClient(&r);
	RedrawWindow(&r ,NULL,RDW_INVALIDATE | RDW_UPDATENOW | RDW_NOERASE);
}

//설치취소 버튼 이벤트임
void CCmsOneLevelDlg::OnBnClickedButtonPrev()
{
	CWnd *pMainWnd = FindWindow(NULL, _T("Tiorsaver Installer"));

	GetDlgItem(IDC_BUTTON_NEXT)->ShowWindow(SW_HIDE);
	GetDlgItem(IDC_BUTTON_PREV)->ShowWindow(SW_HIDE);

	theApp.DeleteRequest();
	if (theApp.m_pMemberData) {
		theApp.m_pMemberData->ResetAgentInformation();
	}
	if (!theApp.m_bJustAfterSelectPath)
		CTiorSaverSetupInfo::DeleteFirewallPolicy();
	this->SetProgressPos(0);
	this->ShowWindow(FALSE);

	CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));
	if( pWnd )
	{
		ShowWindow(FALSE);

		pWnd->SendMessage(MESSAGE_REGISTER_PREV, 0,0);
	}
}

void CCmsOneLevelDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rc;
	GetClientRect(&rc);
	BITMAP bm;
	CDC memDC;
	
	dc.FillSolidRect(rc, RGB(255,255,255));
	// client
	CBitmap * bitmap = NULL;
	m_hBitBack.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_hBitBack);
	dc.BitBlt(0, 0, bm.bmWidth , bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
}

void CCmsOneLevelDlg::OnBnClickedButtonExit()
{
	int ret = MessageBox(_T("Are you sure you want to cancel the installation?"), _T("Tiorsaver Installer"), MB_YESNO);
	if(IDYES == ret) //설치 취소시
	{
		m_bSetupThreadStop = TRUE;
		CDialog *pDlg = reinterpret_cast<CDialog *>( GetParent() );
		::SendMessage(pDlg->m_hWnd, MESSAGE_HIDE, 0, 0);

		if (100 > m_Progress.GetPos()) {
			int	iLimitCnt= 0;
			while(TRUE)
			{
				if(iLimitCnt > 8) //8초 이상동안 안되면 쓰레드 강제 종료 
				{
					TerminateThread(m_hThread, 0);
					break;
				}

				if(m_bStartUninst)
					break;
			
				Sleep(1000);
				iLimitCnt++;

			}
		}
		::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
	}
}

LRESULT CCmsOneLevelDlg::OnNcHitTest(CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
	{
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		pDlg->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));  
	}

	return CDialog::OnNcHitTest(point);
}

BOOL CCmsOneLevelDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

void CCmsOneLevelDlg::OnBnClickedBtnMin()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}

void CCmsOneLevelDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case TIMER_PREV_PAGE:
		{
			KillTimer(TIMER_PREV_PAGE);
			CWnd *pMainWnd = FindWindow(NULL, _T("Tiorsaver Installer"));
			if(pMainWnd)
			{
				ShowWindow(FALSE);
				pMainWnd->SendMessage(MESSAGE_ONE_PREV, 0,0);
			}

			break;
		}

	case TIMER_WINPCAP_INSATLL_CHECK:
		{
			KillTimer(TIMER_WINPCAP_INSATLL_CHECK);
			IsWinPcapThere();
			break;
		}
	case TIMER_WINPCAP_INSATLL_COMPLETE:
		{
			KillTimer(TIMER_WINPCAP_INSATLL_COMPLETE);
			ExtractAndCopyInstallFiles();
			break;
		}
	}
	CDialog::OnTimer(nIDEvent);
}

void CCmsOneLevelDlg::OnBnClickedButtonCancel()
{
	int ret = MessageBox(_T("Are you sure you want to cancel the installation?"), _T("Tiorsaver Installer"), MB_YESNO);
	if (IDYES == ret) //설치 취소시
	{
		m_bSetupThreadStop = TRUE;
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		::SendMessage(pDlg->m_hWnd, MESSAGE_HIDE, 0, 0);

		if (100 > m_Progress.GetPos()) {
			int	iLimitCnt = 0;
			while (TRUE)
			{
				if (iLimitCnt > 8) //8초 이상동안 안되면 쓰레드 강제 종료 
				{
					TerminateThread(m_hThread, 0);
					break;
				}

				if (m_bStartUninst)
					break;

				Sleep(1000);
				iLimitCnt++;

			}
		}
		::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
	}
}


BOOL CCmsOneLevelDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE )
		{
			//엔터를 Dialog로 넘겨주지 않음
			return FALSE;
		}

	}
	return CDialog::PreTranslateMessage(pMsg);
}


void CCmsOneLevelDlg::Report(int nTotal, int nCurrent, CString _strReport)
{

		CString strDbg;
		if(0< nTotal){

			int nCab = nTotal/20;

			if(nCab<1)
				nCab = 1;

			int nPos = 70+nCurrent/nCab;

			//if(90<nPos)
			//	nPos = nPos;

			if(90<nPos)		//hhh: 90%를 넘어가는 버그로 인해 90을 넘는 경우 90으로 고정
				nPos = 90;
				
		
			SetProgressPos(nPos);
			strDbg.Format(_T("Installing a file.(%d/%d) : %s") ,nCurrent,nTotal,_strReport);
		}
		else
			strDbg=_strReport;

		Set_InfoText(strDbg);
}
void CCmsOneLevelDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CCmsOneLevelDlg::Register()
{
	BOOL bResult = FALSE;
	int nErr = 0;
	CString strToken = _T("");
	CString strReturn = _T("");
	CString strParameter = _T("");
	CString strUrl = _T("");
	if (CheckThreadStopFlag())  //---------------ThreadStop Check
		return FALSE;
	strUrl.Format(_T("%s/v1/agent/register"), theApp.m_pMemberData->GetServerHostURL());
	CParamParser parser;
	CRegistryQueryForAppList appList;
	if (CheckThreadStopFlag())  //---------------ThreadStop Check
		return FALSE;
	CString strAppList = appList.GetInstallAppList();
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strAppList.GetString(), (int)strAppList.GetLength(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed + 1, 0);
	WideCharToMultiByte(CP_UTF8, 0, strAppList.GetString(), (int)strAppList.GetLength(), &strTo[0], size_needed, NULL, NULL);
	CStringA strFullDataA = strTo.c_str();
	CBase64 base64;
	CString strEncodedFileData = base64.base64encode(strFullDataA, size_needed);
	CString strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
	if (CheckThreadStopFlag())  //---------------ThreadStop Check
		return FALSE;
	strParameter = parser.CreateAgentRegisterRequest(theApp.m_nDepartmentID, theApp.m_pMemberData->GetAgentName(), strUrlEncodedData);
	if (theApp) {
		strReturn = theApp.m_curl.CurlPost(strParameter, strUrl, theApp.GetToken());
	}
	if (CheckThreadStopFlag())  //---------------ThreadStop Check
		return FALSE;
	//theApp.m_pMemberData->SetDepartmentID(theApp.m_arrDepartment.GetAt(i)->nDepartmentID);

	if (parser.ParserOnlyToken(strReturn, strToken)) {
		theApp.SetToken(strToken);
		//CString strCountFile = _T("");
		//CString strCount = _T("");
		//DWORD dwBytesWritten = 0;
		//strCountFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("register.success"));
		//UM_WRITE_LOG(strCount);
		//if (WriteFileExample(strCountFile, strCount, dwBytesWritten)) {
		//MessageBox(_T("등록 성공!"), _T("TiorSaver"), MB_OK);
		CString strQuery = _T("");
		strQuery.Format(_T("update %s set init_screen_idx = %d where target_id = \"%s\""), DB_STATE_POLICY, theApp.m_nScreenIdx, theApp.m_pMemberData->GetDeviceUUID());
		CCmsDBManager dbManage(DB_STATE_POLICY);
		dbManage.UpdateQuery(strQuery);
		dbManage.Free();

		if (CheckThreadStopFlag())  //---------------ThreadStop Check
			return FALSE;
		CString strCountFile = _T("");
		CString strCount = _T("");
		DWORD dwBytesWritten = 0;
		strCountFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("register.success"));
		UM_WRITE_LOG(strCount);
		if (WriteFileExample(strCountFile, strCount, dwBytesWritten))
			bResult = TRUE;
		CWnd *pWnd = FindWindow(NULL, _T("Tiorsaver Installer"));
		if (pWnd)
		{
			ShowWindow(FALSE);
			pWnd->SendMessage(MESSAGE_COMPLETE, 0, 0);
		}
	} else {
		if (strToken.IsEmpty()) {
			if (0 == nErr) {
				strToken.Format(_T("%s"), strReturn);
			} else {
				strToken.Format(_T("서버 요청 에러 %d"), nErr);
			}
			if (CheckThreadStopFlag())  //---------------ThreadStop Check
				return FALSE;
		}
		theApp.m_pMemberData->ResetAgentInformation();
		//m_cbDepartment.ResetContent();
		Set_InfoText(strToken);
		if (CheckThreadStopFlag())  //---------------ThreadStop Check
			return FALSE;
	}
	return bResult;
}

void CCmsOneLevelDlg::BringWindowToFront(HWND parm_dest_wnd)
{ 
	if(NULL != parm_dest_wnd && ::IsWindow(parm_dest_wnd)){ 
		CWnd *p_prev_wnd = CWnd::FromHandle(parm_dest_wnd); 

		if(NULL != p_prev_wnd){ 
			p_prev_wnd->BringWindowToTop(); 

			CWnd *p_child_wnd = p_prev_wnd->GetLastActivePopup(); 
			if(p_prev_wnd->IsIconic() == TRUE) p_prev_wnd->ShowWindow(SW_RESTORE); 
			p_prev_wnd->ShowWindow(SW_SHOWNORMAL); 

			if(p_child_wnd != NULL && p_prev_wnd != p_child_wnd){ 
				p_child_wnd->BringWindowToTop(); 
				p_child_wnd->SetForegroundWindow(); 
			} else p_prev_wnd->SetForegroundWindow(); 
		} 
	} 
} 

BOOL CCmsOneLevelDlg::IsWinPcapThere()
{
	if (INVALID_HANDLE_VALUE != m_hWinPcapHandle && IsWindow(m_hwndWinPcapHWND)) {
		BringWindowToFront(m_hwndWinPcapHWND);
		SetTimer(TIMER_WINPCAP_INSATLL_CHECK, WINPCAP_INSATLL_CHECK_RETRY_TIME, NULL);
		return FALSE;
	}
	BOOL bIsWinPcapThere = FALSE;
	CString strWinPcap = _T("");
	CString strWinPcapInstaller = _T("");
	CRegistryQueryForAppList appList;
	strWinPcap.Format(_T("%s"), appList.GetInstallAppList());
	strWinPcapInstaller.Format(_T("%sWinPcap_4_1_3.exe"), GetCurrentModulePath());
	if (-1 == strWinPcap.Find(WINPCAP_PROC_NAME)) {
		//CImpersonator imp;
		//imp.CreateProcess(strWinPcapInstaller);
		//imp.Free();
		bool result;
		STARTUPINFO si;
		ZeroMemory(&si,sizeof(si));
		si.cb = sizeof(si);
		PROCESS_INFORMATION pi;

		result = CreateProcess(strWinPcapInstaller,NULL,NULL,NULL,TRUE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi); // 프로세스 만들기

		if( result )
		{
			HWND hwnd;
			DWORD startTime = timeGetTime();

			while(true) // CreateProcess 하면 바로 윈도우가 열리는게 아니므로 대기하기 위해
			{
				if( timeGetTime() - startTime > 5000 ) // 무한 대기 방지용. 5초 초과되면 루프 나가야 하므로 판단
					break;
				else
					Sleep(100); 

				hwnd = GetWinHandle(pi.dwProcessId); // 프로세스 아이디로 윈도우 핸들 얻기

				if( hwnd != NULL )
				{
					// 프로세스 아이디와 핸들을 이용
					m_hWinPcapHandle = pi.hProcess; 
					m_hwndWinPcapHWND = hwnd;
					BringWindowToFront(m_hwndWinPcapHWND);
					break;
				}
			}
		}
		//MessageBox(_T("B2Saver를 설치하기 전 WinPcap 설치를 완료하세요."), _T("TiorSaver"), MB_OK);
	} else {
		bIsWinPcapThere = TRUE;
	}
	if (bIsWinPcapThere) {
		SetTimer(TIMER_WINPCAP_INSATLL_COMPLETE, WINPCAP_INSATLL_CHECK_RETRY_TIME, NULL);
	} else {
		SetTimer(TIMER_WINPCAP_INSATLL_CHECK, WINPCAP_INSATLL_CHECK_RETRY_TIME, NULL);
	}
	return bIsWinPcapThere;
}

void CCmsOneLevelDlg::ExtractAndCopyInstallFiles()
{
	DWORD dwUIThreadID = 0;
	m_hThread= CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ExtractThread, this, 0, &dwUIThreadID);
}

void CCmsOneLevelDlg::OnClose()
{
	int ret = MessageBox(_T("Are you sure you want to cancel the installation?"), _T("Tiorsaver Installer"), MB_YESNO);
	if (IDYES == ret) //설치 취소시
	{
		m_bSetupThreadStop = TRUE;
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		::SendMessage(pDlg->m_hWnd, MESSAGE_HIDE, 0, 0);

		if (100 > m_Progress.GetPos()) {
			int	iLimitCnt = 0;
			while (TRUE)
			{
				if (iLimitCnt > 8) //8초 이상동안 안되면 쓰레드 강제 종료 
				{
					TerminateThread(m_hThread, 0);
					break;
				}

				if (m_bStartUninst)
					break;

				Sleep(1000);
				iLimitCnt++;

			}
		}
		::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
	}
	//__super::OnClose();
}
