// CmsCompleteDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "TiorSaverSetup.h"
#include "CmsCompleteDlg.h"
#include "PathInfo.h"


// CCmsCompleteDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CCmsCompleteDlg, CDialog)

CCmsCompleteDlg::CCmsCompleteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCmsCompleteDlg::IDD, pParent)
	, m_btComplete(IDB_BMP_COMPLETE_N, IDB_BMP_COMPLETE_O, IDB_BMP_COMPLETE_O, 0, 0, _T(""), 0)
	,m_btnClose(IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_N, IDB_BMP_CLOSE_O,  0, 0, _T(""),0)
	,m_btnMin(IDB_BMP_MIN_N,IDB_BMP_MIN_N, IDB_BMP_MIN_O, 0, 0, _T(""),0)
{
}

CCmsCompleteDlg::~CCmsCompleteDlg()
{
}

void CCmsCompleteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_BUTTON_CANCEL, m_btInstallCompleteThusCancel);
	DDX_Control(pDX, IDC_BUTTON_COMPLETE, m_btComplete);

	DDX_Control(pDX, IDC_BUTTON_MIN, m_btnMin);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_btnClose);
}


BEGIN_MESSAGE_MAP(CCmsCompleteDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_COMPLETE, &CCmsCompleteDlg::OnBnClickedButtonReboot)
	ON_WM_PAINT()
	ON_WM_NCHITTEST()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CCmsCompleteDlg::OnBnClickedBtnMin)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CCmsCompleteDlg::OnBnClickedBtnClose)
	ON_WM_CTLCOLOR()
	//ON_BN_CLICKED(IDC_BUTTON_CANCEL, &CCmsCompleteDlg::OnBnClickedButtonCancel)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CCmsCompleteDlg 메시지 처리기입니다.

BOOL CCmsCompleteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	ComponentLineUp();

	ReLoad();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//m_btReboot.SizeToContent();
//	m_btNoReboot.SizeToContent();
	
//	m_hBitBack.LoadBitmap(IDB_BITMAP_COMPLETE);
	//m_btNoReboot.ShowWindow(SW_HIDE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CCmsCompleteDlg::ReLoad()
{
	m_btComplete.ReLoadImage(theApp.GetRcPath(_T("0_btn_complete_n.bmp")),theApp.GetRcPath(_T("0_btn_complete_n.bmp")),  theApp.GetRcPath(_T("0_btn_complete_o.bmp")), _T(""), theApp.GetRcPath(_T("0_btn_complete_n.bmp")));
}

void CCmsCompleteDlg::ComponentLineUp()
{
	BITMAP bm;
	m_hBitBack.LoadBitmap(IDB_BITMAP_BKG_COMPLETE);
	m_hBitBack.GetBitmap(&bm);
	this->MoveWindow(0,0, bm.bmWidth, bm.bmHeight);

	m_btComplete.SizeToContent();
	m_btnMin.SizeToContent();
	m_btnClose.SizeToContent();
	m_btComplete.MoveWindow(485, 327, m_btComplete.GetWidth(), m_btComplete.GetHeight());
	m_btnClose.MoveWindow(550, 4, m_btnClose.GetWidth(),   m_btnClose.GetHeight());
	m_btnMin.MoveWindow(520, 4, m_btnMin.GetWidth(),   m_btnMin.GetHeight());
	
}



void CCmsCompleteDlg::OnBnClickedButtonReboot()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	int ret = MessageBox(_T("설치를 완료하였습니다. 윈도우를 다시 시작하셔야 정상적인 사용이 가능합니다. 윈도우를 다시 시작 하시겠습니까?"), _T("Tiorsaver Installer"), MB_YESNO);
	if (IDYES == ret) //설치 취소시
	{
		MySystemReboot();

	}
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	//::SendMessage(pDlg->m_hWnd, MESSAGE_PROCESS_EXIT, 0, 0);
	exit(0);
}

void CCmsCompleteDlg::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	// 그리기 메시지에 대해서는 CDialog::OnPaint()을(를) 호출하지 마십시오.

	CRect rc;
	GetClientRect(&rc);
	BITMAP bm;
	CDC memDC;
	
	dc.FillSolidRect(rc, RGB(255,255,255));
	// client
	CBitmap * bitmap = NULL;
	m_hBitBack.GetBitmap(&bm);
	memDC.CreateCompatibleDC(&dc);
	bitmap = memDC.SelectObject(&m_hBitBack);
	
	dc.BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(bitmap);
	memDC.DeleteDC();
}


LRESULT CCmsCompleteDlg::OnNcHitTest(CPoint point)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UINT	nHitTest = CDialog::OnNcHitTest(point);
	if(nHitTest == HTCLIENT)
	{
		CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
		pDlg->SendMessage(WM_NCLBUTTONDOWN, HTCAPTION, MAKELPARAM(point.x, point.y));  
	}
	return CDialog::OnNcHitTest(point);
}

BOOL CCmsCompleteDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	return TRUE;
	//return CDialog::OnEraseBkgnd(pDC);
}

void CCmsCompleteDlg::OnBnClickedBtnMin()
{
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	::SendMessage(pDlg->m_hWnd, MESSAGE_MINUMUM, 0, 0);
}

HBRUSH CCmsCompleteDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if(nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetBkMode(TRANSPARENT);
		pDC->SetTextColor(RGB(39, 39, 39));
		return (HBRUSH)::GetStockObject(NULL_BRUSH);	
	}

	// TODO:  여기서 DC의 특성을 변경합니다.

	// TODO:  기본값이 적당하지 않으면 다른 브러시를 반환합니다.
	return hbr;
}

//void CCmsCompleteDlg::OnBnClickedButtonCancel()
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	exit(0);
//}

void CCmsCompleteDlg::OnBnClickedBtnClose()
{
	OnBnClickedButtonReboot();
	//exit(0);
}

BOOL CCmsCompleteDlg::MySystemReboot()
{
	HANDLE hToken;
	TOKEN_PRIVILEGES tkp;

	// Get a token for this process. 

	if (!OpenProcessToken(GetCurrentProcess(),
		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		return(FALSE);

	// Get the LUID for the shutdown privilege. 

	LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,
		&tkp.Privileges[0].Luid);

	tkp.PrivilegeCount = 1;  // one privilege to set    
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	// Get the reboot privilege for this process. 


	AdjustTokenPrivileges(hToken, FALSE, &tkp, 0,
		(PTOKEN_PRIVILEGES)NULL, 0);

	if (GetLastError() != ERROR_SUCCESS)
		return FALSE;

	// Reboot the system and force all applications to close. 

	if (!ExitWindowsEx(EWX_REBOOT, 0))
		return FALSE;

	return TRUE;
}

void CCmsCompleteDlg::OnBnClickedButtonNoReboot()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CString strTray = _T("");

	strTray.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("CmsTray.exe"));
	ShellExecute(NULL, _T("open"), strTray, NULL, NULL, SW_SHOW);

	//Sleep(2000);
	CDialog *pDlg = reinterpret_cast<CDialog *>(GetParent());
	exit(0);

}

void CCmsCompleteDlg::OnClose()
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	OnBnClickedButtonReboot();
	CDialog::OnClose();
}
