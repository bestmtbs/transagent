//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TiorSaverSetup.rc
//
#define ID_INSTALL_CANCLE               2
#define ID_INSTALL_CANCEL2              3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TIORSAVERSETUP_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_INFO                 129
#define IDD_DIALOG_AGREE                130
#define IDD_REGISTER_DIALOG             131
#define IDD_DIALOG_ONE_LEVEL            132
#define IDD_VERIFY_DIALOG               133
#define IDD_DIALOG_FOUR_LEVEL           134
#define IDD_DIALOG_NEW_WAIT             136
#define IDD_DIALOG_COMPLETE             137
#define IDR_GIF1                        153
#define IDR_GIF_PROGRESS                153
#define IDR_CAB4                        154
#define IDR_TXT_TERMS                   174
#define IDB_BMP_WELCOME_TXT             176
#define IDB_BMP_NEXT_N                  177
#define IDB_BMP_NEXT_O                  178
#define IDB_BMP_SETUP_CANCEL_N          179
#define IDB_BMP_SETUP_CANCEL_O          180
#define IDB_BMP_PREV_N                  181
#define IDB_BMP_PREV_O                  182
#define IDB_BMP_CLOSE_N                 183
#define IDB_BMP_CLOSE_O                 184
#define IDB_BMP_MIN_N                   185
#define IDB_BMP_MIN_O                   186
#define IDB_BMP_COMPLETE_BKG            211
#define IDR_INI_ITCMS                   214
#define IDR_INI_ITCMS_100               215
#define IDR_DLL_7Z                      219
#define IDR_EXE_7Z                      220
#define IDR_ZIP_IMG                     221
#define IDR_ZIP_ITCMS                   222
#define IDR_BAT1                        223
#define IDR_DELETE                      223
#define IDR_ZIP_CONFIG                  224
#define IDD_DIALOG1                     225
#define IDD_DIALOG_PATH                 225
#define IDB_BMP_FIND_N                  226
#define IDB_BMP_FIND_O                  227
#define IDB_BITMAP_BKG_LICENSE          231
#define IDB_BITMAP_BKG_WELCOME          232
#define IDB_BITMAP_BKG_AGREE            233
#define IDB_BITMAP_BKG_NAME             234
#define IDB_BITMAP_BKG_PATH             235
#define IDB_BITMAP_BKG_PROGRESS         236
#define IDB_BITMAP_BKG_PROGRESS_2       238
#define IDB_BITMAP_BKG_COMPLETE			237
#define IDB_BMP_COMPLETE_O				240
#define IDB_BMP_COMPLETE_N				241
#define IDB_BMP_PATH_INPUT_O			242
#define IDB_BMP_PATH_INPUT_N			243
#define IDB_BMP_LICENSE_INPUT_O			244
#define IDB_BMP_LICENSE_INPUT_N			245
#define IDB_BMP_NAME_INPUT_O			246
#define IDB_BMP_NAME_INPUT_N			247
#define IDC_STATIC_AGREE                1004
#define IDC_PROGRESS_ONE                1006
#define IDC_STATIC_TEXT                 1007
#define IDC_STATIC_RESULT               1016
#define IDC_BUTTON_NEXT                 1024
#define IDC_BUTTON_INSTALL_CANCEL       1025
#define IDC_BUTTON_EXIT                 1026
#define IDC_BUTTON_SETUP_CANCEL         1037
#define IDC_BUTTON_SETUP_INSTALL_CALCEL 1038
#define IDC_BUTTON_SETUP_INSTALL_NEXT   1039
#define IDC_EDIT_LICENSE_KEY            1040
#define IDC_STATIC_MAIN                 1044
#define IDC_STATIC_INSTALL_STEP_GUIDE   1045
#define IDC_STATIC_INSTALL_SUCCESS_GUIDE 1047
#define IDC_STATIC_WAIT                 1048
#define IDC_STATIC_WAIT_GIF             1049
#define IDC_STATIC_WAIT_MAIN            1050
#define IDC_BUTTON_INSTALL_NEXT         1055
#define IDC_BUTTON_MIN                  1063
#define IDC_BUTTON_CLOSE                1064
#define IDR_TXT_TERMS_KR                1098
#define IDR_TXT_TERMS_EN                1099
#define IDR_TXT_TERMS_JP                1100
#define IDC_BUTTON_FIND                 1114
#define IDC_STATIC_PROGRESSING          1116
#define IDC_EDIT_PATH                   1116
#define IDC_PROGRESS_PROGRESSING        1117
#define IDC_STATIC_PROGRESSING2         1118
#define IDC_STATIC_PROGRESSING_STATUS   1118
#define IDC_EDIT_SERVER_HOST_URL        1123
#define IDC_BUTTON_GETTOKEN_CURL        1126
#define IDC_COMBOBOX_DEPARTMENT         1129
#define IDC_EDIT_AGENT_NAME             1131
#define IDC_BUTTON_REGISTER_CURL        1132
#define IDC_CHECK_AGREE                 1133
#define IDC_EDIT_TERMS                  1134
#define IDC_BUTTON_CANCEL               1137
#define IDC_BUTTON_SETUP_SUCCESS        1140
#define IDC_LIST_RESULT                 1142
#define IDC_BUTTON_PREV                 1143
#define IDR_NOTOSAN_BOLD_FONT           1144
#define IDR_NOTOSAN_REGULAR_FONT        1145
#define IDC_BUTTON_COMPLETE				1146

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        238
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1118
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
