// ServiceMgr.cpp: implementation of the CServiceManager class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ServiceMgr.h"
#include <vector>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SRVARG CServiceManager::m_InstallParam;


CServiceManager::CServiceManager()
{
	ConfigInitialize();
}

VOID CServiceManager::ConfigInitialize()
{
	static BOOL bFirstInit = FALSE;

	if(bFirstInit == TRUE)
		return;
	bFirstInit = TRUE;

	memset(&m_InstallParam, 0, sizeof(SRVARG));

//	TCHAR tTemp[MAX_PATH];
	GetModuleFileName(NULL, m_InstallParam.lpModulePath, MAX_PATH);
	
	_tcscpy_s(m_InstallParam.lpBinaryPathName, _countof(m_InstallParam.lpBinaryPathName), m_InstallParam.lpModulePath);	
	
// 	int len = strlen(m_InstallParam.lpModulePath), i;
// 	for(i=len-1; i>= 0; --i) {
// 		if(m_InstallParam.lpModulePath[i] == '\\') {
// 			strcpy(tTemp, &m_InstallParam.lpModulePath[i+1]);
// 			break;
// 		}
// 	}
// 	strcpy(m_InstallParam.lpBinaryPathName, m_InstallParam.lpModulePath);
// 	m_InstallParam.lpModulePath[i] = 0;
// 	
// 	len = strlen(tTemp);
// 	for(i=len-1; i>= 0; --i) {
// 		if(tTemp[i] == '.') {
// 			tTemp[i] = 0;
// 			break;
// 		}
// 	}
// 	strcpy(m_InstallParam.lpServiceName, tTemp);
// 	strcpy(m_InstallParam.lpDisplayName, tTemp);

	_tcscpy_s(m_InstallParam.lpServiceName, _countof(m_InstallParam.lpServiceName), _T("ofsSvc"));		//이름 설정하지 않으면 우선 디폴트로 CmsSvc로 설정
	_tcscpy_s(m_InstallParam.lpDisplayName, _countof(m_InstallParam.lpDisplayName), _T("ofsSvc"));

	m_InstallParam.dwDesiredAccess	= SERVICE_ALL_ACCESS;
	m_InstallParam.dwServiceType	= SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS;
	m_InstallParam.dwStartType		= SERVICE_AUTO_START;
	m_InstallParam.dwErrorControl	= SERVICE_ERROR_NORMAL;
}

CServiceManager::~CServiceManager()
{

}

VOID CServiceManager::ConfigServiceName(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.lpServiceName, _countof(m_InstallParam.lpServiceName), lpszStr); }
VOID CServiceManager::ConfigServiceDisp(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.lpDisplayName, _countof(m_InstallParam.lpDisplayName), lpszStr); }
VOID CServiceManager::ConfigServiceDesc(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.lpDescription, _countof(m_InstallParam.lpDescription), lpszStr); }
VOID CServiceManager::ConfigServiceExec(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.lpBinaryPathName, _countof(m_InstallParam.lpBinaryPathName), lpszStr); }
VOID CServiceManager::ConfigDependencies(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.lpDependencies, _countof(m_InstallParam.lpDependencies), lpszStr); }
VOID CServiceManager::ConfigSfaRebootMessage(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.sfaReBootMessage, _countof(m_InstallParam.sfaReBootMessage), lpszStr); }
VOID CServiceManager::ConfigSfaRetryCommand(LPCTSTR lpszStr)
	{ _tcscpy_s(m_InstallParam.sfaRetryCommand, _countof(m_InstallParam.sfaRetryCommand), lpszStr); }
VOID  CServiceManager::ConfigSfaCounterReset(UINT count)
	{ m_InstallParam.sfaFailureCounter = count; }
VOID CServiceManager::SetServiceHandler(SERVICE_STATUS_HANDLE ssh)
	{ m_InstallParam.sshHandler = ssh; }
SERVICE_STATUS_HANDLE CServiceManager::GetServiceHandler()
	{ return m_InstallParam.sshHandler; }

VOID CServiceManager::ConfigStartUser(LPCTSTR lpszUser, LPCTSTR lpszPassword)
{
	CServiceUtility csu;
	csu.UserPrivileges(lpszUser, _T("SeServiceLogonRight") );
	_tcscpy_s(m_InstallParam.lpServiceStartName, _countof(m_InstallParam.lpServiceStartName), lpszUser);
//	strcpy(m_InstallParam.lpServiceStartName, lpszUser);

	if(lpszPassword == NULL || lpszPassword[0] == 0)
	{
		csu.ServiceUserBlankPassword(TRUE);
		m_InstallParam.lpPassword[0] = 0;
	}
	else
		_tcscpy_s(m_InstallParam.lpPassword, _countof(m_InstallParam.lpPassword), lpszPassword);
		//strcpy(m_InstallParam.lpPassword, lpszPassword);
}

VOID CServiceManager::ConfigSfaAction(UINT uIDX, SC_ACTION_TYPE action, UINT delay)
{
	if(uIDX > 2)
		return;

	m_InstallParam.sfaAction[uIDX].Delay = delay;
	m_InstallParam.sfaAction[uIDX].Type  = action;

	if(action == SC_ACTION_REBOOT)
	{
		CServiceUtility csu;
		csu.ProcessPrivileges(GetCurrentProcess(), SE_SHUTDOWN_NAME, TRUE);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////

DWORD CServiceManager::Install()
{
	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;

	hHandle.hSrv = CreateService(hHandle.hScm,
		m_InstallParam.lpServiceName,
		m_InstallParam.lpDisplayName,
		m_InstallParam.dwDesiredAccess,
		m_InstallParam.dwServiceType,
		m_InstallParam.dwStartType,
		m_InstallParam.dwErrorControl,
		m_InstallParam.lpBinaryPathName,
		NULL,
		NULL,
		m_InstallParam.lpDependencies,
		m_InstallParam.lpServiceStartName[0] == 0 ? NULL : m_InstallParam.lpServiceStartName,
		m_InstallParam.lpPassword[0] == 0 ? NULL : m_InstallParam.lpPassword);

	if (hHandle.hSrv == NULL) 
	{
		DWORD er = GetLastError();
		return er;
	}
		

	SERVICE_DESCRIPTION Desc;
	memset(&Desc, 0x00, sizeof(Desc));
	Desc.lpDescription = m_InstallParam.lpDescription;
	SERVICE_FAILURE_ACTIONS sfa;
	memset(&sfa, 0x00, sizeof(sfa));
	sfa.dwResetPeriod = INFINITE;
	sfa.cActions = 1;
	SC_ACTION failActions[3];
	failActions[0].Type = SC_ACTION_RESTART; //Failure action: Restart Service
	failActions[0].Delay = 60000; //number of seconds to wait before performing failure action, in milliseconds = 2minutes
	failActions[1].Type = SC_ACTION_RESTART;
	failActions[1].Delay = 60000;
	failActions[2].Type = SC_ACTION_RESTART;
	failActions[2].Delay = 60000;
	sfa.lpsaActions = failActions;
	ChangeServiceConfig2(hHandle.hSrv, SERVICE_CONFIG_DESCRIPTION, &Desc);
	//m_InstallParam.sfa.cActions = 3;
    ChangeServiceConfig2(hHandle.hSrv, SERVICE_CONFIG_FAILURE_ACTIONS, &sfa);

    return ERROR_SUCCESS;
}

DWORD CServiceManager::Uninstall(BOOL bForce, DWORD dwWait)
{
	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;

	DWORD dwError;
	hHandle.hSrv = OpenService(hHandle.hScm, m_InstallParam.lpServiceName, SERVICE_ALL_ACCESS);
	if (hHandle.hSrv == NULL) 
	{
		dwError = GetLastError();
		if(dwError == ERROR_SERVICE_DOES_NOT_EXIST)
			return ERROR_SUCCESS;
		return dwError;
	}

	SERVICE_STATUS ss = {0};
	dwError = GetServiceRunStatus(ss);
	if(dwError != ERROR_SUCCESS)
	{
		if(dwError == ERROR_SERVICE_NOT_ACTIVE)
		{
			if(!DeleteService(hHandle.hSrv))
				return GetLastError();
			return ERROR_SUCCESS;
		}
		return GetLastError();
	}

	if (ss.dwCurrentState != SERVICE_STOPPED) 
	{
		if(Stop(bForce, dwWait) != ERROR_SUCCESS)
			return GetLastError();
	}

	if(!DeleteService(hHandle.hSrv))
		return GetLastError();

    return ERROR_SUCCESS;
}

DWORD CServiceManager::GetServiceRunStatus(SERVICE_STATUS& ss)
{
	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;

	hHandle.hSrv = OpenService(hHandle.hScm, m_InstallParam.lpServiceName, SERVICE_INTERROGATE);
	if (hHandle.hSrv == NULL) 
		return GetLastError();

	if(ControlService(hHandle.hSrv, SERVICE_CONTROL_INTERROGATE, &ss) == 0)
	{
		DWORD dw = GetLastError();
		if(dw == ERROR_SERVICE_NOT_ACTIVE)
		{
			ss.dwCurrentState = SERVICE_STOPPED;
			return ERROR_SUCCESS;
		}
		
		return dw;
	}

	return ERROR_SUCCESS;
}

DWORD CServiceManager::GetServiceRunStatus()
{
	CServiceUtility utility;
	if(utility.IsServiceMode() == FALSE)
		return SERVICE_RUNNING;

	SERVICE_STATUS ss = {0};
	if(GetServiceRunStatus(ss) != ERROR_SUCCESS)
		return ERROR_SUCCESS;

	return ss.dwCurrentState;
}


DWORD CServiceManager::SetServiceRunStatus(DWORD dwStatus)
{
	SERVICE_STATUS ss = {0};
	ss.dwServiceType=CServiceManager::m_InstallParam.dwServiceType;
	ss.dwCurrentState = dwStatus;
	ss.dwControlsAccepted = 0xFF;
	if(SetServiceStatus(CServiceManager::m_InstallParam.sshHandler, &ss) == FALSE)
		return GetLastError();

	return ERROR_SUCCESS;
}


DWORD CServiceManager::Start(DWORD dwWait)
{
	SERVICE_STATUS ss = {0};
	if(GetServiceRunStatus(ss) != ERROR_SUCCESS)
		return GetLastError();

	if(ss.dwCurrentState == SERVICE_RUNNING)
		return ERROR_SUCCESS;

	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;

	SERVICE_DESCRIPTION Desc;
	memset(&Desc, 0x00, sizeof(Desc));
	Desc.lpDescription = m_InstallParam.lpDescription;
	SERVICE_FAILURE_ACTIONS sfa;
	memset(&sfa, 0x00, sizeof(sfa));
	sfa.dwResetPeriod = INFINITE;
	sfa.cActions = 1;
	SC_ACTION failActions[3];
	failActions[0].Type = SC_ACTION_RESTART; //Failure action: Restart Service
	failActions[0].Delay = 60000; //number of seconds to wait before performing failure action, in milliseconds = 2minutes
	failActions[1].Type = SC_ACTION_RESTART;
	failActions[1].Delay = 60000;
	failActions[2].Type = SC_ACTION_RESTART;
	failActions[2].Delay = 60000;
	sfa.lpsaActions = failActions;
	ChangeServiceConfig2(hHandle.hSrv, SERVICE_CONFIG_DESCRIPTION, &Desc);
	//m_InstallParam.sfa.cActions = 3;
	ChangeServiceConfig2(hHandle.hSrv, SERVICE_CONFIG_FAILURE_ACTIONS, &sfa);

	hHandle.hSrv = OpenService(hHandle.hScm, m_InstallParam.lpServiceName, SERVICE_START | SERVICE_QUERY_STATUS);
	if (hHandle.hSrv == NULL) 
		return GetLastError();
	if(!StartService(hHandle.hSrv, 0, NULL))
		return GetLastError();

	while(--dwWait)
	{
		GetServiceRunStatus(ss);
		if(ss.dwCurrentState == SERVICE_RUNNING)
			return ERROR_SUCCESS;
		Sleep(1000);
	}
	
	return WAIT_TIMEOUT;
}

DWORD CServiceManager::Stop(BOOL bForce, DWORD dwWait)
{
 	SERVICE_STATUS ss = {0};
	if(GetServiceRunStatus(ss) != ERROR_SUCCESS)
		return GetLastError();

	if(ss.dwCurrentState == SERVICE_STOPPED)
		return ERROR_SUCCESS;

	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;
	
	hHandle.hSrv = OpenService(hHandle.hScm, m_InstallParam.lpServiceName, SERVICE_ALL_ACCESS);
	if (hHandle.hSrv == NULL) 
		return GetLastError();

	if(!ControlService(hHandle.hSrv, SERVICE_CONTROL_STOP, &ss))
		return GetLastError();

	while(--dwWait)
	{
		GetServiceRunStatus(ss);
		if(ss.dwCurrentState == SERVICE_STOPPED)
			return ERROR_SUCCESS;
		Sleep(1000);
	}
	
	return WAIT_TIMEOUT;
}

DWORD CServiceManager::Pause(DWORD dwWait)
{
 	SERVICE_STATUS ss = {0};
	if(GetServiceRunStatus(ss) != ERROR_SUCCESS)
		return GetLastError();

	if(ss.dwCurrentState == SERVICE_PAUSED)
		return ERROR_SUCCESS;

	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;
	hHandle.hSrv = OpenService(hHandle.hScm, m_InstallParam.lpServiceName, SERVICE_ALL_ACCESS);
	if (hHandle.hSrv == NULL) 
		return GetLastError();

	if(!ControlService(hHandle.hSrv, SERVICE_CONTROL_PAUSE, &ss))
		return GetLastError();

	while(--dwWait)
	{
		GetServiceRunStatus(ss);
		if(ss.dwCurrentState == SERVICE_PAUSED)
			return ERROR_SUCCESS;
		Sleep(1000);
	}
	
	return WAIT_TIMEOUT;
}

DWORD CServiceManager::Continue(DWORD dwWait)
{
 	SERVICE_STATUS ss = {0};
	if(GetServiceRunStatus(ss) != ERROR_SUCCESS)
		return GetLastError();

	if(ss.dwCurrentState != SERVICE_PAUSED)
		return ERROR_SUCCESS;

	CSrvHandle hHandle;
	hHandle.hScm = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	if(hHandle.hScm == NULL)
		return ERROR_INVALID_HANDLE;
	
	hHandle.hSrv = OpenService(hHandle.hScm, m_InstallParam.lpServiceName, SERVICE_ALL_ACCESS);
	if (hHandle.hSrv == NULL) 
		return GetLastError();

	if(!ControlService(hHandle.hSrv, SERVICE_CONTROL_CONTINUE, &ss))
		return GetLastError();

	while(--dwWait)
	{
		GetServiceRunStatus(ss);
		if(ss.dwCurrentState == SERVICE_RUNNING)
			return ERROR_SUCCESS;
		Sleep(1000);
	}
	
	return WAIT_TIMEOUT;
}




