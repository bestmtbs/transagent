// ServiceUtil.h: interface for the CServiceUtil class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERVICEUTIL_H__33AC0EB8_F77D_4B21_BEE9_F025DA3F0D50__INCLUDED_)
#define AFX_SERVICEUTIL_H__33AC0EB8_F77D_4B21_BEE9_F025DA3F0D50__INCLUDED_

#pragma warning(disable: 4996)

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _WINDOWS_
#	include <windows.h>
#endif

#ifndef _WINSVC_
#	include <winsvc.h>
#endif

#include <Ntsecapi.h>
#include <WtsApi32.h>
#include <stdio.h>

class CServiceUtility 
{

public:
	CServiceUtility();
	virtual ~CServiceUtility();

	/*------------------------------------------
	 | Service and Account authority Functions |
	 ------------------------------------------*/

	// allow blank password user
	BOOL ServiceUserBlankPassword(BOOL bAllow);

	// get user SID by username
	BOOL GetUserSID(PSID sid, const TCHAR* username);

	// add privilege to sid user
	//BOOL UserPrivileges(const char* username, wchar_t* pszPrivilege);
	BOOL UserPrivileges(const TCHAR* username, wchar_t* pszPrivilege);

	// enable/disale privilege to pid process
	BOOL ProcessPrivileges(HANDLE pid, TCHAR* pszPrivilege, BOOL bEnable);

	// check under service mode
	BOOL IsServiceMode();

	// create user application to session
	BOOL CreateProcessToDesktop(TCHAR* pszExecute, TCHAR* args, STARTUPINFO& si, PROCESS_INFORMATION& pi, UINT SessionID);



	/*------------------------------------
	 | Terminal Server Utility Functions |
	 ------------------------------------*/

	// enumerate windows terminal sessions
	BOOL WTSEnumSessions(PWTS_SESSION_INFO& info, LPDWORD count);

	// enumerate windows terminal process 
	BOOL WTSEnumProcesses(PWTS_PROCESS_INFO& info, LPDWORD count);

	// free memory (WTSEnumSessions, WTSEnumServers, WTSEnumProcesses 1'st argument)
	void WTSFree(void* pData);

	// query session detail info
	// ex) CServiceUtil::CWTSSession ws(WTS_CURRENT_SERVER_HANDLE, session id);
	class CWTSSession
	{
	public:
		CWTSSession(HANDLE hWTS, DWORD SessionID);
		~CWTSSession();
		
		//CHAR	IPAddress[16];
		TCHAR	IPAddress[16];
		USHORT	HorizontalResolution;
		USHORT	VerticalResolution;
		USHORT	ColorDepth;
		USHORT	ProtocolType;
		LPTSTR	ClientName;
		LPTSTR	DomainName;
		LPTSTR	UserName;
		LPTSTR	WinStation;
	protected:
		void Query(HANDLE hWTS, DWORD SessionID);
		void Init();
		void Free();
	};

	// from msdn: http://msdn.microsoft.com/en-us/library/ms724429(VS.85).aspx
	//BOOL GetOSDisplayString( LPTSTR pszOS);
	BOOL GetOSDisplayString( LPCTSTR pszOS);
	 

	// very simple file pointer handler for file logging
	class CSimpleLog
	{
	public:
		CSimpleLog(char* path) 
			{ m_pf = fopen(path, "a"); }
		~CSimpleLog() 
			{ if(m_pf) fclose(m_pf); }
		void flush() 
			{ if(m_pf) fflush(m_pf); }
		operator FILE*() const
			{ return m_pf; }
	private:
		FILE* m_pf;
	};
};

#endif // !defined(AFX_SERVICEUTIL_H__33AC0EB8_F77D_4B21_BEE9_F025DA3F0D50__INCLUDED_)
