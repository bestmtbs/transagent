//#define _WIN32_WINNT 0x0501

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <winsvc.h>


#include "ServiceUtil.h"
#include "ServiceMgr.h"
#include "PathInfo.h"
#include "Impersonator.h"
#include "UtilsProcess.h"
//#include "CommonDefine.h"
#include "PathInfo.h"
#include "GeneralUtil.h"
#include "UtilsReg.h"
#include "UtilsFile.h"
#include "ALCInformation.h"

//using namespace std;
////////////////////////////////////////////////////////////////////
// 아래의 구조체는 서비스에서 제공받는 이벤트를 시각적으로 출력할 때
// 사용하기 위하여 이벤트와 이름을 매핑하여 놓은 것이다.
////////////////////////////////////////////////////////////////////
typedef struct _STNAME
{
	const char* text;
	DWORD id;
} STNAME;

STNAME stname[] = {
	{"",									  0x00000000},
	{"SERVICE_CONTROL_STOP",                  0x00000001},
	{"SERVICE_CONTROL_PAUSE",                 0x00000002},
	{"SERVICE_CONTROL_CONTINUE",              0x00000003},
	{"SERVICE_CONTROL_INTERROGATE",           0x00000004},
	{"SERVICE_CONTROL_SHUTDOWN",              0x00000005},
	{"SERVICE_CONTROL_PARAMCHANGE",           0x00000006},
	{"SERVICE_CONTROL_NETBINDADD",            0x00000007},
	{"SERVICE_CONTROL_NETBINDREMOVE",         0x00000008},
	{"SERVICE_CONTROL_NETBINDENABLE",         0x00000009},
	{"SERVICE_CONTROL_NETBINDDISABLE",        0x0000000A},
	{"SERVICE_CONTROL_DEVICEEVENT",           0x0000000B},
	{"SERVICE_CONTROL_HARDWAREPROFILECHANGE", 0x0000000C},
	{"SERVICE_CONTROL_POWEREVENT",            0x0000000D},
	{"SERVICE_CONTROL_SESSIONCHANGE",         0x0000000E},
	{"SERVICE_CONTROL_PRESHUTDOWN",           0x0000000F},
};

STNAME wtsname[] = {
	{"",									  0x00000000},
	{"CONSOLE_CONNECT",						  0x00000001},
	{"CONSOLE_DISCONNECT",					  0x00000002},
	{"REMOTE_CONNECT",						  0x00000003},
	{"REMOTE_DISCONNECT",					  0x00000004},
	{"LOGON",								  0x00000005},
	{"LOGOFF",								  0x00000006},
	{"LOCK",								  0x00000007},
	{"UNLOCK",								  0x00000008},
	{"SESSION_REMOTE_CONTROL",				  0x00000009},
};

BOOL g_bLogon = FALSE;
BOOL g_bSleep = FALSE;
INT g_nCnt;

CServiceUtility::CSimpleLog FileLog("C:\\srv.log");
void Start_ofsMgr_Process();
void OfficseSafer_DeleteService();
BOOL StopAT_Suspend();
void Check_InstallFD_SecyrityOption();
void Change_AllFileSecurity(CString _strPath);

void GetClearModulePath(char* path)
{
	_strlwr(path);
	char* p = strstr(path, "debug");
	if(p)
	{
		path[p-path] = 0;
		return;
	}

	p = strstr(path, "release");
	if(p)
	{
		path[p-path] = 0;
		return;
	}
}

DWORD WINAPI service_handler(DWORD fdwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext)
{
	CServiceManager manager;

	//#ifdef _DEBUG
	if(fdwControl != SERVICE_CONTROL_INTERROGATE)
	{
		fprintf(FileLog, "EVENT> %s\n", stname[fdwControl].text);
		FileLog.flush();
	}
	//#endif

	switch (fdwControl) 
	{ 
	case SERVICE_CONTROL_PAUSE:
		manager.SetServiceRunStatus(SERVICE_PAUSE_PENDING);
		// 서비스를 일시 중지 시킨다.
		manager.SetServiceRunStatus(SERVICE_PAUSED);
		break;

	case SERVICE_CONTROL_CONTINUE:
		manager.SetServiceRunStatus(SERVICE_CONTINUE_PENDING);
		// 일시 중지 시킨 서비스를 재개한다.
		manager.SetServiceRunStatus(SERVICE_RUNNING);
		break;

	case SERVICE_CONTROL_STOP:
		manager.SetServiceRunStatus(SERVICE_STOP_PENDING);
		// 서비스를 멈춘다 (즉, 종료와 같은 의미)
		// 서비스를 종료하면, service_main 는 절대로 리턴하지 않는다.
		// 그러므로 해제하려면 작업이 있으면 모든것을 이곳에서 처리한다.
		manager.SetServiceRunStatus(SERVICE_STOPPED);
		break;

		// 윈도우에서 세션에 변경 사항이 발생하였을 경우, 발생하는 이벤트.
	case SERVICE_CONTROL_SESSIONCHANGE:
		{
			// 세션에 관련된 이벤트 정보는 아래의 구조체에 복사하여 사용한다.
			WTSSESSION_NOTIFICATION wtsnoti = {0};
			memcpy(&wtsnoti, lpEventData, sizeof(WTSSESSION_NOTIFICATION));

			CServiceUtility::CWTSSession ws(WTS_CURRENT_SERVER_HANDLE, wtsnoti.dwSessionId);
#ifdef _DEBUG
			fprintf(FileLog, "   WTS[%s <%d>] => IP(%s), Client(%s), Domain(%s), User(%s), WinStation(%s)\n",
				wtsname[dwEventType].text, 
				wtsnoti.dwSessionId, 
				ws.IPAddress, 
				ws.ClientName, 
				ws.DomainName, 
				ws.UserName, 
				ws.WinStation);
			FileLog.flush();
#endif
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_service"));
			//CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("dwEventType: %d[line: %d, function: %s, file: %s]"), dwEventType, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_service"), strCount);
			//윈도우 계정 로그인 했을대
			if(WTS_SESSION_LOGON == dwEventType || WTS_SESSION_UNLOCK == dwEventType)
			{
				{
					//CString strCountFile = _T("");
					CString strCount = _T("");
					//DWORD dwBytesWritten = 0;
					//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_service"));
					//CTime cTime = CTime::GetCurrentTime();
					strCount.Format(_T("dwEventType: %d, g_bSleep: %d[line: %d, function: %s, file: %s]"), dwEventType, g_bSleep, __LINE__, __FUNCTIONW__, __FILEW__);
					//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
					DROP_TRACE_LOG(_T("TiorSaver_service"), strCount);
				}
				g_bSleep = FALSE;
				OutputDebugString(_T("[Test SVC] WTS_SESSION_LOGON "));
			} else {
				g_nCnt = 0;
				{
					//CString strCountFile = _T("");
					CString strCount = _T("");
					//DWORD dwBytesWritten = 0;
					//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_service_count.log"));
					//CTime cTime = CTime::GetCurrentTime();
					strCount.Format(_T("dwEventType: %d, g_bSleep: %d, nCnt: %d [line: %d, function: %s, file: %s]"), dwEventType, g_bSleep, g_nCnt, __LINE__, __FUNCTIONW__, __FILEW__);
					DROP_TRACE_LOG(_T("TiorSaver_service_count"), strCount);
					//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
				}
				g_bSleep = TRUE;
				CProcess::KillProcessByName(CPathInfo::GetClientInstallPath() + WTIOR_TRANS_AGENT_NAME);
				{
					CString strCount = _T("");
					strCount.Format(_T("dwEventType: %d, g_bSleep: %d, nCnt: %d [line: %d, function: %s, file: %s]"), dwEventType, g_bSleep, g_nCnt, __LINE__, __FUNCTIONW__, __FILEW__);
					DROP_TRACE_LOG(_T("TiorSaver_service_count"), strCount);
				}
			}
			//if (WTS_SESSION_TERMINATE == dwEventType) {
			//	g_bSleep = TRUE;
			//	CString strCountFile = _T("");
			//	CString strCount = _T("");
			//	DWORD dwBytesWritten = 0;
			//	strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_service"));
			//	CTime cTime = CTime::GetCurrentTime();
			//	strCount.Format(_T("[%d][%d] %d %s[line: %d, function: %s, file: %s]\r\n"), getpid(), dwEventType, g_bSleep, cTime.Format(_T("%Y-%m-%d %H:%M:%S")), __LINE__, __FUNCTIONW__, __FILEW__);
			//	WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			//}
			////사용자 전환시 처리
			//if(dwEventType == WTS_CONSOLE_DISCONNECT)
			//{
			//	g_bSleep = TRUE;
			//	CString strCountFile = _T("");
			//	CString strCount = _T("");
			//	DWORD dwBytesWritten = 0;
			//	strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_service"));
			//	CTime cTime = CTime::GetCurrentTime();
			//	strCount.Format(_T("[%d][%d] %d %s[line: %d, function: %s, file: %s]\r\n"), getpid(), dwEventType, g_bSleep, cTime.Format(_T("%Y-%m-%d %H:%M:%S")), __LINE__, __FUNCTIONW__, __FILEW__);
			//	WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			//	OutputDebugString(_T("[Test SVC] WTS_CONSOLE_DISCONNECT "));
			//}
		}

	default:
		break;
	}

	return NO_ERROR;
}


/**
@brief     설치폴더의 보안 속성을 확인하고 변경
@author    HHH
@date      2013.11.07
@note      설치 폴더에 Everyone 이 추가되어 있고 권한이 모두 설정되어 있는지 확인하고 그렇지 않으면 추가
*/
void Check_InstallFD_SecyrityOption()
{

	//CString strInstallPath, strTiorSaverZonePath;
	CString strInstallPath = _T("");
	strInstallPath =  CPathInfo::GetClientInstallPath(FALSE);

	//strInstallPath =  CPathInfo::GetOfficeRootPath(FALSE);
	//strTiorSaverZonePath = CPathInfo::GetTiorSaverZone_Path(FALSE);
	
	CALCInformation acl_info;
	acl_info.SetPath(strInstallPath);
	vector<ACL_USER>::iterator itor;
	vector<ACL_USER> vtData;
	acl_info.GetALCInfo(vtData);

	BOOL bFind = FALSE;
	for(itor = vtData.begin() ; itor != vtData.end(); ++itor)
	{
		ACL_USER aclData = *(itor);
		CString strUserName =aclData.username;
		if(strUserName == L"Everyone")
		{
			bFind = TRUE;
			break;
		}						
	}

	
	if(bFind == FALSE)		//dsntech폴더에 everyone없다면 폴더 보안속성을 조절하지 않은것이므로 조절
	{
		Change_AllFileSecurity(strInstallPath);
		//Change_AllFileSecurity(strTiorSaverZonePath);
	}
}

/**
@brief     인자로 들어온 폴더의 하위 폴더 및 파일의 모든 권한 변경
@author    HHH
@param	   _strPath: 경로
@param	   bChangeRootPath: 인자로 들어온 최상위 루트 폴더도 바꿀것인지
@date      2013.11.12
@note      
*/
void Change_AllFileSecurity(CString _strPath)
{
	CALCInformation acl_info;
	CStringArray arrAccountList;
	arrAccountList.Add(L"Everyone");	//설치폴더 권한을 Everyone으로 설정한다.
	CFileFind finder;
	
	acl_info.ChangeFileSecurity(_strPath, arrAccountList, GENERIC_ALL | GENERIC_READ |GENERIC_WRITE | GENERIC_EXECUTE) ;

	BOOL bWorking = finder.FindFile(_strPath + _T("\\*.*"));
	DWORD dwError = 0;
	while(bWorking)
	{
		bWorking = finder.FindNextFile();

		if( finder.IsDots())
			continue;
		
		if( finder.IsDirectory() )		//폴더인경우 폴더안에 .info파일 이외에 복호화된 파일이 존재하면 수정날짜를 확인하여 한달이상 변경이 없는 경우에만 삭제
		{
			Change_AllFileSecurity(finder.GetFilePath());
		}
		else				
			acl_info.ChangeFileSecurity(finder.GetFilePath(), arrAccountList, GENERIC_ALL | GENERIC_READ |GENERIC_WRITE | GENERIC_EXECUTE) ;
		
				
	}

}

int service_main(INT ARGC, LPSTR* ARGV)
{
	//g_bSleep = FALSE;
	g_nCnt = 0;
	CServiceManager manager;
	CServiceUtility utility;
	if(utility.IsServiceMode() == TRUE)
	{
		manager.SetServiceHandler(RegisterServiceCtrlHandlerEx(CServiceManager::m_InstallParam.lpServiceName, service_handler, NULL));
		if (manager.GetServiceHandler() == NULL) 
			return 0;
	}
	else
	{
		printf("run debug mode\npress any key close...\n");
	}
	
	manager.SetServiceRunStatus(SERVICE_START_PENDING);

	// bla bla initialize...
	Sleep(1000);

	//CString strLicenseCmsFilePath = CPathInfo::GetClientInstallPath() + STR_LICENSE_CMS;
	//OutputDebugString(_T("[OfsSvc] RootPath : ") + strLicenseCmsFilePath);

	//if( FALSE == IsFileExist( strLicenseCmsFilePath.GetBuffer(0) ) )
	//{
	//	//license.cms 파일이 존재하지 않는다면 비정상 설치로 파단하고 삭제한다.
	//	OutputDebugString(_T("[OfsSvc] #### Abnormal setup ####") );
	//	CString strSetupPath = CPathInfo::GetOfficeRootPath(FALSE);
	//	DWORD dwErrorCode = 0;
	//	ForceDeleteDir(strSetupPath, &dwErrorCode);	
	//	OfficseSafer_DeleteService();
	//	return 0;
	//}

	//서비스 구동시에 dsntech 설치 폴더의 권한을 Everyone이 없음녀 Everyone을 추가, 모든 권한을 부여해준다.
	Check_InstallFD_SecyrityOption();


	// start ok, i'm ready receive event
	manager.SetServiceRunStatus(SERVICE_RUNNING);

	DWORD dwRet = 0;
	CImpersonator imp;
	CString strUpdatePath = CPathInfo::GetClientInstallPath();
	CString strClientModule  =strUpdatePath+ WTIOR_UPDATE_AGENT_NAME;
	strClientModule +=_T(" -c");		
	imp.RunProcessAndWait(strClientModule, strUpdatePath,  &dwRet );



	// 프로세스를 감시하고 종료하면 다시 실행시킨다.
	while(manager.GetServiceRunStatus() != SERVICE_STOPPED)
	{
		if(manager.GetServiceRunStatus() == SERVICE_PAUSED)
		{
			Sleep(1000);
			continue;
		}
		OutputDebugString(_T("[Test SVC] Running......"));
		/*if(utility.IsServiceMode() == FALSE && _kbhit())
		{
			printf("stop debug mode\n");
			break;
		}*/
		Sleep(2000);
		Start_ofsMgr_Process();
	}

	return 0;
}

int main(int argc, char** argv)
{
	CString a;
#if 0
	/////////////////////////////////////////////////////////////////////
	// test code
	/////////////////////////////////////////////////////////////////////

	CServiceUtility u;
	u.ServiceUserBlankPassword(FALSE);

	u.UserPrivileges("Administrtor", L"SeServiceLogonRight");
	u.ProcessPrivileges(GetCurrentProcess(), SE_SHUTDOWN_NAME, TRUE);

	PWTS_PROCESS_INFO sinfo = NULL;
	DWORD count = 0;
	u.WTSEnumProcesses(sinfo, &count);
	u.WTSFree(sinfo);

	PWTS_SESSION_INFO info = NULL;
	count = 0;
	u.WTSEnumSessions(info, &count);
	u.WTSFree(info);

	CServiceUtility::CWTSSession ws(WTS_CURRENT_SERVER_HANDLE, 0);

	char buffer[512] = {0};
	if(u.GetOSDisplayString(buffer))
		printf("%s\n", buffer);
#endif




	DWORD dwRet = 0;
	DWORD nErr;
	//CImpersonator imp;
	//CString strUpdatePath = CPathInfo::GetClientInstallPath();
	//CString strClientModule  =strUpdatePath+ UPDATE_PROC_NAME;
	//strClientModule +=_T(" -c");		
	//imp.RunProcessAndWait(strClientModule, strUpdatePath,  &dwRet );




	CServiceManager manager;
	CServiceUtility utility;

	/* 설정하지 않으면, 기본 파일값으로 
	파일 이름을 잘라다가 자동으로 설정한다. */
	manager.ConfigServiceName(_T("TiorSaver SERVICE"));
	manager.ConfigServiceDisp(_T("TiorSaver SERVICE")); 
	//manager.ConfigServiceDesc("CROWBACK's test service, forever ting... ting...");
	CString str;
	str.Format(_T("argc : %d"), argc);
	OutputDebugString(_T("[OfsSvc] --- 1: ") + str);
	
	char ch = 0;
	if(argc == 2)
	{
		ch = argv[1][0];
		DWORD nErr = 0;

		switch(ch)
		{
		case 'i':
			nErr = manager.Install();
			if( nErr == ERROR_SUCCESS )
			{
				OutputDebugString(_T("[OfsSvc] --- Good"));
				Sleep(100);
				if( manager.Start() == ERROR_SUCCESS)
				{

					_tprintf(_T("ok"));
					return 0;
				}
			}
			_tprintf(_T("fail %d"), nErr);
			return 0;
		case 'u':
			if( manager.Uninstall() == ERROR_SUCCESS)
				_tprintf(_T("ok"));
			else
				_tprintf(_T("fail"));

			return 0;
		case 's':
			manager.Start();
			return 0;
		case 't':
			manager.Stop();
			return 0;
		case 'p':
			manager.Pause();
			return 0;
		case 'c':
			manager.Continue();
			return 0;
		case 'e':		
			nErr  = manager.Install();
			if(ERROR_SUCCESS == nErr )
			{
				_tprintf(_T("ok"));
			}
			else
			{	
				CString strDbg;
				strDbg.Format(_T("[OfsSvc] Services Install GetLastError %d"), nErr);
				OutputDebugString(strDbg);
				_tprintf(_T("fail"));
			}
			return 0;

		default:
			return service_main(argc, argv);
		}
	}

// 	if(utility.IsServiceMode() == FALSE)
// 	{
// 		OutputDebugString(_T("[OfsSvc] --- 2"));
// 		char ch = 0;
// 		if(argc != 2)
// 		{
// 			printf("sample.exe [option: i, u, s, t, p, c]\n");
// 			printf("  i - install\n");
// 			printf("  u - uninstall\n");
// 			printf("  s - start\n");
// 			printf("  t - stop\n");
// 			printf("  p - pause\n");
// 			
// 			printf("  c - continue\n\n");
// 
// 
// 			printf("input command: ");
// 			ch = getch();
// 		}
// 		else
// 			ch = argv[1][0];
// 
// 		switch(ch)
// 		{
// 		case 'i':
// 			if( manager.Install() == ERROR_SUCCESS )
// 			{
// 				Sleep(200);
// 				if( manager.Start() != ERROR_SUCCESS)
// 				{
// 					printf("ok");
// 					return 0;
// 				}
// 			}
// 			printf("fail");
// 			return 0;
// 		case 'u':
// 			if( manager.Uninstall() == ERROR_SUCCESS)
// 				printf("ok");
// 			else
// 				printf("fail");
// 
// 			return 0;
// 		case 's':
// 			manager.Start();
// 			return 0;
// 		case 't':
// 			manager.Stop();
// 			return 0;
// 		case 'p':
// 			manager.Pause();
// 			return 0;
// 		 case 'c':
// 			manager.Continue();
// 			return 0;
// 		 
// 
// 		default:
// 			return service_main(argc, argv);
// 		}
// 	}

	OutputDebugString(_T("[OfsSvc] --- 3"));
	SERVICE_TABLE_ENTRY STE[] =
	{
		{manager.m_InstallParam.lpServiceName, (LPSERVICE_MAIN_FUNCTION)service_main},
		{NULL,NULL}
	};

	if(StartServiceCtrlDispatcher(STE) == FALSE)
		return -1;

	return 0;
}

void Start_ofsMgr_Process()
{
	if (g_bSleep) {
		return;
	}
	OutputDebugString(_T("[CmsSvc] Check ofsMgr -- 1"));

	// 2017-05-18 kh.choi 뮤텍스 체크 -> 프로세스명 체크. 윈도우10 업데이트 후 뮤텍스 체크 실패
	//CString strMutexName ;
	//strMutexName.Format(_T("Global\\%s"), RELOAD_MUTEXT_NAME);
	//
	//if( false == CProcess::IsExistMutex(strMutexName) )
	// 2017-05-18 kh.choi 여기까지 주석처리
	CImpersonator imp;
	//CString strOfsReload = WTIOR_AGENT_RELOAD_NAME;
	CString strOfsTransfer = WTIOR_TRANS_AGENT_NAME;
	CString strOfsDummy = WTIOR_AGENT_DUMMY_NAME;
	if (FALSE == imp.GetProcessExist(strOfsTransfer) && FALSE == imp.GetProcessExist(strOfsDummy) && !g_bSleep)
	{
		//CString strCountFile = _T("");
		CString strCount = _T("");
		//DWORD dwBytesWritten = 0;
		//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_service"));
		//CTime cTime = CTime::GetCurrentTime();
		strCount.Format(_T("[Start_ofsMgr_Process] g_bSleep: %d [line: %d, function: %s, file: %s]"), g_bSleep, __LINE__, __FUNCTIONW__, __FILEW__);
		//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
		DROP_TRACE_LOG(_T("TiorSaver_service"), strCount);
		OutputDebugString(_T("[CmsSvc] Start Dummy"));
		//CString strExePath =  CPathInfo::GetClientInstallPath() + RELOAD_PROC_NAME;
		CString strExePath =  CPathInfo::GetClientInstallPath() + WTIOR_AGENT_DUMMY_NAME;
		OutputDebugString(strExePath);
		CImpersonator imp;
		if( FALSE == imp.CreateProcess(strExePath, SW_HIDE) )
		{
			OutputDebugString(_T("[CmsSvc] imp.CreateProcess Fail....."));
		}
		imp.Free();
	}

	//프로세스가 suspend상태 일수 있으므로 그것을 처리
	DWORD dwOfsReloadPid = GetProcessID(WTIOR_TRANS_AGENT_NAME);
	if(dwOfsReloadPid > 0)
	{
		if( FALSE == StopAT_Suspend() )
			ResumeProcess(dwOfsReloadPid);
	}

}

/**
@brief     ResumeThread 기능을 동작할건지 여부 체크
@author    hhh
@date      2013.07.26
@return	   BOOL
*/
BOOL StopAT_Suspend()
{
	DWORD dwDefalut = 0, dwRet = 0;
	dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, DBG_LOG_DEFAULT_KEY, L"ATSuspend", dwDefalut);
	if(dwRet == 1)
		return TRUE;
	else
		return FALSE;
}

/**
@brief     DeleteService
@author   hhh
@date      2013.08.29
@note     서비스 삭제
*/
void OfficseSafer_DeleteService()
{
	CImpersonator imp;
	CString strServiceName = _T("");
	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
	if(FALSE == imp.CreatProcessAndCompareStr(strServiceName,  "ok", _T("u")) )
	{
		OutputDebugString(_T("[OfsSvc] TiorSaver Service Delte Fail."));		
	}

	imp.Free();
}
