rem SET MA_SOLUTION_PATH=Solution dir
rem SET MA_SOLUTION_NAME=Solution name
rem SET MA_PROJECT_NAME=Projectname(like XmlliteTestApp)
rem SET MA_CONFIG_NAME=build configuration (like Release, Release-x86..)

::cd "%SLN_PATH%\%MA_SOLUTION_PATH%"
cd "%MA_SOLUTION_PATH%"
SET TARGETPATH=%CD%
SET TARGETPRJ=%MA_PROJECT_NAME%

if "%MA_MSI_BUILD%" == "1" goto MSI-BUILD

IF "%MACLEANBUILD%" == "0" goto MA_BUILD_32_1
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /Clean "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Projectconfig "%MA_CONFIG_NAME%|win32" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"
:MA_BUILD_32_1
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /build "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Projectconfig "%MA_CONFIG_NAME%|win32" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"

IF "%X64BUILD%" == "0" goto END
IF "%MACLEANBUILD%" == "0" goto MA_BUILD_64_1
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /Clean "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Projectconfig "%MA_CONFIG_NAME%|x64" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"

:MA_BUILD_64_1
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /build "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Projectconfig "%MA_CONFIG_NAME%|x64" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"
goto END

:MSI-BUILD
IF "%MACLEANBUILD%" == "0" goto MA_BUILD_32_2
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /Clean "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"
:MA_BUILD_32_2
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /build "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"

IF "%X64BUILD%" == "0" goto END
IF "%MACLEANBUILD%" == "0" goto MA_BUILD_64_2
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /Clean "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"
:MA_BUILD_64_2
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\IDE\devenv" "%MA_SOLUTION_NAME%.sln" /build "%MA_CONFIG_NAME%" /Project "%MA_PROJECT_NAME%" /Out "%MAROOTPATH%\%MA_LOG_PATH%\%MA_LOG_NAME%%MABUILDTIME%.log"

:END
cd %MKROOTPATH%
