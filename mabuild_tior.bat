SET MAROOTPATH=%CD%


if EXIST "%ProgramFiles(x86)%" goto x86-1
if EXIST "%ProgramFiles%" goto x64-1

:x86-1
call "%ProgramFiles(x86)%\Microsoft Visual Studio 14.0\VC\bin\vcvars32.bat"
goto START

:x64-1
call "%ProgramFiles%\Microsoft Visual Studio 14.0\VC\bin\vcvars32.bat"

:START

if not "%MACLEANBUILD%" == "1" SET MACLEANBUILD=0

SET X64BUILD=0
SET MA_LOG_NAME=TiorSaver
SET MA_LOG_PATH=Bin
mkdir %MA_LOG_PATH%
SET MA_MSI_BUILD=0

SET HH=%TIME:~0,2% 
SET MN=%TIME:~3,2% 
SET SE=%TIME:~6,2% 
SET MATIME=%DATE:~0,10%
SET MABUILDTIME=[%MATIME%][%HH:~0, 2%-%MN:~0, 2%-%SE:~0, 2%]

call mkda_tior.bat
cd %MAROOTPATH%

