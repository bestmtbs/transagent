

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      StringListEx.cpp 
 @brief     StringListEx Class 구현 파일

            MFC의 CStringList 기능을 확장한 클래스 

 @author    hang ryul lee
 @date      create 2011.07.09
*/


#include "stdafx.h"
#include "StringListEx.h"

/**
 @brief     문자열 리스트의 값을 _sToken구분자를 사용하여 하나의 문자열로 반환한다.
 @author    hang ryul lee
 @date      2008.07.09
 @param     [in] _sToken    구분자 문자열값(Default = _T("\n"))
 @return    _sToken으로 구분된 하나의 문자열값
*/
CString CStringListEx::GetText(const CString _sToken /* = _T("\n") */)
{
    CString sBuffer = _T("");
    CString sValue = _T("");

    POSITION pos = GetHeadPosition();
    while (pos)
    {
        sValue = GetNext(pos) + _sToken;
        sBuffer += sValue;
        sValue = _T("");
    }
    return sBuffer;
}

/**
 @brief     하나의 문자열을 _sToken으로 구분하여 문자열 리스트에 추가한다.
 @author    hang ryul lee
 @date      2008.07.09
 @param     [in] _sFullString   문자열값
 @param     [in] _sToken        구분자 문자열값(Default = _T("\n"))
*/
void CStringListEx::SetText(const CString &_sFullString, const CString _sToken /* = _T("\n") */)
{    
    int nPos = 0;
    CString sValue = _T("");
    
    RemoveAll();

    sValue = _sFullString.Tokenize(_sToken, nPos);
    while (_T("") != sValue)
    {
        AddTail(sValue);
        sValue = _sFullString.Tokenize(_sToken, nPos);
    }
}

/**
 @brief     문자열 리스트에 Name=Value형식으로 문자열이 등록되어 있을 경우
            주어진 POSITION의 Name값을 구한다.
 @author    hang ryul lee
 @date      2008.09.02
 @param     [in] _pos   문자열 리스트의 POSITION 값
 @return    Name 문자열값
*/
CString CStringListEx::GetName(POSITION &_rPosition)
{
    CString sName = _T("");

    if (NULL == _rPosition)
        return sName;

    int nStart = 0;
    sName = GetNext(_rPosition);
    return sName.Tokenize(_T("="), nStart).Trim();
}

/**
 @brief     문자열 리스트에 Name=Value형식으로 문자열이 등록되어 있을 경우
            주어진 Name의 Value값을 구한다.
 @author    hang ryul lee
 @date      2008.09.02
 @param     [in] _sName     Name 문자열값
 @return    Value 문자열값
*/
CString CStringListEx::GetValue(const CString &_sName)
{
    CString sValue = _T("");

    if (0 == _sName.CompareNoCase(_T("")))
        return sValue;

    CString sBuffer = _T("");
    CString sName = _T("");
    POSITION pos = GetHeadPosition();
    while (pos)
    {
        int nStart = 0;
        sBuffer = GetNext(pos);
        sName   = sBuffer.Tokenize(_T("="), nStart).Trim();
        sValue  = sBuffer.Tokenize(_T("="), nStart).Trim();
        if (0 == _sName.CompareNoCase(sName))
            break;
    }

    return sValue;
}

/**
 @brief     문자열 리스트에 Name=Value형식으로 문자열이 등록되어 있을 경우
            주어진 POSITION의 Name과 Value값을 구한다.
 @author    hang ryul lee
 @date      2008.09.30
 @param     [in] _rPosition 문자열 리스트의 POSITION 값
 @param     [out] _sName    Name값
 @param     [out] _sValue   Value값
*/
void CStringListEx::GetNameAndValue(POSITION &_rPosition, CString &_sName, CString &_sValue)
{
    if (NULL == _rPosition)
        return;

    int nStart = 0;
    CString sItem = GetNext(_rPosition);
    _sName = sItem.Tokenize(_T("="), nStart).Trim();
    _sValue = sItem.Tokenize(_T("="), nStart).Trim();
}