/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.05.29		
********************************************************************************/
/**
@file       ConnectParamInfo.h 
@brief     ConnectParamInfo 정의 파일  
@author   jhlee
@date      create 2011.07.20
*/
#pragma once
#include "SecurityControl.h"
class CConnectParamInfo
{
public:
	CConnectParamInfo(void);
	virtual ~CConnectParamInfo(void);

	CString CreateAuthInfoData(CString _strUserID, CString _strPwd, CString _strTrid, CString _strPCID, CString _strKey,CString _strSiteIndex, CString _strIP =_T(""), CString _strAgtVer=_T(""));
	CString CreatePwdChangeInfoData(CString _strTrid, int _iUsrIdx, CString _strPwd, CString _strDueDate,CString _strLicKey, CString _strPCID, int _iLicKeyIdx = 0);
	CString CreatePCInfoData(int _iInsUsrIdx, CString _strPCID, CString _strIP, CString _strMac, CString _strPCName, CString _strTrid,CString _strLicenseKey,CString _strVersion,CString _strOSName, CString _strUserIdx);
	CString CreateLogInfoData();
	CString GetPasswordType();
	CString CreatePolicyRequest(int _iUsrIdx, CString _strTrid,CString _strLicenseKey = _T(""));
	CString CreateUserInfoRequest(int _iUsrIdx, CString _strLicenseKey,int _iLicKeyIdx, CString _strEncKey);
	CString CreateData(CString _strTrid);
	CString CreateConnectInfo(int _iUserIdx, int _iLicKeyIdx, CString _strTrid);
	CString CreateLicenseInfo(int _iUserIdx, int _iLicKeyIdx, CString _strTrid);
	CString CreateUseAllUserInfo(int _iLicKeyIdx);
	CString CreateLicenseCheck(int _iUserIdx, int _iLicKeyIdx);			//2013.05.23 : hhh- zombie pc체크
	CString CreateNewVaccineInfo(NEW_VACCINE_INFO* _pInfo);		//2013.05.23 : hhh-  서버로 새로운 백신 정보 올리기
	////////////////////////로그 데이타 생성 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	CString CreateEncryptFileLogData(ENC_POLICY_LOG* _pLog);	
	//CString CreateSecretLogData(SECRET_LOG* log);
	CString CreateWinPatchLogData(WIN_PATCH_LOG* _pLog);
	CString CreateFirewallLogData(FIREWALL_LOG* _pLog);
	CString CreateWebMailLogData(WEBMAIL_LOG* _pLog);
	CString CreateTrayMailLogData(TRAYMAIL_LOG* _pLog); //hhh
	CString CreateSMTPLogData(SMTP_LOG* _pLog);
	CString CreateFTPLogData(FTP_LOG* _pLog);
	CString CreateSMBLogData(SMB_LOG* _pLog);
	CString CreateAuthLogData(AUTH_LOG* _pLog);
	CString CreateDecryptFileLogData(DECRYPT_LOG* _pLog);
	CString CreateUninstallLogData(CString _strValue);
	CString CreateVaccineLogData(VACCINE_LOG* _pLog);
	CString CreateShareFolderLogData(SHARE_FOLDER_LOG* _pLog);
	CString CreateHDDSpaceLogData(HDD_SPACE_LOG* _pLog);
	CString CreateMediaLogData(MEDIA_LOG* _pLog);
	CString CreateEncryptFileDelLogData(ENC_DELETE_LOG* _pLog);
	CString CreateEncryptFilePrintLogData(ENC_PRINT_LOG* _pLog);
	CString CreateGeneralFileLogData(GENERAL_POLICY_LOG* _pLog);
	CString CreateGeneralFileDelLogData(GENERAL_DELETE_LOG* _pLog);

	CString CreateZombieCheck(CString _strLicenseKey);
	//////////////////////복호화 신청////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	CString CreateInDecReqData(IN_DECRYPT_REQ* _decrypt);
	CString CreateInDecryptData(IN_DECRYPT* _decrypt);
	CString CreateInDecryptLogData(IN_DECRYPT_LOG* _decrypt);

	CString CreateOutDecReqData(OUT_DECRYPT_REQ* _decrypt);
	CString CreateOutDecReqData_Check(OUT_DECRYPT_REQ* _decrypt);
	CString CreateOutDecryptData(OUT_DECRYPT* _decrypt);
	CString CreateOutDecryptLogData(OUT_DECRYPT_LOG* _decrypt);

	CString CreateDecryptAuthKeyData(DEC_AUTH_KEY* _auth);
	CString CreateRequsetVaccineInfo(CString _strVaccineName);


	////////////////////사이트인덱스//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void GetSiteIndex();
	static CString m_strSiteIndex;		// 2017-05-22 kh.choi license.cms 파일에서 한 번만 읽어오면 계속 사용되도록 static 으로 선언
	CString GetSiteIndexData() {return m_strSiteIndex; };

private:
	CString m_strPasswordType;
};
