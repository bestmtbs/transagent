
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      Tray.h 
 @brief     Tray Class 정의 파일

            Tray Class base 클래스

 @author    odkwon
 @date      create 2011.05.06 
*/

#pragma once

#define WM_TRAY_NOTIFY WM_APP + 1000


class CTray;

/**
 * @ingroup   hAgentTray
 * @class     CTrayMsgRecevier
 * @brief     CTrayMsgRecevier Class \n
              CTray에 콜백메세지를 수신하는 클래스, explorer가 종료되었다 살아날때 이벤트 처리
 * 
 * @author    odkwon
 * @date      2008-05-20
*/
class CTrayMsgRecevier: public CWnd
{
    DECLARE_MESSAGE_MAP()
public:
    CTrayMsgRecevier();
    virtual ~CTrayMsgRecevier();    
    bool CreateWnd(CTray *_pTray);
protected:    
    afx_msg LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);
    virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
private:

    CTrayMsgRecevier(const CTrayMsgRecevier &_rTrayMsgRecevier);               /**< 복사 생성자 : 사용금지를 위해 private에 정의 */ 
    CTrayMsgRecevier& operator = (const CTrayMsgRecevier &_rTrayMsgRecevier);  /**< 대입 연산자 : 사용금지를 위해 private에 정의 */ 
    UINT m_nShellRestart;        /**< explorer.exe 시작시 수신받는 메세지 */      
    CTray *m_pTray;              /**< CTray참조 포인터 */    
};


/**
 @ingroup   hAgentTray
 @class     CTray
 @brief     CTray Class 
              트레이 아이콘을 위한 클래스\n
              다이얼로그 베이스 기반 sdi,mdi등 윈도우를 갖고 있는 프로그램에 트레이아이콘을 추가할때 사용한다.\n  
 @author    odkwon
 @date      2008-05-15
 @note      CTray를 상속받아 마우스이벤트클릭 함수를 구현하면 된다.\n
              - 기능\n
              1. 트레이 아이콘 생성\n
              2. 트레이 아이콘 변경\n
              3. ToolTip설정\n
              4. Balloon ToolTip 제공 (OS에 따라 기능이 동작하지 않을수 있다. 9x, NT에서는 지원하지않는다.)\n
              5. 트레이 아이콘 클릭시 메뉴 보임\n\n
              - 사용방법\n
              1. 메인 class에서 CTray를 상속받아 구현한 CAgentTray 멤버변수로 선언\n
              2. 메인 class에서 Init함수에서 CAgentTray.AddTrayIcon를 호출하여 트래이 아이콘을 생성한다.\n

              ex)\n
                1.CAgentTray m_Tray;\n
                2.m_Tray.Create(theApp.LoadIcon(IDR_MAINFRAME), _T("VMS에이전트"));\n
*/
class CTray  
{
    friend CTrayMsgRecevier;
public:
    CTray();
    virtual ~CTray();
    bool AddIcon(HICON _hIcon, LPCTSTR _szToolTip = _T(""));    

    bool SetIcon(HICON _hIcon);
    bool SetIcon(LPCTSTR _lpszIconName);
    bool SetIcon(UINT _nIDResource);

    bool SetTooltipText(LPCTSTR _pszTip);
    bool SetTooltipText(UINT _nID);    

    bool ShowBalloon(LPCTSTR _szBalloonTitle, LPCTSTR _szBalloonMsg, DWORD _dwIcon = NIIF_NONE,UINT _nTimeOut = 10);
	void SetWnd(HWND _hWnd);
protected:   
    bool ShowMenu(CMenu *_pContextMenu);
    virtual void OnTrayLButtonDbClick()  = 0;
    virtual void OnTrayLButtonDown()     = 0;
    virtual void OnTrayLButtonUp()       = 0;
    virtual void OnTrayRButtonDown()     = 0;
    virtual void OnTrayRButtonUp()       = 0;
private:
    CTray(const CTray &_rTray);                 /**< 복사 생성자 : 사용금지를 위해 private에 정의 */ 
    CTray& operator = (const CTray &_rTray);    /**< 대입 연산자 : 사용금지를 위해 private에 정의 */     
    bool ShowTray();
    void RemoveTrayIcon();    

    CTrayMsgRecevier m_TrayMsgRecevier;         /**< Tray아이콘 콜백메세지를 처리하는 클래스 */      
    NOTIFYICONDATA m_NotiIconData;              /**< Tray아이콘 정보를 담고있는 구조체 */  
};