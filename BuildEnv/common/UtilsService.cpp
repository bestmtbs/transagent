
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsService.cpp
 @brief     Service 제어 Utils

            Service 를 제어하는 유틸을 제공한다. 
 @author    odo kwon 
 @date      create 2011.03.17
*/

#include "stdafx.h"
#include "UtilsService.h"


/**
 @brief     서비스 시작 타입을 얻어온다.
 @author    odkwon
 @date      2008.03.17
 @param     [in]_sSvcName   서비스명
 @return    DWORD값의 서비스 시작 타입값
 @note      서비스 시작 타입값                 \n
            SERVICE_BOOT_START   = 0x00000000  \n
            SERVICE_SYSTEM_START = 0x00000001  \n
            SERVICE_AUTO_START   = 0x00000002  \n
            SERVICE_DEMAND_START = 0x00000003  \n
            SERVICE_DISABLED     = 0x00000004  \n 
 @note      서비스 시작 타입값의 내용은 http://msdn.microsoft.com/en-us/library/ms684950(VS.85).aspx 참고   
*/
DWORD GetServiceStartType(const CString &_sSvcName)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;       
    DWORD dwReturn = SERVICE_UNKNOWN;

    if (!CheckVaildServiceName(_sSvcName)) 
        return dwReturn;    
        
    schSCManager = ::OpenSCManager(NULL, NULL, GENERIC_READ);
    if (NULL == schSCManager)
        return dwReturn;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_QUERY_CONFIG);        
    if (NULL == schService)    
    {
        ::CloseServiceHandle(schSCManager);
        return dwReturn;
    } 

    DWORD dwNeedByte = 0;
    BOOL bQueryService = ::QueryServiceConfig(schService, NULL, 0, &dwNeedByte);
    if (FALSE == bQueryService)
    {
        DWORD dwstatus = ::GetLastError();
        if (ERROR_INSUFFICIENT_BUFFER != dwstatus) 
        {
            ::CloseServiceHandle(schService);
            ::CloseServiceHandle(schSCManager);
            return dwReturn;
        }
    }

    LPQUERY_SERVICE_CONFIG lpServiceConfig = reinterpret_cast<LPQUERY_SERVICE_CONFIG>(malloc(dwNeedByte));
    if (NULL == lpServiceConfig) 
    {    
        ::CloseServiceHandle(schService);
        ::CloseServiceHandle(schSCManager);        
        return dwReturn;
    }

    bQueryService = ::QueryServiceConfig(schService, lpServiceConfig, dwNeedByte, &dwNeedByte); 
    if (FALSE == bQueryService) 
    {
        free(lpServiceConfig);
        ::CloseServiceHandle(schService);
        ::CloseServiceHandle(schSCManager);
        return dwReturn;
    }

    dwReturn = lpServiceConfig->dwStartType;
    free(lpServiceConfig);

    ::CloseServiceHandle(schService);
    ::CloseServiceHandle(schSCManager);

    return dwReturn;
}


/**
 @brief     서비스가 등록되어 있는지 확인한다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false
*/
bool ExistsService(const CString &_sSvcName)
{

    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;

    if (!CheckVaildServiceName(_sSvcName)) 
        return false;    
    
    schSCManager = ::OpenSCManager(NULL, NULL, GENERIC_READ);
    if (NULL == schSCManager)
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_QUERY_STATUS);        
    if (NULL == schService)    
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    ::CloseServiceHandle(schService);
    ::CloseServiceHandle(schSCManager);

    return true;
}


/**
 @brief     서비스의 현재 상태값을 구한다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    서비스의 상태값                        \n
            SERVICE_STOPPED          = 0x00000001  \n
            SERVICE_START_PENDING    = 0x00000002  \n
            SERVICE_STOP_PENDING     = 0x00000003  \n
            SERVICE_RUNNING          = 0x00000004  \n
            SERVICE_CONTINUE_PENDING = 0x00000005  \n
            SERVICE_PAUSE_PENDING    = 0x00000006  \n
            SERVICE_PAUSED           = 0x00000007  \n
 @note      서비스 상태값의 내용은 http://msdn.microsoft.com/en-us/library/ms684950(VS.85).aspx 참고  
*/
DWORD GetServiceStatus(const CString &_sSvcName)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    SERVICE_STATUS ssStatus;
    ::ZeroMemory(&ssStatus, sizeof(SERVICE_STATUS));

    if (!CheckVaildServiceName(_sSvcName)) 
        return SERVICE_UNKNOWN;

    schSCManager = ::OpenSCManager(NULL, NULL, GENERIC_READ);
    if (NULL == schSCManager)
        return SERVICE_UNKNOWN;

    schService = ::OpenService(schSCManager, _sSvcName, SC_MANAGER_ENUMERATE_SERVICE);        
    if (NULL == schService)    
    {
        ::CloseServiceHandle(schSCManager);
        return SERVICE_UNKNOWN;
    }

    if (!::QueryServiceStatus(schService, &ssStatus))
    {
        ::CloseServiceHandle(schSCManager);
        return SERVICE_UNKNOWN;
    }

    ::CloseServiceHandle(schService);
    ::CloseServiceHandle(schSCManager);

    return ssStatus.dwCurrentState;
}

/**
 @brief     서비스를 등록한다.
 @author    odkwon
 @date      2008.03.17
 @param     [in]_sSvcFile        [in]서비스 파일명(Full Path)
 @param     [in]_sSvcName        [in]서비스명
 @param     [in]_sDisplayName    [in]서비스 표시명
 @param     [in]_sDescription    [in]서비스 설명
 @param     [in]_sDependent      [in]종속성 (서비스명으로 입력해야 하며 두개 이상일 경우 서비스명1\0서비스명2\0로 입력해야 한다.
 @param     [in]_dwServiceType   [in]서비스 Type 입력안할경우 수동으로 자동 설정된다.
 @return    true / false 
 @remarks   서비스 이름이 유효하지 않거나 이미 같은 서비스가 존재하면 실패처리를 한다.
*/
bool RegisterServices(const CString &_sSvcFile, const CString &_sSvcName, const CString &_sDisplayName, CString &_sDescription, CString &_sDependent ,DWORD _dwServiceType )
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    CWinOsVersion WinOS;
    
    if (WinOS.IsWin9x()) 
        return false;    

    if (!FileExists(_sSvcFile)) 
    {
		AfxMessageBox(_sSvcFile);
        return false;
    }

    if (!CheckVaildServiceName(_sSvcName)) 
    {
		AfxMessageBox(_T("파일 이름 실패"));
        ::SetLastError(ERROR_INVALID_NAME);
        return false;
    }

    if (MAXLEN_SERVICEDISPNAME < _sDisplayName.GetLength()) 
    {
        ::SetLastError(ERROR_INVALID_PARAMETER);
        return false;
    }


    if (MAXLEN_SERVICEDICRIPTION < _sDescription.GetLength()) 
    {
        ::SetLastError(ERROR_INVALID_PARAMETER);
        return false;
    }

    if (_T("") != _sDependent)
    {
        if (!CheckVaildServiceName(_sDependent)) 
        {
            ::SetLastError(ERROR_INVALID_PARAMETER);
            return false;     
        }
    }
    
    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
    if (NULL == schSCManager)
        return false;

    schService = ::CreateService(schSCManager, _sSvcName, _sDisplayName, SERVICE_ALL_ACCESS, 
                                SERVICE_WIN32_OWN_PROCESS, _dwServiceType, SERVICE_ERROR_NORMAL, 
                                _sSvcFile, NULL, NULL, /*_sDependent*/NULL, NULL, NULL); 
    if (NULL == schService)
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    SERVICE_DESCRIPTION srvdesc = {0, };
    srvdesc.lpDescription = _sDescription.GetBuffer(_sDescription.GetLength());
    _sDescription.ReleaseBuffer();

    fpChangeServiceConfig2 pChangeServiceConfig2 = NULL;
    HINSTANCE hModule = ::LoadLibrary(_T("Advapi32.dll"));
    if (NULL == hModule)
    {
        ::CloseServiceHandle(schService);
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    pChangeServiceConfig2 = reinterpret_cast<fpChangeServiceConfig2>(::GetProcAddress (hModule, CHANGESERVICECONFIG));
    if (NULL != pChangeServiceConfig2)
    {
        pChangeServiceConfig2(schService, SERVICE_CONFIG_DESCRIPTION, &srvdesc);
    }
    ::FreeLibrary(hModule);
    ::CloseServiceHandle(schService);
    ::CloseServiceHandle(schSCManager);

    return true;
}

/**
 @brief     서비스를 삭제한다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false 
*/

bool UnRegisterServices(const CString &_sSvcName, DWORD *_pdwErrorCode /*  NULL */)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    
    if (!CheckVaildServiceName(_sSvcName)) 
        return false;    
    if (!ExistsService(_sSvcName)) 
        return false;

    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (NULL == schSCManager) 
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, DELETE);        
    if (NULL == schService)    
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    StopService(_sSvcName);
    
    BOOL bResult = ::DeleteService(schService);

     if ((NULL !=_pdwErrorCode) && (!bResult))
       *_pdwErrorCode = ::GetLastError();

    ::CloseServiceHandle(schService);        
    ::CloseServiceHandle(schSCManager);

    return (TRUE == bResult);
}

/**
 @brief     서비스를 시작시킨다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false 
*/
bool RunService(const CString &_sSvcName, DWORD *_pdwErrorCode /*  NULL */)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;

   if (!CheckVaildServiceName(_sSvcName)) 
       return false;    
   if (!ExistsService(_sSvcName)) 
       return false;

   if (SERVICE_RUNNING == GetServiceStatus(_sSvcName)) 
       return true;

    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (NULL == schSCManager)
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_ALL_ACCESS);
    if (NULL == schService)
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }
    
    BOOL bResult = ::StartService(schService, 0, NULL);
    if ((NULL !=_pdwErrorCode) && (!bResult))
       *_pdwErrorCode = ::GetLastError();

    if ((FALSE == bResult) && (SERVICE_PAUSED == GetServiceStatus(_sSvcName))) 
        SetControlCodeToService(_sSvcName, SERVICE_CONTROL_CONTINUE);

    INT nPenddingCount = 0;
    while (SERVICE_START_PENDING == GetServiceStatus(_sSvcName) && (nPenddingCount < SERVICE_WAITTIME)) 
    {
        nPenddingCount++;
        Sleep(1000);
    }

    if (SERVICE_RUNNING == GetServiceStatus(_sSvcName))
        bResult = TRUE;
    else
        bResult = FALSE;
    
    ::CloseServiceHandle(schService);        
    ::CloseServiceHandle(schSCManager);
    
    return (TRUE == bResult);   
}

/**
 @brief     서비스를 중지시킨다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false 
*/
bool StopService(const CString &_sSvcName, DWORD *_pdwErrorCode /*  NULL */)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    //bool bRet = false;

    if (!CheckVaildServiceName(_sSvcName)) 
        return false;
    if (!ExistsService(_sSvcName)) 
        return false;

    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (NULL == schSCManager)
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_ALL_ACCESS);

    if (NULL == schService)
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    INT nPenddingCount = 0;
    SERVICE_STATUS ssStatus;
    ::ZeroMemory(&ssStatus, sizeof(SERVICE_STATUS));

    bool bResult = false;
    bResult = (TRUE == ::ControlService(schService, SERVICE_CONTROL_STOP, &ssStatus));
    if (false == bResult)
    {
        if (NULL !=_pdwErrorCode)
            *_pdwErrorCode = ::GetLastError();

        ::CloseServiceHandle(schService);
        ::CloseServiceHandle(schSCManager);
        if (ssStatus.dwCurrentState | SERVICE_ACCEPT_SHUTDOWN)
        {
            bResult = StopHauriService(_sSvcName);
        }
        return bResult;
    }

    ::ZeroMemory(&ssStatus, sizeof(SERVICE_STATUS));
    ::QueryServiceStatus(schService, &ssStatus);
    while (SERVICE_STOP_PENDING == ssStatus.dwCurrentState && (nPenddingCount < SERVICE_WAITTIME)) 
    {
        nPenddingCount++;
        ::Sleep(1000);
        if (FALSE == ::QueryServiceStatus(schService, &ssStatus))
            break;
    }
    
    bResult = (SERVICE_STOPPED == ssStatus.dwCurrentState);

    ::CloseServiceHandle(schService);        
    ::CloseServiceHandle(schSCManager);

    return bResult;
}


/**
 @brief     서비스를 일시중지시킨다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false 
*/
bool PauseService(const CString &_sSvcName, DWORD *_pdwErrorCode /*  NULL */)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    bool bRet = false;

    if (!CheckVaildServiceName(_sSvcName)) 
        return false;    
    if (!ExistsService(_sSvcName)) 
        return false;    

    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (NULL == schSCManager)
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_ALL_ACCESS);        
    if (NULL == schService)
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    // 서비스가 실행중이라면 중지한다.
    SERVICE_STATUS ssStatus;
    ::ZeroMemory(&ssStatus, sizeof(SERVICE_STATUS));
    if (TRUE == ::ControlService(schService, SERVICE_CONTROL_PAUSE, &ssStatus)) 
    {    
        ::Sleep(1000);    
        while(::QueryServiceStatus(schService, &ssStatus)) 
        {
            if (SERVICE_PAUSE_PENDING == ssStatus.dwCurrentState)
                ::Sleep(1000);
            else 
                break;
        }
                    
        if (SERVICE_PAUSED == ssStatus.dwCurrentState) 
            bRet = true;
        else 
            bRet = false;
    }    

    if ((NULL !=_pdwErrorCode) && (!bRet))
       *_pdwErrorCode = ::GetLastError();

    ::CloseServiceHandle(schService);
    ::CloseServiceHandle(schSCManager);

    return bRet;
    
}

/**
 @brief     하우리 서비스를 중지시킨다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false 
*/
bool StopHauriService(const CString &_sSvcName)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    
    if (!ExistsService(_sSvcName)) 
        return false;

    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (NULL == schSCManager)
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_ALL_ACCESS);        
    if (NULL == schService)    
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    bool bResult = false;
    SERVICE_STATUS ssStatus;
    ::ZeroMemory(&ssStatus, sizeof(SERVICE_STATUS));
    bResult = (TRUE == ::ControlService(schService, SERVICE_CONTROL_HAURISVC_STOP, &ssStatus));
    if (bResult)
    {
        int nPenddingCount = 0;
        ::QueryServiceStatus(schService, &ssStatus);
        while ((SERVICE_STOP_PENDING == ssStatus.dwCurrentState) && (nPenddingCount < SERVICE_WAITTIME)) 
        {
            nPenddingCount++;
            Sleep(1000);
            ::QueryServiceStatus(schService, &ssStatus);
        }
        bResult = (SERVICE_STOPPED == ssStatus.dwCurrentState);
    }

    ::CloseServiceHandle(schService);        
    ::CloseServiceHandle(schSCManager);
    
    return bResult;
}


/**
 @brief     서비스에 Controll code를 전송한다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName      서비스명
@param      [in] _dwControlCode 서비스 코드
 @return    true / false 
*/
bool SetControlCodeToService(const CString &_sSvcName,const DWORD _dwControlCode, DWORD *_pdwErrorCode /*  NULL */)
{
    SC_HANDLE schService = NULL;
    SC_HANDLE schSCManager = NULL;
    
    if (!ExistsService(_sSvcName)) 
        return false;

    schSCManager = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (NULL == schSCManager)
        return false;

    schService = ::OpenService(schSCManager, _sSvcName, SERVICE_ALL_ACCESS);        
    if (NULL == schService)    
    {
        ::CloseServiceHandle(schSCManager);
        return false;
    }

    SERVICE_STATUS ssStatus;
    ::ZeroMemory(&ssStatus, sizeof(SERVICE_STATUS));
    BOOL bResult = ::ControlService(schService, _dwControlCode, &ssStatus);

    if ((NULL !=_pdwErrorCode) && (!bResult))
        *_pdwErrorCode = ::GetLastError();

    ::CloseServiceHandle(schService);
    ::CloseServiceHandle(schSCManager);
    
    return (TRUE == bResult);
}


/**
 @brief     서비스 이름의 유효성을 검사한다.
 @author    odkwon
 @date      2008.03.17
 @param     [in] _sSvcName  서비스명
 @return    true / false 
 @note      서비스 이름 유효성 체크 기준은 CreateService Function의 두번째 파라미터
            (lpServiceName)의 기준을 적용. \n
            서비스 이름의 최대 길이는 182(NULL 종료문자 제외)로 제한하며 NT OS의 경우 80(NULL 종료문자 제외)으로 제한하고 대소문자를 구분하지 않는다. \n
            서비스 이름에 forward-slash(/), back-slash(\)가 존재하면 않된다.  \n
            자세한 내용은 http://msdn.microsoft.com/en-us/library/ms682450(VS.85).aspx 참고.\n
            MSDN에서는 서비스이름을 최대 256까지 지원한다고 하지만 \n
            실제 서비스등록 Test를 한결과 등록은 되지만 시작, 속상창을 띄울때 문제가 발생한다\n 
            실제 Test결과 서비스 이름의 최대 길이는 182(NULL 종료문자 제외)로 제한하며 NT OS의 경우 80(NULL 종료문자 제외)으로 제한한다.\n

*/
bool CheckVaildServiceName(const CString &_sServiceName)
{    
    if (_T("") == _sServiceName) 
        return false;

    CWinOsVersion OSVersion;
    if (osWinNT == OSVersion.GetProduct()) 
    {
        if (MAXLEN_NTSERVICENAME < _sServiceName.GetLength()) 
            return false;
    }
    else
    {
        if (MAXLEN_SERVICENAME < _sServiceName.GetLength()) 
            return false;
    }

    if (-1 != _sServiceName.Find(_T("/"))) 
        return false;    
    if (-1 != _sServiceName.Find(_T("\\")))
        return false;

    return true;
}


//서비스 이벤트로그 등록
/**
 @brief     서비스 이벤트로그 reg값을 등록한다.서비스 이벤트로그를 기록하려면\n
            eventlog\application\서비스명에 reg값을 기록해야한다.
 @author    odkwon
 @date      2008.07.01
 @param     [in] _sServiceName      서비스명
 @return    true / false 
*/
bool RegisterServiceEventLog(const CString &_sServiceName, const CString &sMsgFile)
{
    bool bRet = false;
    if (!CheckVaildServiceName(_sServiceName)) 
        return false;
    bRet =  AddRegKey(HKEY_LOCAL_MACHINE, SERVICE_EVENTLOGREG, _sServiceName);
    if (bRet)
    {
        CString sKey = _T("");
        CString sTmpServiceName = _T("");
        sTmpServiceName = _sServiceName;
        sKey.Format(_T("%s\\%s"), SERVICE_EVENTLOGREG, sTmpServiceName.GetBuffer());
        sTmpServiceName.ReleaseBuffer();
        bRet = SetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("EventMessageFile"), sMsgFile);
        bRet = SetRegDWORDValue (HKEY_LOCAL_MACHINE, sKey, _T("TypesSupported"), EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE);
    }
    return bRet;
}
/**
 @brief     서비스 이벤트로그 reg값을 삭제한다.\n
            서비스 이벤트로그를 기록하려면 eventlog\application\서비스명에 reg값을 삭제해야한다.
 @author    odkwon
 @date      2008.07.01
 @param     [in] _sServiceName      서비스명
 @return    true / false 
*/
bool UnRegisterServiceEventLog(const CString &_sServiceName)
{
    if (!CheckVaildServiceName(_sServiceName)) 
        return false;
    CString sDelKey = _T("");
    CString sTmpServiceName = _T("");
    sTmpServiceName = _sServiceName;
    sDelKey.Format(_T("%s\\%s"), SERVICE_EVENTLOGREG, sTmpServiceName.GetBuffer());
    sTmpServiceName.ReleaseBuffer();
    return DelRegKey(HKEY_LOCAL_MACHINE, sDelKey);
}


/**
 @brief     서비스 이벤트로그를 추가한다.
            서비스 이벤트로그를 기록하려면 eventlog\application\서비스명에 reg값을 삭제해야한다.
 @author    odkwon
 @date      2008.07.01
 @param     [in] _sServiceName      서비스명 
 @param     [in] _wEventType        이벤트 타입(EVENTLOG_ERROR_TYPE, EVENTLOG_WARNING_TYPE, EVENTLOG_INFORMATION_TYPE)
 @param     [in] _dwEventID         이벤트 아이디
 @param     [in] _wCategory         카테고리
 @param     [in] _plpStrings        이벤트 로그 string에 추가될 파라메터
 @param     [in] _wNumlpStrings     이벤트 로그 string에 추가될 파라메터 카운트
 @return    true / false 
*/
bool AddServiceEventLog(const CString &_sServiceName, const DWORD _dwEventID,  const WORD _wEventType /* =  EVENTLOG_SUCCESS */,
                        const WORD _wCategory /* = 0 */, LPCTSTR* _plpStrings /* = NULL */, WORD _wNumlpStrings /* = 0 */)
{
    if (!CheckVaildServiceName(_sServiceName)) 
        return false;

    BOOL bSuccess = FALSE;    
    HANDLE hEventLog = ::RegisterEventSource( NULL, _sServiceName);
    if (NULL != hEventLog)
    {
        bSuccess = ::ReportEvent(hEventLog, _wEventType, _wCategory, _dwEventID, NULL, _wNumlpStrings,	0, _plpStrings,	NULL);
        ::DeregisterEventSource(hEventLog);
    }
    return (TRUE == bSuccess);
}