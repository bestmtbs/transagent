
/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.05.29		
********************************************************************************/
/**
@file      CParseUtil.cpp 
@brief     CParseUtil  구현 파일  
@author   jhlee
@date      create 2011.08.09
*/
#include "stdafx.h"
#include "utilParse.h"
#include <Psapi.h>

/********************************************************************************
@class     CParseUtil
@brief      문자열 파싱
@note       파싱하고자 하는 문자열을 입력해주면 그 문자열로 파싱하여 배열에 추가됨
*********************************************************************************/

/**
@brief     ParseStringToStringArray
@author   JHLEE
@date      2011.08.09
@param   [IN]pszDiv 파싱 문자
@param   [IN]strLog 파싱할 문자열
@param   [OUT]pstrArrLog 파싱 결과값
@note      특정 문자로 파싱하여 결과값을 얻고자 할때 이 함수를 사용
*/

void CParseUtil::ParseStringToStringArray(TCHAR* _pszDiv, CString _strLog, CStringArray* _pstrArryLog)
{
	int nFindFirst = 0, nFindNext;
	int nDivLen = _tcslen(_pszDiv);

	CString strTmp;

	while ( ( nFindNext = _strLog.Find( _pszDiv, nFindFirst ) ) >= 0 )
	{
		strTmp = _strLog.Mid( nFindFirst, nFindNext - nFindFirst );
		_pstrArryLog->Add(strTmp);
		nFindFirst = nFindNext + nDivLen;
	}

	if(nFindFirst < _strLog.GetLength())
	{
		strTmp = _strLog.Mid( nFindFirst, _strLog.GetLength() - nFindFirst);
		_pstrArryLog->Add(strTmp);
	}
}


/**
@brief     RemoveSpecialCharacterForQuery
@author	   sy.choi
@date      2016.05.20
@note      DB 쿼리 실행 시 특수문자 제거
*/
CString CParseUtil::RemoveSpecialCharacterForQuery(CString _strName)
{
	CString strReName = _T("");
	strReName = _strName;
	strReName.Remove(_T('\''));
	strReName.Remove(_T('\"'));
	strReName.Remove(_T('/'));
	strReName.Remove(_T('\\'));
	strReName.Remove(_T('|'));
	strReName.Remove(_T(','));

	return strReName;
}

CString CParseUtil::ReplaceSpecialCharacterForQuery(CString _strName)
{
	CString strReName = _T("");
	strReName = _strName;
	strReName.Replace('+', '-');
	strReName.Replace('/', '_');
	return strReName;
}