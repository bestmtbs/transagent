/**
 @file      MemFileEx.cpp 
 @brief     Stream 사용을 위한 클래스.  
 @author    hang ryul lee
 @date      

 @note      2008.04.02 yunmi\n
            - 스트림을 통째로 파일로 저장할 경우 유니코드, 안시코드 파일공유 안됨.\n
            - 스트림 자체를 안시코드 기반으로 변경.\n
            - Read/WriteString 함수내에서 유니코드일경우 Ansi 코드로 변환하는 로직 추가.\n
 @note      2008.04.03 yunmi\n
            - 스트림은 Unicode 기반으로 구성되어야함. (서버와 Data 통신기반이 스트림)\n
            - Write/ReadString 함수를 Unicode 기반으로 하고,\n
              Ansi코드 용 따로 작성.\n
 @note      2008.04.07 yunmi\n
            - Unicode 스트림을 그대로 파일로 저장할 경우 글자 제대로 저장안됨\n
            - Unicode로 구성된 Stream을 파일로 저장할 때 Ansi로 전환 안됨.\n
            - 표준 Unicode 파일 저장 Lib가 제공될때까지 File read/write 기능 사용 중단.\n
            - Unicode로 스트림 저장된 경우 unicode 스트링이 제대로 전달은 되지만 \n
              컨트롤에 제대로 출력안됨.(ListBox, Editbox, RichEditbox 확인)
 @note        2008.04.17 yunmi \n
            - 사용상의 편의 위해 default 값 = NULL 제공.
            - CString & 파라미터를 CString *로 변경하려 하였으나 그럴경우 ReadString(NULL)로 호출했을때 \n
              ReadString(LPTSTR *)와 ReadString(CString *) 함수사이에 모호함 발생함.\n
            - LPTSTR *파라미터를 갖는 함수에 default 값제공.\n
            - ReadStringA2T함수의 경우도 ReadString함수와 동일하게 사용하기 위해 LPTSTr *를 파라미터로 갖는\n
              함수 오버로딩함.\n
 */

#include "StdAfx.h"
#include "afxconv.h"
#include "MemFileEx.h"


#define MAX_LEN 255

/**
 @brief     생성자
 @author    hang ryul lee
 @date      
 @param     [in] nGrowBytes : 사용할 스트림 사이즈(default = 1024)
*/
CMemFileEx::CMemFileEx(UINT _nGrowBytes)
{
    CMemFile::CMemFile(_nGrowBytes);
}

/**
 @brief     생성자
 @author    hang ryul lee
 @date
 @param     [in] _lpBuffer : 초기화할 내용이 들은 buffer
 @param     [in] _nBufferSize : 버퍼 사이즈
 @param     [in] _nGrowBytes : 추가로 사용할 스트림 사이즈 (default = 0)
*/
CMemFileEx::CMemFileEx(BYTE* _lpBuffer, UINT _nBufferSize, UINT _nGrowBytes)
{
    CMemFile::CMemFile(_lpBuffer, _nBufferSize, _nGrowBytes);
}

/**
 @brief     메모리의 포인터를 얻음
 @author    hang ryul lee
 @date
 @return    구성되어있는 스트림 메모리의 포인터
*/
BYTE* CMemFileEx::GetBuffer()
{
    return reinterpret_cast<BYTE *>(m_lpBuffer);
}

/**
 @brief     파일에서 값 읽어옴
 @author    hang ryul lee
 @date
 @param     [in] _sFilePath : 읽어올 파일 이름
 @return    true / false
*/
bool CMemFileEx::ReadFromFile(const CString &_sFilePath)
{
    BYTE *byte = NULL;

    CFile File;
    if (!File.Open(_sFilePath, CFile::modeRead | CFile::shareDenyWrite)) return false;
    
    int nSize = static_cast<int>(File.GetLength());
    if (0 >= nSize) return false;

    byte = new BYTE[nSize];
    File.Read(reinterpret_cast<LPVOID>(byte),static_cast<UINT>(nSize));

    CMemFile::Write(byte, nSize);

    delete [] byte;
    File.Close();

    return true;
}

/**
 @brief     스트림 내용을 파일에 씀
 @author    hang ryul lee
 @date
 @param     [in] _sFilePath : 읽어올 파일 이름
 @return    true / false
*/
bool CMemFileEx::WriteToFile(const CString &_sFilePath)
{
    CFile File;
    if (!File.Open(_sFilePath, CFile::modeCreate | CFile::modeWrite))    return false;
    
    int nSize = static_cast<int>(CMemFile::GetLength());
    if (0>= nSize)    return false;

    File.Write(GetBuffer(), nSize);

    File.Close();
    return true;
}

/**
 @brief     스트림에서 Byte 읽음
 @author    hang ryul lee
 @date
 @param     [out] _lpBuf : 읽어온 byte 값 저장
*/
void CMemFileEx::ReadByte(BYTE *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(BYTE));
}

/**
 @brief     스트림에서 int 읽음
 @author    hang ryul lee
 @date
 @param     [out] _lpBuf : 읽어온 int 값 저장
*/
void CMemFileEx::ReadInt(INT *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(INT));
}

/**
 @brief     스트림에서 int64 읽음
 @author    hang ryul lee
 @date      2008.09.09
 @param     [out] _lpBuf : 읽어온 int64 값 저장
*/
void CMemFileEx::ReadInt64(INT64 *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(INT64));
}

/**
 @brief     스트림에서 WORD 읽음
 @author    hang ryul lee
 @date
 @param     [out] _lpBuf : 읽어온 WORD 값 저장
*/
void CMemFileEx::ReadWORD(WORD *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(WORD));
}

/**
 @brief     스트림에서 DWORD 읽음
 @author    hang ryul lee
 @date
 @param     [out] _lpBuf : 읽어온 DWORD 값 저장
*/
void CMemFileEx::ReadDWORD(DWORD *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(DWORD));
}

/**
 @brief     스트림에서 Double 읽음
 @author    hang ryul lee
 @date
 @param     [out] _lpBuf : 읽어온 Double 값 저장
*/
void CMemFileEx::ReadDouble(double *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(double));
}

/**
 @brief     스트림에서 bool을 읽음
 @author    odkwon
 @date
 @param     [out] bool : 읽어온 bool 값 저장
*/
void CMemFileEx::Readbool(bool *_lpBuf)
{
    CMemFile::Read(_lpBuf, sizeof(bool));
}


/**
 @brief     스트림에서 string 읽음
 @author    hang ryul lee
 @date      
 @param    [out] lpBuf : 저장될 버퍼
 @note      -2008.04.03 yunmi\n
             .strsafe 함수로 변경하면서 필요한 파라미터 추가 (_nLength)\n
             .Ansi->Uni code로 변경 로직 추가.\n
 @note      -2008.04.07 yunmi \n
             .다국어지원위해 다시 유니코드 기반으로 변경
 @note        -2008.04.17 yunmi \n
             .불필요한 값일 경우를 위해 default 값 NULL 주어짐. \n
             .lpBuf가 NULL일경우 해당 사이즈만큼 메모리 포인터만 이동.
*/
void CMemFileEx::ReadString(LPTSTR _lpBuf)
{
    BYTE nLen = 0;
    ReadByte(&nLen);
    if (NULL == _lpBuf)
    {
        TCHAR szBuf[MAX_LEN+1] = { 0, };
        CMemFile::Read(szBuf, nLen*sizeof(TCHAR));
    }
    else
    {
        CMemFile::Read(_lpBuf, nLen*sizeof(TCHAR));
    }
}

/**
 @brief     스트림에서 string 읽음
 @author    hang ryul lee
 @date      
 @param    [out] _sStr : 저장될 버퍼
 @note     사용상 편의를 위해 ReadString 함수오버로딩.
 @note     해당 함수를 통해 CString 형태로 편하게 사용 가능.
 @note     -2008.04.17 yunmi\n
           .사용하지 않을 때 Param 값 없이 Mem포인터만 이동하기 위해 Param을 Cstring * 형으로 변경하려 하였으나 \n
             그럴 경우 ReadString(NULL) 의 형식으로 함수 호출시\n
             ReadString(CString *_sStr) 함수와 ReadString(LPTSTR *_lpBuf) 사이에 모호함 발생.
           .그대로 &형태로 유지.
*/
void CMemFileEx::ReadString(CString &_sStr)
{
    TCHAR szBuffer[MAX_LEN+1] = { 0, };
    ReadString((szBuffer));
        
    _sStr = szBuffer;
}

/**
 @brief     스트림에서 Ansi String을 Unicode로 읽음
 @author    hang ryul lee
 @date      2008.04.15
 @param     [out]    _sStr : 읽은 문자열
 @note      2008.04.17 yunmi\n
            - param을 CString *로 변경하려 하였으나, ReadString과 사용형식을 맞추기 위해,\n
              그대로 &로 둠. -> LPTSTR *형 받는 함수 오버로딩.
*/
void CMemFileEx::ReadStringA2T(CString &_sStr)
{
    TCHAR szBuf[MAX_LEN+1] = { 0, };
    ReadStringA2T((szBuf));

    _sStr = szBuf;
}

/**
 @brief     스트림에서 Ansi String을 Unicode로 읽음
 @author    hang ryul lee
 @date      2008.04.15
 @param     [out] _lpBuf : 읽은 문자열
 @note        2008.04.17 yunmi\n
            - 파라미터에 default값으로 NULL 주어짐.\n
            - 사용하지 않는 값일 경우 NULL로 파라미터를 주면, 해당 스트링 만큼 그냥 읽고 넘김.
*/
void CMemFileEx::ReadStringA2T(LPTSTR _lpBuf)
{
    char szBuf[MAX_LEN + 1] = {0, };
    BYTE nLen = 0;
    ReadByte(&nLen);
    CMemFile::Read(szBuf, nLen);

    if (NULL == _lpBuf) return;

    USES_CONVERSION_EX;
    LPTSTR szStr = A2T_EX(szBuf, static_cast<UINT>(strlen(szBuf)));

    if (NULL != szStr)
        _tcscpy_s(_lpBuf, MAX_LEN, szStr);
}

/**
 @brief     스트림에서 Unicode String을 Ansi로 읽음
 @author    hang ryul lee
 @date      2008.04.15
 @param     [out]_lpBuf : 읽은 Ansi 문자열
 @note      - 2008.04.17 yunmi        \n
             .사용상의 편의 위하여 lpBuf에 default값 주어짐 (NULL) \n
             .lpBuf가 NULL일 경우 사이즈만큼 메모리 포인터만 이동\n
*/
void CMemFileEx::ReadStringT2A(LPSTR _lpBuf)
{
    TCHAR szBuf[MAX_LEN + 1] = {0, };
    BYTE nLen = 0;
    ReadByte(&nLen);
    CMemFile::Read(szBuf, nLen * sizeof(TCHAR));

    if (NULL == _lpBuf) return;

    USES_CONVERSION_EX;
    LPSTR szStr = T2A_EX(szBuf, static_cast<UINT>(_tcslen(szBuf)));

    if (NULL != szStr)
        strcpy_s(_lpBuf, MAX_LEN, szStr); 
}

/**
 @brief     스트림에서 CStringList 읽음
 @author    hang ryul lee
 @date
 @param     [out] _pStrList : 읽어온 CStringList 값 저장
*/
void CMemFileEx::ReadStrings(CStringList *_pStrList)
{
    if (NULL == _pStrList)
        return;

     int nCount =0;
     ReadInt(&nCount);
     for (INT i = 0; i < nCount; i++)
     {
        CString sTmp =_T("");
        TCHAR    szTmp[MAX_LEN] = {0,};
    
        ReadString((szTmp));
        sTmp = szTmp;
        _pStrList->AddTail(sTmp);
     }
}



/**
 @brief     Steam 데이타를 MemFileEx에 부터 읽어드린다.\n
 @author    odkwon
 @date      2008-05-26
 @param     [in]_rStream CMemFile Stream Data를 _rStream로 read한다.
 */
void CMemFileEx::ReadStream(CMemFileEx &_rStream)
{
    BYTE nLen = 0;
    ReadByte(&nLen);
    _rStream.SetLength(nLen);
    CMemFile::Read(_rStream.GetBuffer(), nLen);
}

/**
 @brief     스트림에서 COleDateTime값을 읽는다.
 @author    hang ryul lee
 @date      2008.05.28
 @param     [out] _dtBuf : COleDateTime 포인터
 @note      _dtBuf가 NULL일 경우 스트림에서 COleDateTime 사이즈만큼 포인터만 이동한다.
*/
void CMemFileEx::ReadDateTime(COleDateTime *_dtBuf)
{
    if (NULL == _dtBuf)
    {
        COleDateTime dtTemp;
        CMemFile::Read(&dtTemp, sizeof(DATE));
    }
    else
    {
        CMemFile::Read(_dtBuf, sizeof(DATE)); 
    }
}

/**
 @brief     스트림에 Byte 씀
 @author    hang ryul lee
 @date
 @param     [in] _bValue : 스트림에 저장할 Byte값
*/
void CMemFileEx::WriteByte(BYTE _bValue)
{
    CMemFile::Write(&_bValue, sizeof(BYTE));
}

/**
 @brief     스트림에 INT 씀
 @author    hang ryul lee
 @date
 @param     [in] _nValue : 스트림에 저장할 INT값
*/
void CMemFileEx::WriteInt(INT _nValue)
{
    CMemFile::Write(&_nValue, sizeof(INT));
}

/**
 @brief     스트림에 INT64 씀
 @author    hang ryul lee
 @date
 @param     [in]_nValue : 스트림에 저장할 INT64값
*/
void CMemFileEx::WriteInt64(INT64 _nValue)
{
    CMemFile::Write(&_nValue, sizeof(INT64));
}

/**
 @brief     스트림에 WORD 씀
 @author    hang ryul lee
 @date
 @param     [in]_wValue : 스트림에 저장할 WORD값
*/
void CMemFileEx::WriteWORD(WORD _wValue)
{
    CMemFile::Write(&_wValue, sizeof(WORD));
}

/**
 @brief     스트림에 DWORD 씀
 @author    hang ryul lee
 @date
 @param     [in]_dwValue : 스트림에 저장할 DWORD값
*/
void CMemFileEx::WriteDWORD(DWORD _dwValue)
{
    CMemFile::Write(&_dwValue, sizeof(DWORD));
}


/**
 @brief     스트림에 Double 씀
 @author    hang ryul lee
 @date
 @param     [in]_dwValue : 스트림에 저장할 Double값
*/
void CMemFileEx::WriteDouble(double _dValue)
{
    CMemFile::Write(&_dValue, sizeof(double));
}



/**
 @brief     스트림에 string 씀
 @author    hang ryul lee
 @date      
 @param     [in] _szValue : wirte할 문자열
 @note      -2008.04.03 yunmi\n
             .strsafe 함수로 변경.
             .Uni-> Ansi code로 변경 로직 추가.
 @note      -2008.04.07 yunmi \n
             .다국어지원위해 다시 유니코드 기반으로 변경
*/
void CMemFileEx::WriteString(const CString &_szValue)
{
    BYTE nLen;
    TCHAR buf[MAX_LEN + 1] = {0, };
    INT nStr = _szValue.GetLength();

    if (_T("") == _szValue)
    {
        nLen = 0;
        buf[0] = '\0';
    }
    else if (nStr > MAX_LEN)
    {
        nLen = MAX_LEN;
        _tcsncpy_s(buf, _szValue, MAX_LEN);
    }
    else
    {
        nLen = static_cast<BYTE>(nStr);
        _tcsncpy_s(buf, _szValue, nLen);
    }

    WriteByte(nLen);

    Write(buf, static_cast<UINT>(nLen)*sizeof(TCHAR));
}

/**
 @brief     스트림에 Ansi String을 Unicode로 씀
 @author    hang ryul lee
 @date      2008.04.15
 @param     [in] _szValue : 쓸 Ansi 문자열
*/
void CMemFileEx::WriteStringA2T(LPSTR _szValue)
{    
    size_t nStr = 0;
    nStr = strlen(_szValue);

    USES_CONVERSION_EX;
    LPTSTR szBuf = A2T_EX(_szValue, static_cast<UINT>(nStr));

    WriteString(szBuf);
}

/**
 @brief     스트림에 Unicode String을 Ansi 로 씀
 @author    hang ryul lee
 @date      2008.04.15
 @param     [in] _szValue : 쓸 Unicode 문자열
*/
void CMemFileEx::WriteStringT2A(const CString &_szValue)
{
    BYTE nLen;
    TCHAR buf[MAX_LEN+1] = {0, };
    INT nStr = _szValue.GetLength();

    if (_T("") == _szValue)
    {
        nLen = 0;
        buf[0] = '\0';
    }
    else if (nStr > MAX_LEN)
    {
        nLen = static_cast<BYTE>(nStr);
        _tcsncpy_s(buf, _szValue, MAX_LEN);
    }
    else
    {
        nLen = static_cast<BYTE>(nStr);
        _tcsncpy_s(buf, _szValue, nLen);
    }

    CString sValue = _szValue;
    USES_CONVERSION_EX;
    LPSTR szStr = T2A_EX(sValue.GetBuffer(0), nLen);
    sValue.ReleaseBuffer();

    if (NULL != szStr)
    {
        nStr = static_cast<INT>(strlen(szStr));
        WriteByte(static_cast<BYTE>(nStr));
        Write(szStr, static_cast<UINT>(nStr));
    }
    else
    {
        WriteByte(0);
        Write(NULL, 0);
    }
}

/**
 @brief     스트림에 CStringList 씀
 @author    hang ryul lee
 @date
 @param     [in] _StrList : 스트림에 저장할 CStringList값
*/
void CMemFileEx::WriteStrings(const CStringList &_StrList)
{
    WriteInt((INT)_StrList.GetCount());
    POSITION pos = _StrList.GetHeadPosition();
    while (NULL != pos)
    {
        CString sStr = _StrList.GetNext(pos);
        WriteString(sStr);
    }
}


/**
 @brief     Stream 데이타를 MemFileEx에 쓰는 함수\n
            최대 255(Byte 최대값)의 제한을 갖는다.
 @author    odkwon
 @date      2008-05-26
 @param     [in]_pStream _pStream을 CMemFile에 기록한다.
 @note      파라미터로 주어진 _pStream이 NULL이면 빈 Stream을 기록한다.
 */
void CMemFileEx::WriteStream(CMemFileEx *_pStream)
{
    BYTE nLen;
    if (NULL == _pStream) 
    {
        nLen = 0;
        _pStream = new CMemFileEx;
    } 
    if (MAX_LEN < _pStream->GetLength()) return;
    nLen = static_cast<BYTE>(_pStream->GetLength());
    WriteByte(nLen);
    Write(_pStream->GetBuffer(), nLen);
}

/**
 @brief     스트림에서 COleDateTime값을 쓴다.
 @author    hang ryul lee
 @date      2008.05.28
 @param     [in] _rValue : COleDateTime 값
*/
void CMemFileEx::WriteDateTime(COleDateTime &_rValue)
{
    CMemFile::Write(&_rValue, sizeof(DATE));
}

/**
 @brief     스트림에서 bool값을 쓴다.
 @author    odkwon
 @date      2008.09.11
 @param     [in] _bValue : bool 값
*/
void CMemFileEx::Writebool(bool _bValue)
{
    CMemFile::Write(&_bValue, sizeof(bool));
}
