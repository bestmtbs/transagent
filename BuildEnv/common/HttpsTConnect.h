#pragma once

//#include <winhttp.h>

#include <afxinet.h>
//#include <wininet.h>



class CHttpsTConnect
{
public:
	CHttpsTConnect(void);
	~CHttpsTConnect(void);

	
	public:
	 CString m_strDomain;
	 CString m_strUri;
	 BOOL m_bGet;
	 BOOL HttpConnect( CString _strURI=L"") ;
	 void   Clear();
	 virtual CString HttpReadRequest(CString _strParameter);
	 void SetServerInfo(CString _strDomain);
	 DWORD GetErrorCode() {return m_LastErrorCode;}
	 void SetTokenFromAgent(CString _strTokenA);
	 CString GetTokenFromAgent();
	 CString GetErrorMsg(){return m_ErrorMsg;}


private:

	CInternetSession*  m_pSession;
	CHttpConnection*   m_pConnection;
	CHttpFile*			m_pHttp;

	DWORD       m_LastErrorCode;
	CString     m_ErrorMsg;
	CString	m_strToken;

	CStringA    Utf8_Encode(CStringW strData);
	CStringW    Utf8_Decode(CStringA strData);

	INTERNET_PORT	m_nPort;


	//CString     Utf8ToUnicode(LPCSTR strData,int nRecvLen);


	char*        m_pData;
};
