#pragma once
#include "..\..\SQLite\TBAgentInfo.h"
#include "..\..\SQLite\TBUserInfo.h"
#include "..\..\SQLite\TBSendNcviList.h"
#include "..\..\SQLite\TBSendNkfiList.h"
#include "..\..\SQLite\TBSendLogList.h"
#include "..\..\SQLite\TBCollectPolicy.h"
#include "..\..\SQLite\TBNotice.h"
#include "..\..\SQLite\TBStatePolicy.h"
#include "..\..\SQLite\TBUrlPolicy.h"
#include "..\..\SQLite\TBServerInfo.h"
//#include "..\include\CommonDefine.h"
//#define DB_WRITE_TRY_CNT	5
#define DB_WRITE_TRY_CNT	10	// 2017-05-17 kh.choi 일단 10번으로 교체

class CCmsDBManager
{
public:
	CCmsDBManager(void);
	CCmsDBManager(CString _strType);
	virtual ~CCmsDBManager(void);

	BOOL CreateQuery();
	BOOL ExecQuery(CString _strQuery);
	BOOL InsertQuery(CString _strQuery, int nTryCnt = DB_WRITE_TRY_CNT );
	BOOL UpdateQuery(CString _strQuery, int nTryCnt = DB_WRITE_TRY_CNT);
	BOOL SelectAllQuery(CString _strQuery, CStringArray* _arrList, int nTryCnt = DB_WRITE_TRY_CNT);
	BOOL SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField, int nTryCnt = DB_WRITE_TRY_CNT);
	BOOL SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField,CString _strField2, int nTryCnt = DB_WRITE_TRY_CNT);
	BOOL SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField,CString _strField2,CString _strField3, int nTryCnt = DB_WRITE_TRY_CNT);
	BOOL SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField,CString _strField2,CString _strField3, CString _strField4, int nTryCnt = DB_WRITE_TRY_CNT);
	BOOL DeleteQuery(CString _strQuery, int nTryCnt = DB_WRITE_TRY_CNT);
	void Free();
private:
	CDBConnect *m_pDBManage;
};