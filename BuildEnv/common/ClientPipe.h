/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.08.16		
 ********************************************************************************/

/**
 @file      ClientPipe.h 
 @brief    ClientPipet정의 클래스

 @author    hang ryul lee
 @date      create 2011.08.16
 @note      
*/

#include "MemFileEx.h"
#include "Pipe.h"

#pragma once




class CClientPipe
{
public:
    CClientPipe();																			 /**< 기본 생성자 */
	CClientPipe(TCHAR* _pszPipeName);
    virtual ~CClientPipe();																/**< 파괴자 */

public:


/*****************Client ******************************************************/
	int SetPipeName(TCHAR* _pszPipeName);// Kevin(2013-4-29)
	int ClientConnectNamedPipe();
	int SetPipeState();

	BOOL	 IsNamedPipe();

	DWORD GetFunctionErrorCode();
	void SetFunctionErrorCode(DWORD dwError);

	virtual BOOL OnSetSendData(void* pData) = 0;
	virtual BOOL OnSendData() = 0;
	
	virtual BOOL OnSetHeader(PIPE_HEADER* _pPipeHeader) = 0;
	virtual BOOL OnSendBody() = 0;
	virtual BOOL OnSendHeader() = 0;

	virtual BOOL OnReceiveData() = 0;

	HANDLE		m_hPipe;						      	// Pipe Handle
	TCHAR	m_szPipeName[256];					// Pipe Name

	PIPE_HEADER* m_pPipeHeader;

	DWORD  m_dwErrorCode;

	BOOL PipeClientStartUp();
	BOOL PipeClientNewStartUp();
	BOOL PipeClientStartUp2(); // Kevin(2013-4-22)
	BOOL PipeClientStartUpForUserScan(); // 2016-07-18 sy.choi DlgUserScan의 ScanRequest()에서 senddata의 getLastError가 2(ERROR_FILE_NOT_FOUND)일 경우 TRUE로 리턴 하도록 PipeClientStartUp()에추가

private:
    CClientPipe(const CClientPipe &_rIpcPipe);										 /**< 복사 생성자 */
    CClientPipe& operator = (const CClientPipe &_rIpcPipe);					/**< 대입 연산자 */
};