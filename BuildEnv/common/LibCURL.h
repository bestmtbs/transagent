#pragma once

#include <locale>
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "curl\curl.h"
#pragma comment(lib, "wldap32.lib")
#pragma comment (lib, "crypt32")

#ifndef _DEBUG
#pragma comment (lib, "../BuildEnv/Lib/x86/libcurl.lib")
#else	// #ifdef _DEBUG
#pragma  comment (lib, "../BuildEnv/Lib/x86/libcurld.lib")
#endif	// #else	// #ifdef _DEBUG
#if LIBCURL_VERSION_NUM < 0x070c03
#error "upgrade your libcurl to no less than 7.12.3"
#endif

class CLibCURL
{
public:
	CLibCURL();
	~CLibCURL();

public:
	CString m_strDomain;
	CString m_strUri;
	CString CurlPost(CString _strParam, CString _strUrl);
	CString CurlGET(CString _strUrl);
	CString CurlDelete(CString _strParam, CString _strUrl);


private:
	DWORD	m_LastErrorCode;
	CString m_ErrorMsg;
	CString	m_strToken;

	CStringA Utf8_Encode(CStringW strData);
	CStringW Utf8_Decode(CStringA strData);
	void SetToken(CString _strToken);
	CString GetToken();
};

