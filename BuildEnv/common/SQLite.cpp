
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      SQLite.h 
 @brief     SQLite Class 구현 파일

            SQLite를 Wrapping한 클래스.

 @author    hang ryul lee
 @date      create 2011.05.12 
 @note      sqlite 3.5.8 버전을 기반으로 작성하였음.(http://www.sqlite.org/)
*/

#include "stdafx.h"
#include "afxconv.h"
#include "SQLite.h"

#if defined(_UNICODE) || defined(UNICODE)
    #define tsqlite3_bind_int               sqlite3_bind_int
    #define tsqlite3_bind_double            sqlite3_bind_double
    #define tsqlite3_bind_parameter_index   sqlite3_bind_parameter_index
    #define tsqlite3_bind_text              sqlite3_bind_text16
    #define tsqlite3_busy_handler           sqlite3_busy_handler
    #define tsqlite3_busy_timeout           sqlite3_busy_timeout
    #define tsqlite3_changes                sqlite3_changes
    #define tsqlite3_close                  sqlite3_close
    #define tsqlite3_column_count           sqlite3_column_count
    #define tsqlite3_column_double          sqlite3_column_double
    #define tsqlite3_column_int             sqlite3_column_int
    #define tsqlite3_column_name            sqlite3_column_name16
    #define tsqlite3_column_text            sqlite3_column_text16
    #define tsqlite3_column_type            sqlite3_column_type
    #define tsqlite3_complete               sqlite3_complete16
    #define tsqlite3_finalize               sqlite3_finalize
    #define tsqlite3_open                   sqlite3_open16
    #define tsqlite3_prepare                sqlite3_prepare16
    #define tsqlite3_reset                  sqlite3_reset
    #define tsqlite3_step                   sqlite3_step
	#define tsqlite3_initialize				sqlite3_initialize
	#define tsqlite3_exec					sqlite3_exec
	#define tsqlite3_free					sqlite3_free
#else
    #define tsqlite3_bind_int               sqlite3_bind_int
    #define tsqlite3_bind_double            sqlite3_bind_double
    #define tsqlite3_bind_parameter_index   sqlite3_bind_parameter_index
    #define tsqlite3_bind_text              sqlite3_bind_text  
    #define tsqlite3_busy_handler           sqlite3_busy_handler
    #define tsqlite3_busy_timeout           sqlite3_busy_timeout
    #define tsqlite3_changes                sqlite3_changes
    #define tsqlite3_close                  sqlite3_close
    #define tsqlite3_column_count           sqlite3_column_count
    #define tsqlite3_column_double          sqlite3_column_double
    #define tsqlite3_column_int             sqlite3_column_int
    #define tsqlite3_column_name            sqlite3_column_name
    #define tsqlite3_column_text            sqlite3_column_text
    #define tsqlite3_column_type            sqlite3_column_type
    #define tsqlite3_complete               sqlite3_complete
    #define tsqlite3_finalize               sqlite3_finalize
    #define tsqlite3_open                   sqlite3_open
    #define tsqlite3_prepare                sqlite3_prepare   
    #define tsqlite3_reset                  sqlite3_reset
	#define tsqlite3_step                   sqlite3_step
	#define tsqlite3_initialize				sqlite3_initialize
	#define tsqlite3_exec					sqlite3_exec
	#define tsqlite3_free					sqlite3_free
#endif

/**
 @brief     오류 코드에대한 오류 메시지를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @param     [in] _dwError   오류 코드
 @return    오류 메시지
*/
CString ErrorCodeToMessage(DWORD _dwError)
{
    CString sMessage = _T("");

    switch (_dwError)
    {
    case SQLITE_OK          : sMessage = _T("Successful result"); break;
    case SQLITE_ERROR       : sMessage = _T("SQL error or missing database"); break;
    case SQLITE_INTERNAL    : sMessage = _T("Internal logic error in SQLite"); break;
    case SQLITE_PERM        : sMessage = _T("Access permission denied"); break;
    case SQLITE_ABORT       : sMessage = _T("Callback routine requested an abort"); break;
    case SQLITE_BUSY        : sMessage = _T("The database file is locked"); break;
    case SQLITE_LOCKED      : sMessage = _T("A table in the database is locked"); break;
    case SQLITE_NOMEM       : sMessage = _T("A malloc() failed"); break;
    case SQLITE_READONLY    : sMessage = _T("Attempt to write a readonly database"); break;
    case SQLITE_INTERRUPT   : sMessage = _T("Operation terminated by sqlite3_interrupt()"); break;
    case SQLITE_IOERR       : sMessage = _T("Some kind of disk I/O error occurred"); break;
    case SQLITE_CORRUPT     : sMessage = _T("The database disk image is malformed"); break;
    case SQLITE_NOTFOUND    : sMessage = _T("NOT USED. Table or record not found"); break;
    case SQLITE_FULL        : sMessage = _T("Insertion failed because database is full"); break;
    case SQLITE_CANTOPEN    : sMessage = _T("Unable to open the database file"); break;
    case SQLITE_PROTOCOL    : sMessage = _T("NOT USED. Database lock protocol error"); break;
    case SQLITE_EMPTY       : sMessage = _T("Database is empty"); break;
    case SQLITE_SCHEMA      : sMessage = _T("The database schema changed"); break;
    case SQLITE_TOOBIG      : sMessage = _T("String or BLOB exceeds size limit"); break;
    case SQLITE_CONSTRAINT  : sMessage = _T("Abort due to constraint violation"); break;
    case SQLITE_MISMATCH    : sMessage = _T("Data type mismatch"); break;
    case SQLITE_MISUSE      : sMessage = _T("Library used incorrectly"); break;
    case SQLITE_NOLFS       : sMessage = _T("Uses OS features not supported on host"); break;
    case SQLITE_AUTH        : sMessage = _T("Authorization denied"); break;
    case SQLITE_FORMAT      : sMessage = _T("Auxiliary database format error"); break;
    case SQLITE_RANGE       : sMessage = _T("2nd parameter to sqlite3_bind out of range"); break;
    case SQLITE_NOTADB      : sMessage = _T("File opened that is not a database file"); break;
    case SQLITE_ROW         : sMessage = _T("sqlite3_step() has another row ready"); break;
    case SQLITE_DONE        : sMessage = _T("sqlite3_step() has finished executing"); break;
    default :
        sMessage = _T("Unknown Error");
    }

    return sMessage;
}
/********************************************************************************
 @class     CSQLiteConnection
 @brief     SQLite Database와의 연결을 제공하는 Class

            SQLite의 Connection Object는 Single Transction과 Single Connection을 
            제공한다.
*********************************************************************************/

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2008.05.12
*/
CSQLiteConnection::CSQLiteConnection()
{
    m_pConnection = NULL;
    m_lNestLevel = 0;
    m_lCommandTimeout = 10 * 1000;  //30 * 1000 에서 수정
    m_lConnectionTimeout = 15 * 1000;
    m_bConnected = false;
    m_dwError = 0;
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2008.05.12
*/
CSQLiteConnection::~CSQLiteConnection()
{
		
    Close();
}

/**
 @brief     SQL 문장을 컴파일하고 SQL문 실행을 위한 리스스를 할당한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sSql  단일 SQL 문장
 @return    성공 : sqlite3_stmt 포인터 \n
            실패 : NULL 포인터
 @note      세미콜론(";")으로 구분된 다중 SQL문장에 대한 처리는 지원하지 않음.
*/
sqlite3_stmt* CSQLiteConnection::Prepare(const CString &_sSql)
{
    
    if (!m_bConnected)
        return NULL;

    sqlite3_stmt *pStmt = NULL;
    int nLen = _sSql.GetLength() * sizeof(TCHAR);

    int nResult = tsqlite3_prepare(m_pConnection, _sSql, nLen, &pStmt, NULL);    
    if (SQLITE_OK != nResult)
    {
        m_dwError = nResult;
        return NULL;
    }

    return pStmt;
}

/**
 @brief     Transaction을 시작한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    Transaction 중첩 Level값
*/
long CSQLiteConnection::BeginTrans()
{
    if (!m_bConnected)
        return m_lNestLevel;

    sqlite3_stmt *pStmt = Prepare(_T("BEGIN TRANSACTION;"));
    if (NULL == pStmt)
        return m_lNestLevel;

    int nResult = tsqlite3_step(pStmt);
    if ((SQLITE_OK == nResult) || (SQLITE_DONE == nResult))
    {
        ::InterlockedIncrement(&m_lNestLevel);
    }
    else
    {
        m_dwError = nResult;
    }

    tsqlite3_finalize(pStmt);
    pStmt = NULL;

    return m_lNestLevel;
}

/**
 @brief     Transaction을 Commit 한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    Transaction 중첩 Level값
 @note      CSQLiteConnection을 사용하는 CSQLiteCommand 객체중 최근 수행한 CSQLiteCommand의 Execute의 결과가 \n
            SQLITE_DONE이 아닌경우 CommitTrans()가 정상적으로 수행되지 않는다. 그이유는 다음과 같다        
 @note      sqlite3 connection을 사용한 가장 최근의 sqlite3_stmt가 sqlite3_finalize을 호출하지 않은 상태이고 \n
            sqlite3_step호출결과가 SQLITE_OK 또는 SQLITE_DONE이 아닌경우에 이 connection을 사용하는 새로운   \n
            sqlite3_stmt를 생성해서 sqlite_step를 호출하면 오류(SQLITE_ERROR)를 리터하기 때문이다. 
*/
long CSQLiteConnection::CommitTrans()
{
    if (!m_bConnected)
        return m_lNestLevel;

    sqlite3_stmt *pStmt = Prepare(_T("COMMIT TRANSACTION;"));
    if (NULL == pStmt)
        return m_lNestLevel;

    int nResult = tsqlite3_step(pStmt);
    if ((SQLITE_OK == nResult) || (SQLITE_DONE == nResult))
    {
        ::InterlockedDecrement(&m_lNestLevel);
    }
    else
    {
        m_dwError = nResult;
    }

    tsqlite3_finalize(pStmt);
    pStmt = NULL;
    
    return m_lNestLevel;
}

/**
 @brief     Transaction을 Rollback 한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    Transaction 중첩 Level값
 @note      CSQLiteConnection을 사용하는 CSQLiteCommand 객체중 최근 수행한 CSQLiteCommand의 Execute의 결과가 \n
            SQLITE_DONE이 아닌경우 RollbackTrans()가 정상적으로 수행되지 않는다. 그이유는 다음과 같다        
 @note      sqlite3 connection을 사용한 가장 최근의 sqlite3_stmt가 sqlite3_finalize을 호출하지 않은 상태이고 \n
            sqlite3_step호출결과가 SQLITE_OK 또는 SQLITE_DONE이 아닌경우에 이 connection을 사용하는 새로운   \n
            sqlite3_stmt를 생성해서 sqlite_step를 호출하면 오류(SQLITE_ERROR)를 리터하기 때문이다. 
*/
long CSQLiteConnection::RollbackTrans()
{
    if (!m_bConnected)
        return m_lNestLevel;

    sqlite3_stmt *pStmt = Prepare(_T("ROLLBACK TRANSACTION;"));
    if (NULL == pStmt)
        return m_lNestLevel;

    int nResult = tsqlite3_step(pStmt);
    if ((SQLITE_OK == nResult) || (SQLITE_DONE == nResult))
    {
        ::InterlockedDecrement(&m_lNestLevel);
    }
    else
    {
        m_dwError = nResult;
    }

    tsqlite3_finalize(pStmt);
    pStmt = NULL;
    
    return m_lNestLevel;
}

/**
 @brief     Database와의 접속을 종료한다.
 @author    hang ryul lee
 @date      2008.05.12
*/
void CSQLiteConnection::Close()
{
	//if (NULL != m_pStmt) {
	//	tsqlite3_finalize(pStmt);
	//}
	if (!m_bConnected)
		return;

	if( m_pConnection != NULL )
	{
		tsqlite3_close(m_pConnection);
		m_pConnection = NULL;
	}

	m_bConnected = false;
}

/**
 @brief     Database에 접속을 한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    true/false
 @note      SetConnectionString()으로 설정된 정보를 사용하여 Database에 접속을 시도한다.
*/
bool CSQLiteConnection::Open()
{
	int nResult = tsqlite3_open(m_sConnectionStr, &m_pConnection);
    m_bConnected = SQLITE_OK == nResult;
    if (!m_bConnected)
    {
        m_dwError = nResult;
    }
	//UM_WRITE_LOG(_T("[CmsTray] Open"));
   // tsqlite3_busy_handler(m_pConnection, OnBusyHandler, this); //--hhh test -> 이놈때문에 문제가 발생하는거 같음. 실패처리는 랩핑하는 함수쪽에서 처리하도록 해보자
    return m_bConnected;
}


bool CSQLiteConnection::Open(CString &_sConnectionStr)
{
	SetConnectionString(_sConnectionStr);

	return Open();
}



/**
 @brief     SQL 문장을 실행한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sCommandText 실행할 SQL 문장
 @return    true/false
 @note      SQL문 실행 결과 Data를 리턴하지 않고 실행 성공 유무만 리턴한다.
 @note      세미콜론(";")으로 구분된 다중 SQL문장을 지원한다.
 @note      다중 SQL문장일경우 그중 하나만 실패하여도 false를 리턴하고 이후의 SQL문장을 실행하지 않는다.
 @note      2008.05.19 hang ryul lee\n
            _sCommandText가 공백문자열(_T(""))일 경우에 대한 처리 추가
*/
bool CSQLiteConnection::Execute(const CString &_sCommandText)
{
    if (!m_bConnected)
        return false;

    sqlite3_stmt *pStmt = NULL;
    int nResult = SQLITE_OK;
    CString sSql = _sCommandText;
    sSql = sSql.Trim();
    if (0 == sSql.GetLength())
        return false;

    if (_T(";") != sSql.Right(1))
        sSql += _T(";");
	
	sSql += _T("PRAGMA synchronous=OFF;PRAGMA count_changes=OFF;PRAGMA temp_store=memory;"); // 빠른거 테스트 matrix 20121206
    
    LPCTSTR szTail = NULL; 
    int nLen = 0;
	//UM_WRITE_LOG(_T("[CmsTray] Execute Start.."));
    while (tsqlite3_complete(sSql))
    {
        nLen = sSql.GetLength() * sizeof(TCHAR);
        if (0 == nLen)
            break;

#if defined(_UNICODE) || defined(UNICODE)
        nResult = tsqlite3_prepare(m_pConnection, sSql, nLen, &pStmt, reinterpret_cast<const void **>(&szTail));
#else
        nResult = tsqlite3_prepare(m_pConnection, sSql, nLen, &pStmt, reinterpret_cast<const char **>(&szTail));
#endif
        if (SQLITE_OK != nResult)
        {
            m_dwError = nResult;
            tsqlite3_finalize(pStmt);
            pStmt = NULL;
            return false;
        }

        nResult = tsqlite3_step(pStmt);
        if (SQLITE_ERROR == nResult)
		{

            m_dwError = nResult;
            tsqlite3_finalize(pStmt);
            pStmt = NULL;
			//UM_WRITE_LOG(_T("[CmsTray] Execute Error - SQLITE_ERROR"));
            return false;
        }

        tsqlite3_finalize(pStmt);
        pStmt = NULL;

        sSql = szTail;
    }   
    
    if (NULL != pStmt)
    {
        tsqlite3_finalize(pStmt);
        pStmt = NULL;
    }
	//UM_WRITE_LOG(_T("[CmsTray] Execute Return.."));
    return true;
}

/**
 @brief     SQL Command 실행 Timeout값을 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    CommandTimeout값(초)
 @note      CommandTimeout값은 초단위이고 Default값은 30초이다.
*/
long CSQLiteConnection::GetCommandTimeout() const
{
    return (m_lCommandTimeout / 1000);
}

/**
 @brief     SQL Command 실행 Timeout값을 설정한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _lCommandTimeout CommandTime값(초)
 @note      CommandTimeout값은 초단위이고 Default값은 30초이다.
*/
void CSQLiteConnection::SetCommandTimeout(long _lCommandTimeout)
{
	
    if (m_lCommandTimeout == (_lCommandTimeout * 1000))
        return;

    m_lCommandTimeout = (_lCommandTimeout * 1000);
    tsqlite3_busy_timeout(m_pConnection, m_lCommandTimeout);
}

/**
 @brief     Database와의 접속유무를 확인한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    true/false
*/
bool CSQLiteConnection::IsConnected() const
{
    return m_bConnected;
}

/**
 @brief     Database 접속을 위해 사용한 Database File Name을 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    Database File Name
 @note      Database File Name이 ":memory:" 또는 공백문자열("")일 경우 in-memory database를 생성한다.
*/
CString CSQLiteConnection::GetConnectionString() const
{
    return m_sConnectionStr;
}

/**
 @brief     Database 접속을 위해 사용한 Database File Name을 설정 한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sConnectionStr    Database File Name
 @note      Database File Name이 ":memory:" 또는 공백문자열("")일 경우 in-memory database를 생성한다.
*/
void CSQLiteConnection::SetConnectionString(const CString &_sConnectionStr)
{
    m_sConnectionStr = _sConnectionStr;
}

/**
 @brief     Database 접속 Timeout값을 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    ConnectionTimeout값(초)
 @note      SQLite는 Embeded Datatbase이므로 이설정값은 의미가 없다.
 @note      ConnectionTimeout값은 초단위이고 기본값은 15초이다.
 @note      ConnectionTimeout값은 DB 접속 유무와 상관없이 구할수 있다.
*/
long CSQLiteConnection::GetConnectionTimeout() const
{
    return (m_lConnectionTimeout / 1000);
}

/**
 @brief     Database 접속 Timeout값을 설정한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _lConnectionTimeout값(초)
 @note      SQLite는 Embeded Datatbase이므로 이설정값은 의미가 없다.
 @note      ConnectionTimeout값은 초단위이고 기본값은 15초이다.
 @note      ConnectionTimeout값은 DB 접속 유무와 상관없이 구할수 있다.
*/
void CSQLiteConnection::SetConnectionTimeout(long _lConnectionTimeout)
{
    if (m_lConnectionTimeout == (_lConnectionTimeout * 1000))
        return;

    m_lConnectionTimeout = (_lConnectionTimeout * 1000);    
}

/**
 @brief     최근 오류 코드를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @return    오류 코드
*/
DWORD CSQLiteConnection::GetError() const
{
    return m_dwError;
}

/**
 @brief     최근 오류 코드에대한 오류 메시지를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @return    오류 메시지
*/
CString CSQLiteConnection::GetErrorMessage() const
{
    return ErrorCodeToMessage(m_dwError);
}

/**
 @brief     SQLITE_BUSY 발생시 호출되는 Callback함수.
 @author    hang ryul lee
 @date      2008.12.02
 @param     [in] _pConnection   CSQLiteConnection 포인터
 @param     [in] _nCount        OnBusyHandler 호출 횟수
 @return    0 : Timeout, 1 : 재시도
 @note      첫번째 파라미터(_pConnection)은 sqlite3_busy_handler 함수의 세번째 파라미더로 설정된다.
 @note      두번째 파라미터(_nCount)는 busy handler에의해서 호출된 횟수가 설정된다.
 @note      이 Callback함수에서 0을 리턴하면 database access를위해 추가적인 시도를하지 않고
            SQLITE_BUSY 또는 SQLITE_IOERR_BLOCKED를 리턴한다.
 @note      이 Callback함수에서 0이 아닌값을 리턴하면 database access를위한 기존의 시도를 다시반복하거나
            새로운 시도를 받아들이게 된다.
*/
int CSQLiteConnection::OnBusyHandler(void *_pConnection, int _nCount)
{
	//return 0; ///hhh_test
    CSQLiteConnection *pConnection = reinterpret_cast<CSQLiteConnection *>(_pConnection);
    if (NULL == pConnection)
        return 0;

    int nTimeout = pConnection->GetCommandTimeout() * 1000;
    if (nTimeout > (10 * _nCount))
    {
		//UM_WRITE_LOG(_T("[CmsTray] OnBusyHandler Call.."));
        ::Sleep(100);  //Sleep(10) -> Sleep(100)
        return 1;
    }

    return 0;
}


/********************************************************************************
 @class     CSQLiteDataSet
 @brief     SQL 명령의 실행으로 생성된 레코드의 집합을 Class화함.

            한번에 한 레코드씩 읽을수 있다.
*********************************************************************************/

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2008.05.12
*/
CSQLiteDataSet::CSQLiteDataSet()
{
    m_pStmt = NULL;
    m_bBof = true;
    m_bEof = true;
    m_dwError = 0;
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2008.05.12
*/
CSQLiteDataSet::~CSQLiteDataSet()
{
    Close();
}


/**
 @brief     열려있는 레코드의 집합을 닫는다.
 @author    hang ryul lee
 @date      2008.05.12
*/
void CSQLiteDataSet::Close()
{
    if (NULL != m_pStmt)
    {        
        tsqlite3_finalize(m_pStmt);
        m_pStmt = NULL;
    }
    m_bBof = true;
    m_bEof = true;
    m_dwError = 0;
}

/**
 @brief     열려있는 레코드의 집합에서 다음 레코드로 위치를 이동한다.
 @author    hang ryul lee
 @date      2008.05.12
*/
void CSQLiteDataSet::Next()
{
    if (NULL == m_pStmt)
        return;

    if (m_bEof)
        return;

    int nResult = tsqlite3_step(m_pStmt);
    if (SQLITE_DONE == nResult)
    {
		m_bEof = true;
		tsqlite3_reset(m_pStmt);
    }
    else if (SQLITE_ROW == nResult)
    {
        if (m_bBof)
            m_bBof = false;
    }
    else
    {
        m_dwError = nResult;
    }      
}

/**
 @brief     현재 레코드의 위치가 레코드의 집합에서 처음 레코드에 위치했는지 판단한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    true/false
*/
bool CSQLiteDataSet::IsBof()
{
    return m_bBof;
}

/**
 @brief     현재 레코드의 위치가 레코드의 집합에서 마지막 레코드에 위치했는지 판단한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    true/false
*/
bool CSQLiteDataSet::IsEof()
{
    return m_bEof;
}

/**
 @brief     DataSet에 레코드가 존재하지 않는지 확인한다.
 @author    hang ryul lee
 @date      2008.10.02
 @return    true/false
*/
bool CSQLiteDataSet::IsEmpty()
{
    return (m_bBof && m_bEof);
}

/**
 @brief     레코드의 집합에서 해당 필드의 Index를 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sFieldName 필드 이름
 @return    정상 : 0 이상의 값
            오류 : -1 (해당 필드가 존재하지 않을경우)
*/
int CSQLiteDataSet::GetFieldIndex(const CString &_sFieldName)
{
    int nIndex = -1;

    if (NULL == m_pStmt)
        return nIndex;

    if (_T("") == _sFieldName)
        return nIndex;

    CString sName = _T("");
    for (nIndex = 0; nIndex < tsqlite3_column_count(m_pStmt); nIndex++)
    {
        sName = static_cast<LPCTSTR>(tsqlite3_column_name(m_pStmt, nIndex));
        if (0 == _tcscmp(sName, _sFieldName))
            return nIndex;
    }   

    return -1;
}

/**
 @brief     레코드의 집합에서 해당 필드의 Data Type을 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _nColumn 컬럼 인덱스값
 @return    정상 :  1 이상의 값 \n
                    SQLITE_INTEGER = 1 \n
                    SQLITE_FLOAT   = 2 \n
                    SQLITE_TEXT    = 3 \n
                    SQLITE_BLOB    = 4 \n
                    SQLITE_NULL    = 5 \n
            오류 : -1 
*/
int CSQLiteDataSet::GetFieldDataType(int _nColumn)
{
    if (0 > _nColumn)
        return -1;
    
    if (NULL == m_pStmt) 
        return -1;

    if (_nColumn > tsqlite3_column_count(m_pStmt))
        return -1;

    return tsqlite3_column_type(m_pStmt, _nColumn);
}

/**
 @brief     레코드의 집합에서 해당 필드가 NULL인지 판단한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sFieldName 필드 이름
 @return    true/false
*/
bool CSQLiteDataSet::IsFieldNull(const CString &_sFieldName)
{
    int nColumn = GetFieldIndex(_sFieldName);
    return SQLITE_NULL == GetFieldDataType(nColumn);
}

/**
 @brief     레코드의 집합에서 Int Type 필드의 값을 읽어온다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sFieldName 필드 이름
 @param     [in] _nDefault   Default값 (0)
 @return    레코드의 해당 필드값
 @note      해당 레코드가 존재하지 않거나 NULL필드인 경우 Default값을 리턴한다.
*/
int CSQLiteDataSet::GetIntField(const CString &_sFieldName, int _nDefault /* = 0 */)
{
    int nColumn = GetFieldIndex(_sFieldName);
    if (-1 == nColumn)
        return _nDefault;

    if (SQLITE_NULL == GetFieldDataType(nColumn))
        return _nDefault;

    return tsqlite3_column_int(m_pStmt, nColumn);
}


/**
 @brief     레코드의 집합에서 double Type 필드의 값을 읽어온다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sFieldName 필드 이름
 @param     [in] _dDefault   Default값 (0.0)
 @return    레코드의 해당 필드값
 @note      해당 레코드가 존재하지 않거나 NULL필드인 경우 Default값을 리턴한다.
*/
double CSQLiteDataSet::GetDoubleField(const CString &_sFieldName, double _dDefault /* = 0.0 */)
{
    int nColumn = GetFieldIndex(_sFieldName);
    if (-1 == nColumn)
        return _dDefault;

    if (SQLITE_NULL == GetFieldDataType(nColumn))
        return _dDefault;

    return tsqlite3_column_double(m_pStmt, nColumn);
}


/**
 @brief     레코드의 집합에서 string Type 필드의 값을 읽어온다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sFieldName 필드 이름
 @param     [in] _sDefault   Default값 ("")
 @return    레코드의 해당 필드값
 @note      해당 레코드가 존재하지 않거나 NULL필드인 경우 Default값을 리턴한다.
*/
CString CSQLiteDataSet::GetStringField(const CString &_sFieldName, CString _sDefault /* = _T("") */)
{
    int nColumn = GetFieldIndex(_sFieldName);
    if (-1 == nColumn)
        return _sDefault;

    if (SQLITE_NULL == GetFieldDataType(nColumn))
        return _sDefault;

    return reinterpret_cast<LPCTSTR>(tsqlite3_column_text(m_pStmt, nColumn));
}

/**
 @brief     레코드의 집합에서 datetime Type 필드의 값을 읽어온다.
 @author    hang ryul lee
 @date      2008.11.03
 @param     [in] _sFieldName 필드 이름
 @param     [in] _dtDefault   Default값 ("1899년 12월 30일 0시 0분 0초")
 @return    레코드의 해당 필드값
 @note      해당 레코드가 존재하지 않거나 NULL필드인 경우 Default값을 리턴한다.
*/
COleDateTime CSQLiteDataSet::GetDateTimeField(const CString &_sFieldName, COleDateTime _dtDefault /* = COleDateTime() */)
{
    CString sDateTime = GetStringField(_sFieldName, _T(""));
    if (_T("") == sDateTime)
        return _dtDefault;

    return CConvert::ToDateTime(sDateTime);
}

/**
 @brief     최근 오류 코드를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @return    오류 코드
*/
DWORD CSQLiteDataSet::GetError() const
{
    return m_dwError;
}

/**
 @brief     최근 오류 코드에대한 오류 메시지를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @return    오류 메시지
*/
CString CSQLiteDataSet::GetErrorMessage() const
{
    return ErrorCodeToMessage(m_dwError);
}

/********************************************************************************
 @class     CSQLiteCommand
 @brief     SQL문을 실행하는 Class

*********************************************************************************/

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _pConnection   CSQLiteConnection 포인터
 @param     [in] _pDataSet      CSQLiteDataSet 포인터 (Default값은 NULL)
 @note      SQL문을 실행하고 결과 레코드 집합이 필요한경우 결과값을 포함할 CSQLiteDataSet에 대한 포인터를 
            _pDataSet에 설정하면된다.
*/
CSQLiteCommand::CSQLiteCommand(CSQLiteConnection *_pConnection, CSQLiteDataSet *_pDataSet /* = NULL */)
{
    m_pSQLiteConnection = _pConnection;
    m_pDataSet = _pDataSet;
    m_bInternalCreated = false;
    m_nPrepared = SQLITE_OK;
    m_dwError = 0;

    if (NULL == _pDataSet)
    {
        m_pDataSet = new CSQLiteDataSet();
        m_bInternalCreated = true;
    }
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2008.05.12
*/
CSQLiteCommand::~CSQLiteCommand()
{
	//int nResult = sqlite3_shutdown();
    if (m_bInternalCreated && (m_pDataSet != NULL))
        delete m_pDataSet;
}

/**
 @brief     SetCommandText로 설정된 SQL문을 실행한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in/out] _nRecordsAffected  실행된 명령에의해 변경된 레코드의 개수 
 @return    true/false
 @note      _nRecordsAffected는 UPDATE, INSERT, DELETE문 실행에의행 영향을 받은 레코드수이다.
*/
bool CSQLiteCommand::Execute(int &_nRecordsAffected)
{    
    try
    {
		CString strMsg;

        int nResult = SQLITE_OK;

        if (SQLITE_OK != m_nPrepared)
		{		
            return false;
		}

        if (!m_pSQLiteConnection->IsConnected())
		{
			strMsg.Format(_T("CMSDB : connection error (%d) "), GetLastError());
			UM_WRITE_LOG(_T("[CmsTray] ExecQuery Error: ") + strMsg);
//		DBGLOG(strMsg);
            return false;
		}
        
		//UM_WRITE_LOG(_T("[CmsTray] Execute Step ---- 1") );
        m_pDataSet->m_bBof = true;
		m_pDataSet->m_bEof = true;
		CString strText = _T("");
		strText = GetCommandText();
		strText.MakeLower();
		nResult = tsqlite3_step(m_pDataSet->m_pStmt);
		//strMsg.Format(_T("[Test] tsqlite3_step : %d"), nResult);
		//UM_WRITE_LOG(strMsg);
     /*   if (SQLITE_ERROR == nResult )			
        {
            m_dwError = nResult;
            return false;
        }*/
		//UM_WRITE_LOG(_T("[CmsTray] Execute Step ---- 2") );
        if (SQLITE_DONE == nResult || SQLITE_EMPTY ==  nResult)
        {
			m_pDataSet->m_bEof = true;
			if (SQLITE_EMPTY != nResult) {
				BOOL nReset = ResetParameters();	// 2017-03-23 sy.choi step 후 stmt 재사용 위해 reset
			}
        }
        else if (SQLITE_ROW == nResult)
        {
            m_pDataSet->m_bEof = false;
        }
		else			//hhh: 2013.03.04 저 이외 값을 리턴한다면 정상적으라고 볼 수 없다.
		{
			//UM_WRITE_LOG(_T("[CmsTray] Execute Step ---- error") );
			m_dwError = nResult;
			return false;
		}
		//UM_WRITE_LOG(_T("[CmsTray] Execute Step ---- 3") );
		_nRecordsAffected = tsqlite3_changes(m_pSQLiteConnection->m_pConnection);
		//UM_WRITE_LOG(_T("[CmsTray] Execute Step ---- 4") );
        return true;
    }
    catch(...)
    {
		//UM_WRITE_LOG(_T("[CmsTray] Execute Step ---- 5") );
        return false;
    }
}


/**
 @brief     SQL Command 실행 Timeout값을 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    CommandTimeout값(초)
 @note      CommandTimeout값은 초단위이고 Default값은 30초이다.
*/
long CSQLiteCommand::GetCommandTimeout() const
{
    return m_pSQLiteConnection->GetCommandTimeout();
}

/**
 @brief     SQL Command 실행 Timeout값을 설정한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _lCommandTimeout CommandTime값(초)
 @note      CommandTimeout값은 초단위이고 Default값은 30초이다.
*/
void CSQLiteCommand::SetCommandTimeout(long _lCommandTimeout)
{
    m_pSQLiteConnection->SetCommandTimeout(_lCommandTimeout);
}

/**
 @brief     실행하기위해 설정한 SQL문을 구한다.
 @author    hang ryul lee
 @date      2008.05.12
 @return    SQL문    
*/
CString CSQLiteCommand::GetCommandText() const
{
    return m_sCommandText;
}

/**
 @brief     실행할 SQL문을 설정한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _rCommandText  실행할 SQL문    
 @note      CommandText의 Default값은 "" 이다.            
*/
void CSQLiteCommand::SetCommandText(const CString &_rCommandText)
{
    if (0 == _tcsicmp(m_sCommandText, _rCommandText))
        return;

    m_sCommandText = _rCommandText; 
    if (NULL != m_pDataSet->m_pStmt)
    {
	//	DBGLOG(_T("CMSDB : SetCommandText close"));
        m_pDataSet->Close();
    }

	tsqlite3_initialize();
    m_nPrepared = tsqlite3_prepare(m_pSQLiteConnection->m_pConnection, m_sCommandText, -1, &m_pDataSet->m_pStmt, NULL);
    if (SQLITE_OK != m_nPrepared)
    {
        m_dwError = m_nPrepared;
    }
}

/**
 @brief     매개변수를 가지는 SQL문에 Int형 매개변수 정보를 생성하여 추가한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sName         매개변수 이름
 @param     [in] _nValue        매개변수 값
 @param     [in] _nDirection    매개변수 방향값 (Default값은 1)  \n
                                알수없음         = 0,   \n
                                입력 매개변수    = 1,   \n
                                출력 매개변수    = 2,   \n
                                입/출력 매개변수 = 3,   \n
                                반환 매개변수    = 4    \n
 @return    true/false
 @note      SQLite의 매개변수는 모두 입력형식이므로 _nDirection값에 상관없이 입력형식(1)으로 설정된다.
 @note      주어진 Parameter이름이 SQL문에 존재하지 않을 경우 false를 리턴한다.
*/
bool CSQLiteCommand::AddParameter(const CString &_sName, int _nValue, int _nDirection /* = 1 */)
{
    UNUSED_ALWAYS(_nDirection);

#if defined(_UNICODE) || defined(UNICODE)
    USES_CONVERSION_EX;
    LPSTR szName = T2A_EX(_sName, _szName.GetLength());
#else
    CString sTempName = _sName;
    LPSTR szName = sTempName.GetBuffer(sTempName.GetLength());
    sTempName.ReleaseBuffer();
#endif
    int nParam = tsqlite3_bind_parameter_index(m_pDataSet->m_pStmt, szName);
    if (0 == nParam)
        return false;

    int nResult = tsqlite3_bind_int(m_pDataSet->m_pStmt, nParam, _nValue);
    if (SQLITE_OK == nResult)
    {
        return true;
    }
    else
    {
        m_dwError = nResult;
        return false;
    }
}

/**
 @brief     매개변수를 가지는 SQL문에 double형 매개변수 정보를 생성하여 추가한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sName         매개변수 이름
 @param     [in] _nValue        매개변수 값
 @param     [in] _nDirection    매개변수 방향값 (Default값은 1)  \n
                                알수없음         = 0,   \n
                                입력 매개변수    = 1,   \n
                                출력 매개변수    = 2,   \n
                                입/출력 매개변수 = 3,   \n
                                반환 매개변수    = 4    \n
 @param     [in] _nPrecision    사용할 최대 자릿수(정밀도)값
 @param     [in] _nScale        소수점이하 자릿수 값 
 @return    true/false
 @note      SQLite의 매개변수는 모두 입력형식이므로 _nDirection값에 상관없이 입력형식(1)으로 설정된다.
 @note      SQLite의 매개변수 정보 설정에서는 사용할 최대 자릿수 및 소수점이하 자릿수값을 사용하지 않는다.
 @note      주어진 Parameter이름이 SQL문에 존재하지 않을 경우 false를 리턴한다.
*/
bool CSQLiteCommand::AddParameter(const CString &_sName, double _dValue, int _nDirection /* = 1 */, BYTE _nPrecision /* = 0 */, BYTE _nScale /* = 0 */)
{
    UNUSED_ALWAYS(_nDirection);
    UNUSED_ALWAYS(_nPrecision);
    UNUSED_ALWAYS(_nScale);

#if defined(_UNICODE) || defined(UNICODE)
    USES_CONVERSION_EX;
    LPSTR szName = T2A_EX(_sName, _szName.GetLength());
#else
    CString sTempName = _sName;
    LPSTR szName = sTempName.GetBuffer(sTempName.GetLength());
    sTempName.ReleaseBuffer();
#endif

    int nParam = tsqlite3_bind_parameter_index(m_pDataSet->m_pStmt, szName);
    if (0 == nParam)
        return false;

    int nResult = tsqlite3_bind_double(m_pDataSet->m_pStmt, nParam, _dValue);
    if (SQLITE_OK == nResult)
    {
        return true;
    }
    else
    {
        m_dwError = nResult;
        return false;
    }
}

/**
 @brief     매개변수를 가지는 SQL문에 string형 매개변수 정보를 생성하여 추가한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sName         매개변수 이름
 @param     [in] _nValue        매개변수 값
 @param     [in] _nDirection    매개변수 방향값 (Default값은 1)  \n
                                알수없음         = 0,   \n
                                입력 매개변수    = 1,   \n
                                출력 매개변수    = 2,   \n
                                입/출력 매개변수 = 3,   \n
                                반환 매개변수    = 4    \n
 @param     [in] _lSize         사용할 최대 자릿수(정밀도)값
 @return    true/false
 @note      SQLite의 매개변수는 모두 입력형식이므로 _nDirection값에 상관없이 입력형식(1)으로 설정된다.
 @note      SQLite의 매개변수 정보 설정에서는 출력 또는 반환 매개변수에서 사용하는 가변길이 자료에 대한 크기설정값은 사용하지 않는다.
 @note      주어진 Parameter이름이 SQL문에 존재하지 않을 경우 false를 리턴한다.
*/
bool CSQLiteCommand::AddParameter(const CString &_sName, CString _sValue, int _nDirection /* = 1 */, long _lSize /* = 256 */)
{
    UNUSED_ALWAYS(_nDirection);
    UNUSED_ALWAYS(_lSize);

#if defined(_UNICODE) || defined(UNICODE)
    USES_CONVERSION_EX;
    LPSTR szName = T2A_EX(_sName, _szName.GetLength());
#else
    CString sTempName = _sName;
    LPSTR szName = sTempName.GetBuffer(sTempName.GetLength());
    sTempName.ReleaseBuffer();
#endif

    int nParam = tsqlite3_bind_parameter_index(m_pDataSet->m_pStmt, szName);
    if (0 == nParam)
        return false;

    int nResult = tsqlite3_bind_text(m_pDataSet->m_pStmt, nParam, _sValue, -1, SQLITE_TRANSIENT);
    if (SQLITE_OK == nResult)
    {
        return true;
    }
    else
    {
        m_dwError = nResult;
        return false;
    }
}

/**
 @brief     매개변수를 가지는 SQL문에 date형 매개변수 정보를 생성하여 추가한다.
 @author    hang ryul lee
 @date      2008.05.12
 @param     [in] _sName         매개변수 이름
 @param     [in] _nValue        매개변수 값
 @param     [in] _nDirection    매개변수 방향값 (Default값은 1)  \n
                                알수없음         = 0,   \n
                                입력 매개변수    = 1,   \n
                                출력 매개변수    = 2,   \n
                                입/출력 매개변수 = 3,   \n
                                반환 매개변수    = 4    \n
 @param     [in] _lSize         사용할 최대 자릿수(정밀도)값
 @return    true/false
 @note      SQLite의 매개변수는 모두 입력형식이므로 _nDirection값에 상관없이 입력형식(1)으로 설정된다.
 @note      주어진 Parameter이름이 SQL문에 존재하지 않을 경우 false를 리턴한다.
 @note      SQLite의 자료형에는 Date형이 존재하지 않으므로 문자열형으로 처리한다.
*/
bool CSQLiteCommand::AddParameter(const CString &_sName, COleDateTime _dtValue, int _nDirection /* = 1 */)
{
    CString sDateValue = CConvert::ToString(_dtValue);
    return AddParameter(_sName, sDateValue, _nDirection);
}

/**
 @brief     매개변수 정보를 초기화한다.
 @author    hang ryul lee
 @date      2008.05.16
 @return    true/false
*/
bool CSQLiteCommand::ResetParameters()
{
    int nResult = tsqlite3_reset(m_pDataSet->m_pStmt);
    if (SQLITE_OK == nResult)
    {
        return true;
    }
    else
    {
        m_dwError = nResult;
        return false;
    }
}

/**
 @brief     최근 오류 코드를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @return    오류 코드
*/
DWORD CSQLiteCommand::GetError() const
{
    return m_dwError;
}

/**
 @brief     최근 오류 코드에대한 오류 메시지를 리턴한다.
 @author    hang ryul lee
 @date      2008.10.01
 @return    오류 메시지
*/
CString CSQLiteCommand::GetErrorMessage() const
{
    return ErrorCodeToMessage(m_dwError);
}