// KSList.h: interface for the KSList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_KSLIST_H__4B2E22CF_0F60_4986_880C_EA767C640EBE__INCLUDED_)
#define AFX_KSLIST_H__4B2E22CF_0F60_4986_880C_EA767C640EBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include <list>
#include <queue>
#include "KSObject.h"
/**
	@file	KSList.h
	@brief	object List 클래스 정의 

	@author	최초 작성자 : 강기수
	@author 마지막 수정자 : 강기수

	@date 최초 작성일 : 2006.08.01
	@date 마지막 수정일 : 2006.09.07

    리스트 자료구조 클래스 정의 
*/
/* KSObject의 리스트 클래스 : KSObject를 상속받은 클래스들은 모두 리스트에 담을 수 있다. */
class  KSList : public KSObject
{
public:
	typedef  std::list <KSObject* >::iterator	Position;

			KSList();
	virtual	~KSList();

	virtual	void		clear();
	virtual	Position	headPosition();
	virtual Position	tailPosition();

	virtual	bool		isEnd( Position pos );
	virtual	int			size();
	virtual	Position	find( KSObject* arg );

	virtual	void		pushHead( KSObject* arg );
	virtual	void		pushTail( KSObject* arg );
	virtual	KSObject*	popHead();
	virtual	void		popTail();

	virtual	KSObject*	removeItem( Position pos );
	virtual	KSObject*	getItem( Position pos );
	virtual	KSObject*	removeHead();

private:
	std::list	< KSObject* >	elementList;
};

/* KSObject의 Stack 클래스 : KSList를 이용하여 구현 */

class KSStack : public KSList
{
public:
	KSStack();
	virtual ~KSStack();

	void push( KSObject* arg );
	KSObject* pop();
	int size() { return KSList::size(); };
		};

/* KSObject의 Queue 클래스 : KSList를 이용해서 구현 */
class KSQueue : public KSList
{
public:
	KSQueue();
	virtual ~KSQueue();

	void push( KSObject* arg );
	KSObject* pop();
	int size() { return KSList::size(); };
};

#endif // !defined(AFX_KSLIST_H__4B2E22CF_0F60_4986_880C_EA767C640EBE__INCLUDED_)
