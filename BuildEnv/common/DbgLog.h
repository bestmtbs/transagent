#ifndef __DBGLOG_H__

#define __DBGLOG_H__

// not used
#define	DBG_NOT_LOG		0
#define	DBG_WFILE		1
#define	DBG_WVIEW		2
#define	DBG_WMSG		4

#define DBG_W_VF			3
#define DBG_W_MF			5
#define DBG_W_VMF			7

#define DBG_FILTER_LEVEL_4		0x00000010L

#define DBG_LOG_PRODUCT			L"[TIORSAVER]"
#define DBG_LOG_PRODUCTA			"[TIORSAVER]"

#define DBG_LOG_DEFAULT_KEY		L"SOFTWARE\\dsntech\\TIORSAVER\\LogOption"

class CDbgLog
{
public:
	CDbgLog();
	CDbgLog(CString strFileName);
	CDbgLog(CString strPath, CString strFileName);
	CDbgLog(CString strPath, CString strFileName, CString strPrefix);
	virtual ~CDbgLog();

	// Kevin(2013-4-15)
	// 현재 프로세스명 얻기
	WCHAR		m_strProcess[MAX_PATH];
	char			m_strProcessA[MAX_PATH];

	enum _LOG_DEF_ {
		eMAX_LOG=8192*5,
		eMin_LOG=10240,
	};

	void WriteLogW( LPWSTR ptcpFormat, ... );// Kevin(2013-4-30)
	void WriteLogA(LPSTR ptcpFormat, ... );// Kevin(2013-4-30)
	void WriteLogToDVW(  LPWSTR ptcpFormat, ... );// Kevin(2013-4-30)
	void WriteLogToDVA(LPSTR ptcpFormat, ... );// Kevin(2013-4-30)
	BOOL GetCurProcessName();// Kevin(2013-4-30)

	BOOL CreateLogDir();// Kevin(2013-4-30)
	void InitLog(BOOL bDelPrevLog=TRUE); // 이전 로그 강제삭제// Kevin(2013-4-30)
	void SetRegPath();
	void SetOutType(int nOutType);

	ULONG m_ulTickInterval;
	void SetTickInterval(ULONG ulInterval);
private:
	// Kevin(2013-4-15)
	// default log path
	CString		m_strDefaultLogPath;

	CString		m_strLogRegName;
	CString		m_strLogFileName;
	CString		m_strLogFilePath;
	CString		m_strLogFileFullName;
	CString		m_strPrefixString;
	BOOL			m_bWriteFileFlag;
	DWORD		m_bOutType;	// Kevin(2013-5-7) 1이면 file 2이면 UM_WRITE_LOG()

	CRITICAL_SECTION	ownCriticalSection;
	BOOL		m_bInitCriticalSection;

public:
	void WriteTraceLog(CString strLog);
	void WriteTraceLog(TCHAR* fmt, ...);

	void SetPrefix(CString str);
	void SetLogFileName(CString str);
	void SetLogFilePath(CString str);

	CString GetPrefix();
};

// Kevin(2013-4-15)
// add for debug log default log macro
extern CDbgLog g_cDbgLog;

#ifdef _UNICODE
#define DBGLOG			g_cDbgLog.WriteLogW
#define DBGLOGA			g_cDbgLog.WriteLogA
#define DBGLOGW		g_cDbgLog.WriteLogW

#define DBGLOGDV		g_cDbgLog.WriteLogToDVW
#define DBGLOGDVW		g_cDbgLog.WriteLogToDVW
#define DBGLOGDVA		g_cDbgLog.WriteLogToDVA
#define DBGOUT			DBGLOGDVW
#define DBGOUTA			DBGLOGDVA
#define UM_WRITE_LOG(log) \
	g_cDbgLog.WriteTraceLog(log);
#define DBGTRACE(log)	g_cDbgLog.WriteTraceLog(log)
#else
#define DBGLOG		g_cDbgLog.WriteLogA
#define DBGLOGA		g_cDbgLog.WriteLogA
#define DBGLOGW	g_cDbgLog.WriteLogW
#endif

#endif