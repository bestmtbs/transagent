
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      Impersonator.h 
 @brief     CImpersonator class 정의 파일

            현재 프로세스의 권한을 변경한다.

 @author    hang ryul lee 
 @date      create 2007.11.30  
*/
#pragma once
#include "ImpersonatorConst.h"

/**
 @class     CImpersonator 
 @brief     현재 프로세스를 주어진 권한으로 Impersonate 한다.
 @author    hang ryul lee 
 @date      create 2007.11.30 
 @note      Impersonate된 권한으로 프로세스를 생성할 수 있다.
 @note      m_bLoadProfile 멤버 변수\n
            XP이하의 NT계열에서 Winlogon.exe 프로세스 Token으로 \n
            LoadUserProfile 할경우 이벤트로그에 오류가 기록되어 FLoadProfile \n
            을 구분자로하여 LoadUserProfile 사용 또는 사용하지 않도록 구분함.
 @note      Windows NT 이하에서는 Impersonate를 지원하지 않지만 사용의 편의를 위해\n
            내부에서 알아서 처리한다. (class 사용 가능)
 @note      class를 선언만 했을 경우에는 run 등의 함수를 호출해도 fail이 떨어진다.\n
            CreateAsSystem / CreateAsLogonUser 둘중 하나의 생성 함수를 호출하여야\n
            class 내 함수의 기능이 제대로 동작한다.\n
            (Create 함수 호출시 해당 프로세스가 Impersonate 된다. 사용이 끝나면\n
            close 함수를 호출하여 원래 권한으로 되돌리도록 한다.)
*/
class CImpersonator
{
public:
    CImpersonator();
    virtual ~CImpersonator();

    bool CreateAsSystem(const int _nSessionID = 0);
    bool CreateAsLogonUser(const int _nSessionID = 0);
    void Free();

    bool Run(const CString &_sExeName, const CString &_sParam = _T(""), const WORD &_wShowOpt = SW_SHOW);
    bool RunAndWait(const CString &_sExeName, const CString &_sParam = _T(""), const WORD &_wShowOpt = SW_HIDE, DWORD *_pExitCode = NULL);

    HKEY GetHKCUKey();
    CString GetImpersonatedUserName() const;
    DWORD GetSessionID() const;

	BOOL		ChangeCreateProcess(TCHAR *pFilePath, BOOL bSessionID,DWORD dwILevel, DWORD dwWait = 0 );
	BOOL 	CreateProcessEx(CString strCommand, USHORT iShowMode =SW_SHOW,CString _strDeskTop = _T("") );
	BOOL		CreateProcess(CString strCommand, USHORT iShowMode =  SW_SHOW, CString _strDeskTop = _T(""));
	BOOL		RunProcessAndWait(CString sCmdLine,   CString sRunningDir, DWORD *nRetValue);
	BOOL		GetProcessExist(const CString &_sProcessName);
	BOOL		CreatProcessAndCompareStr(CString _strExeFullPath, char* pszCompareStr, CString _strCmd=_T(""));
	CString		CreatProcessAndGetConsole(CString _strExeFullPath, CString _strCmd=_T(""));
	BOOL		RunAsExecuteProcess(CString _strPath, CString _strParam, BOOL IsShow= TRUE, BOOL IsWait= FALSE);
	DWORD		GetParentPID();
	CString		GetProcessFullPath(DWORD _dwPid);
	BOOL		IsGoodParentProcess(CString _strParentPath);

	
private:
    CImpersonator(CImpersonator &_Im);

    HANDLE m_hToken;

    bool m_bImpersonated;   //!< Impersonate 되어있는지 구분
    bool m_bProfileLoaded;  //!< profile이 load되어 있는지 구분
    bool m_bLoadProfile;    //!< LoadProfile 사용여부 구분
    CString m_sUserName;    //!< user name
    DWORD m_dwSessionID;    //!< 주어진 session id
    PROFILEINFO m_ProfileInfo;

    bool m_bIsWinNT;
    bool m_bIs64bit;

    CString GetShellName() const;

    DWORD GetProcessSessionId(const DWORD &_dwProcessID) const;
    HANDLE GetProcessToken(const CString &_sProcessName);
    HANDLE GetNTProcessToken(const CString &_sProcessName);
   HANDLE Get64ProcessToken(const CString &_sProcessName);

    bool DoImpersonate();
    CString GetUserNameFromToken(const HANDLE &_hToken) const;
    bool UserProfileLoad();
    bool UserProfileUnLoad();
    CString GetCurrentUserName() const;
};
