#ifndef _CBASE64_H
#define _CBASE64_H

class CBase64  
{
public:
	CBase64();
	virtual ~CBase64();
//for setup.dat file encryption
	int Decode(CString &strBase64,BYTE abytOutput[]);
	int Decode(CString &strInput, CString &strOutput);

public:
	int base64decode(char *s, void *data);
	CString base64encode(const void *buf, int size);
	char *UrlEncode(char *str);
	int POS(char c);

//for setup.dat file encryption
	void EncodeBlock(BYTE abytRaw[], int iRawLength, int iOffset, BYTE abytOutput[]);
	char GetCharacter(int iSixBit);
	int GetValue(TCHAR chValue);

};

#endif // _CBASE64_H
