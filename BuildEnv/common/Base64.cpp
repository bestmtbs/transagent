#include "stdafx.h"
#include "Base64.h"

CBase64::CBase64()
{
}

CBase64::~CBase64()
{
}

int CBase64::POS(char c)
{
	if(c == '=') 
		return 64;

	if(isupper(c))
		return c - 'A';

	if(islower(c))
		return c - 'a' + 26;

	if(isdigit(c))
		return c - '0' + 52;

	if(c == '+')
		return 62;

	if(c == '/')
		return 63;

	return -1;
}

CString CBase64::base64encode(const void *buf, int size)
{
	char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	char *str = (char*)malloc((size+3)*4/3+1);
	char *p=str;
	unsigned char *q = (unsigned char*)buf;
	int i;
	int c;
	i=0;

	while(i<size)
	{
		c=q[i++];
		c*=256;

		if(i<size)
		  c+=q[i];

		i++;
		c*=256;

		if(i<size)
		  c+=q[i];

		i++;

		p[0]=base64[(c&0x00fc0000) >> 18];
		p[1]=base64[(c&0x0003f000) >> 12];
		p[2]=base64[(c&0x00000fc0) >> 6];
		p[3]=base64[(c&0x0000003f) >> 0];

		if(i>size)
		  p[3]='=';

		if(i>size+1)
		  p[2]='=';

		p+=4;
	}
	*p=0;

	CString strResult = _T("");
	//strResult.Format(_T("%s"), str);/*= str;*/
	strResult = str;
	//if (str) {
		free(str);
	//}
	return strResult;
}

int CBase64::base64decode(char *s, void *data)
{
	char *p;
	unsigned char *q;
	int n[4];

	if(strlen(s) % 4)
		return -1;

	q=(unsigned char*)data;

	for(p=s; *p; p+=4)
	{
		n[0] = POS(p[0]);
		n[1] = POS(p[1]);
		n[2] = POS(p[2]);
		n[3] = POS(p[3]);

		if((n[0] | n[1] | n[2] | n[3]) < 0)
		  return -1;

		if(n[0] == 64 || n[1] == 64)
		  return -1;

		if(n[2] == 64 && n[3] < 64)
		  return -1;

		q[0] = (n[0] << 2) + (n[1] >> 4);

		if(n[2] < 64)
		{
		  q[1] = ((n[1] & 15) << 4) + (n[2] >> 2);
		}

		if(n[3] < 64)
		{
		  q[2] = ((n[2] & 3) << 6) + n[3];
		}

		q+=3;
	}

	q -= (n[2] == 64) + (n[3] == 64);
	return q - (unsigned char*)data;
}

int CBase64::Decode(CString &strBase64,BYTE abytOutput[])
{
	int iPad = 0;
	int i;
	for ( i = strBase64.GetLength() - 1; strBase64.GetAt(i) == '='; i--)
	  iPad++;

	int iLength = strBase64.GetLength() * 6 / 8 - iPad;
	int iOutputIndex = 0;
	int iBlock;

	for (i = 0; i < strBase64.GetLength(); i += 4)
	{
		iBlock = (GetValue(strBase64.GetAt(i)) << 18) + (GetValue(strBase64.GetAt(i + 1)) << 12) +
				 (GetValue(strBase64.GetAt(i + 2)) << 6) + (GetValue(strBase64.GetAt(i + 3)));

		for (int j = 0; j < 3 ; j++)
			abytOutput[iOutputIndex + j] = (BYTE)((iBlock >> (8 * (2 - j))) & 0xff);

		iOutputIndex += 3;
	}
	return iLength;
}      

int CBase64::Decode(CString &strInput, CString &strOutput)
{
	int iExpectLength = strInput.GetLength()/4 * 3;

	iExpectLength = iExpectLength / sizeof(TCHAR);
	
	strOutput.Empty();
	BYTE *pbytOutputBuffer = (BYTE *)strOutput.GetBufferSetLength(iExpectLength);

	Decode(strInput, pbytOutputBuffer);

	strOutput.ReleaseBuffer();

	return strOutput.GetLength();
}      

void CBase64::EncodeBlock(BYTE abytRaw[], int iRawLength, int iOffset, BYTE abytOutput[])
{
	int iSlack = iRawLength - iOffset - 1;
	int end = (iSlack >= 2) ? 2 : iSlack;
	int iNeuter;
	BYTE bytTemp;
	int iBlock = 0;

	int i;
	for ( i = 0; i <= end; i++)
	{
		bytTemp = abytRaw[iOffset + i];
		iNeuter = (bytTemp < 0) ? bytTemp + 256 : bytTemp;
		iBlock += iNeuter << (8 * (2 - i));
	}

	int iSixbit;
	for (i = 0; i < 4; i++)
	{
		iSixbit = (iBlock >> (6 * (3 - i))) & 0x3f;
		abytOutput[i] = GetCharacter(iSixbit);
	}

	if (iSlack < 1)
		abytOutput[2] = '=';

	if (iSlack < 2)
		abytOutput[3] = '=';
}       

char CBase64::GetCharacter(int iSixBit)
{

	if (iSixBit >= 0 && iSixBit <= 25)
		return (char)('A' + iSixBit);

	if (iSixBit >= 26 && iSixBit <= 51)
		return (char)('a' + (iSixBit - 26));

	if (iSixBit >= 52 && iSixBit <= 61)
		return (char)('0' + (iSixBit - 52));

	if (iSixBit == 62)
		return '+';

	if (iSixBit == 63)
		return '/';

	return '?';      
}

int CBase64::GetValue(TCHAR chValue)
{
	if (chValue >= _T('A') && chValue <= _T('Z'))
		return chValue - _T('A');

	if (chValue >= _T('a') && chValue <= _T('z'))
		return chValue - _T('a') + 26;

	if (chValue >= _T('0') && chValue <= _T('9'))
		return chValue - _T('0') + 52;

	if (chValue == _T('+'))
		return 62;

	if (chValue == _T('/'))
		return 63;

	if (chValue == _T('=')) 
		return 0;

	return -1;
}

char *CBase64::UrlEncode(char *str) 
{
	char *encstr, buf[2+1];
	unsigned char c;
	int i, j;

	if(str == NULL) return NULL;
	if((encstr = (char *)malloc((strlen(str) * 3) + 1)) == NULL) 
		return NULL;

	for(i = j = 0; str[i]; i++) 
	{
		c = (unsigned char)str[i];
		if((c >= '0') && (c <= '9')) encstr[j++] = c;
		else if((c >= 'A') && (c <= 'Z')) encstr[j++] = c;
		else if((c >= 'a') && (c <= 'z')) encstr[j++] = c;
		else if((c == '@') || (c == '.') || (c == '/') || (c == '\\')
			|| (c == '-') || (c == '_') || (c == ':') ) 
			encstr[j++] = c;
		else 
		{
			sprintf(buf, "%02x", c);
			encstr[j++] = '%';
			encstr[j++] = buf[0];
			encstr[j++] = buf[1];
		}
	}
	encstr[j] = NULL;

	return encstr;
}
