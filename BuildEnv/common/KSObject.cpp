/*
 * FILE: KSObject.cpp
 * DESCRIPTION: KDK 라이브러리 기저 클래스 구현부
 * DESIGN: kisoo . kang
 * E-Mail: idelsoo@itnade.co.kr
 * DATE: 2006. 8. 15
 * LAST UPDATE: 2006. 8. 15
 */



#include "stdafx.h"
#include "KSObject.h"
#include <stdio.h>
#include <time.h>



/* +-------------------------------+
   | KSObject Class Implementation |
   +-------------------------------+ */

KSObject::KSObject()
{
	m_bLocked = false;
	::InitializeCriticalSection( &m_CriticalSection );
}

KSObject::~KSObject()
{
	::DeleteCriticalSection( &m_CriticalSection );
}


/* +------------------------------------+
   | KSSynchronize Class Implementation |
   +------------------------------------+ */

/* 생성과 동시에 임계구역 설정 */
KSSynchronize::KSSynchronize( KSObject* ptrObject )
{
	if ( ptrObject == 0 )
		return ;

	m_ptrObject = ptrObject;

	m_ptrObject->m_bLocked = true;
	::EnterCriticalSection( &(m_ptrObject->m_CriticalSection) );
}

/* 파괴와 동시에 임계구역 해제 */
KSSynchronize::~KSSynchronize()
{
	if ( m_ptrObject == 0)
		return ;

	::LeaveCriticalSection( &(m_ptrObject->m_CriticalSection) );
	m_ptrObject->m_bLocked = false;
}


