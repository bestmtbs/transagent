
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsNetwork.h 
 @brief     network 관련 function 정의 파일 
 @author    odkwon
 @date      create 2011.04.21

 @note      Network관련 필요한 함수를 모은 utils\n
            2008.09.11 IPv6관련 로컬아이피 구하기 추가  GetLocalIPEx, GetLocalIPListEx
            IPv6에서 변경된 네트워크 API http://www.vsix.net/other/faq/IPv6DNS/IP_address.htm
            로컬아이피선정 디폴트 알리즘에 대해 http://www.vsix.net/other/faq/IPv6DNS/IPv6_application.htm
 @todo      IsLocalIP, IsValidIP경우 Ipv6 포맷이 워낙다양해서 좀더 확인후 추가 예정\n
             GetLocalIPEx, GetLocalIPListEx경우 실제 ipv6망에서 테스트해봐야 함.\n
            Test XP환경에서는 ::1(127.0.0.1)로 나오는데 터널링에 의해 그렇게 나오는지 확인해야함
*/

#pragma once
#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <Wspiapi.h>

// 네트워크 관련 Utils을 추가하도록한다.

//IP 관련 함수들
bool IsValidIP(const CString &_sIP);
bool IsLocalIP(const  CString &_sIP);
bool GetLocallPList(CStringList &_IPList);
CString GetLocalIP();

// IPv6지원 로컬아이피 구하기 
CString GetLocalIPEx();
bool GetLocalIPListEx(CStringList &_IPList);
// IPv6지원 아이피 유효성 채크 
bool IsValidIPEx(const CString &_sIP);

//PC 이름 얻어오기
//odkwon Utils
CString GetPcName();