#pragma once

class CMemFileEx : public CMemFile
{
public:
    CMemFileEx(UINT _nGrowBytes = 1024);
    CMemFileEx(BYTE* _lpBuffer, UINT _nBufferSize, UINT _nGrowBytes = 0);

    virtual bool ReadFromFile(const CString &_sFilePath);
    virtual bool WriteToFile(const CString &_sFilePath);

    virtual BYTE* GetBuffer();

    virtual void ReadByte(BYTE *_lpBuf);
    virtual void ReadInt(INT *_lpBuf);
    virtual void ReadInt64(INT64 *_lpBuf);
    virtual void ReadWORD(WORD *_lpBuf);
    virtual void ReadDWORD(DWORD *_lpBuf);
    virtual void ReadString(LPTSTR _lpBuf = NULL);
    virtual void ReadString(CString &_sStr);
    virtual void ReadStringA2T(CString &_sStr);
    virtual void ReadStringA2T(LPTSTR _lpBuf = NULL);
    virtual void ReadStringT2A(LPSTR _lpBuf = NULL);
    virtual void ReadStrings(CStringList *_pStrList);
    virtual void ReadStream(CMemFileEx &_rStream);
    virtual void ReadDateTime(COleDateTime *_dtBuf);
    virtual void ReadDouble(double *_lpBuf);
    virtual void Readbool(bool *_lpBuf);


    virtual void WriteByte(BYTE _bValue); //
    virtual void WriteInt(INT _nValue);
    virtual void WriteInt64(INT64 _nValue);
    virtual void WriteWORD(WORD _wValue);
    virtual void WriteDWORD(DWORD _dwValue);
    virtual void WriteString(const CString &_szValue);
    virtual void WriteStringA2T(LPSTR _szValue);
    virtual void WriteStringT2A(const CString &_szValue);
    virtual void WriteStrings(const CStringList &_StrList);
    virtual void WriteStream(CMemFileEx *_pStream);
    virtual void WriteDateTime(COleDateTime &_rValue);
    virtual void WriteDouble(double _dValue);
    virtual void Writebool(bool _bValue);
};