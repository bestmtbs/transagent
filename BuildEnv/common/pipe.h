#ifndef _PIPE_DEFINE_H
#define _PIPE_DEFINE_H

#define PIPE_TIMEOUT 5000
#define BUFSIZE 4096
#define	PIPE_BUF_SIZE 4096

#define SE_RETURN_OK		    	_T("FS_OK")
#define SE_RETURN_ERR			_T("FS_ERR")
#define SE_RETURN_NULL			_T("FS_NULL")
#define SE_RETURN_TARY_BUSY		_T("FS_TARY_BUSY")		//2013.11.11 - hhh

#define SE_RETURN_ERROR_CODE					1000
#define SE_RETURN_ERROR_TRAY_BUSY				1001   //2013.11.11 - hhh

typedef struct _PIPE_HEADER
{
	LONG_PTR				dwAct;											     // 타입 정의

	LONG_PTR				dwMsg;											    // 메시지 정의

	BOOL						bStruct;												// 구조체 여부

	INT						nBufferCnt;										// 버퍼 카운트

	INT						nBufferSize;										// 버퍼 싸이즈

	CObList*				pObList;
	_PIPE_HEADER() {
		dwAct = 0;
		dwMsg = 0;
		bStruct = FALSE;
		nBufferCnt = 0;
		nBufferSize = 0;
		pObList = NULL;
	}

}PIPE_HEADER;

typedef struct _SHARE_DATA_WITH_COLLECT_AGENT
{
	DWORD		dwAct;
	BOOL		bState;
	DWORD		dwScreenIdx;
	TCHAR		szCollectType[64];	//	"FL"|"URL"
	DWORD		dwCollectDataSize;	//	szCollectType의 길이
	TCHAR		szExtionsionToIgnore[4096*2];	//	무시할 확장자 목록
	DWORD		dwExtionsionToIgnore;			//	길이
	TCHAR		szDomain[4096*2];	//	"naver.com"|"google.com"
	DWORD		dwDeleteFileSize;
	TCHAR		szDeleteFile[MAX_PATH];
	DWORD		dwDomainSize;		//	szDomain의 길이
	TCHAR		szTimeOnServer[SERVERTIME_SIZE];	// timestamp: yyyymmddhhmmssccc 최초 실행 후에는 받지 않으므로 default "".
	DWORD		dwUrlProcNameSize;		//	szUrlProcName의 길이
	TCHAR		szUrlProcName[4096];	// 필터링할 브라우저 이름
	DWORD		dwSplitSize;
	_SHARE_DATA_WITH_COLLECT_AGENT() {
		bState = AGENT_STATE_START;
		memset(szCollectType, 0x00, (64) * sizeof(TCHAR));
		dwCollectDataSize = 0;
		dwScreenIdx = 0;
		memset(szDomain, 0x00, (4096 * 2) * sizeof(TCHAR));
		dwDomainSize = 0;
		memset(szTimeOnServer, 0x00, SERVERTIME_SIZE * sizeof(TCHAR));
		dwExtionsionToIgnore = 0;
		memset(szExtionsionToIgnore, 0x00, (4096 * 2) * sizeof(TCHAR));
		dwDeleteFileSize = 0;
		memset(szDeleteFile, 0x00, (MAX_PATH) * sizeof(TCHAR));
		dwUrlProcNameSize = 0;
		memset(szUrlProcName, 0x00, (4096) * sizeof(TCHAR));
		dwSplitSize = 30;
	}
}SHARE_DATA_WITH_COLLECT_AGENT;

//typedef struct _SHARE_DELETE_FILE_DATA
//{
//	DWORD           dwAct;
//	DWORD              dwDeleteFileSize;
//	TCHAR              szDeleteFile[MAX_PATH * 2];
//	_SHARE_DELETE_FILE_DATA() {
//		dwAct = 0;
//		dwDeleteFileSize = 0;
//		memset(szDeleteFile, 0x00, (MAX_PATH * 2) * sizeof(TCHAR));
//	}
//}SHARE_DELETE_FILE_DATA;

typedef struct _SHARE_DATA
{
	LONG           dwAct;
	LONG           dwMsg;
	DWORD              dwBufferSize;
	TCHAR              szBuffer[4096*2];
	_SHARE_DATA() {
		dwAct = 0;
		dwMsg = 0;
		dwBufferSize = 0;
		memset(szBuffer, 0x00, (4096 * 2) * sizeof(TCHAR));
	}
}SHARE_DATA;

typedef struct _SHARE_THUMBNAIL_DATA
{
	LONG           dwAct;
	DWORD              dwBufferSize;
	BYTE              szBuffer[SEND_JPG_MAX_SIZE];
	_SHARE_THUMBNAIL_DATA() {
		dwAct = 0;
		dwBufferSize = 0;
		memset(szBuffer, 0x00, (SEND_JPG_MAX_SIZE) * sizeof(BYTE));
	}
}SHARE_THUMBNAIL_DATA;

typedef struct 
{ 
   OVERLAPPED oOverlap; 
   HANDLE hPipeInst; 
   unsigned char chRequest[BUFSIZE]; 
   DWORD cbRead;
   TCHAR chReply[BUFSIZE];
   DWORD cbToWrite; 
   DWORD dwState; 
   BOOL fPendingIO; 
} PIPEINST, *LPPIPEINST; 

//파이프 이름
#define	STOC_SYS_PIPE_NAME 	_T("\\\\.\\Pipe\\STOC_SYS_PIPE")
#define	STOC_USR_PIPE_NAME 	_T("\\\\.\\Pipe\\STOC_USR_PIPE")
#define	CTOS_PIPE_NAME 	_T("\\\\.\\Pipe\\CTOS_PIPE")
#define PTOA_PIPE_NAME	_T("\\\\.\\Pipe\\PTOAPIPE")
#define ATOR_PIPE_NAME	_T("\\\\.\\Pipe\\ATORPIPE")

//파이프 메세지 통신
//SendAgent -> CollectAgent
#define ACT_POLICY_CHANGE		0x00000001
#define ACT_EXIT_COLLECT_AGENT	0x00000010
#define ACT_SYNC_SERVER_TIME	0x00000011
#define ACT_INITIAL_FILE_INDEX	0x00000100
#define ACT_THUMBNAIL_START		0x00000101
#define ACT_THUMBNAIL_STOP		0x00000110
#define ACT_DELETE_FILE			0x00000113
#define ACT_SEND_THUMBNAIL		0x00000114

//UPDATE -> AGENT
#define ACT_AGENT_UPDATE_END					0x00000001
#define ACT_AGENT_UPDATE_LAST					0x00000002
#define ACT_AGENT_UPDATE_COMPLETE				0x00000011
#define ACT_AGENT_UPDATE_FAIL					0x00000012
#define ACT_AGENT_UPDATE						0x00000022
#define ACT_UPDATE_COMPLETE_RESTART				0x00000100
#define ACT_KILL_PROCESS						0x00000101
//#define ACT_KILL_COLLECT_AGENT					0x00000102

#endif
