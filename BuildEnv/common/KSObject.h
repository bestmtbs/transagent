/*
 * FILE: KSObject.h
 * DESCRIPTION: KS Library의 모든 클래스들의 기저 클래스(Stack, List 구조를 지원하고 쓰레드 동기화 자원을 제공한다.)
 * DESIGN: kisoo . kang
 * E-Mail: idelsoo@itnade.co.kr
 * DATE: 2006.8.15
 * LAST UPDATE: 2006.8.15
 */
#if !defined(AFX_KSOBJECT_H__F38FAAFB_A76D_4897_8892_FC30C8055D93__INCLUDED_)
#define AFX_KSOBJECT_H__F38FAAFB_A76D_4897_8892_FC30C8055D93__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#pragma once
#include <typeinfo.h>
#include <windows.h>


class KSSynchronize;
const int NOTLINKED = -1;
/* For Static Linked list */
typedef struct OBJECTNODE
{
	int			prev;
	int			next;

	/* Default Constructor */
	OBJECTNODE() {
		prev = NOTLINKED;
		next = NOTLINKED;
	};

}*LPOBJECTNODE;
/**
	@file	KSObject.h
	@brief	모든 클래스가 동일한 최상위 부모 클래스를 갖기 위한 클래스 정의 

	@author	최초 작성자 : 강기수
	@author 마지막 수정자 : 강기수

	@date 최초 작성일 : 2006.08.01
	@date 마지막 수정일 : 2006.09.07

	모든 클래스가 동일한 최상위 부모 클래스를 갖기 위한 클래스이며, 
	쓰레드 동기화 기능을 제공한다.
*/
class  KSObject
{
friend KSSynchronize;

public:
	/* Default Constructor & Destructor */
	KSObject();
	~KSObject();

	bool isLocked( void ) { return m_bLocked; };	/* Is Locked this object? */

/* Private Member Attribute */
private:
	CRITICAL_SECTION m_CriticalSection;		/* For Thread Synchronization */
	bool m_bLocked;
};
/* 동기화 클래스 : 정적으로 생성해서 사용한다. */
class  KSSynchronize
{
public:
	KSSynchronize( KSObject* ptrObject );
	~KSSynchronize();

private:
	KSObject* m_ptrObject;
};
#endif // !defined(AFX_KSOBJECT_H__F38FAAFB_A76D_4897_8892_FC30C8055D93__INCLUDED_)
