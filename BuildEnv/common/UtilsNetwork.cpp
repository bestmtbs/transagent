
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsNetwork.cpp 
 @brief     network 관련 function 정의 파일 
 @author    odkwon
 @date      create 2011.04.21

 @note      Network관련 필요한 함수를 모은 utils
*/

#include "stdafx.h"
#include "UtilsNetwork.h"

/**
 @brief     로컬 IP 리스트를 얻어온다. ipv6 미지원
 @author    odkwon
 @date      2008-04-18
 @param     [out]_IPList 로컬 PC IP리스트를 리턴한다.
 @return    true/false
 */
bool GetLocallPList(CStringList &_IPList)
{
    CString stmpIP = _T("");
    const int MAX_HOSTLEN = 255;
    char pHostName[MAX_HOSTLEN] = {0,};
    WORD wVersionRequested;
    WSADATA wsaData;
    try
    {
        wVersionRequested = MAKEWORD(2, 2); 
        ::WSAStartup(wVersionRequested, &wsaData);

        _IPList.RemoveAll();
        if (SOCKET_ERROR == gethostname(pHostName, MAX_HOSTLEN)) 
            return false;
        HOSTENT *phostEnt = gethostbyname(pHostName);
        int i = 0;

        SOCKADDR_IN sockaddr;
        sockaddr.sin_family=AF_INET;
        
        while(phostEnt->h_addr_list[i])
        {
            memcpy(&sockaddr.sin_addr,phostEnt->h_addr_list[i], sizeof(u_long));
            stmpIP = inet_ntoa( sockaddr.sin_addr );
            _IPList.AddTail(stmpIP);
            i++;
        } // while(phostEnt->h_addr_list[i])
    }
    catch(...)
    {
    }
    ::WSACleanup();

    return (!_IPList.IsEmpty());
}

/**
 @brief     로컬 IP를 얻어온다.. ipv6 미지원
 @author    odkwon
 @date      2008-04-18
 @return    CString 로컬PC 맨 상위 아이피 하나가 리턴된다.
 */
CString GetLocalIP()  
{
    CString DefaultIP = _T("0.0.0.0");
    CStringList IPList;
    if (GetLocallPList(IPList)) 
    {
        POSITION IPPos = IPList.GetHeadPosition();
        return IPList.GetNext(IPPos);
    }
    return DefaultIP;
}

/**
 @brief     ip가 로컬 아이피 리스트에 있는지 판단한다. . ipv6 미지원
 @author    odkwon
 @date      2008-04-18
  @return   true/false
 */
bool IsLocalIP(const CString &_sIP)
{    
    
    if (_T("127.0.0.1") == _sIP) return true;
    if (!IsValidIP(_sIP)) return false;
    // To.Do ipv6일경우 127 추가
    CStringList strIPList;
    GetLocallPList(strIPList);

    if (NULL != strIPList.Find(_sIP)) return true;
    
    return false;
}

/**
 @brief     IP가 정상인지 판단한다. ipv6 미지원
 @author    odkwon
 @date      2008-04-18
 @param     [in]_sIP IP값으로 "172.1.4.5"형식으로 들어온다.
 @return    true/false
 */
bool IsValidIP(const CString &_sIP)
{
    const INT MAXIP_LENTH = 24;
    const INT IPV6_COUNT  = 6;
    const INT IPV4_COUNT  = 4;

    INT Startpos = 0;
    INT TokenCount = 0;
    if (_T("") == _sIP) return false;
    if (MAXIP_LENTH < _sIP.GetLength()) return false;
    CString sTokenStr = _T("");

    while(-1 != Startpos)
    {
        sTokenStr = _sIP.Tokenize(_T("."),Startpos);
        if (-1 == Startpos) break;
        TokenCount++;
        int nIPToken = _tstoi(sTokenStr);
        if ((0 >= nIPToken) && (_T("0") != sTokenStr)) return false;
        if (255 < nIPToken) return false;
        sTokenStr = _T("");
    } 
    return (IPV6_COUNT == TokenCount) || (IPV4_COUNT == TokenCount);
}

/**
 @brief     IP가 정상인지 판단한다. ipv6 지원
 @author    odkwon
 @date      2008.09.16
 @param     [in]_sIP IP값으로 IPv4 와 IPv6 형식을 지원한다.
 @return    true/false
 @note      WSAStringToAddress를 이용하여 ipv4, ipv6 형식으로 유효한지 판단한다.\n
            WSAStringToAddress는 String형 IP주소를 SocketAddr 구조체로 변환시켜주는 함수이다.\n
            IP 포맷에 맞지않은 String의 경우 SOCKET_ERROR를 리턴한다..\n
            특이사항 ipv4에서는 "IP:Port"도 올바른 IP라 인식한다. SocketAddr구조체에 ip,port내용을 할당한다.\n
            이함수를 정식으로 사용할 경우 : 문자열을 체크해서 빼내도 된다.\n
            실제 IPv6 포맷으로 다양한 Test가 필요함.\n
 */
bool IsValidIPEx(const CString &_sIP)
{
    WSADATA wsa;
    if (0 != ::WSAStartup(MAKEWORD(2, 2), &wsa)) 
        return false;
    INT nRet = 0;
    CString sIP = _T("");
    struct sockaddr_storage ssSockStorage;
    INT nScokAddrLen = 0;
    nScokAddrLen = sizeof(ssSockStorage);   
    ::ZeroMemory(&ssSockStorage, sizeof(ssSockStorage));

    sIP = _sIP;
    nRet = ::WSAStringToAddress(sIP.GetBuffer(_sIP.GetLength()), AF_INET, NULL,
        reinterpret_cast<struct sockaddr*>(&ssSockStorage), &nScokAddrLen);
    sIP.ReleaseBuffer();
    
    if ((SOCKET_ERROR != nRet))
    {
        ::WSACleanup();
        return true;
    }
    nScokAddrLen = sizeof(ssSockStorage);   
    ::ZeroMemory(&ssSockStorage, sizeof(ssSockStorage)); 
    nRet = ::WSAStringToAddress(sIP.GetBuffer(_sIP.GetLength()), AF_INET6, NULL, 
        reinterpret_cast<struct sockaddr*>(&ssSockStorage), &nScokAddrLen);
    sIP.ReleaseBuffer();
    if ((SOCKET_ERROR != nRet))
    {
        ::WSACleanup();
        return true;
    }
    return false;
}

/**
 @brief     로컬 IP를 얻어온다. IPv6 지원
 @author    odkwon
 @date      2008.09.11
 @return    CString 로컬PC 맨 상위 아이피 하나가 리턴된다.
 @note      ipv4, ipv6를 같이 쓰는 경우에는 getaddrinfo api특성상 IPv6가 우선적을 얻어진다.\n
            문서에서도 getaddrinfo에서 얻어지는 처음 IP를 로컬아이피로 선택한다고 기술되어 있다.\n

 */
CString GetLocalIPEx()  
{
    CString DefaultIP = _T("0.0.0.0");
    CStringList IPList;
    if (GetLocalIPListEx(IPList)) 
    {
        POSITION IPPos = IPList.GetHeadPosition();
        return IPList.GetNext(IPPos);
    }
    return DefaultIP;
}

/**
 @brief     로컬 IP를 얻어온다. IPv6 지원
 @author    odkwon
 @date      2008.09.11
 @return    CString 로컬PC 맨 상위 아이피 하나가 리턴된다.
 @note      ipv4, ipv6를 같이 쓰는 경우에는 getaddrinfo api특성상 IPv6가 우선적을 얻어진다.
 */
bool GetLocalIPListEx(CStringList &_IPList)
{
    const int MAX_HOSTLEN = 256;
    const int HINTFLAG    = 0x00000400 ;
    WSADATA wsa;
    if (0 != ::WSAStartup(MAKEWORD(2, 2), &wsa)) 
        return false;
    
    ADDRINFO aiHints, *aiAddrInfo = NULL, *AI = NULL;

    char szAddrName[NI_MAXHOST];
    char szHostName[MAX_HOSTLEN];

    ::ZeroMemory(szAddrName, NI_MAXHOST);
    ::ZeroMemory(szHostName, MAX_HOSTLEN);
    ::ZeroMemory(&aiHints, sizeof(ADDRINFO));

    aiHints.ai_family = AF_UNSPEC;
    aiHints.ai_socktype = SOCK_STREAM;
    aiHints.ai_flags = HINTFLAG;
    aiHints.ai_protocol = IPPROTO_TCP;

    if (SOCKET_ERROR == gethostname(szHostName, MAX_HOSTLEN)) 
        return false;
    if (0 != getaddrinfo(szHostName, NULL, &aiHints, &aiAddrInfo))
        return false;
    for (AI=aiAddrInfo; AI!=NULL; AI=AI->ai_next) 
    {
        if ((PF_INET6 != AI->ai_family) && (PF_INET != AI->ai_family))
            continue;
        if (0 != getnameinfo(reinterpret_cast<LPSOCKADDR>(AI->ai_addr), static_cast<socklen_t>(AI->ai_addrlen), 
            szAddrName, sizeof(szAddrName), NULL, 0, NI_NUMERICHOST)) 
        {
            continue;
        }
        CString sAddr = _T("");
        sAddr = szAddrName;
        _IPList.AddTail(sAddr);
        continue;
    }
    freeaddrinfo(aiAddrInfo);
    ::WSACleanup();
    return (!_IPList.IsEmpty());
}


/**
 @brief     PC명을 얻어온다.
 @author    odkwon
 @date      2008.09.11
 @return    로컬 PC이름을 얻어온다.
 */
CString GetPcName()
{
    TCHAR szPcName[MAX_COMPUTERNAME_LENGTH + 1] = {0, };
    DWORD nSize = MAX_COMPUTERNAME_LENGTH + 1;
    ::GetComputerName(szPcName, &nSize);
    return szPcName;
}