
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      StringListEx.h 
 @brief     StringListEx Class 정의 파일

            MFC의 CStringList 기능을 확장한 클래스 

 @author    hang ryul lee
 @date      create 2011.07.09
*/

#pragma once

class CStringListEx : public CStringList
{
public:
    CString GetText(const CString _sToken = _T("\n"));
    CString GetName(POSITION &_rPosition);
    CString GetValue(const CString &_sName);
    void GetNameAndValue(POSITION &_rPosition, CString &_sName, CString &_sValue);

    void SetText(const CString &_sFullString, const CString _sToken = _T("\n"));
private:
};