/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.06.06		
 ********************************************************************************/
/**
 @file      UtilsUnicode.cpp 
 @brief     Unicode Utilities function 구현 파일 
 @author    hang ryul lee
 @date      create 2011.06.06 

 @note 
*/

#include "stdafx.h"

// CP949 기준으로 함


#ifndef _CHAR_CP949
#define _CHAR_CP949

#define _CP949_2BYTE_START			0x80
#define	LATIN_CAPITAL_LETTER_A		0x41
#define	LATIN_CAPITAL_LETTER_Z		0x5A
#define LATIN_SMALL_LETTER_A		0x61
#define LATIN_SMALL_LETTER_Z		0x7A
#endif



LPSTR UniToAnsi (LPWSTR		pwzStr)
{
	int			nRes = 0;
	int			nUniLen = 0;
	int			nAnsiLen = 0;
	LPSTR		pzStr = NULL;


	if( ( pwzStr == NULL) || ( ( nUniLen = wcslen( pwzStr)) == 0)) return NULL;

	nAnsiLen = WideCharToMultiByte( CP_ACP, 0, pwzStr, nUniLen, 0, 0, 0, 0);

	pzStr = ( LPSTR) calloc( 1, sizeof( char) * ( nAnsiLen + 1));

	if( pzStr == NULL) 
		return NULL;
	
	//
	// return	 : (성공) 이고 (d) 가 0 이 아니면 리턴값은 쓰여진 ansi 길이
	//			 : (성공) 이고 (d) 가 0 이면 리턴값은 요구되는 버퍼길이
	//			 : (실패) 면 리턴값 0
	//
	//														(d)
	nRes = WideCharToMultiByte( CP_ACP, 0, pwzStr, nUniLen, pzStr, nAnsiLen + 1, 0, 0);
	//
	// fail
	//
	if( nRes == 0) {

		if(pzStr != NULL)
		{
			free(pzStr);
		}

		return NULL;
	}
	else if( ( nRes > 0) && ( nAnsiLen != 0)) {

		pzStr[ nAnsiLen] = 0;

		return pzStr;

	}
	else {

		if(pzStr != NULL)
		{
			free(pzStr);
		}

		return NULL;
	}
}


/**
@brief     유니코드 문자열을 ansi 문자열로 변환
@author    jhlee
@date      2011/06/16
@param     [out] pszDest : ansi  문자열
@param     [IN] pszSrc : unicode 문자열
@return    성공하면 TRUE
*/

BOOL UnicodeToAnsi(LPSTR pszDest, LPCTSTR pszSrc, SIZE_T size)
{ 
	int nSizeMem = wcslen(pszSrc)*2+2;
	char* szAnsiBuff = new char[nSizeMem];
	if(!szAnsiBuff)
		return FALSE;

	memset(szAnsiBuff,0,nSizeMem);

	int nSize = 0;
	nSize = ::WideCharToMultiByte(CP_ACP, 0, pszSrc, -1, NULL, 0, NULL, NULL);
	if(nSize>0)
		nSize = ::WideCharToMultiByte(CP_ACP, 0, pszSrc, -1, szAnsiBuff, nSize, NULL, NULL);
	else
		szAnsiBuff[0]=NULL;

	strcpy_s(pszDest,size, szAnsiBuff);

	if( szAnsiBuff )
		delete [] szAnsiBuff;

	return TRUE;
}

/**
 @brief      
 @author    hang ryul lee
 @date       2011.08.01
 @param     
*/
LPWSTR AnsiCodeToUniCode(char* _pzText)
{
	int				nRes = 0;
	int				nAnsiLen = 0;
	int				nUniLen = 0;
	LPWSTR			pwzNewText = NULL;

	
	if( ( _pzText == NULL) || ( ( nAnsiLen = strlen( _pzText)) == 0)) return NULL;


	nUniLen = MultiByteToWideChar( CP_ACP, 0, _pzText, nAnsiLen, 0, 0);

	pwzNewText = ( LPWSTR) calloc( 1, sizeof( WCHAR) * ( nUniLen + 1));
	if( pwzNewText == NULL) return NULL;

	//
	// parameter : (b) 가 -1 이면 길이 자동계산
	//			   (d) 가 0 이면 필요로 하는 버퍼 크기 리턴
	//
	// return	 : (성공) 이고 (d) 가 0 이 아니면 리턴값은 쓰여진 uni 길이
	//			 : (성공) 이고 (d) 가 0 이면 리턴값은 요구되는 버퍼길이
	//			 : (실패) 면 리턴값 0
	//
	//										(a)     (b) (c)         (d)
	nRes = MultiByteToWideChar( CP_ACP, 0L, _pzText, -1, pwzNewText, nUniLen + 1);
	//
	// fail
	//
	if( nRes == 0) {

		if( pwzNewText)
		{
			delete pwzNewText;
			pwzNewText = NULL;
		}

		return NULL;
	}
	else if( ( nRes > 0) && ( nUniLen != 0)) {

		pwzNewText[ nUniLen] = 0;

		return pwzNewText;

	}
	else {

		if( pwzNewText)
		{
			delete pwzNewText;
			pwzNewText = NULL;
		}

		return NULL;
	}

}



/**
 @brief      완성형 한글만 지원되도록 수정		
 @author    hang ryul lee
 @date       2011.08.01
 @param     
*/
BOOL IsHan ( 
		  const char *	pzStr,		// [I]
		  int			nOpt		// [I] 사용안함
)
{
	unsigned int	nTest = 0;
	unsigned char	cTmp = 0;


	cTmp = ( unsigned char)pzStr[ 0];

	if( cTmp < _CP949_2BYTE_START) return FALSE;

	nTest = ( int)cTmp;	
	
	nTest = nTest << 8;

	cTmp = ( unsigned char)pzStr[ 1];

	nTest |= cTmp;

	if( ( ( nTest >= 0x8141) && ( nTest <= 0xA0FE)) || ( ( nTest >= 0xB041) && ( nTest <= 0xC5FE))) {

		unsigned int nRmn = nTest % 0x100;

		if( ( nRmn >= 0x0) && ( nRmn <= 0x40))			return FALSE;
		else if( ( nRmn >= 0x5B) && ( nRmn <= 0x60))	return FALSE;
		else if( ( nRmn >= 0x7B) && ( nRmn <= 0x80))	return FALSE;
		else if( nRmn == 0xFF)							return FALSE;
		else											return TRUE;
	}

	else if( ( nTest >= 0xA141) && ( nTest <= 0xAFA0)) {

		unsigned int nRmn = nTest % 0x100;

		if( ( nRmn >= 0x0) && ( nRmn <= 0x40))			return FALSE;
		else if( ( nRmn >= 0x5B) && ( nRmn <= 0x60))	return FALSE;
		else if( ( nRmn >= 0x7B) && ( nRmn <= 0x80))	return FALSE;
		else if( nRmn >= 0xA1)							return FALSE;
		else											return TRUE;
	}

	else if( ( nTest >= 0xC641) && ( nTest <= 0xC652))	return TRUE;
	else if( ( nTest >= 0xC6A1) && ( nTest <= 0xC6FE))	return TRUE;
	else if( ( nTest >= 0xC7A1) && ( nTest <= 0xC7FE))	return TRUE;
	else if( ( nTest >= 0xC8A1) && ( nTest <= 0xC8FE))	return TRUE;

	else return FALSE;
}

/**
 @brief      
 @author    hang ryul lee
 @date       2011.08.01
 @param     
*/
BOOL IsHanStr(const char* pzInStr)
{
	size_t		nIdx = 0;
	char *		pcCh = ( char *)&pzInStr[ nIdx];


	while( nIdx < strlen( pzInStr)) {

		if( !IsHan( pcCh, 0)) return FALSE;

		nIdx += 2;

		pcCh = ( char *)&pzInStr[ nIdx];
	}

	if( nIdx > 0) return TRUE;
	else return FALSE;

	return TRUE;
}
