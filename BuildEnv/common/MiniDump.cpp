
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      MiniDump.cpp 
 @brief     CMiniDump Class 구현 파일

            DbgHelp.dll의 MiniDumpWriteDump를 사용하여 MiniDump파일을 생성.\n
            CMiniDump Class는 Singleton Pattern으로 작성되었음.

 @author    hang ryul lee
 @date      create 2011.06.16 
 @note      SetUnhandledExceptionFilter를 호출하여 지정한 예외필터는 프로세스에
            전역적이다.
 @note      DbgHelp.dll 버전 5.1.2600 이상에 MiniDumpWriteDump 함수가 포함되어있다.
 @note      DbgHelp.dll 버전 6.0 이전에는 현재 프로세스에서 미니덤프 파일을 작성하기
            위해 호출할때 교착상태가 발생하는 버그가 존재하였음.
 @note      DbgHelp.dll 버전 6.1.17.1 이후 버전 사용을 권장함.
 @note      DbgHelp.dll은 Debugging Tools for Windows의 구성요소이다.
            (http://www.microsoft.com/whdc/devtools/debugging/default.mspx)
 @note      미니덤프 타입값은 http://msdn.microsoft.com/en-us/library/ms680519(VS.85).aspx 을 참고 
*/

#include "stdafx.h"
#include "MiniDump.h"
#include "UtilsFile.h"
#include "Convert.h"

CMiniDump *CMiniDump::s_pInstance = NULL;


/**
 @brief     생성자
 @author    hang ryul lee
 @date      2008.06.16
 @param     [in] _nDumpType     미니덤프 파일 타입(Default = MiniDumpNormal)
 @param     [in] _sPath         미니덤프 저장 경로(Default = _T(""))
 @param     [in] _sFilePrefix   미니덤프파일 Prefix(Default = _T(""))
 @note      파라미터 _sPath가 _T("")이면 미니덤프 저장 경로는 현재 모듈의 경로가 된다.
 @note      파라미터 _sFilePrefix가 _T("")이면 미니덤프 파일 Prefix는 현재 모듈명에서 확장자를 제거한값이 된다.\n
            ex) 모듈명 : Test.exe -> Prefix : Test)
*/
CMiniDump::CMiniDump(MINIDUMP_TYPE _nDumpType /* = MiniDumpNormal */, const CString &_sPath /* = _T("") */, const CString &_sFilePrefix /* = _T("") */)
{
    if (NULL != s_pInstance)
        return;

    CString sCurModuleName = GetCurrentModuleFileName();

    m_nDumpType = _nDumpType;
    m_sPath = _sPath;
    if (_T("") == m_sPath)
    {
        m_sPath = ExtractFilePath(sCurModuleName);        
    }
    m_sPath = IncludeTrailingBackslash(m_sPath);
    
    m_sFilePrefix = _sFilePrefix;
    if (_T("") == m_sFilePrefix)
    {
        m_sFilePrefix = ExtractBaseFileName(sCurModuleName);
    }
    m_sFilePrefix = m_sFilePrefix;
    
    m_nPrevErrorMode = ::SetErrorMode(SEM_FAILCRITICALERRORS);
    m_pPrevFilter = ::SetUnhandledExceptionFilter(WriteCrashDump);

    s_pInstance = this;
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2008.06.16
*/
CMiniDump::~CMiniDump()
{
    if (NULL != s_pInstance)
    {
        ::SetErrorMode(m_nPrevErrorMode);
        ::SetUnhandledExceptionFilter(m_pPrevFilter);
    }

    s_pInstance = NULL;
}


/**
 @brief     미니덤프 저장 경로를 구한다..
 @author    hang ryul lee
 @date      2008.06.16
 @return    마지막에 역슬레쉬(\)가포함된 미니덤프 저장경로
*/
CString CMiniDump::GetPath() const
{
    return m_sPath;
}

/**
 @brief     미니덤프 파일 Prefix를 구한다..
 @author    hang ryul lee
 @date      2008.06.16
 @return    미니덤프 파일 Prefix값
*/
CString CMiniDump::GetFilePrefix() const
{
    return m_sFilePrefix;
}

/**
 @brief     미니덤프 타입을 구한다.
 @author    hang ryul lee
 @date      2008.06.16
 @return    미니덤프 타입값
 @note      미니덤프 타입값은 http://msdn.microsoft.com/en-us/library/ms680519(VS.85).aspx 을 참고 
*/
MINIDUMP_TYPE CMiniDump::GetDumpType() const
{
    return m_nDumpType;
}

/**
 @brief     미니덤프 타입을 설정한다.
 @author    hang ryul lee
 @date      2008.06.19
 @param     [in] _nDumpType 미니덤프 타입값
*/
void CMiniDump::SetDumpType(MINIDUMP_TYPE _nDumpType)
{
    m_nDumpType = _nDumpType;
}

/**
 @brief     예외가 발생되었을때 호출되어 미니덤프를 작성하는 콜백함수.
 @author    hang ryul lee
 @date      2008.06.16
 @param     [in] _pExceptionInfo  예외정보를 담고있는 구조체 포인터
 @return    
*/
LONG WINAPI CMiniDump::WriteCrashDump(PEXCEPTION_POINTERS _pExceptionInfo)
{
    if (NULL == s_pInstance) 
        return EXCEPTION_CONTINUE_SEARCH;

    if (NULL == _pExceptionInfo)
        return EXCEPTION_CONTINUE_SEARCH;

    typedef BOOL (WINAPI *fpMiniDumpWriteDump)(HANDLE, DWORD, HANDLE, MINIDUMP_TYPE, PMINIDUMP_EXCEPTION_INFORMATION, PMINIDUMP_USER_STREAM_INFORMATION, PMINIDUMP_CALLBACK_INFORMATION);
    fpMiniDumpWriteDump pMiniDumpWriteDump = NULL;

    HMODULE hModule = ::LoadLibrary(_T("DbgHelp.dll"));
    if (NULL == hModule)
        return EXCEPTION_CONTINUE_SEARCH;

    pMiniDumpWriteDump = reinterpret_cast<fpMiniDumpWriteDump>(::GetProcAddress(hModule, "MiniDumpWriteDump"));
    if (NULL == pMiniDumpWriteDump)
    {
        ::FreeLibrary(hModule);
        return EXCEPTION_CONTINUE_SEARCH;
    }

    bool bOk = true;
    s_pInstance->OnShowDialog(bOk);
    if (!bOk)
    {
        ::FreeLibrary(hModule);
        return EXCEPTION_EXECUTE_HANDLER;
    }

    if (!DirectoryExists(s_pInstance->GetPath()))
        ForceCreateDir(s_pInstance->GetPath());


    CString sNow = CConvert::ToString(COleDateTime::GetCurrentTime(), _T("%Y-%m-%d-%H-%M-%S"));
    CString sDumpFile = s_pInstance->GetPath() + s_pInstance->GetFilePrefix() + _T("_") + sNow + _T(".dmp");
    HANDLE hFile = ::CreateFile(sDumpFile, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (INVALID_HANDLE_VALUE == hFile)
    {
        ::FreeLibrary(hModule);
        return EXCEPTION_CONTINUE_SEARCH;
    }

    MINIDUMP_EXCEPTION_INFORMATION ExceptionInfo;
    ExceptionInfo.ThreadId = ::GetCurrentThreadId();
    ExceptionInfo.ExceptionPointers = _pExceptionInfo;
    ExceptionInfo.ClientPointers = FALSE;

    BOOL bDump = FALSE;
    bDump = pMiniDumpWriteDump(::GetCurrentProcess(), ::GetCurrentProcessId(), hFile, s_pInstance->GetDumpType(), &ExceptionInfo, NULL, NULL);
    if (bDump)
    {        
        s_pInstance->OnAfterWriteDump(sDumpFile);
    }
    else
    {
        DWORD dwError = ::GetLastError();
        s_pInstance->OnErrorWriteDump(sDumpFile, dwError);
    }

    ::CloseHandle(hFile);
    ::FreeLibrary(hModule);

    return EXCEPTION_EXECUTE_HANDLER;
}

/**
 @brief     미니덤프 파일을 작성하기전에 사용자에게 확인을 구하는창을 표시하기위한 가상함수.
 @author    hang ryul lee
 @date      2008.06.16
 @param     [in/out] _bOk  미니덤프 작성을 계속 진행할지 결정하는 값
 @note      파라미터 _bOk를 false로 설정하면 미니덤프파일을 작성하지 않고 예외처리는 종료된다.
 @note      이함수를 재정의하여 각 제품에 맞는 오류공지 및 확인 처리를 하면된다.
*/
void CMiniDump::OnShowDialog(bool &_bOk)
{
    UNUSED_ALWAYS(_bOk);    
}

/**
 @brief     미니덤프 파일이 작성된 이후 처리를위한 가상함수.
 @author    hang ryul lee
 @date      2008.06.16
 @param     [in] _sFileName  미니덤프 파일명
*/
void CMiniDump::OnAfterWriteDump(const CString &_sFileName)
{
    UNUSED_ALWAYS(_sFileName);
}


/**
 @brief     미니덤프 파일이 작성이 실패한 경우 호출되는 가상함수.
 @author    hang ryul lee
 @date      2008.06.16
 @param     [in] _sFileName  미니덤프 파일명
 @param     [in] _dwError    오류 코드
 @note      MiniDumpWriteDump API호출이 실패할 경우 호출된다.
*/
void CMiniDump::OnErrorWriteDump(const CString &_sFileName, DWORD _dwError)
{
    UNUSED_ALWAYS(_sFileName);
    UNUSED_ALWAYS(_dwError);
}