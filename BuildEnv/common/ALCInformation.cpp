#include "StdAfx.h"
#include "ALCInformation.h"
#include <Strsafe.h>
#include <Aclapi.h>

CALCInformation::CALCInformation(void)
{
	m_strPath =L"";
}

CALCInformation::~CALCInformation(void)
{
}

/**
 @brief     파일 또는 폴더 경로 입력
 @author    hhh
 @date      2013.11.06
 @param     [in] _strPath    ACL정보를 확인할 파일 또는 폴더 경로 
*/
void CALCInformation::SetPath(CString _strPath)
{
	m_strPath = _strPath;
}


/**
 @brief     파일 또는 폴더로 부터 ACL 정보를 구해온다.
 @author    hhh
 @date      2013.11.06
 @param     [_out] pSecDescriptor    access control list (ACL).
*/
HRESULT CALCInformation::Acl_Query(BYTE** pSecDescriptor)
{
	CString strLog;
	BOOL bSuccess = TRUE;
	BYTE* pSecDescriptorBuf;
	DWORD dwSizeNeeded = 0;

	// clear any previously queried information
	//ClearAceList();

	// Find out size of needed buffer for security descriptor with DACL
	// DACL = Discretionary Access Control List
	bSuccess = GetFileSecurity(m_strPath,
								DACL_SECURITY_INFORMATION,
								NULL,
								0,
								&dwSizeNeeded);

	if (0 == dwSizeNeeded)
	{
		return E_FAIL;
	}
	pSecDescriptorBuf = new BYTE[dwSizeNeeded];

	// Retrieve security descriptor with DACL information
	bSuccess = GetFileSecurityW(m_strPath,
								DACL_SECURITY_INFORMATION,
								pSecDescriptorBuf,
								dwSizeNeeded,
								&dwSizeNeeded);

	// Check if we successfully retrieved security descriptor with DACL information
	if (!bSuccess)
	{		
		if(pSecDescriptorBuf != NULL)
		{
			delete pSecDescriptorBuf;
			pSecDescriptorBuf = NULL;
		}
		return E_FAIL;
	}
	
	// Getting DACL from Security Descriptor
	PACL pacl;
	BOOL bDaclPresent, bDaclDefaulted;
	bSuccess = GetSecurityDescriptorDacl((SECURITY_DESCRIPTOR*)pSecDescriptorBuf,
										 &bDaclPresent, &pacl, &bDaclDefaulted);
	
	// Check if we successfully retrieved DACL
	if (!bSuccess)
	{
		strLog.Format(_T("Failed to retrieve DACL from security descriptor (%d)"), GetLastError());
		OutputDebugString(strLog);
		if(pSecDescriptorBuf != NULL)
		{
			delete pSecDescriptorBuf;
			pSecDescriptorBuf = NULL;
		}
		return E_FAIL;
	}
	
	// Check if DACL present in security descriptor
	if (!bDaclPresent)
	{
		OutputDebugString(_T("DACL was not found."));
		if(pSecDescriptorBuf != NULL)
		{
			delete pSecDescriptorBuf;
			pSecDescriptorBuf = NULL;
		}
		return E_FAIL;
	}
	
	// 2016-02-01 kh.choi 추가. 365정형외과의원 Windows XP 설치된 컴퓨터에서 pacl NULL 인 경우가 발견됨
	if (NULL == pacl) {
		OutputDebugString(_T("pacl is NULL."));
		if(pSecDescriptorBuf != NULL)
		{
			delete pSecDescriptorBuf;
			pSecDescriptorBuf = NULL;
		}
		return E_FAIL;
	}

	// DACL for specified file was retrieved successfully
	// Now, we should fill in the linked list of ACEs
	// Iterate through ACEs (Access Control Entries) of DACL
	for (USHORT i = 0; i < pacl->AceCount; i++)
	{
		LPVOID pAce;
		bSuccess = GetAce(pacl, i, &pAce);
		if (!bSuccess)
		{
			DWORD dwError = GetLastError();
			strLog.Format(_T("Failed to get ace %d (%d)"), i, GetLastError());
			OutputDebugString(strLog);
			continue;
		}
		AddToInfo((ACCESS_ALLOWED_ACE*)pAce);
		
	}
	*pSecDescriptor = pSecDescriptorBuf;
	return S_OK;
}

/**
 @brief     파일 또는 폴더로 부터 ACL 정보를 구해온다.
 @author    hhh
 @date      2013.11.06
 @param     [_in] pAce     address of the ACE
*/
void CALCInformation::AddToInfo(ACCESS_ALLOWED_ACE* pAce)
{
	ACE_INFO acl_Info;
	
	if(pAce->Header.AceType ==  ACCESS_ALLOWED_ACE_TYPE)	
		acl_Info.allowed = TRUE;	
	else 	
		acl_Info.allowed = FALSE;

	acl_Info.ace = pAce;
	
	m_vtAcl_into.push_back(acl_Info);	
}

/**
 @brief     파일 또는 폴더로 부터 ACL 정보를 구해와 인자로 들어온 벡터 레퍼런스에 리턴
 @author    hhh
 @date      2013.11.06
 @param     [_out_] _vtActUser  그룹또는사용자별 acl
*/
BOOL CALCInformation::GetALCInfo(vector<ACL_USER>& _vtActUser)
{
	BOOL bRet = FALSE;

	BYTE* pSecDescriptorBuf = NULL;

	HRESULT hResult = Acl_Query(&pSecDescriptorBuf);
	if(pSecDescriptorBuf == NULL)
		return FALSE;

	ACCESS_ALLOWED_ACE* pAce;
	SID* pAceSid;
	ACCESS_MASK maskPermissions;

	if (S_OK == hResult)
	{
		vector<ACE_INFO>::iterator iter = m_vtAcl_into.begin();
		while(iter != m_vtAcl_into.end())
		{
			ACE_INFO List = *iter;

			pAce = List.ace;
			if (List.allowed)
			{
				ACCESS_ALLOWED_ACE* pAllowed = (ACCESS_ALLOWED_ACE*)pAce;
				pAceSid = (SID*)(&(pAllowed->SidStart));
				maskPermissions = pAllowed->Mask;
			}
			else
			{
				ACCESS_DENIED_ACE* pDenied = (ACCESS_DENIED_ACE*)pAce;
				pAceSid = (SID*)(&(pDenied->SidStart));
				maskPermissions = pDenied->Mask;
			}
			DWORD dwCbName = 0;
			DWORD dwCbDomainName = 0;
			SID_NAME_USE SidNameUse;
			TCHAR bufName[MAX_PATH];
			TCHAR bufDomain[MAX_PATH];
			dwCbName = sizeof(bufName);
			dwCbDomainName = sizeof(bufDomain);

			// Get account name for SID
			BOOL bSuccess = LookupAccountSid(NULL,
								pAceSid,
								bufName,
								&dwCbName,
								bufDomain,
								&dwCbDomainName,
								&SidNameUse);
			if (!bSuccess)
			{
				OutputDebugString(_T("Failed to get account for SID"));
				iter++;
				continue;
			}
			
			ACL_USER NewOne;
			StringCchCopy(NewOne.domain, MAX_PATH, bufDomain);
			StringCchCopy(NewOne.username, MAX_PATH, bufName);
			// For Allowed aces
			if (List.allowed)
			{
				// Read Permissions
				if ((maskPermissions & READ_PERMISSIONS) == READ_PERMISSIONS)
				{
					NewOne.read = TRUE;					
				}
				else
				{
					NewOne.read = FALSE;					
				}
				// Write permissions
				if ((maskPermissions & WRITE_PERMISSIONS) == WRITE_PERMISSIONS)
				{
					NewOne.write = TRUE;					
				}
				else
				{
					NewOne.write = FALSE;					
				}
				// Execute Permissions
				if ((maskPermissions & EXECUTE_PERMISSIONS) == EXECUTE_PERMISSIONS)
				{
					NewOne.execute = TRUE;					
				}
				else
				{
					NewOne.execute = FALSE;					
				}
			}
			else
			// Denied Ace permissions
			{
				// Read Permissions
				if ((maskPermissions & READ_PERMISSIONS) != 0)
				{
					NewOne.read = TRUE;					
				}
				else
				{
					NewOne.read = FALSE;					
				}
				// Write permissions
				if ((maskPermissions & WRITE_PERMISSIONS) != 0)
				{
					NewOne.write = TRUE;					
				}
				else
				{
					NewOne.write = FALSE;					
				}
				// Execute Permissions
				if ((maskPermissions & EXECUTE_PERMISSIONS) != 0)
				{
					NewOne.execute = TRUE;					
				}
				else
				{
					NewOne.execute = FALSE;					
				}
			}

			_vtActUser.push_back(NewOne);
			bRet = TRUE;
			iter++;
		}
	}

	if(pSecDescriptorBuf != NULL)
	{
		delete pSecDescriptorBuf;
		pSecDescriptorBuf = NULL;
	}

	return bRet;
}



// AddToACL
// 주어진 ACL (Access Control List )에 해당 유저를 추가하고, 권한을 설정한다.

BOOL CALCInformation::AddToACL(PACL& pACL, CString AccountName, DWORD AccessOption)
{
	
    TCHAR  sid[MAX_PATH] = {0};
    DWORD sidlen = MAX_PATH;
    TCHAR  dn[MAX_PATH] = {0};
    DWORD dnlen = MAX_PATH;
    SID_NAME_USE SNU;
    PACL temp = NULL;

	
    // 주어진 유저의 이름을 토대로 필요한 SID 를 구한다.
    if(!LookupAccountName(NULL, AccountName, (PSID)sid, &sidlen, dn, &dnlen, &SNU))
        return FALSE;

    // 특정한 사용자의 accesss control 에 대한 정보를 가공하는데 사용되는 구조.
    EXPLICIT_ACCESS ea = {0};
    ea.grfAccessPermissions = AccessOption;
    ea.grfAccessMode = SET_ACCESS;
    ea.grfInheritance= SUB_CONTAINERS_AND_OBJECTS_INHERIT;
    ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
    ea.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
    ea.Trustee.ptstrName  = (LPWSTR) (PSID)sid;

    // 새로운 ACL 을 생성한다. 만약 기존에 주어진게 있으면 거기에 머지하여 생성한다.
   if(SetEntriesInAcl(1, &ea, pACL, &temp) != ERROR_SUCCESS)
     return FALSE;

    // 기존 ACL 을 지워주고, 새로 생성한걸로 대치한다.
    LocalFree(pACL);
    pACL = temp;

    return TRUE;
}

BOOL CALCInformation::ChangeFileSecurity(CString path, CStringArray& arrAccountList, DWORD _dwAccessOption)
{

//	CString strElog;
    SECURITY_DESCRIPTOR SD;    
    SECURITY_ATTRIBUTES SA;    
    BYTE AclBuf[100] = {0};  
    PACL pACL;  
          

//	strElog.Format(L"[OfsSVc] ChangePath : %s ", path);
//	OutputDebugString(strElog);

    if(!InitializeSecurityDescriptor(&SD, SECURITY_DESCRIPTOR_REVISION))    
        return FALSE;  
       
    pACL = (PACL)AclBuf;    
    if(!InitializeAcl(pACL, 100, ACL_REVISION))    
        return FALSE;  
  
	//DWORD dwAccessOption = GENERIC_ALL | GENERIC_READ |GENERIC_WRITE | GENERIC_EXECUTE; 

	if(arrAccountList.GetSize() < 1)
		return FALSE;

	for(int i=0; i< arrAccountList.GetSize() ; i ++)
	{
		AddToACL(pACL, arrAccountList.GetAt(i), _dwAccessOption);    
	}
    
	//AddToACL(pACL, L"Administrator", dwAccessOption);    
    //AddToACL(pACL, L"SYSTEM", dwAccessOption);  
          
    if(!SetSecurityDescriptorDacl(&SD, TRUE, pACL, FALSE))   
	{
//		OutputDebugString(L"[OfsSvc] - SetSecurityDescriptorDacl Error");
		return FALSE;  
	}
      
       
    SA.nLength = sizeof(SECURITY_ATTRIBUTES);   
    SA.bInheritHandle = FALSE;   
    SA.lpSecurityDescriptor = &SD;  
     
	BOOL bRet = SetFileSecurity(path, DACL_SECURITY_INFORMATION, &SD);  
	if(!bRet)
	{
		
//		strElog.Format(L"[OfsSvc] - SetFileSecurity Error- 0x%x", GetLastError());
//		OutputDebugString(strElog);
	}
		
    return bRet;
}