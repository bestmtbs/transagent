#include "stdafx.h"
#include "RC4.h"

CRC4::CRC4()
{
	ZeroMemory(m_pchKey, sizeof(m_pchKey));
}

CRC4::CRC4(unsigned char *pchKeyData, int iKeyLength)
{
}

CRC4::~CRC4()
{
}

void CRC4::SetKey(LPBYTE pchKey, int iLength)
{
	ZeroMemory(m_pchKey, sizeof(m_pchKey));

	if (iLength > 16) 
		iLength = 16;

	memcpy(m_pchKey, pchKey, iLength);
}

void CRC4::PrepareKey(unsigned char *pchKeyData, int iKeyLength)
{
	unsigned char chIndex1, chIndex2;
	unsigned char *pchState;

	pchState = &m_RC4Key.chState[0];
	for(short iCounter = 0; iCounter < 256; iCounter++)
		pchState[iCounter] = (unsigned char)iCounter;

	m_RC4Key.X	= m_RC4Key.Y = 0;
	chIndex1	= chIndex2 = 0;

	unsigned char chSwapByte;

	for (iCounter = 0; iCounter < 256; iCounter++)
	{
		chIndex2 = (pchKeyData[chIndex1] + pchState[iCounter] + chIndex2) % 256;
		chSwapByte = pchState[iCounter]; pchState[iCounter] = pchState[chIndex2]; pchState[chIndex2] = chSwapByte;
		chIndex1 = (chIndex1 + 1) % iKeyLength;
	}
}

void CRC4::Conversion(unsigned char *pchBuffer, int iBufferLength)
{
	PrepareKey(m_pchKey, sizeof(m_pchKey));

	unsigned char X = m_RC4Key.X;
	unsigned char Y = m_RC4Key.Y;
	unsigned char *pchState;
	unsigned char chXORIndex;
	unsigned char swapByte;

	pchState = &m_RC4Key.chState[0];

	for (int iCounter = 0; iCounter < iBufferLength; iCounter++)
	{
		X = (X + 1) % 256;
		Y = (pchState[X] + Y) % 256;
		swapByte = pchState[X]; pchState[X] = pchState[Y]; pchState[Y] = swapByte;
		chXORIndex = pchState[X] + (pchState[Y]) % 256;
		pchBuffer[iCounter] ^= pchState[chXORIndex];
	}

	m_RC4Key.X = 0;
	m_RC4Key.Y = 0;
}