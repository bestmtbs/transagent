// cryptlib.h - written and placed in the public domain by Wei Dai
/*! \file
 	This file contains the declarations for the abstract base
	classes that provide a uniform interface to this library.
*/

/*!	\mainpage <a href="http://www.cryptopp.com">Crypto++</a> Reference Manual
<dl>
<dt>Abstract Base Classes<dd>
	cryptlib.h
<dt>Algebraic Structures<dd>
	Integer, PolynomialMod2, PolynomialOver, RingOfPolynomialsOver,
	ModularArithmetic, MontgomeryRepresentation, GFP2_ONB,
	GF2NP, GF256, GF2_32, EC2N, ECP
<dt>Block Ciphers (in ECB mode)<dd>
	3way.h, blowfish.h, cast.h, des.h, diamond.h, gost.h,
	idea.h, lubyrack.h, mars.h, mdc.h,
	rc2.h, rc5.h, rc6.h, rijndael.h, safer.h, serpent.h, shark.h, skipjack.h,
	square.h, tea.h, twofish.h
<dt>Block Cipher Modes<dd>
	modes.h, cbc.h
<dt>Compression<dd>
	Deflator, Inflator, Gzip, Gunzip, ZlibCompressor, ZlibDecompressor
<dt>Secret Sharing and Information Dispersal<dd>
	SecretSharing, SecretRecovery, InformationDispersal, InformationRecovery
<dt>Stream Ciphers<dd>
	ARC4, PanamaCipher, BlumBlumShub, SEAL, SapphireEncryption, WAKEEncryption
<dt>Hash Functions<dd>
	HAVAL, MD2, MD5, PanamaHash, RIPEMD160, SHA, SHA256, SHA384, SHA512, Tiger
<dt>Non-Cryptographic Checksums<dd>
	CRC32, Adler32
<dt>Message Authentication Codes<dd>
	MD5MAC, XMACC, HMAC, CBC_MAC, DMAC, PanamaMAC
<dt>Random Number Generators<dd>
	NullRNG, LC_RNG, RandomPool, BlockingRng, NonblockingRng, AutoSeededRandomPool
<dt>Public Key Cryptography<dd>
	blumgold.h, dh.h, dh2.h, dsa.h, eccrypto.h, luc.h, mqv.h,
	nr.h, rsa.h, rabin.h, rw.h, xtrcrypt.h
<dt>Input Source Classes<dd>
	StringSource, FileSource, SocketSource, WindowsPipeSource, RandomNumberSource
<dt>Output Sink Classes<dd>
	StringSinkTemplate, ArraySink, FileSink, SocketSink, WindowsPipeSink
<dt>Filter Wrappers<dd>
	StreamCipherFilter, HashFilter, HashVerifier, SignerFilter, VerifierFilter
<dt>Binary to Text Encoders and Decoders<dd>
	HexEncoder, HexDecoder, Base64Encoder, Base64Decoder
<dt>Wrappers for OS features<dd>
	Timer, Socket, WindowsHandle, WindowsReadPipe, WindowsWritePipe
</dl>

<p>This reference manual is very much a work in progress. Many classes are still lacking detailed descriptions.
<p>Click <a href="CryptoPPRef.zip">here</a> to download a zip archive containing this manual.
<p>Thanks to Ryan Phillips for providing the Doxygen configuration file
and getting me started with this manual.
*/

#ifndef CRYPTOPP_CRYPTLIB_H
#define CRYPTOPP_CRYPTLIB_H

#include "config.h"
#include <limits.h>
#include <exception>
#include <string>

NAMESPACE_BEGIN(Crypto)

//! base class for all exceptions thrown by Crypto++

class Exception : public std::exception
{
public:
	explicit Exception(const std::string &s) : m_what(s) {}
	virtual ~Exception() throw() {}
	const char *what() const throw() {return (m_what.c_str());}
	const std::string &GetWhat() const {return m_what;}
	void SetWhat(const std::string &s) {m_what = s;}

private:
	std::string m_what;
};

//! used to specify a direction for a cipher to operate in (encrypt or decrypt)
enum CipherDir {
	//!
	ENCRYPTION,
	//!
	DECRYPTION};


//! abstract base class for block ciphers

/*! All classes derived from BlockTransformation are block ciphers
	in ECB mode (for example the DESEncryption class), which are stateless.
	These classes should not be used directly, but only in combination with
	a mode class (see CipherMode).

	Note: BlockTransformation objects may assume that pointers to input and
	output blocks are aligned on 32-bit word boundaries.
*/
class BlockTransformation
{
public:
	//!
	virtual ~BlockTransformation() {}

	//! encrypt or decrypt one block in place
	/*! \pre size of inoutBlock == BlockSize() */
	virtual void ProcessBlock(byte *inoutBlock) const =0;

	//! encrypt or decrypt one block, may assume inBlock != outBlock
	/*! \pre size of inBlock and outBlock == BlockSize() */
	virtual void ProcessBlock(const byte *inBlock, byte *outBlock) const =0;

	//! block size of the cipher in bytes
	virtual unsigned int BlockSize() const =0;
};

//! provides an implementation of BlockSize()
template <unsigned int N>
class FixedBlockSize : public BlockTransformation
{
public:
	enum {BLOCKSIZE = N};
	virtual unsigned int BlockSize() const {return BLOCKSIZE;}
};

//! abstract base class for stream ciphers

class StreamCipher
{
public:
	//!
	virtual ~StreamCipher() {}

	//! encrypt or decrypt one byte
	virtual byte ProcessByte(byte input) =0;

	//! encrypt or decrypt an array of bytes of specified length in place
	virtual void ProcessString(byte *inoutString, unsigned int length);
	//! encrypt or decrypt an array of bytes of specified length, may assume inString != outString
	virtual void ProcessString(byte *outString, const byte *inString, unsigned int length);
};

//! abstract base class for hash functions

/*! HashModule objects are stateful.  They are created in an initial state,
	change state as Update() is called, and return to the initial
	state when Final() is called.  This interface allows a large message to
	be hashed in pieces by calling Update() on each piece followed by
	calling Final().
*/
class HashModule
{
public:
	//!
	virtual ~HashModule() {}

	//! process more input
	virtual void Update(const byte *input, unsigned int length) =0;

	/*/ calculate hash for the current message (the concatenation of all
		inputs passed in via Update()), then reinitialize the object */
	//* Precondition: size of digest == DigestSize().
	virtual void Final(byte *digest) =0;

	//! size of the hash returned by Final()
	virtual unsigned int DigestSize() const =0;

	//! use this if your input is short and you don't want to call Update() and Final() seperately
	virtual void CalculateDigest(byte *digest, const byte *input, int length)
		{Update(input, length); Final(digest);}

	//! verify that digest is a valid digest for the current message, then reinitialize the object
	/*! Default implementation is to call Final() and do a bitwise comparison
		between its output and digest. */
	virtual bool Verify(const byte *digest);

	//! use this if your input is short and you don't want to call Update() and Verify() seperately
	virtual bool VerifyDigest(const byte *digest, const byte *input, int length)
		{Update(input, length); return Verify(digest);}
};

//! abstract base class for message authentication codes

/*! The main differences between a MAC and an hash function (in terms of
	programmatic interface) is that a MAC is keyed, and that calculating
	a MAC for the same message twice may produce two different results so
	verifying a MAC may not be simply recalculating it and doing a bitwise
	comparison.
*/
class MessageAuthenticationCode : public virtual HashModule
{
public:
	//!
	virtual ~MessageAuthenticationCode() {}
};


NAMESPACE_END

#endif
