// cryptlib.cpp - written and placed in the public domain by Wei Dai

#include "stdafx.h"

#include "cryptlib.h"
#include "misc.h"

#include <memory>

NAMESPACE_BEGIN(Crypto)

void StreamCipher::ProcessString(byte *outString, const byte *inString, unsigned int length)
{
	while(length--)
		*outString++ = ProcessByte(*inString++);
}

void StreamCipher::ProcessString(byte *inoutString, unsigned int length)
{
	while(length--)
		*inoutString++ = ProcessByte(*inoutString);
}

bool HashModule::Verify(const byte *digestIn)
{
	SecByteBlock digest(DigestSize());
	Final(digest);
	return memcmp(digest, digestIn, DigestSize()) == 0;
}

NAMESPACE_END