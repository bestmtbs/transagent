// NICAddress.cpp: implementation of the CNICAddress class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "NICAddress.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CNICAddress::CNICAddress()
{
  m_pchMACAddress = (char *)malloc(sizeof(char) * (MACADDRESS_SIZE +1));
}

CNICAddress::~CNICAddress()
{
  free (m_pchMACAddress);
}

char *CNICAddress::GetNICAddress()
{
    NCB ncb;
    UCHAR uRetCode;

    memset(&ncb, 0, sizeof(ncb));
    ncb.ncb_command = NCBRESET;
    ncb.ncb_lana_num = 0;

    uRetCode = Netbios( &ncb );

    memset( &ncb, 0, sizeof(ncb) );
    ncb.ncb_command = NCBASTAT;
    ncb.ncb_lana_num = 0;

    strcpy((char *)ncb.ncb_callname, "*               ");
    ncb.ncb_buffer = (unsigned char *) &Adapter;
    ncb.ncb_length = sizeof(Adapter);

    uRetCode = Netbios( &ncb );
    
    if ( uRetCode == 0 )
    {
        sprintf(m_pchMACAddress, "%02x%02x%02x%02x%02x%02x",
                Adapter.adapt.adapter_address[0],
                Adapter.adapt.adapter_address[1],
                Adapter.adapt.adapter_address[2],
                Adapter.adapt.adapter_address[3],
                Adapter.adapt.adapter_address[4],
                Adapter.adapt.adapter_address[5] );
    }

    return m_pchMACAddress;
}
