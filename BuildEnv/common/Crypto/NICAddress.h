// NICAddress.h: interface for the CNICAddress class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NICADDRESS_H__231FFA3B_6CCF_11D4_B8D1_00E04C672911__INCLUDED_)
#define AFX_NICADDRESS_H__231FFA3B_6CCF_11D4_B8D1_00E04C672911__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <nb30.h>
#include <mbstring.h>
#include <wincon.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MACADDRESS_SIZE 12

class CNICAddress  
{
public:
	CNICAddress();
	virtual ~CNICAddress();

public:
   char *GetNICAddress();

   typedef struct _ASTAT_
   {
      ADAPTER_STATUS adapt;
      NAME_BUFFER    NameBuff [30];
   } ASTAT, * PASTAT;

   ASTAT Adapter;

private:
  char *m_pchMACAddress;
};

#endif // !defined(AFX_NICADDRESS_H__231FFA3B_6CCF_11D4_B8D1_00E04C672911__INCLUDED_)
