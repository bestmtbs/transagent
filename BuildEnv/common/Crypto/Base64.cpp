#include "stdafx.h"
#include "Base64.h"
#include <stdexcept>
//#ifdef _TIOR_TRANSFER_
//#include <boost/locale/encoding.hpp>
//#include <codecvt>
//#endif // _TIOR_TRANSFER_


CBase64::CBase64()
{
}

CBase64::~CBase64()
{
}

int CBase64::POS(char c)
{
	if(c == '=') 
		return 64;

	if(isupper(c))
		return c - 'A';

	if(islower(c))
		return c - 'a' + 26;

	if(isdigit(c))
		return c - '0' + 52;

	if(c == '+')
		return 62;

	if(c == '/')
		return 63;

	return -1;
}

int CBase64::base64decode(char *s, void *data)
{
	char *p;
	unsigned char *q;
	int n[4];

	if(strlen(s) % 4)
		return -1;

	q=(unsigned char*)data;

	for(p=s; *p; p+=4)
	{
		n[0] = POS(p[0]);
		n[1] = POS(p[1]);
		n[2] = POS(p[2]);
		n[3] = POS(p[3]);

		if((n[0] | n[1] | n[2] | n[3]) < 0)
		  return -1;

		if(n[0] == 64 || n[1] == 64)
		  return -1;

		if(n[2] == 64 && n[3] < 64)
		  return -1;

		q[0] = (n[0] << 2) + (n[1] >> 4);

		if(n[2] < 64)
		{
		  q[1] = ((n[1] & 15) << 4) + (n[2] >> 2);
		}

		if(n[3] < 64)
		{
		  q[2] = ((n[2] & 3) << 6) + n[3];
		}

		q+=3;
	}

	q -= (n[2] == 64) + (n[3] == 64);
	return q - (unsigned char*)data;
}

void CBase64::Encode(BYTE abytRaw[], int iLength, CString& strOutput)
{
	strOutput.Empty();
	BYTE abytBuffer[5] = { 0,0,0,0,0 };

	for (int i = 0; i < iLength; i += 3)
	{
		EncodeBlock(abytRaw, iLength, i, abytBuffer);
		strOutput += (LPSTR)abytBuffer;
	}
}

void CBase64::Encode(CString &strInput,  CString& strOutput)
{
	Encode((BYTE *)(LPCTSTR)strInput, strInput.GetLength()*sizeof(TCHAR), strOutput);      
}

int CBase64::Decode(CString &strBase64,BYTE abytOutput[])
{
	int iPad = 0;
	int i = 0;
	for (i = strBase64.GetLength() - 1; strBase64.GetAt(i) == '='; i--)
	  iPad++;

	int iLength = strBase64.GetLength() * 6 / 8 - iPad;
	int iOutputIndex = 0;
	int iBlock;

	for (i = 0; i < strBase64.GetLength(); i += 4)
	{
		iBlock = (GetValue(strBase64.GetAt(i)) << 18) + (GetValue(strBase64.GetAt(i + 1)) << 12) +
				 (GetValue(strBase64.GetAt(i + 2)) << 6) + (GetValue(strBase64.GetAt(i + 3)));

		for (int j = 0; j < 3 ; j++)
			abytOutput[iOutputIndex + j] = (BYTE)((iBlock >> (8 * (2 - j))) & 0xff);

		iOutputIndex += 3;
	}
	return iLength;
}      

int CBase64::Decode(CString &strInput, CString &strOutput)
{
	int iExpectLength = strInput.GetLength()/4 * 3;

	iExpectLength = iExpectLength / sizeof(TCHAR);
	
	strOutput.Empty();
	BYTE *pbytOutputBuffer = (BYTE *)strOutput.GetBufferSetLength(iExpectLength);

	Decode(strInput, pbytOutputBuffer);

	strOutput.ReleaseBuffer();

	return strOutput.GetLength();
}      

void CBase64::EncodeBlock(BYTE abytRaw[], int iRawLength, int iOffset, BYTE abytOutput[])
{
	int iSlack = iRawLength - iOffset - 1;
	int end = (iSlack >= 2) ? 2 : iSlack;
	int iNeuter;
	BYTE bytTemp;
	int iBlock = 0;
	int i = 0;

	for (i = 0; i <= end; i++)
	{
		bytTemp = abytRaw[iOffset + i];
		iNeuter = (bytTemp < 0) ? bytTemp + 256 : bytTemp;
		iBlock += iNeuter << (8 * (2 - i));
	}

	int iSixbit;
	for (i = 0; i < 4; i++)
	{
		iSixbit = (iBlock >> (6 * (3 - i))) & 0x3f;
		abytOutput[i] = GetCharacter(iSixbit);
	}

	if (iSlack < 1)
		abytOutput[2] = '=';

	if (iSlack < 2)
		abytOutput[3] = '=';
}       

char CBase64::GetCharacter(int iSixBit)
{

	if (iSixBit >= 0 && iSixBit <= 25)
		return (char)('A' + iSixBit);

	if (iSixBit >= 26 && iSixBit <= 51)
		return (char)('a' + (iSixBit - 26));

	if (iSixBit >= 52 && iSixBit <= 61)
		return (char)('0' + (iSixBit - 52));

	if (iSixBit == 62)
		return '+';

	if (iSixBit == 63)
		return '/';

	return '?';      
}

int CBase64::GetValue(TCHAR chValue)
{
	if (chValue >= _T('A') && chValue <= _T('Z'))
		return chValue - _T('A');

	if (chValue >= _T('a') && chValue <= _T('z'))
		return chValue - _T('a') + 26;

	if (chValue >= _T('0') && chValue <= _T('9'))
		return chValue - _T('0') + 52;

	if (chValue == _T('+'))
		return 62;

	if (chValue == _T('/'))
		return 63;

	if (chValue == _T('=')) 
		return 0;

	return -1;
}

char *CBase64::UrlEncode(char *str) 
{
	char *encstr, buf[2+1];
	unsigned char c;
	int i, j;

	if(str == NULL) return NULL;
	if((encstr = (char *)malloc((strlen(str) * 3) + 1)) == NULL) 
		return NULL;

	for(i = j = 0; str[i]; i++) 
	{
		c = (unsigned char)str[i];
		if((c >= '0') && (c <= '9')) encstr[j++] = c;
		else if((c >= 'A') && (c <= 'Z')) encstr[j++] = c;
		else if((c >= 'a') && (c <= 'z')) encstr[j++] = c;
		else if((c == '@') || (c == '.') || (c == '/') || (c == '\\')
			|| (c == '-') || (c == '_') || (c == ':') ) 
			encstr[j++] = c;
		else 
		{
			sprintf(buf, "%02x", c);
			encstr[j++] = '%';
			encstr[j++] = buf[0];
			encstr[j++] = buf[1];
		}
	}
	encstr[j] = NULL;

	return encstr;
}

//void wstrToUtf8(std::string& dest, const std::wstring& src, size_t size_needed) {
//	dest.clear();
//	for (size_t i = 0; i < size_needed; i++) {
//		wchar_t w = src[i];
//		if (w <= 0x7f) 
//			dest.push_back((char)w);
//		else if (w <= 0x7ff) {
//			dest.push_back(0xc0 | ((w >> 6) & 0x1f));
//			dest.push_back(0x80 | (w & 0x3f));
//		}
//		else if (w <= 0xffff) {
//			dest.push_back(0xe0 | ((w >> 12) & 0x0f));
//			dest.push_back(0x80 | ((w >> 6) & 0x3f));
//			dest.push_back(0x80 | (w & 0x3f));
//		}
//		else if (w <= 0x10ffff) {
//			dest.push_back(0xf0 | ((w >> 18) & 0x07));
//			dest.push_back(0x80 | ((w >> 12) & 0x3f));
//			dest.push_back(0x80 | ((w >> 6) & 0x3f));
//			dest.push_back(0x80 | (w & 0x3f));
//		}
//		else dest.push_back('?');
//	}
//} 
//
//#ifdef _TIOR_TRANSFER_
//#include <boost/archive/iterators/base64_from_binary.hpp>
//#include <boost/archive/iterators/binary_from_base64.hpp>
//#include <boost/archive/iterators/transform_width.hpp>
//#include <boost/archive/iterators/insert_linebreaks.hpp>
//#include <boost/archive/iterators/remove_whitespace.hpp>
//#include <iostream>
//#include <string>
//
//using namespace boost::archive::iterators;
//using namespace std;
//
//
//
////=========================================================== // base64 //=========================================================== 
//std::wstring CBase64::base64EncodeW(const std::wstring input, size_t size_needed){
//	string utf8_input(size_needed+1, 0);
//	//wstrToUtf8(utf8_input, input, size_needed);
//	utf8_input = boost::locale::conv::utf_to_utf<char>(input);
//	//CFile oFile2(_T("backsul.txt"), CFile::modeCreate | CFile::modeReadWrite | CFile::typeBinary);
//	//oFile2.Write(utf8_input.c_str(), utf8_input.size());
//	//oFile2.Close();
//
//	typedef transform_width< binary_from_base64<remove_whitespace<string::const_iterator> >, 8, 6 > it_binary_t;
//	typedef insert_linebreaks<base64_from_binary<transform_width<string::const_iterator, 6, 8> >, 72 > it_base64_t;
//
//	// Encode
//	unsigned int writePaddChars = (3 - utf8_input.length() % 3) % 3;
//	string base64(it_base64_t(utf8_input.begin()), it_base64_t(utf8_input.end()));
//	base64.append(writePaddChars, '=');
//
//	wstring base64W = boost::locale::conv::utf_to_utf<wchar_t>(base64);
//	//unsigned int paddChars = count(base64.begin(), base64.end(), '=');
//	//std::replace(base64.begin(), base64.end(), '=', 'A'); // replace '=' by base64 encoding of '\0'
//	//string result(it_binary_t(base64.begin()), it_binary_t(base64.end())); // decode
//	//result.erase(result.end() - paddChars, result.end());  // erase padding '\0' characters
//	//std::wstring resultW = boost::locale::conv::utf_to_utf<wchar_t>(result);
//	return base64W;
//	//unsigned char const* buffer = (unsigned const char*)(utf8_input.c_str());
//	//size_t size = utf8_input.length();
//	//static wchar_t const* base64Table = L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
//	//// for = 
//	//// size_t base64Size = (size + 2 - ((size + 2) % 3)) / 3 * 4;
//	//// wstring result(base64Size, L'=');
//	//size_t base64Size = (size_t)ceil(size * 4.0 / 3);
//	//std::wstring result(base64Size, L' ');
//	//unsigned char const* s = buffer;	// source pointer 
//	//size_t di = 0;	// destination index
//	//for(size_t i = 0; i < size / 3; i++){ 
//	//// input: 0000_0000 0000_0000 0000_0000 
//	//// 
//	//// out1: 0000_00 
//	//// out2: 00 0000 
//	//// out3: _0000 00 
//	//// out4: 00_0000 
//	//	result[di++] = base64Table[s[0] >> 2];
//	//	result[di++] = base64Table[((s[0] << 4) | (s[1] >> 4)) & 0x3f];
//	//	result[di++] = base64Table[((s[1] << 2) | (s[2] >> 6)) & 0x3f];
//	//	result[di++] = base64Table[s[2] & 0x3f];
//	//	s += 3;
//	//} 
//
//	//size_t remainSize = size % 3;
//
//	// switch (remainSize) {
//	// case 0: break;
//	// case 1: result[di++] = base64Table[s[0] >> 2];
//	//	 result[di++] = base64Table[(s[0] << 4) & 0x3f];
//	//	 break;
//	// case 2: result[di++] = base64Table[s[0] >> 2];
//	//	 result[di++] = base64Table[((s[0] << 4) | (s[1] >> 4)) & 0x3f];
//	//	 result[di++] = base64Table[(s[1] << 2) & 0x3f];
//	//	 break;
//	// default: throw std::logic_error("Should not happen.");
//	// } 
//	// return result;
// }
//
//#endif // _TIOR_TRANSFER_
////
////std::wstring CBase64::base64EncodeW(const std::wstring input, size_t size_needed){
////	using std::wstring;
////	static wchar_t const* base64Table =
////		L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
////
////	size_t base64Size = (size_needed + 2 - ((size_needed + 2) % 3)) / 3 * 4;
////	wstring result(base64Size, L'=');
////
////	std::string utf8_input(size_needed+1, 0);
////	utf8_input = boost::locale::conv::utf_to_utf<char>(input);
////	unsigned char const* s = reinterpret_cast<const unsigned char *> (strVar.c_str());// source pointer
////	size_t di = 0;                    // destination index
////	for (size_t i = 0; i < size_needed / 3; i++) {
////		// input: 0000_0000 0000_0000 0000_0000
////		// 
////		// out1:  0000_00
////		// out2:         00 0000
////		// out3:                _0000 00
////		// out4:                        00_0000
////
////		result[di++] = base64Table[s[0] >> 2];
////		result[di++] = base64Table[((s[0] << 4) | (s[1] >> 4)) & 0x3f];
////		result[di++] = base64Table[((s[1] << 2) | (s[2] >> 6)) & 0x3f];
////		result[di++] = base64Table[s[2] & 0x3f];
////		s += 3;
////	}
////
////	size_t remainSize = size_needed % 3;
////	switch (remainSize) {
////	case 0:
////		break;
////	case 1:
////		result[di++] = base64Table[s[0] >> 2];
////		result[di++] = base64Table[(s[0] << 4) & 0x3f];
////		break;
////	case 2:
////		result[di++] = base64Table[s[0] >> 2];
////		result[di++] = base64Table[((s[0] << 4) | (s[1] >> 4)) & 0x3f];
////		result[di++] = base64Table[(s[1] << 2) & 0x3f];
////		break;
////	default:
////		throw std::logic_error("Should not happen.");
////	}
////	return result;
////}

CString CBase64::base64encode(const void *buf, int size)
{
	char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	char *str = (char*)malloc((size + 3) * 4 / 3 + 1);
	char *p = str;
	unsigned char *q = (unsigned char*)buf;
	int i;
	int c;
	i = 0;

	while (i<size)
	{
		c = q[i++];
		c *= 256;

		if (i<size)
			c += q[i];

		i++;
		c *= 256;

		if (i<size)
			c += q[i];

		i++;

		p[0] = base64[(c & 0x00fc0000) >> 18];
		p[1] = base64[(c & 0x0003f000) >> 12];
		p[2] = base64[(c & 0x00000fc0) >> 6];
		p[3] = base64[(c & 0x0000003f) >> 0];

		if (i>size)
			p[3] = '=';

		if (i>size + 1)
			p[2] = '=';

		p += 4;
	}
	*p = 0;

	//CString strResult = _T("");
	//strResult.Format(_T("%s"), str);
	CString strResult(str);
	free(str);
	return strResult;
}