
#pragma once

#include "ksobject.h"

template < class T > 
class  KSSingleton : public KSObject
{
public:
	static	T*		getInstance();
	static	void	releaseInstance();

private:
	static	T*		selfInstance;
};

template < class T > 
T* KSSingleton< T >::selfInstance = 0;

template < class T > T* 
KSSingleton< T >::getInstance()
{
	if( selfInstance == 0 )
		selfInstance = new T();

return selfInstance;
}

template < class T > 
void KSSingleton< T >::releaseInstance()
{
	if( selfInstance != 0 )
		delete selfInstance;

	selfInstance = 0;
}