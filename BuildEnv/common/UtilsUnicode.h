/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.06.06		
 ********************************************************************************/
/**
 @file      UtilsUnicode.h
 @brief     Unicode Utilities function 정의 파일 
 @author    hang ryul lee
 @date      create 2011.06.06 

 @note 
*/




LPSTR UniToAnsi (LPWSTR		pwzStr);   // UNICODE 를 ANSI로 변경 해준다 메모리 해제에 주의
BOOL UnicodeToAnsi(LPSTR pszDest, LPCTSTR pszSrc, SIZE_T size); // UNICODE 를 ANSI로 변경

LPWSTR AnsiCodeToUniCode(char* _pzText); // ANSI 를 UNICODE로 변경 해준다 메모리 해제에 주의>>>>>>> .r680

// 글자 제어

BOOL			IsHan( const char * pzStr, int nOpt);
BOOL			IsHanStr( const char * pzInStr);