/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsProcess.cpp 
 @brief     Process 관련 Utils
 @author    hang ryul lee
 @date      create 2011.05.29
 @note      Process 관련 유틸함수 모음 \n
            Process 관련 동적로딩하는 API들도 여기 있음
*/

#include "stdafx.h"
#include "UtilsProcess.h"
#include "UtilsFile.h"
#include "GeneralUtil.h"
//#include "Impersonator.h"

#include <yvals.h>
#include <Userenv.h>

CProcess::CProcess(){
	m_pProcess32First = NULL;
	m_pProcess32Next =NULL;
	m_pCreateToolhelp32Snapshot = NULL;
}
CProcess::~CProcess(){
	m_pProcess32First = NULL;
	m_pProcess32Next =NULL;
	m_pCreateToolhelp32Snapshot = NULL;
}

/**
 @brief     Kernel32.dll에 CreateToolhelp32Snapshot 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_dwFlags        스냅샷에 포함할 정보 (TH32CS_SNAPPROCESS: 모든 프로세스정보)
 @param     [in]_th32ProcessID  프로세스 아이디
 @return    HANDLE 스냅샷핸들
*/
HANDLE CallCreateToolhelp32Snapshot(DWORD _dwFlags, DWORD _th32ProcessID)
{
    typedef HANDLE (WINAPI* fpCreateToolhelp32Snapshot)(DWORD, DWORD);

    HANDLE bRetHandle = INVALID_HANDLE_VALUE;
	HINSTANCE hInstLib = NULL;

	hInstLib = ::LoadLibrary(_T("Kernel32.dll"));
	if(NULL == hInstLib) 
        return bRetHandle;

    fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
	pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot)::GetProcAddress(hInstLib, "CreateToolhelp32Snapshot");
	if (NULL == pCreateToolhelp32Snapshot)
        return bRetHandle;
    bRetHandle =  pCreateToolhelp32Snapshot(_dwFlags, _th32ProcessID);
    ::FreeLibrary(hInstLib);

    return bRetHandle;
}

/**
 @brief     Kernel32.dll에 Process32First 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_hSnapshot      스냅샷 핸들
 @param     [out]_lppe          프로세스 정보
 @return    true/false
*/
bool CallProcess32First(HANDLE _hSnapshot, LPPROCESSENTRY32 _lppe)
{
    #ifdef UNICODE 
        #define PROCESS32FIRST  "Process32FirstW"
    #else
        #define PROCESS32FIRST  "Process32First"
    #endif
   typedef BOOL (WINAPI* fpProcess32First)(HANDLE, LPPROCESSENTRY32);

    bool bRet = false;
	HINSTANCE hInstLib = NULL;

	hInstLib = ::LoadLibrary(_T("Kernel32.dll"));
	if(NULL == hInstLib) 
        return bRet;

    fpProcess32First pProcess32First = NULL;
	pProcess32First = (fpProcess32First)::GetProcAddress(hInstLib, PROCESS32FIRST);
	if (NULL == pProcess32First)
        return bRet;
	bRet = (TRUE == pProcess32First(_hSnapshot, _lppe));
    ::FreeLibrary(hInstLib);

    return bRet;
}

/**
 @brief     Kernel32.dll에 Process32Next 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_hSnapshot      스냅샷 핸들
 @param     [out]_lppe          프로세스 정보
 @return    true/false
*/
bool CallProcess32Next(HANDLE _hSnapshot, LPPROCESSENTRY32 _lppe)
{
    #ifdef UNICODE 
        #define PROCESS32NEXT  "Process32NextW"
    #else
        #define PROCESS32NEXT  "Process32Next"
    #endif

   typedef BOOL (WINAPI* fpProcess32Next)(HANDLE, LPPROCESSENTRY32);

    bool bRet = false;
	HINSTANCE hInstLib = NULL;

	hInstLib = ::LoadLibrary(_T("Kernel32.dll"));
	if(NULL == hInstLib) 
        return bRet;

    fpProcess32Next pProcess32Next = NULL;
	pProcess32Next = (fpProcess32Next)::GetProcAddress(hInstLib, PROCESS32NEXT);
	if (NULL == pProcess32Next)
        return bRet;
	bRet = (TRUE == pProcess32Next(_hSnapshot, _lppe));
    ::FreeLibrary(hInstLib);
    return bRet;
}

/**
 @brief     Psapi.dll에 EnumProcesses 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_hSnapshot      스냅샷 핸들
 @param     [out]_lppe          프로세스 정보
 @return    true/false
*/
bool CallEnumProcesses(DWORD *_pdwProcessIds, DWORD _dwcb, DWORD* _pdwBytesReturned)
{
    typedef BOOL (WINAPI *fpEnumProcesses)(DWORD*, DWORD, DWORD*);

    bool bRet = false;
	HINSTANCE hInstLib = NULL;

	hInstLib = ::LoadLibrary(_T("Psapi.dll"));
	if(NULL == hInstLib) 
        return bRet;

    fpEnumProcesses pEnumProcesses = NULL;
	pEnumProcesses = (fpEnumProcesses)::GetProcAddress(hInstLib, "EnumProcesses");
	if (NULL == pEnumProcesses)
        return bRet;
	bRet = (TRUE == pEnumProcesses(_pdwProcessIds, _dwcb, _pdwBytesReturned));
    ::FreeLibrary(hInstLib);

    return bRet;
}

/**
 @brief     Psapi.dll에 GetModuleBaseName 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_hProcess      프로세스 핸들
 @param     [in]_hModule       모듈 핸들
 @param     [out]_lpBaseName   모듈이름
 @param     [in]_dwSize        _lpBaseName 사이즈
 @return    DWORD              _lpBaseName 버퍼사이즈
*/
DWORD CallGetModuleBaseName(HANDLE _hProcess, HMODULE _hModule, LPTSTR _lpBaseName, DWORD _dwSize)
{
    #ifdef UNICODE
	    #define GETMOUDLEBASENAME     "GetModuleBaseNameW"
    #else
	    #define GETMOUDLEBASENAME     "GetModuleBaseNameA"
    #endif

    typedef DWORD (WINAPI *fpGetModuleBaseName)(HANDLE, HMODULE, LPTSTR, DWORD);

    DWORD dwRet = 0;
	HINSTANCE hInstLib = NULL;

	hInstLib = ::LoadLibrary(_T("Psapi.dll"));
	if(NULL == hInstLib) 
        return dwRet;

	fpGetModuleBaseName pGetModuleBaseName = NULL;
	pGetModuleBaseName = (fpGetModuleBaseName)::GetProcAddress(hInstLib, GETMOUDLEBASENAME);
	if (NULL == pGetModuleBaseName)
        return dwRet;
	dwRet = pGetModuleBaseName(_hProcess, _hModule, _lpBaseName, _dwSize);
    ::FreeLibrary(hInstLib);
    return dwRet;
}


/**
 @brief     Psapi.dll에 EnumProcessModules 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee
 @date      2008.11.04
 @param     [in]hProcess		프로세스 핸들
 @param     [out]lphModule      모듈 핸들 포인터정보
 @param     [in]dwCb			모듈 핸들 포인터의 크기
 @param     [out]lpcbNeeded      필요한 크기
 @return    true/false
 @note      제품 개발팀 요청으로 추가
*/
bool CallEnumProcessModules(HANDLE hProcess, HMODULE *lphModule, DWORD dwCb, DWORD* lpcbNeeded)
{
    typedef BOOL (WINAPI *fpEnumProcessModules)(HANDLE , HMODULE *, DWORD , DWORD* );

    bool bRet = false;
	HINSTANCE hInstLib = NULL;

	if( hProcess && lphModule && lpcbNeeded )
	{
		hInstLib = ::LoadLibrary(_T("Psapi.dll"));
		if(NULL == hInstLib) 
			return bRet;

		fpEnumProcessModules pEnumProcessModules = NULL;
		pEnumProcessModules = (fpEnumProcessModules)::GetProcAddress(hInstLib, "EnumProcessModules");
		if (NULL == pEnumProcessModules)
			return bRet;
		bRet = (TRUE == pEnumProcessModules( hProcess, lphModule, dwCb, lpcbNeeded ) );
		::FreeLibrary(hInstLib);
	}

    return bRet;
}

/**
 @brief     Psapi.dll에 GetModuleFileNameEx 함수를 로드하고 호출한다.\n            
 @author    hang ryul lee 
 @date      2008.11.04
 @param     [in]hProcess		프로세스 핸들
 @param     [in]hModule			모듈 핸들
 @param     [out]lpFileName		프로세스 파일 네임
 @param     [in]nSize			크기
 @return    true/false
 @note      제품 개발팀 요청으로 추가
*/
bool CallGetModuleFileNameEx(HANDLE hProcess, HMODULE hModule, LPTSTR lpFileName, DWORD nSize)
{
    typedef BOOL (WINAPI *fpGetModuleFileNameEx)(HANDLE , HMODULE, LPTSTR , DWORD );

	#ifdef _UNICODE
	#define MOULE_FUNC_NAME "GetModuleFileNameExW"
	#else
	#define MOULE_FUNC_NAME "GetModuleFileNameExA"
	#endif

    bool bRet = false;
	HINSTANCE hInstLib = NULL;

	if( hProcess && lpFileName )
	{
		hInstLib = ::LoadLibrary(_T("Psapi.dll"));
		if(NULL == hInstLib) 
			return bRet;

		fpGetModuleFileNameEx pGetModuleFileNameEx = NULL;
		pGetModuleFileNameEx = (fpGetModuleFileNameEx)::GetProcAddress(hInstLib, MOULE_FUNC_NAME );

		if (NULL == pGetModuleFileNameEx)
			return bRet;
		bRet = ( pGetModuleFileNameEx( hProcess, hModule, lpFileName, nSize ) > 0 );
		::FreeLibrary(hInstLib);
	}

    return bRet;
}

BOOL CProcess::SetPrivilege(
        HANDLE hToken,   // 토큰 핸들
        LPCTSTR Privilege,   // 활성/비활성화할 권한
        BOOL bEnablePrivilege  // 권한 활성화 여부?
        )
    {
        TOKEN_PRIVILEGES tp;
        LUID luid;
        TOKEN_PRIVILEGES tpPrevious;
        DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);
 
        if (!LookupPrivilegeValue( NULL, Privilege, &luid ))
            return FALSE;
 
        // 현재의 권한 설정 얻기
        tp.PrivilegeCount   = 1;
        tp.Privileges[0].Luid   = luid;
        tp.Privileges[0].Attributes = 0;
 
        AdjustTokenPrivileges(
                hToken,
                FALSE,
                &tp,
                sizeof(TOKEN_PRIVILEGES),
                &tpPrevious,
                &cbPrevious
                );
 
        if (GetLastError() != ERROR_SUCCESS)
            return FALSE;
 
        // 이전의 권한 설정에 따라 권한 설정하기
        tpPrevious.PrivilegeCount       = 1;
        tpPrevious.Privileges[0].Luid   = luid;
 
        if (bEnablePrivilege) {
            tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
        }
        else {
            tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
                tpPrevious.Privileges[0].Attributes);
        }
 
        AdjustTokenPrivileges(
                hToken,
                FALSE,
                &tpPrevious,
                cbPrevious,
                NULL,
                NULL
                );
 
        if (GetLastError() != ERROR_SUCCESS)
            return FALSE;
 
        return TRUE;
    }


/**
 @brief     특정 프로세스를 찾는다.        
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_sFileName    프로세스 이름
 @param     [in]_sParam       프로세스 파람
 @return    true/false
*/
bool CProcess::FindProcess(const CString &_sFileName, const CString &_sParam /* = _T("") */)
{

		
	HANDLE hToken;


	OpenProcessToken(GetCurrentProcess(),
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
        &hToken);

	SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);


    bool bRet = false;
    CWinOsVersion WinOsVersion;
    if (WinOsVersion.IsWinNT())
        bRet = FindProcessNT(_sFileName, _sParam);
    else bRet = FindProcess9x(_sFileName);

	SetPrivilege(hToken, SE_DEBUG_NAME, FALSE);
    CloseHandle(hToken);


    return bRet;
}

/**
 @brief     특정 프로세스를 찾는다.(NT)        
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_sFileName    프로세스 이름
 @param     [in]_sParam       프로세스 파람
 @return    true/false
*/
bool CProcess::FindProcessNT(const CString &_sFileName, const CString &_sParam /*= _T("") */)
{
    UNUSED_ALWAYS(_sParam);
    if (FILENAME_MAX_LENGTH <= _sFileName.GetLength()) 
        return false;
	if (_T("") == _sFileName) 
        return false;

    bool bResult = false;
	CString	 sExeName = _T(""); 		
	HANDLE   hProcess = NULL; 
    DWORD dwProcesseArray[1024] = {0, };
    DWORD dwcbNeeded = 0;
    DWORD dwcbProcess = 0;
	TCHAR szProcessName[MAX_PATH] = _T("");

	sExeName = ExtractFileName(_sFileName);
	sExeName.MakeUpper();

	if (!CallEnumProcesses(dwProcesseArray, sizeof(dwProcesseArray), &dwcbNeeded)) 
        return false;

	dwcbProcess = dwcbNeeded / sizeof(DWORD);
	for(DWORD i = 0; i < dwcbProcess; i++)
	{
		if (::GetCurrentProcessId() == dwProcesseArray[i]) 
            continue;
		hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcesseArray[i]);
		if (NULL == hProcess) 
            continue;
		if (0 == CallGetModuleBaseName(hProcess, 0, szProcessName, sizeof(szProcessName)/sizeof(TCHAR))) 
            continue;
       	CString  sProcessName = _T("");  
        sProcessName = ExtractFileName(szProcessName);
        sProcessName.MakeUpper();
		if (0 == sProcessName.CompareNoCase(sExeName))
		{
			bResult = true;
            ::CloseHandle(hProcess);
			break;
		}			
		::CloseHandle(hProcess);
	}
	return bResult;    

}

/**
 @brief     특정 프로세스를 찾는다.(9x)        
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_sFileName    프로세스 이름
 @param     [in]_sParam       프로세스 파람
 @return    true/false
*/
bool CProcess::FindProcess9x(const CString &_sFileName)
{
    if (FILENAME_MAX_LENGTH <= _sFileName.GetLength()) 
        return false;
    if (_T("") == _sFileName) 
        return false;
	CString  sExeName = _T("");
	HANDLE hSnapHandle = NULL;
	PROCESSENTRY32 Processentry32;
	bool bResult = false;

	sExeName = ExtractFileName(_sFileName);
	sExeName.MakeUpper();
	hSnapHandle = CallCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE == hSnapHandle) 
		return bResult;
	Processentry32.dwSize = sizeof(Processentry32);
	if (!CallProcess32First(hSnapHandle, &Processentry32))
	{
		::CloseHandle(hSnapHandle);
		return bResult;
	}
	do
	{
		if (::GetCurrentProcessId() == Processentry32.th32ProcessID) 
            continue;
        CString sProcessName = _T("");
        sProcessName = ExtractFileName(Processentry32.szExeFile);
		sProcessName.MakeUpper();
		if (0 ==  sProcessName.CompareNoCase(sExeName))
		{
			bResult = true;				
			break;
		}
	}while (CallProcess32Next(hSnapHandle, &Processentry32));		
	::CloseHandle(hSnapHandle);		
	return bResult;

}

/**
 @brief     특정 프로세스 프로세스 이름으로 찾아 KILL한다.      
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_sFileName    프로세스 이름
 @param     [in]_sParam       프로세스 파람
 @return    true/false
*/
bool CProcess::KillProcess(const CString &_sFileName, const CString &_sParam /* = _T("") */)
{
    bool bRet = false;
    CWinOsVersion WinOsVersion;
    
	if (WinOsVersion.IsWinNT())
        bRet = KillProcessNT(_sFileName, _sParam);
    else
		bRet = KillProcess9x(_sFileName);


    return bRet;
}

/**
 @brief     특정 프로세스 PID로 찾아 KILL한다.      
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_dwPid    프로세스ID
 @return    true/false
*/
bool CProcess::KillProcess(const DWORD _dwPid)
{
	CString strMsg = _T("");
    bool bResult = false;
    HANDLE hKillProcess = NULL;
	hKillProcess = ::OpenProcess(PROCESS_TERMINATE, FALSE, _dwPid);
//	hKillProcess = ::OpenProcess(MAXIMUM_ALLOWED, FALSE, _dwPid);
	DWORD dwError = ::GetLastError();


    if (ERROR_INVALID_PARAMETER == dwError)
        bResult = (NULL == hKillProcess); 
    if (NULL != hKillProcess) 
    {
        bResult = (TRUE == ::TerminateProcess(hKillProcess, 0));
		strMsg.Format(_T("[kill] CMSUNINSTALL : KILLPROCESS  FAIL (%d) (%d) (%d)"), _dwPid,GetLastError(), bResult);
		UM_WRITE_LOG(strMsg);

        ::CloseHandle(hKillProcess);
    }
    return bResult;
}

/**
 @brief     특정 프로세스 PID로 찾아 KILL한다. (9x)     
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_sFileName    프로세스 이름
 @return    true/false
*/
bool CProcess::KillProcess9x(const CString &_sFileName)
{
    if (FILENAME_MAX_LENGTH <= _sFileName.GetLength()) 
        return false;
    if (_T("") == _sFileName) 
        return false;
	CString  sExeName = _T("");
	HANDLE hSnapHandle = NULL;
	PROCESSENTRY32 Processentry32;
	bool bResult = false;

	sExeName = ExtractFileName(_sFileName);
	sExeName.MakeUpper();
	hSnapHandle = CallCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	if (INVALID_HANDLE_VALUE == hSnapHandle) 
		return bResult;
	Processentry32.dwSize = sizeof(Processentry32);
	if (!CallProcess32First(hSnapHandle, &Processentry32))
	{
		::CloseHandle(hSnapHandle);
		return bResult;
	}
	do
	{
		if (::GetCurrentProcessId() == Processentry32.th32ProcessID) 
            continue;
        CString sProcessName = _T("");
        sProcessName = ExtractFileName(Processentry32.szExeFile);
		sProcessName.MakeUpper();
		if (0 ==  sProcessName.CompareNoCase(sExeName))
		{            
            for(int i=0; i<10; i++)
            {
                bool bKillProcess = false;
                bKillProcess = KillProcess(Processentry32.th32ProcessID);
                ::Sleep(500);
                bResult = bKillProcess;
                if (bKillProcess)
                    break;
            }
		}
	}while (CallProcess32Next(hSnapHandle, &Processentry32));		
	::CloseHandle(hSnapHandle);		
	return bResult;
}


/**
 @brief     특정 프로세스 PID로 찾아 KILL한다. (NT)     
 @author    hang ryul lee
 @date      2011.05.29 
 @param     [in]_sFileName    프로세스 이름
 @param     [in]@param        프로세스 param
 @return    true/false
*/
bool CProcess::KillProcessNT(const CString &_sFileName, const CString &_sParam /* = _T("") */)
{
//	UM_WRITE_LOG(_T("[kill] NT start"));
    UNUSED_ALWAYS(_sParam);
    if (FILENAME_MAX_LENGTH <= _sFileName.GetLength()) 
        return false;
	if (_T("") == _sFileName) 
        return false;

	//UM_WRITE_LOG(_T("[kill] NT start 2"));

    bool bResult = false;
	CString	 sExeName = _T(""); 		
	HANDLE   hProcess = NULL; 
    DWORD dwProcesseArray[1024] = {0, };
    DWORD dwcbNeeded = 0;
    DWORD dwcbProcess = 0;
	TCHAR szProcessName[MAX_PATH] = _T("");

	sExeName = ExtractFileName(_sFileName);
	sExeName.MakeUpper();

	 CString strLog; //test_h
	if (!CallEnumProcesses(dwProcesseArray, sizeof(dwProcesseArray), &dwcbNeeded)) 
        return false;

	//UM_WRITE_LOG(_T("[kill] NT start 3")); //test_h
	dwcbProcess = dwcbNeeded / sizeof(DWORD);
	strLog.Format(_T("[kill] NT start 3 -count : %d, %s"), dwcbProcess,sExeName);
//	UM_WRITE_LOG(strLog);
	for(DWORD i = 0; i < dwcbProcess; i++)
	{
		if (::GetCurrentProcessId() == dwProcesseArray[i]) 
		{
			strLog.Format(_T("[kill] Current Pid : %d, Find Pid: %d"), ::GetCurrentProcessId(), dwProcesseArray[i]);
		//	UM_WRITE_LOG(strLog);
			continue;
		}
            
	
		/*strLog.Format(_T("[kill] Find Pid : %d"), dwProcesseArray[i]);
		UM_WRITE_LOG(strLog);
		hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcesseArray[i]);
		if (NULL == hProcess) 
		{
			strLog.Format(_T("[kill] open fail  Find Pid : %d, GetLastError: %d"), dwProcesseArray[i], GetLastError());
			UM_WRITE_LOG(strLog);
			continue;
		}
            
		if (0 == CallGetModuleBaseName(hProcess, 0, szProcessName, sizeof(szProcessName)/sizeof(TCHAR))) 
            continue;*/


	//	strLog.Format(_T("[kill] Find Pid : %d"), dwProcesseArray[i]);
	//	UM_WRITE_LOG(strLog);


		if(FALSE == GetProcessPathNameFromPid(dwProcesseArray[i],szProcessName, sizeof(szProcessName)/sizeof(TCHAR))){

			strLog.Format(_T("[kill] -Fail GetProcessName - Find Pid : %d"), dwProcesseArray[i]);
		//	UM_WRITE_LOG(strLog);

			continue;
		}


       	CString  sProcessName = _T("");  
        sProcessName = ExtractFileName(szProcessName);
        sProcessName.MakeUpper();
		strLog.Format(_T("[kill] find_name: %s, %s"), sProcessName,sExeName);
		UM_WRITE_LOG(strLog);
		if (0 == sProcessName.CompareNoCase(sExeName))
		{
		//	UM_WRITE_LOG(_T("[kill] CmsUninstall Kill Process"));
            bResult = KillProcess(dwProcesseArray[i]);    
            ::Sleep(200);
            if (false == bResult) 
			{
				strLog.Format(_T("[kill] -Fail GetProcessName - Find Pid : %d,%s, error = %d"), dwProcesseArray[i],sProcessName,GetLastError());
				UM_WRITE_LOG(strLog);

                break;			
			}
		}			
		::CloseHandle(hProcess);
	}
//	UM_WRITE_LOG(_T("--- [kill] KillProcessNT -- "));
	return bResult;    
}

/**
 @brief      프로세스 뮤텍스 생성
 @author   JHLEE
 @date      2011.06.07 
 @param    [in]   Global 를 제외한 프로세스 뮤텍스명
 @return    bool( true / false)
 */
bool  CProcess::ProcessCreateMutex(CString strMutexName)
{
	HANDLE hMutex = NULL;

	hMutex = CreateMutex(NULL, TRUE,  _T("Global\\") + strMutexName);
	DWORD err=  GetLastError();
	CString strLog;
	strLog.Format(_T("[Util] CreateMutex error : %d"), err);
//	UM_WRITE_LOG(strLog);
	if( GetLastError() == ERROR_ALREADY_EXISTS || !hMutex )
	{
		ReleaseMutex(hMutex);
		CloseHandle(hMutex);

		return false;
	}

	return true;
}

/**
@brief      뮤텍스 체크
@author   hhh
@date      2013.04.24 
@param    [in]   프로세스 뮤텍스명
@return    bool( true / false)
*/
bool CProcess::IsExistMutex(CString strMutexName)
{
	
	HANDLE hMutex = CreateMutex(NULL, TRUE, strMutexName);
	if (GetLastError() == ERROR_ALREADY_EXISTS||!hMutex)
	{
		if(hMutex)
			ReleaseMutex(hMutex);

		CloseHandle(hMutex);
		return true;
	}
	else
	{
		if(hMutex)
			ReleaseMutex(hMutex);

		CloseHandle(hMutex);
		return false;
	}

}


/**
 @brief      프로세스 실행 중복 체크
 @author   JHLEE
 @date      2011.06.07 
 @param    [in]   Global 를 제외한 프로세스 뮤텍스명
 @return    bool( true / false)
 */
bool  CProcess::IsProcessExist(CString strMutexName)
{
	HANDLE hMutex = NULL;

	hMutex  = CreateMutex(NULL, TRUE,  _T("Global\\") + strMutexName);

	if( GetLastError() == ERROR_ALREADY_EXISTS || !hMutex )
	{
			if(hMutex)
			{
				ReleaseMutex(hMutex);
				CloseHandle(hMutex);
			}

		return true;

	}
	else
	{
			if(hMutex)
			{
				ReleaseMutex(hMutex);
				CloseHandle(hMutex);
			}

	}

	return false;
}

DWORD CProcess::FindProcessWaitForInputIdle(CString _sFileName)
{
    if (FILENAME_MAX_LENGTH <= _sFileName.GetLength()) 
        return false;
	if (_T("") == _sFileName) 
        return false;

    bool bResult = false;
	CString	 sExeName = _T(""); 		
	HANDLE   hProcess = NULL; 
    DWORD dwProcesseArray[1024] = {0, };
    DWORD dwcbNeeded = 0;
    DWORD dwcbProcess = 0;
	TCHAR szProcessName[MAX_PATH] = _T("");

	sExeName = ExtractFileName(_sFileName);
	sExeName.MakeUpper();

	if (!CallEnumProcesses(dwProcesseArray, sizeof(dwProcesseArray), &dwcbNeeded)) 
        return 0;

	dwcbProcess = dwcbNeeded / sizeof(DWORD);
	for(DWORD i = 0; i < dwcbProcess; i++)
	{
		if (::GetCurrentProcessId() == dwProcesseArray[i]) 
            continue;
		hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcesseArray[i]);
		if (NULL == hProcess) 
            continue;
		if (0 == CallGetModuleBaseName(hProcess, 0, szProcessName, sizeof(szProcessName)/sizeof(TCHAR))) 
            continue;
       	CString  sProcessName = _T("");  
        sProcessName = ExtractFileName(szProcessName);
        sProcessName.MakeUpper();
		if (0 == sProcessName.CompareNoCase(sExeName))
		{
			::CloseHandle(hProcess);
			return dwProcesseArray[i];    
		}			
		::CloseHandle(hProcess);
	}
	return 0;    
}


BOOL CProcess::KillProcessByName(CString sExeName)
{
	sExeName = ExtractFileName(sExeName);
	sExeName.MakeUpper();
	DWORD dwErr = 0;
	HANDLE hSnapshot = CallCreateToolhelp32Snapshot ( TH32CS_SNAPPROCESS, 0 );
	if ( (int)hSnapshot != -1 )
	{
		PROCESSENTRY32 pe32 ;
		pe32.dwSize=sizeof(PROCESSENTRY32);
		BOOL bContinue ;
		CString strProcessName;
		if ( CallProcess32First ( hSnapshot, &pe32 ) )
		{
			do
			{
				strProcessName = pe32.szExeFile; //strProcessName이 프로세스 이름; 
				strProcessName = strProcessName.MakeUpper();
				if( ( strProcessName.Find(sExeName,0) != -1 ) )
				{
					HANDLE hProcess = ::OpenProcess( PROCESS_TERMINATE , FALSE, pe32.th32ProcessID );
					if( hProcess )
					{
						DWORD dwExitCode;
						GetExitCodeProcess( hProcess, &dwExitCode);
						TerminateProcess( hProcess, dwExitCode);
						CloseHandle(hProcess);
						CloseHandle( hSnapshot );
						return TRUE;
					}
					dwErr = GetLastError();
					return FALSE;
				}
				bContinue = CallProcess32Next( hSnapshot, &pe32 );
			} while ( bContinue );
		}
		CloseHandle( hSnapshot );
	}
	return TRUE;	// 2017-09-20 sy.choi 프로세스 못찾았을 때
}

void CProcess::CheckTiorProcess(CString _strPath, CString _strMutexName, CString _strParam)
{
	CString strTrayMutex = _T("");

	PWTS_SESSION_INFO ppSessionInfo = NULL;
	DWORD     pCount = 0, dwRetSessionID = -1;
	WTS_SESSION_INFO  wts;
	CString strStationName, strLog;
	if (FALSE == WTSEnumerateSessions(WTS_CURRENT_SERVER_HANDLE, 0, 1, &ppSessionInfo, &pCount)) {
		return;
	}

	for( DWORD i = 0; i < pCount; i++ )
	{

		wts = ppSessionInfo[i];
		LPTSTR  ppBuffer        = NULL;
		DWORD   pBytesReturned  = 0;
		if(wts.SessionId == RDP_LISTENING_SESSION)			//해당 세션은 리모드 리스닝 세션이므로 무시.
			continue;

		//해당세션 id를 가진 explorer.exe가 존재하는지 체크,
		DWORD dwProcessID = IsGoodSessionID(wts.SessionId, EXPLORER_PROC_NAME);

		//해당세션에 explorer.exe가 돌고있음
		if(dwProcessID != -1 || wts.SessionId != 0)
		{
			//if (_strPath.Find(WTIOR_UNINSTALL_AGENT_NAME) != -1) {
			//	if( StartProcessAsUser(dwProcessID, _strPath, _strParam) )	// 2017-10-12 sy.choi 삭제실행
			//	{
			//		UM_WRITE_LOG(_T("[TiorSaverReload] Uninstall Start Success : ")+ _strPath );
			//	}					
			//	else
			//		UM_WRITE_LOG(_T("[TiorSaverReload] Uninstall Start Fail : ")+ _strPath );
			//} else 
			{
				strTrayMutex.Format(_T("%s_%d"), _strMutexName, wts.SessionId);
				//Process가 작동중이 아니라면
				if(false == CProcess::IsExistMutex(strTrayMutex) )	// 2017-05-08 kh.choi 윈도우 업데이트 후 모조리 false 로 리턴. 해서 프로세스 명을 확인하도록 아래로 바꿈 -> 다시 뮤텍스 사용
				{
					//인자로 들어가는 프로세스의 권한으로 프로세를 실행시킨다.
					if( StartProcessAsUser(dwProcessID, _strPath) )	//test
					{
						UM_WRITE_LOG(_T("[TiorSaverReload] Process Start Success : ")+ _strPath );
					}					
					else
						UM_WRITE_LOG(_T("[TiorSaverReload] Process Start Fail : ")+ _strPath );
				}
			}
		}

	}
}

int CProcess::IsGoodSessionID(DWORD dwSessionID, CString _strCompareProcessName)
{

	BOOL bGet = FALSE;  
	
	HANDLE hSnapshot;  
	PROCESSENTRY32 ppe;     
	CWTSSession cWtsession;
	hSnapshot = m_pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
	ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  

	CString strProcessName =_T(""), strLog;
	DWORD dwGetSessionID, dwRetPid = -1;

	bGet = m_pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  

	while (bGet)  
	{  
		strProcessName.Format(_T("%s"), ppe.szExeFile);
		strProcessName.MakeLower();
		
		//세션을 확인할 프로세스 이름 발견시
		if( strProcessName ==_strCompareProcessName ) 
		{	
			dwGetSessionID = cWtsession.GetProcessSessionId(ppe.th32ProcessID);
			strLog.Format(_T("[TiorSaverReload] Pid:%d(SeesionID: %d) - _InSessionID:%d"), ppe.th32ProcessID, dwGetSessionID, dwSessionID);
			UM_WRITE_LOG(strLog);
			if(dwGetSessionID == dwSessionID)
			{
				dwRetPid = ppe.th32ProcessID;
				break;
			}
		}
	
		bGet = m_pProcess32Next(hSnapshot, &ppe);  
	}  

	CloseHandle(hSnapshot);  

	return dwRetPid;  
}

BOOL CProcess::StartProcessAsUser(DWORD pid, CString strExePath, CString _strParam)
{
	CString strLog;
	HANDLE hd = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if(hd == NULL)
		return FALSE;

	HANDLE token = NULL;
	if(OpenProcessToken(hd, TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE, &token) == FALSE)
	{
		strLog.Format(_T("[TiorSaverReload] OpenProcessToken Error - %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
		CloseHandle(hd);
		return FALSE;
	}

	HANDLE newtoken = NULL;
	if(::DuplicateTokenEx(token, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, NULL, SecurityImpersonation, TokenPrimary, &newtoken) == FALSE)
	{
		strLog.Format(_T("[TiorSaverReload] DuplicateTokenEx Error - %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
		CloseHandle(hd);
		CloseHandle(token);
		return FALSE;
	}

	CloseHandle(token);

	void* EnvBlock = NULL;
	CreateEnvironmentBlock(&EnvBlock, newtoken, FALSE);

	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi = {0};

	if(::CreateProcessAsUser(newtoken,
		strExePath,
		_strParam.GetBuffer(0),
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT |
		CREATE_NEW_CONSOLE  | CREATE_SEPARATE_WOW_VDM |
		CREATE_NEW_PROCESS_GROUP,
		EnvBlock,
		NULL,
		&si,
		&pi) == TRUE)
	{

		CloseHandle(hd);
		CloseHandle(newtoken);
		return TRUE;
	}

	strLog.Format(_T("[ofsMgr] CreateProcessAsUser %s Error - %d"), strExePath, GetLastError() );
	UM_WRITE_LOG(strLog);

	CloseHandle(hd);
	CloseHandle(newtoken);
	return FALSE;

}

BOOL CProcess::IsProcessExistByName(CString _strCompareProcessName)
{
	BOOL bGet = FALSE;
	BOOL bReturn = FALSE;

	HANDLE hSnapshot;  
	PROCESSENTRY32 ppe;     

	hSnapshot = m_pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다
	if (INVALID_HANDLE_VALUE == hSnapshot)
	{
		//	UM_WRITE_LOG(_T("GetProcessExist - Error 1"));
		{
			CString strLog = _T("");
			strLog.Format(_T("[TiorSaverReload] [ERROR] hSnapshot is INVALID_HANDLE_VALUE!! in IsProcessExist. _strCompareProcessName: %s, bReturn: %d [line: %d, function: %s, file: %s]"), _strCompareProcessName, bReturn, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-25 kh.choi 에러 로그 추가
			UM_WRITE_LOG(strLog);
		}
		return FALSE;
	}
	ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  

	CString strProcessName =_T(""), strLog;
	//DWORD dwGetSessionID;
	DWORD dwRetPid = -1;

	bGet = m_pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.

	while (bGet)  
	{
		strProcessName.Format(_T("%s"), ppe.szExeFile);
		strProcessName.MakeLower();

		//세션을 확인할 프로세스 이름 발견시
		if (0 == strProcessName.CompareNoCase(_strCompareProcessName)) {
			bReturn = TRUE;
			break;
		}

		bGet = m_pProcess32Next(hSnapshot, &ppe);  
	}  

	CloseHandle(hSnapshot);  

	return bReturn;  
}


BOOL CProcess::IsWindowLogin()
{
	DWORD dwCSID;
	dwCSID = GetRemoteCurrentSessionId();

	if(dwCSID == -1)	//원격이 아닐때
		dwCSID = WTSGetActiveConsoleSessionId();		//현재 활성화된 세션ID체크


	//strLog.Format(_T("[TiorSaverReload] ActiveconsoleSession Id: %d"), dwCSID);
	//UM_WRITE_LOG(strLog);
	if(dwCSID == -1)
	{
		return FALSE;
	}

	DWORD dwProcessID = IsGoodSessionID(dwCSID, EXPLORER_PROC_NAME);	//인자로 들어가는 dwCSID는 현재 Actice된 세션ID	
	if( dwProcessID > 0)
	{
		return TRUE;
	}

	return FALSE;
}

DWORD CProcess::GetRemoteCurrentSessionId()
{
	PWTS_SESSION_INFO ppSessionInfo = NULL;
	DWORD     pCount = 0, dwRetSessionID = -1;
	WTS_SESSION_INFO  wts;
	CString strStationName, strLog;
	WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0, 1, &ppSessionInfo, &pCount );
	//strLog.Format(_T("[TiorSaverReload] SessionCnt: %d"), pCount);
	//UM_WRITE_LOG(strLog);
	for( DWORD i = 0; i < pCount; i++ )
	{

		wts = ppSessionInfo[i];
		LPTSTR  ppBuffer        = NULL;
		DWORD   pBytesReturned  = 0;

		//strLog.Format(_T("[TiorSaverReload] SessionID: %d"), wts.SessionId);
		//UM_WRITE_LOG(strLog);
		if(wts.SessionId == RDP_LISTENING_SESSION)			//해당 세션은 리모드 리스닝 세션이므로 무시.
			continue;

		if( WTSQuerySessionInformation( WTS_CURRENT_SERVER_HANDLE,
			wts.SessionId,
			WTSWinStationName,
			&ppBuffer,
			&pBytesReturned) )
		{
			strStationName.Format(_T("%s"), ppBuffer);
			//UM_WRITE_LOG(_T("[TiorSaverReload] SessionName : ") +  strStationName);
			if(strStationName.Find(_T("RDP")) != -1)		//원격데스크톱 session-name를 가진 세션ID를 리턴
			{			
				WTSFreeMemory( ppBuffer );
				dwRetSessionID = wts.SessionId;
				break;
			}

		}   
		WTSFreeMemory( ppBuffer );

	}

	return dwRetSessionID;
}