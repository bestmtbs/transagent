/**
@file						GeneralUtil.h
@project				ITCMS
@product company	DSNTCH  digital solution & technology partner
@url						www.dsntech.com
@division				ITCMS Div
@date					2013년 4월 30일
*/
#ifndef _GENERAL_
#define _GENERAL_


#define PLATFORM_X86						1
#define PLATFORM_X64						2
#define PLATFORM_IA64					3


#define VISTA_MAJOR_VERSION  6  

#define VISTA_MINOR_VERSION		0
#define OS_MINOR_WIN7_2K8		1
#define OS_MINOR_WIN8_2K12		2
#define OS_MINOR_WIN81_2K12R2	3


#define WINXP									1
#define WIN2K3								2
#define WINVISTA							3
#define WINVISTAMORE					3 // Same vista
#define WIN2K8								4
#define WIN7									5
#define WIN2K8R2							6
#define WIN8									7
#define WIN2K12								8
#define WIN81									9
#define WIN2K12R2							10


LPVOID AllocVirtual(DWORD dwSize);
BOOL FreeVirtual(LPVOID pMem);
ULONG GetVirtualSize(LPCVOID *lpBuffer);

BOOL GetSecurityDesc(PSECURITY_DESCRIPTOR psd);

BOOL IsUsbDevice( WCHAR letter );// Kevin(2013-7-29)

int CreateMemFileMap(IN TCHAR *pstrSharedName, IN int nSize, OUT HANDLE *phShared, OUT VOID **ppSharedInfo);
int OpenMemFileMap(IN TCHAR *pstrSharedName, IN int nSize, OUT HANDLE *phShared, OUT VOID **ppSharedInfo);
BOOL CloseMemFileMap(IN HANDLE hSharedInfo, VOID *pSharedInfo);

typedef BOOL (WINAPI *PFN_QueryFullProcessImageNameW)(
    __in HANDLE hProcess,
    __in DWORD dwFlags,
    __out_ecount_part(*lpdwSize, *lpdwSize) LPWSTR lpExeName,
    __inout PDWORD lpdwSize
    );

typedef BOOL (WINAPI *PFN_EnumProcessModules)(
    __in  HANDLE hProcess,
    __out_bcount(cb) HMODULE *lphModule,
    __in  DWORD cb,
    __out LPDWORD lpcbNeeded
    );

int GetWinVer();
int GetWinVer2();// Kevin(2014-4-1)
int GetPlatform();// Kevin(2013-5-13)
int GetCPUCount();// Kevin(2013-6-11)
void CPUCountSleep(ULONG *pulPrevCount, ULONG ulInterval=1000, ULONG ulSleep=2);
BOOL ChkCPUCount(ULONG *pulPrevCount, ULONG ulInterval=1);// Kevin(2013-11-11)
void CountSleep(ULONG *pulPrevCount, ULONG ulInterval=1, ULONG ulSleep=2); // Kevin(2014-1-21) 그냥 슬립

// Kevin Start(2013-4-18)
int UnicodeToAnsi3(WCHAR *pszW, CHAR *pszA);
int AnsiToUnicode3(char *strAnsi, TCHAR *strUnicode);
BOOL IsWinVista();
BOOL GetProcessPathNameFromPid(DWORD dwID, TCHAR *pszName, int nLength);

int OfsGetFileTime(WCHAR *pFileName, LPFILETIME lpCreationTime, LPFILETIME lpLastAccessTime, LPFILETIME lpLastWriteTime);
BOOL IsFileExist(WCHAR *pFileName);// Kevin(2013-6-6)
BOOL IsFileExistA(CHAR *pFileName);// Kevin(2013-6-6)
__int64 OfsGetFileSize(WCHAR *strFilePath);// Kevin(2013-8-14)
BOOL IsProtectedFile(WCHAR *pstrFile); // Kevin(2013-12-9)
ULONG GetFileExtension(WCHAR *pFile, WCHAR *pExt, ULONG ulLen);// Kevin(2014-1-20)

BOOL GetDecPathNameFromEncPathName(WCHAR *pstrEnc, ULONG ulLen, WCHAR *pDec, ULONG ulDecLen);
BOOL GetEncPathNameFromDecPathName(WCHAR *pstrDec, ULONG ulLen, WCHAR *pEnc, ULONG ulDecLen);
BOOL GetPatternName(WCHAR *_strNumber, WCHAR *pRet, ULONG ulLen);


DWORD GetProcessID(WCHAR* pszWProcessName, int dwSessionID = -1); 

BOOL  ForceProcessKill(CString& strKillProcessList, BOOL _bSetDebugPrivilege = FALSE, int nRoopSleep = 5, BOOL bLowerCompare = TRUE, CStringArray* parrKilledProcessList = NULL); //hhh(2013-08-16)

BOOL GetNewEncPathName(WCHAR *pstrDecName, WCHAR *pstrNewEncName, ULONG ulLen);// Kevin(2013-7-4)

// Kevin(2013-7-18)
// Engine 동작 유무 확인함수
BOOL IsEngineRunning(); 

void ResumeProcess(DWORD dwPid);					//hhh(2013.07.24)
BOOL SetPrivilegeToken( HANDLE hToken, LPCTSTR Privilege, BOOL bEnablePrivilege );

BOOL IsNetworkDrive(WCHAR *pstrDrv);
DWORD GetNetDrives();// Kevin(2013-11-11) GetLogicalDrives() for network drive
BOOL	GetVolumeDiskNumber(TCHAR* szLetter, int& _nDiskNumber); //hhh- 2014.02.25
BOOL	GetDeviceNumber_USE_DrvLetter(TCHAR* szLetter, int& _nDeviceNumber); //hhh- 2014.02.25
DWORD	GetDeviceNumber( HANDLE deviceHandle );

BOOL GetNextNoFileName(WCHAR *pSrcName, WCHAR *pDestName, ULONG ulLen);
DWORD	GetPid_UseProcessName(WCHAR* pszWProcessName, DWORD _dwParentPid = 0);
#endif