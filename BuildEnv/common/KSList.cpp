// KSList.cpp: implementation of the KSList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "KSList.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/**
	@file	KSList.cpp
	@brief	object List 클래스 구현

	@author	최초 작성자 : 강기수
	@author 마지막 수정자 : 강기수

	@date 최초 작성일 : 2006.08.01
	@date 마지막 수정일 : 2006.09.07

    리스트 자료구조 클래스 구현
*/
KSList::KSList()
{
	headPosition();
}

KSList::~KSList()
{
	clear();
}

void KSList::clear()
{
	elementList.clear();
}

KSList::Position KSList::headPosition()
{
	return elementList.begin();
}

KSList::Position KSList::tailPosition()
{
	return elementList.end();
}

bool KSList::isEnd( Position pos )
{
	if( pos == elementList.end() )
		return true;

	return false;
}

int KSList::size()
{
	return elementList.size();
}

KSList::Position KSList::find( KSObject* arg )
{
	KSSynchronize sync( this );
	Position pos = headPosition();

	while( !isEnd( pos ) )
	{
		if( getItem( pos ) == arg )
			return pos;
		
		pos++;
	}

	return elementList.end();
}

void KSList::pushHead( KSObject* arg )
{
	KSSynchronize sync( this );
	
	elementList.push_front( arg );
}

void KSList::pushTail( KSObject* arg )
{
	KSSynchronize sync( this );
	
	elementList.push_back( arg );
}

KSObject* KSList::popHead()
{
	KSSynchronize sync( this );

	KSObject* returnValue;

	if( size() == 0 )
		return 0;

	returnValue = getItem( headPosition() );
	elementList.pop_front();

	return returnValue;
}

void KSList::popTail()
{
	KSSynchronize sync( this );

	if( size() == 0 )
		return;

	elementList.pop_back();
}

KSObject* KSList::removeItem( Position pos )
{
	KSSynchronize sync( this );
	KSObject* item = 0;

	if( isEnd( pos ) )
		return 0;

	item = *pos;
	elementList.erase( pos );

	return item;
}

KSObject* KSList::getItem( Position pos )
{
	return *pos;
}

KSObject* KSList::removeHead()
{
	return removeItem( headPosition() );
}


KSStack::KSStack()
{
	headPosition();
}

KSStack::~KSStack()
{
	clear();
}

KSObject* KSStack::pop()
{
	return KSList::popHead();
}

void KSStack::push( KSObject* arg )
{
	KSList::pushHead( arg );
}

KSQueue::KSQueue()
{
	headPosition();
}

KSQueue::~KSQueue()
{
	clear();
}

KSObject* KSQueue::pop()
{
	return popHead();
}

void KSQueue::push( KSObject* arg )
{
	pushTail( arg );
}
