#include "StdAfx.h"
#include "HttpsTConnect.h"
#include <vector>
#include <string>

//#pragma comment(lib, "winhttp.lib")

#pragma comment(lib, "wininet.lib")

CHttpsTConnect::CHttpsTConnect(void)
{

	m_pConnection = NULL;
	m_pHttp = NULL;
	m_pSession = NULL;
	m_nPort = INTERNET_DEFAULT_HTTP_PORT;

}

CHttpsTConnect::~CHttpsTConnect(void)
{
	Clear();
}



/**
@brief      서버 도메인/ip설정 
@author   jhlee
@date      2011.06.23 
@param	[IN] _strDomain : 서버 도메인 또는 ip
@note       HttpConnect를 사용하기전 serverinfo 설정이 필요하다. 
				추가 2012.12.07: 도메인을 ip로 변경하는 루틴 추가
*/
void CHttpsTConnect::SetServerInfo(CString _strDomain)
{
	CString strServer;
	m_nPort = INTERNET_DEFAULT_HTTP_PORT;
	strServer = _strDomain;
	int nPos = _strDomain.Find(_T("://"));
	if(-1 != nPos){
		strServer = _strDomain.Mid(nPos+3);
		if(5== nPos)
			m_nPort = INTERNET_DEFAULT_HTTPS_PORT;
		
	}

	nPos = strServer.Find(_T("/"));

	if(-1 != nPos){
		m_strUri = strServer.Mid(nPos+1);
		strServer = strServer.Mid(0,nPos);
	}
	m_strDomain = strServer;
}

CString CHttpsTConnect::GetTokenFromAgent()
{
	return m_strToken;
}

void CHttpsTConnect::SetTokenFromAgent(CString _strToken)
{
	m_strToken = _strToken;
}




/**
@brief      핸들 종료 
@author   jhlee
@date      2011.06.23 
*/
void CHttpsTConnect::Clear()
{
	CString strLog = _T("");
	m_LastErrorCode =0;

	try {
		if( m_pHttp != NULL )
		{	
			m_pHttp->Close();
			delete m_pHttp;
			m_pHttp = NULL;
		}

		if( m_pConnection != NULL )
		{
			m_pConnection->Close();
			delete m_pConnection;
			m_pConnection = NULL;
		}

		if( m_pSession != NULL )
		{
			m_pSession->Close();
			delete m_pSession;
			m_pSession = NULL;
		}
	} catch(...) {
		strLog.Format(_T("[ReqTimeOut][Clear-catch][line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
		//////ezLog(_T("[HttpsTConnect][Clear-catch][line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);
	}
}





/**
@brief      웹서버 연결 
@author   jhlee
@date      2011.06.23 
@param	[IN] _strURI : jsp 호출 페이지
@note       기능에 따라 jsp 호출 페이지가 다르기 때문에 url 은 입력 받아야 한다.
@return    BOOL(false/true)  
*/
BOOL CHttpsTConnect::HttpConnect ( CString _strURI)
{
#ifdef _SHOW_HTTP_DETAIL_LOG_
	//////ezLog(_T("[HttpsTConnect] _strURI: %s, m_strUri: %s [line: %d, function: %s, file: %s]\r\n"), _strURI, m_strUri, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_

	if (0 <_strURI.GetLength()) {
		m_strUri = _strURI;
	}
	
		

	if (m_strUri.GetLength() < 1) {
		//////ezLog(_T("[HttpsTConnect] [ERROR] m_strUri empty. will return FALSE. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
		return FALSE;
	}
		
		


	//CString szUrl =_T("");
	//CString szServer = _T("");
	//CString szObject = _T("");
	//DWORD dsServerType = 0;
	//INTERNET_PORT nPort = 0;


//	DWORD dwFlags  = INTERNET_FLAG_RELOAD|INTERNET_FLAG_NO_CACHE_WRITE;
		DWORD dwFlags  =  INTERNET_FLAG_RELOAD|\
						  INTERNET_FLAG_DONT_CACHE | \
                          INTERNET_FLAG_EXISTING_CONNECT | \
                          INTERNET_FLAG_IGNORE_CERT_CN_INVALID | \
                          INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;

	if(INTERNET_DEFAULT_HTTPS_PORT == m_nPort)
		dwFlags |=  INTERNET_FLAG_SECURE;



	try
	{

		m_pSession = new CInternetSession();
		if (NULL == m_pSession) {
			//////ezLog(_T("[HttpsTConnect] [ERROR] new CInternetSession() return NULL. will throw \"Creating CInternetSession failed\" [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
			throw CString("Creating CInternetSession failed");
		}
#ifdef _KHCHOI_FAILED_TEST_
		///*defines the connection timeout in ms*/	DWORD dwTimeout = 1200000;InternetSetOption(*pHTTP,INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeout, sizeof(DWORD));
		DWORD dwTimeout = 1000;	// milliseconds
		//DWORD dwTimeout = 1;	// milliseconds
		//InternetSetOption(,INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeout, sizeof(DWORD));
		//m_pSession->SetOption(INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		//BOOL bRet = m_pSession->SetOption(INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeout, sizeof(dwTimeout), INTERNET_FLAG_DONT_CACHE);
		BOOL bRet = m_pSession->SetOption(INTERNET_OPTION_CONNECT_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		//BOOL bRet = m_pSession->SetOption(INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		DWORD dwRet = GetLastError();
		CString strMsg = _T("");
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);

		bRet = m_pSession->SetOption(INTERNET_OPTION_RECEIVE_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		dwRet = GetLastError();
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);

		bRet = m_pSession->SetOption(INTERNET_OPTION_SEND_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		dwRet = GetLastError();
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);

		bRet = m_pSession->SetOption(INTERNET_OPTION_DATA_RECEIVE_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		dwRet = GetLastError();
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);

		bRet = m_pSession->SetOption(INTERNET_OPTION_DATA_SEND_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		dwRet = GetLastError();
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);

		bRet = m_pSession->SetOption(HTTP_STATUS_REQUEST_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		dwRet = GetLastError();
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);

		bRet = m_pSession->SetOption(INTERNET_OPTION_LISTEN_TIMEOUT, &dwTimeout, sizeof(dwTimeout));
		dwRet = GetLastError();
		strMsg.Format(_T("[line: %d, function: %s, file: %s] SetOption() return value is %d\nGetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		AfxMessageBox(strMsg);
#endif
		m_pConnection = m_pSession->GetHttpConnection(m_strDomain,INTERNET_OPEN_TYPE_PRECONFIG,m_nPort,NULL,NULL);

#ifdef _KHCHOI_OLD_TEST_
		strMsg.Format(_T("[line: %d, function: %s, file: %s] m_pConnection : %p"), __LINE__, __FUNCTIONW__, __FILEW__, m_pConnection);
		AfxMessageBox(strMsg);
#endif

		if (m_pConnection == NULL) {
			//////ezLog(_T("[HttpsTConnect] [ERROR] GetHttpConnection() return NULL. will throw \"Http connection failed\" [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
            throw CString("Http connection failed");
		}

		if (m_bGet) {
			m_pHttp = m_pConnection->OpenRequest(CHttpConnection::HTTP_VERB_GET,m_strUri,NULL,1,NULL, _T("HTTP/1.1"),dwFlags);
		} else {
			m_pHttp = m_pConnection->OpenRequest(CHttpConnection::HTTP_VERB_POST,m_strUri,NULL,1,NULL, _T("HTTP/1.1"),dwFlags);
		}
#ifdef _KHCHOI_OLD_TEST_
		DWORD dwTimeout = 2000;	// milliseconds
		BOOL bRet = m_pHttp->SetOption(HTTP_STATUS_REQUEST_TIMEOUT, &dwTimeout, sizeof(dwTimeout));	// return FALSE
		DWORD dwRet = GetLastError();	// dwRet set 12009
		CString strMsg = _T("");
		strMsg.Format(_T("[ReqTimeOut][line: %d, function: %s, file: %s] SetOption() return value is %d | GetLastError() return value is %d"), __LINE__, __FUNCTIONW__, __FILEW__, bRet, dwRet);
		UM_WRITE_LOG(strMsg);
		//AfxMessageBox(strMsg);
		strMsg.Format(_T("[ReqTimeOut][line: %d, function: %s, file: %s] m_pHttp : %p"), __LINE__, __FUNCTIONW__, __FILEW__, m_pHttp);
		UM_WRITE_LOG(strMsg);
		//AfxMessageBox(strMsg);
#endif

#ifdef _KHCHOI_TEST_
		CString strMsg = _T("");
		strMsg.Format(_T("[ReqTimeOut][line: %d, function: %s, file: %s] m_pSession: %p, m_hSession: %p, m_pConnection: %p, m_pHttp: %p"),
			__LINE__, __FUNCTIONW__, __FILEW__, m_pSession, (HINTERNET)*m_pSession, m_pConnection, m_pHttp);
		UM_WRITE_LOG(strMsg);
#endif

		if (!m_pHttp) {
			//////ezLog(_T("[HttpsTConnect] [ERROR] OpenRequest() return NULL. will throw \"HttpFile connection failed\" [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
            throw CString("HttpFile connection failed");
		}


	}
	catch(CInternetException pEx){
#ifdef _SHOW_HTTP_DETAIL_LOG_
		DWORD dwLastError = GetLastError();
		//////ezLog(_T("[HttpsTConnect] catch(CInternetException pEx). GetLastError(): %d. will return FALSE. [line: %d, function: %s, file: %s]\r\n"), dwLastError, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_
		Clear();

		return FALSE;
	}
	catch (CString e) {
#ifdef _SHOW_HTTP_DETAIL_LOG_
		DWORD dwLastError = GetLastError();
		//////ezLog(_T("[HttpsTConnect] catch (CString e). e: %s, GetLastError(): %d. will return FALSE. [line: %d, function: %s, file: %s]\r\n"), e, dwLastError, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_
        	Clear();

			m_ErrorMsg = e;

		//	prank test for men
		return FALSE;
    }

#ifdef _KHCHOI_TEST_
	CString strMsg = _T("");
	strMsg.Format(_T("[ReqTimeOut][line: %d, function: %s, file: %s] m_pSession: %p, m_hSession: %p, m_pConnection: %p, m_pHttp: %p"),
		__LINE__, __FUNCTIONW__, __FILEW__, m_pSession, m_pSession->operator HINTERNET(), m_pConnection, m_pHttp);
	UM_WRITE_LOG(strMsg);
#endif


	return TRUE;

}

CString CHttpsTConnect::HttpReadRequest(   CString _strParameter )
{

	if (NULL == m_pHttp) {
		//////ezLog(_T("[HttpsTConnect] [ERROR] m_pHttp is NULL. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
		return _T("");
	}


	CString strRead = _T(""),strReadData = _T("");

	try
	{

		CString szHeader = _T("");
		CString strDataLength = _T("");
		CString strTokenHeader = _T("");
		CString strToken = _T("");
		CStringA strData;

		if (!m_bGet) {
			//strData = Utf8_Encode(_T("device=NOYE5HO&device_uuid=uuid2&ver=1.0&filename=D:\DSNTECH\WinTior\Log\20170801\sd_img.s01.20170707.ncvi&data=RABTAE4AVABFAEMASAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBAGQAbQBpAG4AaQBzAHQAcgBhAHQAbwByAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIAMAAxADcAMAA4ADAAMQAxADcAMQAwADIAOQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAC0xAQAAAAAANAAAAHU3gFkAAAAALTEBAAEAAACKvAsAejeAWQAAAAAtMQEAAgAAABAbDAB/N4BZAAAAAC0xAQADAAAAWhwMAI43gFkAAAAALTEBAAQAAACCqQ4AmDeAWQAAAAAtMQEABQAAAOeqDgCuN4BZAAAAAC0xAQAGAAAAAK0OAK83gFkAAAAALTEBAAcAAABQsQ4AsTeAWQAAAAAtMQEACAAAAHO1DgCxN4BZAAAAAC0xAQAJAAAANTwRALE3gFkAAAAALTEBAAoAAADDbBMAsjeAWQAAAAAtMQEACwAAAHYKFACyN4BZAAAAAC0xAQAMAAAAFjEUALY3gFkAAAAALTEBAA0AAAC5nBQAtjeAWQAAAAAtMQEADgAAAJDpFAC5N4BZAAAAAC0xAQAPAAAAW7kZALk3gFkAAAAALTEBABAAAABYExoAujeAWQAAAAAtMQEAEQAAADWRHAC6N4BZAAAAAC0xAQASAAAAoqscAL43gFkAAAAALTEBABMAAADDqxwAvjeAWQAAAAAtMQEAFAAAAIl9HgC/N4BZAAAAAC0xAQAVAAAARRwhAL83gFkAAAAALTEBABYAAADBHiEAvzeAWQAAAAAtMQEAFwAAAC8gIQDAN4BZAAAAAC0xAQAYAAAATNEiAMA3gFkAAAAALTEBABkAAACMPCMAwDeAWQAAAAAtMQEAGgAAABlzIwDAN4BZAAAAAC0xAQAbAAAAAqEkAME3gFkAAAAALTEBABwAAABvuyQA0DeAWQAAAAAtMQEAHQAAAJ0qLQDQN4BZAAAAAC0xAQAeAAAAnNIxANM3gFkAAAAALTEBAB8AAAC%2brDIA0zeAWQ%3d%3d"));
			strData = Utf8_Encode(_strParameter);
		}
		int nLen = strData.GetLength();
		strDataLength.Format(_T("Content-Length: %d\r\n"),nLen);
		//szHeader  = _T("Content-type:application/json\r\n");	2017-07-20 sy.choi
		szHeader  = _T("Content-type:application/x-www-form-urlencoded\r\n");
		strToken = GetTokenFromAgent();
		if (!strToken.IsEmpty()) {
			strTokenHeader.Format(_T("Authorization: Bearer %s\r\n"), strToken);
			szHeader += strTokenHeader;
		}
		szHeader  +=strDataLength;
		szHeader  +=_T("Content-Language:UTF-8\r\n");
#ifdef _SHOW_HTTP_DETAIL_LOG_
		//////ezLog(_T("[HttpsTConnect] #1 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_

		if (m_pHttp) {
			m_pHttp->AddRequestHeaders(szHeader);
		} else {
			//////ezLog(_T("[HttpsTConnect] [ERROR] m_pHttp is NULL. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
			return _T("");
		}

#ifdef _SHOW_HTTP_DETAIL_LOG_
		//////ezLog(_T("[HttpsTConnect] #2 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_
	
		BOOL bResult = m_pHttp->SendRequest(NULL,0,(LPVOID)strData.GetString(),strData.GetLength());

		if (FALSE == bResult) {
			CString strLog = _T("");
			strLog.Format(_T("[HttpsTConnect] [ERROR] SendRequest return FALSE. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
			return _T("");
		}

#ifdef _SHOW_HTTP_DETAIL_LOG_
		//////ezLog(_T("[HttpsTConnect] #3 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_


		DWORD dwRet = 0;
		BOOL bRet = m_pHttp->QueryInfoStatusCode(dwRet);
		if (HTTP_STATUS_OK != dwRet) {
			DWORD dwGetLastError = GetLastError();
			//////ezLog(_T("[HttpsTConnect] [ERROR] HttpQueryInfo() StatusCode is not HTTP_STATUS_OK. bRet: %d, dwRet: %d (winhttp.h), dwGetLastError(): %d [line: %d, function: %s, file: %s]\r\n"), bRet, dwRet, dwGetLastError, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
			return _T("");
		}

#ifdef _SHOW_HTTP_DETAIL_LOG_
		//////ezLog(_T("[HttpsTConnect] #4 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_

		TCHAR szResp[10240]={0,};			

		if (NULL == m_pHttp) {
			//////ezLog(_T("[HttpsTConnect] [ERROR] m_pHttp is NULL. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
			return _T("");
		}

		while ( NULL != m_pHttp->ReadString(szResp,10240) ) {
#ifdef _SHOW_HTTP_DETAIL_LOG_
			//////ezLog(_T("[HttpsTConnect] #5 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_
			strRead+=Utf8_Decode((char*)szResp);
#ifdef _SHOW_HTTP_DETAIL_LOG_
			//////ezLog(_T("[HttpsTConnect] #6. strRead: %s [line: %d, function: %s, file: %s]\r\n"), strRead, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_

			ZeroMemory(&szResp,sizeof(TCHAR)*10240);
#ifdef _SHOW_HTTP_DETAIL_LOG_
			//////ezLog(_T("[HttpsTConnect] #7 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_
			if (NULL == m_pHttp) {
				//////ezLog(_T("[HttpsTConnect] [ERROR] m_pHttp is NULL. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
				return _T("");
			}
		}

#ifdef _SHOW_HTTP_DETAIL_LOG_
		//////ezLog(_T("[HttpsTConnect] #8 [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
#endif //#ifdef _SHOW_HTTP_DETAIL_LOG_
	}
	catch(...)
	{
		DWORD dwLastError = GetLastError();
		//////ezLog(_T("[HttpsTConnect] [ERROR] catch. GetLastError(): %d [line: %d, function: %s, file: %s]\r\n"), dwLastError, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-03-02 kh.choi 파일로그
		return _T("");
	}
	//catch(CInternetException pEx){
		
	//}
	//catch (CString e) {
	//	m_ErrorMsg = e; 
	//}

	//Clear();	<- 소멸자에서 호출됨. 필요없어서 주석처리
	return strRead;
}


CStringA   CHttpsTConnect::Utf8_Encode(CStringW strData)
{


	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), NULL, 0, NULL, NULL);
    std::string strTo( size_needed+1, 0 );
    WideCharToMultiByte                  (CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), &strTo[0], size_needed, NULL, NULL);
	return strTo.c_str();
}

CStringW     CHttpsTConnect::Utf8_Decode(CStringA strData)
{
	int size_needed = MultiByteToWideChar(CP_UTF8, 0,strData.GetString(), -1, NULL, 0);
    std::wstring wstrTo( size_needed+1, 0 );
	wstrTo.clear();
    MultiByteToWideChar (CP_UTF8, 0, strData.GetString(),-1, &wstrTo[0], size_needed+1);
    return wstrTo.c_str();
}
