
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsFile.h 
 @brief     File/Directory 관련 function 정의 파일 
 @author    hang ryul lee
 @date      create 2007.06.22 

 @note 
*/

#pragma once
#include "StringListEx.h"


//Directory Path 관련 함수
CString ExtractFilePath(const CString &_sFileName);
void ExtractFilePath(const CString &_sFileName, CString &_sPath);
CString ExtractFileName(const CString &_sFileName);
CString ExtractBaseFileName(const CString &_sFileName);
CString ExtractFileExt(const CString &_sFileName);
CString ExtractFileDrive(const CString &_sFileName);


CString ExcludeTrailingBackslash(const CString &_sName);
void ExcludeTrailingBackslash(const CString &_sSrcName, CString &_sDestName);

CString IncludeTrailingBackslash(const CString &_sName);

CString GetCurrentDrive();

//File 관련 함수
bool FileExists(const CString &_sFileName);
bool FileExistsEnvPathExt(const CString &_sFileName);
bool FileExistsEnvPath(const CString &_sFileName);
int    FileWrite(char* pzFileName, char* pzMsg, bool bNext=true);
BOOL IsFile(CString _strPath);			//폴더인지 파일인지 체크
BOOL ExistPath(CString _strPath);		//경로가 존재하는지(폴더 및 파일 유무 확인)


//환경 경로, 환경확장자 얻어오는 함수
bool GetEnvironmentPath(CStringListEx &_PathList);
bool GetEnvironmentPathEx(CStringListEx &_ExList);

bool ForceDeleteFile(const CString &_sFileName, DWORD *_pdwErrorCode = NULL);
bool ForceCopyFile(const CString &_sSrcFile, const CString &_sDestFile, DWORD *_pdwErrorCode = NULL); 
bool ForceMoveFile(const CString &_sOldFile, const CString &_sNewFile, DWORD *_pdwErrorCode = NULL);
DWORD GetFileLength(const CString &_sFileName);

//Directory관련 함수
bool ForceCreateDir(CString _sDir, DWORD *_pdwErrorCode = NULL);
bool ForceDeleteDir(const CString &_sPath, DWORD *_pdwErrorCode = NULL);
bool ForceDeleteDir2(const CString &_sPath, CString strTargetFile, DWORD dwRemoveDir, DWORD *_pdwErrorCode /*= NULL*/);
bool DirectoryExists(const CString &_sName);
bool ForceCopyDir(const CString &_sSrcDir, const CString &_sDestDir, DWORD *_pdwErrorCode = NULL);

INT GetSubDirNames(const CString &_sPath, CStringList &_sListDirs, const bool _bHidden = false);
INT GetFileNames(const CString &_sPath, CStringList &_sListFiles, const bool _bHidden = false);

CString GetCurrentModuleFileName();
CString GetCurrentModulePath();

BOOL DropFile(DWORD nID ,CString desFile,CString strType);

BOOL DeCompressFile(CString srcFile, CString desPath);
BOOL ResourceCreateFile(WORD _iResource, CString& _strFilePath, CString _strType);

BOOL GetFileTimeInfo(CString _strFilePath, CTime& _tmCreat, CTime& _tmAccess, CTime& _tmWrite, DWORD* _pdwError=NULL);  //hhh(2013.10.25)
BOOL ReadUnicodeFile(CString _strPath, CString& _strReadData, CString _strSeparator=L"");
BOOL ReadAnsiFileTxt(CString _strPath, CString& _strReadData, CString _strSeparator=L"");	///hhh(2014.06.03)
BOOL WriteFileTxt(CString _strOutPath, CString _strWriteData, UINT _nFlag);				///hhh(2014.06.03)
BOOL WriteFileExample(CString _strPath, CString _strToWrite, DWORD& _dwBytesWritten, BOOL _bIsAppend = FALSE);
void DROP_TRACE_LOG(CString _strLogFile, CString _strToWrite);
UINT GetDirectorySize(CString strDirPath);
__int64 GetFileSize(CString  _strFilePath);