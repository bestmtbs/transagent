
/**
 @file      IniFileEx.cpp 
 @brief     IniFile Wrapper Class 구현 파일  
 @author    hang ryul lee
 @date      create 2007.07.16 

 @note      64K 이상의 Ini형식의 파일을 관리 할 수 있다.
 @note      한라인에 문자열 길이가 256이상의 값도 Read/Write 할 수 있다.
 @note      UpdateFile()을 호출해야 수정내용이 파일에 적용된다.
*/

#include "stdafx.h"
#include "IniFileEx.h"
#include <tchar.h>
#include <strsafe.h>
/**
 @brief     기본 생성자
 @author    hang ryul lee
 @date      2007.07.16
 @note      기본 생성자 사용을 금지하기위해 protected 영역에 구현함.
*/
CIniFileEx::CIniFileEx()
{
    m_sFileName = _T("");
}

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2007.07.16
*/
CIniFileEx::CIniFileEx(const CString &_sFileName)
{
	m_sFileName = _sFileName;
	m_pSections = new CObList();
	LoadValues();
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2007.07.16
*/
CIniFileEx::~CIniFileEx()
{
	if (m_pSections)
		Clear();

	delete m_pSections;
	m_pSections = NULL;
}

/**
 @brief     Ini FileName을 구한다.
 @author    hang ryul lee
 @date      2007.07.16
 @return    FileName
*/
CString CIniFileEx::GetFileName() const
{
    return m_sFileName;
}


/**
 @brief     Ini 파일로부터 내용을 메모리로 읽어들인다.
 @author    hang ryul lee
 @date      2007.07.16 
 @return    void
 @bug       2008.04.11 odkwon \
            파일을 오픈후 close를 안해서 handle이 증가하는것을 수정 
*/
void CIniFileEx::LoadValues()
{
	if ((m_sFileName != _T("")) && (FileExists((m_sFileName))))
	{		
		CStdioFile IniFile;
		CString sBuffer = _T("");
		
		if (!IniFile.Open(m_sFileName, CFile::modeRead | CFile::shareDenyNone))
			return;

		CStringList pList;
		try
		{
			while (IniFile.ReadString(sBuffer))
			{
               	pList.AddTail(sBuffer);
			}
			SetStrings(pList);
		}
		catch(...)
		{
		}
        IniFile.Close();
	}
	else
	{
		Clear();
	}
}

/**
 @brief     메모리에 존재하던 Ini 파일내용을 모두 Clear한다. 
 @author    soon cheol schin
 @date      2007.07.16
 @return    void
*/
void CIniFileEx::Clear()
{
	if (m_pSections->GetCount() <= 0) 
		return;

	POSITION pos = m_pSections->GetHeadPosition();
	while (pos != NULL)
	{
		SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetNext(pos);
		if (pSecInfo != NULL)
		{
			pSecInfo->sName = _T("");
			if (pSecInfo->pData != NULL)
			{
				delete pSecInfo->pData;
				pSecInfo->pData = NULL;
			}

			delete pSecInfo;
			pSecInfo = NULL;
		}
	}
	m_pSections->RemoveAll();
}


/**
 @brief     CStringList로부터 정보를 읽어들인다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _rList [in] Ini파일정보를 포함하고 있는 CStringList
 @return    void
 @note      공백 문자열을 읽어들이지 않는다.
 @note      첫글자가 ";"인 주석 문자열을 읽어들이지 않는다
*/
void CIniFileEx::SetStrings(CStringList &_rList)
{
	Clear();

	CStringList *pSections = NULL;
	POSITION pos = _rList.GetHeadPosition();
	while (pos != NULL)
	{
		CString sTemp = _rList.GetNext(pos);        
		if ((sTemp == _T("")) || (sTemp.Left(1) == _T(";")))
			continue;

		if ((sTemp.Left(1) == _T("[")) && (sTemp.Right(1) == _T("]")))
			pSections = AddSection(sTemp.Mid(1, sTemp.GetLength() - 2));
		else
			if (pSections != NULL)
				pSections->AddTail(sTemp);				
	}	
}


/**
 @brief     Ini 파일에 섹션을 추가한다. 
 @author    hang ryul lee
 @date      create 2007.07.16
 @param     _sSection [in] 섹션명
 @return    섹션에 포함되는 값을 관리하는 CStringList *
 @date      update 2007.08.27 scshin \n
            메모리 누수가능성이 존재하는 부분을 수정함.
*/
CStringList *CIniFileEx::AddSection(const CString &_sSection)
{
	SECTION_INFO *pSecInfo = new SECTION_INFO();

    try
    {
	    pSecInfo->sName = _sSection;
	    pSecInfo->pData = new CStringList();
	    m_pSections->AddTail((CObject *)pSecInfo);

	    return pSecInfo->pData;
    } 
    catch (...)
    {
        if ((pSecInfo != NULL) && (pSecInfo->pData != NULL))
            delete pSecInfo->pData;

        if (pSecInfo != NULL)
            delete pSecInfo;

        return NULL;
    }
}


/**
 @brief     섹션정보를 관리하는 리스트에서 주어진 섹션의 POSITION값을 구한다. 
 @author    hang ryul lee
 @date      create 2007.07.16
 @param     _sSection [in] 섹션명
 @return    POSITION값 : 주어진 섹션명의 POSITION값. \n
            NULL       : 주어진 섹션명이 존재하지 않음.
*/
POSITION CIniFileEx::FindSection(const CString &_sSection)
{
	if (m_pSections == NULL) return NULL;

    POSITION posBefore = NULL;
	POSITION pos = m_pSections->GetHeadPosition();
	while (pos != NULL)
	{
        posBefore = pos;
		SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetNext(pos);
		if (pSecInfo != NULL)
		{
			if (_tcsicmp(pSecInfo->sName, _sSection) == 0)
				return posBefore;
		}
	}
	return NULL;
}


/**
 @brief     섹션에 포함된 리스트중에서 주어진 KeyName의 POSITION값을 구한다. 
 @author    hang ryul lee
 @date      create 2007.07.16
 @param     _pList [in] Ini파일정보를 포함하고 있는 CStringList
 @param     _sIdent [in] 검색을 원하는 KeyName값
 @return    POSITION값 : 주어진 KeyName의 POSITION값. \n
            NULL       : 주어진 KeyName이 존재하지 않음.
 @note      IniFile은 아래와 같은 형식으로 저장되어 있다.\n
            .저장 형식 \n
              [Section Name] \n
              \<KeyName\>=\<Value\> \n
              ...
 
*/
POSITION CIniFileEx::FindByName(CStringList *_pList, const CString &_sIdent)
{
	if (_pList == NULL) return NULL;

    POSITION posBefore = NULL;
	POSITION pos = _pList->GetHeadPosition();

	while (pos != NULL)
	{
        posBefore = pos;
		CString sTemp = _pList->GetNext(pos);
		int nIndex = sTemp.Find(_T("="));
		if (nIndex >= 0)
		{
			if (_tcsicmp(sTemp.Left(nIndex), _sIdent) == 0)
				return posBefore;
		}
	}
	return NULL;
}


/**
 @brief     섹션에 존재하는 해당Key값을 삭제한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @return    void
*/
void CIniFileEx::DeleteKey(const CString &_sSection, const CString &_sIdent)
{
	POSITION pos = FindSection(_sSection);
	if (pos == NULL) return;

	SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetAt(pos);
	if (pSecInfo == NULL) return;
	if (pSecInfo->pData == NULL) return;

	POSITION posName = FindByName(pSecInfo->pData, _sIdent);
	if (posName == NULL) return;
	pSecInfo->pData->RemoveAt(posName);
}


/**
 @brief     Ini 파일에서 해당 섹션을 삭제한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @return    void
 */
void CIniFileEx::EraseSection(const CString &_sSection)
{
	POSITION pos = FindSection(_sSection);
	if (pos == NULL) return;

	SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetAt(pos);
	if (pSecInfo == NULL) return;
	if (pSecInfo->pData != NULL)
	{
		delete pSecInfo->pData;
		pSecInfo->pData = NULL;
	}

	m_pSections->RemoveAt(pos);

	delete pSecInfo;
}


/**
 @brief     Ini 파일의 모든 내용을 읽어 파라미터로 넘어온 리스트에 추가한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _rList [ref] Ini 파일 내용을 저장할 CStringList
 @return    void
*/
void CIniFileEx::GetStrings(CStringList &_rList)
{
	if (m_pSections == NULL) return;

	POSITION pos = m_pSections->GetHeadPosition();
	while (pos != NULL)
	{
		SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetNext(pos);
		
		if (pSecInfo == NULL) continue;
		CString sSection = pSecInfo->sName;
        sSection = _T("[") + sSection + _T("]");
		_rList.AddTail(sSection);
		
		if (pSecInfo->pData == NULL) continue;
		POSITION posData = pSecInfo->pData->GetHeadPosition();
		while (posData != NULL)
		{
			_rList.AddTail(pSecInfo->pData->GetNext(posData));
		}
		_rList.AddTail(_T(""));
	}
}


/**
 @brief     섹션명에 포함되어 있는 모든 KeyName을 파라미터로 넘어온 리스트에 추가한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _rList [ref] KeyName을 저장할 CStringList
 @return    void
*/
void CIniFileEx::ReadSection(const CString &_sSection, CStringList &_rList)
{
	_rList.RemoveAll();

	POSITION pos = FindSection(_sSection);
	if (pos == NULL) return;

	SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetAt(pos);
	if (pSecInfo == NULL) return;
	if (pSecInfo->pData == NULL) return;
	POSITION posData = pSecInfo->pData->GetHeadPosition();
	while (posData != NULL)
	{
		CString sTemp = pSecInfo->pData->GetNext(posData);
		int nIndex = sTemp.Find(_T("="));
		if (nIndex < 0) continue;
		CString sName = sTemp.Left(nIndex);
		_rList.AddTail(sName);
	}
}

/**
 @brief     Ini 파일에 포함된 모든 섹션명을 파라미터로 넘어온 리스트에 추가한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _rList [ref] Ini 파일의 섹션명을 저장할 CStringList
 @return    void
*/
void CIniFileEx::ReadSections(CStringList &_rList)
{
	POSITION pos = m_pSections->GetHeadPosition();
	while (pos != NULL)
	{
		SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetNext(pos);
		
		if (pSecInfo == NULL) continue;
		CString sSection = pSecInfo->sName;
		_rList.AddTail(sSection);
	}
}


/**
 @brief     섹션명에 포함되어 있는 모든 값을 파라미터로 넘어온 리스트에 추가한다. 
 @author    hang ryul lee
 @param     _sSection   [in] 섹션명
 @param     _rList      [ref] 값을 저장할 CStringList
 @return    void
*/
void CIniFileEx::ReadSectionValues(const CString &_sSection, CStringList &_rList)
{
	_rList.RemoveAll();

	POSITION pos = FindSection(_sSection);
	if (pos == NULL) return;

	SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetAt(pos);
	if (pSecInfo == NULL) return;
	if (pSecInfo->pData == NULL) return;
	POSITION posData = pSecInfo->pData->GetHeadPosition();
	while (posData != NULL)
	{
		CString sValue = pSecInfo->pData->GetNext(posData);
		_rList.AddTail(sValue);
	}
}


/**
 @brief     Ini 파일의 해당 섹션에 bool값을 읽는다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @param     _bDefault [in] Default 값
 @return    읽은 bool 값
*/
bool CIniFileEx::ReadBool(const CString &_sSection, const CString &_sIdent, const bool &_bDefault)
{
    return ReadInt(_sSection, _sIdent, INT(_bDefault)) != 0;
}


/**
 @brief     Ini 파일의 해당 섹션에 INT값을 읽는다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @param     _nDefault [in] Default 값
 @return    읽은 INT 값
*/
INT CIniFileEx::ReadInt(const CString &_sSection, const CString &_sIdent, const INT &_nDefault)
{
    CString sTemp = ReadString(_sSection, _sIdent, _T(""));
    if (sTemp == "")
        return _nDefault;
    else
    {
        INT nValue = _nDefault;
        try
        {
            nValue = _ttoi(sTemp);
        }
        catch (...)
        {
        }
        return nValue;
    }
}


/**
 @brief     Ini 파일의 해당 섹션에 문자열값을 읽는다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @param     _sDefault [in] Default 값
 @return    읽은 문자열 값
*/
CString CIniFileEx::ReadString(const CString &_sSection, const CString &_sIdent, const CString &_sDefault)
{
	POSITION pos = FindSection(_sSection);
	if (pos == NULL) return _sDefault;

	SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetAt(pos);
	if (pSecInfo == NULL) return _sDefault;
	if (pSecInfo->pData == NULL) return _sDefault;

	POSITION posName = FindByName(pSecInfo->pData, _sIdent);
	if (posName == NULL) return _sDefault;
	CString sTemp = pSecInfo->pData->GetAt(posName);

	int nIndex = sTemp.Find(_T("="));
	if (nIndex < 0) return _sDefault;
	CString sValue = sTemp.Mid(nIndex + 1, sTemp.GetLength() - nIndex + 1);
	return sValue;
}


/**
 @brief     메모리에 존재하던 Ini 파일의 정보를 실제 파일로 쓴다. 
 @author    hang ryul lee
 @date      create 2007.07.16
 @return    void
 @date      update 2007.08.08 scshin \n
            .파일저장은 MultiByte형식으로 저장하도록 수정.
*/
void CIniFileEx::UpdateFile()
{
  	CStringList rList;
	GetStrings(rList);

    CStdioFile IniFile;
    if (!IniFile.Open(m_sFileName, CFile::modeCreate | CFile::modeWrite))
		return;

    IniFile.SeekToEnd();
    POSITION pos = rList.GetHeadPosition();
    CString sBuffer = _T("");
	CStringA sBufferA;
    while (pos != NULL)
    {
        sBuffer = rList.GetNext(pos) + _T("\n");
//  #ifdef _UNICODE
//        LPTSTR szTemp = {0, };
//         //szTemp = CW2A(sBuffer);
//  	   sBufferA = CW2A(sBuffer);
//         //IniFile.WriteString(sBufferA);//sBuffer);
// 		IniFile.Write(sBufferA, sBufferA.GetLength() );//sBuffer);
//  #else
        IniFile.WriteString(sBuffer.GetBuffer(0)) ;   
// #endif
    }
    IniFile.Close();
}


/**
 @brief     Ini 파일의 해당 섹션이 존재하는지 확인한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @return    true/false
*/
bool CIniFileEx::SectionExists(const CString &_sSection)
{
    CStringList rSectionList;
    ReadSection(_sSection, rSectionList);
    return rSectionList.GetCount() > 0;
}

/**
 @brief     Ini 파일의 해당 섹션에 Key값이 존재하는지 확인한다. 
 @author    hang ryul lee
 @date      create 2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @return    true/false
 @date      update 2007.08.24 scshin \N
            .키이름을 찾지못하던 버그 수정
*/
bool CIniFileEx::ValueExists(const CString &_sSection, const CString &_sIdent)
{
    CStringList rSectionList;
    ReadSection(_sSection, rSectionList);
 
    if (rSectionList.GetCount() <= 0) return false;

	POSITION pos = rSectionList.GetHeadPosition();
	while (pos != NULL)
	{
		CString sTemp = rSectionList.GetNext(pos);
		if (_tcsicmp(sTemp, _sIdent) == 0)
			return true;		
	}
	return false;
}


/**
 @brief     Ini 파일의 해당 섹션에 bool값을 기록한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @param     _bValue   [in] Value 값
 @return    void
*/
void CIniFileEx::WriteBool(const CString &_sSection, const CString &_sIdent, const bool &_bValue)
{
    if (_bValue)
        WriteString(_sSection, _sIdent, _T("1"));
    else
        WriteString(_sSection, _sIdent, _T("0"));
}

/**
 @brief     Ini 파일의 해당 섹션에 INT값을 기록한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @param     _nValue   [in] Value 값
 @return    void
*/
void CIniFileEx::WriteInt(const CString &_sSection, const CString &_sIdent, const INT &_nValue)
{
    TCHAR szValue[12];
    _itot_s(_nValue, szValue, 10);
    WriteString(_sSection, _sIdent, szValue);
}

/**
 @brief     Ini 파일의 해당 섹션에 문자열값을 기록한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sSection [in] 섹션명
 @param     _sIdent   [in] KeyName
 @param     _sValue   [in] Value 값
 @return    void
*/
void CIniFileEx::WriteString(const CString &_sSection, const CString &_sIdent, const CString &_sValue)
{
    CStringList *pList = NULL;

    POSITION pos = FindSection(_sSection);
	if (pos != NULL)
    {
        SECTION_INFO *pSecInfo = (SECTION_INFO *)m_pSections->GetAt(pos);
	    if ((pSecInfo != NULL) && (pSecInfo->pData != NULL))
            pList = pSecInfo->pData;
    }
    
    if (pList == NULL)
        pList = AddSection(_sSection);
    
    CString sTemp = _sIdent + _T("=") + _sValue;
	POSITION posName = FindByName(pList, _sIdent);
	if (posName != NULL)
        pList->SetAt(posName, sTemp);
    else
        pList->AddTail(sTemp);
}

/**
 @brief     파일 존재 유무를 판단한다. 
 @author    hang ryul lee
 @date      2007.07.16
 @param     _sFileName [in] 파일명
 @return    true  : 파일 존재. \n
            false : 파일 존재하지 않음.
*/
bool CIniFileEx::FileExists(const CString &_sFileName)
{
	bool bRet  = false;
	if(_sFileName.GetLength() == 0)
		return bRet;

	CString strFile = L"";
	CString strFile2 = _sFileName;

	
	try {
		strFile2.Format(L"\"%s\"", _sFileName);
		UM_WRITE_LOG(L"FileExists()");
		UM_WRITE_LOG(strFile2);
		int nstate = _waccess(strFile2.GetBuffer(), 0);
		if(nstate != -1)
		{
			bRet = true;
			//UM_WRITE_LOG
			UM_WRITE_LOG(L"FileExists exist");
		}
		else
		{
			WIN32_FIND_DATA				sFindData;
			HANDLE hFind = FindFirstFile(_sFileName, &sFindData);
			if(hFind != INVALID_HANDLE_VALUE)
			{
				bRet = true;
				UM_WRITE_LOG(L"FileExists from findfiirstfile exist");
				//UM_WRITE_LOG(L"IsFileExist from findfiirstfile exist");
				FindClose(hFind);
			}
			else
			{
				//UM_WRITE_LOG(L"IsFileExist not exist = %d", nstate);
				WCHAR str[MAX_PATH] ={0};
				StringCchPrintf(str, MAX_PATH, L"FileExists  findfiirstfile not exist LastError = %d", GetLastError());
				UM_WRITE_LOG(str);
			}
		}
	} catch(...)
	{
		UM_WRITE_LOG(L"FileExists() exception occure = ");
		UM_WRITE_LOG(strFile2);
	}
	return bRet;
}
