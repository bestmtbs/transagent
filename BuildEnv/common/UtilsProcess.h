
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      Utils.h 
 @brief     Process 관련 유틸 파일

 @author    odkwon
 @date      create 2008.09.11
 @note      Process 관련 유틸을 구현한다.
 @note      KillProcess, FindProcess\n
            OS Test 9x, NT , 2000, Vista Test
 @todo      Unit Test 해야함\n
            KillProcess, FindProcess 에 프로세스 파라메터는 현재 미지원\n
            NT이상에서 커멘드라인을 얻을수있으나 이를 적용할지는 결정해야함
 */
#pragma once
#include <Psapi.h>
#include <Wtsapi32.h>
#include "Impersonator.h"
#include "WinOsVersion.h"
#include "WTSSession.h"

#define RDP_LISTENING_SESSION	65536

typedef DWORD (WINAPI *fpWTSGetActiveConsoleSessionId)();
typedef HANDLE (WINAPI *fpCreateToolhelp32Snapshot) (DWORD, DWORD);
typedef BOOL (WINAPI *fpProcess32First) (HANDLE, LPPROCESSENTRY32);
typedef BOOL (WINAPI *fpProcess32Next) (HANDLE, LPPROCESSENTRY32);

// Process 관련 API 동적로딩함수
bool CallEnumProcesses(DWORD *_pdwProcessIds, DWORD _dwcb, DWORD* _pdwBytesReturned);
DWORD CallGetModuleBaseName(HANDLE _hProcess, HMODULE _hModule, LPTSTR _lpBaseName, DWORD _dwSize);
HANDLE CallCreateToolhelp32Snapshot(DWORD _dwFlags, DWORD _th32ProcessID);
bool CallProcess32First(HANDLE _hSnapshot, LPPROCESSENTRY32 _lppe);
bool CallProcess32Next(HANDLE _hSnapshot, LPPROCESSENTRY32 _lppe);
bool CallEnumProcessModules(HANDLE hProcess, HMODULE *lphModule, DWORD dwCb, DWORD* lpcbNeeded);
bool CallGetModuleFileNameEx(HANDLE hProcess, HMODULE hModule, LPTSTR lpFileName, DWORD nSize);

class CProcess
{
public:
	CProcess();/**< 기본 생성자 */
	virtual ~CProcess();                                /**< 파괴자 */

	CProcess(const CProcess &_rProcess);                /**< 복사 생성자 */
	CProcess& operator = (const CProcess &_CProcess);   /**< 대입 연산자 */

	fpCreateToolhelp32Snapshot m_pCreateToolhelp32Snapshot ;
	fpProcess32First   m_pProcess32First ;
	fpProcess32Next    m_pProcess32Next ;
	void CheckTiorProcess(CString _strPath, CString _strMutexName, CString _strParam = _T(""));
	int IsGoodSessionID(DWORD dwSessionID, CString _strCompareProcessName);
	BOOL StartProcessAsUser(DWORD pid, CString strExePath, CString _strParam=L"");
	BOOL IsProcessExistByName(CString _strCompareProcessName);	// 2017-05-08 kh.choi 프로세스 존재유무 체크
	BOOL	IsWindowLogin();
	DWORD GetRemoteCurrentSessionId();

	static BOOL KillProcessByName(CString sExeName);
    static bool KillProcess(const CString &_sFileName, const CString &_sParam = _T(""));  
    static bool KillProcess(const DWORD _dwPid);

    static bool FindProcess(const CString &_sFileName, const CString &_sParam = _T(""));
	static bool ProcessCreateMutex(CString strMutexName);
	static bool IsExistMutex(CString strMutexName);
	static bool IsProcessExist(CString strMutexName);
	static DWORD FindProcessWaitForInputIdle(CString _sFileName);

	static BOOL SetPrivilege( HANDLE hToken,  LPCTSTR Privilege, BOOL bEnablePrivilege);


private:                                         
    static bool KillProcess9x(const CString &_sFileName);
    static bool KillProcessNT(const CString &_sFileName, const CString &_sParam = _T(""));
    static bool FindProcess9x(const CString &_sFileName); 
    static bool FindProcessNT(const CString &_sFileName, const CString &_sParam = _T(""));

    static const DWORD FILENAME_MAX_LENGTH = 256;
	
    

    
    
};