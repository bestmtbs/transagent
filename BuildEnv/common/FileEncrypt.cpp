#include "StdAfx.h"
#include <tchar.h>
#include <strsafe.h>
#include <shlwapi.h>
#include "FileEncrypt.h"
#include "hash.h"
#include "hash_global.h"
#include "Base64.h"
#include "UtilsUnicode.h"
#include "UtilsFile.h"
#include "PathInfo.h"
#include "Crypto/des.h"
#include "Crypto/md5mac.h"
#include "CmsDBManager.h"

#include <WinCrypt.h>

#define ENCRYPT_EXTENTEION							_T(".ENC")
#define SELF_ENCRYPT_EXTENTEION						_T(".SENC")
#define SE_ENCKEY_BACKUP_FILE						_T("Ofs2K.sys")
#define SE_ENCKEY_BACKUP_ENC_FILE					_T("EOfs2K.sys")
#define SE_KEYBACKUP_KEY							_T("KAE@2dfva~58ADV")

USING_NAMESPACE(Crypto)

BOOL IsFileExist2(WCHAR *pFileName)
{
	BOOL bRet  = FALSE;
	if(pFileName == NULL)
		return bRet;
	if(lstrlen(pFileName) == 0)
		return bRet;

	try {

		WCHAR strFile[1024]={0};
		StringCchPrintf(strFile, 1024, L"\"%s\"", pFileName);
		DBGLOG(L"IsFileExist2t() = %s", strFile);
		int nstate = _waccess(strFile, 0);
		if(nstate != -1)
		{
			bRet = TRUE;
			DBGLOG(L"IsFileExist2t exist");
		}
		else
		{
			WIN32_FIND_DATA sFindData;
			HANDLE hFind = FindFirstFile(pFileName, &sFindData);
			if(hFind != INVALID_HANDLE_VALUE)
			{
				bRet = TRUE;
				DBGLOG(L"IsFileExist2 from findfiirstfile exist");

				FindClose(hFind);
			}
			else
			{
				DBGLOG(L"IsFileExist2 not exist = %d", nstate);
			}	
		}
	}
	catch(...)
	{
		DBGLOG(L"IsFileExist2 exception occure = %s", pFileName);
	}

	return bRet;
}

CFileEncrypt::CFileEncrypt(void)
{
	m_strDecPathName = L"";
	m_strEncPathName = L"";
	m_strSiteIdx = L"";
	m_strProductVersion = L"1.2.68.45";
	LoadSiteIdx();
	memcpy(m_Reserve, FILE_ENC_TYPE_DEFAULT, 2);
}

CFileEncrypt::~CFileEncrypt(void)
{
}

BOOL CFileEncrypt::SetTiorSaverVersion(CString strVer)
{
	m_strProductVersion = strVer;
	return TRUE;
}
BOOL CFileEncrypt::SetReserveData(BYTE *Reserve)
{
	if(Reserve == NULL)
		return FALSE;

	memcpy(m_Reserve, Reserve, 2);
	return TRUE;
}
BOOL CFileEncrypt::SetEncInfo(CString strVer, BYTE *Reserve)
{
	if(strVer.GetLength() > 0)
	{
		m_strProductVersion = strVer;
	}

	if(Reserve != NULL)
	{
		memcpy(m_Reserve, Reserve, 2);
	}
	return TRUE;

}
BOOL CFileEncrypt::LoadSiteIdx()
{
	CString strKeyFile = L"";
	WCHAR strPath[MAX_PATH];
	BOOL bFindSuccess = FALSE;	// 2017-05-18 kh.choi license.cms 파일에서 사이트 인덱스를 읽어온 경우 TRUE

	GetWindowsDirectory(strPath, MAX_PATH);
	//CString strLi = L"";
	strKeyFile .Format(L"%c:\\dsntech\\TiorSaver\\%s", strPath[0], _T("license.cms"));
	//strKeyFile.Format(_T("%s%s"), strInstallPath, _T("license.cms"));

	CFile cFile;
	if( cFile.Open(strKeyFile, CFile::modeRead))
	{
		DWORD dwLength  = (DWORD)cFile.GetLength();
		if(dwLength > 0)
		{
			char str[10] = {0};
			cFile.Read(str, 4);
			m_strSiteIdx = str;
			bFindSuccess = TRUE;
		}
		else
		{
			m_strSiteIdx = L"0000"; // default value
		}
		cFile.Close(); 
	}
	else	// 2017-05-22 kh.choi else 문 추가. license.cms 파일이 없거나 open 에 실패한 경우
	{
		m_strSiteIdx = L"0000"; // default value
	}

	//strKeyFile.ReleaseBuffer();
	//strKeyFile.Empty();	// 2017-05-17 kh.choi frees memory as appropriate. ReleaseBuffer() 아님. 둘 다 안해도 될 듯

	////ezLog(_T("[OfsCrypt] [#####] m_strSiteIdx: %s [line: %d, function: %s, file: %s]\r\n"), m_strSiteIdx, __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-22 kh.choi 파일로그

#ifndef _SETUP	// 2017-05-22 kh.choi OfsSetup 에서는 로컬DB에 입력되기 이전이라서 사용 못함
	// 2017-05-17 kh.choi license.cms 파일에서 읽어오기 실패한 경우 ltb_config 테이블 etc_3 필드에서 읽어오기
	if (!bFindSuccess) {
		CString strQuery = _T("");
		CString strLicenseKey = _T("");
		CString strSiteIndex = _T("0000");	// default value
		CStringArray arrList;

		arrList.RemoveAll();

		CCmsDBManager dbConfig(DB_AGENT_INFO);

		strQuery.Format(_T("SELECT * FROM %s"), DB_AGENT_INFO);
		BOOL bResult = dbConfig.SelectQuery(strQuery, &arrList, _T("etc_3"));
		dbConfig.Free();

		if (bResult) {
			if (arrList.GetSize() >= 1) {	// ltb_config table의 record는 1개 이므로 record 갯수만 확인만 하고 etc_3 field 반환
				strLicenseKey = arrList.GetAt(0);

				if (strLicenseKey.IsEmpty()) {
					//ezLog(_T("[OfsCrypt] [ERROR] ltb_config table etc_3 field is empty. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-17 kh.choi 파일로그
				} else {
					int nFind = strLicenseKey.Find(_T('-'));
					if (4 > nFind) {
						//ezLog(_T("[OfsCrypt] [ERROR] ltb_config table etc_3 field incorrect. strLicenseKey: %s [line: %d, function: %s, file: %s]\r\n"), strLicenseKey, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-17 kh.choi 파일로그
					} else {	// ltb_config 테이블의 etc_3 필드에서 읽어온 문자열에서 '-' 문자의 발견 위치가 4 이상인 경우
						strSiteIndex = strLicenseKey.Left(nFind);
					}
				}
			} else {
				//ezLog(_T("[OfsCrypt] [ERROR] ltb_config table is empty. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-17 kh.choi 파일로그
			}
		} else {
			DBGLOG(_T("Select query failed. strQuery: %s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
			//ezLog(_T("[OfsCrypt] [ERROR] Select query failed. strQuery: %s [line: %d, function: %s, file: %s]\r\n"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-17 kh.choi 파일로그
		}
		m_strSiteIdx = strSiteIndex;
	}
#endif //#ifndef _SETUP

	return TRUE;
}

//ThreadSafe문제로 간결화 작업은 추후로 미룸(각 암복호화 함수에서 독립적으로 처리 할 수 있도록 함.)

/*
BOOL CFileEncrypt::AES256_Init(CString _strKey)
{

	// Get the handle to the default provider. 
	if( FALSE == CryptAcquireContext(
		&m_hCryptProv, 
		NULL, 
		NULL, 
		PROV_RSA_AES, 
		CRYPT_VERIFYCONTEXT))
	{
		
	}

	// Create a hash object. 
	if(FALSE == CryptCreateHash(
		m_hCryptProv, 
		CALG_SHA_256, 
		0, 
		0, 
		&m_hHash))
	{
		
	}

	if(CryptHashData(
		m_hHash, 
		(BYTE *)_strKey.GetBuffer(0), 
		lstrlen(_strKey.GetBuffer(0)), 
		0))
	{	
		
	}	

	// Derive a session key from the hash object. 
	if(FALSE == CryptDeriveKey(
		m_hCryptProv, 
		CALG_AES_256, 
		m_hHash, 
		CRYPT_EXPORTABLE, 
		&m_hKey))
	{
		
	}
}*/


BOOL CFileEncrypt::SetDecFilePathName(CString strDecPathName)
{
	BOOL bRet = FALSE;
	if(strDecPathName.GetLength() > 0)
	{
		m_strDecPathName = strDecPathName;
		bRet = TRUE;
	}
	return bRet;
}

BOOL CFileEncrypt::SetEncFilePathName(CString strEncPathName)
{
	BOOL bRet = FALSE;
	if(strEncPathName.GetLength() > 0)
	{
		m_strEncPathName = strEncPathName;
		bRet = TRUE;
	}
	
	return bRet;
}

BOOL CFileEncrypt::EncFile(int _iInsUsrIdx, CString _strUserID, CString _strFilePath, CString _strEncFilePath, CString& _strKey, CString &_strHash, CString _strSiteUUID, BOOL _isSiteEncrypt, DWORD* pRetCode, DWORD* pErrPos)
{
	__int64 llEncSize = 0;
	BOOL bResult = TRUE;
	PBYTE pbBuffer = NULL;
	HANDLE hRead = NULL, hWrite = NULL;
	CString strKey = _T(""), strHash = _T(""), strMsg = _T(""), strEncFilePath = _T(""), strOneTimekey=_T("");

	DBGLOG(L"EncFile %s", _strFilePath.GetBuffer());
#ifdef _SYCHOI_TEST_
	//ezLog(_T("[OfsCrypt] Enter EncFile. _strFilePath: %s [line: %d, function: %s file: %s]\r\n"), _strFilePath, __LINE__, __FUNCTIONW__, __FILEW__);
#endif //#define _SYCHOI_TEST_

	if(_strFilePath.GetLength() == 0)
		return FALSE;

	if(IsFileExist2(_strFilePath.GetBuffer())  == FALSE)
		return FALSE;

	strHash = CreateFileHash(_strFilePath);
	if( strHash.IsEmpty() )
	{
		bResult = FALSE;
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -1;
		
		return FALSE;
	}

	// yjLee(2015-03-12) : 사이트키 암호화 여부에 따른 key 생성
	CString strBuff=_T("");
	if (_isSiteEncrypt)
		strBuff = _strSiteUUID;
	else
		strBuff = _strUserID;
		
	if( CreateKey(strBuff, strKey, strHash) == FALSE )
	{
		if(pRetCode)
			*pRetCode = 0;

		if(pErrPos)
			*pErrPos = -2;
		
		return FALSE;
	}
		
	_strHash = strHash;
	hRead  = CreateFile(_strFilePath.GetBuffer(),
									GENERIC_READ,// | GENERIC_WRITE,
									FILE_SHARE_READ,// | FILE_SHARE_WRITE,
									0,
									OPEN_EXISTING,
									NULL,
									0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DBGLOG(_T("%s read error (%d)"), _strFilePath.GetBuffer(), GetLastError());

		bResult = FALSE;
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -3;
		return FALSE;
	}

	
	hWrite = CreateFile(_strEncFilePath.GetBuffer(),
									GENERIC_READ | GENERIC_WRITE,
									FILE_SHARE_READ | FILE_SHARE_WRITE,
									0,
									CREATE_ALWAYS,
									NULL,
									0);

	if( hWrite == INVALID_HANDLE_VALUE || hWrite == NULL )
	{

		DBGLOG(_T("%s create error (%d)"), _strEncFilePath.GetBuffer(), GetLastError());
		CloseHandle(hRead);
		
		bResult = FALSE;
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -4;
		return FALSE;
	}

	DWORD dwEncBufferSize = 0, dwTotalFileSize = 0, dwCount = 0; 
	byte pseudoKey[FILE_ENC_KEY_LEN +1] = {0,};
	byte oneTimeKey[FILE_ENC_KEY_LEN+1]={0,};

	dwTotalFileSize = ::GetFileSize(hRead, NULL);
	ZeroMemory(oneTimeKey, sizeof(oneTimeKey));
	ZeroMemory(pseudoKey, sizeof(pseudoKey));// Kevin(2013-12-2)

	srand( (unsigned)time(NULL));
	for( int i=0; i < FILE_ENC_KEY_LEN; i++ )
		oneTimeKey[i] = (unsigned char)(rand() % 255);

	_strKey = strKey;

	memcpy(pseudoKey, oneTimeKey, FILE_ENC_KEY_LEN);

	Encrypt_Header header;
	memset(&header, 0, sizeof(Encrypt_Header));


	//create a header 
//#ifdef CRYPT_12
	StringCchCopyA(header.sig, 16, ITCMS_SIG2);
//#endif

	StringCchCopy(header.FileFullPathName, 1024, _strFilePath.GetBuffer());
	StringCchCopy(header.FileExt, 16, PathFindExtension(_strFilePath.GetBuffer()));
	StringCchCopy(header.FileHash, 64, strHash.GetBuffer());
	StringCchCopy(header.UserID, MAX_PATH, _strUserID.GetBuffer());
	StringCchCopy(header.Version, 32, m_strProductVersion.GetBuffer());
	if(m_strSiteIdx.GetBuffer() == 0)
		LoadSiteIdx();
	StringCchCopy(header.SiteIndex, 16, m_strSiteIdx.GetBuffer());
	StringCchCopy(header.SiteUUID, 40, _strSiteUUID.GetBuffer());	// yjLee(2015-03-12) : 암호화 헤더에 site uuid 추가

	SYSTEMTIME st;
	ZeroMemory(&st, sizeof(st));
	GetLocalTime(&st);
	CopyMemory(&header.FileCreateTime, &st, sizeof(st));
	header.length = dwTotalFileSize;

#ifdef _FILE_KEY_DISPLAY
	{
		BYTE Tmp[MAX_PATH] = {0};
		CHAR strSub[10] = {0};
		for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
		{
			ZeroMemory(strSub, 10);
			StringCchPrintfA(strSub, 10, "%02x", pseudoKey[i]);
			StringCchCatA((CHAR *)Tmp, MAX_PATH, strSub); 
		}
		DBGLOGA("sudokey = [%s]", Tmp);
	}

#endif
	//pseudoKey 암호화
	DWORD dwDatalen, dwErrorCode, dwErrorPos;
	dwDatalen = FILE_ENC_KEY_LEN;
	BYTE Key[MAX_PATH] = {0};
	//UnicodeToAnsi2(strKey.GetBuffer(), (char *)Key);
	//if( AES256_Enc2(Key, strKey.GetLength(), (BYTE*)pseudoKey, sizeof(pseudoKey), dwDatalen, &dwErrorCode, &dwErrorPos) )
	if( AES256_Enc(strKey.GetBuffer(0), (BYTE*)pseudoKey, sizeof(pseudoKey), dwDatalen, &dwErrorCode, &dwErrorPos) )
	{
		memcpy(header.pseudoKey, pseudoKey, dwDatalen);
		DBGLOG(L"header.pseudoKey OFFSET SUDO KEY LEN = %u", dwDatalen);
	}
	else
	{
 		DBGLOG(_T("Aes256 pseudoKey enc Err (ErrorCode: %d, ErrorPos: %d)"), dwErrorCode, dwErrorPos);
 		bResult = FALSE;
 		if(pRetCode)
 			*pRetCode = dwErrorCode;
 
 		if(pErrPos)
 			*pErrPos = dwErrorPos;
 
 		goto Exit_Enc;

	}

	header.dwKeyLen = dwDatalen;
#ifdef _FILE_KEY_DISPLAY
	/*DBGLOGA("enc sudokey = ");
	for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
	{
		DBGLOGA("%x", pseudoKey[i]);
	}*/
	{
		BYTE Tmp[MAX_PATH] = {0};
		CHAR strSub[10] = {0};
		for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
		{
			ZeroMemory(strSub, 10);
			StringCchPrintfA(strSub, 10, "%02x", pseudoKey[i]);
			StringCchCatA((CHAR *)Tmp, MAX_PATH, strSub); 
		}
		DBGLOGA("enc sudokey = [%s]", Tmp);
	}
#endif

	// Kevin(2013-6-3)
	// HEADER암호화

	DWORD dwWriteSize = 0;

	dwDatalen=dwErrorCode=dwErrorPos = 0;
	dwDatalen = sizeof(header); // - sizeof(header.caption) - sizeof(header.pseudoKey) - sizeof(header.mac);
	DWORD dwHeaderBlockLen = ENC_BLOCK_LEN *(dwDatalen / ENC_BLOCK_LEN);
	//if(dwDatalen % ENC_BLOCK_LEN > 0)
	dwHeaderBlockLen += ENC_BLOCK_LEN*3;

	DWORD dwRetlen = dwDatalen;
	LPBYTE lpEncData= (LPBYTE)malloc(dwHeaderBlockLen);
	if(lpEncData != NULL)
	{
		ZeroMemory(lpEncData, dwDatalen);
		CopyMemory(lpEncData, (BYTE *)&header, sizeof(header));
		DBGLOG(L"header enc size = %d->%d", dwDatalen, dwHeaderBlockLen);

//		if( AES256_Enc2(KEY_ENC_KEY, strlen(KEY_ENC_KEY), (BYTE *)lpEncData, dwHeaderBlockLen, dwRetlen, &dwErrorCode, &dwErrorPos) == FALSE)
		if( AES256_Enc(KEY_ENC_KEY, (BYTE *)lpEncData, dwHeaderBlockLen, dwRetlen, &dwErrorCode, &dwErrorPos) == FALSE)
		{
			free(lpEncData);
 			DBGLOG(_T("Aes256 header enc Err (ErrorCode: %d, ErrorPos: %d)"), dwErrorCode, dwErrorPos);
 			bResult = FALSE;
 			if(pRetCode)
 				*pRetCode = dwErrorCode;
	 
 			if(pErrPos)
 				*pErrPos = dwErrorPos;
	 
 			goto Exit_Enc;
		}
		DBGLOG(_T("Aes256 enc succeed! %s[%d->%d]"), header.FileFullPathName, dwDatalen, dwRetlen);


		DEFAULTENCHEADER hDHeader;
		ZeroMemory(&hDHeader, sizeof(hDHeader));
		CopyMemory(&hDHeader.caption, ITCMS_SIG, strlen(ITCMS_SIG));
		hDHeader.headersize = dwRetlen;
		hDHeader.headeroffset = sizeof(hDHeader);
		hDHeader.reserved[0] = m_Reserve[0];
		hDHeader.reserved[1] = m_Reserve[1];

		// yjLee(2015-03-12) : Base header에 사이트키 암호화의 구분을 위한 정보 추가
		if (_isSiteEncrypt)
			hDHeader.encKeyType = ENC_KEY_TYPE_UUID;
		else
			hDHeader.encKeyType = ENC_KEY_TYPE_USERID;
	
		//헤더파일 기록
		BOOL bWrittenSame = FALSE;
		BOOL bWriteSuccess = WriteFile(hWrite, &hDHeader, sizeof(hDHeader), &dwWriteSize, NULL );
		if (dwWriteSize == sizeof(hDHeader)) {
			bWrittenSame = TRUE;
		} else {
			//ezLog(_T("[OfsCrypt] Error: dwWriteSize != sizeof(hDHeader) [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);
		}
		if (!bWriteSuccess || !bWrittenSame) {
			free(lpEncData);
			bResult = FALSE;
			if(pRetCode)
				*pRetCode = GetLastError();

			if(pErrPos)
				*pErrPos = -20;

			DBGLOG(_T("[default aes fail]---1"));
			goto Exit_Enc;
		} 
		llEncSize += dwWriteSize;
		//DBGLOG(_T("base [%d->%d]=%d"), sizeof(hDHeader), dwWriteSize, llEncSize);
		DBGLOG(_T("base [%d->%lu]=%I64d [line: %d, function: %s file: %s]\n"), sizeof(hDHeader), dwWriteSize, llEncSize, __LINE__, __FUNCTIONW__, __FILEW__);

		//memcpy(&header.pseudoKey, pseudoKey, dwDatalen);
		//CopyMemory(&header, lpEncData, dwRetlen);

		//헤더파일 기록
		dwWriteSize = 0;
		bWrittenSame = FALSE;
		bWriteSuccess = WriteFile(hWrite, lpEncData, dwRetlen, &dwWriteSize, NULL );
		if(dwWriteSize == dwRetlen) {
			bWrittenSame = TRUE;
		} else {
			//ezLog(_T("[OfsCrypt] [Error] dwWriteSize: %lu != dwRetlen: %lu [line: %d, function: %s, file: %s]\r\n"), dwWriteSize, dwRetlen, __LINE__, __FUNCTIONW__, __FILEW__);
		}
		if(!bWriteSuccess || !bWrittenSame) {
			free(lpEncData);
			bResult = FALSE;
			if(pRetCode)
				*pRetCode = GetLastError();

			if(pErrPos)
				*pErrPos = -21;

			DBGLOG(_T("[header aes fail]---1"));
			goto Exit_Enc;
		}
		free(lpEncData);
		llEncSize += dwWriteSize;
		DBGLOG(_T("enc [%lu->%lu]=%I64d [line: %d, function: %s, file: %s]"), dwRetlen, dwWriteSize, llEncSize, __LINE__, __FUNCTIONW__, __FILEW__);
	}

	dwEncBufferSize = dwTotalFileSize;		//32바이트 가량 더 여유를 잡아 준다.
	DWORD dwAllocBlock = ENC_BLOCK_LEN * (dwEncBufferSize/ENC_BLOCK_LEN);
	dwAllocBlock += (ENC_BLOCK_LEN*3);
	DBGLOG(L"dwTotalFileSize = %d, dwAllocBlock = %d", dwTotalFileSize, dwAllocBlock);
	pbBuffer = (BYTE *)malloc(dwAllocBlock);//dwEncBufferSize);
	if(!pbBuffer)
	{
 		if(pRetCode)
 			*pRetCode = GetLastError();
 
 		if(pErrPos)
 			*pErrPos = -7;
	
		bResult = FALSE;
		goto Exit_Enc;
	}

	ZeroMemory(pbBuffer, dwAllocBlock);
	if(!ReadFile(	hRead, pbBuffer, dwAllocBlock, &dwCount, NULL))
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -8;

		bResult = FALSE;
		
		goto Exit_Enc;
	}

	DWORD dwNorLen = dwCount;
	DBGLOG(L"dwAllocBlock = %d, dwCount = %d", dwAllocBlock, dwCount);
	oneTimeKey[15] = '\0';
	//if( FALSE ==  AES256_Enc(strOneTimekey.GetBuffer(0), pbBuffer, dwAllocBlock, dwCount, &dwErrorCode, &dwErrorPos) )
	if( FALSE ==  AES256_Enc2(oneTimeKey, FILE_ENC_KEY_LEN, pbBuffer, dwAllocBlock, dwCount, &dwErrorCode, &dwErrorPos) )
	{
		if(pRetCode)
			*pRetCode = dwErrorCode;

		if(pErrPos)
			*pErrPos = dwErrorPos;
		
		bResult = FALSE;
		goto Exit_Enc;
	}

	if(dwNorLen > dwCount)
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = 1554;

		DBGLOG(L"Enc file Len > enc len %d : %d", dwNorLen, dwCount);
		bResult = FALSE;
		goto Exit_Enc;

	}
	if((dwCount % ENC_BLOCK_LEN) != 0)
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = 1550;

		DBGLOG(L"Enc Block Length not dwCount / ENC_BLOCK_LEN = %d [line: %d, function: %s, file: %s]", dwCount, __LINE__, __FUNCTIONW__, __FILEW__);

		//ezLog(_T("[OfsCrypt] [Error] NOT MULTIPLE ENC_BLOCK_LEN dwCount = %lu [line: %d, function: %s, file: %s]\r\n"), dwCount, __LINE__, __FUNCTIONW__, __FILEW__);
		bResult = FALSE;
		goto Exit_Enc;

	}
	// Kevin(2014-3-4)
	// 한번에 암호화데이터를 쓰지못한 경우 나머지 데이터를 쓰도록한다.
	DWORD dwWriteCnt = 0;
	DWORD dwRemainder = dwCount;
	DBGLOG(L"Enc dwCount = %d", dwCount);

	DWORD dwPos = 0;
	do {
		dwWriteCnt = 0;
		if(!WriteFile(hWrite, pbBuffer+dwPos, dwRemainder, &dwWriteCnt, NULL))
		{
			if(pRetCode)
				*pRetCode = GetLastError();

			if(pErrPos)
				*pErrPos = -10;

			bResult = FALSE;
			DBGLOG(_T("[aes fail]---5"));
			goto Exit_Enc;
		}
		llEncSize += dwWriteCnt;
		dwRemainder -= dwWriteCnt;
		dwPos += dwWriteCnt;
		
		if (dwRemainder > 0) {
			//ezLog(_T("[OfsCrypt] dwRemainder > 0 dwRemainder: %d [line: %d, function: %s file: %s]\r\n"), dwRemainder, __LINE__, __FUNCTIONW__, __FILEW__);
		}
	}while(dwRemainder > 0);

	//DBGLOG(_T("data [%d->%d]=%d"), dwRetlen, dwWriteSize, nEncSize);

	DBGLOG(_T("data [%lu>%lu]=%I64d [line: %d, function: %s file: %s]"), dwRetlen, dwWriteSize, llEncSize, __LINE__, __FUNCTIONW__, __FILEW__);
	DBGLOG(L"ENCFILE WRITE %d, written = %d", dwAllocBlock, dwWriteCnt);
	
Exit_Enc:

	if (hRead) {
		CloseHandle(hRead);
		hRead = NULL;
	}

	if (hWrite) {
		CloseHandle(hWrite);
		hWrite = NULL;
	}

	//HANDLE hEncFile = CreateFile(_strEncFilePath.GetBuffer(),
	//								GENERIC_READ | GENERIC_WRITE,
	//								FILE_SHARE_READ | FILE_SHARE_WRITE,
	//								0,
	//								CREATE_ALWAYS,
	//								NULL,
	//								0);

	//if( hEncFile != INVALID_HANDLE_VALUE && hEncFile != NULL )
	//{
	//	LARGE_INTEGER ulEncLen;
	//	ulEncLen.QuadPart = 0;
	//	ulEncLen.LowPart = GetFileSize(hEncFile, (LPDWORD)&ulEncLen.HighPart);
	//DBGLOG(L"enc len = %d", nEncSize);
	//if((nEncSize % ENC_BLOCK_LEN) != 0)
	//{
	//	bResult = FALSE;
	//	if(pRetCode)
	//		*pRetCode = 0;
	//
	//	if(pErrPos)
	//		*pErrPos = 2000;
	//}
	//	CloseHandle(hEncFile);
	//}
	
	if(pbBuffer) 
	{
		free(pbBuffer); 
	}

	//실패시 빈 암호화 파일은 삭제
	if(FALSE == bResult )
	{
		DeleteFile(_strEncFilePath);
	}
	DBGLOG(L"bResult = %s", (bResult) ? L"TRUE" : L"FALSE");
	return bResult;
}

BOOL CFileEncrypt::CreateKey(CString _strUserID,CString& _strKey, CString _strHash)
{
	CString strBuffer = _T(""), strKey  = _T(""), strResult = _T("");
	UCHAR szSha1[MAX_PATH] = {0,};
	strKey = _strUserID  + _strHash;
	char szAnsiBuff[MAX_PATH] = {0,};	
	memset(szAnsiBuff, 0x00, sizeof(szAnsiBuff));
	UnicodeToAnsi(szAnsiBuff, strKey, MAX_PATH);
	CBase64 base;
	CmsHashBin(szSha1, (UCHAR*)szAnsiBuff, strlen(szAnsiBuff), HASH_ALGID_SHA256);
	strResult.Format(_T("%s"), base.base64encode(szSha1, 20));
	DBGLOG(L"CreateKey() = %s\n", strResult.GetBuffer());//, (LPCSTR)strResult.GetBuffer());
	//DBGLOG(L"\n");
	//DBGLOG((LPCSTR)szSha1);
	//DBGLOG(L"\n");
	 
	_strKey = strResult;
	return TRUE;
}

CString CFileEncrypt::CreateFileHash(CString _strFilePath)
{
	UCHAR szSha1[MAX_PATH] = {0,};
	CString strHash = _T("");
	DWORD dwFileLength =0, cbRead;
	HANDLE hRead = NULL;
	DBGLOG(_T("CFileEncrypt::CreateFileHash (%s)"), _strFilePath.GetBuffer());

	if(_strFilePath.GetLength() == 0)
		return strHash;

	// file not exist
	if(IsFileExist2(_strFilePath.GetBuffer())  == FALSE)
	{
		DBGLOG(L"CreateFileHash() not exist file");
		return strHash;
	}

	char* pszFileName = NULL;
	pszFileName = UniToAnsi(_strFilePath.GetBuffer(0));

	hRead = CreateFileA(pszFileName,
								GENERIC_READ,// | GENERIC_WRITE,
								FILE_SHARE_READ,// | FILE_SHARE_WRITE,
								0,
								OPEN_EXISTING,
								NULL,
								0);
	free(pszFileName);// Kevin(2013-8-1) 추가

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DBGLOG(_T("%s read error (%d)"), _strFilePath.GetBuffer(), GetLastError());
		return strHash;
	}

	dwFileLength = ::GetFileSize(hRead, NULL);

	char* pszBuffer = new char[dwFileLength+1];
	ZeroMemory(pszBuffer,dwFileLength+1);

	if( ReadFile(hRead, pszBuffer, dwFileLength, &cbRead, NULL )  == FALSE )
	{
		CloseHandle(hRead);
		delete [] pszBuffer;
		return strHash;
	}
	CloseHandle(hRead);

	//memcpy(pszBuffer2, pszBuffer, dwFileLength);

	CmsHashBin(szSha1, (UCHAR*)pszBuffer, /*strlen(pszBuffer)*/cbRead, HASH_ALGID_SHA256);
	CBase64 base;
	strHash.Format(_T("%s"), base.base64encode(szSha1, 20));
	if( pszBuffer != NULL )
	{
		delete [] pszBuffer;
		pszBuffer = NULL;
	}

	DBGLOG(strHash.GetBuffer());

	return strHash;
}



BOOL CFileEncrypt::DecFile(CString _strEncFilePath, CString _strEncKey, DWORD* pRetCode, DWORD* pErrPos, BOOL _bChangeName, BOOL bOrgFileDel)
{
	PBYTE pbBuffer = NULL; 
	HANDLE hRead = NULL, hWrite = NULL;
	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	HCRYPTPROV hCryptProv = NULL; 

	DWORD dwFileLength, dwEncBufferSize ;
	BOOL bResult = TRUE;

	DWORD dwReadData=0;
	unsigned char oneTimeKey[FILE_ENC_KEY_LEN+1] ={0,};
	unsigned char pseudoKey[FILE_ENC_KEY_LEN+1] = {0};
	DWORD dwHeaderRead = 0;
	PEncrypt_Header pencrypt = NULL;

	DBGLOG(L"DecFile %s", _strEncFilePath.GetBuffer());

	if(_strEncFilePath.GetLength() == 0)
		return FALSE;

	if(IsFileExist2(_strEncFilePath.GetBuffer())  == FALSE)
		return FALSE;

	// Kevin(2013-5-13)
	CString strDecFilePath = L"";
	BOOL bRename = FALSE;
	if(m_strDecPathName.GetLength() > 0)
	{
		strDecFilePath = m_strDecPathName + ENCRYPT_EXTENTEION;
	}
	else
	{
		strDecFilePath = _strEncFilePath + ENCRYPT_EXTENTEION;
		bRename = TRUE;
	}

	hRead = CreateFile(_strEncFilePath.GetBuffer(), 
								GENERIC_READ, // | GENERIC_WRITE,
								FILE_SHARE_READ,// | FILE_SHARE_WRITE,
								0,
								OPEN_EXISTING,
								NULL,
								0);
	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		m_strDecPathName = L"";// Kevin(2013-5-13) 값을 초기화
		return FALSE;
	}

	dwFileLength = ::GetFileSize(hRead, NULL);
	if( dwFileLength <= 0)
	{
		CloseHandle(hRead);

		if(pRetCode)
			*pRetCode = 0;
		if(pErrPos)
			*pErrPos = -2;
		bResult = FALSE;
		goto Exit_Dec;
	}

	DWORD dwPlainLen = 0;
	DWORD dwKeyLen = 0;
	// Kevin(2013-6-4)
	DEFAULTENCHEADER dheader;
	ZeroMemory(&dheader, sizeof(dheader));
	if( ReadFile(hRead, &dheader, sizeof(dheader), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
		return FALSE;
	}

	if(!		(	strcmp(dheader.caption, ITCMS_SIG) == 0
			&&	( (dheader.reserved[0] == 'o' && dheader.reserved[1] == 'f')
				||(dheader.reserved[0] == 'e' && dheader.reserved[1] == 'g')
				||(dheader.reserved[0] == 's' && dheader.reserved[1] == 'f') ))
	)
	{
		DBGLOG(L"DecFile() unkonw file format!!");
		CloseHandle(hRead);
		CloseHandle(hWrite);

		*pRetCode = 100;
		*pErrPos = 100;
		return FALSE;
	}

	LPBYTE lpEncHeader = (LPBYTE)malloc(dheader.headersize);
	if(lpEncHeader != NULL)
	{
		ZeroMemory(lpEncHeader, dheader.headersize); // Kevin(2013-12-2)
		DWORD dwHeaderRead = dheader.headersize;
		if( ReadFile(hRead, lpEncHeader, dheader.headersize, &dwHeaderRead, NULL ) == FALSE )
		{
			free(lpEncHeader);

			if(pRetCode)
				*pRetCode = GetLastError();
			if(pErrPos)
				*pErrPos = -3;
			bResult =  FALSE;
			goto Exit_Dec;
		}
		pencrypt = (PEncrypt_Header)lpEncHeader;
		DWORD dwRetLen = dheader.headersize;
		DWORD dwErrorCode=0, dwErrorPos=0;
		if( FALSE == AES256_Dec(KEY_ENC_KEY,  (BYTE *)lpEncHeader, dheader.headersize,  dwRetLen, &dwErrorCode, &dwErrorPos))
		{
			if(dwErrorCode)
				DBGLOG(L"error Code  = %d", dwErrorCode);

			if(dwErrorPos)
				DBGLOG(L"error Code  = %d", dwErrorPos);
			free(lpEncHeader);
			bResult =  FALSE;
			goto Exit_Dec;
		}
		if(memcmp(pencrypt->sig, ITCMS_SIG2, sizeof(ITCMS_SIG2)) != 0)
		{
			DBGLOGA("DecFile() unkonw enc heade %s %sr!!", pencrypt->sig, ITCMS_SIG2);
			*pRetCode = 101;
			*pErrPos = 101;
			free(lpEncHeader);
			bResult =  FALSE;
			goto Exit_Dec;
		}
		//DBGLOG(L"header = %s", pencrypt->FileFullPathName);
		//DBGLOG(L"header = %s", pencrypt->FileExt);
		//DBGLOG(L"header = %s", pencrypt->FileHash);
		//DBGLOG(L"header = %s", pencrypt->UserID);
		dwPlainLen = pencrypt->length;
		dwKeyLen =pencrypt->dwKeyLen;
		//DBGLOG(L"header = %s", pencrypt->SiteIndex);
		//DBGLOG(L"header.pseudoKey OFFSET = %u", ((DWORD_PTR)pencrypt->pseudoKey - (DWORD_PTR)(pencrypt)));
		memcpy(pseudoKey, pencrypt->pseudoKey, pencrypt->dwKeyLen);
#ifdef _FILE_KEY_DISPLAY
		{
			BYTE Tmp[MAX_PATH] = {0};
			CHAR strSub[10] = {0};
			for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
			{
				ZeroMemory(strSub, 10);
				StringCchPrintfA(strSub, 10, "%02x", pseudoKey[i]);
				StringCchCatA((CHAR *)Tmp, MAX_PATH, strSub); 
			}
			DBGLOGA("enc sudokey = [%s]", Tmp);
		}
#endif
		DWORD dwEncLen = pencrypt->dwKeyLen;//FILE_ENC_KEY_LEN;		//-1을 하는 이유는 여유분으로 1바이트를 추가해놓았기 때문 
		DBGLOG(L"dwKeyLen = %d, dwEncLen = %d", pencrypt->dwKeyLen,  dwEncLen);
		if( FALSE == AES256_Dec(_strEncKey.GetBuffer(0), pseudoKey, pencrypt->dwKeyLen,  dwEncLen, &dwErrorCode, &dwErrorPos))
		{
			if(pRetCode)
				*pRetCode = dwErrorCode;
			if(pErrPos)
				*pErrPos = dwErrorPos;
			DBGLOG(L"dec error Code  = %d, dwErrorPos = %d", dwErrorCode, dwErrorPos);
			free(lpEncHeader);

			bResult = FALSE;
			goto Exit_Dec;
		}
#ifdef  _FILE_KEY_DISPLAY
		{
			BYTE Tmp[MAX_PATH] = {0};
			CHAR strSub[10] = {0};
			for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
			{
				ZeroMemory(strSub, 10);
				StringCchPrintfA(strSub, 10, "%02x", pseudoKey[i]);
				StringCchCatA((CHAR *)Tmp, MAX_PATH, strSub); 
			}
			DBGLOGA("sudokey = [%s]", Tmp);
		}
#endif
		free(lpEncHeader);
		memcpy(oneTimeKey, pseudoKey, FILE_ENC_KEY_LEN);
	}
	//복호화 시작
	dwEncBufferSize = dwFileLength - (dheader.headersize + sizeof(DEFAULTENCHEADER));		//헤더길이 만큼 제거
	//dwEncBufferSize + (ENC_BLOCK_LEN * 2);	// <- 2015-01-06 kh.choi 요 부분 이상 
	dwEncBufferSize += (ENC_BLOCK_LEN * 2);	// <- 2015-01-06 kh.choi 이걸 의도한게 아닐런지...
	//dwEncBufferSize = dwFileLength - dheader.headersize ;
	DBGLOG(L"decfile dwEncBufferSize = %d", dwEncBufferSize);
	pbBuffer = (BYTE *)malloc(dwEncBufferSize);
	if(pbBuffer == NULL) // Kevin(2014-3-3)
	{
		*pRetCode = GetLastError();
		*pErrPos = -500;
		DBGLOG(L"ALLOC DATA BUF error Code  = %d", *pRetCode, *pErrPos);

		bResult = FALSE;
		goto Exit_Dec;
	}
	memset(pbBuffer, 0, dwEncBufferSize);

	dwReadData = 0;
	if( ReadFile(hRead, pbBuffer, dwEncBufferSize, &dwReadData, NULL ) == FALSE )
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -7;

		DBGLOG(L"readfile error Code  = %d, dwErrorPos = %d", pRetCode, pErrPos);
		bResult = FALSE;
		goto Exit_Dec;
	}
	DBGLOG(L"dwReadData = %d", dwReadData);

	DWORD dwErrorCode, dwErrorPos;
	dwErrorCode = dwErrorPos = 0;
	//파일 복호화
	oneTimeKey[15] = '\0';
	if( FALSE == AES256_Dec2(oneTimeKey, FILE_ENC_KEY_LEN, pbBuffer , dwEncBufferSize,  dwReadData, &dwErrorCode, &dwErrorPos))
	{
		if(pRetCode)
			*pRetCode = dwErrorCode;

		if(pErrPos)
			*pErrPos = dwErrorPos;
		DBGLOG(L"dec2 error Code  = %d, dwErrorPos = %d", dwErrorCode, pErrPos);
		bResult = FALSE;
		goto Exit_Dec;
	}

	DBGLOG(L"dwReadData writ size= %d", dwReadData);
	hWrite = CreateFile(strDecFilePath.GetBuffer(), //_strEncFilePath + ENCRYPT_EXTENTEION,
								GENERIC_READ | GENERIC_WRITE,
								FILE_SHARE_READ | FILE_SHARE_WRITE,
								0,
								CREATE_ALWAYS,
								NULL,
								0);
	if( hWrite == INVALID_HANDLE_VALUE || hWrite == NULL )
	{
		CloseHandle(hRead);

		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -1;

		m_strDecPathName = L"";// Kevin(2013-5-13) 값을 초기화
		return FALSE;
	}

	//복호화된 내용 파일 write
	DWORD dwWritelen = 0;
	BOOL bWrittenSame = FALSE;
	BOOL bWriteSuccess = WriteFile(hWrite, pbBuffer, dwPlainLen/*dwEncBufferSize*/, &dwWritelen, NULL );
	if (dwWritelen == dwPlainLen) {
		bWrittenSame = TRUE;
	} else {
		//ezLog(_T("[OfsCrypt] Error: dwWritelen != dwPlainLen [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);
	}
	if (!bWriteSuccess || !bWrittenSame) {
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -9;

		DBGLOG(_T("error Code  = %d, dwErrorPos = %d [line: %d, function: %s, file: %s]"), dwErrorCode, dwErrorPos, __LINE__, __FUNCTIONW__, __FILEW__);
		bResult = FALSE;
	} 
	DBGLOG(L"dwPlain file size = %d dwWritelen = %d", dwPlainLen, dwWritelen);
Exit_Dec:

	if(pbBuffer) 
	{
		free(pbBuffer); 
	}

	if( !bResult )
	{
		if(hRead)
			CloseHandle(hRead);
		if(hWrite)
			CloseHandle(hWrite);

		DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);

		m_strDecPathName = L"";// Kevin(2013-5-13) 값을 초기화
		return bResult;
	}
	if(hRead)
		CloseHandle(hRead);
	if(hWrite)
		CloseHandle(hWrite);
	//_bChangeName 플래그가 필요한 이유는 암호화된 파일을 사용자가 더블클릭하여 열때  
	// CmsExcelInDecrypt.exe와 같은 프로그램들과 호환을 위해 파일복호화만 하고 파일명을 변경하지 않는다.(CmsExcelInDecrypt에서 추후 처리됨)
	if( FALSE == _bChangeName)
	{
		m_strDecPathName = L""; // Kevin(2013-5-13) 값을 초기화
		return TRUE;
	}
	CString strDecFile = L""; // 최종 복호화된 파일명 기록// Kevin(2014-4-21)
	CString strFilePath = _T(""), strMsg = _T("");
	strFilePath =GetFileFullPath(_strEncFilePath);
	strDecFile = strFilePath; // Kevin(2014-4-21) DEC FILE NAME기록

	try
	{
		if(bRename == TRUE) // Kevin(2013-5-13)
		{
			if( IsFileExist2(strFilePath.GetBuffer() ) )	//복원할 파일명이 이미 존재한다면
			{
				int i = 0;
				while(TRUE)
				{
					if(i > 300)
					{
						//복호화 실패로 간주한다.
						*pErrPos = -10;
						return FALSE;
						//break;
					}
					CString strOriPath = _T("");
					int iFind = strFilePath.ReverseFind('.');
					strOriPath.Format(_T("%s(n%d)%s"),  strFilePath.Left(iFind), i, ExtractFileExt(strFilePath) );
					strDecFile = strOriPath;// Kevin(2014-4-21)
					if( IsFileExist2(strOriPath.GetBuffer()) )		//새로운 복원 파일명 또한 존재한다면
					{
						if( DeleteFile(strOriPath) )	//삭제 해서 성공시
						{
							CFile::Rename(strDecFilePath, strOriPath); // hhh(2013-7-16)
							break;
						}
					}
					else
					{
						CFile::Rename(strDecFilePath, strOriPath); // hhh(2013-7-16)
						break;
					}
					i++;
				}
			}
			else
			{
				CFile::Rename(strDecFilePath/*_strEncFilePath + ENCRYPT_EXTENTEION*/, strFilePath); // Kevin(2013-5-13)
				DBGOUT(_T("strFilePath %s"), strFilePath.GetBuffer());
			}
		}
	}
	catch(CFileException* pEx )
	{
		/*
		int i = 0;
		while(TRUE)
		{
			CString strOriPath = _T("");
			int iFind = strFilePath.ReverseFind('.');
			strOriPath.Format(_T("%s(e%d)%s"),  strFilePath.Left(iFind), ExtractFileExt(strFilePath) );
		}

		
		if(bRename == TRUE) // Kevin(2013-5-13)
		{
			CFile::Rename(strDecFilePath, strOriPath);
			DBGOUT(_T("strOriginal exist %s"), strOriPath.GetBuffer());
		}*/
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -11;

		pEx->Delete();
		return FALSE;
	}

	// Kevin(2013-5-13)
	// 원본 암호화 파일에 대한 삭제를 원할 경우 삭제
	// 지우지 않는 경우가 있음
	if(bOrgFileDel == TRUE)
	{
		if(strDecFile.GetLength()  != 0)
		{
			if(IsFileExist2(strDecFile.GetBuffer()) == TRUE)
			{
				DeleteFile(strDecFilePath.GetBuffer()/*_strEncFilePath + ENCRYPT_EXTENTEION*/);
				DeleteFile(_strEncFilePath.GetBuffer());
			}
			else
			{
				CFile::Rename(strDecFilePath, strDecFile);
				DeleteFile(strDecFilePath.GetBuffer());
				if(IsFileExist2(strDecFile.GetBuffer()) == TRUE)
				{
					DeleteFile(_strEncFilePath.GetBuffer());
				}
			}
		}
	}

	m_strDecPathName = L""; // Kevin(2013-5-13) 값을 초기화
	return TRUE;
}

/**        
 @brief     지정한 경로에 파일을 복호화 한다. 원본 파일은 그대로
 @author    sy.choi
 @date      2016.11.24
 @param     _strEncFilePath [in] 복호화할 원본 파일경
 @param     _strDecPath     [in] 복호화 할 파일경로
 @return    BOOL
*/
BOOL CFileEncrypt::DecFile2(CString _strEncFilePath, CString _strDecPath, CString _strEncKey, DWORD* pRetCode, DWORD* pErrPos, BOOL _bChangeName, BOOL bOrgFileDel)
{
	PBYTE pbBuffer = NULL; 
	HANDLE hRead = NULL, hWrite = NULL;
	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	HCRYPTPROV hCryptProv = NULL; 

	DWORD dwFileLength, dwEncBufferSize ;
	BOOL bResult = TRUE;

	DWORD dwReadData=0;
	unsigned char oneTimeKey[FILE_ENC_KEY_LEN+1] ={0,};
	unsigned char pseudoKey[FILE_ENC_KEY_LEN+1] = {0};
	DWORD dwHeaderRead = 0;
	PEncrypt_Header pencrypt = NULL;

	DBGLOG(L"DecFile %s", _strEncFilePath.GetBuffer());

	if(_strEncFilePath.GetLength() == 0)
		return FALSE;

	if(IsFileExist2(_strEncFilePath.GetBuffer())  == FALSE)
		return FALSE;

	// Kevin(2013-5-13)
	CString strDecFilePath = L"";
	strDecFilePath.Format(_T("%s\\%s%s"), _strDecPath, ExtractFileName(_strEncFilePath), ENCRYPT_EXTENTEION);

	hRead = CreateFile(_strEncFilePath.GetBuffer(), 
								GENERIC_READ, // | GENERIC_WRITE,
								FILE_SHARE_READ,// | FILE_SHARE_WRITE,
								0,
								OPEN_EXISTING,
								NULL,
								0);
	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		m_strDecPathName = L"";// Kevin(2013-5-13) 값을 초기화
		return FALSE;
	}

	dwFileLength = ::GetFileSize(hRead, NULL);
	if( dwFileLength <= 0)
	{
		CloseHandle(hRead);

		if(pRetCode)
			*pRetCode = 0;
		if(pErrPos)
			*pErrPos = -2;
		bResult = FALSE;
		//goto Exit_Dec;
		{
			if(pbBuffer) 
			{
				free(pbBuffer); 
			}
			DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
			if(hRead)
				CloseHandle(hRead);
			if(hWrite)
				CloseHandle(hWrite);
			return bResult;
		}
	}

	DWORD dwPlainLen = 0;
	DWORD dwKeyLen = 0;
	// Kevin(2013-6-4)
	DEFAULTENCHEADER dheader;
	ZeroMemory(&dheader, sizeof(dheader));
	if( ReadFile(hRead, &dheader, sizeof(dheader), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
		return FALSE;
	}

	if(!		(	strcmp(dheader.caption, ITCMS_SIG) == 0
			&&	( (dheader.reserved[0] == 'o' && dheader.reserved[1] == 'f')
				||(dheader.reserved[0] == 'e' && dheader.reserved[1] == 'g')
				||(dheader.reserved[0] == 's' && dheader.reserved[1] == 'f') ))
	)
	{
		DBGLOG(L"DecFile() unkonw file format!!");
		CloseHandle(hRead);
		CloseHandle(hWrite);

		*pRetCode = 100;
		*pErrPos = 100;
		return FALSE;
	}

	LPBYTE lpEncHeader = (LPBYTE)malloc(dheader.headersize);
	if(lpEncHeader != NULL)
	{
		ZeroMemory(lpEncHeader, dheader.headersize); // Kevin(2013-12-2)
		DWORD dwHeaderRead = dheader.headersize;
		if( ReadFile(hRead, lpEncHeader, dheader.headersize, &dwHeaderRead, NULL ) == FALSE )
		{
			free(lpEncHeader);

			if(pRetCode)
				*pRetCode = GetLastError();
			if(pErrPos)
				*pErrPos = -3;
			bResult =  FALSE;
			{
				if(pbBuffer) 
				{
					free(pbBuffer); 
				}
				DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
				if(hRead)
					CloseHandle(hRead);
				if(hWrite)
					CloseHandle(hWrite);
				return bResult;
			}
		}
		pencrypt = (PEncrypt_Header)lpEncHeader;
		DWORD dwRetLen = dheader.headersize;
		DWORD dwErrorCode=0, dwErrorPos=0;
		if( FALSE == AES256_Dec(KEY_ENC_KEY,  (BYTE *)lpEncHeader, dheader.headersize,  dwRetLen, &dwErrorCode, &dwErrorPos))
		{
			if(dwErrorCode)
				DBGLOG(L"error Code  = %d", dwErrorCode);

			if(dwErrorPos)
				DBGLOG(L"error Code  = %d", dwErrorPos);
			free(lpEncHeader);
			bResult =  FALSE;
			{
				if(pbBuffer) 
				{
					free(pbBuffer); 
				}
				DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
				if(hRead)
					CloseHandle(hRead);
				if(hWrite)
					CloseHandle(hWrite);
				return bResult;
			}
		}
		if(memcmp(pencrypt->sig, ITCMS_SIG2, sizeof(ITCMS_SIG2)) != 0)
		{
			DBGLOGA("DecFile() unkonw enc heade %s %sr!!", pencrypt->sig, ITCMS_SIG2);
			*pRetCode = 101;
			*pErrPos = 101;
			free(lpEncHeader);
			bResult =  FALSE;
			{
				if(pbBuffer) 
				{
					free(pbBuffer); 
				}
				DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
				if(hRead)
					CloseHandle(hRead);
				if(hWrite)
					CloseHandle(hWrite);
				return bResult;
			}
		}
		dwPlainLen = pencrypt->length;
		dwKeyLen =pencrypt->dwKeyLen;
		memcpy(pseudoKey, pencrypt->pseudoKey, pencrypt->dwKeyLen);
#ifdef _FILE_KEY_DISPLAY
		{
			BYTE Tmp[MAX_PATH] = {0};
			CHAR strSub[10] = {0};
			for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
			{
				ZeroMemory(strSub, 10);
				StringCchPrintfA(strSub, 10, "%02x", pseudoKey[i]);
				StringCchCatA((CHAR *)Tmp, MAX_PATH, strSub); 
			}
			DBGLOGA("enc sudokey = [%s]", Tmp);
		}
#endif
		DWORD dwEncLen = pencrypt->dwKeyLen;//FILE_ENC_KEY_LEN;		//-1을 하는 이유는 여유분으로 1바이트를 추가해놓았기 때문 
		DBGLOG(L"dwKeyLen = %d, dwEncLen = %d", pencrypt->dwKeyLen,  dwEncLen);
		if( FALSE == AES256_Dec(_strEncKey.GetBuffer(0), pseudoKey, pencrypt->dwKeyLen,  dwEncLen, &dwErrorCode, &dwErrorPos))
		{
			if(pRetCode)
				*pRetCode = dwErrorCode;
			if(pErrPos)
				*pErrPos = dwErrorPos;
			DBGLOG(L"dec error Code  = %d, dwErrorPos = %d", dwErrorCode, dwErrorPos);
			free(lpEncHeader);

			bResult = FALSE;
			{
				if(pbBuffer) 
				{
					free(pbBuffer); 
				}
				DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
				if(hRead)
					CloseHandle(hRead);
				if(hWrite)
					CloseHandle(hWrite);
				return bResult;
			}
		}
#ifdef  _FILE_KEY_DISPLAY
		{
			BYTE Tmp[MAX_PATH] = {0};
			CHAR strSub[10] = {0};
			for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
			{
				ZeroMemory(strSub, 10);
				StringCchPrintfA(strSub, 10, "%02x", pseudoKey[i]);
				StringCchCatA((CHAR *)Tmp, MAX_PATH, strSub); 
			}
			DBGLOGA("sudokey = [%s]", Tmp);
		}
#endif
		free(lpEncHeader);
		memcpy(oneTimeKey, pseudoKey, FILE_ENC_KEY_LEN);
	}
	//복호화 시작
	dwEncBufferSize = dwFileLength - (dheader.headersize + sizeof(DEFAULTENCHEADER));		//헤더길이 만큼 제거
	//dwEncBufferSize + (ENC_BLOCK_LEN * 2);	// <- 2015-01-06 kh.choi 요 부분 이상 
	dwEncBufferSize += (ENC_BLOCK_LEN * 2);	// <- 2015-01-06 kh.choi 이걸 의도한게 아닐런지...
	//dwEncBufferSize = dwFileLength - dheader.headersize ;
	DBGLOG(L"decfile dwEncBufferSize = %d", dwEncBufferSize);
	pbBuffer = (BYTE *)malloc(dwEncBufferSize);
	if(pbBuffer == NULL) // Kevin(2014-3-3)
	{
		*pRetCode = GetLastError();
		*pErrPos = -500;
		DBGLOG(L"ALLOC DATA BUF error Code  = %d", *pRetCode, *pErrPos);

		bResult = FALSE;
		{
			if(pbBuffer) 
			{
				free(pbBuffer); 
			}
			DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
			if(hRead)
				CloseHandle(hRead);
			if(hWrite)
				CloseHandle(hWrite);
			return bResult;
		}
	}
	memset(pbBuffer, 0, dwEncBufferSize);

	dwReadData = 0;
	if( ReadFile(hRead, pbBuffer, dwEncBufferSize, &dwReadData, NULL ) == FALSE )
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -7;

		DBGLOG(L"readfile error Code  = %d, dwErrorPos = %d", pRetCode, pErrPos);
		bResult = FALSE;
		{
			if(pbBuffer) 
			{
				free(pbBuffer); 
			}
			DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
			if(hRead)
				CloseHandle(hRead);
			if(hWrite)
				CloseHandle(hWrite);
			return bResult;
		}
	}
	DBGLOG(L"dwReadData = %d", dwReadData);

	DWORD dwErrorCode, dwErrorPos;
	dwErrorCode = dwErrorPos = 0;
	//파일 복호화
	oneTimeKey[15] = '\0';
	if( FALSE == AES256_Dec2(oneTimeKey, FILE_ENC_KEY_LEN, pbBuffer , dwEncBufferSize,  dwReadData, &dwErrorCode, &dwErrorPos))
	{
		if(pRetCode)
			*pRetCode = dwErrorCode;

		if(pErrPos)
			*pErrPos = dwErrorPos;
		DBGLOG(L"dec2 error Code  = %d, dwErrorPos = %d", dwErrorCode, pErrPos);
		bResult = FALSE;
		{
			if(pbBuffer) 
			{
				free(pbBuffer); 
			}
			DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);
			if(hRead)
				CloseHandle(hRead);
			if(hWrite)
				CloseHandle(hWrite);
			return bResult;
		}
	}

	DBGLOG(L"dwReadData writ size= %d", dwReadData);
	hWrite = CreateFile(strDecFilePath.GetBuffer(), //_strEncFilePath + ENCRYPT_EXTENTEION,
								GENERIC_READ | GENERIC_WRITE,
								FILE_SHARE_READ | FILE_SHARE_WRITE,
								0,
								CREATE_ALWAYS,
								NULL,
								0);
	if( hWrite == INVALID_HANDLE_VALUE || hWrite == NULL )
	{
		CloseHandle(hRead);

		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -1;

		m_strDecPathName = L"";// Kevin(2013-5-13) 값을 초기화
		return FALSE;
	}

	//복호화된 내용 파일 write
	DWORD dwWritelen = 0;
	BOOL bWrittenSame = FALSE;
	BOOL bWriteSuccess = WriteFile(hWrite, pbBuffer, dwPlainLen/*dwEncBufferSize*/, &dwWritelen, NULL );
	if (dwWritelen == dwPlainLen) {
		bWrittenSame = TRUE;
	} else {
		//ezLog(_T("[OfsCrypt] Error: dwWritelen != dwPlainLen [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);
	}
	if (!bWriteSuccess || !bWrittenSame) {
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -9;

		DBGLOG(_T("error Code  = %d, dwErrorPos = %d [line: %d, function: %s, file: %s]"), dwErrorCode, dwErrorPos, __LINE__, __FUNCTIONW__, __FILEW__);
		bResult = FALSE;
	} 
	DBGLOG(L"dwPlain file size = %d dwWritelen = %d", dwPlainLen, dwWritelen);
//Exit_Dec:
	{
		if(pbBuffer) 
		{
			free(pbBuffer); 
		}

		if( !bResult )
		{
			if(hRead)
				CloseHandle(hRead);
			if(hWrite)
				CloseHandle(hWrite);

			DeleteFile(strDecFilePath);// Kevin(2013-5-13)_strEncFilePath + ENCRYPT_EXTENTEION);

			m_strDecPathName = L"";// Kevin(2013-5-13) 값을 초기화
			return bResult;
		}
		if(hRead)
			CloseHandle(hRead);
		if(hWrite)
			CloseHandle(hWrite);
	}
	//_bChangeName 플래그가 필요한 이유는 암호화된 파일을 사용자가 더블클릭하여 열때  
	// CmsExcelInDecrypt.exe와 같은 프로그램들과 호환을 위해 파일복호화만 하고 파일명을 변경하지 않는다.(CmsExcelInDecrypt에서 추후 처리됨)
	if( FALSE == _bChangeName)
	{
		m_strDecPathName = L""; // Kevin(2013-5-13) 값을 초기화
		return TRUE;
	}
	CString strDecFile = L""; // 최종 복호화된 파일명 기록// Kevin(2014-4-21)
	CString strTemp = _T(""), strMsg = _T("");
	CString strFilePath = _T("");
	strTemp = GetFileFullPath(_strEncFilePath);
	strFilePath.Format(_T("%s\\%s"), _strDecPath, ExtractFileName(strTemp));
	strDecFile = strFilePath; // Kevin(2014-4-21) DEC FILE NAME기록

	try
	{
			if(IsFileExist2(strFilePath.GetBuffer()))	//복원할 파일명이 이미 존재한다면
			{
				int i = 0;
				while(TRUE)
				{
					if(i > 300)
					{
						//복호화 실패로 간주한다.
						*pErrPos = -10;
						return FALSE;
						//break;
					}
					CString strOriPath = _T("");
					int iFind = strFilePath.ReverseFind('.');
					strOriPath.Format(_T("%s(n%d)%s"),  strFilePath.Left(iFind), i, ExtractFileExt(strFilePath) );
					strDecFile = strOriPath;// Kevin(2014-4-21)
					if( IsFileExist2(strOriPath.GetBuffer()) )		//새로운 복원 파일명 또한 존재한다면
					{
						if( DeleteFile(strOriPath) )	//삭제 해서 성공시
						{
							CFile::Rename(strDecFilePath, strOriPath); // hhh(2013-7-16)
							break;
						}
					}
					else
					{
						CFile::Rename(strDecFilePath, strOriPath); // hhh(2013-7-16)
						break;
					}
					i++;
				}
			}
			else
			{

				CFile::Rename(strDecFilePath/*_strEncFilePath + ENCRYPT_EXTENTEION*/, strFilePath); // Kevin(2013-5-13)
				DBGOUT(_T("strFilePath %s"), strFilePath.GetBuffer());
			}
	}
	catch(CFileException* pEx )
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -11;

		pEx->Delete();
		return FALSE;
	}

	// Kevin(2013-5-13)
	// 원본 암호화 파일에 대한 삭제를 원할 경우 삭제
	// 지우지 않는 경우가 있음
	if(bOrgFileDel == TRUE)
	{
		if(strDecFile.GetLength() != 0)
		{
			if(IsFileExist2(strDecFile.GetBuffer()) == TRUE)
			{
				DeleteFile(strDecFilePath.GetBuffer()/*_strEncFilePath + ENCRYPT_EXTENTEION*/);
				//DeleteFile(_strEncFilePath.GetBuffer());
			}
			else
			{
				if(IsFileExist2(strDecFilePath.GetBuffer()) == TRUE && IsFileExist2(strDecFilePath.GetBuffer()) == FALSE)
					CFile::Rename(strDecFilePath, strDecFile);
				DeleteFile(strDecFilePath.GetBuffer());
				if(IsFileExist2(strDecFile.GetBuffer()) == TRUE)
				{
					//DeleteFile(_strEncFilePath.GetBuffer());
				}
			}
		}
	}

	m_strDecPathName = L""; // Kevin(2013-5-13) 값을 초기화
	return TRUE;
}

CString CFileEncrypt::GetFileHashFromHeader(CString _strEncFilePath)
{
	HANDLE hRead = NULL;
	DBGLOG(L"GetFileHashFromHeader(%s)", _strEncFilePath.GetBuffer());
	hRead = CreateFile(_strEncFilePath.GetBuffer(), 
								GENERIC_READ,
								FILE_SHARE_READ,
								0,
								OPEN_EXISTING,
								NULL,
								0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE = %d"), GetLastError());
		return FALSE;
	}

	DWORD dwFileLength = ::GetFileSize(hRead, NULL);
	if( dwFileLength <= 0)
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE3 = %d"), GetLastError());

		DeleteFile(_strEncFilePath + ENCRYPT_EXTENTEION);
		return FALSE;
	}
	
	// Kevin(2013-6-4)
	DWORD dwHeaderRead = 0;
	DEFAULTENCHEADER dheader;
	if( ReadFile(hRead, &dheader, sizeof(dheader), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2 = %d"), GetLastError());
		return FALSE;
	}

	CString strHash = _T("");
	LPBYTE lpEncHeader = (LPBYTE)malloc(dheader.headersize);
	if(lpEncHeader != NULL)
	{
		dwHeaderRead = dheader.headersize;
		if( ReadFile(hRead, lpEncHeader, dheader.headersize, &dwHeaderRead, NULL ) == FALSE )
		{
			free(lpEncHeader);
			CloseHandle(hRead);
			DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE4 = %d"), GetLastError());
			return FALSE;
		}

		PEncrypt_Header pdecHeader = (PEncrypt_Header)lpEncHeader;
		DWORD dwRetLen =dwHeaderRead;
		DWORD dwErrorCode=0, dwErrorPos=0;
		
		if( FALSE == AES256_Dec(KEY_ENC_KEY,  (BYTE *)pdecHeader, dheader.headersize,  dwRetLen, &dwErrorCode, &dwErrorPos))
		{
			if(dwErrorCode)
				DBGLOG(L"error Code  = %d", dwErrorCode);

			if(dwErrorPos)
				DBGLOG(L"error Code  = %d", dwErrorPos);
		}
		else
		{
			strHash = pdecHeader->FileHash;
			DBGLOG(L"header = %s", pdecHeader->FileFullPathName);
			DBGLOG(L"header = %s", pdecHeader->FileExt);
			DBGLOG(L"header = %s", pdecHeader->FileHash);
			DBGLOG(L"header = %s", pdecHeader->UserID);
		}
		free(lpEncHeader);
	}
	CloseHandle(hRead);
	return strHash;
}


BOOL CFileEncrypt::GetFileHeader(CString _strEncFilePath, OUT BYTE *pHeader, IN ULONG ulHeaderLen)
{
	HANDLE hRead = NULL;
	DBGLOG(L"GetFileHeader(%s)", _strEncFilePath.GetBuffer());
	if(pHeader == NULL)
	{
		DBGLOG(L"GetFileHeader pHeader == NULL");
		return FALSE;
	}

	hRead = CreateFile(_strEncFilePath.GetBuffer(), 
		GENERIC_READ,
		FILE_SHARE_READ,
		0,
		OPEN_EXISTING,
		NULL,
		0);
	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE"));
		return FALSE;
	}

	DWORD dwFileLength = ::GetFileSize(hRead, NULL);

	if( dwFileLength <= 0)
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE3"));
		DeleteFile(_strEncFilePath + ENCRYPT_EXTENTEION);
		return FALSE;
	}
	
	// Kevin(2013-6-4)
	DWORD dwHeaderRead = 0;
	DEFAULTENCHEADER dheader;
	if( ReadFile(hRead, &dheader, sizeof(dheader), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
		return FALSE;
	}

	if(!		(	strcmp(dheader.caption, ITCMS_SIG) == 0
			&&	( (dheader.reserved[0] == 'o' && dheader.reserved[1] == 'f')
				||(dheader.reserved[0] == 'e' && dheader.reserved[1] == 'g')
				||(dheader.reserved[0] == 's' && dheader.reserved[1] == 'f') ))
	)
	{
		CloseHandle(hRead);
		DBGLOG(L"getfileheader() unkonw file format!!");
		return FALSE;
	}

	CString strHash = _T("");
	LPBYTE lpEncHeader = (LPBYTE)malloc(dheader.headersize);
	if(lpEncHeader != NULL)
	{
		dwHeaderRead = dheader.headersize;
		if( ReadFile(hRead, lpEncHeader, dheader.headersize, &dwHeaderRead, NULL ) == FALSE )
		{
			free(lpEncHeader);
			CloseHandle(hRead);
			DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
			return FALSE;
		}

		PEncrypt_Header pdecHeader = (PEncrypt_Header)lpEncHeader;
		DWORD dwRetLen =dwHeaderRead;
		DWORD dwErrorCode=0, dwErrorPos=0;
		
		if( FALSE == AES256_Dec(KEY_ENC_KEY,  (BYTE *)pdecHeader, dheader.headersize,  dwRetLen, &dwErrorCode, &dwErrorPos))
		{
			if(dwErrorCode)
				DBGLOG(L"error Code  = %d", dwErrorCode);

			if(dwErrorPos)
				DBGLOG(L"error Code  = %d", dwErrorPos);
		}
		else
		{
			if(memcmp(pdecHeader->sig, ITCMS_SIG2, sizeof(ITCMS_SIG2)) != 0)
			{
				DBGLOGA("GetFileHeader() unkonw enc heade %s %sr!!", pdecHeader->sig, ITCMS_SIG2);
			}
			else
			{
				if(ulHeaderLen >= sizeof(Encrypt_Header))
				{
					memcpy(pHeader, pdecHeader, sizeof(Encrypt_Header));
					DBGLOG(L"header userid = %s", pdecHeader->UserID);
				}
				else
					memcpy(pHeader, pdecHeader, ulHeaderLen);
			}
		}
		free(lpEncHeader);
	}
	CloseHandle(hRead);
	return TRUE;
}

BOOL CFileEncrypt::GetEncFileHash(CString _strFilePath, CString& _strHash)
{
	HANDLE hRead = NULL;

	hRead = CreateFile(_strFilePath.GetBuffer(), 
								GENERIC_READ,// | GENERIC_WRITE,
								FILE_SHARE_READ,// | FILE_SHARE_WRITE,
								0,
								OPEN_EXISTING,
								NULL,
								0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DBGLOG(_T("%s read error (%d)"), _strFilePath.GetBuffer(), GetLastError());
		return FALSE;
	}

	DWORD dwFileLength = ::GetFileSize(hRead, NULL);
	if( dwFileLength <= 0)
	{
		CloseHandle(hRead);
		return FALSE;
	}

		// Kevin(2013-6-4)
	DWORD dwHeaderRead = 0;
	DEFAULTENCHEADER dheader;

	if( ReadFile(hRead, &dheader, sizeof(dheader), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
		return FALSE;
	}

	CString strHash = _T("");
	LPBYTE lpEncHeader = (LPBYTE)malloc(dheader.headersize);
	if(lpEncHeader != NULL)
	{
		dwHeaderRead = dheader.headersize;
		if( ReadFile(hRead, lpEncHeader, dheader.headersize, &dwHeaderRead, NULL ) == FALSE )
		{
			free(lpEncHeader);
			CloseHandle(hRead);
			DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
			return FALSE;
		}
	
		PEncrypt_Header pdecHeader = (PEncrypt_Header)lpEncHeader;
		DWORD dwRetLen = dheader.headersize;
		DWORD dwErrorCode=0, dwErrorPos=0;
		
		if( FALSE == AES256_Dec(KEY_ENC_KEY,  (BYTE *)pdecHeader, dheader.headersize,  dwRetLen, &dwErrorCode, &dwErrorPos))
		{
			if(dwErrorCode)
				DBGLOG(L"error Code  = %d", dwErrorCode);

			if(dwErrorPos)
				DBGLOG(L"error Code  = %d", dwErrorPos);
		}
		else
		{
			strHash = pdecHeader->FileHash;
		}
		free(lpEncHeader);
	}

	CloseHandle(hRead);
	_strHash = strHash;

	return TRUE;
}

/**
@brief      SelfEncFile
@author   JHLEE
@date      2012.03.15
@note      트레이 메뉴/파일 보호의 수동 암호화 관련 작업
                -> 암호화된 파일 확장자는 .SENC /  기존 확장자는 암호화된 파일안에 기록해둔다.
@CString  원래 파일 경로
@note		암호화 하는데 사용되는 CryptoApi들의 변수들을 멤버 변수로 설정하여 사용하도록 수정 필요
               현재는 매 함수마다 같은 함수들이 반복되는데 이것을 멤버 함수로 변경하여 코드 재사용을 할 필요 있음
			   우선은 매 함수마다 사용하도록 우선 처리 (추후 개선 해야 함)
*/
BOOL CFileEncrypt::SelfEncFile(CString _strUserID,CString _strFilePath, DWORD* pRetCode, DWORD* pErrPos)
{
	__int64 nEncSize = 0;

	PBYTE pbBuffer = NULL; 
	DWORD dwCount; 
	DWORD dwTotalFileSize = 0;//암호화 할 파이 크기
	DWORD dwEncBufferSize; 
	BOOL bResult = TRUE;
	CString strDefaultHash = _T("DSNTECH2012");

	CString strKey = _T(""), strMsg = _T(""), strEncPath = _T("") , strExt = _T(""), strOneTimekey=_T("");

	//.txt, .log, .dat 
	CString strText = ExtractFileExt(_strFilePath);
	strExt = strText.Mid(1);
	strEncPath.Format(_T("%s%s.SENC"), ExtractFilePath(_strFilePath), ExtractBaseFileName(_strFilePath));

	if( FALSE == CreateKey(_strUserID, strKey, strDefaultHash))//// Kevin(2013-6-3)_T("DSNTECH2012")) )
	{
		if(pRetCode)
			*pRetCode = 0;
		
		if(pErrPos)
			*pErrPos = -1;
		return FALSE;
	}
		
	HANDLE hRead = NULL, hWrite = NULL;
	hRead  = CreateFile(_strFilePath.GetBuffer(),
									GENERIC_READ,// | GENERIC_WRITE,
									FILE_SHARE_READ,// | FILE_SHARE_WRITE,
									0,
									OPEN_EXISTING,
									NULL,
									0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DBGLOG(_T("[SelfEncFile] %s read error (%d)"), _strFilePath.GetBuffer(), GetLastError());

		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -2;
		
		return FALSE;
	}

	hWrite = CreateFile(strEncPath.GetBuffer(),
									GENERIC_READ | GENERIC_WRITE,
									FILE_SHARE_READ | FILE_SHARE_WRITE,
									0,
									CREATE_ALWAYS,
									NULL,
									0);

	if( hWrite == INVALID_HANDLE_VALUE || hWrite == NULL )
	{

		DBGLOG(_T("[SelfEncFile] %s create error (%d)"), strEncPath.GetBuffer(), GetLastError());

		CloseHandle(hRead);

		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -3;

		return FALSE;
	}


	dwTotalFileSize = ::GetFileSize(hRead, NULL);

	//암호화 헤더를 써준다.
	//CString strOneTimekey;
	//unsigned char userKey[8] = {0,};
	byte pseudoKey[FILE_ENC_KEY_LEN+1] = {0,};
	byte oneTimeKey[FILE_ENC_KEY_LEN+1]={0,};

	srand( (unsigned)time(NULL));

	for( int i=0; i<FILE_ENC_KEY_LEN; i++ )
		oneTimeKey[i] = (unsigned char)(rand() % 255);

	memcpy(pseudoKey, oneTimeKey, FILE_ENC_KEY_LEN);

	Encrypt_Header header;
	memset(&header, 0, sizeof(Encrypt_Header));

	CString strHeader = _T("");
	StringCchCopyA(header.sig, 16, ITCMS_SIG2);
	StringCchCopy(header.FileFullPathName, 1024, _strFilePath.GetBuffer());
	StringCchCopy(header.FileExt, 16, PathFindExtension(_strFilePath.GetBuffer()));
	StringCchCopy(header.FileHash, 64, strDefaultHash.GetBuffer());
	StringCchCopy(header.UserID, MAX_PATH, _strUserID.GetBuffer());
	StringCchCopy(header.Version, 32, m_strProductVersion.GetBuffer());
	if(m_strSiteIdx.GetBuffer() == 0)
		LoadSiteIdx();
	StringCchCopy(header.SiteIndex, 16, m_strSiteIdx.GetBuffer());
	SYSTEMTIME st;
	ZeroMemory(&st, sizeof(st));
	GetLocalTime(&st);
	CopyMemory(&header.FileCreateTime, &st, sizeof(st));
	header.length = dwTotalFileSize;
	
	DBGLOGA("enc normal sudokey = ");
	for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
	{
		DBGLOGA("%x", pseudoKey[i]);
	}

//------------
	//pseudoKey 암호화
	DWORD dwDatalen, dwErrorCode, dwErrorPos;
	dwDatalen = FILE_ENC_KEY_LEN;
	DBGLOG(L"EncfFile kEY = %s", strKey.GetBuffer(0));
	if( AES256_Enc(strKey.GetBuffer(0), (BYTE*)pseudoKey, sizeof(pseudoKey), dwDatalen, &dwErrorCode, &dwErrorPos) )
	{
		DBGLOG(L"pseudoKey = %d", dwDatalen);
		memcpy(header.pseudoKey, pseudoKey, dwDatalen);
	}
	else
	{
		DBGLOG(_T("Aes256 pseudoKey enc Err (ErrorCode: %d, ErrorPos: %d)"), dwErrorCode, dwErrorPos);
		bResult = FALSE;
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -4;

		goto Exit_SelfEnc;
	}

	header.dwKeyLen = dwDatalen;
	DBGLOGA("enc enc sudokey = ");
	for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
	{
		DBGLOGA("%x", pseudoKey[i]);
	}
		// Kevin(2013-6-3)
	// HEADER암호화
	DWORD dwWriteSize = 0;

	dwDatalen=dwErrorCode=dwErrorPos = 0;
	dwDatalen = sizeof(header); // - sizeof(header.caption) - sizeof(header.pseudoKey) - sizeof(header.mac);
	DWORD dwHeaderBlockLen = ENC_BLOCK_LEN *(dwDatalen / ENC_BLOCK_LEN);
	//if(dwDatalen % ENC_BLOCK_LEN > 0)
	dwHeaderBlockLen += ENC_BLOCK_LEN;

	DWORD dwRetlen = dwDatalen;
	LPBYTE lpEncData= (LPBYTE)malloc(dwHeaderBlockLen);
	if(lpEncData != NULL)
	{
		ZeroMemory(lpEncData, dwDatalen);
		CopyMemory(lpEncData, (BYTE *)&header, sizeof(header));
		DBGLOG(L"header enc size = %d", dwDatalen);
		if( AES256_Enc(KEY_ENC_KEY, (BYTE *)lpEncData, dwHeaderBlockLen, dwRetlen, &dwErrorCode, &dwErrorPos) == FALSE)
		{
 			DBGLOG(_T("Aes256 pseudoKey enc Err (ErrorCode: %d, ErrorPos: %d)"), dwErrorCode, dwErrorPos);
 			bResult = FALSE;
			free(lpEncData);
 			if(pRetCode)
 				*pRetCode = dwErrorCode;
	 
 			if(pErrPos)
 				*pErrPos = dwErrorPos;
	 
 			goto Exit_SelfEnc;
		}
		DEFAULTENCHEADER hDHeader;
		ZeroMemory(&hDHeader, sizeof(hDHeader));
		CopyMemory(&hDHeader.caption, ITCMS_SIG, strlen(ITCMS_SIG));
		hDHeader.headersize = dwRetlen;
		hDHeader.headeroffset = sizeof(hDHeader);
		hDHeader.reserved[0] = 's';
		hDHeader.reserved[1] = 'f';

		//헤더파일 기록
		BOOL bWrittenSame = FALSE;
		BOOL bWriteSuccess = WriteFile(hWrite, &hDHeader, sizeof(hDHeader), &dwWriteSize, NULL);
		if (dwWriteSize == sizeof(hDHeader)) {
			bWrittenSame = TRUE;
		} else {
			//ezLog(_T("[OfsCrypt] Error: dwWriteSize != sizeof(hDHeader) [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);
		}
		if (!bWriteSuccess || !bWrittenSame) {
			free(lpEncData);
			bResult = FALSE;
			if(pRetCode)
				*pRetCode = GetLastError();

			if(pErrPos)
				*pErrPos = -20;

			DBGLOG(_T("[default aes fail]---1"));
			goto Exit_SelfEnc;
		}
		nEncSize += dwWriteSize;

		//memcpy(&header.pseudoKey, pseudoKey, dwDatalen);
		DBGLOG(_T("SelfEncFile enc succeed! %s[%d->%d]"), header.FileFullPathName, dwDatalen, dwRetlen);
		bWriteSuccess = WriteFile(hWrite, lpEncData, dwRetlen, &dwWriteSize, NULL);
		bWrittenSame = FALSE;
		if (dwWriteSize == dwRetlen) {
			bWrittenSame = TRUE;
		} else {
			//ezLog(_T("[OfsCrypt] [Error] dwWriteSize: %lu != dwRetlen: %lu [line: %d, function: %s, file: %s]\r\n"), dwWriteSize, dwRetlen, __LINE__, __FUNCTIONW__, __FILEW__);
		}

		if (!bWriteSuccess || !bWrittenSame) {
			free(lpEncData);
			CloseHandle(hRead);
			CloseHandle(hWrite);
			//DeleteFile(strEncPath);
			bResult = FALSE;
			if(pRetCode)
				*pRetCode = GetLastError();

			if(pErrPos)
				*pErrPos = -5;

			goto Exit_SelfEnc;
		}
		free(lpEncData);
		nEncSize += dwWriteSize;
	}

	//-------------------------------------------------------------------------------------------------------------------------------
	dwEncBufferSize = dwTotalFileSize;		//32바이트 가량 더 여유를 잡아 준다.
	DWORD dwAllocBlock = ENC_BLOCK_LEN * (dwEncBufferSize/ENC_BLOCK_LEN);
	//if((dwEncBufferSize % ENC_BLOCK_LEN) > 0)
	dwAllocBlock += (ENC_BLOCK_LEN * 3);
	DBGLOG(L"dwTotalFileSize = %d, dwAllocBlock = %d", dwTotalFileSize, dwAllocBlock);
	pbBuffer = (BYTE *)malloc(dwAllocBlock);//dwEncBufferSize);
	if(!pbBuffer)
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -6;
		
		bResult = FALSE;
		//goto Exit_SelfEnc;
	}

	if(!ReadFile(
		hRead, 
		pbBuffer, 
		dwEncBufferSize, 
		&dwCount, 
		NULL))
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -7;
		
		bResult = FALSE;
		goto Exit_SelfEnc;
	}

	DWORD dwNorLen = dwCount;
	DBGLOG(L"dwAllocBlock = %d, dwCount = %d", dwAllocBlock, dwCount);

	oneTimeKey[15] = '\0';
	if( FALSE ==  AES256_Enc2(oneTimeKey, FILE_ENC_KEY_LEN, pbBuffer, dwAllocBlock, dwCount, &dwErrorCode, &dwErrorPos) )
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -8;

		bResult = FALSE;
		goto Exit_SelfEnc;
	}
	if(dwNorLen > dwCount)
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = 1554;

		DBGLOG(L"Self Enc file Len > enc len %d : %d", dwNorLen, dwCount);
		bResult = FALSE;
		goto Exit_SelfEnc;

	}
	
	if((dwCount % ENC_BLOCK_LEN) != 0)
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = 1555;

		DBGLOG(L"Self Enc Block Length not dwCount / ENC_BLOCK_LEN = %d", dwCount);
		bResult = FALSE;
		goto Exit_SelfEnc;

	}
	// Kevin(2014-3-4)
	// 한번에 암호화데이터를 쓰지못한 경우 나머지데이터를 쓰도록한다.
	DWORD dwWriteCnt = 0;
	DWORD dwRemainder = dwCount;
	DWORD dwPos = 0;
	do {
		dwWriteCnt = 0;
		if(!WriteFile(hWrite, pbBuffer+dwPos, dwRemainder, &dwWriteCnt, NULL))
		{
			if(pRetCode)
				*pRetCode = GetLastError();
			if(pErrPos)
				*pErrPos = -10;

			bResult = FALSE;
			DBGLOG(_T("[SelfEncFile fail]---5"));
			goto Exit_SelfEnc;
		}
		nEncSize += dwWriteCnt;
		dwRemainder -= dwWriteCnt;
		dwPos += dwWriteCnt;

		if (dwRemainder > 0) {
			//ezLog(_T("[OfsCrypt] dwRemainder > 0 dwRemainder: %d [line: %d, function: %s file: %s]\r\n"), dwRemainder, __LINE__, __FUNCTIONW__, __FILEW__);
		}
	}while(dwRemainder > 0);

	FlushFileBuffers(hWrite);
	DBGLOG(L"selfEncFile write %d, written = %d", dwAllocBlock, dwWriteCnt);

Exit_SelfEnc:

	if( hRead )
		CloseHandle(hRead);
	
	if( hWrite )
		CloseHandle(hWrite);

	DBGLOG(L"selfencfile enc len = %d", nEncSize);
	if((nEncSize % ENC_BLOCK_LEN) != 0)
	{
		bResult = FALSE;
		if(pRetCode)
			*pRetCode = 0;

		if(pErrPos)
			*pErrPos = 2000;
	}

	if(pbBuffer) 
	{
		free(pbBuffer); 
	}

	//실패시 빈 암호화 파일은 삭제
	if(FALSE == bResult )
	{
		DeleteFile(strEncPath);
	}

	DBGLOG(L"selfencfile bResult = %s", (bResult) ? L"TRUE" : L"FALSE");
	return bResult;	
}


BOOL CFileEncrypt::SelfDecFile(CString _strUserID, CString _strEncFilePath, DWORD* pRetCode, DWORD* pErrPos )
{

	PBYTE pbBuffer = NULL; 
	HANDLE hRead = NULL, hWrite = NULL;
	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	HCRYPTPROV hCryptProv = NULL; 

	DWORD dwPlainLen = 0;
	DWORD dwKeyLen = 0;

	DWORD dwFileLength, dwEncBufferSize ;
	BOOL bResult = TRUE;
	CString strKey = _T(""), strExt = _T(""), strHeader = _T("");
	
	DWORD dwReadData=0;
	byte oneTimeKey[FILE_ENC_KEY_LEN] ={0,};
	byte pseudoKey[FILE_ENC_KEY_LEN+1] = {0};
	DWORD dwHeaderRead = 0;
	//PEncrypt_Header pencrypt = NULL;
	DBGLOG(L"SelfDecFile(%s)", _strEncFilePath.GetBuffer());

	hRead = CreateFile(_strEncFilePath.GetBuffer(), 
		GENERIC_READ,// | GENERIC_WRITE,
		FILE_SHARE_READ,// | FILE_SHARE_WRITE,
		0,
		OPEN_EXISTING,
		NULL,
		0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		return FALSE;
	}
	
	dwFileLength = ::GetFileSize(hRead, NULL);
	if( dwFileLength <= 0)
	{
		
		if(pRetCode)
			*pRetCode = 0;

		if(pErrPos)
			*pErrPos = -2;

		bResult = FALSE;
		goto Exit_SelfDec;
	}

		// Kevin(2013-6-4)
	DEFAULTENCHEADER dheader;
	ZeroMemory(&dheader, sizeof(dheader));
	if( ReadFile(hRead, &dheader, sizeof(dheader), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		CloseHandle(hWrite);
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE2"));
		return FALSE;
	}
	DBGLOG(L"SelfDecFile() Read");
	if(!		(	strcmp(dheader.caption, ITCMS_SIG) == 0
			&&	( (dheader.reserved[0] == 'o' && dheader.reserved[1] == 'f')
				||(dheader.reserved[0] == 'e' && dheader.reserved[1] == 'g')
				||(dheader.reserved[0] == 's' && dheader.reserved[1] == 'f') ))
	)
	{
		DBGLOG(L"SelfDecFile() unkonw file format!!");
		CloseHandle(hRead);

		*pRetCode = 100;
		*pErrPos = 100;
		return FALSE;
	}

	LPBYTE lpEncHeader = (LPBYTE)malloc(dheader.headersize);
	if(lpEncHeader != NULL)
	{
		DWORD dwHeaderRead = dheader.headersize;
		if( ReadFile(hRead,lpEncHeader, dheader.headersize, &dwHeaderRead, NULL ) == FALSE )
		{
			free(lpEncHeader);
			if(pRetCode)
				*pRetCode = GetLastError();

			if(pErrPos)
				*pErrPos = -3;
			bResult =  FALSE;
			goto Exit_SelfDec;
		}

		DBGLOG(L"SelfDecFile() Read encheader");

		PEncrypt_Header pdecHeader = (PEncrypt_Header)lpEncHeader;
		DWORD dwRetLen = dheader.headersize;

		DWORD dwErrorCode=0, dwErrorPos=0;
		if( FALSE == AES256_Dec(KEY_ENC_KEY,  (BYTE *)lpEncHeader, dheader.headersize,  dwRetLen, &dwErrorCode, &dwErrorPos))
		{
			if(dwErrorCode)
				DBGLOG(L"error Code  = %d", dwErrorCode);

			if(dwErrorPos)
				DBGLOG(L"error Code  = %d", dwErrorPos);

			free(lpEncHeader);
			goto Exit_SelfDec;
		}
		else
		{
			DBGLOG(L"SelfDecFile() after header decrypt");
			if(memcmp(pdecHeader->sig, ITCMS_SIG2, sizeof(ITCMS_SIG2)) != 0)
			{
				free(lpEncHeader);
				DBGLOG(L"SelfDecFile() unkonw file format!!");
				*pRetCode = 101;
				*pErrPos = 101;

				goto Exit_SelfDec;
			}
			strExt = pdecHeader->FileExt;
			dwPlainLen = pdecHeader->length;
			dwKeyLen =pdecHeader->dwKeyLen;
			DBGLOG(L"SelfDecFile() ext = %s",pdecHeader->FileExt );
			DBGLOG(L"SelfDecFile() length = %d",pdecHeader->length );
			DBGLOG(L"SelfDecFile() dwKeyLen = %d",pdecHeader->dwKeyLen );
			memcpy(pseudoKey, pdecHeader->pseudoKey, pdecHeader->dwKeyLen);
		}
		free(lpEncHeader);
	}

	if( FALSE == CreateKey(_strUserID, strKey, _T("DSNTECH2012")) )
	{
		if(pRetCode)
			*pRetCode = 0;

		if(pErrPos)
			*pErrPos = -4;
		
		bResult = FALSE;
		goto Exit_SelfDec;
		
	}

	DWORD dwEncLen = 0;
	DWORD dwErrorCode = 0, dwErrorPos = 0;

	DBGLOGA("dec enc sudokey = FILE_ENC_KEY_LEN");
	for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
	{
		DBGLOGA("%x", pseudoKey[i]);
	}
	
	dwEncLen = dwKeyLen;
	DBGLOG(L"DefFile kEY = %s", strKey.GetBuffer(0));
	//dwEncLen = pdecHeader->dwKeyLen;		//-1을 하는 이유는 여유분으로 1바이트를 추가해놓았기 때문 
	if( FALSE == AES256_Dec(strKey.GetBuffer(0), pseudoKey, dwKeyLen,  dwEncLen, &dwErrorCode, &dwErrorPos))
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -5;

		bResult = FALSE;
		goto Exit_SelfDec;
	}

	for(int i=0; i < FILE_ENC_KEY_LEN+1; i++)
	{
		DBGLOGA("%x", pseudoKey[i]);
	}

	memcpy(oneTimeKey, pseudoKey, FILE_ENC_KEY_LEN);

	//복호화 시작
	dwEncBufferSize = dwFileLength - dheader.headersize - sizeof(DEFAULTENCHEADER);		//헤더길이 만큼 제거
	pbBuffer = (BYTE *)malloc(dwEncBufferSize);
	memset(pbBuffer, 0, dwEncBufferSize);

	if( ReadFile(hRead, pbBuffer, dwEncBufferSize, &dwReadData, NULL ) == FALSE )
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -7;

		bResult = FALSE;
		goto Exit_SelfDec;
	}

	oneTimeKey[15] = '\0';
	//파일 복호화
	if( FALSE == AES256_Dec2(oneTimeKey, FILE_ENC_KEY_LEN, pbBuffer , dwEncBufferSize,  dwReadData, &dwErrorCode, &dwErrorPos))
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -8;

		bResult = FALSE;
		goto Exit_SelfDec;
	}
	hWrite = CreateFile(_strEncFilePath + SELF_ENCRYPT_EXTENTEION,
								GENERIC_READ | GENERIC_WRITE,
								FILE_SHARE_READ | FILE_SHARE_WRITE,
								0,
								CREATE_ALWAYS,
								NULL,
								0);


	if( hWrite == INVALID_HANDLE_VALUE || hWrite == NULL )
	{
		CloseHandle(hRead);
	
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -1;
		return FALSE;
	}
	//복호화된 내용 파일 write
	DWORD dwWritelen = 0;
	BOOL bWrittenSame = FALSE;
	BOOL bWriteSuccess = WriteFile(hWrite, pbBuffer, dwPlainLen/*dwEncBufferSize*/, &dwWritelen, NULL );
	if (dwWritelen == dwPlainLen) {
		bWrittenSame = TRUE;
	} else {
		//ezLog(_T("[OfsCrypt] Error: dwWritelen != dwPlainLen [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);
	}
	if (!bWriteSuccess || !bWrittenSame) {
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -9;

		bResult = FALSE;
		goto Exit_SelfDec;
	}

Exit_SelfDec:
	if(pbBuffer) 
	{
		free(pbBuffer); 
	}

	if( !bResult )
	{
		if(hRead)
			CloseHandle(hRead);
		if(hWrite)
			CloseHandle(hWrite);
		DeleteFile(_strEncFilePath + SELF_ENCRYPT_EXTENTEION);
		return bResult;
	}
	
	if(hRead)
		CloseHandle(hRead);
	if(hWrite)
		CloseHandle(hWrite);

	CString strFilePath = _T(""), strMsg = _T("");
	//파일 확장자는 strExt값..
	strFilePath = GetSelfFileFullPath(_strEncFilePath, strExt);

	strMsg.Format(_T("strFilePath %s"), strFilePath);

	DBGLOG(strMsg.GetBuffer());
	CString strDecFile = L"";
	strDecFile = strFilePath;

	try
	{
		if( IsFileExist2(strFilePath.GetBuffer() ) )	//복원할 파일명이 이미 존재한다면
		{
			int i = 0;
			while(TRUE)
			{
				if(i > 300)
				{
					//복호화 실패로 간주한다.
					*pErrPos = -10;
					return FALSE;
						//break;
				}
				CString strOriPath = _T("");
				int iFind = strFilePath.ReverseFind('.');
				strOriPath.Format(_T("%s(n%d)%s"),  strFilePath.Left(iFind), i, ExtractFileExt(strFilePath) );
				strDecFile = strOriPath;
				if( IsFileExist2(strOriPath.GetBuffer()) )		//새로운 복원 파일명 또한 존재한다면
				{
					if( DeleteFile(strOriPath) )	//삭제 해서 성공시
					{
						CFile::Rename(_strEncFilePath + SELF_ENCRYPT_EXTENTEION, strOriPath); // hhh(2013-7-16)
						break;
					}
				}
				else
				{
					CFile::Rename(_strEncFilePath + SELF_ENCRYPT_EXTENTEION, strOriPath); // hhh(2013-7-16)
					break;
				}
				i++;
			}
		}
		else
		{
			CFile::Rename(_strEncFilePath + SELF_ENCRYPT_EXTENTEION, strFilePath); // Kevin(2013-5-13)
			DBGOUT(_T("strFilePath %s"), strFilePath.GetBuffer());
		}
	}
	catch(CFileException* pEx )
	{
		if(pRetCode)
			*pRetCode = GetLastError();

		if(pErrPos)
			*pErrPos = -11;

		pEx->Delete();
		return FALSE;
	}
	DeleteFile(_strEncFilePath + SELF_ENCRYPT_EXTENTEION);
	if(IsFileExist2(strDecFile.GetBuffer()) == TRUE)
	{
		DeleteFile(_strEncFilePath);
	}

	return TRUE;
}

/**
@brief      GetSelfEncryptFileFullPath
@author   JHLEE
@date      2012.03.15
@note      암호화 파일 확장자 변경하여 경로 리턴
@CString  암호화 파일 전체 경로
*/
CString CFileEncrypt::GetSelfEncryptFileFullPath(CString _strFilePath)
{
	CString strFileExt = _T(""), strFullPath = _T(""), strFilePath = _T(""), strExt  = _T(""), strOriFile = _T("");
	
	strFilePath = ExtractFilePath(_strFilePath);
	strOriFile = ExtractBaseFileName(_strFilePath);
	strExt = ExtractFileExt(_strFilePath);

	DBGLOG( strOriFile.GetBuffer());

	if( strOriFile.IsEmpty() ) return strFileExt;
	int iFind = strExt.ReverseFind('.');
	strFileExt = strExt.Mid( iFind+1);

	DBGLOG( strFileExt.GetBuffer());
	strFullPath.Format(_T("%s%s.s%s"), strFilePath, strOriFile, strFileExt);

	return strFullPath;
}

/**
@brief      GetEncryptFileFullPath
@author   JHLEE
@date      2012.03.15
@note      암호화 파일 확장자 변경하여 경로 리턴
@CString  암호화 파일 전체 경로
*/
CString CFileEncrypt::GetEncryptFileFullPath(CString _strFilePath)
{
	CString strFileExt = _T(""), strFullPath = _T(""), strFilePath = _T(""), strExt  = _T(""), strOriFile = _T("");

	strFilePath = ExtractFilePath(_strFilePath);
	strOriFile = ExtractBaseFileName(_strFilePath);
	strExt = ExtractFileExt(_strFilePath);

	DBGLOG(strOriFile.GetBuffer());

	if( strOriFile.IsEmpty() ) return strFileExt;
	int iFind = strExt.ReverseFind('.');
	strFileExt = strExt.Mid( iFind+1);

	DBGLOG( strFileExt.GetBuffer());
	strFullPath.Format(_T("%s%s.e%s"), strFilePath, strOriFile, strFileExt);
	return strFullPath;
}

/**
@brief      GetFileFullPath
@author   JHLEE
@date      2012.03.15
@note      암호화된 파일을 복호화 하는 과정으로 원래의 파일명으로 되돌려 줘야 한다.
@CString  원래 파일 경로
*/
CString CFileEncrypt::GetFileFullPath(CString _strEncFilePath)
{
	CString strFileExt = _T(""), strFullPath = _T(""), strFilePath = _T(""), strExt = _T(""), strOriFile = _T("");
	strFilePath = 	ExtractFilePath(_strEncFilePath);
	strOriFile = ExtractBaseFileName(_strEncFilePath);
	strExt = ExtractFileExt(_strEncFilePath);

	if( strOriFile.IsEmpty() ) return strFileExt;
	int iFind = strExt.ReverseFind('.');
	strFileExt = strExt.Mid( iFind+2);
	strFullPath.Format(_T("%s%s.%s"), strFilePath, strOriFile, strFileExt);

	return strFullPath;
}

/**
 @brief      GetFileFullPath
 @author   JHLEE
 @date      2012.03.28
 @note      암호화된 파일을 복호화 하는 과정으로 원래의 파일명으로 되돌려 줘야 한다.(수동암호화)
 @CString  원래 파일 경로
 */
CString CFileEncrypt::GetSelfFileFullPath(CString _strEncFilePath, CString _strExt)
{
	CString strFileExt = _T(""), strFullPath = _T(""), strFilePath = _T(""), strExt = _T(""), strOriFile = _T("");

	strFilePath = 	ExtractFilePath(_strEncFilePath);
	strOriFile = ExtractBaseFileName(_strEncFilePath);
	strFullPath.Format(_T("%s%s%s"), strFilePath, strOriFile, _strExt);

	return strFullPath;
}

/**
@brief      AES256_Enc			
@author   hyo haeng heo
@date      2013.02.19
@param	_pKey						암호화에 사용될 키값
@param	_pDataBuff					암호화할 데이터가 들어있는 포인터
@param	_dwDataBuffLen			암호화할 데이터가 들어있는 버퍼 사이즈
@param	_dwDataLen(_InOut)	암호화할 데이터의 길이(out: 암호화된 사이즈)
@param	pdwErrorCode			암호화 실패시 GetLastError값
@param	pdwErrPos					암호화 실패시 실패가 발생한 함수 위치

@return		BOOL
*/
BOOL CFileEncrypt::AES256_Enc(LPTSTR _pKey, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos)
{
	HCRYPTPROV hCryptProv = NULL; 
	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	BOOL bResult = TRUE;

	if(_dwDataBuffLen < _dwDataLen)
	{
		if(pdwErrorCode)
			*pdwErrorCode = 0;

		if(pdwErrPos)
			*pdwErrPos = -10;
		return FALSE;
	}

	if( FALSE == CryptAcquireContext(
		&hCryptProv, 
		NULL, 
		NULL, 
		PROV_RSA_AES, 
		CRYPT_VERIFYCONTEXT))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();
		
		if(pdwErrPos)
			*pdwErrPos = -11;
		goto Exit_AES256_Enc;
	}

	// Create a hash object. 
	if( FALSE == CryptCreateHash(
											hCryptProv, 
											// Kevin(2013-4-26)CALG_SHA_256, 
											CALG_SHA1, // Kevin(2013-4-26) sha256은 xp에서 사용불가함
											0, 
											0, 
											&hHash))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -12;
		goto Exit_AES256_Enc;
	}
	
	// Hash the password. 
	if( FALSE == CryptHashData(
		hHash, 
		(BYTE *)_pKey, 
		lstrlen(_pKey), 
		0))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -13;
		goto Exit_AES256_Enc;
	}

	// Derive a session key from the hash object. 
	if( FALSE == CryptDeriveKey(
		hCryptProv, 
		CALG_AES_256, 
		hHash, 
		CRYPT_EXPORTABLE, 
		&hKey))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -14;
		goto Exit_AES256_Enc;
	}

	// Encrypt data. 
	if( FALSE == CryptEncrypt(
		hKey, 
		NULL, 
		TRUE,
		0, 
		_pDataBuff, 
		&_dwDataLen, 
		_dwDataBuffLen))
	{ 
		DBGLOG(L"dwDataLen = %d", _dwDataLen);
		bResult = FALSE;
		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -15;
	} 

Exit_AES256_Enc:
	if(hHash) 
	{
		if(!(CryptDestroyHash(hHash)))
		{
		}
		hHash = NULL;
	}

	// Release the session key. 
	if(hKey)
	{
		if(!(CryptDestroyKey(hKey)))
		{
		}
	}

	// Release the provider handle. 
	if(hCryptProv)
	{
		if(!(CryptReleaseContext(hCryptProv, 0)))
		{
		}
	}

	return bResult;
}

BOOL CFileEncrypt::AES256_Enc2(LPBYTE _pKey, DWORD dwKeyLen, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos)
{
	HCRYPTPROV hCryptProv = NULL;
	HCRYPTKEY hKey = NULL;
	HCRYPTHASH hHash = NULL;
	BOOL bResult = TRUE;

	if(_dwDataBuffLen < _dwDataLen)
	{
		if(pdwErrorCode)
			*pdwErrorCode = 0;

		if(pdwErrPos)
			*pdwErrPos = -10;
		return FALSE;
	}

	if( FALSE == CryptAcquireContext(
												&hCryptProv, 
												NULL, 
												NULL, 
												PROV_RSA_AES, 
												CRYPT_VERIFYCONTEXT))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();
		
		if(pdwErrPos)
			*pdwErrPos = -11;
		goto Exit_AES256_Enc;
	}

	// Create a hash object. 
	if( FALSE == CryptCreateHash(
											hCryptProv, 
											// Kevin(2013-4-26)CALG_SHA_256, 
											CALG_SHA1, // Kevin(2013-4-26) sha256은 xp에서 사용불가함
											0, 
											0, 
											&hHash))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -12;
		goto Exit_AES256_Enc;
	}
	
	// Hash the password. 
	if( FALSE == CryptHashData(
										hHash, 
										(BYTE *)_pKey, 
										dwKeyLen, 
										0))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -13;
		goto Exit_AES256_Enc;
	}


	// Derive a session key from the hash object. 
	if( FALSE == CryptDeriveKey(
											hCryptProv, 
											CALG_AES_256, 
											hHash, 
											CRYPT_EXPORTABLE, 
											&hKey))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -14;
		goto Exit_AES256_Enc;
	}
	

	// Encrypt data. 
	if( FALSE == CryptEncrypt(
										hKey, 
										NULL, 
										TRUE,
										0, 
										_pDataBuff, 
										&_dwDataLen, 
										_dwDataBuffLen))
	{
		DBGLOG(L"dwDataLen = %d", _dwDataLen);
		bResult = FALSE;
		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -15;
	} 

Exit_AES256_Enc:
	if(hHash) 
	{
		if(!(CryptDestroyHash(hHash)))
		{
	
		}
		hHash = NULL;
	}

	// Release the session key. 
	if(hKey)
	{
		if(!(CryptDestroyKey(hKey)))
		{
			
		}
	}

	// Release the provider handle. 
	if(hCryptProv)
	{
		if(!(CryptReleaseContext(hCryptProv, 0)))
		{
		
		}
	}

	return bResult;
}

/**
@brief      AES256_Enc			
@author   hyo haeng heo
@date      2013.02.19
@param	_pKey						암호화에 사용될 키값
@param	_pDataBuff					암호화할 데이터가 들어있는 포인터
@param	_dwDataBuffLen			암호화할 데이터가 들어있는 버퍼 사이즈
@param	_dwDataLen(_InOut)	암호화할 데이터의 길이(out: 암호화된 사이즈)
@param	pdwErrorCode			암호화 실패시 GetLastError값
@param	pdwErrPos					암호화 실패시 실패가 발생한 함수 위치
@return		BOOL
*/
BOOL CFileEncrypt::AES256_Dec(LPTSTR _pKey, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos)
{
	HCRYPTPROV hCryptProv = NULL; 
	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	BOOL bResult = TRUE;

	//버퍼의 길이보다 암호화 할 길이를 더 넣어준 경우 
	if(_dwDataBuffLen < _dwDataLen)
	{
		if(pdwErrorCode)
			*pdwErrorCode =  0;

		if(pdwErrPos)
			*pdwErrPos = -10;
		return FALSE;
	}

	if( FALSE == CryptAcquireContext(
													&hCryptProv, 
													NULL, 
													NULL, 
													PROV_RSA_AES, 
													CRYPT_VERIFYCONTEXT))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		//ERROR_PROC_NOT_FOUND
		//127 (0x7F)
		//The specified procedure could not be found.
		// 2015-08-27 kh.choi 추가
		if (ERROR_PROC_NOT_FOUND == *pdwErrorCode) {
			AfxMessageBox(_T("윈도우에 포함된 advapi32.dll 안의 함수 CryptAcquireContext 호출에 실패했습니다.\n")
					_T("윈도우를 최신으로 업데이트 하신 후 다시 설치해 주십시오."), MB_ICONERROR | MB_TOPMOST);
		}	// 2015-08-27

		if(pdwErrPos)
			*pdwErrPos = -11;
		goto Exit_AES256_Dec;
	}


	// Create a hash object. 
	if( FALSE == CryptCreateHash(
												hCryptProv, 
												// Kevin(2013-4-26)CALG_SHA_256, 
												CALG_SHA1, // Kevin(2013-4-26) sha256은 xp에서 지원 x
												0, 
												0, 
												&hHash))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -12;
		goto Exit_AES256_Dec;
	}

	// Hash the password. 
	if( FALSE == CryptHashData(
											hHash, 
											(BYTE *)_pKey, 
											lstrlen(_pKey), 
											0))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -13;
		goto Exit_AES256_Dec;
	}


	// Derive a session key from the hash object. 
	if( FALSE == CryptDeriveKey(
											hCryptProv, 
											CALG_AES_256, 
											hHash, 
											CRYPT_EXPORTABLE, 
											&hKey))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -14;
		goto Exit_AES256_Dec;
	}

	if(!CryptDecrypt(
							hKey, 
							0, 
							TRUE, 
							0, 
							_pDataBuff, 
							&_dwDataLen))
	{
		bResult = FALSE; //2013.07.18

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -15;
	}

Exit_AES256_Dec:
	if(hHash) 
	{
		if(!(CryptDestroyHash(hHash)))
		{
		}
		hHash = NULL;
	}

	// Release the session key. 
	if(hKey)
	{
		if(!(CryptDestroyKey(hKey)))
		{

		}
	}

	// Release the provider handle. 
	if(hCryptProv)
	{
		if(!(CryptReleaseContext(hCryptProv, 0)))
		{

		}
	}

	return bResult;
}

/* AES256_Dec(......) 함수에 DWORD dwKeyLen 인자 추가 */
BOOL CFileEncrypt::AES256_Dec2(LPBYTE _pKey, DWORD dwKeyLen, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos)
{
	HCRYPTPROV hCryptProv = NULL; 
	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	BOOL bResult = TRUE;

	//버퍼의 길이보다 암호화 할 길이를 더 넣어준 경우 
	if(_dwDataBuffLen < _dwDataLen)
	{
		DBGLOG(L"AES256_Dec2 %d < %d", _dwDataBuffLen, _dwDataLen);
		if(pdwErrorCode)
			*pdwErrorCode =  0;

		if(pdwErrPos)
			*pdwErrPos = -10;
		return FALSE;
	}
		
	if( FALSE == CryptAcquireContext(
													&hCryptProv, 
													NULL, 
													NULL, 
													PROV_RSA_AES, 
													CRYPT_VERIFYCONTEXT))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -11;
		DBGLOG(L"AES256_Dec2 CryptAcquireContext %d", *pdwErrorCode);

		goto Exit_AES256_Dec;
	}

	// Create a hash object. 
	if( FALSE == CryptCreateHash(
												hCryptProv, 
												// Kevin(2013-4-26)CALG_SHA_256, 
												CALG_SHA1, // Kevin(2013-4-26) sha256은 xp에서 지원 x
												0, 
												0, 
												&hHash))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -12;
			DBGLOG(L"AES256_Dec2 CryptCreateHash %d", *pdwErrorCode);

		goto Exit_AES256_Dec;
	}


	// Hash the password. 
	if( FALSE == CryptHashData(
											hHash, 
											(BYTE *)_pKey, 
											dwKeyLen, 
											0))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -13;

		DBGLOG(L"AES256_Dec2 CryptHashData %d", *pdwErrorCode);

		goto Exit_AES256_Dec;
	}

	// Derive a session key from the hash object. 
	if( FALSE == CryptDeriveKey(
											hCryptProv, 
											CALG_AES_256, 
											hHash, 
											CRYPT_EXPORTABLE, 
											&hKey))
	{
		bResult = FALSE;

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -14;
		DBGLOG(L"AES256_Dec2 CryptDeriveKey %d", *pdwErrorCode);
		goto Exit_AES256_Dec;
	}

	if(!CryptDecrypt(
							hKey, 
							0, 
							TRUE, 
							0, 
							_pDataBuff, 
							&_dwDataLen))
	{
		bResult = FALSE;	//2013.07.18

		if(pdwErrorCode)
			*pdwErrorCode =  GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -15;

		DBGLOG(L"AES256_Dec2 CryptDecrypt %d", *pdwErrorCode);

	}

Exit_AES256_Dec:
	if(hHash) 
	{
		if(!(CryptDestroyHash(hHash)))
		{
		}
		hHash = NULL;
	}

	// Release the session key. 
	if(hKey)
	{
		if(!(CryptDestroyKey(hKey)))
		{
		}
	}

	// Release the provider handle. 
	if(hCryptProv)
	{
		if(!(CryptReleaseContext(hCryptProv, 0)))
		{
		}
	}
	DBGLOG(L"AES256_Dec2 end");

	return bResult;
}


/**
@brief       SimpleEncXor 함수
@author     hyo haeng heo
@date       2013.02.20
@param   _strEncData		xor로 암호화할 데이터 
@note		Key값을 파일에 백업시 xor로 데이터를 암호화 한다.
*/
CString CFileEncrypt::SimpleXor(CString _strData)
{
	CString strRet=_T("");
	char	szAnsiBuff[MAX_PATH]={0,};
	char	szXorData[MAX_PATH]={0,};
	BYTE	keyChar[8] = {0x01, 0x03, 0x01, 0x05, 0x01, 0x03, 0x01, 0x01};

	LPWSTR pRet =NULL ;
	wsprintfA(szAnsiBuff, "%s", _strData);

	for (int i= 0, j=0;  i< _strData.GetLength(); i++)
	{
		szXorData[i] = szAnsiBuff[i] ^ keyChar[j];
		if( ++j == sizeof(keyChar) )
			j =0;
	}

	WCHAR Text[80]={0,};
	wsprintfW(Text, L"%s",szXorData );
	strRet.Format(_T("%s"), Text);
	return strRet;
}


/**
@brief       EncStrData 함수
@author    hyo haeng heo
@date       2013.02.20
@param   _strData		암호화할 데이터
@param   _strKey		암호화할 Key
@return		암호화된 데이터를 CString으로 반환
@note		인자로 들어온 데이터를 정해진 키값에 따라 암호화 하고 base64로 encode한다.
*/
CString CFileEncrypt::EncStrData(CString _strData, CString _strKey, DWORD* pdwErrorCode , DWORD* pdwErrPos)
{
	CFileEncrypt fileEnc;
	CString strKey = _T(""), strRet =_T("");
	PBYTE pBuffer=NULL, pBuffer_out= NULL;
	DWORD dwBufLen, dwDataLen, dwErrorPos, dwErrCode;
	dwBufLen = (_strData.GetLength() * 2) + 32;
	dwDataLen = _strData.GetLength() * 2;

	strKey = _strKey;
	pBuffer = (BYTE*) malloc(dwBufLen);
	memset(pBuffer, 0, dwBufLen);
	memcpy(pBuffer, _strData.GetBuffer(0), _strData.GetLength() *2);

	if(FALSE == fileEnc.AES256_Enc(strKey.GetBuffer(0), pBuffer, dwBufLen, dwDataLen, &dwErrCode, &dwErrorPos) )
	{
		DBGLOG(_T("[CmsEngine] EncKeyData Fail (ErrorCode: %d, ErrorPos: %d)"), dwErrCode, dwErrorPos);

		if(pdwErrorCode)
			*pdwErrorCode = dwErrCode;
		
		if(pdwErrPos)
			*pdwErrPos = dwErrorPos;

		if(pBuffer)
			free(pBuffer);

		return strRet;
	}


	pBuffer_out = (BYTE*) malloc(dwDataLen + 2);
	memset(pBuffer_out, 0, dwDataLen +2);
	memcpy(pBuffer_out, pBuffer, dwDataLen);

	CBase64 base64;
	strRet = base64.base64encode(pBuffer_out, dwDataLen);

	if(pBuffer)
		free(pBuffer);

	if(pBuffer_out)
		free(pBuffer_out);
	
	return strRet;
}

/**
@brief       DecStrData 함수
@author    hyo haeng heo
@date       2013.02.20
@param   _strData		복호화 할 데이터
@param   _strKey		복호화 할 키
@return		복호화된 데이터를 CString으로 반환
@note		인자로 들어온 데이터를 정해진 키값에 따라 복호화 한다.
*/
CString CFileEncrypt::DecStrData(CString _strData, CString _strKey,  DWORD* pdwErrorCode , DWORD* pdwErrPos)
{
	CString strKey =_T("") , strRet =_T(""), strLog; //SE_ENCKEY
	DWORD dwErrorPos, dwErrCode;
	
	strKey = _strKey;

	CBase64 base;
	DWORD dwBuffSize = _strData.GetLength() + 4 ;
	//DWORD dwDecLen =  (_strData.GetLength() * 2 ) ;
	char* szEncodeBuff = (char*)malloc(dwBuffSize);
	char* szDecodeBuff = (char*)malloc(dwBuffSize);
	LPWSTR pRet =NULL ;

	memset(szEncodeBuff, 0, dwBuffSize);
	memset(szDecodeBuff, 0, dwBuffSize);

	UnicodeToAnsi(szEncodeBuff, _strData, dwBuffSize);
	DWORD nDecodeSize = base.base64decode(szEncodeBuff, szDecodeBuff);

	if(szEncodeBuff)
		free(szEncodeBuff);

	if(FALSE == AES256_Dec(strKey.GetBuffer(0), (BYTE*)szDecodeBuff, dwBuffSize, nDecodeSize, &dwErrCode, &dwErrorPos) )
	{
		DBGLOG(_T("[CmsEngine] DecKeyData Fail (ErrorCode: %d, ErrorPos: %d)"), dwErrCode, dwErrorPos);
		
		if(pdwErrorCode)
			*pdwErrorCode = dwErrCode;

		if(pdwErrPos)
			*pdwErrPos = dwErrorPos;

		if(szDecodeBuff)
			free(szDecodeBuff);

		return strRet;
	}

	char* pDecBuffer = NULL;
	pDecBuffer = (char*)malloc(nDecodeSize + 4);
	if(pDecBuffer)
	{
		memset(pDecBuffer, 0, nDecodeSize + 4);
		memcpy(pDecBuffer, szDecodeBuff, nDecodeSize);
		strRet.Format(_T("%s"), pDecBuffer);
		free(pDecBuffer);
	}
	else
	{
		DBGLOG( _T("[CmsEngine] DecKeyData Fail  malloc Fail : %d"), GetLastError() );

		if(pdwErrorCode)
			*pdwErrorCode = GetLastError();

		if(pdwErrPos)
			*pdwErrPos = -1;
	}

	if(szDecodeBuff)
		free(szDecodeBuff);

	return strRet;
}

BOOL CFileEncrypt::IsEncFile(CString _strEncFilePath, CString _strEncKey/* yjLee(2015-03-17) 사용 안함. 지워도 될것으로 예상*/)
{
	PBYTE pbBuffer = NULL; 
	HANDLE hRead = NULL;
	DBGOUT(L"IsEncFile() = %s", _strEncFilePath.GetBuffer());

	hRead = CreateFile(_strEncFilePath.GetBuffer(), 
								GENERIC_READ,// | GENERIC_WRITE,
								FILE_SHARE_READ,// | FILE_SHARE_WRITE,
								0,
								OPEN_EXISTING,
								NULL,
								0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		return FALSE;
	}

	DWORD dwHeadSize = sizeof(DEFAULTENCHEADER);
	DWORD dwSize = GetFileSize(hRead, NULL);
	if(dwSize < dwHeadSize)
	{
		return FALSE;
	}

	DEFAULTENCHEADER dHeader;
	DWORD dwHeaderRead = 0;
	if( ReadFile(hRead, &dHeader, sizeof(DEFAULTENCHEADER), &dwHeaderRead, NULL ) == FALSE )
	{
		CloseHandle(hRead);
		return FALSE;
	}

	CloseHandle(hRead);

	if(dwHeaderRead > 5)
	{
		if(memcmp(&dHeader, "ITCMS", 5) == 0)
		{
			if(isalpha(dHeader.reserved[0]) && isalpha(dHeader.reserved[1]))
			{
				DBGOUT(L"IsEncFile() = this is enc file");
				return TRUE;
			}
		}
	}

	return FALSE;
}

/**
	@brief	DEFAULTENCHEADER 와 Encrypt_Header 를 파일에서 읽어 구조체에 입력. 성공하면 TRUE, 실패하면 FALSE 반환
	@author	yjLee
	@date	2015-03-17
*/
BOOL CFileEncrypt::GetFileAllHeader(CString _strEncFilePath, DEFAULTENCHEADER& _pBaseHeader, Encrypt_Header& _pEncHeader)
{
	if (_strEncFilePath.IsEmpty())
	{
		return FALSE;
	}

	HCRYPTKEY hKey = NULL; 
	HCRYPTHASH hHash = NULL; 
	HCRYPTPROV hCryptProv = NULL; 


	HANDLE hRead = NULL;
	hRead = CreateFile(_strEncFilePath.GetBuffer(),
		GENERIC_READ,
		FILE_SHARE_READ,
		0,
		OPEN_EXISTING,
		NULL,
		0);

	if(hRead == INVALID_HANDLE_VALUE || hRead == NULL)
	{
		CloseHandle(hRead);
		return FALSE;
	}

	ZeroMemory(&_pBaseHeader, sizeof(_pBaseHeader));
	ZeroMemory(&_pEncHeader, sizeof(_pEncHeader));


	// yjLee(2015-03-17) : base header 정보
	DWORD dwHeaderRead = 0;
	if (FALSE == ReadFile(hRead, &_pBaseHeader, sizeof(_pBaseHeader), &dwHeaderRead, NULL))
	{
		CloseHandle(hRead);
		return FALSE;
	}

	// yjLee(2015-03-17) : TiorSaver에 의해 암호화되지 않은 파일일 경우
	if( !(	strcmp(_pBaseHeader.caption, ITCMS_SIG) == 0 && 
		((_pBaseHeader.reserved[0] == 'o' && _pBaseHeader.reserved[1] == 'f') ||
		(_pBaseHeader.reserved[0] == 'e' && _pBaseHeader.reserved[1] == 'g') ||
		(_pBaseHeader.reserved[0] == 's' && _pBaseHeader.reserved[1] == 'f')))
		)
	{
		CloseHandle(hRead);
		return FALSE;
	}

	
	// yjLee(2015-03-17) : 암호화 헤더 
	LPBYTE lpEncHeader = (LPBYTE)malloc(_pBaseHeader.headersize);

	if (lpEncHeader == NULL)
	{
		CloseHandle(hRead);
		return FALSE;
	}

	ZeroMemory(lpEncHeader, _pBaseHeader.headersize);

	if (FALSE == ReadFile(hRead, lpEncHeader, _pBaseHeader.headersize, &dwHeaderRead, NULL))
	{
		free(lpEncHeader);
		CloseHandle(hRead);
		return FALSE;
	}

	PEncrypt_Header pEncHeader = (PEncrypt_Header)lpEncHeader;

	DWORD dwRetLen = _pBaseHeader.headersize;
	DWORD dwErrPos = 0;
	DWORD dwDecErr = 0;

	if (FALSE == AES256_Dec(KEY_ENC_KEY, (BYTE*)lpEncHeader, _pBaseHeader.headersize, dwRetLen, &dwDecErr, &dwErrPos))
	{
		free(lpEncHeader);
		CloseHandle(hRead);
		return FALSE;
	}

	if (memcmp(pEncHeader->sig, ITCMS_SIG2, sizeof(ITCMS_SIG2)) != 0)
	{
		free(lpEncHeader);
		CloseHandle(hRead);
		return FALSE;
	}


	// yjLee(2015-03-17) : 실제 데이터 암호화에 사용되는 암/복호화 키를 복호화, 해당 부분 필요 없음.
	/*
	unsigned char pseudoKey[FILE_ENC_KEY_LEN+1] = {0,};
	DWORD dwEncLen = pEncHeader->dwKeyLen;

	memcpy(pseudoKey, pEncHeader->pseudoKey, pEncHeader->dwKeyLen);

	CString strKey=_T("");


	// yjLee(2015-03-10) : one time key를 복호화 할 경우 base header의 encKeyType 확인하여 key를 create
	if(_pBaseHeader.reserved[0] == 'o' && _pBaseHeader.reserved[1] == 'f')
	{
		// 기존 생성 된 암호화 파일은 해당 byte가 0으로 설정되 있음.(ENC_KEY_TYPE_DEFAULT)
		if ( _pBaseHeader.encKeyType == ENC_KEY_TYPE_USERID)
		{
			if(FALSE == CreateKey(pEncHeader->UserID, strKey,pEncHeader->FileHash))
			{
				free(lpEncHeader);
				CloseHandle(hRead);
				return FALSE;
			}
		}
		else if ( _pBaseHeader.encKeyType == ENC_KEY_TYPE_UUID  )
		{
			if(FALSE == CreateKey(pEncHeader->SiteUUID, strKey,pEncHeader->FileHash))
			{
				free(lpEncHeader);
				CloseHandle(hRead);
				return FALSE;
			}
		}
		else
		{
			free(lpEncHeader);
			CloseHandle(hRead);
			return FALSE;
		}
	}
	else		// yjLee(2015-03-17) : base header의 reserved 영역이 "of가 아니면 실패
	{
		free(lpEncHeader);
		CloseHandle(hRead);
		return FALSE;
	}


	if(FALSE == AES256_Dec(strKey.GetBuffer(0), pseudoKey, pEncHeader->dwKeyLen, dwEncLen, &dwDecErr, &dwErrPos)  )
	{
		free(lpEncHeader);
		CloseHandle(hRead);
		return FALSE;
	}
	memcpy(pEncHeader->pseudoKey, pseudoKey, FILE_ENC_KEY_LEN+1);
	*/
	_pEncHeader = *pEncHeader;
	
	// 정리 작업
	free(lpEncHeader);
	CloseHandle(hRead);
	return TRUE;
}