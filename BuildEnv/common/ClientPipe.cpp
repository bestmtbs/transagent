

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.08.16		
 ********************************************************************************/

/**
 @file      ClientPipe.cpp
 @brief    Pipe 정의 클래스

 @author    hang ryul lee
 @date      create 2011.08.16
 @note      
*/

#include "stdafx.h"
#include "ClientPipe.h"
#include "WTSSession.h"
#include <tchar.h> // Kevin(2013-4-29)
#include <strsafe.h>// Kevin(2013-4-29)
//#include "dbglog.h"


/**
 @brief     생성자
 @author    hang ryul lee
 @date      2011.08.13
*/
CClientPipe::CClientPipe(void)
{



}

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2011.08.13
*/
CClientPipe::CClientPipe(TCHAR* _pszPipeName)
{
	memset( m_szPipeName, 0x00, 256);

	_tcscpy_s(m_szPipeName,256, _pszPipeName);


}

/**
 @brief      소멸자
 @author    hang ryul lee
 @date      2011.08.13
*/
CClientPipe::~CClientPipe()
{
	if(m_hPipe != NULL)// Kevin(2013-5-27)
	{
		FlushFileBuffers(m_hPipe);
	}

	if(m_hPipe)
	{
		if (INVALID_HANDLE_VALUE != m_hPipe) {
			CloseHandle(m_hPipe);
		}
		m_hPipe = NULL;
	}

}

/**
@fn			CClientPipe::SetPipeName(
				TCHAR* _pszPipeName)ClientPipe.cpp
@brief		파이프명에 SESSION을 적용하기위한 함수 
@param		_pszPipeName
@return		int
@sa			
@callgraph	@date			2013년 4월 29일
*/
int CClientPipe::SetPipeName(TCHAR* _pszPipeName)
{
	memset( m_szPipeName, 0x00, 256);
	CWTSSession wtsession;
	DWORD dwSId = wtsession.GetCurrentLogonSessionID();
	StringCchPrintf(m_szPipeName, 256, L"%s%d", _pszPipeName, dwSId);
//DBGOUT(L"SetPipeName = %s", m_szPipeName);
	return TRUE;
}

/**
 @brief      클라이언트에서 파이프 접속을 위해 사용
 @author    hang ryul lee
 @date      2011.08.13
*/
int  CClientPipe::ClientConnectNamedPipe()
{

	int		nLastError = 0;
	HANDLE hPipe = NULL;

	//DBGLOG(L"PipeName = %s", m_szPipeName);
	while(TRUE)
	{
		m_hPipe = CreateFile(m_szPipeName,
									GENERIC_READ | GENERIC_WRITE,
									0,
									NULL,
									OPEN_EXISTING,
									0,
									NULL);

		// 정상 접속 됨
		if(m_hPipe != INVALID_HANDLE_VALUE)
			break;

		nLastError = GetLastError();

		//  접속 오류
		if(nLastError  != ERROR_PIPE_BUSY)
			return nLastError;

		if(!WaitNamedPipe(m_szPipeName, NMPWAIT_USE_DEFAULT_WAIT))
		{
			nLastError = GetLastError();

			if(nLastError  == ERROR_FILE_NOT_FOUND) 
				return nLastError;

			return -1;
		}

	} // end while(TRUE)


	return 0;
}

/**
 @brief      클라이언트에서 파이프 유효성을 판단하기 위해 사용
 @author    hang ryul lee
 @date      2011.08.13
*/
BOOL	CClientPipe:: IsNamedPipe()
{

	if(GetFileAttributes(m_szPipeName) == (DWORD)INVALID_HANDLE_VALUE)
		return FALSE;

	return TRUE;

}

/**
 @brief      
 @author    hang ryul lee
 @date      2011.08.13
*/
BOOL CClientPipe::SetPipeState ()
{
	BOOL				bRes = 0;
	DWORD			nPipeMode = 0;

	nPipeMode = PIPE_READMODE_MESSAGE | PIPE_WAIT;

	bRes = SetNamedPipeHandleState( m_hPipe, &nPipeMode, NULL, NULL);

	if( bRes != TRUE)
		return GetLastError( );
	
	return bRes;
}

/**
 @brief      
 @author    hang ryul lee
 @date      2011.08.13
*/
BOOL CClientPipe::PipeClientStartUp()
{
	BOOL		bRes = 0;
	int			nRes = 0;
	int			nLastErr = 0;
	HANDLE		hPipe = NULL;
	DWORD		nWriteLen = 0;
	int			nCnt = 0;
	TCHAR		zInBuf[ 4096] = { 0,};
	DWORD		nReadLen = 0;
	//UM_WRITE_LOG(_T("PipeClientStartUp STart.\n")); ////hhh_log
//	DBGLOG(_T("SendLogMessage - PipeClientStartUp() Start..."))
	

SE_LOOP:
	nRes = ClientConnectNamedPipe();

	if(nRes != 0)
	{
		if(nRes == -1)
		{
			// TODO 로그
			// Time Out
			UM_WRITE_LOG(_T("PipeClientStartUp -ClientConnectNamedPipe Time out")); //hhh_log
		}
		else
		{
			// 서버가 아직 안 뜬경우
			if(nRes == ERROR_FILE_NOT_FOUND)
			{
				Sleep(1000);
				nCnt++;

				if(nCnt == 3)
				{
					//TODO 로그
					UM_WRITE_LOG(_T("PipeClientStartUp -ClientConnectNamedPipe - ERROR_FILE_NOT_FOUND")); //hhh_log
					return FALSE;
				}
				goto SE_LOOP;

			}
		}
		return FALSE;
	} // end if(nRes != 0)
	
//	DBGLOG(_T("PipeClientStartUp -SetPipeState Starat..\n")); ////hhh_log
	nRes = SetPipeState();
//	DBGLOG(_T("PipeClientStartUp -SetPipeState end.\n")); ////hhh_log
	if(nRes != 1)
	{
		//TODO 로그
		UM_WRITE_LOG(_T("PipeClientStartUp -SetPipeState Fail..\n")); ////hhh_log
		CloseHandle(m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}
/***************************** 가상 함수 *********************************/
//	DBGLOG(_T("PipeClientStartUp -OnSendData start.\n")); ////hhh_log
	if(FALSE == OnSendData())
	{	
		if( GetLastError() == 233)	//한쪽끝에 프로세스가 없는경우..senddata는 성공했으므로 이경우에는 정상적으로 처리가 되었다고 일단 판단. 추후 개선시 이부분 수정되어야 할듯
		{
			UM_WRITE_LOG(_T("PipeClientStartUp - GetLastError() == 233.\n")); ////hhh_log
			if(m_hPipe)
			{
				CloseHandle(m_hPipe);	
				m_hPipe = NULL;
			}
			return TRUE;
		}
		else
		{
			UM_WRITE_LOG(_T("PipeClientStartUp -OnSendData return FALSE.\n")); ////hhh_log
			CloseHandle(m_hPipe);
			m_hPipe = NULL;
			return FALSE;
		}							
	}


/**************************** 가상 함수 ************************************/
//	DBGLOG(_T("PipeClientStartUp -OnReceiveData start.\n")); ////hhh_log
	if(FALSE == OnReceiveData())
	{		
		if( GetLastError() == 233)	//한쪽끝에 프로세스가 없는경우..senddata는 성공했으므로 이경우에는 정상적으로 처리가 되었다고 일단 판단. 추후 개선시 이부분 수정되어야 할듯
		{
			UM_WRITE_LOG(_T("PipeClientStartUp - GetLastError() == 233.\n")); ////hhh_log
			if(m_hPipe)
			{
				CloseHandle(m_hPipe);	
				m_hPipe = NULL;
			}
			return TRUE;
		}
		else
		{
			UM_WRITE_LOG(_T("PipeClientStartUp -OnReceiveData return FALSE.\n")); ////hhh_log
			CloseHandle(m_hPipe);
			m_hPipe = NULL;
			return FALSE;
		}							
	}

	//DBGLOG(_T("PipeClientStartUp return TRUE..End.\n")); ////hhh_log
	if(m_hPipe)
	{
		CloseHandle(m_hPipe);	
		m_hPipe = NULL;
	}
		

	
	return TRUE;
}


/**
 @brief      
 @author    hang ryul lee
 @date      2011.08.13
*/
BOOL CClientPipe::PipeClientNewStartUp()
{
	BOOL		bRes = 0;
	int			nRes = 0;
	int			nLastErr = 0;
	HANDLE		hPipe = NULL;
	DWORD		nWriteLen = 0;
	int			nCnt = 0;
	TCHAR		zInBuf[ 4096] = { 0,};
	DWORD		nReadLen = 0;


SE_LOOP:
	nRes = ClientConnectNamedPipe();

	if(nRes != 0)
	{
		if(nRes == -1)
		{
			// TODO 로그
			// Time Out
		}
		else
		{
			// 서버가 아직 안 뜬경우
			if(nRes == ERROR_FILE_NOT_FOUND)
			{
				Sleep(1000);
				nCnt++;

				if(nCnt == 3)
				{
					//TODO 로그
					return FALSE;
				}
				goto SE_LOOP;

			}
		}
		return FALSE;
	} // end if(nRes != 0)
	
	nRes = SetPipeState();

	if(nRes != 1)
	{
		//TODO 로그
		CloseHandle(m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}
/***************************** 가상 함수 *********************************/

	if(FALSE == OnSendHeader())
	{
		CloseHandle( m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}


	if(FALSE == OnSendBody())
	{
		CloseHandle( m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}



/**************************** 가상 함수 ************************************/

	if(FALSE == OnReceiveData())
	{
		CloseHandle( m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}

	FlushFileBuffers( m_hPipe);
	CloseHandle( m_hPipe);
	m_hPipe = NULL;

	return TRUE;
}


/**ClientPipe.cpp
@fn			CClientPipe::PipeClientStartUp2()
@brief		
@return		BOOL
@sa			
@callgraph	@date			2013년 4월 22일
*/
BOOL CClientPipe::PipeClientStartUp2()
{
	BOOL		bRes = 0;
	int			nRes = 0;
	int			nLastErr = 0;
	HANDLE		hPipe = NULL;
	DWORD		nWriteLen = 0;
	int			nCnt = 0;
	TCHAR		zInBuf[ 4096] = { 0,};
	DWORD		nReadLen = 0;

	nRes = ClientConnectNamedPipe();
	if(nRes != 0)
	{
		if(nRes == -1)
		{
			// TODO 로그
			// Time Out
		}
		else
		{
			// 서버가 아직 안 뜬경우
			if(nRes == ERROR_FILE_NOT_FOUND)
			{
				//TODO 로그
				return FALSE;
			}
		}
		return FALSE;
	} // end if(nRes != 0)
	
	nRes = SetPipeState();

	if(nRes != 1)
	{
		//TODO 로그
		CloseHandle(m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}
/***************************** 가상 함수 *********************************/

	if(FALSE == OnSendHeader())
	{
		CloseHandle( m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}


	if(FALSE == OnSendBody())
	{
		CloseHandle( m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}



/**************************** 가상 함수 ************************************/

	if(FALSE == OnReceiveData())
	{
		CloseHandle( m_hPipe);
		return FALSE;
	}

	FlushFileBuffers( m_hPipe);


	CloseHandle( m_hPipe);
	m_hPipe = NULL;

	return TRUE;
}
/**
 @brief      
 @author    hang ryul lee
 @date      2011.08.13
*/
DWORD CClientPipe::GetFunctionErrorCode()
{
	return m_dwErrorCode;
}

/**
 @brief      
 @author    hang ryul lee
 @date      2011.08.13
*/
void CClientPipe::SetFunctionErrorCode(DWORD dwError)
{
	m_dwErrorCode = dwError;
}

/**ClientPipe.cpp
@fn			CClientPipe::PipeClientStartUpForUserScan()
@brief		DlgUserScan의 ScanRequest()에서 senddata의 getLastError가 2(ERROR_FILE_NOT_FOUND)일 경우 TRUE로 리턴 하도록 PipeClientStartUp()에추가
@return		BOOL
@sa			
@callgraph	@date			2016년 7월 18일
*/
BOOL CClientPipe::PipeClientStartUpForUserScan()
{
	int			nRes = 0;
	int			nLastErr = 0;
	HANDLE		hPipe = NULL;
	DWORD		nWriteLen = 0;
	int			nCnt = 0;
	TCHAR		zInBuf[ 4096] = { 0,};
	DWORD		nReadLen = 0;
	//UM_WRITE_LOG(_T("PipeClientStartUp STart.\n")); ////hhh_log
	//	DBGLOG(_T("SendLogMessage - PipeClientStartUp() Start..."))


SE_LOOP:
	nRes = ClientConnectNamedPipe();

	if(nRes != 0)
	{
		if(nRes == -1)
		{
			// TODO 로그
			// Time Out
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan -ClientConnectNamedPipe Time out")); //hhh_log
		}
		else
		{
			// 서버가 아직 안 뜬경우
			if(nRes == ERROR_FILE_NOT_FOUND)
			{
				Sleep(1000);
				nCnt++;

				if(nCnt == 3)
				{
					//TODO 로그
					UM_WRITE_LOG(_T("PipeClientStartUpForUserScan -ClientConnectNamedPipe - ERROR_FILE_NOT_FOUND")); //hhh_log
					return FALSE;
				}
				goto SE_LOOP;

			}
		}
		return FALSE;
	} // end if(nRes != 0)

	//	DBGLOG(_T("PipeClientStartUp -SetPipeState Starat..\n")); ////hhh_log
	nRes = SetPipeState();
	//	DBGLOG(_T("PipeClientStartUp -SetPipeState end.\n")); ////hhh_log
	if(nRes != 1)
	{
		//TODO 로그
		UM_WRITE_LOG(_T("PipeClientStartUpForUserScan -SetPipeState Fail..\n")); ////hhh_log
		CloseHandle(m_hPipe);
		m_hPipe = NULL;
		return FALSE;
	}
	/***************************** 가상 함수 *********************************/
	//	DBGLOG(_T("PipeClientStartUp -OnSendData start.\n")); ////hhh_log
	if(FALSE == OnSendData())
	{
		nLastErr = GetLastError();
		if(233 == nLastErr)	//한쪽끝에 프로세스가 없는경우..senddata는 성공했으므로 이경우에는 정상적으로 처리가 되었다고 일단 판단. 추후 개선시 이부분 수정되어야 할듯
		{
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan - GetLastError() == 233.\n")); ////hhh_log
			if(m_hPipe)
			{
				CloseHandle(m_hPipe);	
				m_hPipe = NULL;
			}
			return TRUE;
		} else if(2 == nLastErr)	//한쪽끝에 프로세스가 없는경우..senddata는 성공했으므로 이경우에는 정상적으로 처리가 되었다고 일단 판단. 추후 개선시 이부분 수정되어야 할듯
		{
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan - GetLastError() == 2.\n")); ////hhh_log
			if(m_hPipe)
			{
				CloseHandle(m_hPipe);	
				m_hPipe = NULL;
			}
			return TRUE;
		} else
		{
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan -OnSendData return FALSE.\n")); ////hhh_log
			CloseHandle(m_hPipe);
			m_hPipe = NULL;
			return FALSE;
		}							
	}


	/**************************** 가상 함수 ************************************/
	//	DBGLOG(_T("PipeClientStartUp -OnReceiveData start.\n")); ////hhh_log
	if(FALSE == OnReceiveData())
	{
		nLastErr = GetLastError();	// 2017-02-23 kh.choi == 로 되어있어서, = 로 수정
		if(233 == nLastErr)	//한쪽끝에 프로세스가 없는경우..senddata는 성공했으므로 이경우에는 정상적으로 처리가 되었다고 일단 판단. 추후 개선시 이부분 수정되어야 할듯
		{
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan - GetLastError() == 233.\n")); ////hhh_log
			if(m_hPipe)
			{
				CloseHandle(m_hPipe);	
				m_hPipe = NULL;
			}
			return TRUE;
		} else if(2 == nLastErr)	//한쪽끝에 프로세스가 없는경우..senddata는 성공했으므로 이경우에는 정상적으로 처리가 되었다고 일단 판단. 추후 개선시 이부분 수정되어야 할듯
		{
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan - GetLastError() == 2.\n")); ////hhh_log
			if(m_hPipe)
			{
				CloseHandle(m_hPipe);	
				m_hPipe = NULL;
			}
			return TRUE;
		} else
		{
			UM_WRITE_LOG(_T("PipeClientStartUpForUserScan -OnReceiveData return FALSE. %d \n")); ////hhh_log
			CloseHandle(m_hPipe);
			m_hPipe = NULL;
			return FALSE;
		}							
	}

	//DBGLOG(_T("PipeClientStartUp return TRUE..End.\n")); ////hhh_log
	if(m_hPipe)
	{
		CloseHandle(m_hPipe);	
		m_hPipe = NULL;
	}



	return TRUE;
}