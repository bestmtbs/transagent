
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      CryptBase64.h 
 @brief     CCryptBase64 Class 정의 파일

            Base64 Encoding/Decoding 기능을 구현하였음.\n
            ISMS 3.0/3.5에서 사용한 DCPcrypt v2.0의 DCPBase64 소스를 
            기반으로 작성하였음.
            http://www.cityinthesky.co.uk/cryptography.html

 @author    hang ryul lee
 @date      create 2011.07.21 
*/

#pragma once

class CCryptBase64
{
public:
    CCryptBase64();
    virtual ~CCryptBase64();

    DWORD Encode(const PVOID _pInput, PVOID _pOutput, DWORD _dwSize);
    DWORD Decode(const PVOID _pInput, PVOID _pOutput, DWORD _dwSize);
    CString EncodeStr(const CString &_sValue);
    CString DecodeStr(const CString &_sValue);
    size_t GetEncodeBufferSize(DWORD _dwSize) const;
    size_t GetDecodeBufferSize(DWORD _dwSize) const;
private:
    static const BYTE B64[64];  /**< */
};