
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      RunRegistry.cpp
 @brief     LOCAL_MACHINE의 Run, RunOnce, RunOnceEx, RunServices 를 관리하는 
            Class 구현 파일 (자동 시작 프로그램)
 @author    hang ryul lee
 @date      create 2011.04.15 
*/

#include "StdAfx.h"
#include "RunRegistry.h"
#include "UtilsReg.h"

#define REG_KEY_RUN 	        _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run")	                               
#define REG_KEY_RUNONCE     	_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce")
#define REG_KEY_RUNONCEEX   	_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx")
#define REG_KEY_RUNSERVICES 	_T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices")

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2008.04.15
*/
CRunRegistry::CRunRegistry(void)
{
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2008.04.15
*/
CRunRegistry::~CRunRegistry(void)
{
}

/**
 @brief     Run 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRun(const HKEY _hRoot, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = SetRegStringValue(_hRoot, REG_KEY_RUN, _sValName, _sValue);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunOnce 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRunOnce(const HKEY _hRoot, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = SetRegStringValue(_hRoot, REG_KEY_RUNONCE, _sValName, _sValue);
	}
	catch(...)
	{
	}
	return bResult; 

}

/**
 @brief     RunOnceEx 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @note      RunOnceEx 는 자동프로그램 등록시 키를 따로 생성하고 생성된 키 안에서 실행프로그램의 경로를
            문자열 값으로 등록하는 방식이다. 그러므로 생성할 키를 파라미터로 등록 받아야 한다.
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRunOnceEx(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;
	
	CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey + _T("\\") +_sKey ;

	try
	{
		bResult = SetRegStringValue(_hRoot, sKey, _sValName, _sValue, true);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunServices 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRunServices(const HKEY _hRoot, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = SetRegStringValue(_hRoot, REG_KEY_RUNSERVICES, _sValName, _sValue, true);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     Run 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false		 Value
*/
bool CRunRegistry::DeleteRun(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = DelRegValue(_hRoot, REG_KEY_RUN, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunOnce 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false         
*/
bool CRunRegistry::DeleteRunOnce(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = DelRegValue(_hRoot, REG_KEY_RUNONCE, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunOnceEx 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sKey       [in]  Key
 @return	true / false        
*/
bool CRunRegistry::DeleteRunOnceEx(const HKEY _hRoot, const CString &_sKey)
{
	bool bResult = false;

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;

    CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey + _T("\\") +_sKey ;

	try
	{
		bResult = DelRegKey(_hRoot, sKey);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunServices 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false		 
*/
bool CRunRegistry::DeleteRunServices(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = DelRegValue(_hRoot, REG_KEY_RUNSERVICES, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     Run에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunValueExists(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = IsRegValueExists(_hRoot, REG_KEY_RUN, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunOnce에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_hRoot     [in]  Root Key
			_sValName  [in]	 Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunOnceValueExists(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = IsRegValueExists(_hRoot, REG_KEY_RUNONCE , _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunOnceEx에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sKey       [in]  Key
			_sValName	[in]  Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunOnceExValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName)
{
	bool bResult = false;

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;

	CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey +_T("\\") +_sKey;
	try
	{
		bResult = IsRegValueExists(_hRoot, sKey, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     RunOnceServices에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunServicesValueExist(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = IsRegValueExists(_hRoot, REG_KEY_RUNSERVICES, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     Run에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false	    
*/
CString CRunRegistry::GetRunValue(const HKEY _hRoot, const CString &_sValName)
{
	CString sResult = _T("");
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;

	try
	{
		sResult = GetRegStringValue(_hRoot, REG_KEY_RUN, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}

/**
 @brief     RunOnce에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_hRoot     [in]  Root Key
			_sValName[in]	 Value Name
 @return	true / false	    
*/
CString CRunRegistry::GetRunOnceValue(const HKEY _hRoot, const CString &_sValName)
{
	CString sResult = _T("");
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;
	try
	{
		sResult = GetRegStringValue(_hRoot, REG_KEY_RUNONCE, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}

/**
 @brief     RunOnceEx에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sKey       [in]  Key
			_sValName	[in]  Value Name			
 @return	true / false	    
*/
CString CRunRegistry::GetRunOnceExValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName)
{
	CString sResult = _T("");

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;

	CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey +_T("\\") +_sKey ;

	try
	{
		sResult = GetRegStringValue(_hRoot, sKey, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}

/**
 @brief     RunServices에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false	    
*/
CString CRunRegistry::GetRunServicesValue(const HKEY _hRoot, const CString &_sValName) 
{
	CString sResult = _T("");
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;

	try
	{
		sResult = GetRegStringValue(_hRoot, REG_KEY_RUNSERVICES, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}


//Wow 함수 추가

/**
 @brief     64비트 환경에서 32비트 Run 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRunWow64(const HKEY _hRoot, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = SetRegStringWow64Value(_hRoot, REG_KEY_RUN, _sValName, _sValue);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnce 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRunOnceWow64(const HKEY _hRoot, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = SetRegStringWow64Value(_hRoot, REG_KEY_RUNONCE, _sValName, _sValue);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnceEx 키에 자동시작 프로그램을 등록한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @note      RunOnceEx 는 자동프로그램 등록시 키를 따로 생성하고 생성된 키 안에서 실행프로그램의 경로를
            문자열 값으로 등록하는 방식이다. 그러므로 생성할 키를 파라미터로 등록 받아야 한다.
 @param		_sValName	[in]	 Value Name
			_sValue		[in]	 Value
 @return	true / false
*/
bool CRunRegistry::SetRunOnceExWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue)
{
	bool bResult = false;

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;
	
	CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey + _T("\\") +_sKey ;

	try
	{
		bResult = SetRegStringWow64Value(_hRoot, sKey, _sValName, _sValue);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     64비트 환경에서 32비트 Run 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false		 Value
*/
bool CRunRegistry::DeleteRunWow64(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = DelRegWow64Value(_hRoot, REG_KEY_RUN, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnce 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false         
*/
bool CRunRegistry::DeleteRunOnceWow64(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = DelRegWow64Value(_hRoot, REG_KEY_RUNONCE, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnceEx 키에 등록된 자동시작 프로그램을 삭제한다
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sKey       [in]  Key
 @return	true / false        
*/
bool CRunRegistry::DeleteRunOnceExWow64(const HKEY _hRoot, const CString &_sKey)
{
	bool bResult = false;

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;

	try
	{
		bResult = DelRegWow64Key(_hRoot, REG_KEY_RUNONCEEX, _sKey);
	}
	catch(...)
	{
	}
	return bResult; 
}



/**
 @brief     64비트 환경에서 32비트 Run에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunWow64ValueExists(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = IsRegWow64ValueExists(_hRoot, REG_KEY_RUN, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnce에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_hRoot     [in]  Root Key
			_sValName  [in]	 Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunOnceWow64ValueExists(const HKEY _hRoot, const CString &_sValName)
{
	bool bResult = false;
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;

	try
	{
		bResult = IsRegWow64ValueExists(_hRoot, REG_KEY_RUNONCE , _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}


/**
 @brief     64비트 환경에서 32비트 RunOnceEx에 등록된 Value의 존재유무 확인
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sKey       [in]  Key
			_sValName	[in]  Value Name
 @return	true / false	    
*/
bool CRunRegistry::IsRunOnceExWow64ValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName)
{
	bool bResult = false;

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return false;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;

	CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey +_T("\\") +_sKey;
	try
	{
		bResult = IsRegWow64ValueExists(_hRoot, sKey, _sValName);
	}
	catch(...)
	{
	}
	return bResult; 
}
/**
 @brief     64비트 환경에서 32비트 Run에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sValName	[in]	 Value Name
 @return	true / false	    
*/
CString CRunRegistry::GetRunWow64Value(const HKEY _hRoot, const CString &_sValName)
{
	CString sResult = _T("");
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;

	try
	{
		sResult = GetRegStringWow64Value(_hRoot, REG_KEY_RUN, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnce에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_hRoot     [in]  Root Key
			_sValName  [in]	 Value Name
 @return	true / false	    
*/
CString CRunRegistry::GetRunOnceWow64Value(const HKEY _hRoot, const CString &_sValName)
{
	CString sResult = _T("");
	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;
	try
	{
		sResult = GetRegStringWow64Value(_hRoot, REG_KEY_RUNONCE, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}

/**
 @brief     64비트 환경에서 32비트 RunOnceEx에 등록된 Value Name을 이용하여 Value를 구한다.
 @author    hang ryul lee
 @date      create 2011.04.15
 @param		_sKey       [in]  Key
			_sValName	[in]  Value Name			
 @return	true / false	    
*/
CString CRunRegistry::GetRunOnceWow64ExValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName)
{
	CString sResult = _T("");

	if ((HKEY_LOCAL_MACHINE != _hRoot) && (HKEY_CURRENT_USER != _hRoot)) return sResult;
	if (MAX_KEYNAME_LEN < _sKey.GetLength()) return false;
	if (_sKey == _T("")) return false;

	CString sKey = REG_KEY_RUNONCEEX;
	sKey = sKey +_T("\\") +_sKey ;

	try
	{
		sResult = GetRegStringWow64Value(_hRoot, sKey, _sValName);
	}
	catch(...)
	{
	}
	return sResult; 
}

