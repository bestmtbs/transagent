/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/

#include "hash_global.h"
#include "hash.h"
#include "hash_md5.h"



#define EV_MD5_DIGEST_LENGTH 16

#define DK_MD5_F(x, y, z)	(((x) & (y)) | ((~x) & (z)))
#define DK_MD5_G(x, y, z)	(((x) & (z)) | ((y) & (~z)))
#define DK_MD5_H(x, y, z)	((x) ^ (y) ^ (z))
#define DK_MD5_I(x, y, z)	((y) ^ ((x) | (~z)))

#define DK_MD5_FF(a, b, c, d, x, s, ac) {							\
	(a) += DK_MD5_F( (b), (c), (d) ) + (x) + (unsigned int)(ac);	        \
	(a) = (ROTL32((a) & MASK32, (s)) + (b)) & MASK32;				\
}

#define DK_MD5_GG(a, b, c, d, x, s, ac) {					        \
	(a) += DK_MD5_G( (b), (c), (d) ) + (x) + (unsigned int)(ac);         \
	(a) = (ROTL32((a) & MASK32, (s)) + (b)) & MASK32;				\
}

#define DK_MD5_HH(a, b, c, d, x, s, ac) {						    \
	(a) += DK_MD5_H( (b), (c), (d) ) + (x) + (unsigned int)(ac);         \
	(a) = (ROTL32((a) & MASK32, (s))+(b)) & MASK32;					\
}

#define DK_MD5_II(a, b, c, d, x, s, ac) {					        \
	(a) += DK_MD5_I( (b), (c), (d) ) + (x) + (unsigned int)(ac);         \
	(a) = (ROTL32((a) & MASK32, (s))+(b)) & MASK32;					\
}


void
CmsMd5Transform	(
				 unsigned int *		pState,		// [O]

				unsigned char *		pInput		// [I]
)
{
	unsigned int			a, b, c, d, x[16];
	int						nIdx;
	
	a = pState[0];
	b = pState[1];
	c = pState[2];
	d = pState[3];

	for (nIdx = 0; nIdx < 16; nIdx++ )
		x[nIdx] = (((unsigned int )pInput[4 * nIdx] )
		      | (((unsigned int )pInput[4 * nIdx + 1] ) << 8 )
			  |	(((unsigned int )pInput[4 * nIdx + 2] ) << 16 )
			  | (((unsigned int )pInput[4 * nIdx + 3] ) << 24 ) );

	/* Round 1*/
	DK_MD5_FF (a, b, c, d, x[ 0],  7, 0xd76aa478); /* 1 */
	DK_MD5_FF (d, a, b, c, x[ 1], 12, 0xe8c7b756); /* 2 */
	DK_MD5_FF (c, d, a, b, x[ 2], 17, 0x242070db); /* 3 */
	DK_MD5_FF (b, c, d, a, x[ 3], 22, 0xc1bdceee); /* 4 */
	DK_MD5_FF (a, b, c, d, x[ 4],  7, 0xf57c0faf); /* 5 */
	DK_MD5_FF (d, a, b, c, x[ 5], 12, 0x4787c62a); /* 6 */
	DK_MD5_FF (c, d, a, b, x[ 6], 17, 0xa8304613); /* 7 */
	DK_MD5_FF (b, c, d, a, x[ 7], 22, 0xfd469501); /* 8 */
	DK_MD5_FF (a, b, c, d, x[ 8],  7, 0x698098d8); /* 9 */
	DK_MD5_FF (d, a, b, c, x[ 9], 12, 0x8b44f7af); /* 10 */
	DK_MD5_FF (c, d, a, b, x[10], 17, 0xffff5bb1); /* 11 */
	DK_MD5_FF (b, c, d, a, x[11], 22, 0x895cd7be); /* 12 */
	DK_MD5_FF (a, b, c, d, x[12],  7, 0x6b901122); /* 13 */
	DK_MD5_FF (d, a, b, c, x[13], 12, 0xfd987193); /* 14 */
	DK_MD5_FF (c, d, a, b, x[14], 17, 0xa679438e); /* 15 */
	DK_MD5_FF (b, c, d, a, x[15], 22, 0x49b40821); /* 16 */
	
	/* Round 2 */
	DK_MD5_GG (a, b, c, d, x[ 1],  5, 0xf61e2562); /* 17 */
	DK_MD5_GG (d, a, b, c, x[ 6],  9, 0xc040b340); /* 18 */
	DK_MD5_GG (c, d, a, b, x[11], 14, 0x265e5a51); /* 19 */
	DK_MD5_GG (b, c, d, a, x[ 0], 20, 0xe9b6c7aa); /* 20 */
	DK_MD5_GG (a, b, c, d, x[ 5],  5, 0xd62f105d); /* 21 */
	DK_MD5_GG (d, a, b, c, x[10],  9,  0x2441453); /* 22 */
	DK_MD5_GG (c, d, a, b, x[15], 14, 0xd8a1e681); /* 23 */
	DK_MD5_GG (b, c, d, a, x[ 4], 20, 0xe7d3fbc8); /* 24 */
	DK_MD5_GG (a, b, c, d, x[ 9],  5, 0x21e1cde6); /* 25 */
	DK_MD5_GG (d, a, b, c, x[14],  9, 0xc33707d6); /* 26 */
	DK_MD5_GG (c, d, a, b, x[ 3], 14, 0xf4d50d87); /* 27 */
	DK_MD5_GG (b, c, d, a, x[ 8], 20, 0x455a14ed); /* 28 */
	DK_MD5_GG (a, b, c, d, x[13],  5, 0xa9e3e905); /* 29 */
	DK_MD5_GG (d, a, b, c, x[ 2],  9, 0xfcefa3f8); /* 30 */
	DK_MD5_GG (c, d, a, b, x[ 7], 14, 0x676f02d9); /* 31 */
	DK_MD5_GG (b, c, d, a, x[12], 20, 0x8d2a4c8a); /* 32 */
	
	/* Round 3 */
	DK_MD5_HH (a, b, c, d, x[ 5],  4, 0xfffa3942); /* 33 */
	DK_MD5_HH (d, a, b, c, x[ 8], 11, 0x8771f681); /* 34 */
	DK_MD5_HH (c, d, a, b, x[11], 16, 0x6d9d6122); /* 35 */
	DK_MD5_HH (b, c, d, a, x[14], 23, 0xfde5380c); /* 36 */
	DK_MD5_HH (a, b, c, d, x[ 1],  4, 0xa4beea44); /* 37 */
	DK_MD5_HH (d, a, b, c, x[ 4], 11, 0x4bdecfa9); /* 38 */
	DK_MD5_HH (c, d, a, b, x[ 7], 16, 0xf6bb4b60); /* 39 */
	DK_MD5_HH (b, c, d, a, x[10], 23, 0xbebfbc70); /* 40 */
	DK_MD5_HH (a, b, c, d, x[13],  4, 0x289b7ec6); /* 41 */
	DK_MD5_HH (d, a, b, c, x[ 0], 11, 0xeaa127fa); /* 42 */
	DK_MD5_HH (c, d, a, b, x[ 3], 16, 0xd4ef3085); /* 43 */
	DK_MD5_HH (b, c, d, a, x[ 6], 23,  0x4881d05); /* 44 */
	DK_MD5_HH (a, b, c, d, x[ 9],  4, 0xd9d4d039); /* 45 */
	DK_MD5_HH (d, a, b, c, x[12], 11, 0xe6db99e5); /* 46 */
	DK_MD5_HH (c, d, a, b, x[15], 16, 0x1fa27cf8); /* 47 */
	DK_MD5_HH (b, c, d, a, x[ 2], 23, 0xc4ac5665); /* 48 */

	/* Round 4 */
	DK_MD5_II (a, b, c, d, x[ 0],  6, 0xf4292244); /* 49 */  
	DK_MD5_II (d, a, b, c, x[ 7], 10, 0x432aff97); /* 50 */
	DK_MD5_II (c, d, a, b, x[14], 15, 0xab9423a7); /* 51 */
	DK_MD5_II (b, c, d, a, x[ 5], 21, 0xfc93a039); /* 52 */
	DK_MD5_II (a, b, c, d, x[12],  6, 0x655b59c3); /* 53 */
	DK_MD5_II (d, a, b, c, x[ 3], 10, 0x8f0ccc92); /* 54 */
	DK_MD5_II (c, d, a, b, x[10], 15, 0xffeff47d); /* 55 */
	DK_MD5_II (b, c, d, a, x[ 1], 21, 0x85845dd1); /* 56 */
	DK_MD5_II (a, b, c, d, x[ 8],  6, 0x6fa87e4f); /* 57 */
	DK_MD5_II (d, a, b, c, x[15], 10, 0xfe2ce6e0); /* 58 */
	DK_MD5_II (c, d, a, b, x[ 6], 15, 0xa3014314); /* 59 */
	DK_MD5_II (b, c, d, a, x[13], 21, 0x4e0811a1); /* 60 */
	DK_MD5_II (a, b, c, d, x[ 4],  6, 0xf7537e82); /* 61 */
	DK_MD5_II (d, a, b, c, x[11], 10, 0xbd3af235); /* 62 */
	DK_MD5_II (c, d, a, b, x[ 2], 15, 0x2ad7d2bb); /* 63 */
	DK_MD5_II (b, c, d, a, x[ 9], 21, 0xeb86d391); /* 64 */

	pState[0] += a;
	pState[1] += b;
	pState[2] += c;
	pState[3] += d;

  /* Zeroize sensitive information. */
   memset (x, 0, 64 );

}

void
CmsMd5Init (
			HASH_CTX *		psCtx
)
{
	psCtx->nDataLen[ 0] = psCtx->nDataLen[1] = 0L;

	psCtx->caStat[ 0] = 0x67452301;
	psCtx->caStat[ 1] = 0xefcdab89;
	psCtx->caStat[ 2] = 0x98badcfe;
	psCtx->caStat[ 3] = 0x10325476;

	
}

void
CmsMd5Update (
			  HASH_CTX *		psCtx,				// [O]

			  unsigned char *			pInput,				// [I]
			  unsigned int			nInputByteLength	// [I]
)
{
	unsigned int			nIdx = 0;
	unsigned int			nIndex = 0;
	unsigned int			nPartLength = 0;



	nIndex = (unsigned int)((psCtx->nDataLen[ 0] >> 3 ) & 0x3f );

	if ((psCtx->nDataLen[0] += (nInputByteLength << 3 ) ) < (nInputByteLength << 3 ) )
		psCtx->nDataLen[1]++;
	
	psCtx->nDataLen[1] += (nInputByteLength >> 29 );
	nPartLength = 64 - nIndex;

	if (nInputByteLength >= nPartLength ) {

		memcpy(psCtx->caBuf + nIndex, pInput, nPartLength );

		CmsMd5Transform( psCtx->caStat, psCtx->caBuf);

		for (nIdx = nPartLength; nIdx + 63 < nInputByteLength; nIdx += 64)

			CmsMd5Transform( psCtx->caStat,	pInput + nIdx);

		nIndex = 0;
	}
	else
		nIdx = 0;

	memcpy(psCtx->caBuf + nIndex, pInput + nIdx, nInputByteLength - nIdx );

}


void
CmsMd5Final	(
		  	 HASH_CTX *		psCtx,			// [I/O]

			 unsigned char *			pHashValue		// [O]
)
{
	unsigned char			sBits[8] = {0,};
	unsigned char			sPad[64] = {
							 0x80, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0, 
								0, 0, 0, 0, 0, 0, 0, 0
							};
	unsigned int					nIndex = 0;
	unsigned int					nPadLength = 0;
	int						nIdx = 0;
	
	for (nIdx = 0; nIdx < 8; nIdx++ )
		sBits[nIdx] = (unsigned char )(psCtx->nDataLen[nIdx / 4] >> ((nIdx & 3 ) << 3 ) );
	
	nIndex = (psCtx->nDataLen[0] >> 3 ) & 0x3f;
	nPadLength = (nIndex < 56 ) ? (56 - nIndex ) : (120 - nIndex );
	
	CmsMd5Update( psCtx, sPad, nPadLength);

	CmsMd5Update( psCtx, sBits, 8);

	for (nIdx = 0; nIdx < 16; nIdx++ )
		pHashValue[nIdx] = (unsigned char )(psCtx->caStat[nIdx >> 2] >> ((nIdx & 3 ) << 3 ) );

	memset(psCtx->caBuf, 0, 64 );
	memset(psCtx->caStat, 0, 24 );
}

void
CmsMd5 (
		unsigned char *			pHashValue,			// [O]
		unsigned char *			pInput,				// [I]
		unsigned int				nInputByteLength	// [I]
)
{
	HASH_CTX *				psCtx = NULL;


	psCtx = /*ExAllocatePool(NonPagedPool ,sizeof(DK_HASH_CTX ) );*/calloc(1,sizeof(HASH_CTX ) );
	memset(psCtx,0x00,sizeof(HASH_CTX ) );
	CmsMd5Init( psCtx);

	CmsMd5Update( psCtx, pInput, nInputByteLength);

	CmsMd5Final( psCtx,	pHashValue);

	/*ExFreePool(psCtx);*/
	free( psCtx);

}
