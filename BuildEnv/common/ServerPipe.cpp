
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.08.16		
 ********************************************************************************/

/**
 @file      ServerPipe.cpp
 @brief    ServerPipe 정의 클래스

 @author    hang ryul lee
 @date      create 2011.08.16
 @note      
*/

#include "stdafx.h"
#include <tchar.h>// Kevin(2013-5-29)
#include <strsafe.h>// Kevin(2013-5-29)
#include "ServerPipe.h"


/**
 @brief     생성자
 @author    hang ryul lee
 @date      2011.08.13
*/
CServerPipe::CServerPipe(void)
{
	m_bPipeServerExit = FALSE;  //hhh(2013.06.05)
}

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2011.08.13
*/
CServerPipe::CServerPipe(TCHAR* _pszPipeName)
{
	m_bPipeServerExit = FALSE; //hhh(2013.06.05)
	m_hPipe = NULL;
	memset( m_szPipeName, 0x00, 256);
	_tcscpy_s(m_szPipeName,sizeof(m_szPipeName), _pszPipeName);
	m_hPipeEvent = NULL;

	CString strTemp;		//hhh(2013.06.12) : pipe명을 가지고 event만들때 개행문자 처리(win7 홈에서 지정된 경로를 못찾는다는 에러 발생)
	strTemp.Format(L"%s", _pszPipeName);
	strTemp.Replace(L"\\", L"");

	WCHAR strPipeEvent[MAX_PATH];
	//StringCchPrintf(strPipeEvent, MAX_PATH, L"%s_{C8CC6042-EEA4-4a08-BF6B-89167D2F1164}", _pszPipeName);
	StringCchPrintf(strPipeEvent, MAX_PATH, L"%s_{C8CC6042-EEA4-4a08-BF6B-89167D2F1164}", strTemp);
	
	strTemp.Format(L"CreateEvent %s", strPipeEvent);
	//UM_WRITE_LOG(strTemp);
	m_hPipeEvent = CreateEvent(NULL, FALSE, FALSE, strPipeEvent);

	if(m_hPipeEvent == NULL)
	{
		CString strLog;
		strLog.Format(L"[Pipe Error] :(%s) ErrorCode- %d", strPipeEvent, GetLastError());
		//UM_WRITE_LOG(strLog);
	}

	WCHAR strPipeNewEvet[MAX_PATH];
	//StringCchPrintf(strPipeEvent, MAX_PATH, L"%s_{C8CC6042-EEA4-4a08-BF6B-89167D2F1164}", _pszPipeName);
	StringCchPrintf(strPipeEvent, MAX_PATH, L"%s_{C8CC6042-EEA4-4a08-BF6B-89167D2F1164}", strTemp);
	
	m_hPipeNewEvent = CreateEvent(NULL, FALSE, FALSE, strPipeNewEvet);
	
	if(m_hPipeNewEvent == NULL)
	{
		CString strLog;
		strLog.Format(L"[Pipe Error new] :(%s) ErrorCode- %d", strPipeNewEvet, GetLastError());
		//UM_WRITE_LOG(strLog);
	}

}

/**
 @brief      소멸자
 @author    hang ryul lee
 @date      2011.08.13
*/
CServerPipe::~CServerPipe()
{
	// Kevin(2013-5-27)
	if(m_hPipeEvent != NULL)
	{
		//UM_WRITE_LOG(L"SetEvent(m_hPipeEvent)");
		SetEvent(m_hPipeEvent);
		Sleep(1000);
		m_hPipeEvent = NULL;
	}
	if(m_hPipeNewEvent != NULL)
	{
		SetEvent(m_hPipeNewEvent);
		Sleep(1000);
		m_hPipeNewEvent = NULL;
	}
}

/**
 @brief      파이프 서버에서 파이프 초기화를 위해 사용
 @author    hang ryul lee
 @date      2011.08.13
*/
int CServerPipe::InitializeNamedPipe()
{
//	BOOL			bRes = 0;
//	int				nLastError = 0;
//	HANDLE		hPipe = NULL;
	BYTE sd[SECURITY_DESCRIPTOR_MIN_LENGTH];
	SECURITY_ATTRIBUTES sa;

	sa.nLength = sizeof(sa);
	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = &sd;

	InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
	SetSecurityDescriptorDacl(&sd, TRUE, (PACL) 0, FALSE);


	m_hPipe = CreateNamedPipe(m_szPipeName,																				//	pipe name
										  PIPE_ACCESS_DUPLEX,								// read / write access
										  PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,       // message-type pipe, message read mode, blocking mode
										  PIPE_UNLIMITED_INSTANCES,																						// instance 갯수
											sizeof(SHARE_THUMBNAIL_DATA),	// output buffer size
											sizeof(SHARE_THUMBNAIL_DATA),	// input buffer size
										  PIPE_TIME_WAIT,                                                                            // client time - out
										  &sa/*NULL*/);                                                                                             // default secuirty attributes  :hhh 2013.04.30

	if(m_hPipe == INVALID_HANDLE_VALUE)
		return GetLastError();


	return 0;
}


/**
 @brief      파이프 서버에서 파이프 Listening을 위해 사용
 @author    hang ryul lee
 @date      2011.08.13
*/
int		CServerPipe::ListenNamedPipe()
{

	BOOL bRes = 0;
	int		nLastError = 0;
	bRes = ConnectNamedPipe(m_hPipe,NULL);

	if(bRes != TRUE)
	{
		nLastError = GetLastError();

		return nLastError;
	}

	return 0;

}

/**
 @brief      파이프 서버에서 파이프 Listening을 위해 사용
 @author    hang ryul lee
 @date      2011.08.13
*/
void CServerPipe::PipeServerStartUp()
{
	int nRes = 0;
	BOOL bRes = FALSE;
	DWORD nReadLen = 0;
	DWORD nWriteLen = 0;
	int			nLastErr = 0;
	CString	 strLog;
	
	strLog.Format(_T("PipeServerStartUp .. (nRes: %d)m_szPipeName  :%s "), nRes, m_szPipeName);
	UM_WRITE_LOG(strLog);
	
	while(TRUE)
	{
		//hhh(2013-06-12) : handle이 없는경우 타지 안도록 하기 위해
		if(m_hPipeEvent)
		{	// Kevin(2013-5-27)
			if(WaitForSingleObject(m_hPipeEvent, 1) == WAIT_OBJECT_0 )			
			{
				break; 
			}
		}
		nRes = InitializeNamedPipe();
		
		strLog.Format(_T("InitializeNamedPipe .. (nRes: %d)m_szPipeName  :%s "), nRes, m_szPipeName);
		UM_WRITE_LOG(strLog);

		if(nRes != 0)
		{
			// TODO 로그
			
			Sleep(1000);
			continue;
		}

		while(TRUE)
		{
			nRes = ListenNamedPipe();
			if(nRes != 0)
			{
				break;
			}

			if(m_bPipeServerExit) //hhh(2013.11.12)
			{
				if( m_hPipe )
				{		
					DisconnectNamedPipe(m_hPipe);
					CloseHandle(m_hPipe);
				}
				return;
			}

			/*********************** Client에서 오는 부분 읽는 가상함수 ******************************************************/

			if ( FALSE == OnReceiveData())
				break;

			/******************Client로 주는 부분 쓰는 가상 함수 ************************************************************/
			if( FALSE == OnSendData())
 				break;
			
			
			if( m_hPipe )
			{			
				//FlushFileBuffers( m_hPipe); 					//-> 사용금지, 사용시  hang에 빠진다.
				DisconnectNamedPipe( m_hPipe);			
			}

			if(m_bPipeServerExit) //hhh(2013.11.12)
			{
				if( m_hPipe )
				{							
					CloseHandle(m_hPipe);
				}
				return;
			}
		} // end while(TRUE)

		if( m_hPipe )
		{		
			DisconnectNamedPipe(m_hPipe);
			CloseHandle(m_hPipe);
		}
	} // end while(TRUE)

	if(m_hPipeEvent)
	{
		CloseHandle(m_hPipeEvent);
		m_hPipeEvent = NULL;
	}

}

/**
 @brief      파이프 서버에서 파이프 Listening을 위해 사용
 @author    hang ryul lee
 @date      2011.08.13
*/
void CServerPipe::PipeServerNewStartUp()
{

	int nRes = 0;
	BOOL bRes = FALSE;
//	HANDLE hPipe = NULL;
	TCHAR	zInBuf[4096 ] = {0,};
	TCHAR	zOutBuf[4096] = {0,};
	DWORD nReadLen = 0;
	DWORD nWriteLen = 0;
	int				nLastErr = 0;
	
//	DBGLOG(_T("CServerPipe::PipeServerNewStartUp()"));
	while(TRUE)
	{

		//hhh(2013-06-12) : handle이 없는경우 타지 안도록 하기 위해
		if(m_hPipeNewEvent)
		{	
			// Kevin(2013-5-27)
			if(WaitForSingleObject(m_hPipeNewEvent, 1) == WAIT_OBJECT_0 )
				break; 
		}
		

		nRes = InitializeNamedPipe();

		if(nRes != 0)
		{
			// TODO 로그
			CString strError=   _T("");
			strError.Format(_T("error (%d)"), nRes);
		//	DBGLOG(strError);

			Sleep(1000);
			continue;
		}
		while(TRUE)
		{
			nRes = ListenNamedPipe();

			if(nRes != 0)
			{
				CString strError=   _T("");
				strError.Format(_T(" LISTEN error (%d)"), nRes);
		//		DBGLOG(strError);

				//TODO 로그
				break;
			}

			if(m_bPipeServerExit) //hhh(2013.11.12)
			{
				if( m_hPipe )
				{		
					DisconnectNamedPipe(m_hPipe);
					CloseHandle(m_hPipe);
				}

				if(m_hPipeNewEvent)
				{
					CloseHandle(m_hPipeNewEvent);
					m_hPipeNewEvent = NULL;
				}
				return;
			}
			 
			/*********************** Client에서 오는 부분 읽는 가상함수 ******************************************************/

			if ( FALSE == OnReceiveHeader())
				break;

			if ( FALSE == OnReceiveBody())
				break;

			/******************Client로 주는 부분 쓰는 가상 함수 ************************************************************/
			if( FALSE == OnSendData())
 				break;


			if( m_hPipe )
			{

				FlushFileBuffers( m_hPipe);
				DisconnectNamedPipe( m_hPipe);
			}

			if(m_bPipeServerExit) //hhh(2013.11.12)
			{
				if( m_hPipe )											
					CloseHandle(m_hPipe);

				if(m_hPipeNewEvent)
				{
					CloseHandle(m_hPipeNewEvent);
					m_hPipeNewEvent = NULL;
				}
				
				return;
			}
			

		} // end while(TRUE)

		if( m_hPipe )
		{
			DisconnectNamedPipe(m_hPipe);

			CloseHandle(m_hPipe);


		}
		
	} // end while(TRUE)
	if(m_hPipeNewEvent)
	{
		CloseHandle(m_hPipeNewEvent);
		m_hPipeNewEvent = NULL;
	}

}

//hhh: 스스로 pipeserver를 종료하기 위해
void CServerPipe::SetExitPipeServer() 
{ 
	m_bPipeServerExit = TRUE;
	ClientConnectNamedPipe();
}

/**
 @brief     pipeserver 접속을 종료하기 위해 자기자신에게 연결시도
 @author    hhh
 @date      2013.11.12
*/
int  CServerPipe::ClientConnectNamedPipe()
{

	int		nLastError = 0;
	HANDLE hPipe = NULL;

	
//	DBGLOG(L"PipeName = %s", m_szPipeName);
	while(TRUE)
	{
		hPipe = CreateFile(m_szPipeName,
									GENERIC_READ | GENERIC_WRITE,
									0,
									NULL,
									OPEN_EXISTING,
									0,
									NULL);

		// 정상 접속 됨
		if(hPipe != INVALID_HANDLE_VALUE)
			break;

		nLastError = GetLastError();

		//  접속 오류
		if(nLastError  != ERROR_PIPE_BUSY)
			return nLastError;

		if(!WaitNamedPipe(m_szPipeName, NMPWAIT_USE_DEFAULT_WAIT))
		{
			nLastError = GetLastError();

			if(nLastError  == ERROR_FILE_NOT_FOUND) 
				return nLastError;

			return -1;
		}

	} // end while(TRUE)


	return 0;
}

