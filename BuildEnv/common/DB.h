

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      DB.h 
 @brief     Database 접근을위한 Abstract Class 정의 파일

            Database 접근을위한 클래스를 작성해야 할 경우 여기에 기술된 
            Abstract Class 들을 상속받아 구현하면 된다.

 @author    hang ryul lee
 @date      create 2011.05.12 
 @note      실제적인 DB구현 기술과 해당 기능을 사용하는 프로그램 소스의 의존성을 
            낮추기위해 중간계층의 Interface역할을 담당한다.
*/

#pragma once


/**
 @class     CDbConnection 
 @brief     Database 접속을 담당하는 Abstract Class.

            Database 접속 및 접속관련 속성설정을 설정한다. \n
            Database 접속에 대한 Transaction 처리 기능을 수행한다. \n

 @author    hang ryul lee
 @date      create 2011.05.31 
*/
class CDbConnection
{
public:   
    CDbConnection() {};
    virtual ~CDbConnection() {};

    virtual long BeginTrans() = 0;
    virtual void Close() = 0;
    virtual long CommitTrans() = 0;
    virtual bool Execute(const CString &_sCommandText) = 0;
    virtual bool Open() = 0;
    virtual long RollbackTrans() = 0;

    virtual long GetCommandTimeout() const = 0;
    virtual void SetCommandTimeout(long _lCommandTimeout) = 0;
    virtual bool IsConnected() const = 0;
    virtual CString GetConnectionString() const = 0 ;
    virtual void SetConnectionString(const CString &_sConnectionStr) = 0;
    virtual long GetConnectionTimeout() const = 0;
    virtual void SetConnectionTimeout(long _lConnectionTimeout) = 0;

    virtual DWORD GetError() const = 0;
    virtual CString GetErrorMessage() const = 0;
private:
    CDbConnection(const CDbConnection &_rDbConnection);               /**< 복사 생성자 */
    CDbConnection& operator = (const CDbConnection &_rDbConnection);  /**< 대입 연산자 */
};

/**
 @class     CDbDataSet 
 @brief     SQL 명령의 실행으로 생성된 레코드의 집합을 Class화한 Abstract Class.

            한번에 한 레코드씩 읽을수 있다.

 @author    hang ryul lee
 @date      create 2011.05.12 
*/
class CDbDataSet
{
public:
    CDbDataSet() {};
    virtual ~CDbDataSet() {};

    virtual void Next() = 0;

    virtual int GetIntField(const CString &_sFieldName, int _nDefault = 0) = 0 ;
    virtual double GetDoubleField(const CString &_sFieldName, double _dDefault = 0.0) = 0; 
    virtual CString GetStringField(const CString &_sFieldName, CString _sDefault = _T("")) = 0;
    virtual COleDateTime GetDateTimeField(const CString &_sFieldName, COleDateTime _dtDefault = COleDateTime()) = 0;
    
    virtual bool IsBof() = 0;
    virtual bool IsEof() = 0;
    virtual bool IsEmpty() = 0;
    virtual bool IsFieldNull(const CString &_sFieldName) = 0;

    virtual DWORD GetError() const = 0;
    virtual CString GetErrorMessage() const = 0;
private:
    CDbDataSet(const CDbDataSet &_rDbDataSet);
    CDbDataSet& operator = (const CDbDataSet &_rDbDataSet);
};


/**
 @class     CDbCommand 
 @brief     SQL문을 실행하는 Class

            CDbConnection을 사용하여 SQL문을 실행하고 결과 레코드 집합인 CDbDataSet을 생성한다.

 @author    hang ryul lee
 @date      create 2011.05.12
*/
class CDbCommand
{
public:
    CDbCommand() {};
    virtual ~CDbCommand() {};

    virtual bool Execute(int &_nRecordsAffected) = 0;

    virtual bool AddParameter(const CString &_sName, int _nValue, int _nDirection = 1) = 0;
	virtual bool AddParameter(const CString &_sName, double _dValue, int _nDirection = 1, BYTE _nPrecision = 0, BYTE _nScale = 0) = 0;
    virtual bool AddParameter(const CString &_sName, CString _sValue, int _nDirection = 1, long _lSize = 256) = 0;
    virtual bool AddParameter(const CString &_sName, COleDateTime _dtValue, int _nDirection = 1) = 0;
	
    virtual long GetCommandTimeout() const = 0;
    virtual void SetCommandTimeout(long _lCommandTimeout) = 0;
    virtual CString GetCommandText() const = 0;
    virtual void SetCommandText(const CString &_rCommandText) = 0;

    virtual DWORD GetError() const = 0;
    virtual CString GetErrorMessage() const = 0;
private:
    CDbCommand(const CDbCommand &_rDbCommand);
    CDbCommand& operator = (const CDbCommand &_rDbCommand);
};


