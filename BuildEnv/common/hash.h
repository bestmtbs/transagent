/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.09.06		
********************************************************************************/
#ifndef _HASH_H_
#define _HASH_H_

#include "hash_global.h"


/* ---------------------------------------------------------------------------------------------- *
 *	[!] HASH Algorithm ID 																		  *
 * ---------------------------------------------------------------------------------------------- */
#define HASH							0x02000000
#define HASH_ALGID_MD2				HASH + 1
#define HASH_ALGID_MD5				HASH + 2
#define HASH_ALGID_RIPEMD160	HASH + 3
#define HASH_ALGID_SHA1				HASH + 4
#define HASH_ALGID_HAS160			HASH + 5
#define HASH_ALGID_SHA256			HASH + 6

#define HASH_ALGID_MD2_LEN			16
#define HASH_ALGID_MD5_LEN			16
#define HASH_ALGID_RIPEMD160_LEN		16
#define HASH_ALGID_SHA1_LEN			20
#define HASH_ALGID_HAS160_LEN		20
#define HASH_ALGID_SHA256_LEN		32

#define NUM_HASH_AlgorithmID			6

/* ---------------------------------------------------------------------------------------------- *
 * [!] 																							  *
 * ---------------------------------------------------------------------------------------------- */
#define	DK_DEFAULT_HASH_FILE_READ_LENGTH	1024		// default length of a time read from file
#define	DK_HASH_MODE_HMAC					5

/* [4]================================================================================= macro === */
/* [5]====================================================================== global variables === */


/* [6]============================================================================= structure === */
typedef struct _HASH_CTX {

	unsigned int				nAlgId;
	unsigned int					nHashLen;		// 해쉬 결과 바이트 길이 : 연접연산으로 따로 계산시 필요 
	unsigned int					caStat[ 8];		// 내부 계산 상태 저장 
	unsigned int					nDataLen[ 2];	// 64비트 정수로 입력 데이터 길이 저장, Final 계산시 패딩계산 
	unsigned char					caBuf[ 64];		// 계산할 한 블럭(64Byte) 저장
} HASH_CTX;

/* [7]=================================================================== function prototypes === */
#ifdef  __cplusplus
extern "C" {
#endif

void CmsHashBin(unsigned char * pOutData, unsigned char * pInData,	unsigned int	nInLen,	unsigned int	nAlgId);
HASH_CTX * CmsCreateHashCtx( unsigned int nHashId);
void CmsGetHashLen( unsigned int * pnHashLen, unsigned int nHashId);
void CmsInitHash( HASH_CTX *	psCtx);
void CmsUpdateHash( HASH_CTX * psCtx, unsigned char * pcInData, unsigned int nInLen);
void CmsFinalHash( unsigned char * pOutData, HASH_CTX * psCtx);
void CmsFreeHashCtx( HASH_CTX * psCtx);

/* =============================================================================== header end === */
#ifdef  __cplusplus
}
#endif

#endif