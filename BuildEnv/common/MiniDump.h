

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      MiniDump.h 
 @brief     CMiniDump Class 정의 파일

            DbgHelp.dll의 MiniDumpWriteDump를 사용하여 MiniDump파일을 생성.\n
            CMiniDump Class는 Singleton Pattern으로 작성되었음.

 @author    hang ryul lee
 @date      create 2011.06.16 
 @note      SetUnhandledExceptionFilter를 호출하여 지정한 예외필터는 프로세스에
            전역적이다.
 @note      DbgHelp.dll 버전 5.1.2600 이상에 MiniDumpWriteDump 함수가 포함되어있다.
 @note      DbgHelp.dll 버전 6.0 이전에는 현재 프로세스에서 미니덤프 파일을 작성하기
            위해 호출할때 교착상태가 발생하는 버그가 존재하였음.
 @note      DbgHelp.dll 버전 6.1.17.1 이후 버전 사용을 권장함.
 @note      DbgHelp.dll은 Debugging Tools for Windows의 구성요소이다.
            (http://www.microsoft.com/whdc/devtools/debugging/default.mspx)
 @note      미니덤프 타입값은 http://msdn.microsoft.com/en-us/library/ms680519(VS.85).aspx 을 참고 
*/

#pragma once

#include <DbgHelp.h>

class CMiniDump
{
public:
    CMiniDump(MINIDUMP_TYPE _nDumpType = MiniDumpNormal, const CString &_sPath = _T(""), const CString &_sFilePrefix = _T(""));
    virtual ~CMiniDump();

    CString GetPath() const;
    CString GetFilePrefix() const;
    MINIDUMP_TYPE GetDumpType() const;
    void SetDumpType(MINIDUMP_TYPE _nDumpType);
protected:
    virtual void OnShowDialog(bool &_bOk);
    virtual void OnAfterWriteDump(const CString &_sFileName);
    virtual void OnErrorWriteDump(const CString &_sFileName, DWORD _dwError); 
private:
    static CMiniDump *s_pInstance;                          /**< CMiniDump 인스턴스   */
    UINT m_nPrevErrorMode;                                  /**< 이전의 에러 모드값   */
    LPTOP_LEVEL_EXCEPTION_FILTER m_pPrevFilter;             /**< 이전의 예외필터값    */
    MINIDUMP_TYPE m_nDumpType;                              /**< 미니덤프 작성 타입   */
    CString m_sPath;                                        /**< 미니덤프 저장 경로   */
    CString m_sFilePrefix;                                  /**< 미니덤프 파일 Prefix */
    
    CMiniDump(const CMiniDump &_rMiniDump);                 /**< 복사 생성자 */
    CMiniDump& operator = (const CMiniDump &_rMiniDump);    /**< 대입 연산자 */

    static LONG WINAPI WriteCrashDump(PEXCEPTION_POINTERS _pExceptionInfo);
};