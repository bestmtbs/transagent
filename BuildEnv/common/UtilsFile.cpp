

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsFile.cpp 
 @brief     File/Directory 관련 function 구현 파일 
 @author    hang ryul lee
 @date      create 2007.06.22 

 @note 
*/
#include "stdafx.h"
#include "UtilsFile.h"
#include "WinOsVersion.h"
#include "atlbase.h"

#include "PathInfo.h"
#include "convert.h"
#include "Impersonator.h"
#include <tchar.h>
#include <strsafe.h>

/**        
 @brief     전체 경로를 포함한 파일명으로부터 파일경로를 구한다. 

            경로명 마지막에 \\를 포함하는 파일경로를 구한다. \n
            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n

 @author    hang ryul lee
 @date      2007.06.22
 @param     _sFileName [in]  파일명(전체 경로를 포함한 파일명), 최대 MAX_PATH(260)
 @param     _sPath     [ref] 파일경로(파일명을 제외한 파일경로), 최대 MAX_PATH(260)
 @return    void
*/

void ExtractFilePath(const CString &_sFileName, CString &_sPath)
{
    _sPath = _T("");
    int nLen = _sFileName.GetLength();
    if ((0 == nLen) || (MAX_PATH < nLen + 1)) 
		return;

    TCHAR szDrive[_MAX_DRIVE] = {0, }; 
    TCHAR szDir[_MAX_DIR] = {0, };

	_tsplitpath_s(_sFileName, szDrive, _MAX_DRIVE, szDir, _MAX_DIR, NULL, 0, NULL, 0);

	_sPath = szDrive;
	_sPath += szDir;
}

/**
 @brief     전체 경로를 포함한 파일명으로부터 파일경로를 구한다. 
 
            경로명 마지막에 \\를 포함하는 파일경로를 구한다. \n
            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n

 @author    hang ryul lee
 @date      2007.06.22
 @param     _sFileName [in]  파일명(전체 경로를 포함한 파일명), 최대 MAX_PATH(260)
 @return    파일경로(파일명을 제외한 파일경로)
*/
CString ExtractFilePath(const CString &_sFileName)
{
    int nLen = _sFileName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1))
		return _T("");

    TCHAR szDrive[_MAX_DRIVE] = {0, }; 
    TCHAR szDir[_MAX_DIR] = {0, };

	_tsplitpath_s(_sFileName, szDrive, _MAX_DRIVE, szDir, _MAX_DIR, NULL, 0, NULL, 0);

	CString sPath;
	sPath = szDrive;
	sPath += szDir;

	return sPath;
}

/**
 @brief     전체 경로를 포함한 파일명으로부터 경로를 제외한 파일이름을 구한다. 
 
            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n

 @author    hang ryul lee
 @date      2008.04.11
 @param     _sFileName [in]  파일명(전체 경로를 포함한 파일명), 최대 MAX_PATH(260)
 @return    파일명(파일 경로를 제외한 파일명)
*/
CString ExtractFileName(const CString &_sFileName)
{
	CString sFileName = _sFileName;
	sFileName = sFileName.Trim();

	int nLen = sFileName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1))
		return _T("");

	TCHAR szName[_MAX_FNAME] = {0, };
    TCHAR szExt[_MAX_EXT] = {0, };

	_tsplitpath_s(_sFileName, NULL, 0, NULL, 0, szName, _MAX_FNAME, szExt, _MAX_EXT);

	CString sName = _T("");
	sName = szName;
	if (_T("") != sName)
		sName += szExt;

	return sName;
}

/**
 @brief     전체 경로를 포함한 파일명으로부터 Base File Name을 구한다. 
 
            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n

 @author    hang ryul lee
 @date      2008.06.16
 @param     _sFileName [in]  파일명(전체 경로를 포함한 파일명), 최대 MAX_PATH(260)
 @return    Base File Name(파일명중 확장자를 제외한 파일이름)
*/
CString ExtractBaseFileName(const CString &_sFileName)
{
    CString sFileName = _sFileName;
	sFileName = sFileName.Trim();

	int nLen = sFileName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1))
		return _T("");

	TCHAR szName[_MAX_FNAME] = {0, };

	_tsplitpath_s(_sFileName, NULL, 0, NULL, 0, szName, _MAX_FNAME, NULL, 0);

	return szName;
}


/**
 @brief     전체 경로를 포함한 파일명으로부터 파일확장자를 구한다. 
 
            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n

 @author    hang ryul lee
 @date      2008.06.16
 @param     _sFileName [in]  파일명(전체 경로를 포함한 파일명), 최대 MAX_PATH(260)
 @return    파일확장자("."을 포함한 파일확장자)
*/
CString ExtractFileExt(const CString &_sFileName)
{
    CString sFileName = _sFileName;
	sFileName = sFileName.Trim();

	int nLen = sFileName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1))
		return _T("");

    TCHAR szExt[_MAX_EXT] = {0, };

	_tsplitpath_s(_sFileName, NULL, 0, NULL, 0, NULL, 0, szExt, _MAX_EXT);

	return szExt;
}

/**
 @brief     전체 경로를 포함한 파일명으로부터 드라이브명을 구한다. 
 
            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n

 @author    hang ryul lee
 @date      2009.02.02
 @param     _sFileName [in]  파일명(전체 경로를 포함한 파일명), 최대 MAX_PATH(260)
 @return    파일 드라이브 명
*/
CString ExtractFileDrive(const CString &_sFileName)
{
    CString sFileName = _sFileName;
	sFileName.Trim();

	int nLen = sFileName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1))
		return _T("");

    TCHAR szDrive[_MAX_DRIVE] = {0, };

	_tsplitpath_s(_sFileName, szDrive, _MAX_DRIVE, NULL, 0, NULL, 0, NULL, 0);

	return szDrive;
}

/**
 @brief     경로명에서 마지막에 존재하는 경로 구분자(\\)를 제거한다. 

            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n
 @author    hang ryul lee
 @date      2007.06.22
 @param     _sSrcName  [in]  파일경로, 최대 MAX_PATH(260)
 @param     _sDestName [ref] 마지막 구분자(\\)를 제거한 파일경로, 최대 MAX_PATH(260)
 @return    void
*/
void ExcludeTrailingBackslash(const CString &_sSrcName, CString &_sDestName)
{    
    _sDestName = _T("");
    int nLen = _sSrcName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1)) 
		return;

	_sDestName = _sSrcName;
	if (_sDestName.Right(1) == _T("\\"))
		_sDestName = _sDestName.Left(_sDestName.GetLength() - 1);

}

/**
 @brief     경로명에서 마지막에 존재하는 경로 구분자(\\)를 제거한다. 

            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n
 @author    hang ryul lee
 @date      2007.06.22
 @param     _sName  [in]  파일경로, 최대 MAX_PATH(260)
 @return    마지막 구분자(\\)를 제거한 파일경로, 최대 MAX_PATH(260)
*/
CString ExcludeTrailingBackslash(const CString &_sSrcName)
{
    int nLen = _sSrcName.GetLength();
	if ((0 == nLen) || (MAX_PATH < nLen + 1)) 
		return _T("");

	if (_sSrcName.Right(1) == _T("\\"))
		return _sSrcName.Left(nLen - 1);
    else
        return _sSrcName;
}

/**
 @brief     경로명 마지막에 존재하는 구분자(\\)이 존재하지 않을경우 구분자(\\)를 추가한다.

            경로명 길이가 MAX_PATH(260)을 초과할 경우 공백문자열을 리턴한다. \n
 @author    hang ryul lee
 @date      2007.06.22
 @param     _sSrcName  [in]  파일경로, 최대 MAX_PATH(260)
 @return    마지막 구분자(\\)를 포함한 파일경로, 최대 MAX_PATH(260)
*/
CString IncludeTrailingBackslash(const CString &_sSrcName)
{
    CString sName = _sSrcName;
    sName.Trim();

    int nLen = sName.GetLength();
    if ((0 == nLen) || (MAX_PATH < nLen + 1))
        return _T("");

    if (sName.Right(1) != _T("\\"))
    {
        sName += _T("\\");
        if (MAX_PATH < sName.GetLength() + 1)
            return _T("");
        else
            return sName;
    }
    else
    {
        return sName;
    }
}

/**
 @brief     주어진 디렉토리가 실제 디렉토리로 존재하는지 확인한다. 

            경로명 길이가 MAX_PATH(260)을 초과할 경우 false를 리턴한다. \n
 @author    hang ryul lee
 @date      2007.06.22
 @param     _sName  [in]  디렉토리명, 최대 MAX_PATH(260)
 @return    true : 디렉토리 존재 \n
            false: 디렉토리 존재하지 않음.
*/
bool DirectoryExists(const CString &_sSrcName)
{
    CString sName = _sSrcName;
    sName.Trim();

    if (MAX_PATH < sName.GetLength() + 1)
        return false;

	DWORD dwCode = ::GetFileAttributes(sName);

	return (dwCode != -1) && ((FILE_ATTRIBUTE_DIRECTORY & dwCode) != 0);
}

/**
 @brief     현재 드라이브값을 구한다. 

 @author    hang ryul lee
 @date      2008.03.31
 @return    성공시 현재 드라이브 문자열값(예:c:, d:) \n
            실패시 공백 문자열값
*/
CString GetCurrentDrive()
{
    TCHAR szCurrentDir[MAX_PATH] = {0, };
    
    if (0 == ::GetCurrentDirectory(MAX_PATH, szCurrentDir))
    {
        return _T("");
    }
    else
    {
        TCHAR szDrive[_MAX_DRIVE] = {0, };

	    _tsplitpath_s(szCurrentDir, szDrive, _MAX_DRIVE, NULL, 0, NULL, 0, NULL, 0);

	    CString sDrive = szDrive;
        return sDrive;;
    }
}

/**
 @brief     주어진 파일명의 파일이 실제 존재하는지 확인한다. 
 @author    hang ryul lee
 @date      2007.06.22
 @param     _sFileName  [in]  파일명, 최대 MAX_PATH(260)
 @return    true : 파일 존재 \n
            false: 파일 존재하지 않음.
 @note      .2008.08.26 yunmi\n
             -FindFirstFile 이후 FindClose 해주지 않는문제 수정
*/
bool FileExists(const CString &_sFileName)
{
	bool bRet  = false;
	if(_sFileName.GetLength() == 0)
		return bRet;

	CString strFile = L"";
	CString strFile2 = _sFileName;

	
	try {
		strFile2.Format(L"\"%s\"", _sFileName);
		UM_WRITE_LOG(L"FileExists()");
		UM_WRITE_LOG(strFile2\
			);
		int nstate = _waccess(strFile2.GetBuffer(), 0);
		if(nstate != -1)
		{
			bRet = true;
			//UM_WRITE_LOG
			UM_WRITE_LOG(L"FileExists exist");
		}
		else
		{
			WIN32_FIND_DATA				sFindData;
			HANDLE hFind = FindFirstFile(_sFileName, &sFindData);
			if(hFind != INVALID_HANDLE_VALUE)
			{
				bRet = true;
				UM_WRITE_LOG(L"FileExists from findfiirstfile exist");
				//UM_WRITE_LOG(L"IsFileExist from findfiirstfile exist");

				FindClose(hFind);
			}
			else
			{
				//UM_WRITE_LOG(L"IsFileExist not exist = %d", nstate);
				WCHAR str[MAX_PATH] ={0};
				StringCchPrintf(str, MAX_PATH, L"FileExists  findfiirstfile not exist LastError = %d", GetLastError());
				UM_WRITE_LOG(str);

			}
		}
	} catch(...)
	{
		UM_WRITE_LOG(L"FileExists() exception occure = ");
		UM_WRITE_LOG(strFile2);
	}
	return bRet;
}



/**
 @brief     주어진 파일명의 파일을 삭제한다.

            파일속성이 읽기전용인 파일까지도 삭제가 가능하다.
 @author    hang ryul lee
 @date      2008.03.27
 @param     [in]_sFileName   파일명, 최대 MAX_PATH(260)
 @param     [out]_pdwError   시스템 에러코드
 @return    true : 파일 삭제 성공 \n
            false: 파일 삭제 실패, \n
                   삭제 대상 파일이 존재하지 않을 경우, \n
                   삭제 대상 파일명 길이가 MAX_PATH(260)보다 큰경우.
*/
bool ForceDeleteFile(const CString &_sFileName, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;

    DWORD dwAttrs = ::GetFileAttributes(_sFileName);
    if (dwAttrs & FILE_ATTRIBUTE_READONLY)
    {
        ::SetFileAttributes(_sFileName, dwAttrs & ~FILE_ATTRIBUTE_READONLY); 
    }        
    BOOL bDelete = ::DeleteFile(_sFileName);
    if (!bDelete && (NULL != _pdwErrorCode))
        *_pdwErrorCode = ::GetLastError();
    
    return (TRUE == bDelete);
}

/**
 @brief     파일을 복사한다.

            복사대상지에 읽기전용, 숨김, 시스템 파일속성의 파일이 
            이미 존재하더라도복사가 가능하다.
 @author    hang ryul lee
 @date      2008.03.27
 @param     [in]_sSrcFile    복사 원본 파일명, 최대 MAX_PATH(260)
 @param     [in]_sDestFile   복사 대상 파일명, 최대 MAX_PATH(260)
 @param     [out]_pdwError   시스템 에러코드
 @return    true : 파일 복사 성공 \n
            false: 파일 복사 실패
*/
bool ForceCopyFile(const CString &_sSrcFile, const CString &_sDestFile, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;

    if (FileExists(_sDestFile))
    {        
        DWORD dwAttrs = ::GetFileAttributes(_sDestFile);
        if (dwAttrs & FILE_ATTRIBUTE_READONLY)
        {
            dwAttrs = dwAttrs & ~FILE_ATTRIBUTE_READONLY; 
        }
        
        if (dwAttrs & FILE_ATTRIBUTE_HIDDEN)
        {
            dwAttrs = dwAttrs & ~FILE_ATTRIBUTE_HIDDEN;
        }
        
        if (dwAttrs & FILE_ATTRIBUTE_SYSTEM)
        {
            dwAttrs = dwAttrs & ~FILE_ATTRIBUTE_SYSTEM;
        }
        ::SetFileAttributes(_sDestFile, dwAttrs);
    }
    BOOL bCopy = ::CopyFile(_sSrcFile, _sDestFile, FALSE);
    if (!bCopy && (NULL != _pdwErrorCode))
        *_pdwErrorCode = ::GetLastError();

    return (TRUE == bCopy);
}

/**
 @brief     파일또는 폴더를 이동한다.\n
            폴더이동시 하위 폴더도 모두 포함하여 이동한다.
 @author    hang ryul lee
 @date      2008.08.18
 @param     [in] _sOldFile :원본 파일명(경로포함)
 @param     [in] _sNewFile :수정 파일명(경로포함) - 최대길이 MAX_PATH(260)
 @param     [out]_pdwError   시스템 에러코드
 @return    true : 파일 복사 성공 \n
            false: 파일 복사 실패
*/
bool ForceMoveFile(const CString &_sOldFile, const CString &_sNewFile, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;

    if (!FileExists(_sOldFile))
        return false;

    INT nLen = _sNewFile.GetLength();
    if ((0 == nLen) || (MAX_PATH < nLen + 1)) 
        return false;

    if (DirectoryExists(_sNewFile)) 
        ForceDeleteDir(_sNewFile);
    else if (FileExists(_sNewFile))
        ForceDeleteFile(_sNewFile);

    BOOL bMove = ::MoveFile(_sOldFile, _sNewFile);
    if (!bMove && (NULL != _pdwErrorCode))
        *_pdwErrorCode = ::GetLastError();

    return (TRUE == bMove);
}

/**
 @brief     주어진 디렉터리를 생성한다.

            상위 디렉터리가 존재하지 않을 경우 상위디렉토리까지 생성해준다.\n
            경로명 길이가 최대길이(248)을 초과할 경우 디렉터리를 생성하지 않고 false를 리턴한다. \n
            최대 길이는 마자막의(\\)을 제외한 길이가 248임.(MSDN의 CreateDirectory 참조)
 @author    hang ryul lee
 @date      2007.06.22
 @param     [in] _sDir     :디렉터리명, 최대길이(248)
 @param     [out]_pdwErrorCode :시스템 에러코드
 @return    true : 디렉터리 생성 성공, \n
                   디렉터리가 이미 존재하는 경우.
            false: 디렉터리 생성 실패.
*/
bool ForceCreateDir(CString _sDir, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;

    _sDir.Trim();
    int nLen = _sDir.GetLength();
	if ((0 == nLen) || (248 < nLen + 1))
		return false;

	_sDir = ExcludeTrailingBackslash(_sDir);

	if ((_sDir.GetLength() < 3) || (DirectoryExists(_sDir)))
		return true;

    int nFound = _sDir.ReverseFind('\\'); 
    if (!ForceCreateDir(_sDir.Left(nFound), _pdwErrorCode))
        return false;

    if (!::CreateDirectory(_sDir, NULL))
    {
        if (NULL != _pdwErrorCode)
            *_pdwErrorCode = ::GetLastError();
        return false;
    }
    
    return true;
}

/**
 @brief     주어진경로의 서브 디렉토리 리스트를 구한다.
 @author    hang ryul lee
 @date      2008.08.18
 @param     [in] _sPath :조사할 경로
 @param     [out]_sListDirs :서브디렉토리 리스트(파일의 경로제외)
 @return    서브 디렉토리 개수
 @note      리스트에는 경로를 포함한 서브디렉토리의 풀네임이 저장된다.\n
 @note      숨김파일까지 다 구해진다.
*/
INT GetSubDirNames(const CString &_sPath, CStringList &_sListDirs, const bool _bHidden /*= false*/)
{
    if (!DirectoryExists(_sPath)) return 0;

    if (0 < _sListDirs.GetCount()) 
        _sListDirs.RemoveAll();

    CFileFind Finder;
    BOOL bFind = Finder.FindFile(IncludeTrailingBackslash(_sPath) + _T("*.*"));
    while (bFind)
    {
        bFind = Finder.FindNextFile();
        if (Finder.IsDots())
            continue;

        if (!_bHidden && Finder.IsHidden())
            continue;

        if (Finder.IsDirectory())
        {
            CString sName = Finder.GetFileName();
            _sListDirs.AddTail(sName);
        }
    }
    Finder.Close();

    return static_cast<INT>(_sListDirs.GetCount());
}

/**
 @brief     주어진경로의 파일 리스트를 구한다.
 @author    hang ryul lee
 @date      2008.08.18
 @param     [in] _sPath :조사할 경로
 @param     [out]_sListFiles :파일 리스트(파일의 경로제외)
 @return    파일 개수
 @note      리스트에는 경로를 포함한 파일의 풀네임이 저장된다.\n
 @note      숨김파일까지 다 구해진다.
*/
INT GetFileNames(const CString &_sPath, CStringList &_sListFiles, const bool _bHidden /*= false*/)
{
    if (!DirectoryExists(_sPath)) return 0;

    if (0 < _sListFiles.GetCount()) 
        _sListFiles.RemoveAll();

    CFileFind Finder;
    BOOL bFind = Finder.FindFile(IncludeTrailingBackslash(_sPath) + _T("*.*"));
    while (bFind)
    {
        bFind = Finder.FindNextFile();
        if (Finder.IsDots())
            continue;

        if (!_bHidden && Finder.IsHidden())
            continue;

        if (!Finder.IsDirectory())
        {
            CString sName = Finder.GetFileName();
            _sListFiles.AddTail(sName);
        }
    }
    Finder.Close();

    return static_cast<INT>(_sListFiles.GetCount());
}

/**
 @brief     디렉토리를 복사한다.
 @author    hang ryul lee
 @date      2008.08.18
 @param     [in] _sSrcDir :복사할 소스 디렉토리경로
 @param     [in] _sDestDir:복사 목적지 디렉토리경로
 @param     [out]_pdwError:시스템 에러코드
 @return    true / false
 @note      서브 디렉토리 까지 복사한다.
 @note      목적 디렉토리가 존재할 경우, 삭제 후 복사한다.\n
            (목적 디렉토리 속성값에 상관없이 ForceDeleteDir함수를 이용하여\n
            삭제 후 소스 디렉토리와 동일한 속성의 폴더를 생성하여 복사한다.)
*/
bool ForceCopyDir(const CString &_sSrcDir, const CString &_sDestDir, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;

    if (!DirectoryExists(_sSrcDir)) return false;
       
   // if (0 <= _sDestDir.Find(_sSrcDir)) return false;
    if (DirectoryExists(_sDestDir))
        if (!ForceDeleteDir(_sDestDir)) return false;
    
    if (!ForceCreateDir(_sDestDir, _pdwErrorCode)) return false;

    //속성복사
    DWORD dwAttrs = ::GetFileAttributes(_sSrcDir);
    ::SetFileAttributes(_sDestDir, dwAttrs);

    bool bRe = true;

    CFileFind Finder;
    BOOL bFind = Finder.FindFile(IncludeTrailingBackslash(_sSrcDir) + _T("*.*"));
    while (bFind)
    {
        bFind = Finder.FindNextFile();
        if (Finder.IsDots())
            continue;

        if (Finder.IsDirectory())
        {
            CString sDest = IncludeTrailingBackslash(_sDestDir) + Finder.GetFileName();
            if (!ForceCopyDir(Finder.GetFilePath(), sDest, _pdwErrorCode))
            {
                bRe = false;
                break;
            }
        }
        else
        {
            CString sDestFile = IncludeTrailingBackslash(_sDestDir) + Finder.GetFileName();
            if (!ForceCopyFile(Finder.GetFilePath(), sDestFile, _pdwErrorCode))
            {
                bRe = false;
                break;
            }
        }
    }
    Finder.Close();

    return bRe;
}

/**
 @brief     주어진 디렉터리를 삭제한다.

            대상 디렉터리 내부에 파일 및 서브디렉터리가 존재하면 모두 삭제후 대상 디렉터리를 삭제한다.\n
            대상 디렉터리 내부에 읽기 전용의 파일이 존재하더라도 삭제가 가능하다.\n

 @author    hang ryul lee
 @date      2008.03.27
 @param     [in] _sFileName 대상경로명, 최대 MAX_PATH(260)
 @param     [out]_pdwError  시스템 에러코드
 @return    true : 삭제 성공 \n
            false: 삭제 실패
*/
bool ForceDeleteDir(const CString &_sPath, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;


    if (!DirectoryExists(_sPath))
        return false;

    CFileFind Finder;
    BOOL bFind = Finder.FindFile(IncludeTrailingBackslash(_sPath) + _T("*.*"));
    while (bFind)
    {
        bFind = Finder.FindNextFile();
        if (Finder.IsDots())
            continue;

        if (Finder.IsDirectory())
            ForceDeleteDir(Finder.GetFilePath());
        else
            ForceDeleteFile(Finder.GetFilePath());
    }
    Finder.Close();

    DWORD dwAttrs = ::GetFileAttributes(_sPath);
    if (dwAttrs & FILE_ATTRIBUTE_READONLY)
        ::SetFileAttributes(_sPath, dwAttrs & ~FILE_ATTRIBUTE_READONLY);

    BOOL bRemove = ::RemoveDirectory(_sPath);
    if (!bRemove && (NULL != _pdwErrorCode))
        *_pdwErrorCode = ::GetLastError();

    return (TRUE == bRemove);
}

bool ForceDeleteDir2(const CString &_sPath, CString strTargetFile, DWORD dwRemoveDir, DWORD *_pdwErrorCode /*= NULL*/)
{
    if (NULL != _pdwErrorCode)
        *_pdwErrorCode = 0;


    if (!DirectoryExists(_sPath))
        return false;

    CFileFind Finder;
    BOOL bFind = Finder.FindFile(IncludeTrailingBackslash(_sPath) + strTargetFile);///_T("*.*"));
    while (bFind)
    {
        bFind = Finder.FindNextFile();
        if (Finder.IsDots())
            continue;

        if (Finder.IsDirectory())
            ForceDeleteDir(Finder.GetFilePath());
        else
            ForceDeleteFile(Finder.GetFilePath());
    }
    Finder.Close();

	if(dwRemoveDir == 1)
	{
		DWORD dwAttrs = ::GetFileAttributes(_sPath);
		if (dwAttrs & FILE_ATTRIBUTE_READONLY)
			::SetFileAttributes(_sPath, dwAttrs & ~FILE_ATTRIBUTE_READONLY);

		
		BOOL bRemove = ::RemoveDirectory(_sPath);
		if (!bRemove && (NULL != _pdwErrorCode))
			*_pdwErrorCode = ::GetLastError();
		return (TRUE == bRemove);
	}
    return TRUE;
}

/**
 @brief     현재 모듈의 파일이름을 구한다.

 @author    hang ryul lee
 @date      2008.06.16
 @return    성공일경우 전체경로를 포함한 파일이름 \n
            실패일경우 공백문자열
*/
CString GetCurrentModuleFileName()
{
    TCHAR szBuffer[MAX_PATH] = {0, };

    if (0 == ::GetModuleFileName(NULL, szBuffer, MAX_PATH))
        return _T("");

    return szBuffer;
}

/**
 @brief     현재 모듈의 경로를 구한다.

 @author    hang ryul lee
 @date      2008.06.16
 @return    성공일경우 마지막에 역슬레쉬(\)가 포함된 전체경로 \n
            실패일경우 공백문자열
*/
CString GetCurrentModulePath()
{
    return ExtractFilePath(GetCurrentModuleFileName());
}

/**
 @brief     파일 사이즈를 구한다

 @author    odkwon
 @date      2008.08.28
 @return    파일 사이즈
 @note      파일 사이즈 DWORD 2^32 = 8천기가까지 표현가능
*/
DWORD GetFileLength(const CString &_sFileName)
{
    CFile File;
    try
    {
        if (!File.Open(_sFileName, CFile::modeRead | CFile::shareDenyNone))
            return 0;
		DWORD nLength = (DWORD)File.GetLength();

		File.Close();


        return static_cast<DWORD>(nLength);
    }
    catch (...)
    {
        return 0;
    }

}

/**
 @brief     OS에 등록되어 있는 환경경로를 리스트로 구한다.
 @author    odkwon
 @date      2008.19.10
 @param     [out] _PathList 환경경로 리스트
 @return    true/false
*/
bool GetEnvironmentPath(CStringListEx &_PathList)
{
    LPTCH lpvEnv; 
    LPTSTR lpszVariable; 
    bool bRet = false;

    if (0 < _PathList.GetCount())
        _PathList.RemoveAll();

    lpvEnv = GetEnvironmentStrings();
    if (NULL == lpvEnv)
        return bRet; 

    bRet = true;
    lpszVariable = reinterpret_cast<LPTSTR>(lpvEnv);
    while (*lpszVariable)
    {
        CString tmpVariable = lpszVariable;
        int nStartPos = 0;
        CString sValue = tmpVariable.Tokenize(_T("="), nStartPos);
        if (0 == sValue.CompareNoCase(_T("PATH")))
        {
            tmpVariable = tmpVariable.TrimLeft(_T("PATH="));
            _PathList.SetText(tmpVariable, _T(";"));
            break;
        }
        lpszVariable += _tcslen(lpszVariable) + 1;
        
    }
    FreeEnvironmentStrings(lpvEnv);
    return bRet;
}

/**
 @brief     OS에 등록되어 있는 환경확장자를 리스트로 구한다.
 @author    odkwon
 @date      2008.19.10
 @param     [out] _PathList 환경경로 리스트
 @return    true/false
*/

bool GetEnvironmentPathExt(CStringListEx &_ExList)
{
    LPTCH lpvEnv; 
    LPTSTR lpszVariable; 
    bool bRet = false;
    if (0 < _ExList.GetCount())
        _ExList.RemoveAll();
    CWinOsVersion OsVersion;
    if(OsVersion.IsWin9x())
    {
        _ExList.AddTail(_T(".COM"));
        _ExList.AddTail(_T(".BAT"));
        _ExList.AddTail(_T(".EXE"));
    }
    else
    {
        lpvEnv = GetEnvironmentStrings();
        if (NULL == lpvEnv)
            return bRet; 
        bRet = true;

        lpszVariable = reinterpret_cast<LPTSTR>(lpvEnv);
        while (*lpszVariable)
        {
            CString tmpVariable = lpszVariable;
            int nStartPos = 0;
            CString sValue = tmpVariable.Tokenize(_T("="), nStartPos);
            if (0 == sValue.CompareNoCase(_T("PATHEXT")))
            {
                tmpVariable = tmpVariable.TrimLeft(_T("PATHEXT="));
                _ExList.SetText(tmpVariable, _T(";"));
                break;
            }
            lpszVariable += _tcslen(lpszVariable) + 1;
            
        }
        FreeEnvironmentStrings(lpvEnv);
    }
    return bRet;
}


/**
 @brief     실행파일에 경로정보가 없을 경우 환경경로를 추가하여 해당 파일이 있는지 확인한다.
 @author    odkwon
 @date      2008.10.13
 @param     _sFileName  [in]  파일명, 최대 MAX_PATH(260)
 @return    true : 파일 존재 \n
            false: 파일 존재하지 않음.
*/
bool FileExistsEnvPath(const CString &_sFileName)
{
    if (FileExists(_sFileName))
        return true;
    CString stmpFileName = ExtractFileName(_sFileName);
    CStringListEx EnPathList;

    GetEnvironmentPath(EnPathList);
    INT_PTR nIndex = 0;
    POSITION pos = EnPathList.FindIndex(nIndex);
    while (NULL != pos)
    {
        if (FileExists(IncludeTrailingBackslash(EnPathList.GetAt(pos)) + stmpFileName))
            return true;
        pos = EnPathList.FindIndex(++nIndex);
    }    
    return false;
}

/**
 @brief     확장자가 없을 경우,경로가 없을 경우 OS환경변수에서 환경경로,확장자를 수집하여\n
            해당파일이 존재하는지 확인한다.
 @author    odkwon
 @date      2008.10.13
 @param     _sFileName  [in]  파일명, 최대 MAX_PATH(260)
 @return    true : 파일 존재 \n
            false: 파일 존재하지 않음.
 @note      .notepad ->입력시 환경경로,환경확장자를 붙여서 해당 파일이 존재하는지 확인한다.
*/
bool FileExistsEnvPathExt(const CString &_sFileName)
{
    if (FileExists(_sFileName))
        return true;
    CString stmpFileName = ExtractFileName(_sFileName);
    bool bExtExists = false;
    bExtExists = (ExtractFileExt(_sFileName) != _T(""));

    CStringListEx EnPathList;
    CStringListEx EnExeList;

    GetEnvironmentPath(EnPathList);
    if (!bExtExists)
        GetEnvironmentPathExt(EnExeList);

    INT_PTR nIndex = 0;
    POSITION pos = EnPathList.FindIndex(nIndex);
    while (NULL != pos)
    {
        if (bExtExists)
        {
            if (FileExists(IncludeTrailingBackslash(EnPathList.GetAt(pos)) + stmpFileName))
            {
                return true;
            }
        } else 
        {
            INT_PTR nExtIndex = 0;
            POSITION posExt = EnExeList.FindIndex(nExtIndex);
            while (NULL != posExt)
            {
                if (FileExists(IncludeTrailingBackslash(EnPathList.GetAt(pos)) + stmpFileName + EnExeList.GetAt(posExt)))
                    return true;
                posExt = EnExeList.FindIndex(++nExtIndex);
            }
        }
        pos = EnPathList.FindIndex(++nIndex);
    }    

    return false;
}


/**
 @brief      파일에 pzMsg를 라인단위로 Write 한다. 다쓰고 난 다음에는 항상 끝을 가르킨다
 @author    hang ryul lee
 @date       2011.08.01
 @param     
*/
int FileWrite(char* pzFileName, char* pzMsg, bool bNext)
{

	HANDLE			hLogFile = NULL;
	DWORD			nByte = 0;
	char			zBuf[ 8192 * 5] = { 0,};
	
	if(bNext == true)
		_snprintf_s( zBuf, 8192 * 5, "%s\r\n", pzMsg);
	else
		_snprintf_s( zBuf, 8192 * 5, "%s", pzMsg);

	hLogFile = CreateFileA( pzFileName,
							GENERIC_WRITE,
							FILE_SHARE_READ,		// [!] 다른 프로세스에서 읽기 가능
							NULL,
							OPEN_ALWAYS,			// [!] 기존 파일이 있는경우 오픈
							FILE_ATTRIBUTE_NORMAL,
							NULL);

	if( hLogFile == INVALID_HANDLE_VALUE) {

		return -1;
	}

	SetFilePointer( hLogFile, 0, NULL, FILE_END); 

	if (!WriteFile( hLogFile, zBuf, strlen( zBuf), &nByte, NULL)) {
		CString strLog = _T("");
		strLog.Format(_T("writefile fail %d"), GetLastError());
		UM_WRITE_LOG(strLog);
		return 0;
	}

	CloseHandle( hLogFile);

	return 0;
}


/**
@brief     폴더인지 파일인지 확인
@author    hhh
@date       2013.04.066
@param     _strPath  경로
@return		TRUE- 파일, FALSE - 파일아님
*/
BOOL IsFile(CString _strPath)
{
	CString strLog;
	DWORD ret = GetFileAttributes(_strPath);
//	strLog.Format(_T("[Tray] _strPath : %s, ret : %d, GetLastError: %d"), _strPath, ret, GetLastError() );
//	UM_WRITE_LOG(strLog);

	if( ret != INVALID_FILE_ATTRIBUTES )
	{
		if(ret & FILE_ATTRIBUTE_DIRECTORY)
			return FALSE;
		else
			return  TRUE;
	}	
	
	return FALSE;

}

BOOL ExistPath(CString _strPath)
{
	BOOL bRet =FALSE;
	CFileFind dirFind;

	if( dirFind.FindFile(_strPath) )			
		bRet = TRUE;
	else
		bRet = FALSE;

	dirFind.Close();
	return bRet;
}


BOOL ResourceCreateFile(WORD _iResource, CString& _strFilePath, CString _strType)
{
	HRSRC hRes = NULL;

	hRes = FindResource(NULL, MAKEINTRESOURCE(_iResource), _strType);

	if (hRes == NULL) return FALSE;

	LPVOID mem = LoadResource (NULL, hRes);

	if (mem == NULL) return FALSE;

	DWORD dwBufferSize = SizeofResource(NULL, hRes);

	HANDLE handle = CreateFile (_strFilePath, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (handle == NULL || handle == INVALID_HANDLE_VALUE) 
	{
		return FALSE;
	}

	/* Error Handler ///////////////////////////////////////
	if (GetLastError() == ERROR_ALREADY_EXISTS) 
	{
	CloseHandle(handle);
	return TRUE;
	}*//////////////////////////////////////////////////////

	DWORD dwWriteSize = 0;

	BOOL bResult = WriteFile(handle, mem, dwBufferSize, &dwWriteSize, NULL);

	CloseHandle(handle);

	return bResult;
}







BOOL DropFile(DWORD nID ,CString desFile,CString strType)
{
	// 파일 떨어뜨리기 


	ResourceCreateFile((WORD)nID, desFile, strType);
	HANDLE hFile =  ::CreateFile( desFile , GENERIC_READ , FILE_SHARE_READ , NULL , OPEN_EXISTING , FILE_ATTRIBUTE_NORMAL , NULL ) ;


	if( hFile == INVALID_HANDLE_VALUE )
		return FALSE;

	DWORD dwFileSize = ::GetFileSize( hFile , NULL ) ;
	CloseHandle(hFile);

	//파일 존재확인 
	if(dwFileSize < 1)
		return FALSE;

	return TRUE;
}




BOOL DeCompressFile(CString srcFile, CString desPath)
{

	if(FALSE == ExistPath(srcFile))
		return FALSE;

	if(FALSE == ExistPath(desPath))
		ForceCreateDir(desPath);


	CString str7z = CPathInfo::GetClientInstallPath()+_T("7z.exe");
	CString str7zDll = CPathInfo::GetClientInstallPath()+_T("7z.dll");

	if(FALSE == ExistPath(str7z))
		return FALSE;

	if(FALSE == ExistPath(str7zDll))
		return FALSE;



	CString strCmd;
	strCmd.Format(_T("\"%s\" x \"%s\" -o\"%s\" -aoa"),str7z,srcFile,desPath);

	CImpersonator Impersonator;
	//BOOL bResult =Impersonator.RunAsExecuteProcess(str7z,strCmd,SW_HIDE,TRUE);

	DWORD dwRet=0;
	BOOL bResult =Impersonator.RunProcessAndWait(strCmd,CPathInfo::GetClientInstallPath(),&dwRet);
	Impersonator.Free();


	return bResult;

}

/**GeneralUtil.cpp
@fn			
@brief		파일의 날짜 정보를 얻어온다.
@author		hhh
@param		_strFilePath : 날짜 정보를 얻어올 파일 정보
@param		_tmCreat : 파일 생성 날짜 반환
@param		_tmAccess : 파일의 마지막 접근 날짜 반환
@param		_tmWrite :  파일의 마지막 수정 날짜 반환
@return		BOOL
@callgraph	@date			2013년 10월 25일
*/
BOOL GetFileTimeInfo(CString _strFilePath, CTime& _tmCreat, CTime& _tmAccess, CTime& _tmWrite, DWORD* _pdwError)
{

	HANDLE h_File = CreateFile( _strFilePath , GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL) ;
	if(h_File != INVALID_HANDLE_VALUE)
	{
		FILETIME create_time, access_time, write_time;
		// 지정된 파일에서 파일의 생성, 최근 사용 그리고 최근 갱신된 시간을 얻고
		GetFileTime(h_File, &create_time, &access_time, &write_time);
		SYSTEMTIME create_system_time, create_local_time;
		SYSTEMTIME access_system_time, access_local_time;
		SYSTEMTIME write_system_time, write_local_time;
    	
		FileTimeToSystemTime(&create_time, &create_system_time);			//filetime을 systemtime으로 변경
		SystemTimeToTzSpecificLocalTime(NULL, &create_system_time, &create_local_time);  //systemtime UTC이기때문에 시간을 지역시간으로 변환 

		FileTimeToSystemTime(&access_time, &access_system_time);			//filetime을 systemtime으로 변경
		SystemTimeToTzSpecificLocalTime(NULL, &access_system_time, &access_local_time);  //systemtime UTC이기때문에 시간을 지역시간으로 변환 

		FileTimeToSystemTime(&write_time, &write_system_time);			//filetime을 systemtime으로 변경
		SystemTimeToTzSpecificLocalTime(NULL, &write_system_time, &write_local_time);  //systemtime UTC이기때문에 시간을 지역시간으로 변환 

		CTime CreateFileTime(create_local_time);		
		_tmCreat = CreateFileTime;

		CTime AccessFileTime(access_local_time);
		_tmAccess = AccessFileTime;

		CTime WriteFileTime(write_local_time);
		_tmWrite = WriteFileTime;
		
		CloseHandle(h_File);
		return TRUE;
	}
	else
	{
		if(_pdwError)
		{
			*_pdwError = (DWORD)GetLastError();
		}
			
		return FALSE;
	}
			
}

/**GeneralUtil.cpp
@fn			
@brief		유니코드 파일을 읽어온다.(txt 해당)
@author		hhh
@param		_strPath : 읽어올 파일 풀 경로
@param		_strReadData : 읽어온 데이터
@param		_strSeparator : 한라인씩 읽어와서 통으로 만들때 넣어줄 구분자
@return		BOOL			실패, 성공
@callgraph	@date			2014년 1월 21일
*/
BOOL ReadUnicodeFile(CString _strPath, CString& _strReadData, CString _strSeparator)
{
	CFile file;
	CString strPath =_T("");
	CString strReadLine =_T("");
	//strPath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), STR_WINUPDATELIST_INFO);

	if(!file.Open(_strPath, CFile::modeRead)){		
		return FALSE;
	}
	
	CArchive ar(&file, CArchive::load);
	WORD wUnicodeHeader;
	ar >> wUnicodeHeader;
	if(wUnicodeHeader != 0xfeff)
	{		
		return FALSE;
	}

	//file.Seek(2,CFile::begin);

	CString strFileData = L"";
	
	while(ar.ReadString(strReadLine))
	{
		if(strReadLine.IsEmpty())
			continue;
		//to-do: 쿼리 수행시 에러가 날수 있으므로 예외처리를 추가해야 할듯하다.
		//strReadLine.Trim();
		strFileData += strReadLine;
		strFileData += _strSeparator;
		strReadLine=L"";
	}
	ar.Close();
	file.Close();
	
	_strReadData = strFileData;

	return TRUE;	 
}

/**GeneralUtil.cpp
@fn			
@brief		Ansi 파일을 읽어온다.(txt 해당)
@author		hhh
@param		_strPath : 읽어올 파일 풀 경로
@param		_strReadData : 읽어온 데이터
@param		_strSeparator : 한라인씩 읽어와서 통으로 만들때 넣어줄 구분자
@return		BOOL			실패, 성공
@callgraph	@date			2014년 1월 21일
*/
BOOL ReadAnsiFileTxt(CString _strPath, CString& _strReadData, CString _strSeparator)
{
	CString strTemp = L"", strReadData = L"";
	CStdioFile file;

	if( file.Open(_strPath, CFile::modeRead | CFile::typeText))
	{
		while(file.ReadString(strTemp))
		{
			_strReadData += strTemp;
			_strReadData += _strSeparator;
		}	
		file.Close();
		return TRUE;
	}
	else	
		return FALSE;
}

/**GeneralUtil.cpp
@fn			
@brief		파일을 쓴다.(txt해당)
@author		hhh
@param		_strPath : 읽어올 파일 풀 경로
@param		_strReadData : 읽어온 데이터
@param		_strSeparator : 한라인씩 읽어와서 통으로 만들때 넣어줄 구분자
@return		BOOL			실패, 성공
@callgraph	@date			2014년 1월 21일
*/
BOOL WriteFileTxt(CString _strOutPath, CString _strWriteData, UINT _nFlag)
{
	 CStdioFile sFile;
	 
	 if (sFile.Open(_strOutPath, _nFlag)) {  //CFile::modeCreate | CFile::modeWrite | CFile::typeText
		sFile.WriteString(_strWriteData);
		sFile.Close();
		return TRUE;
	 }
	 else
		 return FALSE;

}

BOOL WriteFileExample(CString _strPath, CString _strToWrite, DWORD& _dwBytesWritten, BOOL _bIsAppend)
{
	CString strLog = _T("");
	HANDLE hFile; 
	//char DataBuffer[] = "This is some test data to write to the file.";
	//DWORD dwBytesToWrite = (DWORD)strlen(DataBuffer);
	DWORD dwBytesToWrite = _strToWrite.GetLength();
	DWORD dwBytesWritten = 0;
	BOOL bErrorFlag = FALSE;

	char *pszBuffer = CConvert::ToChar(_strToWrite);
	//char *pszBuffer = new char[dwBytesToWrite];
	//ZeroMemory(pszBuffer, dwBytesToWrite);
	//memcpy(pszBuffer, _strToWrite.GetBuffer(dwBytesToWrite), dwBytesToWrite);
	//_strToWrite.ReleaseBuffer();

	int nProberty = GENERIC_WRITE;
	if (_bIsAppend){
		nProberty = FILE_APPEND_DATA;
	}
	hFile = CreateFile(_strPath,                // name of the write
		nProberty,          // open for writing
		FILE_SHARE_WRITE|FILE_SHARE_READ, // do share
		NULL,                   // default security
		OPEN_ALWAYS ,             // create new file only
		0,  // normal file
		NULL);                  // no attr. template

	if (hFile == INVALID_HANDLE_VALUE) 
	{ 
		//DisplayError(TEXT("CreateFile"));
		strLog.Format(_T("Terminal failure: Unable to open file \"%s\" for write.\n"), _strPath);
		UM_WRITE_LOG(strLog);
		return FALSE;
	}
	if (_bIsAppend) {
		SetEndOfFile(hFile);
	}

	strLog = _T("");
	strLog.Format(_T("Writing %d bytes to %s.\n"), dwBytesToWrite, _strPath);
	UM_WRITE_LOG(strLog);

	bErrorFlag = WriteFile( 
		hFile,           // open file handle
		pszBuffer,      // start of data to write
		dwBytesToWrite,  // number of bytes to write
		&dwBytesWritten, // number of bytes that were written
		NULL);            // no overlapped structure
	SE_MemoryDelete(pszBuffer);

	if (FALSE == bErrorFlag)
	{
		//DisplayError(TEXT("WriteFile"));
		strLog = _T("");
		strLog.Format(_T("Terminal failure: Unable to write to file.\n"));
		UM_WRITE_LOG(strLog);
		CloseHandle(hFile);
		return FALSE;
	}
	else
	{
		if (dwBytesWritten != dwBytesToWrite)
		{
			// This is an error because a synchronous write that results in
			// success (WriteFile returns TRUE) should write all data as
			// requested. This would not necessarily be the case for
			// asynchronous writes.
			strLog = _T("");
			strLog.Format(_T("Error: dwBytesWritten != dwBytesToWrite : %d, %d\n"), dwBytesWritten, dwBytesToWrite);
			UM_WRITE_LOG(strLog);
			CloseHandle(hFile);
			return FALSE;
		}
		else
		{
			// asynchronous writes.
			_dwBytesWritten = dwBytesWritten;
			strLog = _T("");
			strLog.Format(_T("Wrote %d bytes to %s successfully.\n"),  dwBytesWritten, _strPath);
			UM_WRITE_LOG(strLog);
			CloseHandle(hFile);
			return TRUE;
		}
	}
}

void DROP_TRACE_LOG(CString _strLogFile, CString _strToWrite) {
	CString strLogPath = CPathInfo::GetTransferLogFilePath();
	if (FALSE == FileExists(strLogPath))
		if (FALSE == ForceCreateDir(strLogPath))
			return;
	CString strLogFile = _T("");
	CString strLog = _T("");
	DWORD dwBytesWritten = 0;
	CTime cTime = CTime::GetCurrentTime();
	strLogFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _strLogFile);
	strLog.Format(_T("[%s][%d]%s\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), _strToWrite);
	WriteFileExample(strLogFile, strLog, dwBytesWritten, TRUE);
}

UINT GetDirectorySize(CString strDirPath) {
	UINT uFileSize = 0;
	BOOL bExsit;
	CFileFind cFileFind;

	bExsit = cFileFind.FindFile(strDirPath + _T("\\*.*"));
	while (bExsit)
	{
		bExsit = cFileFind.FindNextFileW();

		if (cFileFind.IsDots())
			continue;

		if (cFileFind.IsDirectory())
		{
			CString strNextDir = _T("");
			strNextDir = cFileFind.GetFilePath() + _T("\\*.*");
			GetDirectorySize(strNextDir);
		}
		else
		{
			uFileSize += (UINT)cFileFind.GetLength();
		}
	}
	cFileFind.Close();
	return uFileSize;
}

__int64 GetFileSize(CString _strFilePath)
{
	HANDLE hReadFile = CreateFile(_strFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (hReadFile == INVALID_HANDLE_VALUE)
	{
		// TODO 로그
		return FALSE;
	}

	//DWORD dwSize;
	__int64 nSize;
	LARGE_INTEGER li;
	li.LowPart = GetFileSize(hReadFile, (LPDWORD)&li.HighPart);
	nSize = (__int64)li.QuadPart;

	CloseHandle(hReadFile);

	return nSize;
}