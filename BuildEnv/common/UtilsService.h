
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UtilsService.h
 @brief     Service 제어 Utils

            Service 를 제어하는 유틸을 제공한다. 
 @author    odo kwon 
 @date      create 2011.03.17
*/

#pragma once

#include <windows.h>
#include <winsvc.h>
#include "UtilsFile.h"
#include "WinOsVersion.h"
#include "UtilsReg.h"

#ifdef UNICODE
    #define CHANGESERVICECONFIG  "ChangeServiceConfig2W"
#else
    #define CHANGESERVICECONFIG "ChangeServiceConfig2A"
#endif


typedef BOOL (WINAPI *fpChangeServiceConfig2)(SC_HANDLE hService, DWORD dwInfoLevel, LPVOID lpInfo);


#define SERVICE_CONTROL_HAURISVC_STOP   200        /**< 하우리 서비스 stop 코드*/
// 서버 
#define SERVICE_UNKNOWN                 0x00000010 /**< 서비스 Type을 모를경우 리턴되는값*/

#define MAXLEN_SERVICENAME              182        /**< 서비스 이름 최대 길이NT이상OS (NULL 종료문자를 제외한 최대 길이)      */
#define MAXLEN_NTSERVICENAME            80         /**< 서비스 이름 최대 길이 NT에서만(NULL 종료문자를 제외한 최대 길이)      */

#define MAXLEN_SERVICEDISPNAME          256     /**< 서비스 표시 최대 길이(NULL 종료문자를 제외한 최대 길이) */
#define MAXLEN_SERVICEDICRIPTION        256     /**< 서비스 설명 최대 길이(NULL 종료문자를 제외한 최대 길이) */
//서비스 시작,중지시 Pennding wait time
#define SERVICE_WAITTIME                10      /**< 서비스 상태변경시 Pending WaitTime시간 */
#define SERVICE_EVENTLOGREG             _T("SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application") /**< 이벤트로그 등록 reg */

//서비스 존재 유무
bool ExistsService(const CString &_sSvcName);
//서비스 상태 얻어오기
DWORD GetServiceStatus(const CString &_sSvcName);

//서비스 등록
bool RegisterServices(const CString &_sSvcFile, const CString &_sSvcName, const CString &_sDisplayName, CString &_sDescription, CString &_sDependent, DWORD _dwServiceType = SERVICE_DEMAND_START);
//서비스삭제
bool UnRegisterServices(const CString &_sSvcName, DWORD *_pdwErrorCode = NULL);

//서비스 제어
//서비스 시작
bool RunService(const CString &_sSvcName, DWORD *_pdwErrorCode = NULL);
//서비스 중지
bool StopService(const CString &_sSvcName, DWORD *_pdwErrorCode = NULL);
//서비스 일시중지
bool PauseService(const CString &_sSvcName, DWORD *_pdwErrorCode = NULL);
//하우리 서비스 중지
bool StopHauriService(const CString &_sSvcName);

//서비스명 유효성 체크
bool CheckVaildServiceName(const CString &_sServiceName);
//서비스로 Controll Code 전송
bool SetControlCodeToService(const CString &_sSvcName, const DWORD _dwControlCode, DWORD *_pdwErrorCode = NULL);
// 서비스 Start Type를 얻어오는 함수(수동,자동,사용안함)
DWORD GetServiceStartType(const CString &_sSvcName);

//서비스 이벤트로그 등록
bool RegisterServiceEventLog(const CString &_sServiceName, const CString &sMsgFile);
//서비스 이벤트로그 삭제
bool UnRegisterServiceEventLog(const CString &_sServiceName);
bool AddServiceEventLog(const CString &_sServiceName, const DWORD _dwEventID, const WORD _wEventType =  EVENTLOG_SUCCESS, const WORD _wCategory = 0, LPCTSTR* _plpStrings = NULL, WORD _wNumlpStrings = 0);
