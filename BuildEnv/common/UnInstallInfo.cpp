
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UnInstallInfo.cpp 
 @brief     CUnInstallInfo class 구현 파일

            Registry UnInstall 관련 값 얻음

 @author    hang ryul lee 
 @date      create 2007.10.22  
*/
#include "stdafx.h"
#include "UnInstallInfo.h"
#include "WinOSVersion.h"
#include "UtilsReg.h"

/********************************************************************************
 @class     CUnInstallInfo 
 @brief     UnInstall Registry 관련 정보 얻는 class 

 @note      .각각의 UnInstall 레지스트리 상의 값을 구할 수 있다. \n
            .UnInstall을 수행할 수 있다.
*********************************************************************************/
#define REG_KEY_UNINSTALL       _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\")
#define REG_KEY_UNINSTALL_WOW64 _T("SOFTWARE\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\")

/**
 @brief     기본생성자
 @author    odkwon
 @date      create 2007.11.07
*/
CSoftwareData::CSoftwareData()
{
    m_sKeyName          = _T("");
    m_sDisplayName      = _T("");
    m_sDisplayVersion   = _T("");
    m_sPublisher        = _T("");
    m_sInstallDate      = _T("");
    m_sInstallLocation  = _T("");
    m_sHelpLink         = _T("");
    m_sURLUpdateInfo    = _T("");
    m_sComments         = _T("");
    m_sContact          = _T("");
    m_sModifyPath       = _T("");
    m_sReadme           = _T("");
    m_sUninstallString  = _T("");
    m_sDisplayIcon      = _T("");
    m_bIsWow64          = false;       
}

/**
 @brief     기본파괴자
 @author    odkwon
 @date      create 2007.11.07
*/
CSoftwareData::~CSoftwareData()
{

}

/**
 @brief     Data를 복사한다.
 @author    odkwon
 @date      create 2007.11.07
 @param     [IN] _rSoftwareData 복사할 소프트웨어 DATA 클래스
*/
void CSoftwareData::CopyData(const CSoftwareData &_rSoftwareData)
{
    m_sDisplayName     = _rSoftwareData.m_sDisplayName;
    m_sKeyName         = _rSoftwareData.m_sKeyName;
    m_sDisplayVersion  = _rSoftwareData.m_sDisplayVersion;
    m_sPublisher       = _rSoftwareData.m_sPublisher;
    m_sInstallDate     = _rSoftwareData.m_sInstallDate;
    m_sInstallLocation = _rSoftwareData.m_sInstallLocation;
    m_sHelpLink        = _rSoftwareData.m_sHelpLink;
    m_sURLUpdateInfo   = _rSoftwareData.m_sURLUpdateInfo;
    m_sComments        = _rSoftwareData.m_sComments;
    m_sContact         = _rSoftwareData.m_sContact;
    m_sModifyPath      = _rSoftwareData.m_sModifyPath;
    m_sReadme          = _rSoftwareData.m_sReadme;
    m_sUninstallString = _rSoftwareData.m_sUninstallString;
    m_sDisplayIcon     = _rSoftwareData.m_sDisplayIcon;
    m_bIsWow64         = _rSoftwareData.m_bIsWow64;
}

/**
 @brief     스트림에서 DATA클래스로 값을 로드한다.
 @author    odkwon
 @date      create 2007.11.07
 @param     [IN] _pStream 로드할 Stream
 @return    true/false
*/
bool CSoftwareData::LoadFromStream(CMemFileEx *_pStream)
{ 
    if (NULL == _pStream) 
            return false;
    try
    {
        _pStream->ReadString(m_sDisplayName);
        _pStream->ReadString(m_sUninstallString);
        _pStream->ReadString(m_sDisplayVersion);
        _pStream->ReadString(m_sPublisher);
        _pStream->ReadString(m_sInstallDate);
        _pStream->ReadString(m_sComments);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

/**
 @brief     DATA클래스의 Dtat를 Stream에 저장한다.
 @author    odkwon
 @date      create 2007.11.07
 @param     [IN] _pStream 저장할 Stream
 @return    true/false
*/
bool CSoftwareData::SaveToStream(CMemFileEx *_pStream) 
{ 
    if (NULL == _pStream) 
        return false;
    try
    {
        _pStream->WriteString(m_sDisplayName);
        _pStream->WriteString(m_sUninstallString);
        _pStream->WriteString(m_sDisplayVersion);
        _pStream->WriteString(m_sPublisher);
        _pStream->WriteString(m_sInstallDate);
        _pStream->WriteString(m_sComments);

        return true;
    }
    catch(...)
    {
        return false;
    }
}


/**
 @brief     기본생성자
 @author    hang ryul lee
 @date      create 2007.10.22
 @note      UnInstall Key List 초기화 \n
 @note      64bit Os이면서 프로그램 실행이 wow64로 돌지 않는다면 올바른 64bit 프로그램이다.\n
            -후에 Registry 값을 읽을 때 main UnInstall 부분과 Wow64 UnInstall 부분을 둘다 읽어야한다.\n
            -64bit OS 이면서 프로그램이 wow64로 실행되었다면 main UnIstall로의 접근이\n
             자동으로 wow64쪽으로 전환된다.\n
*/
CSoftwareInfoList::CSoftwareInfoList()
{
    m_bIs64bit = false;

    CWinOsVersion osVer;
    if (osVer.Is64bit())
        if (!osVer.IsWow64())
            m_bIs64bit = true;
}

/**
 @brief     소멸자
 @author    hang ryul lee
 @date      create 2007.10.22
 @note      Key List 삭제
*/
CSoftwareInfoList::~CSoftwareInfoList()
{
    RemoveAll();
    m_KeyList.RemoveAll();
    m_Wow64KeyList.RemoveAll();
}

/**
 @brief     List 삭제
 @author    hang ryul lee
 @date      create 2007.10.22
 @note      구성되어 있는 리스트 삭제
*/
void CSoftwareInfoList::RemoveAll()
{
    m_SoftwareList.RemoveAll();
 }

/**
 @brief     UnInstall Registry의 모든 값 read
 @author    hang ryul lee
 @date      create 2007.10.22
 @return    true/false
 @note      64bit Os인 경우 Wow64부분의 UnInstall Registry 값도 함께 read한다.
*/
bool CSoftwareInfoList::GetSwInfos()
{
    RemoveAll();
    ReadUnInstKeyList();

    ReadAllUnInstValueByKey(REG_KEY_UNINSTALL, m_KeyList);
    if (m_bIs64bit)
        ReadAllUnInstValueByKey(REG_KEY_UNINSTALL_WOW64, m_Wow64KeyList);

    return true;
}

/**
 @brief     Key값을 이용하여 SWInfo를 모두 읽어 들인다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sKey      :Main UNINSTALL RegKey 경로 / WOW64 UNINSTALL RegKey 경로
 @param     [in] _sKeyList  :SW subKey List
 @return    true/false
*/
bool CSoftwareInfoList::ReadAllUnInstValueByKey(const CString &_sKey, const CStringList &_sKeyList)
{
    CString sKey = _sKey;
    sKey = sKey.Trim();
    if (_T("") == sKey) 
        return false;

    //구해진 전체 KeyList에 대해서 Item 정보 구함
    POSITION pos = _sKeyList.GetHeadPosition();
    while (NULL != pos)
    {
        CSoftwareData SWInfo;
        if (ReadUnInstValue(sKey, _sKeyList.GetNext(pos), SWInfo))
        {
           m_SoftwareList.AddTail(SWInfo);
        }
    }
    return false;
}

/**
 @brief     KeyList를 구한다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @return    true/false
 @note      64bit os 의 경우 WOW64쪽 UNINSTALL Key 리스트도 읽어둔다.
*/
bool CSoftwareInfoList::ReadUnInstKeyList()
{
    bool bResult = false;

    if (GetRegSubKeyList(HKEY_LOCAL_MACHINE, REG_KEY_UNINSTALL, m_KeyList))
        bResult = true;
    
    if (m_bIs64bit)
        if (GetRegSubKeyList(HKEY_LOCAL_MACHINE, REG_KEY_UNINSTALL_WOW64, m_Wow64KeyList))
            bResult = true;

    return bResult;
}

/**
 @brief     레지스트리에서 SW_INFO 각값을 읽어온다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sUnInstKey    :REG_KEY_UNINSTALL or REG_KEY_UNINSTALL_WOW64 
 @param     [in] _sSWKey        :subKey
 @param     [out] _SWInfo       :정보를 담을 SOFTWAREINFO 구조체
 @return    true / false
 @note      .다음과 같은 경우에는 리스트에 저장하지 않는다.\n
            .'SystemComponent' 값 = 1 일 경우\n
            .'DisplayName = "" 일 경우
*/
bool CSoftwareInfoList::ReadUnInstValue(const CString &_sUnInstKey, const CString &_sSWKey, CSoftwareData &_SWInfo) const
{
    CString sUnInstKey = _sUnInstKey;
    CString sSWKey = _sSWKey;
    sUnInstKey = sUnInstKey.Trim();
    sSWKey = sSWKey.Trim();

    if ((_T("") == sUnInstKey) || (_T("") == sSWKey)) 
        return false;

    CString sKey = sUnInstKey +sSWKey; 
    DWORD dwComponent = GetRegDWORDValue(HKEY_LOCAL_MACHINE, sKey, _T("SystemComponent"));

    if (0 == dwComponent)
    {
        //DisplayName = ""일 경우에는 리스트에 저장하지 않음.
        _SWInfo.m_sDisplayName = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("DisplayName"));
        if (_SWInfo.m_sDisplayName.Trim() == "") return false;

        _SWInfo.m_sKeyName = _sSWKey;
        _SWInfo.m_sDisplayVersion  = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("DisplayVersion"));
        _SWInfo.m_sPublisher       = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("Publisher"));
        _SWInfo.m_sInstallDate     = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("InstallDate"));
        _SWInfo.m_sInstallLocation = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("InstallLocation"));
        _SWInfo.m_sHelpLink        = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("HelpLink"));
        _SWInfo.m_sURLUpdateInfo   = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("URLUpdateInfo"));
        _SWInfo.m_sComments        = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("Comments"));
        _SWInfo.m_sContact         = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("Contact"));
        _SWInfo.m_sModifyPath      = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("ModifyPath"));
        _SWInfo.m_sReadme          = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("Readme"));
        _SWInfo.m_sUninstallString = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("UninstallString"));
        _SWInfo.m_sDisplayIcon     = GetRegStringValue(HKEY_LOCAL_MACHINE, sKey, _T("DisplayIcon"));

        if (0 == sUnInstKey.CompareNoCase(REG_KEY_UNINSTALL_WOW64)) _SWInfo.m_bIsWow64 = true;
        else _SWInfo.m_bIsWow64 = false;

        return true;
    }
    else 
        return false;
}

/**
 @brief     해당 index에 해당하는 SW INFO 구조체의 값을 전해준다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in]_nIndex     :구하고자 하는 SW의 Index
 @param     [out]_SWInfo    :정보 저장할 SORTWAREINFO 구조체
 @return    true / false
*/
bool CSoftwareInfoList::GetUnInstInfoByIndex(const INT _nIndex, CSoftwareData &_SWInfo) const
{
    CSoftwareData *SWInfo = m_SoftwareList.GetItem(_nIndex);
    _SWInfo.CopyData(*SWInfo);
    return true;
}

/**
 @brief     정보를 구한 SW의 갯수를 구한다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @return    SWList Count
*/
INT_PTR  CSoftwareInfoList::GetCount() const
{
    return m_SoftwareList.GetCount();
}

/**
 @brief     리스트를 스트림으로 저장한다.
 @author    odkwon
 @date      create 2011.11.07
 @param     [out] _pStream   저장할 스트림클래스
*/
bool CSoftwareInfoList::SaveToStream(CMemFileEx *_pStream) 
{
    return m_SoftwareList.SaveToStream(_pStream);
}

/**
 @brief     스트림에서 리스트를 로드한다.
 @author    odkwon
 @date      create 2011.11.07
 @param     [out] _pStream   저장할 스트림클래스
*/
bool CSoftwareInfoList::LoadFromStream(CMemFileEx *_pStream)
{
    return m_SoftwareList.LoadFromStream(_pStream);
}


/**
 @brief     해당 프로그램이 설치되어 있는 지 확인한다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sSWName   :program name
 @return    true / false
 @note      KeyList 또는 Display Name List에 program name이 존재할 경우 설치되어 있다고 판단.
*/
bool CSoftwareInfoList::IsInstalledProgram(const CString &_sSWName) const
{
    CString sSWName = _sSWName;
    sSWName = sSWName.Trim();
    if (_T("") == sSWName) return false;

    if (IsExistUnInstRegKey(sSWName))
        return true;
    else if (IsExistDisplayName(sSWName))
        return true;

    return false;
}

/**
 @brief     Registry Key List에서 해당 값이 존재하는지 찾는다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sSubKey   :찾을 key
 @return    true / false
*/
bool CSoftwareInfoList::IsExistUnInstRegKey(const CString &_sSubKey) const
{
    CString sSubKey = _sSubKey;
    sSubKey = sSubKey.Trim();
    if (_T("") == sSubKey) return false;

    bool bResult = false;
    CSoftwareData *pSWInfo = NULL;

    POSITION pos = m_SoftwareList.GetHeadPosition();
    while (NULL != pos)
    {
        pSWInfo = m_SoftwareList.GetNext(pos);

        if (0 == sSubKey.CompareNoCase(pSWInfo->m_sKeyName))
        {
            bResult = true;
            break;
        }
    }

    return bResult;
}

/**
 @brief     해당값과 동일한 Display Name이 존재하는지 찾는다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sDPName   :찾을 Display Name
 @return    true / false
*/
bool CSoftwareInfoList::IsExistDisplayName(const CString &_sDPName) const
{
    CString sDPName = _sDPName;
    sDPName = sDPName.Trim();
    if (_T("") == sDPName) 
        return false;

    bool bResult = false;
    CSoftwareData *pSWInfo = NULL;

    POSITION pos = m_SoftwareList.GetHeadPosition();
    while (NULL != pos)
    {
        pSWInfo = m_SoftwareList.GetNext(pos);     
        if (NULL == pSWInfo) 
            continue;

        if (0 == sDPName.CompareNoCase(pSWInfo->m_sDisplayName))
        {
            bResult = true;
            break;
        }
    }

    return bResult;
}

/**
 @brief     UnInstall을 진행한다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _SWName    :삭제하려는 프로그램의 Display Name
 @return    true / false
 @note      해당 프로그램의 Display Name으로만 삭제가 가능하다.
*/
bool CSoftwareInfoList::RunUnInstall(const CString &_sSWName, bool _bWait /* = false */) const
{
    CString sSWName = _sSWName;
    sSWName = sSWName.Trim();
    if (_T("") == sSWName) 
        return false;

    CString sUnInstPath = GetUnInstPathByDPName(sSWName);
    if (_T("") == sUnInstPath) 
        return false;

    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    ::ZeroMemory(&si, sizeof(si));
    ::ZeroMemory(&pi, sizeof(pi));

    si.cb = sizeof(si);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow= SW_SHOWNORMAL;

    LPTSTR szStr = sUnInstPath.GetBuffer(0);
    sUnInstPath.ReleaseBuffer();

    BOOL error = ::CreateProcess(NULL, szStr, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
    if ( 0 == error )
        return false;
    if (_bWait)
        ::WaitForSingleObject(pi.hProcess, INFINITE);
    // Close process and thread handles. 
    ::CloseHandle( pi.hProcess );
    ::CloseHandle( pi.hThread );

    return true;
}

/**
 @brief     Display Name을 이용하여 UnInstall Path를 구한다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sDPName   :지우려는 프로그램의 Display Name
 @return    UnInstallString
*/
CString CSoftwareInfoList::GetUnInstPathByDPName(const CString &_sDPName) const
{
    CString sDPName = _sDPName;
    sDPName = sDPName.Trim();
    if (_T("") == sDPName) 
        return false;

    CString sResult = _T("");
    CSoftwareData *pSWInfo = NULL;

    POSITION pos = m_SoftwareList.GetHeadPosition();
    while (NULL != pos)
    {
        pSWInfo = m_SoftwareList.GetNext(pos);
        if (NULL == pSWInfo) 
            continue;

        if (0 == sDPName.CompareNoCase(pSWInfo->m_sDisplayName))
        {
            sResult = pSWInfo->m_sUninstallString;
            break;
        }
    }
    return sResult;
}

/**
 @brief     Key를 이용하여 UnInstall Registry에서 string 값을 구한다.
 @author    hang ryul lee
 @date      create 2007.10.22
 @param     [in] _sKey          :구할 SW의 sub Key
 @param     [in] _sValueName    :구할 정보의 ValueName
 @return    StringValue
 @note      .주어진 key값이 _sKey값을 이용하여 SW정보를 찾는다. \n
             -해당 값이 Main UNISTALL정보인지, Wow64 UNINSTALL 정보인지 알아낸다.\n
            .적절한 경로에서 원하는 값을 구한다.
*/
CString CSoftwareInfoList::GetUnInstStrValueByKey(const CString &_sKey, const CString &_sValueName) const
{
    CString sResult = _T("");
    CSoftwareData *pSWInfo = NULL;
    CString sFullKey = _T("");

    CString sKey = _sKey;
    sKey = sKey.Trim();
    if (_T("") == sKey) 
        return sResult;

    POSITION pos = m_SoftwareList.GetHeadPosition();
    while (NULL != pos)
    {
        pSWInfo = m_SoftwareList.GetNext(pos);
        if (NULL == pSWInfo) 
            continue;

        if (0 == sKey.CompareNoCase(pSWInfo->m_sKeyName))
        {
            if (pSWInfo->m_bIsWow64)
                sFullKey = REG_KEY_UNINSTALL_WOW64+sKey;
            else
                sFullKey = REG_KEY_UNINSTALL+sKey;
            break;
        }
    }

    sResult = GetRegStringValue(HKEY_LOCAL_MACHINE, sFullKey, _sValueName);

    return sResult;
}

/**
 @brief     Key를 이용하여 UnInstall Registry에서 DWORD 값을 구한다.
 @author    hang ryul lee
 @date      create 2011.03.10
 @param     [in] _sKey      :구할 SW의 sub Key
 @param     [in] _sValueName:구할 정보의 ValueName
 @return    DWORD Value
 @note      .주어진 key값이 _sKey값을 이용하여 SW정보를 찾는다. \n
             -해당 값이 Main UNISTALL정보인지, Wow64 UNINSTALL 정보인지 알아낸다.\n
            .적절한 경로에서 원하는 값을 구한다.
*/
DWORD CSoftwareInfoList::GetUnInstDWORDValueByKey(const CString &_sKey, const CString &_sValueName) const
{
    DWORD dwResult = 0;

    CSoftwareData *pSWInfo = NULL;
    CString sFullKey = _T("");

    CString sKey = _sKey;
    sKey = sKey.Trim();
    if (_T("") == sKey) 
        return dwResult;

    POSITION pos = m_SoftwareList.GetHeadPosition();
    while (NULL != pos)
    {
        pSWInfo = m_SoftwareList.GetNext(pos);
        if (NULL == pSWInfo) 
            continue;

        if (0 == sKey.CompareNoCase(pSWInfo->m_sKeyName))
        {
            if (pSWInfo->m_bIsWow64)
                sFullKey = REG_KEY_UNINSTALL_WOW64+sKey;
            else
                sFullKey = REG_KEY_UNINSTALL+sKey;
            break;
        }
    }

    dwResult = GetRegDWORDValue(HKEY_LOCAL_MACHINE, sFullKey, _sValueName);

    return dwResult;
}

/**
 @brief     설치되어 있는 SW의 List를 구한다.
 @author    hang ryul lee
 @date      create 2011.03.10
 @param     [out] _sDPList  :DiplayName List
 @note      .넘겨받은 String List에 DPName List를 저장한다.
*/
void CSoftwareInfoList::GetDPNameList(CStringList &_sDPList) const
{
    CSoftwareData *pSWInfo = NULL;

    if (0 <_sDPList.GetCount()) _sDPList.RemoveAll();

    POSITION pos = m_SoftwareList.GetHeadPosition();
    while (NULL != pos)
    {
        pSWInfo = m_SoftwareList.GetNext(pos);
        if (NULL == pSWInfo) 
            continue;

        _sDPList.AddTail(pSWInfo->m_sDisplayName);
    }
}


/**
 @brief     특정인덱스에 소프트웨어 정보를 리턴한다.
 @author    odkwon
 @date      create 2011.10.06
 @param     [in] _dwIndex        수집할 소프트웨어 인덱스
 @return    SOFTWARE_INFO 포인터
*/
CSoftwareData *CSoftwareInfoList::GetItem(const INT_PTR _nIndex) const
{
    return m_SoftwareList.GetItem(_nIndex);
}


/**
 @brief     소프트웨어 리스트에 소프트웨어 정보를 추가한다.
 @author    odkwon
 @date      create 2007.10.27
 @note      서버에서 DB 정보를 이용하여 소프트웨어 리스트에 Insert할때쓴다.
*/
void CSoftwareInfoList::AddTail(CSoftwareData &_rSoftwareInfo)
{
    m_SoftwareList.AddTail(_rSoftwareInfo);
}

/**
 @brief     해당 포지션에 Data 클래스 포인터를 리턴한다.
 @author    odkwon
 @date      create 2007.10.27
*/
CSoftwareData * CSoftwareInfoList::GetNext(POSITION &_rCurrentPostion) const
{
    return m_SoftwareList.GetNext(_rCurrentPostion);
}

/**
 @brief     소프트웨어 리스트에 해드포지션값을 리턴한다.
 @author    odkwon
 @date      create 2007.10.27
*/
POSITION CSoftwareInfoList::GetHeadPosition() const
{
    return m_SoftwareList.GetHeadPosition();
}