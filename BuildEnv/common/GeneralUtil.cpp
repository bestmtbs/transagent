/**
@file						GeneralUtil.cpp
@project				ITCMS
@product company	DSNTCH  digital solution & technology partner
@url						www.dsntech.com
@division				ITCMS Div
@date					2013년 4월 30일
*/

#include "stdafx.h"
#include "generalUtil.h"
#include <io.h>
#include <Psapi.h>
#include <strsafe.h>
#include <shlwapi.h>
#include <tlhelp32.h> 
#include <sddl.h>
#include <winioctl.h>

#pragma comment(lib, "psapi.lib")
#pragma comment(lib, "Mpr.lib")// Kevin(2013-9-24)

#define  PATTERN_GENERAL_TYPE							0x00000001  // 주민 번호
#define  PATTERN_PASSPORT_TYPE						0x00000002   // 여권 번호			
#define  PATTERN_DRIVER_TYPE							0x00000003   // 운전 면허
#define  PATTERN_TELEPHONE_TYPE                      0x00000004   // 전화 번호 
#define  PATTERN_EMAIL_TYPE								0x00000005   // 이메일
#define  PATTERN_FOREIGN_TYPE							0x00000006   // 외국인 등록번호
#define  PATTERN_BANK_TYPE								0x00000007   //계좌번호
#define  PATTERN_CARD_TYPE								0x00000008   //신용카드
#define	 KEYWORD_TYPE										0x00000100   //키워드

/**
@fn			AllocVirtual(
				DWORD dwSize)
@brief		
@param		dwSize
@return		LPVOID
@sa			
@callgraph	
*/
LPVOID AllocVirtual(DWORD dwSize)
{
	return ::VirtualAlloc(NULL,dwSize,MEM_RESERVE|MEM_COMMIT,PAGE_READWRITE);
}
/**
@fn			FreeVirtual(
				LPVOID pMem)
@brief		
@param		pMem
@return		BOOL
@sa			
@callgraph	
*/
BOOL FreeVirtual(LPVOID pMem)
{
	return ::VirtualFree(pMem,0,MEM_RELEASE);
}

// Kevin(2011-5-24)
/**
@fn			GetVirtualSize(
				LPCVOID *lpBuffer)
@brief		할당한 메모리의 실제 Block size를 리턴하는 함수
@param		*lpBuffer
@return		ULONG
@sa			
@callgraph	
*/
ULONG GetVirtualSize(LPCVOID *lpBuffer)
{
	MEMORY_BASIC_INFORMATION VirtualInfo;
	ZeroMemory((void *)&VirtualInfo, sizeof(MEMORY_BASIC_INFORMATION));
	VirtualQuery(lpBuffer, &VirtualInfo, sizeof(MEMORY_BASIC_INFORMATION));

	return (ULONG)VirtualInfo.RegionSize;
}


/**
@fn			GetWinVer()
@brief		윈도우 Version을 얻는함수
@return		int
@sa			
@callgraph	
*/
int GetWinVer()
{
	BOOL bResult = FALSE;
	OSVERSIONINFO vi = { sizeof(vi) };

	::GetVersionEx(&vi);
	if( vi.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		if(	 vi.dwMajorVersion >= VISTA_MAJOR_VERSION  &&	 vi.dwMinorVersion >= VISTA_MINOR_VERSION )
		{
			return WINVISTA;
		}
		else if(vi.dwMajorVersion == 5 && vi.dwMinorVersion == 2 )
		{
			return WIN2K3;
		}
		else if ( vi.dwMajorVersion == 5 && vi.dwMinorVersion == 1 )
		{
			return WINXP;
		}
	}
	return WINXP;
}
int GetWinVer2()
{
	BOOL bResult = FALSE;
	OSVERSIONINFOEX vi = { sizeof(vi) };

	::GetVersionEx((LPOSVERSIONINFOW)&vi);
	if( vi.dwPlatformId == VER_PLATFORM_WIN32_NT)
	{
		if(	 vi.dwMajorVersion == VISTA_MAJOR_VERSION)
		{
			if(vi.dwMinorVersion >= OS_MINOR_WIN81_2K12R2 )
			{
				if(vi.wProductType == WIN8)
					return WIN81;
				else
					return WIN2K12R2;
			}
			else if(vi.dwMinorVersion >= OS_MINOR_WIN8_2K12 )
			{
				if(vi.wProductType == WIN8)
					return WIN8;
				else
					return WIN2K12;
			}
			else if(vi.dwMinorVersion >= OS_MINOR_WIN7_2K8 )
			{
				if(vi.wProductType == VER_NT_WORKSTATION)
					return WIN7;
				else
					return WIN2K8R2;
			}
			else if(vi.dwMinorVersion >= VISTA_MINOR_VERSION )
			{
				if(vi.wProductType == VER_NT_WORKSTATION)
					return WINVISTA;
				else
					return WIN2K8;
			}
		}
		else if(vi.dwMajorVersion == 5 && vi.dwMinorVersion == 2 )
		{
			return WIN2K3;
		}
		else if ( vi.dwMajorVersion == 5 && vi.dwMinorVersion == 1 )
		{
			return WINXP;
		}
	}
	return WINXP;
}
// Kevin(2013-4-18)
/**
@fn			CConvert::UnicodeToAnsi(
				WCHAR *pszW,
				CHAR *pszA)
@brief		Convert.cpp
@param		*pszW
@param		*pszA
@return		int
@sa			
@callgraph	@date			2013년 4월 18일
*/
int UnicodeToAnsi3(WCHAR *pszW, CHAR *pszA)
{

    ULONG cbAnsi=0, cCharacters = lstrlen(pszW);
    DWORD dwError;
	int nRet=0;

    nRet = WideCharToMultiByte(CP_ACP, 0, pszW, cCharacters, pszA, cbAnsi, NULL, NULL);

    // Convert to ANSI.
    if (0 == WideCharToMultiByte(CP_ACP, 0, pszW, cCharacters, pszA, nRet, NULL, NULL))
    {
        dwError = GetLastError();
        return -1;//HRESULT_FROM_WIN32(dwError);
    }
    return nRet;
}

// Kevin(2013-4-18)
/**
@fn			CConvert::AnsiToUnicode(
				char *strAnsi,
				TCHAR *strUnicode)
@brief		Convert.cpp
@param		*strAnsi
@param		*strUnicode
@return		int
@sa			
@callgraph	@date			2013년 4월 18일

*/
int AnsiToUnicode3(char *strAnsi, TCHAR *strUnicode)
{
	BOOL bRet = TRUE;
	DWORD lenA;
	lenA = (DWORD)strlen(strAnsi);

	DWORD lenW = ::MultiByteToWideChar(CP_ACP, 0, strAnsi, lenA, 0, 0);
	if (lenW > 0)
	{
	  // Check whether conversion was successful
	  ::MultiByteToWideChar(CP_ACP, 0, strAnsi, lenA, (LPWSTR)strUnicode, lenW);
	}
	else
	{
	  // handle the error
        DWORD dwError = GetLastError();
        return -1;//HRESULT_FROM_WIN32(dwError);

	}

	return lenW;
}



/**
@fn			CheckPlatform()
@brief		
@return		int
@sa			
@callgraph	
*/
int GetPlatform()
{
	int nRet = FALSE;
	SYSTEM_INFO si;
	ZeroMemory(&si,sizeof(SYSTEM_INFO));

	::GetNativeSystemInfo(&si);
	if(si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_INTEL)
	{
		//UM_WRITE_LOG(_T("System Intel ARCHITECTURE X86!!!");
		nRet = PLATFORM_X86;
	}
	if(si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_AMD64)
	{
		//UM_WRITE_LOG(_T("System AMD64 ARCHITECTURE Common X64!!!");
		nRet = PLATFORM_X64;
	}
	if(si.wProcessorArchitecture==PROCESSOR_ARCHITECTURE_IA64)
	{
		//UM_WRITE_LOG(_T("System IA64 ARCHITECTURE Common Intel X64!!!--not Support CPU Terminate");
		nRet = PLATFORM_IA64;
	}

	return nRet;
}

/**GeneralUtil.cpp
@fn			GetCPUCount()
@brief		
@return		int
@sa			
@callgraph	@date			2013년 6월 11일
*/
int GetCPUCount()
{
	int nRet = FALSE;
	SYSTEM_INFO si;
	ZeroMemory(&si,sizeof(SYSTEM_INFO));

	::GetNativeSystemInfo(&si);
	nRet = si.dwNumberOfProcessors;

	return nRet;
}

/**GeneralUtil.cpp
@fn			CPUCountSleep(
				ULONG ulPrevCount,
				ULONG ulInterval,
				ULONG ulSleep)
@brief		CPU성능에 따른 점유율 감소 함수
@param		ulPrevCount : 이전 tickcount 최초 0으로 초기화 해두면됨
@param		ulInterval : INTERVAL = 1 sec - 매 설정한 초마다 아래 sleep값으로 sleep동작
@param		ulSleep : Sleep 값 = 2 msec
@return		int
@sa			
@callgraph	@date			2013년 10월 28일
*/
void CPUCountSleep(ULONG *pulPrevCount, ULONG ulInterval, ULONG ulSleep)
{
	DBGLOG(L"CPUCountSleep [%d:%d:%d]", *pulPrevCount, ulInterval, ulSleep);

	// windows vista이면
	int nVer =GetWinVer2();
	// Kevin(2013-10-28)
	static int nCPUCnt = 0;
	// cpu 2개 이하인 경우 sleep 코드 추가
	// Sleep의 동작 조건 추가
	if(nCPUCnt == 0)
	{
		nCPUCnt = GetCPUCount();
	}

	// OS VERSION이 WINVISTA 인경우에대한 처리
	if(nVer == WINVISTA)
	{
		nCPUCnt = 2;
	}

	if(nCPUCnt <= 2)
	{
		ULONG ulTickCount = GetTickCount();
		if(*pulPrevCount == 0)
			*pulPrevCount = ulTickCount;

		DWORD dwTime = ulTickCount - *pulPrevCount;
		dwTime %= 1000; // ms 단위 값으로 변경
		if(dwTime == 0)
			dwTime = 1000;

		DBGLOG(L"CPUCountSleep after dwTime = %d", dwTime);
		if(dwTime >= ulInterval) // 1초가 경과한 경우 
		{
			DBGLOG(L"CPUCountSleep after dwTime = %d", dwTime);
			*pulPrevCount = ulTickCount;
			if(ulSleep > 0)
				Sleep(ulSleep); // default = 2ms
			//Sleep(2);
		}
	}
}

void CountSleep(ULONG *pulPrevCount, ULONG ulInterval, ULONG ulSleep)
{
	DBGLOG(L"CountSleep [%d:%d:%d]", *pulPrevCount, ulInterval, ulSleep);


	// 매 1초에 한번씩 슬립이 호출되도록 코드 추가
	ULONG ulTickCount = GetTickCount();
	if(*pulPrevCount == 0)
		*pulPrevCount = ulTickCount;

	DWORD dwTime = ulTickCount - *pulPrevCount;
	dwTime /= 1000; // 초단위 값으로 변경
	if(dwTime >= ulInterval) // 1초가 경과한 경우 
	{
		DBGLOG(L"CPUCountSleep after dwTime = %d", dwTime);
		*pulPrevCount = ulTickCount;
		if(ulSleep > 0)
			Sleep(ulSleep); // default = 2ms
		//Sleep(2);
	}
}

BOOL ChkCPUCount(ULONG *pulPrevCount, ULONG ulInterval)
{
	// 매 1초에 한번씩 슬립이 호출되도록 코드 추가
	ULONG ulTickCount = GetTickCount();
	if(*pulPrevCount == 0)
		*pulPrevCount = ulTickCount;

	DWORD dwTime = ulTickCount - *pulPrevCount;
	dwTime /= 1000; // 초단위 값으로 변경
	if(dwTime > ulInterval) // 1초가 경과한 경우
	{
		DBGLOG(L"CPUCountSleep after dwTime = %d", dwTime);
		*pulPrevCount = ulTickCount;
		return TRUE;
	}
	return FALSE;
}

#define VISTA_MAJOR_VERSION  6
#define VISTA_MINOR_VERSION  0
#define OS_MINOR_WIN7_2K8	1
#define MINOR_VERSION_1		1
#define MINOR_VERSION_2		2

/**
@fn			IsWinVista()
@brief		비스타인지 확인하는 함수
@return		BOOL
@sa			
@callgraph	
*/
BOOL IsWinVista()
{
	BOOL bResult = FALSE;
	OSVERSIONINFO vi = { sizeof(vi) };

	::GetVersionEx(&vi);
	if( vi.dwPlatformId == VER_PLATFORM_WIN32_NT &&
	 vi.dwMajorVersion >= VISTA_MAJOR_VERSION)
	{
		return TRUE;
	}

	return FALSE;
}




/**
@fn			GetProcessPathNameFromPid(
				DWORD dwID,
				TCHAR *pszName,
				int nLength)
@brief		
@param		dwID
@param		*pszName
@param		nLength
@return		BOOL
@sa			
@callgraph	
*/
BOOL GetProcessPathNameFromPid(DWORD dwID, TCHAR *pszName, int nLength)
{
	BOOL bRet = FALSE;
	HANDLE hProcess = NULL;

	__try
	{
		hProcess = OpenProcess(PROCESS_QUERY_INFORMATION|PROCESS_VM_READ,FALSE, dwID);
		if( !hProcess )
		{
//			TCHAR strError[MAX_PATH]={0};
//			GetLastErrorW(strError, MAX_PATH);
		//	DBGOUT(_T("OpenProcess(%d) Error = %d"), dwID, GetLastError());
			__leave;
		}

		if(		GetPlatform() == PROCESSOR_ARCHITECTURE_AMD64
			&&	IsWinVista() == TRUE )
		{
			PFN_QueryFullProcessImageNameW fnQueryFullProcessImageNameW=NULL;
			fnQueryFullProcessImageNameW= (PFN_QueryFullProcessImageNameW)GetProcAddress(GetModuleHandle(L"kernel32.dll"),"QueryFullProcessImageNameW");
			if (NULL != fnQueryFullProcessImageNameW)
			{
				if(fnQueryFullProcessImageNameW(hProcess, 0, pszName, (PDWORD)&nLength) > FALSE)
				{
				//	DBGOUT(_T("QueryFullProcessImageName= %s"), pszName);
					bRet = TRUE;
				}
			}
		}
		else
		{

			
			// Kevin(2011-10-24)
			// 본함수는 OS버전이나 빌드환경에 따라 다른 DLL에 존재하므로 아래와 같이 호출한다.
			HMODULE hMod;
			DWORD cbNeeded;
			PFN_EnumProcessModules fnEnumProcessModules = NULL;
			fnEnumProcessModules =  (PFN_EnumProcessModules)GetProcAddress(GetModuleHandle(L"PSAPI.dll"), "EnumProcessModules");
			if(fnEnumProcessModules == NULL)
			{
				fnEnumProcessModules =  (PFN_EnumProcessModules)GetProcAddress(GetModuleHandle(L"kernel32.dll"), "EnumProcessModules");
			}

			if(fnEnumProcessModules != NULL)
			{
				if(fnEnumProcessModules( hProcess, &hMod, sizeof(hMod), &cbNeeded))
				{
					GetModuleBaseName( hProcess, hMod, pszName, nLength);
					bRet = TRUE;
				}
			}
		}
	}
	__finally
	{
		if(hProcess != NULL)
		{
			CloseHandle(hProcess);
			hProcess = NULL;
		}
	}
	return bRet;
}

int OfsGetFileTime(WCHAR *pFileName, LPFILETIME lpCreationTime, LPFILETIME lpLastAccessTime, LPFILETIME lpLastWriteTime)
{
	if(pFileName == NULL)
		return -1;
	if(lstrlen(pFileName) == 0)
		return -1;

	HANDLE hFile = CreateFile( pFileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, 0, NULL);
	if( hFile == INVALID_HANDLE_VALUE) 
	{
		//DBGLOG(L"OfsGetFileTime lasterror = %d", GetLastError());
		return -1;
	}

	FILETIME CreationTime, LastAccessTime, LastWriteTime;
	ZeroMemory(&CreationTime, sizeof(CreationTime));
	ZeroMemory(&LastAccessTime, sizeof(LastAccessTime));
	ZeroMemory(&LastWriteTime, sizeof(LastWriteTime));
	
	GetFileTime(hFile, &CreationTime, &LastAccessTime, &LastWriteTime);

	if(lpCreationTime!= NULL)
		CopyMemory(lpCreationTime, &CreationTime, sizeof(CreationTime));
	if(lpLastAccessTime!= NULL)
		CopyMemory(lpLastAccessTime, &LastAccessTime, sizeof(LastAccessTime));
	if(lpLastWriteTime!= NULL)
		CopyMemory(lpLastWriteTime, &LastWriteTime, sizeof(LastWriteTime));

	CloseHandle( hFile);
	return 1;
}


/**
@fn			IsFileExist(
				WCHAR *pFileName)GeneralUtil.cpp
@brief		
@param		*pFileName
@return		BOOL
@sa			
@callgraph	@date			2013년 6월 6일
*/
BOOL IsFileExist(WCHAR *pFileName)
{
	BOOL bRet  = FALSE;
	if(pFileName == NULL)
		return bRet;
	if(lstrlen(pFileName) == 0)
		return bRet;

	try {
		WCHAR strFile[1024]={0};
		StringCchPrintf(strFile, 1024, L"\"%s\"", pFileName);
		DBGLOG(L"IsFileExist()");
		DBGLOG(strFile);
		int nstate = _waccess(strFile, 0);
		if(nstate != -1)
		{
			bRet = TRUE;
			//DBGLOG
			DBGLOG(L"IsFileExist exist");
		}
		else
		{
			WIN32_FIND_DATA				sFindData;
			HANDLE hFind = FindFirstFile(pFileName, &sFindData);
			if(hFind != INVALID_HANDLE_VALUE)
			{
				bRet = TRUE;
				DBGLOG(L"IsFileExist from findfiirstfile exist");
				//DBGLOG(L"IsFileExist from findfiirstfile exist");

				FindClose(hFind);
			}
			else
			{
				// Kevin(2013-10-30)
				//DBGLOG(L"IsFileExist not exist = %d", nstate);
				WCHAR str[MAX_PATH] ={0};
				StringCchPrintf(str, MAX_PATH, L"IsFileExist  findfiirstfile not exist LastError = %d", GetLastError());
				DBGLOG(str);
			}
		}
	} catch(...)
	{
		DBGLOG(L"IsFileExist() exception occure = ");
		DBGLOG(pFileName);
	}

	return bRet;
}

// ansi의 경우 ""로 묶어주어야함.
BOOL IsFileExistA(CHAR *pFileName)
{
	BOOL bRet  = FALSE;
	if(pFileName == NULL)
		return bRet;
	if(strlen(pFileName) == 0)
		return bRet;

	CHAR strFile[1024]={0};
	//StringCchPrintfA(strFile, 1024, "\"%s\"", pFileName);
	StringCchPrintfA(strFile, 1024, "\"%s\"", pFileName);
	OutputDebugStringA("IsFileExistA() = ");
	OutputDebugStringA(strFile);
	int nstate = _access(strFile, 0);
	if(nstate != -1)
	{
		bRet = TRUE;
		OutputDebugStringA("IsFileExistA exist");
	}
	else
	{
		WIN32_FIND_DATAA				sFindData;
		HANDLE hFind = FindFirstFileA(pFileName, &sFindData);
		if(hFind != INVALID_HANDLE_VALUE)
		{
			bRet = TRUE;
			OutputDebugStringA("IsFileExistA from findfiirstfile exist");
			FindClose(hFind);
		}
		else
		{
			WCHAR str[MAX_PATH] ={0};
			StringCchPrintf(str, MAX_PATH, L"IsFileExistA  findfiirstfile not exist LastError = %d", GetLastError());
			DBGLOG(str);
		}	
	}

	return bRet;
}

/**GeneralUtil.cpp
@fn			GetFileExtension(
				WCHAR *pFile,
				WCHAR *pExt,
				ULONG ulLen)
@brief		
@param		*pFile
@param		*pExt
@param		ulLen
@return		ULONG
@sa			
@callgraph	@date			2014년 1월 20일
*/
ULONG GetFileExtension(WCHAR *pFile, WCHAR *pExt, ULONG ulLen)
{
	WCHAR *pExtesion = PathFindExtension(pFile);
	if(pExtesion == NULL)
	{
		return FALSE;
	}
	StringCchCopy(pExt, ulLen,  pExtesion);

	return (ULONG)((ULONG_PTR)pExtesion - (ULONG_PTR)pFile);
}

/**GeneralUtil.cpp
@fn			GetDecPathNameFromEncPathName(
				WCHAR *pstrEnc,
				ULONG ulLen,
				WCHAR *pstrDec,
				ULONG ulDecLen)
@brief		
@param		*pstrEnc
@param		ulLen
@param		*pstrDec
@param		ulDecLen
@return		BOOL
@sa			
@callgraph	@date			2014년 1월 20일
*/
BOOL GetDecPathNameFromEncPathName(WCHAR *pstrEnc, ULONG ulLen, WCHAR *pstrDec, ULONG ulDecLen)
{
	if(pstrEnc == NULL)
		return FALSE;
	if(lstrlen(pstrEnc) ==0)
		return FALSE;

	//DBGLOG(L"GetDecPathNameFromEncPathName()");

	WCHAR strTmp[4096] ={0};
	StringCchCopy(strTmp, 4096, pstrEnc);
	WCHAR *pExtension = PathFindExtension(strTmp);
	if(pExtension != NULL)
	{
		*pExtension = L'\0';
		pExtension++;
		pExtension++;
		StringCchPrintf((STRSAFE_LPWSTR)pstrDec, ulDecLen, L"%s.%s", strTmp, pExtension);
		//DBGLOG(L"GetDecPathNameFromEncPathName() %s", pstrDec);
	}
	return TRUE;
}

/**GeneralUtil.cpp
@fn			GetEncPathNameFromDecPathName(
				WCHAR *pstrDec,
				ULONG ulLen,
				WCHAR *pstrEnc,
				ULONG ulDecLen)
@brief		
@param		*pstrDec
@param		ulLen
@param		*pstrEnc
@param		ulDecLen
@return		BOOL
@sa			
@callgraph	@date			2014년 1월 20일
*/
BOOL GetEncPathNameFromDecPathName(WCHAR *pstrDec, ULONG ulLen, WCHAR *pstrEnc, ULONG ulDecLen)
{
	if(pstrDec == NULL)
		return FALSE;
	if(lstrlen(pstrDec) ==0)
		return FALSE;

	//DBGLOG(L"GetEncPathNameFromDecPathName()");

	WCHAR strTmp[4096] ={0};
	StringCchCopy(strTmp, 4096, pstrDec);
	WCHAR *pExtension = PathFindExtension(strTmp);
	if(pExtension != NULL)
	{
		*pExtension = L'\0';
		pExtension++;
		StringCchPrintf((STRSAFE_LPWSTR)pstrEnc, ulDecLen, L"%s.e%s", strTmp, pExtension);
		//DBGLOG(L"GetEncPathNameFromDecPathName() %s", pstrEnc);
	}
	return TRUE;
}


/**
@fn			GetEncNewPathName(
				WCHAR *pstrDecName,
				WCHAR *pstrNewEncName,
				ULONG ulLen)
@brief		일반파일의 암호화 파일이 존재하는지 확인하는 함수
				a.txt=> a.etxt, a(1).etxt, a(2).etxt, a(3).etxt....
@param		*pstrDecName
@param		*pstrNewEncName
@param		ulLen
@return		BOOL
@sa			
@callgraph	@date			2013년 7월 4일
*/
BOOL GetNewEncPathName(WCHAR *pstrDecName, WCHAR *pstrNewEncName, ULONG ulLen)
{
	// 해당 파일일의 암호화 파일이 존재한다면 암호화 파일명을 새로운 이름으로 작성한다.
	//DBGLOG(L"GetEncNewPathName START");
	WCHAR strPath[4096] ={0};
	StringCchCopy(strPath, 4096, pstrDecName);
	WCHAR strEncName[4096] = {0};
	WCHAR *pExt = PathFindExtension(strPath);
	if(pExt == NULL)
		return FALSE;
	
	*pExt = L'\0';
	pExt++;

	int i = -1;
	BOOL bExist = TRUE;
	while(bExist == TRUE)
	{
		i++;
		if( i == 0)
			StringCchPrintf(strEncName, 4096, L"%s.e%s", strPath, pExt);
		else
			StringCchPrintf(strEncName, 4096, L"%s-(ofs%d).e%s",strPath, i, pExt);

		// 해당 암호화 파일이 존재하는가?
		bExist = IsFileExist(strEncName);
	}

	if(i==0)
		StringCchPrintf(pstrNewEncName, ulLen, L"%s.%s", strPath, pExt);
	else
		StringCchPrintf(pstrNewEncName, 4096, L"%s-(ofs%d).%s",strPath, i, pExt);

	//DBGLOG(L"GetEncNewPathName = %s", pstrNewEncName);
	return TRUE;
}

//pszWProcessName : 찾을 프로세스명
//dwSessionID : 찾을 세션 ID
// 찾을 프로세스의 세션ID가 인자로 들어온 세션 ID랑 매칭될때 해당 프로세스 ID를 리턴, 
// 세션 ID가 -1일경우는 제일 처음으로 찾는 프로세스의 PID를 리턴한다.
DWORD GetProcessID(WCHAR* pszWProcessName, int dwSessionID)
{
	DWORD dwRet = 0;
	BOOL bGet = FALSE;  
    CString strLog;
    HANDLE hSnapshot= NULL;  
    PROCESSENTRY32 ppe;     //구성된 자료구조를 저장하기 위한 엔트리.  
  
    hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
    ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  
    bGet = Process32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  
  
    while (bGet)  
    {  
		if(wcscmp(ppe.szExeFile, pszWProcessName) == 0)
		{
			if(dwSessionID != -1)
			{
				DWORD dwGetSessionID = 0;
				if( ProcessIdToSessionId(ppe.th32ProcessID, &dwGetSessionID) )
				{
					if(dwSessionID == dwGetSessionID)
					{
						dwRet = ppe.th32ProcessID;
						break;
					}
						
				}
			}
			else{

				dwRet = ppe.th32ProcessID;
				break;			
			}
		}
		

        bGet = Process32Next(hSnapshot, &ppe);  
    }  
  
	if(hSnapshot)
		CloseHandle(hSnapshot);  

	return dwRet;
  
}


/**
@fn			CreateSharedMem(
				BYTE *pstrSharedName,
				int nSize,
				HANDLE *phShared,
				VOID **ppSharedInfo)
@brief		
@param		*pstrSharedName
@param		nSize
@param		*phShared
@param		**ppSharedInfo
@return		int
@sa			
@callgraph	
*/
int CreateMemFileMap(IN TCHAR *pstrSharedName, IN int nSize, OUT HANDLE *phShared, OUT VOID **ppSharedInfo)
{
	ULONG nRealSize = 0;
	DBGOUT(_T("CreateMemMapFile"));
	try
	{
		if(*phShared != NULL && *ppSharedInfo!=NULL)
		{
			// 이미 생성된 경우
			return 2;
		}

#define SDDL_REVISION_1     1
typedef BOOL (WINAPI* PFN_ConvertStringSecurityDescriptorToSecurityDescriptorW)( 
			LPCWSTR StringSecurityDescriptor,
			DWORD StringSDRevision,
			PSECURITY_DESCRIPTOR* SecurityDescriptor,
			PULONG SecurityDescriptorSize );
		
		TCHAR       szMemName[MAX_PATH] ={0} ;
		int nOS = GetWinVer();
		switch(nOS)
		{
		default:
		case 	WINXP :
				DBGOUT(_T("CreateMemMapFile Winxp"));
				StringCchPrintf(szMemName, MAX_PATH, _T("%s"), pstrSharedName) ;
				DBGOUT(_T("CreateMemMapFile Name [%s]"),szMemName);
			break;
		case WIN2K3 :
				DBGOUT(_T("CreateMemMapFile Win2003"));
				StringCchPrintf(szMemName, MAX_PATH,	_T("Session\\%s"), pstrSharedName) ;
				DBGOUT(_T("CreateMemMapFile Name [%s]"),szMemName);
			break;
		case WINVISTAMORE :
				DBGOUT(_T("CreateMemMapFile Win Vista..."));
					StringCchPrintf(szMemName, MAX_PATH,	_T("%s"), pstrSharedName) ;
				DBGOUT(_T("CreateMemMapFile Name [%s]"),szMemName);
			break;
		}
	
		// Add session id for multi login
		if(nOS == WINVISTA)
		{
			DBGOUT(_T("CreateMemMapFile Win Vista ..."));	

			PFN_ConvertStringSecurityDescriptorToSecurityDescriptorW pfn = (PFN_ConvertStringSecurityDescriptorToSecurityDescriptorW)
																									GetProcAddress( LoadLibrary(_T("Advapi32.dll")), "ConvertStringSecurityDescriptorToSecurityDescriptorW" );
			if (NULL == pfn) {
				DWORD dwErr = GetLastError();
			}

			DBGOUT(_T("PFN_ConvertStringSecurityDescriptorToSecurityDescriptor [%x]"),pfn);
			//vista UAC On에서 정상동작하지 않는다 따라서 다음 코드를 추가해보았다-08-12-12
			//add code star
			SECURITY_ATTRIBUTES secAttr;
			char secDesc[SECURITY_DESCRIPTOR_MIN_LENGTH]={0};
			secAttr.nLength = sizeof(secAttr);
			secAttr.bInheritHandle = FALSE;
			secAttr.lpSecurityDescriptor = &secDesc;
			InitializeSecurityDescriptor(secAttr.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
			//add end
			if (pfn)
			{
				//add Start
				PSECURITY_DESCRIPTOR pSD;
				//BOOL bReturn = pfn(_T("S:(ML;;NW;;;LW)"), SDDL_REVISION_1, &pSD, NULL);
				BOOL bReturn = pfn(_T("S:(ML;;NW;;;LW)D:(A;;0x12019f;;;WD)"), SDDL_REVISION_1, &pSD, NULL);
				//BOOL bReturn = pfn(_T("S:(ML;;NW;;;LW)D:(A;;0x12019b;;;WD)"), SDDL_REVISION_1, &pSD, NULL);
				if (!bReturn)
				{
					DBGOUT(_T("CreateMemMapFile PFN_ConvertStringSecurityDescriptorToSecurityDescriptorA Fail[%d]"),::GetLastError());
					return FALSE;
				}
				PACL pSacl = NULL;                  // not allocated
				BOOL fSaclPresent = FALSE;
				BOOL fSaclDefaulted = FALSE;
				bReturn = GetSecurityDescriptorSacl(pSD, &fSaclPresent, &pSacl, &fSaclDefaulted);
				if (!bReturn)
				{
					DBGOUT(_T("CreateMemMapFile  GetSecurityDescriptorSacl Fail[%d]"),::GetLastError());
					return FALSE;
				}
				bReturn = SetSecurityDescriptorSacl(secAttr.lpSecurityDescriptor, TRUE, pSacl, FALSE);
				if (!bReturn)
				{
					DBGOUT(_T("CreateMemMapFile SetSecurityDescriptorSacl Fail[%d]"),::GetLastError());
					return FALSE;		
				}
				// Kevin(2013-1-4)
				//if(pSD != NULL)
				//	LocalFree(pSD);
				
				// Kevin(2013-1-7)
				// Windows 8에서 Integrity가 다른 Process간 Memory 공유를 위해 추가됨
				// 이전 버전에서는 잘될까?
				secAttr.lpSecurityDescriptor = pSD;

				*phShared = CreateFileMapping( INVALID_HANDLE_VALUE,
																	&secAttr,
																	PAGE_READWRITE,
																	0,
																	nSize,
																	szMemName
																	);
				if( *phShared == NULL || *phShared == INVALID_HANDLE_VALUE )
				{
					DBGOUT(_T("CreateMemMapFile : CreateFileMapping Error [%d]"), GetLastError());
					SetLastError(0);
					*phShared = CreateFileMapping( INVALID_HANDLE_VALUE,
																	&secAttr,
																	PAGE_READONLY,
																	0,
																	nSize,
																	szMemName);
				}
			}				
			else
			{
				*phShared = CreateFileMapping( INVALID_HANDLE_VALUE,
																		NULL,
																		PAGE_READWRITE,
																		0,
																		nSize,
																		szMemName
																		) ; 
			}
		}
		else
		{
				SECURITY_ATTRIBUTES security; 
				ZeroMemory(&security, sizeof(security)); security.nLength = sizeof(security); 
				ConvertStringSecurityDescriptorToSecurityDescriptor(L"D:P(A;OICI;GA;;;SY)(A;OICI;GA;;;BA)(A;OICI;GWGR;;;IU)", SDDL_REVISION_1, &security.lpSecurityDescriptor, NULL); 
				DBGOUT(_T("CreateMemMapFile XP 32 Or XP 64 Or Win 2003 Mode Shared Mem Create"));
				*phShared = CreateFileMapping( INVALID_HANDLE_VALUE,
																	&security,
																	PAGE_READWRITE,
																	0,
																	nSize,
																	szMemName
																	) ; 
				//LocalFree(securityDescriptor.lpSecurityDescriptor); 
		}

		if( *phShared == NULL || *phShared == INVALID_HANDLE_VALUE )
		{
			DBGOUT(_T("CreateMemMapFile : CreateFileMapping Error [%d]"), GetLastError());
			SetLastError(0);
			*phShared = OpenFileMapping( PAGE_READWRITE, FALSE, szMemName);
			if( *phShared == NULL || *phShared == INVALID_HANDLE_VALUE )
			{
				DBGOUT(_T("CreateMemMapFile : OpenFileMapping Error [%d]"), GetLastError());
				SetLastError(0);
				*phShared = OpenFileMapping( PAGE_READONLY, FALSE, szMemName);
				if( *phShared == NULL || *phShared == INVALID_HANDLE_VALUE )
				{
					DBGOUT(_T("CreateMemMapFile : OpenFileMapping PAGE_READONLY Error [%d]"), GetLastError());
					return FALSE;
				}
			}
			DBGOUT(_T("CreateMemMapFile : OpenFileMapping() success GetLastError [%d]"), GetLastError());
		}

		if( GetLastError() == ERROR_ALREADY_EXISTS )
		{
			DBGOUT(_T("CreateMemMapFile : OpenMap because ERROR_ALREADY_EXISTS"));
		}
		else
		{
			DBGOUT(_T("CreateMemMapFile Initializing"));
		}

		DBGOUT(_T("CreateMemMapFile : CreateFileMapping - hShared = 0x%x"), phShared );
		DWORD dwAccess = FILE_MAP_WRITE|FILE_MAP_READ;
		*ppSharedInfo = (BYTE *)MapViewOfFile(*phShared, dwAccess,0,0,0);
		if( *ppSharedInfo == NULL )
		{
			DBGOUT(_T("CreateMemMapFile MapViewOfFile(READ|WRITE) is NULL %d"), GetLastError());
			// Kevin(2013-1-2)
			// this code for Windows 8 shared memory open problem.
			// when i call MapViewOfFile with FILE_MAP_WRITE|FILE_MAP_READ, getlasterror = 5 return
			// this mean access denied when i try memory access.
			// so change access mode is FILE_MAP_READ, it's work fine.

			dwAccess = FILE_MAP_READ;
			*ppSharedInfo = (BYTE *)MapViewOfFile(*phShared, dwAccess,0,0,0);
			if(*ppSharedInfo == NULL)
			{
				DBGOUT(_T("CreateMemMapFile MapViewOfFile(READ) is NULL %d"), GetLastError());
				return FALSE;
			}
		}
		nRealSize = GetVirtualSize((LPCVOID*)*ppSharedInfo);
		DBGOUT(_T("CreateMemMapFile size = %d => %d"),nSize, nRealSize);
//		ZeroMemory(*ppSharedInfo, nRealSize);
	}
	catch(...)
	{
		DBGOUT(_T("CreateMemMapFile ERROR !"));
	}

	DBGOUT(_T("CreateMemMapFile End=%d!"), nRealSize);

	return nRealSize;
}

int OpenMemFileMap(IN TCHAR *pstrSharedName, IN int nSize, OUT HANDLE *phShared, OUT VOID **ppSharedInfo)
{
	ULONG nRealSize = 0;
	DBGOUT(_T("OpenMemFileMap start"));
	try
	{
		if(*phShared != NULL && *ppSharedInfo!=NULL)
		{
			// 이미 생성된 경우
			return 2;
		}

#define SDDL_REVISION_1     1
typedef BOOL (WINAPI* PFN_ConvertStringSecurityDescriptorToSecurityDescriptorW)( 
			LPCWSTR StringSecurityDescriptor,
			DWORD StringSDRevision,
			PSECURITY_DESCRIPTOR* SecurityDescriptor,
			PULONG SecurityDescriptorSize );
		
		TCHAR       szMemName[MAX_PATH] ={0} ;
		int nOS = GetWinVer();
		switch(nOS)
		{
		default:
		case 	WINXP :
				DBGOUT(_T("OpenMemFileMap Winxp"));
				StringCchPrintf(szMemName, MAX_PATH, _T("%s"), pstrSharedName) ;
				DBGOUT(_T("OpenMemFileMap Name [%s]"),szMemName);
			break;
		case WIN2K3 :
				DBGOUT(_T("OpenMemFileMap Win2003"));
				StringCchPrintf(szMemName, MAX_PATH,	_T("Session\\%s"), pstrSharedName) ;
				DBGOUT(_T("OpenMemFileMap Name [%s]"),szMemName);
			break;
		case WINVISTAMORE :
				DBGOUT(_T("OpenMemFileMap Win Vista..."));
					StringCchPrintf(szMemName, MAX_PATH,	_T("%s"), pstrSharedName) ;
				DBGOUT(_T("OpenMemFileMap Name [%s]"),szMemName);
			break;
		}

		*phShared = OpenFileMapping( PAGE_READWRITE, FALSE, szMemName);
		if( *phShared == NULL || *phShared == INVALID_HANDLE_VALUE )
		{
			DBGOUT(_T("OpenMemFileMap : OpenFileMapping Error [%d]"), GetLastError());
			SetLastError(0);
			*phShared = OpenFileMapping( PAGE_READONLY, FALSE, szMemName);
			if( *phShared == NULL || *phShared == INVALID_HANDLE_VALUE )
			{
				DBGOUT(_T("OpenMemFileMap : OpenFileMapping PAGE_READONLY Error [%d]"), GetLastError());
				return FALSE;
			}
		}

		DBGOUT(_T("OpenMemFileMap : OpenFileMapping - hShared = 0x%x"), phShared );
		DWORD dwAccess = FILE_MAP_WRITE|FILE_MAP_READ;
		*ppSharedInfo = (BYTE *)MapViewOfFile(*phShared, dwAccess,0,0,0);
		if( *ppSharedInfo == NULL )
		{
			DBGOUT(_T("OpenMemFileMap MapViewOfFile(READ|WRITE) is NULL %d"), GetLastError());
			dwAccess = FILE_MAP_READ;
			*ppSharedInfo = (BYTE *)MapViewOfFile(*phShared, dwAccess,0,0,0);
			if(*ppSharedInfo == NULL)
			{
				DBGOUT(_T("OpenMemFileMap MapViewOfFile(READ) is NULL %d"), GetLastError());
				return FALSE;
			}
		}
		nRealSize = GetVirtualSize((LPCVOID*)*ppSharedInfo);
		DBGOUT(_T("OpenMemFileMap size = %d => %d"),nSize, nRealSize);
	}
	catch(...)
	{
		DBGOUT(_T("OpenMemFileMap ERROR !!!"));
	}

	DBGOUT(_T("OpenMemFileMap End=%d !!!"), nRealSize);
	return nRealSize;
}


BOOL CloseMemFileMap(IN HANDLE hSharedInfo, VOID *pSharedInfo)
{
	BOOL bRet = FALSE;
	try
	{
		if(pSharedInfo != NULL)
		{
			DBGOUT(_T("UnmapViewOfFile"));
			UnmapViewOfFile(pSharedInfo);
			pSharedInfo = NULL;
			bRet = TRUE;
		}

		if(hSharedInfo != NULL)
		{
			DBGOUT(_T("Memory Clear"));
			CloseHandle(hSharedInfo);
			hSharedInfo = NULL;
			bRet = TRUE;
		}
	}
	catch(...)
	{
		DBGOUT(_T("Exception of CloseMemFileMap!") );
	}

	return bRet;
}

BOOL GetSecurityDesc(PSECURITY_DESCRIPTOR psd)
{
	SECURITY_DESCRIPTOR sd;
	 InitializeSecurityDescriptor(&sd, SECURITY_DESCRIPTOR_REVISION);
	 SetSecurityDescriptorDacl(&sd, TRUE, NULL, FALSE);

	 SECURITY_ATTRIBUTES sa = {0};
	 sa.nLength = sizeof(sa);
	 sa.lpSecurityDescriptor = &sd;
	 sa.bInheritHandle = FALSE;
	 CopyMemory((void *)psd, (void *)&sd, sizeof(sd));
	 return TRUE;
}

//BOOL GetSecurityDesc(PSECURITY_DESCRIPTOR psd)
//	SECURITY_DESCRIPTOR m_sd;
//	SECURITY_ATTRIBUTES m_sa;
//public:
//	CSecurityToken(BOOL bInheritHandles = FALSE)
//	{
//		InitializeSecurityDescriptor(&m_sd, SECURITY_DESCRIPTOR_REVISION);
//		SetSecurityDescriptorDacl(&m_sd, TRUE, NULL, FALSE);
//		m_sa.nLength = sizeof(m_sa);
//		m_sa.lpSecurityDescriptor = &m_sd;
//		m_sa.bInheritHandle = bInheritHandles;
//	}


/**GeneralUtil.cpp
@fn			IsEngineRunning()
@brief		
@return		BOOL
@sa			
@callgraph	@date			2013년 7월 18일
*/
BOOL IsEngineRunning()
{
	BOOL bEngineWork = TRUE;
	HANDLE hMutex = NULL;
	hMutex = OpenMutex(NULL, NULL, _T("Global\\CMSENGINE"));
	 DWORD dwErr = GetLastError();
	 if(dwErr == ERROR_FILE_NOT_FOUND)
	 {
		 DBGLOG(L"ENGINE MUTAX NOT EXIST");
		 bEngineWork = FALSE;
	 }
 	if(hMutex)
		ReleaseMutex(hMutex);

	return bEngineWork;
}

/**GeneralUtil.cpp
@fn			GetProcessPid(CString _strFileName)
@brief		인자로 들어온 프로세스명의 pid를 리턴한다.
@author		hhh
@param		_strFileName : 찾을 프로세스명
@return		DWORD
@sa			
@callgraph	@date			2013년 7월 24일
*/
/*DWORD GetProcessPid(TCHAR* _strFileName)
{

	HANDLE hProcess = NULL;  
    PROCESSENTRY32 pe32 = {0};  
  
	hProcess = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);  
    pe32.dwSize = sizeof(PROCESSENTRY32);  
      
    if (Process32First(hProcess, &pe32))  
    {  
		do  
		{                 
			if (_tcscmp(_strFileName, pe32.szExeFile) == 0)  
            {                 
				CloseHandle(hProcess);  
				return pe32.th32ProcessID;                
            }  
  
	    } while(Process32Next(hProcess, &pe32));  
    }  
      
	CloseHandle(hProcess);      
    return 0;  
}*/

/**GeneralUtil.cpp
@fn			ResumeProcess(DWORD dwPid)
@brief		Process의 모든 쓰레드를 resumethread 처리 한다.
@author		hhh
@param		dwPid : resumethread 할 프로세스 ID
@return		void
@sa			
@callgraph	@date			2013년 7월 24일
*/
void ResumeProcess(DWORD dwPid)
{
	HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, dwPid);  
  
	if (hSnapshot != INVALID_HANDLE_VALUE)  
	{
	    THREADENTRY32 te = { sizeof(te) };
		BOOL fOk = Thread32First(hSnapshot, &te);
		for (; fOk; fOk = Thread32Next(hSnapshot, &te))
		{
			if (te.th32OwnerProcessID == dwPid)
			{
				HANDLE hThread = OpenThread(THREAD_ALL_ACCESS, FALSE, te.th32ThreadID);
				if (hThread) {
					ResumeThread(hThread);
					CloseHandle(hThread);		// 2015-08-06 kh.choi 핸들 수가 계속 늘어나는 문제 해결
				}
				/*else
				{
					DWORD dwError = GetLastError();
					WCHAR szLog[100]={0,};
					memset(szLog, 0, sizeof(szLog));
					wsprintf(szLog, L"[GeneralUtil] Thread Handle Error :%d", dwError);
					UM_WRITE_LOG(szLog);
				}*/
			}
		}
		CloseHandle(hSnapshot);		// 2015-08-06 kh.choi 핸들 수가 계속 늘어나는 문제 해결
	}
}


BOOL IsUsbDevice( WCHAR letter )
{
	wchar_t volumeAccessPath[] = L"\\\\.\\X:";
	volumeAccessPath[4] = letter;
	HANDLE deviceHandle = CreateFileW( volumeAccessPath,
														0,                // no access to the drive
														FILE_SHARE_READ | // share mode
														FILE_SHARE_WRITE, 
														NULL,             // default security attributes
														OPEN_EXISTING,    // disposition
														0,                // file attributes
														NULL);            // do not copy file attributes
	// setup query
	STORAGE_PROPERTY_QUERY query;
	memset(&query, 0, sizeof(query));
	query.PropertyId = StorageDeviceProperty;
	query.QueryType = PropertyStandardQuery;

	// issue query
	DWORD bytes;
	STORAGE_DEVICE_DESCRIPTOR devd;
	STORAGE_BUS_TYPE busType = BusTypeUnknown;
	if (DeviceIoControl(deviceHandle,
							IOCTL_STORAGE_QUERY_PROPERTY,
							&query, sizeof(query),
							&devd, sizeof(devd),
							&bytes, NULL))
	{
		busType = devd.BusType;
	}
	else
	{
		OutputDebugStringW(L"Failed to define bus type for");
	}
	CloseHandle(deviceHandle);
	
	return BusTypeUsb == busType;
 } 



BOOL GetPatternName(WCHAR *_strNumber, WCHAR *pRet, ULONG ulLen)
{
	//CString strValue = _strNumber.Mid(0,2);

	int iNumber = _ttoi(_strNumber);

	CString strResult = _T("");
	switch(iNumber)
	{
	case PATTERN_GENERAL_TYPE:
		StringCchCopy(pRet, ulLen, _T("주민번호"));
		break;

	case PATTERN_PASSPORT_TYPE:
		StringCchCopy(pRet, ulLen, _T("여권번호"));
		break;

	case PATTERN_DRIVER_TYPE:
		StringCchCopy(pRet, ulLen, _T("운전면허"));
		break;

	case PATTERN_TELEPHONE_TYPE:
		StringCchCopy(pRet, ulLen, _T("전화번호"));
		break;

	case PATTERN_EMAIL_TYPE:
		StringCchCopy(pRet, ulLen, _T("이메일"));
		break;

	case PATTERN_FOREIGN_TYPE:
		StringCchCopy(pRet, ulLen, _T("외국인 등록번호"));
		break;

	case PATTERN_BANK_TYPE:
		StringCchCopy(pRet, ulLen, _T("계좌번호"));
		break;

	case PATTERN_CARD_TYPE:
		StringCchCopy(pRet, ulLen, _T("신용카드"));
		break;
	}

	return TRUE;
}

/**GeneralUtil.cpp
@fn			OfsGetFileSize(
				WCHAR *strFilePath)
@brief		file size얻는 함수
@param		*strFilePath
@return		__int64
@sa			
@callgraph	@date			2013년 8월 16일
*/
__int64 OfsGetFileSize(WCHAR *strFilePath)
{
	HANDLE hReadFile = CreateFile(strFilePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if( hReadFile == INVALID_HANDLE_VALUE) 
	{
		// TODO 로그
		return FALSE;
	}

	//DWORD dwSize;
	__int64 nSize;
	LARGE_INTEGER li;
	li.LowPart = GetFileSize(hReadFile, (LPDWORD)&li.HighPart);
	nSize = (__int64)li.QuadPart;

	CloseHandle(hReadFile);

	return nSize;
}
/**GeneralUtil.cpp
@fn			
@brief		인자로 들어온 프로세스와 같은 이름 존재시 프로세스를 종료한다.
@author		hhh
@param		arrKillProcessList : 종료시킬 프로세스 리스트들로 하나의 문자열로 되어 구성되어야 한다.( ex:  ;abc.exe;ccc.exe;test.exe;) 
@param		_bSetDebugPrivilege : 디버그 권한을 얻어서 종료할 것인지
@param		nRoopSleep : 프로세스 종료를 위해 루프를 돌때 어느정도의 sleep을 줄것인지
@param		bLowerCompare :  대소문자 구별없이 체크 할 것인지 (TRUE: 구별없음, FALSE: 구별)
@param		parrKilledProcessList : 종료시킨 프로세스의 리스트를 받을  arr
@note		프로세스를 찾을때 ;이 찾는 문자열에 앞뒤에 다 붙어 있어야 한다.
@return		void
@sa			대소문자를 구분하지 않는다.
@callgraph	@date			2013년 8월 16일
*/
BOOL  ForceProcessKill(CString& strKillProcessList, BOOL _bSetDebugPrivilege, int nRoopSleep, BOOL bLowerCompare, CStringArray* parrKilledProcessList)
{
	BOOL  bRet = FALSE;
	DWORD dwKillProcessPid = 0;
	BOOL bGet = FALSE;  
    CString strLog;
	HANDLE hToken = NULL;
    HANDLE hSnapshot= NULL;  
    PROCESSENTRY32 ppe;     //구성된 자료구조를 저장하기 위한 엔트리. 
	CString strList;
	
	if(_bSetDebugPrivilege)
	{
		if( OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) )
			SetPrivilegeToken(hToken, SE_DEBUG_NAME, TRUE);
	}

	strList = strKillProcessList;
	if(bLowerCompare)
		strList.MakeLower();

    hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
    ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  
    bGet = Process32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  
  
	CString strKillProcess = L"";
    while (bGet)  
    {  

		BOOL bIsSame = FALSE;
		
		if(bLowerCompare)	//대소문자 구분없이 비교하기 위해 소문자로 변경
		{
			CString strCompareText;	
			strCompareText.Format(L";%s;", ppe.szExeFile);
			strCompareText.MakeLower();

			//차단할 프로세스 라면
			if( strList.Find(strCompareText) != -1 )			
				bIsSame = TRUE;
				
			
									
		}
		else		//대소문자 구별
		{
			
			CString strCompareText;	
			strCompareText.Format(L";%s;", ppe.szExeFile);
			
			//차단할 프로세스 라면
			if( strList.Find(strCompareText) != -1 )			
				bIsSame = TRUE;
				
							
		}
		
		if(bIsSame)
		{
			dwKillProcessPid = ppe.th32ProcessID;

			HANDLE hKillProcess = NULL;
			hKillProcess = ::OpenProcess(PROCESS_TERMINATE, FALSE, dwKillProcessPid);
			if(hKillProcess)
			{
				if( TerminateProcess(hKillProcess, 0) )
				{
					bRet = TRUE;
					if(parrKilledProcessList != NULL)
					{
						CString strKilledProcess;
						strKilledProcess.Format(L"%s", ppe.szExeFile);						
						parrKilledProcessList->Add(strKilledProcess);
					}
						
				}				
				::CloseHandle(hKillProcess);											
			}
			//break;
		}
		
		Sleep(nRoopSleep);
        bGet = Process32Next(hSnapshot, &ppe);  
    }  
  
	if(hSnapshot)
		CloseHandle(hSnapshot);  


	if(_bSetDebugPrivilege)	
	{
		if(hToken)
			SetPrivilegeToken(hToken, SE_DEBUG_NAME, FALSE);
	}
	
	
	return bRet;
}

BOOL SetPrivilegeToken(
        HANDLE hToken,   // 토큰 핸들
        LPCTSTR Privilege,   // 활성/비활성화할 권한
        BOOL bEnablePrivilege  // 권한 활성화 여부?
        )
    {
        TOKEN_PRIVILEGES tp;
        LUID luid;
        TOKEN_PRIVILEGES tpPrevious;
        DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);
 
        if (!LookupPrivilegeValue( NULL, Privilege, &luid ))
            return FALSE;
 
        // 현재의 권한 설정 얻기
        tp.PrivilegeCount   = 1;
        tp.Privileges[0].Luid   = luid;
        tp.Privileges[0].Attributes = 0;
 
        AdjustTokenPrivileges(
                hToken,
                FALSE,
                &tp,
                sizeof(TOKEN_PRIVILEGES),
                &tpPrevious,
                &cbPrevious
                );
 
        if (GetLastError() != ERROR_SUCCESS)
            return FALSE;
 
        // 이전의 권한 설정에 따라 권한 설정하기
        tpPrevious.PrivilegeCount       = 1;
        tpPrevious.Privileges[0].Luid   = luid;
 
        if (bEnablePrivilege) {
            tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
        }
        else {
            tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
                tpPrevious.Privileges[0].Attributes);
        }
 
        AdjustTokenPrivileges(
                hToken,
                FALSE,
                &tpPrevious,
                cbPrevious,
                NULL,
                NULL
                );
 
        if (GetLastError() != ERROR_SUCCESS)
            return FALSE;
 
        return TRUE;
}

BOOL IsNetworkDrive(WCHAR *pstrDrv)
{
	BOOL bRet = FALSE;
	WCHAR buffer[1024]= {0};
	int errorCode = 0;
	DWORD bufferLength = 1024;
	errorCode = WNetGetConnection(pstrDrv/*"Z:"*/, buffer, &bufferLength);
	if(errorCode != 0)
	{
		errorCode = GetLastError();
		switch (errorCode)
		{
			case ERROR_BAD_DEVICE:
				DBGLOG(L"\nERROR_BAD_DEVICE");
				break;
			case ERROR_NOT_CONNECTED:
			DBGLOG(L"\nERROR_NOT_CONNECTED");
				break;
			case ERROR_MORE_DATA:
				DBGLOG(L"\nERROR_MORE_DATA");
				break;
			case ERROR_CONNECTION_UNAVAIL:
				DBGLOG(L"\nERROR_CONNECTION_UNAVAIL");
				break;
			case ERROR_NO_NETWORK:
				DBGLOG(L"\nERROR_NO_NETWORK");
				break;
			case ERROR_EXTENDED_ERROR:
				DBGLOG(L"\nERROR_EXTENDED_ERROR");
				break;
			case ERROR_NO_NET_OR_BAD_PATH:
				DBGLOG(L"\nERROR_NO_NET_OR_BAD_PATH");
				break;
			default:
				DBGLOG(L"\nUnknown Error");
				break;
		}
	}
	else
	{
		bRet = TRUE;
	}

	DBGLOG(L"\nRemote path %s = %s\n", pstrDrv, buffer);

	return bRet;
}


/**GeneralUtil.cpp
@fn			GetNetDrives()
@brief		네트워크 드라이브중 연결된 놈이 있는지 확인
@return		DWORD
@sa			
@callgraph	@date			2013년 11월 12일
*/
DWORD GetNetDrives()
{
	DWORD ulDrv = 0;
	WCHAR strName[MAX_PATH] ={0};
	WCHAR chDrv = L'A';
	for(int i=0; i < 26; i++)
	{
		StringCchPrintf(strName, MAX_PATH, _T("%c:"), chDrv+i );
		if(IsNetworkDrive(strName) == TRUE)
		{
			ulDrv |= (1 << i);

		}
	}
	DBGLOG(L"GetNetDrives = 0X%X", ulDrv);
	WCHAR strLog[MAX_PATH] ={0};
	StringCchPrintf(strLog, MAX_PATH,L"GetNetDrives= 0x%X", ulDrv);
	UM_WRITE_LOG(strLog);
	return ulDrv;
}

BOOL IsProtectedFile(WCHAR *pstrFile)
{
	HANDLE hRead = NULL;
	DBGLOG(L"CanFileOpen(%s)", pstrFile);
	hRead = CreateFile(pstrFile, 
								GENERIC_READ,
								FILE_SHARE_READ,
								0,
								OPEN_EXISTING,
								NULL,
								0);

	if( hRead == INVALID_HANDLE_VALUE || hRead == NULL )
	{
		DWORD dwErr = GetLastError();
		DBGLOG(_T("ITCMS INVALID_HANDLE_VALUE = %d"), dwErr);
		if(ERROR_ACCESS_DENIED == dwErr)
		{
			return TRUE;
		}
	}
	CloseHandle(hRead);

	return FALSE;
}

/**GeneralUtil.cpp
@fn			GetVolumeDiskNumber()
@brief		디스크 넘버를 구한다.
@param		strLetter    :  c:, d: , e: 와 같이 드라브명에 콜론을 붙여준다.
@param		_nDiskNumber :  디스크 넘버를 반환
@return		BOOL 성공, 실패
@sa			
@callgraph	@date			2014년 2월 25일
*/
BOOL GetVolumeDiskNumber(TCHAR* szLetter, int& _nDiskNumber)
{

	TCHAR szDisk[MAX_PATH] ={0};
	StringCchPrintf(szDisk, MAX_PATH, _T("\\\\.\\%s"), szLetter);	
	HANDLE hVolume = ::CreateFile(szDisk, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 	NULL, OPEN_EXISTING, 	0,	NULL);
	if (INVALID_HANDLE_VALUE == hVolume)
	{
		//DBGOUT("Failed to open volume %s: Err=0x%x\n",pstrDrive, GetLastError());
		return FALSE;
	}

	int nSize = sizeof(VOLUME_DISK_EXTENTS)+sizeof(DISK_EXTENT)*10;	
	PVOLUME_DISK_EXTENTS pvdExts1 = (PVOLUME_DISK_EXTENTS)malloc(nSize);
	if(pvdExts1==NULL)
	{
		CloseHandle(hVolume);
		return FALSE;
	}

	DWORD BytesWritten = 0;
	BOOL Res = ::DeviceIoControl(
											hVolume,
											IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS,
											NULL,
											0,
											pvdExts1,
											sizeof(VOLUME_DISK_EXTENTS),
											&BytesWritten,
											NULL);
	if (!Res)
	{
		DWORD dwErr = GetLastError();
		
		if(dwErr == ERROR_MORE_DATA)
		{	
			DWORD dwExtentCnt = pvdExts1->NumberOfDiskExtents;		
			free(pvdExts1);
			
			int nSize = sizeof(VOLUME_DISK_EXTENTS)+sizeof(DISK_EXTENT)*dwExtentCnt;			
			pvdExts1 =  (PVOLUME_DISK_EXTENTS)malloc(nSize);
			BOOL Res = ::DeviceIoControl(
													hVolume,
													IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS,
													NULL,
													0,
													pvdExts1,
													sizeof(VOLUME_DISK_EXTENTS),
													&BytesWritten,
													NULL);
			if(!Res)
			{
				free(pvdExts1);
				CloseHandle(hVolume);
				return FALSE;
			}
		}
	}
	
	if(pvdExts1 != NULL)
	{
		_nDiskNumber = pvdExts1->Extents->DiskNumber;
		free(pvdExts1);
	}

	CloseHandle(hVolume);
	return TRUE;
}

/**GeneralUtil.cpp
@fn			GetDeviceNumber()
@brief		디바이스 넘버를 구한다.
@param		strLetter    :  c:, d: , e: 와 같이 드라브명에 콜론을 붙여준다.
@param		_nDeviceNumber :  디바이스 넘버
@return		BOOL 성공, 실패
@sa			
@callgraph	@date			2014년 2월 25일
@note		Driver letter값을 이용해서 디바이스 넘버를 구한다.
*/
BOOL GetDeviceNumber_USE_DrvLetter(TCHAR* szLetter, int& _nDeviceNumber)
{

	TCHAR szDisk[MAX_PATH] ={0};
	StringCchPrintf(szDisk, MAX_PATH, _T("\\\\.\\%s"), szLetter);	
	HANDLE hVolume = ::CreateFile(szDisk, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 	NULL, OPEN_EXISTING, 	0,	NULL);
	if (INVALID_HANDLE_VALUE == hVolume)
	{
		//DBGOUT("Failed to open volume %s: Err=0x%x\n",pstrDrive, GetLastError());
		return FALSE;
	}

	DWORD dwRet ;
	dwRet = GetDeviceNumber(hVolume);
	if(dwRet != -1)
		_nDeviceNumber = dwRet;
	else
	{
		CloseHandle(hVolume);
		return FALSE;
	}
		
	CloseHandle(hVolume);
	return TRUE;
}


/**GeneralUtil.cpp
@fn			GetDeviceNumber()
@brief		디바이스 넘버를 구한다.
@param		deviceHandle : 디바이스 핸들
@return		디바이스 넘버
@sa			
@callgraph	@date			2014년 2월 25일
@note		디바이스 핸들을 이용해서 디바이스 넘버를 구한다.
*/
DWORD GetDeviceNumber( HANDLE deviceHandle )
{
   STORAGE_DEVICE_NUMBER sdn;
   sdn.DeviceNumber = -1;
   DWORD dwBytesReturned = 0;
   if ( !DeviceIoControl( deviceHandle,
                          IOCTL_STORAGE_GET_DEVICE_NUMBER,
                          NULL, 0, &sdn, sizeof( sdn ),
                          &dwBytesReturned, NULL ) )
   {
      // handle error - like a bad handle.
      return -1;
   }
   return sdn.DeviceNumber;
}


// Kevin(2014-5-28)
/**GeneralUtil.cpp
@fn			GetNextNoFileName(
				WCHAR *pSrcName,
				WCHAR *pDestName,
				ULONG ulDstLen)
@brief		암호화한 파일이 존재할 경우 해당파일에 번호를 부여해주는 함수
				번호는 -(1)->-(2) ....-(1000) 까지 부여
@param		*pSrcName
@param		*pDestName
@param		ulDstLen
@return		BOOL
@sa			
@callgraph	@date			2014년 5월 27일
*/
BOOL GetNextNoFileName(WCHAR *pSrcName, WCHAR *pDestName, ULONG ulLen)
{
	BOOL bRet = FALSE;
	if(pSrcName == NULL)
		return FALSE;
	if(lstrlen(pSrcName) == 0)
		return FALSE;


	WCHAR strExt[MAX_PATH] ={0};
	WCHAR strTmpName[4096]  = {0};
	StringCchCopy(strTmpName, 4096, pSrcName);
	WCHAR *pExt = PathFindExtension(strTmpName);
	if(pExt == NULL)
		return FALSE;
	
	StringCchCopy(strExt, MAX_PATH, pExt);
	//pExt--;
	*pExt = L'\0';

	int nCnt = 1;
	WCHAR strTmpEncName[4096] ={0};
	do
	{
		StringCchPrintf(pDestName, ulLen, L"%s-(%d)%s", strTmpName, nCnt, strExt);
		DBGLOG(L"pDestName = %s", pDestName);
		GetEncPathNameFromDecPathName(pDestName, 4096, strTmpEncName, 4096);

		nCnt++;
		if(	IsFileExist(pDestName) == FALSE 
		&& IsFileExist(strTmpEncName) == FALSE )
		{
			bRet = TRUE;
			break;
		}
	} while(nCnt <= 1000);

	DBGLOG(L"GetNextNoFileName() end");
	return bRet;
}

/*인자로 들어온 프로세스를 찾는다.
_strProcessName : 찾을 프로세스명
_dwParentPid : 찾은 프로세스의 부모 프로세스 pid( 0이면 부모는 비교하지 않는다.)
*/
DWORD	GetPid_UseProcessName(WCHAR* pszWProcessName, DWORD _dwParentPid)
{
	DWORD dwRet = 0;
	BOOL bGet = FALSE;  
    CString strLog;
    HANDLE hSnapshot= NULL;  
    PROCESSENTRY32 ppe;     //구성된 자료구조를 저장하기 위한 엔트리.  
  
    hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
    ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  
    bGet = Process32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  
  
    while (bGet)  
    {  
		if(wcscmp(ppe.szExeFile, pszWProcessName) == 0)
		{
			if(_dwParentPid != 0)
			{
				if(_dwParentPid == ppe.th32ParentProcessID)
				{
					dwRet = ppe.th32ProcessID;
					break;
				}
			}
			else{

				dwRet = ppe.th32ProcessID;
				break;			
			}
		}
		

        bGet = Process32Next(hSnapshot, &ppe);  
    }  
  
	if(hSnapshot)
		CloseHandle(hSnapshot);  

	return dwRet;
  
}