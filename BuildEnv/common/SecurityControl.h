#ifndef _SECURITY_CONTROL_DEFINE_H
#define _SECURITY_CONTROL_DEFINE_H



/*********** 암호화 / 차단 Type ************************************************************/
#define  SECURITY_FILE_ENCRYPT					1
#define  SECURITY_FILE_BLOCK						0



/*********** 패턴 Type *********************************************************************/
#define  PATTERN_GENERAL_TYPE							0x00000001  // 주민 번호
#define  PATTERN_PASSPORT_TYPE						0x00000002   // 여권 번호			
#define  PATTERN_DRIVER_TYPE							0x00000003   // 운전 면허
#define  PATTERN_TELEPHONE_TYPE                      0x00000004   // 전화 번호 
#define  PATTERN_EMAIL_TYPE								0x00000005   // 이메일
#define  PATTERN_FOREIGN_TYPE							0x00000006   // 외국인 등록번호
#define  PATTERN_BANK_TYPE								0x00000007   //계좌번호
#define  PATTERN_CARD_TYPE								0x00000008   //신용카드
#define	 KEYWORD_TYPE										0x00000100   //키워드


#define ENCRYPT_EXTENTEION _T(".ENC")
/********** 로그 전달 구조체****************************************************************/
typedef struct _ENC_POLICY_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrFileName;
	CStringArray arrFullPath;
	CStringArray arrEncFullPath;
	CStringArray arrEncHash;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrEncTime;
	CStringArray arrPlyIdx;
	CStringArray arrEndIdx;			//hhh : 2013.01.31 - 서버로 올릴때는 사용하지 않으니 에이젼트에서 사용
	CStringArray arrPatternID;
}ENC_POLICY_LOG;

typedef struct _GENERAL_POLICY_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrFileName;
	CStringArray arrFullPath;
	CStringArray arrHash;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrEncTime;
	CStringArray arrPlyIdx;
	CStringArray arrIdx;
	CStringArray arrPatternID;	

}GENERAL_POLICY_LOG;
// typedef struct _SECRET_LOG
// {
// 	CStringArray arrUserID;
// 	CStringArray arrPCID;
// 	CStringArray arrMacAddress;
// 	CStringArray arrHostName;
// 	CStringArray arrIP;
// 	CStringArray arrOS;
// 	CStringArray arrParName;
// 	CStringArray arrPlyName;
// 	CStringArray arrLicenseKey;
// 	CStringArray arrUserName;
// 	CStringArray arrFileName;
// 	CStringArray arrFullPath;
// 	CStringArray arrPatName;
// 	CStringArray arrPatCnt;
// 	CStringArray arrPatChkCnt;
// 	CStringArray arrKwdName;
// 	CStringArray arrKwdCnt;
// 	CStringArray arrKwdChkCnt;
// 	CStringArray arrTime;
// 	CStringArray arrLogTime;
// 	CStringArray arrEncrypt;
// }SECRET_LOG;

typedef struct _WIN_PATCH_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrTitle;
	CStringArray arrDesc;
	CStringArray arrGrade;
	CStringArray arrReboot;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
}WIN_PATCH_LOG;

typedef struct _FIREWALL_LOG
{
	CStringArray arrProtocol;
	CStringArray arrDirection;
	CStringArray arrIP;
	CStringArray arrBrkIP;
	CStringArray arrPort;
	CStringArray arrSPort;
	CStringArray arrEPort;
	CStringArray arrSIP;
	CStringArray arrEIP;
	CStringArray arrName;
	CStringArray arrTime;
	CStringArray arrUserID;
	CStringArray arrPCName;
	CStringArray arrMacAddress;
	CStringArray arrPCID;
	CStringArray arrCurrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrUrl;
	CStringArray arrRedir;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;

}FIREWALL_LOG;

typedef struct _FTP_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrAddr;
	CStringArray arrFileName;
	CStringArray arrFileSize;
	
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	
	CStringArray arrTime;
}FTP_LOG;

typedef struct _SMTP_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrSender;
	CStringArray arrReceiver;
	CStringArray arrTitle;
	CStringArray arrFileName;
	CStringArray arrFileSize;
	CStringArray arrTime;

}SMTP_LOG;


typedef struct _WEBMAIL_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTitle;
	CStringArray arrContents;
	CStringArray arrSender;
	CStringArray arrReceiver;
	CStringArray arrWebSvr;
	CStringArray arrFileName;
	CStringArray arrFileSize;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
}WEBMAIL_LOG;

typedef struct _TRAYMAIL_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;;
	CStringArray arrTitle;
	CStringArray arrContents;
	CStringArray arrSender;
	CStringArray arrReceiver;
	CStringArray arrFileName;
	CStringArray arrFileSize;
	CStringArray arrTime;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
}TRAYMAIL_LOG;


typedef struct _SMB_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrCurrIP;
	CStringArray arrFileName;
	CStringArray arrTime;
}SMB_LOG;

typedef struct _AUTH_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrAuthType;
	CStringArray arrSuccess;
	CStringArray arrCount;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;

}AUTH_LOG;


typedef struct _MEDIA_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPCName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrMediaName;
	CStringArray arrMediaString;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;

}MEDIA_LOG;



//내부 복호화 요청
typedef struct _IN_DECRYPT_REQ
{
	TCHAR szSendName[50];
	TCHAR szComment[100+1];
	TCHAR szLicKeyIdx[10];
	CStringArray arrRecvName;
	CStringArray arrFileName;
	CStringArray arrHashKey;
	CStringArray arrFileEncKey;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKeyword;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrPatternID;

}IN_DECRYPT_REQ;

//내부 복호화 로그
typedef struct _IN_DECRYPT_LOG
{
	CStringArray arrFileName;
	TCHAR szRecvName[50];
	char szPCID[40];
	char szMacAddress[30];
	char szHostName[50];
	char szIP[20];
	char szOSName[256];
	CStringArray arrHashKey;
	CStringArray arrResult;
	CStringArray arrFilePath;

}IN_DECRYPT_LOG;

//내부 복호화
typedef struct _IN_DECRYPT
{
	CStringArray arrFileName;
	TCHAR szRecvName[50];
	CStringArray arrHashKey;

}IN_DECRYPT;

#define OUT_DECRYPT_SIGNKEY_MAX_LENGTH 33	// 2016-08-31 kh.choi 복호화키 최대길이 32자로 define + NULL 문자 1
#define OUT_DECRYPT_FULL_SIGNKEY_MAX_LENGTH 35
//외부 복호화 요청
typedef struct _OUT_DECRYPT_REQ
{
	TCHAR szSendName[50];
	TCHAR szRecvName[50];
	char szPhoneNumber[OUT_DECRYPT_FULL_SIGNKEY_MAX_LENGTH * 2];
	TCHAR szComment[100+1];
	TCHAR szLicKeyIdx[10];
	CStringArray arrFileName;
	CStringArray arrHashKey;
	CStringArray arrFileEncKey;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKeyword;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;

}OUT_DECRYPT_REQ;

//외부 복호화 로그
typedef struct _OUT_DECRYPT_LOG
{
	char szSignKey[OUT_DECRYPT_SIGNKEY_MAX_LENGTH * 2];
	char szPhoneNumber[OUT_DECRYPT_FULL_SIGNKEY_MAX_LENGTH * 2];
	char szMacAddress[30];
	char szHostName[50];
	char szIP[20];
	char szOSName[50];
	CStringArray arrFilePath;
	CStringArray arrFileName;
	CStringArray arrHashKey;
	CStringArray arrResult;
}OUT_DECRYPT_LOG;

//외부 복호화 
typedef struct _OUT_DECRYPT
{
	CStringArray arrFileName;
	char szSignKey[OUT_DECRYPT_SIGNKEY_MAX_LENGTH * 2];
	char szPhoneNumber[OUT_DECRYPT_FULL_SIGNKEY_MAX_LENGTH * 2];
	CStringArray arrHashKey;
}OUT_DECRYPT;

//외부 인증키 문자 발송
typedef struct _DEC_AUTH_KEY
{
	char szPhoneNumber[OUT_DECRYPT_FULL_SIGNKEY_MAX_LENGTH * 2];
	CStringArray arrFileName;
	CStringArray arrHashKey;
}DEC_AUTH_KEY;

//리턴으로 받은 파일에 대한 암호화 키 
typedef struct _DEC_KEY_INFO
{
	CStringArray arrDecKey;
	CStringArray arrFileName;
	CStringArray arrdecSuccess;
	CStringArray arrIsSign;
	CStringArray arrIsReject;

}DEC_KEY_INFO;

//복호화 요청의 리턴으로 파일 상태 확인
typedef struct _DEC_REQ_RESULT
{
	CStringArray arrFileName;
	CStringArray arrRecvName;
	CStringArray arrReturn;
	CStringArray arrHashKey;

}DEC_REQ_RESULT;

typedef struct _DECRYPT_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrFileName;
	CStringArray arrEncFullPath;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrPlyIdx;
	CStringArray arrPatternID;

}DECRYPT_LOG;


typedef struct _VACCINE_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrVaccineName;
	CStringArray arrInstall;
	CStringArray arrRun;
	CStringArray arrLastUpdate;
	CStringArray arrRealMode;
	CStringArray arrTime; 

}VACCINE_LOG;

typedef struct _SHARE_FOLDER_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrPath;
	CStringArray arrName;
	CStringArray arrIsEnc;
	CStringArray arrTime;

}SHARE_FOLDER_LOG;



typedef struct _HDD_SPACE_LOG
{
	TCHAR szUserID[50];
	TCHAR szPCID[40];
	TCHAR szMacAddress[30];
	TCHAR szPcName[30];
	TCHAR szIP[20];
	TCHAR szOSName[128];
	TCHAR szParName[50];
	TCHAR szPlyName[50];
	TCHAR szLicenseKey[30];
	TCHAR szUserName[20];
	TCHAR szCpuName[128];
	int		   iRamSize;
	TCHAR szWorkgroupName[64];
	int iUsrIdx;
	int iGroupIdx;
	int iLicKeyIdx;
	int	iTotalSpace;
	int iUsedSpace;
	TCHAR szTime[30];

}HDD_SPACE_LOG;


typedef struct _ENC_DELETE_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrEncFullPath;
	CStringArray arrFileName;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrPlyIdx;
	CStringArray arrEncFileHash;	//hhh:2012.12.10
	CStringArray arrPatternID;

}ENC_DELETE_LOG;

typedef struct _ENC_PRINT_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray	arrCopy;
	CStringArray arrPrint;
	CStringArray arrType;
	CStringArray arrPrintName;
	CStringArray arrFileName;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrPrintIdx; // 2016-08-01 sy.choi 프린트로그 인덱스 추가; delete시 프린트로그 인덱스만 참조하도록 수정

}ENC_PRINT_LOG;



typedef struct _GENERAL_DELETE_LOG
{
	CStringArray arrUserID;
	CStringArray arrPCID;
	CStringArray arrMacAddress;
	CStringArray arrPcName;
	CStringArray arrIP;
	CStringArray arrOS;
	CStringArray arrParName;
	CStringArray arrPlyName;
	CStringArray arrLicenseKey;
	CStringArray arrUserName;
	CStringArray arrFullPath;
	CStringArray arrFileName;
	CStringArray arrPatName;
	CStringArray arrPatCnt;
	CStringArray arrPatChkCnt;
	CStringArray arrKwdName;
	CStringArray arrKwdCnt;
	CStringArray arrKwdChkCnt;
	CStringArray arrTime;
	CStringArray arrUsrIdx;
	CStringArray arrGroupIdx;
	CStringArray arrLicKeyIdx;
	CStringArray arrPlyIdx;
	CStringArray arrFileHash;		//hhh: 2012.12.10
	CStringArray arrPatternID;		
}GENERAL_DELETE_LOG;

typedef struct _ALL_USER_INFO
{

	CStringArray arrName;
	CStringArray arrID;
	CStringArray arrDep;
	CStringArray arrParent;

}ALL_USER_INFO;

//서버로 새로운 백신정보를 추가시키기 위해
typedef struct _NEW_VACCINE_INFO
{
	CStringArray arrName;
	CStringArray arrPath;

}NEW_VACCINE_INFO;

//CmsMemberData -> CmsTray
#define MSG_FILE_DECRYPT_REJECT      WM_USER + 100
#define MSG_FILE_DECRYPT_READY        WM_USER + 101
#define MSG_FILE_DECRYPT_SUCCESS    WM_USER + 102
#define MSG_FILE_ALREADY_DECRYPT	 WM_USER + 103
#endif
