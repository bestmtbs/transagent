/**
 @file      RegUtils.h 
 @brief     Registry 관련 function 선언 파일 
 @author    hang ryul lee
 @date      create 2007.10.08  
*/

#pragma once

#define MAX_KEYNAME_LEN      255
#define MAX_VALUENAME_LEN    255
#define MAX_VALUE_LEN        1024

// Set
bool    SetRegMultiStringValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue, const bool _bForce= false);
bool    SetRegStringValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue, const bool _bForce= false);
bool    SetRegDWORDValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const DWORD &_dwValue, const bool _bForce= false);
bool    AddRegKey(const HKEY _hRoot, const CString &_sKey, const CString &_sAddKey, const bool _bForce=false);
bool    CreateRegKey(const HKEY _hRoot, const CString &_sKey);

// Get
CString GetRegStringValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sDef=_T(""), BOOL bFroceStr=FALSE);
DWORD   GetRegDWORDValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const DWORD &_dwDef=0);
bool    GetRegValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, CString &_sRe);
INT     GetRegValueNameList(const HKEY _hRoot, const CString &_sKey, CStringList &_sValNameList);
INT     GetRegValueList(const HKEY _hRoot, const CString &_sKey, CStringList &_sValList);

INT     GetRegRootKeyList(CStringList &_sRootList);
INT     GetRegSubKeyList(const HKEY _hRoot, const CString &_sKey, CStringList &_sKeyList);
INT     GetRegSubKeyWithSubCount(const HKEY _hRoot, const CString &_sKey, CStringList &_sKeyList);
INT     GetSubKeyCount(const HKEY _hRoot, const CString &_sKey);

// Exists Check
bool    IsRegKeyExists(const HKEY _hRoot, const CString &_sKey);
bool    IsRegValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName);

// edit
bool    DelRegKey(const HKEY _hRoot, const CString &_sDelKey, const bool _bForce = false);
bool    DelRegValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValueName);

bool    RenameRegKey(const HKEY _hRoot, const CString &_sOldKey, const CString &_sNewKey);
bool    RenameRegValue(const HKEY _hRoot, const CString &_sSubKey, const CString &_sOldVal, const CString &_sNewVal);

bool    CopyRegKeys(const HKEY _hRoot, const CString &_sOldKey, const CString &_sNewKey);
bool    CopyRegValues(const HKEY _hRoot, const CString &_sOldKey, const CString &_sNewKey);
bool    CopyRegValue(const HKEY _hRoot, const CString &_sOldKey, const CString &_sOldVal, const CString &_sNewKey, const CString &_sNewVal);

//ycpark 64비트 환경에서 32비트 레지스트리 접근함수 추가
CString GetRegStringWow64Value(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sDef=_T(""));
bool    SetRegStringWow64Value(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue);
bool    IsRegWow64KeyExists(const HKEY _hRoot, const CString &_sKey);
bool    IsRegWow64ValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName);
bool    DelRegWow64Key(const HKEY _hRoot, const CString &_sKey, const CString &_sDelKey);
bool    DelRegWow64Value(const HKEY _hRoot, const CString &_sKey, const CString &_sValueName);

//hhh 64비트 환경에서 32비트 프로세스가 64비트 레지스트리 접근함수 추가 (2012.10.9)
bool    SetRegStringValueWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue, const bool _bForce= false);
CString GetRegStringValueWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sDef=_T(""));
bool	IsRegKeyExistsWow64(const HKEY _hRoot, const CString &_sKey);
bool	CreateRegKeyWow64(const HKEY _hRoot, const CString &_sKey);
bool SetRegDWORDValueWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const DWORD &_dwValue, const bool _bForce = false);

// convert
CString RegValueTypeToText(const DWORD _dwType);
DWORD   TextToRegValueType(const CString &_sType);

HKEY    TextToHKEY(const CString &_sText);
CString HKEYToText(const HKEY _hKey);

HKEY	RootKeyMapping(const HKEY _hRoot);
CString SubKeyMapping(const HKEY _hRoot, const CString &_sSubKey);

void    ParseValueString(const CString &_sValueStr, CString *_pName, CString *_pType, CString *_pData);
