/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.05.29		
********************************************************************************/
/**
@file       ConnectParamInfo.cpp 
@brief     ConnectParamInfo 구현 파일  
@author   jhlee
@date      create 2011.07.20
*/
#include "StdAfx.h"
#include "ConnectParamInfo.h"
#include "PathInfo.h"
#include "UtilParse.h"
#include "json/JSON.h"
#include "CmsDBManager.h"
#ifdef _SETUP
#include "../../20_Installer/CmsSetup/CmsDBManager_Setup.h"
#endif


/********************************************************************************
@class      ConnectParamInfo
@brief      서버와 클라이언트 통신간의 주고받을 데이터 생성
@note      사용자 최초등록, 사용자 비밀번호 등록, 정책 요청, 로그 전송,  클라이언트 정보 전달
*********************************************************************************/

#define	 HEADER			_T("header")
#define  SESSION			_T("Issession")
#define  CRYPTO			_T("Iscrypto")
#define  ENCRYPT			_T("Isencrypt")
#define  DEVICEID			_T("Deviceid")
#define  TRID				_T("Trid")
#define  SITEIDX			_T("Siteidx")
#define  TXID				_T("Txid")
#define  VERSION			_T("Agentver")
#define	 TOKEN				_T("Authtoken")
#define	 LEVEL				_T("UserLevel")
#define  BODY		        _T("body")
#define  USERID				_T("id")
#define	 USRIDX			_T("usrIdx")
#define  USERPWD			_T("password")
#define	 PCID					_T("pcid")
#define  RESULT			_T("result")
#define	 ERRORNUM		_T("Errornum")
#define  PWD_TYPE      _T("passwordType")
#define  IP						_T("ip")
#define  AGENT_VER		_T("agtVersion")

CString CConnectParamInfo::m_strSiteIndex = _T("");		// 2017-05-22 kh.choi static 멤버 초기화

CConnectParamInfo::CConnectParamInfo(void)
{
	GetSiteIndex();
}

CConnectParamInfo::~CConnectParamInfo(void)
{
}

/**
@brief     CreateAuthInfoData
@author   JHLEE
@date      2011.07.20
@note      사용자 최초등록, 사용자 추가 등록 시 서버 전달에 필요한 데이터를 생성한다.
-로그인 요청-
{
	"mbdata":{
		"header":{
				"Issession":"0",
				"Iscrypto":"0",
				"Deviceid":"",
				"Trid":"100001",
				"Siteidx":"12",
				"Txid":"",
				"Agentver":""
			},
			"body":{
				"password":"pass1232",
				"id":"userid"
			}
		}
}
*/
CString CConnectParamInfo::CreateAuthInfoData(CString _strUserID, CString _strPwd, CString _strTrid, CString _strPCID, CString _strKey, CString _strSiteIndex, CString _strIP, CString _strAgtVer)
{
	//ntype에 따라 trid값을 다르게 세팅해준다.
	JSONObject root, root2, root3;

	CString strReturn = _T(""), strTrid = _T("");

	strTrid = _strTrid;

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[ENCRYPT] = new JSONValue(_T(""));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(strTrid);
	root3[SITEIDX] = new JSONValue((double)_ttoi(_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));
	root3[TOKEN] = new JSONValue(_T(""));
	root3[LEVEL] = new JSONValue((double)-1);

	root[HEADER] = new JSONValue(root3);

	root2[USERID] =  new JSONValue(_strUserID);
	root2[USERPWD] =  new JSONValue(_strPwd);

	if( strTrid == _T("110002"))
	{
		root2[PCID] = new JSONValue(_strPCID);
		root2[IP] = new JSONValue(_strIP);
		root2[AGENT_VER] = new JSONValue(_strAgtVer);
	}
		

	root2[_T("licKey")] = new JSONValue(_strKey);

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
	   
  //출력값 : value->Stringfy().c_str()
}



CString CConnectParamInfo::CreateData(CString _strTrid)
{
	//ntype에 따라 trid값을 다르게 세팅해준다.
	JSONObject root, root2;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root2[SESSION] = new JSONValue(_T("0"));
	root2[CRYPTO] = new JSONValue(_T("0"));
	root2[ENCRYPT] = new JSONValue(_T(""));
	root2[DEVICEID] = new JSONValue(_T(""));
	root2[TRID] =  new JSONValue(_strTrid);
	root2[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root2[TXID] = new JSONValue(_T("1"));
	root2[VERSION] = new JSONValue(_T(""));
	root2[TOKEN] = new JSONValue(_T(""));
	root2[LEVEL] = new JSONValue((double)-1);

	
	root[HEADER] = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

	//출력값 : value->Stringfy().c_str()
}

CString CConnectParamInfo::CreatePwdChangeInfoData(CString _strTrid, int _iUsrIdx, CString _strPwd,CString _strDueDate, CString _strLicKey, CString _strPCID, int _iLicKeyIdx)
{
	//ntype에 따라 trid값을 다르게 세팅해준다.
	JSONObject root, root2, root3;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[ENCRYPT] = new JSONValue(_T(""));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_strTrid);
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));
	root3[TOKEN] = new JSONValue(_T(""));
	root3[LEVEL] = new JSONValue((double)-1);


	root[HEADER] = new JSONValue(root3);

	root2[USRIDX] =  new JSONValue((double)_iUsrIdx);
	root2[USERPWD] =  new JSONValue(_T(""));
	if( _strTrid == _T("110923") )
		root2[_T("newPassword")] =  new JSONValue(_strPwd);
	else
		root2[USERPWD] =  new JSONValue(_strPwd);

	root2[_T("nextDueDate")] =  new JSONValue(_strDueDate);
	
	if( _strTrid == _T("110003"))
	{
		root2[_T("licKey")] = new JSONValue(_strLicKey);
		root2[PCID] = new JSONValue(_strPCID);
	}
	if(_strTrid == _T("110923")) {
		root2[_T("licKeyIdx")] = new JSONValue((double)_iLicKeyIdx);
	}

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

	//출력값 : value->Stringfy().c_str()
}

CString CConnectParamInfo::CreatePCInfoData(int _iInsUsrIdx, CString _strPCID, CString _strIP, CString _strMac, CString _strPCName, CString _strTrid, CString _strLicenseKey, CString _strVersion,CString _strOSName, CString _strUserIdx)
{
	JSONObject root, root2, root3;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_strTrid);
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("insUsrIdx")] =  new JSONValue((double)_iInsUsrIdx);
	root2[_T("pcid")] =  new JSONValue(_strPCID);
	root2[_T("demo")] = new JSONValue(_T("1"));
	root2[_T("ip")] = new JSONValue(_strIP);
	root2[_T("mac")] = new JSONValue(_strMac);
	root2[_T("pcname")] = new JSONValue(_strPCName);
	root2[_T("versionInfo")] = new JSONValue(_strVersion);
	root2[_T("status")] = new JSONValue(_T("0"));
	root2[_T("licKey")] = new JSONValue(_strLicenseKey);
	root2[_T("os")] = new JSONValue(_strOSName);
	root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_strUserIdx));

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateEncryptFileLogData(ENC_POLICY_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("141022"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] =  new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("filIdx")] = new JSONValue((double)-1);
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("filHashKey")] = new JSONValue(_pLog->arrEncHash.GetAt(i));
		root2[_T("fullPath")] = new JSONValue(_pLog->arrEncFullPath.GetAt(i));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatChkCnt.GetAt(i)));
		root2[_T("kwdPlyIdx")]= new JSONValue((double)-1);
		root2[_T("keyword")]= new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdChkCnt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));
		root2[_T("encDate")]= new JSONValue(_pLog->arrEncTime.GetAt(i));
		root2[_T("plyIdx")]= new JSONValue((double)_ttoi(_pLog->arrPlyIdx.GetAt(i)));
		root2[_T("patId")]= new JSONValue(_pLog->arrPatternID.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}


CString CConnectParamInfo::CreateGeneralFileLogData(GENERAL_POLICY_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("148022"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] =  new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		//root2[_T("filIdx")] = new JSONValue((double)-1);
		root2[_T("filName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("filHashKey")] = new JSONValue(_pLog->arrHash.GetAt(i));
		root2[_T("fullPath")] = new JSONValue(_pLog->arrFullPath.GetAt(i));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatChkCnt.GetAt(i)));
		root2[_T("keyword")]= new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdChkCnt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));
		root2[_T("encDate")]= new JSONValue(_pLog->arrEncTime.GetAt(i));
		root2[_T("plyIdx")]= new JSONValue((double)_ttoi(_pLog->arrPlyIdx.GetAt(i)));
		root2[_T("patId")] = new JSONValue(_pLog->arrPatternID.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}
/*CString CConnectParamInfo::CreateSecretLogData(SECRET_LOG* log)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("144002"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< log->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(log->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(log->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(log->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(log->arrHostName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(log->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(log->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(log->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(log->arrPlyName.GetAt(i));
		root2[_T("filName")] = new JSONValue(log->arrFileName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(log->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(log->arrUserName.GetAt(i));
		root2[_T("fullPath")] = new JSONValue(log->arrFullPath.GetAt(i));
		root2[_T("patName")] = new JSONValue(log->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(log->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(log->arrPatChkCnt.GetAt(i)));
		root2[_T("kwdword")]= new JSONValue(log->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(log->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(log->arrKwdChkCnt.GetAt(i)));
		root2[_T("bencrypt")]= new JSONValue((double)_ttoi(log->arrEncrypt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(log->arrTime.GetAt(i));
		root2[_T("logDate")]= new JSONValue(log->arrLogTime.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}*/

CString CConnectParamInfo::CreateWinPatchLogData(WIN_PATCH_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	
	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143102"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("title")]= new JSONValue(_pLog->arrTitle.GetAt(i));
		root2[_T("grade")]= new JSONValue((double)_ttoi(_pLog->arrGrade.GetAt(i)));
		root2[_T("description")]= new JSONValue(_pLog->arrDesc.GetAt(i));
		root2[_T("reboot")] = new JSONValue((double)0);
		root2[_T("patFileCnt")]= new JSONValue((double)-1);
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());
	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateFirewallLogData(FIREWALL_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143112"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrProtocol.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPCName.GetAt(i));
		root2[_T("ip")] = new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] = new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] = new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] = new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));	
		root2[_T("direction")] = new JSONValue((double)_ttoi(_pLog->arrDirection.GetAt(i)));
		root2[_T("brkIp")] = new JSONValue(_pLog->arrBrkIP.GetAt(i));
		root2[_T("port")] = new JSONValue((double)_ttoi(_pLog->arrPort.GetAt(i)));
		root2[_T("startIp")] = new JSONValue(_pLog->arrSIP.GetAt(i));
		root2[_T("startPort")] = new JSONValue((double)_ttoi(_pLog->arrSPort.GetAt(i)));
		root2[_T("endIp")] = new JSONValue(_pLog->arrEIP.GetAt(i));
		root2[_T("endPort")] = new JSONValue((double)_ttoi(_pLog->arrEPort.GetAt(i)));
		root2[_T("name")] = new JSONValue(_pLog->arrName.GetAt(i));
		root2[_T("type")] = new JSONValue((double)_ttoi(_pLog->arrProtocol.GetAt(i)));
		root2[_T("url")] = new JSONValue(_pLog->arrUrl.GetAt(i));
		root2[_T("forwardUrl")] = new JSONValue(_pLog->arrRedir.GetAt(i));
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());
	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}

//Tray에서 보낸 메일 로그 json처리
CString CConnectParamInfo::CreateTrayMailLogData(TRAYMAIL_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143272"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPCName.GetAt(i));
		root2[_T("ip")] = new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] = new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] = new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] = new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		//root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		//root2[_T("patOccCnt")] = new JSONValue((double)-1);
		//root2[_T("patChkCnt")] = new JSONValue((double)-1);
		//root2[_T("keyword")] = new JSONValue(_pLog->arrKwdName.GetAt(i));
		//root2[_T("kwdOccCnt")] = new JSONValue((double)-1);
		//root2[_T("kwdChkCnt")] = new JSONValue((double)-1);
		root2[_T("title")] = new JSONValue(_pLog->arrTitle.GetAt(i));
		root2[_T("contents")] = new JSONValue(_pLog->arrContents.GetAt(i));
		root2[_T("sender")] = new JSONValue(_pLog->arrSender.GetAt(i));
		root2[_T("receiver")] = new JSONValue(_pLog->arrReceiver.GetAt(i));
		//root2[_T("webSvr")] = new JSONValue(_pLog->arrWebSvr.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("fileSize")] = new JSONValue((double)_ttoi(_pLog->arrFileSize.GetAt(i)));
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));
		
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateWebMailLogData(WEBMAIL_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143162"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPCName.GetAt(i));
		root2[_T("ip")] = new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] = new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] = new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] = new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")] = new JSONValue((double)-1);
		root2[_T("patChkCnt")] = new JSONValue((double)-1);
		root2[_T("keyword")] = new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")] = new JSONValue((double)-1);
		root2[_T("kwdChkCnt")] = new JSONValue((double)-1);
		root2[_T("title")] = new JSONValue(_pLog->arrTitle.GetAt(i));
		root2[_T("contents")] = new JSONValue(_pLog->arrContents.GetAt(i));
		root2[_T("sender")] = new JSONValue(_pLog->arrSender.GetAt(i));
		root2[_T("receiver")] = new JSONValue(_pLog->arrReceiver.GetAt(i));
		root2[_T("webSvr")] = new JSONValue(_pLog->arrWebSvr.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("fileSize")] = new JSONValue((double)_ttoi(_pLog->arrFileSize.GetAt(i)));
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));
		
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateSMTPLogData(SMTP_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;
	
	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143142"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("sender")] = new JSONValue(_pLog->arrSender.GetAt(i));
		root2[_T("receiver")] = new JSONValue(_pLog->arrReceiver.GetAt(i));
		root2[_T("title")] = new JSONValue(_pLog->arrTitle.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("fileSize")] = new JSONValue((double)-1);
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));
		array.push_back(new JSONValue(root2));

	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateFTPLogData(FTP_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();
	
	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143132"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{

		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPCName.GetAt(i));

		root2[_T("ip")] = new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] = new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] = new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] = new JSONValue(_pLog->arrPlyName.GetAt(i));

		root2[_T("addr")] = new JSONValue(_pLog->arrAddr.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("fileSize")] = new JSONValue((double)_ttoi(_pLog->arrFileSize.GetAt(i)));
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));

		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateSMBLogData(SMB_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143192"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("ip")] = new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateAuthLogData(AUTH_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143172"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	//ID|PCID|MAC|IP|OS|PARNAME|PLYNAME|LICENSEKEY|USERNAME|TYPE|TIME
	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcName")] = new JSONValue(_pLog->arrPCName.GetAt(i));
		root2[_T("ip")] = new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] = new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] = new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] = new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("srcType")] = new JSONValue(_T("AGENT"));
		root2[_T("actType")] = new JSONValue(_pLog->arrAuthType.GetAt(i));
		root2[_T("agtLoginSucess")] = new JSONValue((double)_ttoi(_pLog->arrSuccess.GetAt(i)));
		root2[_T("failCnt")] = new JSONValue((double)_ttoi(_pLog->arrCount.GetAt(i)));
		root2[_T("currDate")] = new JSONValue(_pLog->arrTime.GetAt(i));
		
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}


CString CConnectParamInfo::GetPasswordType()
{
	return m_strPasswordType;
}

CString CConnectParamInfo::CreatePolicyRequest(int _iUsrIdx, CString _strTrid, CString _strLicenseKey)
{
	JSONObject root, root2, root3;

	CString strReturn = _T("") , strTrid = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	strTrid = _strTrid;

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(strTrid);
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[USRIDX] =  new JSONValue((double)_iUsrIdx);
	if( _strTrid ==  _T("110911")  || _strTrid == _T("131011") || _strTrid  == _T("110931") ||_strTrid  == _T("110941") || _strTrid  == _T("110951"))
	{
		root2[_T("licKey")] = new JSONValue(_strLicenseKey);

	}

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateUserInfoRequest(int _iUsrIdx, CString _strLicenseKey, int _iLicKeyIdx, CString _strEncKey)
{
	JSONObject root, root2, root3;

	CString strReturn = _T("") ;

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue( _T("119711"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[USRIDX] =  new JSONValue((double)_iUsrIdx);
	root2[_T("licKey")] =  new JSONValue(_strLicenseKey);
	root2[_T("licKeyIdx")] =  new JSONValue((double)_iLicKeyIdx);
	root2[_T("encKey")] = new JSONValue(_strEncKey);

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());
	//DBGLOG(strReturn);

	//CString strLog = _T("");
	//strLog.Format(_T("[sychoi] CreateUserInfoRequest %s %s"), strReturn, _strEncKey);
	//OutputDebugString(strLog);
	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateInDecReqData(IN_DECRYPT_REQ* _decrypt)
{
	JSONObject root, root2, root3, root4;
	JSONArray array, array2;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105552"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("sndMail")]  = new JSONValue(_decrypt->szSendName);
	root2[_T("reqMsg")] = new JSONValue(_decrypt->szComment);
	root2[_T("licKeyIdx")] = new JSONValue(_decrypt->szLicKeyIdx);

	for( int i =0; i< _decrypt->arrFileName.GetSize(); i++ )
	{
	
		root4[_T("rcvMail")] = new JSONValue(_decrypt->arrRecvName.GetAt(i));
		root4[_T("fileName")] = new JSONValue(_decrypt->arrFileName.GetAt(i));
		root4[_T("fileHashKey")] = new JSONValue(_decrypt->arrHashKey.GetAt(i));
		root4[_T("fileEncKey")] = new JSONValue(_decrypt->arrFileEncKey.GetAt(i));
		root4[_T("patName")] = new JSONValue(_decrypt->arrPatName.GetAt(i));
		root4[_T("patId")] = new JSONValue(_decrypt->arrPatternID.GetAt(i));  //hhh(2013.09.02) :패턴ID 추가
		root4[_T("patOccCnt")] = new JSONValue((double)_ttoi(_decrypt->arrPatCnt.GetAt(i)));
		root4[_T("patChkCnt")] = new JSONValue((double)_ttoi(_decrypt->arrPatChkCnt.GetAt(i)));
		root4[_T("keyword")] = new JSONValue(_decrypt->arrKeyword.GetAt(i));
		root4[_T("kwdOccCnt")] = new JSONValue((double)_ttoi(_decrypt->arrKwdCnt.GetAt(i)));
		root4[_T("kwdChkCnt")] = new JSONValue((double)_ttoi(_decrypt->arrKwdChkCnt.GetAt(i)));

		array2.push_back(new JSONValue(root4));
	}

	root2[_T("decFiles")] = new JSONValue(array2);

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateInDecryptData(IN_DECRYPT* _decrypt)
{
	JSONObject root, root2, root3,root4;
	JSONArray array, array2;
	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105553"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));
	
	root[HEADER] = new JSONValue(root3);

	root2[_T("rcvMail")] =  new JSONValue(_decrypt->szRecvName);

	for( int i=0; i<_decrypt->arrFileName.GetSize(); i++)
	{
		root4[_T("fileName")] =  new JSONValue(_decrypt->arrFileName.GetAt(i));	
		root4[_T("fileHashKey")] =  new JSONValue(_decrypt->arrHashKey.GetAt(i));

		array2.push_back(new JSONValue(root4));
	}
	
	root2[_T("decFiles")] = new JSONValue(array2);

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}


CString CConnectParamInfo::CreateInDecryptLogData(IN_DECRYPT_LOG* _decrypt)
{
	JSONObject root, root2, root3, root4;
	JSONArray array, array2;
	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105559"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("rcvMail")] =  new JSONValue(_decrypt->szRecvName);	
	root2[_T("pcid")] =  new JSONValue((CString)_decrypt->szPCID);
	root2[_T("mac")] =  new JSONValue((CString)_decrypt->szMacAddress);
	root2[_T("pcname")] =  new JSONValue((CString)_decrypt->szHostName);
	root2[_T("ip")] =  new JSONValue((CString)_decrypt->szIP);
	root2[_T("os")] =  new JSONValue((CString)_decrypt->szOSName);



	for( int i=0; i<_decrypt->arrFileName.GetSize(); i++ )
	{
		root4[_T("fileName")] =  new JSONValue(_decrypt->arrFileName.GetAt(i));
		root4[_T("fileHashKey")] =  new JSONValue(_decrypt->arrHashKey.GetAt(i));
		root4[_T("decSucess")] =  new JSONValue((double)_ttoi(_decrypt->arrResult.GetAt(i)));

		array2.push_back(new JSONValue(root4));
	}
	
	root2[_T("decFiles")] = new JSONValue(array2);

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateOutDecReqData(OUT_DECRYPT_REQ* _decrypt)
{
	JSONObject root, root2, root3, root4;
	JSONArray array, array2;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105542"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("mobNum")]  = new JSONValue((CString)_decrypt->szPhoneNumber);
	root2[_T("sndMail")]  = new JSONValue(_decrypt->szSendName);
	root2[_T("rcvMail")] =new  JSONValue(_decrypt->szRecvName);
	root2[_T("reqMsg")] = new JSONValue(_decrypt->szComment);
	root2[_T("licKeyIdx")] = new JSONValue( (double)_ttoi(_decrypt->szLicKeyIdx));


	for( int i =0; i< _decrypt->arrFileName.GetSize(); i++ )
	{
		root4[_T("fileName")] = new JSONValue(_decrypt->arrFileName.GetAt(i));
		root4[_T("fileHashKey")] = new JSONValue(_decrypt->arrHashKey.GetAt(i));
		root4[_T("fileEncKey")] = new JSONValue(_decrypt->arrFileEncKey.GetAt(i));
		root4[_T("patName")] = new JSONValue(_decrypt->arrPatName.GetAt(i));
		root4[_T("patOccCnt")] = new JSONValue((double)_ttoi(_decrypt->arrPatCnt.GetAt(i)));
		root4[_T("patChkCnt")] = new JSONValue((double)_ttoi(_decrypt->arrPatChkCnt.GetAt(i)));
		root4[_T("keyword")] = new JSONValue(_decrypt->arrKeyword.GetAt(i));
		root4[_T("kwdOccCnt")] = new JSONValue((double)_ttoi(_decrypt->arrKwdCnt.GetAt(i)));
		root4[_T("kwdChkCnt")] = new JSONValue((double)_ttoi(_decrypt->arrKwdChkCnt.GetAt(i)));

		array2.push_back(new JSONValue(root4));
	}

	root2[_T("decFiles")] = new JSONValue(array2);

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}



CString CConnectParamInfo::CreateOutDecReqData_Check(OUT_DECRYPT_REQ* _decrypt)
{
	JSONObject root, root2, root3, root4;
	JSONArray array, array2;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105544"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("mobNum")]  = new JSONValue((CString)_decrypt->szPhoneNumber);
	root2[_T("sndMail")]  = new JSONValue(_decrypt->szSendName);
	root2[_T("rcvMail")] =new  JSONValue(_decrypt->szRecvName);
	root2[_T("reqMsg")] = new JSONValue(_decrypt->szComment);
	root2[_T("licKeyIdx")] = new JSONValue( (double)_ttoi(_decrypt->szLicKeyIdx));


	//for( int i =0; i< _decrypt->arrFileName.GetSize(); i++ )
	//{
	//	root4[_T("fileName")] = new JSONValue(_decrypt->arrFileName.GetAt(i));
	//	root4[_T("fileHashKey")] = new JSONValue(_decrypt->arrHashKey.GetAt(i));
	//	root4[_T("fileEncKey")] = new JSONValue(_decrypt->arrFileEncKey.GetAt(i));
	//	root4[_T("patName")] = new JSONValue(_decrypt->arrPatName.GetAt(i));
	//	root4[_T("patOccCnt")] = new JSONValue((double)_ttoi(_decrypt->arrPatCnt.GetAt(i)));
	//	root4[_T("patChkCnt")] = new JSONValue((double)_ttoi(_decrypt->arrPatChkCnt.GetAt(i)));
	//	root4[_T("keyword")] = new JSONValue(_decrypt->arrKeyword.GetAt(i));
	//	root4[_T("kwdOccCnt")] = new JSONValue((double)_ttoi(_decrypt->arrKwdCnt.GetAt(i)));
	//	root4[_T("kwdChkCnt")] = new JSONValue((double)_ttoi(_decrypt->arrKwdChkCnt.GetAt(i)));

	//	array2.push_back(new JSONValue(root4));
	//}


	//if(0< array2.size()){
	//	root2[_T("decFiles")] = new JSONValue(array2);		
	//}

	array.push_back(new JSONValue(root2));
	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}


CString CConnectParamInfo::CreateOutDecryptData(OUT_DECRYPT* _decrypt)
{
	JSONObject root, root2, root3,root4;
	JSONArray array, array2;

	CString strReturn = _T("");

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105543"));
	root3[SITEIDX] = new JSONValue((double)-1);
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("signKey1")] =  new JSONValue((CString)_decrypt->szSignKey);
	root2[_T("mobNum")] =  new JSONValue((CString)_decrypt->szPhoneNumber);

	for( int i =0; i< _decrypt->arrFileName.GetSize(); i++ )
	{
		root4[_T("fileName")] =  new JSONValue(_decrypt->arrFileName.GetAt(i));
		root4[_T("fileHashKey")] =  new JSONValue(_decrypt->arrHashKey.GetAt(i));

		array2.push_back(new JSONValue(root4));
	}

	root2[_T("decFiles")] = new JSONValue(array2);

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}


CString CConnectParamInfo::CreateOutDecryptLogData(OUT_DECRYPT_LOG* _decrypt)
{
	JSONObject root, root2, root3, root4;
	JSONArray array, array2;
	CString strReturn = _T("");

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105549"));
	root3[SITEIDX] = new JSONValue((double)-1);
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("signKey1")] =  new JSONValue((CString)_decrypt->szSignKey);
	root2[_T("mobNum")] =  new JSONValue((CString)_decrypt->szPhoneNumber);
	root2[_T("mac")] =  new JSONValue((CString)_decrypt->szMacAddress);
	root2[_T("pcname")] =  new JSONValue((CString)_decrypt->szHostName);
	root2[_T("ip")] =  new JSONValue((CString)_decrypt->szIP);
	root2[_T("os")] =  new JSONValue((CString)_decrypt->szOSName);


	for( int i=0; i<_decrypt->arrFileName.GetSize(); i++)
	{
		root4[_T("fileName")] =  new JSONValue(_decrypt->arrFileName.GetAt(i));
		root4[_T("fileHashKey")] =  new JSONValue(_decrypt->arrHashKey.GetAt(i));
		root4[_T("decSucess")] =  new JSONValue((double)_ttoi(_decrypt->arrResult.GetAt(i)));

		array2.push_back(new JSONValue(root4));
	}

	root2[_T("decFiles")] = new JSONValue(array2);

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());
	
	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateDecryptAuthKeyData(DEC_AUTH_KEY* _auth)
{
	JSONObject root, root2, root3, root4;
	JSONArray array, array2;
	CString strReturn = _T("");


	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("105540"));
	root3[SITEIDX] = new JSONValue((double)-1);
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("mobNum")] = new JSONValue((CString)_auth->szPhoneNumber);

	for( int i=0; i<_auth->arrFileName.GetSize(); i++)
	{
		root4[_T("fileName")] =  new JSONValue(_auth->arrFileName.GetAt(i));
		root4[_T("fileHashKey")] =  new JSONValue(_auth->arrHashKey.GetAt(i));
		array2.push_back(new JSONValue(root4));
	}

	root2[_T("decFiles")] = new JSONValue(array2);
	
	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;
}


CString CConnectParamInfo::CreateDecryptFileLogData(DECRYPT_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("146022"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("filIdx")] = new JSONValue((double)-1);
		root2[_T("filName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("fullPath")] = new JSONValue(_pLog->arrEncFullPath.GetAt(i));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatChkCnt.GetAt(i)));
		root2[_T("keyword")]= new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdChkCnt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));
		root2[_T("plyIdx")] = new JSONValue((double)_ttoi(_pLog->arrPlyIdx.GetAt(i)));
		root2[_T("patId")] = new JSONValue(_pLog->arrPatternID.GetAt(i));
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateVaccineLogData(VACCINE_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));	
	root3[TRID] =  new JSONValue(_T("143072"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("name")] = new JSONValue(_pLog->arrVaccineName.GetAt(i));
		root2[_T("install")] = new JSONValue((double)_ttoi(_pLog->arrInstall.GetAt(i)));
		root2[_T("run")] = new JSONValue((double)_ttoi(_pLog->arrRun.GetAt(i)));
		root2[_T("lastUpdate")] = new JSONValue((double)_ttoi(_pLog->arrLastUpdate.GetAt(i)));
		root2[_T("realTimeMonitor")]= new JSONValue((double)_ttoi(_pLog->arrRealMode.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateShareFolderLogData(SHARE_FOLDER_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143312"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("name")] = new JSONValue(_pLog->arrName.GetAt(i));
		root2[_T("path")] = new JSONValue(_pLog->arrPath.GetAt(i));
		root2[_T("isEnc")] = new JSONValue((double)_ttoi(_pLog->arrIsEnc.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateHDDSpaceLogData(HDD_SPACE_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143972"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("id")] =  new JSONValue(_pLog->szUserID);
	root2[_T("pcid")] =  new JSONValue(_pLog->szPCID);
	root2[_T("mac")] = new JSONValue(_pLog->szMacAddress);
	root2[_T("pcname")] = new JSONValue(_pLog->szPcName);
	root2[_T("ip")] =  new JSONValue(_pLog->szIP);
	root2[_T("os")] =  new JSONValue(_pLog->szOSName);
	root2[_T("parName")] =  new JSONValue(_pLog->szParName);
	root2[_T("plyName")] =  new JSONValue(_pLog->szPlyName);
	root2[_T("licKey")] = new JSONValue(_pLog->szLicenseKey);
	root2[_T("usrName")] = new JSONValue(_pLog->szUserName);
	root2[_T("usrIdx")] = new JSONValue((double)_pLog->iUsrIdx);
	root2[_T("grpIdx")] = new JSONValue((double)_pLog->iGroupIdx);
	root2[_T("licKeyIdx")] = new JSONValue((double)_pLog->iLicKeyIdx);
	root2[_T("used")] = new JSONValue((double)_pLog->iUsedSpace);
	root2[_T("total")] = new JSONValue((double)_pLog->iTotalSpace);
	root2[_T("currDate")]= new JSONValue(_pLog->szTime);

	root2[_T("workgroup")]= new JSONValue(_pLog->szWorkgroupName);
	root2[_T("cpu")]= new JSONValue(_pLog->szCpuName);
	root2[_T("ram")]= new JSONValue((double)_pLog->iRamSize);

	
	

	array.push_back(new JSONValue(root2));

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateMediaLogData(MEDIA_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143982"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPCName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] = new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("mdaName")] = new JSONValue(_pLog->arrMediaName.GetAt(i));
		root2[_T("name")] = new JSONValue(_pLog->arrMediaString.GetAt(i));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);

	delete value;

	return strReturn;

}
CString CConnectParamInfo::CreateUninstallLogData(CString _strValue)
{

	JSONObject root, root2, root3;
	JSONArray array;

	CStringArray arrData;
	arrData.RemoveAll();

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("147002"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	CParseUtil::ParseStringToStringArray(_T("|"), _strValue,&arrData);

	if( arrData.GetSize() > 9)
	{
		root2[_T("id")] =  new JSONValue(arrData.GetAt(0));
		root2[_T("pcid")] =  new JSONValue(arrData.GetAt(1));
		root2[_T("mac")] =  new JSONValue(arrData.GetAt(2));
		root2[_T("pcname")] =  new JSONValue(arrData.GetAt(3));
		root2[_T("ip")] =  new JSONValue(arrData.GetAt(4));
		root2[_T("os")] =  new JSONValue(arrData.GetAt(5));
		root2[_T("parName")] =  new JSONValue(arrData.GetAt(6));
		root2[_T("plyName")] =  new JSONValue(arrData.GetAt(7));
		root2[_T("type")] =  new JSONValue((double)3);
		root2[_T("licKey")] =  new JSONValue(arrData.GetAt(8));
		root2[_T("delLicKey")] =  new JSONValue(arrData.GetAt(9));
		root2[_T("currDate")] =  new JSONValue(arrData.GetAt(10));

		root2[_T("usrName")] =  new JSONValue(arrData.GetAt(11));
		root2[_T("usrIdx")] =  new JSONValue(arrData.GetAt(12));
		root2[_T("grpIdx")] =  new JSONValue(arrData.GetAt(13));
		root2[_T("licKeyIdx")] =  new JSONValue(arrData.GetAt(14));



	}
	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
}

void CConnectParamInfo::GetSiteIndex()
{
	// 2017-05-17 kh.choi m_strSiteIndex 에 이미 값이 있으면 바로 리턴하도록 수정	// 2017-05-25 kh.choi 멤버변수의 사이트 인덱스가 기본값인 "0000" 이면 파일과 디비에서 사이트 인덱스 값을 다시 읽어오도록 수정
	if (m_strSiteIndex.GetLength() > 3 && 0 != m_strSiteIndex.CompareNoCase(STR_DEFAULT_SITE_INDEX)) {	// CompareNoCase 문자열이 같을 경우 0 반환
		////ezLog(_T("[ConnectParamInfo] [#####] m_strSiteIndex.GetLength(): %d, m_strSiteIndex: %s [line: %d, function: %s, file: %s]\r\n"), m_strSiteIndex.GetLength(), m_strSiteIndex, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-17 kh.choi 파일로그
		return;
	}

	CString strKeyFile = _T("");
	CString strKey = _T("");
	CString strSiteIndex = STR_DEFAULT_SITE_INDEX;	// default

	strKeyFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("license.cms"));

	CFile cFile;

	if( cFile.Open(strKeyFile, CFile::modeRead))
	{
		DWORD dwLength  = (DWORD)cFile.GetLength();
		char* pszBuffer = new char[dwLength+1];
		ZeroMemory(pszBuffer, dwLength+1);

		cFile.Read(pszBuffer, dwLength);
		strKey = pszBuffer;	// CString strKey = pszBuffer; 은 복사 생성자가 없어서 안된다함. http://www.tipssoft.com/bulletin/board.php?bo_table=FAQ&wr_id=1642

		delete [] pszBuffer;

		cFile.Close();	
	} else {
		////ezLog(_T("[ConnectParamInfo] [ERROR] file open failed. strKeyFile: %s, GetLastError(): %d [line: %d, function: %s, file: %s]\r\n"), strKeyFile, GetLastError(), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-17 kh.choi 파일로그
	}

	////ezLog(_T("[ConnectParamInfo] [*****] strKey: %s [line: %d, function: %s, file: %s]\r\n"), strKey, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-17 kh.choi 파일로그

	int iFind = strKey.Find(_T('-'));

	if (4 > iFind) {
		////ezLog(_T("[ConnectParamInfo] [ERROR] Can't get site index from license.cms file. strKeyFile: %s, strKey: %s [line: %d, function: %s, file: %s]\r\n"), strKeyFile, strKey, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-02-27 kh.choi 파일로그

		// 2017-05-17 kh.choi license.cms 파일에서 읽어오기 실패한 경우 ltb_config 테이블 etc_3 필드에서 읽어오기
		CString strQuery = _T("");
		CString strLicenseKey = _T("");
		CStringArray arrList;

		arrList.RemoveAll();

		CCmsDBManager dbConfig(DB_CONFIG);

		strQuery.Format(_T("SELECT * FROM %s"), DB_CONFIG);
		BOOL bResult = dbConfig.SelectQuery(strQuery, &arrList, _T("etc_3"));
		dbConfig.Free();

		if (bResult) {
			if (arrList.GetSize() >= 1) {	// ltb_config table의 record는 1개 이므로 record 갯수만 확인만 하고 etc_3 field 반환
				strLicenseKey = arrList.GetAt(0);

				if (strLicenseKey.IsEmpty()) {
					////ezLog(_T("[ConnectParamInfo] [ERROR] ltb_config table etc_3 field is empty. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-17 kh.choi 파일로그
				} else {
					int nFind = strLicenseKey.Find(_T('-'));
					if (4 > nFind) {
						////ezLog(_T("[ConnectParamInfo] [ERROR] ltb_config table etc_3 field incorrect. strLicenseKey: %s [line: %d, function: %s, file: %s]\r\n"), strLicenseKey, __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-17 kh.choi 파일로그
					} else {	// ltb_config 테이블의 etc_3 필드에서 읽어온 문자열에서 '-' 문자의 발견 위치가 4 이상인 경우
						strSiteIndex = strLicenseKey.Left(nFind);
					}
				}
			} else {
				////ezLog(_T("[ConnectParamInfo] [ERROR] ltb_config table is empty. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-17 kh.choi 파일로그
			}
		} else {
			//DBGLOG(_T("Select query failed. strQuery: %s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
			////ezLog(_T("[ConnectParamInfo] [ERROR] Select query failed. strQuery: %s [line: %d, function: %s, file: %s]\r\n"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);		// 2017-05-17 kh.choi 파일로그
		}
	} else {	// license.cms 에서 읽어온 문자열에서 '-' 문자의 발견 위치가 4 이상인 경우
		strSiteIndex = strKey.Left(iFind);
	}

	m_strSiteIndex = strSiteIndex;
}



CString CConnectParamInfo::CreateEncryptFileDelLogData(ENC_DELETE_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143962"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] =  new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("fullPath")] = new JSONValue(_pLog->arrEncFullPath.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatChkCnt.GetAt(i)));
		root2[_T("keyword")]= new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdChkCnt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));
		root2[_T("plyIdx")] = new JSONValue((double)_ttoi(_pLog->arrPlyIdx.GetAt(i)));
		root2[_T("filHashKey")] = new JSONValue( _pLog->arrEncFileHash.GetAt(i) ); 
		root2[_T("patId")] = new JSONValue(_pLog->arrPatternID.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateEncryptFilePrintLogData(ENC_PRINT_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("143182"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] =  new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("copyCnt")] = new JSONValue((double)_ttoi(_pLog->arrCopy.GetAt(i)));
		root2[_T("pages")] = new JSONValue((double)_ttoi(_pLog->arrPrint.GetAt(i)));
		root2[_T("type")] = new JSONValue((double)_ttoi(_pLog->arrType.GetAt(i)));
		root2[_T("prtName")] = new JSONValue(_pLog->arrPrintName.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatChkCnt.GetAt(i)));
		root2[_T("keyword")]= new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdChkCnt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}
CString CConnectParamInfo::CreateConnectInfo(int _iUserIdx, int _iLicKeyIdx, CString _strTrid)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_strTrid);
	//root3[TRID] =  new JSONValue(_T("100002"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("usrIdx")] = new JSONValue((double)_iUserIdx);
	root2[_T("licKeyIdx")] = new JSONValue((double)_iLicKeyIdx);

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);
	delete value;

	return strReturn;


}

CString CConnectParamInfo::CreateLicenseInfo(int _iUserIdx, int _iLicKeyIdx, CString _strTrid)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_strTrid);
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("usrIdx")] = new JSONValue((double)_iUserIdx);
	root2[_T("licKeyIdx")] = new JSONValue((double)_iLicKeyIdx);

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);
	delete value;

	return strReturn;


}

CString CConnectParamInfo::CreateUseAllUserInfo(int _iLicKeyIdx)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("119712"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("licKeyIdx")] = new JSONValue((double)_iLicKeyIdx);

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);
	delete value;

	return strReturn;


}



CString CConnectParamInfo::CreateGeneralFileDelLogData(GENERAL_DELETE_LOG* _pLog)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("148062"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pLog->arrUserID.GetSize(); i++)
	{
		root2[_T("id")] =  new JSONValue(_pLog->arrUserID.GetAt(i));
		root2[_T("pcid")] =  new JSONValue(_pLog->arrPCID.GetAt(i));
		root2[_T("mac")] = new JSONValue(_pLog->arrMacAddress.GetAt(i));
		root2[_T("pcname")] = new JSONValue(_pLog->arrPcName.GetAt(i));
		root2[_T("ip")] =  new JSONValue(_pLog->arrIP.GetAt(i));
		root2[_T("os")] =  new JSONValue(_pLog->arrOS.GetAt(i));
		root2[_T("parName")] =  new JSONValue(_pLog->arrParName.GetAt(i));
		root2[_T("plyName")] =  new JSONValue(_pLog->arrPlyName.GetAt(i));
		root2[_T("licKey")] =  new JSONValue(_pLog->arrLicenseKey.GetAt(i));
		root2[_T("usrName")] = new JSONValue(_pLog->arrUserName.GetAt(i));
		root2[_T("usrIdx")] = new JSONValue((double)_ttoi(_pLog->arrUsrIdx.GetAt(i)));
		root2[_T("grpIdx")] = new JSONValue((double)_ttoi(_pLog->arrGroupIdx.GetAt(i)));
		root2[_T("licKeyIdx")] = new JSONValue((double)_ttoi(_pLog->arrLicKeyIdx.GetAt(i)));
		root2[_T("fullPath")] = new JSONValue(_pLog->arrFullPath.GetAt(i));
		root2[_T("fileName")] = new JSONValue(_pLog->arrFileName.GetAt(i));
		root2[_T("patName")] = new JSONValue(_pLog->arrPatName.GetAt(i));
		root2[_T("patOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatCnt.GetAt(i)));
		root2[_T("patChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrPatChkCnt.GetAt(i)));
		root2[_T("keyword")]= new JSONValue(_pLog->arrKwdName.GetAt(i));
		root2[_T("kwdOccCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdCnt.GetAt(i)));
		root2[_T("kwdChkCnt")]= new JSONValue((double)_ttoi(_pLog->arrKwdChkCnt.GetAt(i)));
		root2[_T("currDate")]= new JSONValue(_pLog->arrTime.GetAt(i));
		root2[_T("plyIdx")] = new JSONValue((double)_ttoi(_pLog->arrPlyIdx.GetAt(i)));
		root2[_T("filHashKey")] = new JSONValue( _pLog->arrFileHash.GetAt(i) );
		root2[_T("patId")] = new JSONValue( _pLog->arrPatternID.GetAt(i) );

		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;

}

CString CConnectParamInfo::CreateLicenseCheck(int _iUserIdx, int _iLicKeyIdx)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("110741"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("usrIdx")] = new JSONValue((double)_iUserIdx);
	root2[_T("licKeyIdx")] = new JSONValue((double)_iLicKeyIdx);

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);
	delete value;

	return strReturn;
}

//서버로 백신 정보를 전달하기 위해
CString CConnectParamInfo::CreateNewVaccineInfo(NEW_VACCINE_INFO* _pInfo)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("133076"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	for( int i=0; i< _pInfo->arrPath.GetSize(); i++)
	{
		root2[_T("name")] =  new JSONValue(_pInfo->arrName.GetAt(i));
		root2[_T("path")] =  new JSONValue(_pInfo->arrPath.GetAt(i));
		array.push_back(new JSONValue(root2));
	}

	root[BODY]  = new JSONValue(array);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
}

CString CConnectParamInfo::CreateZombieCheck(CString _strLicenseKey)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("113009"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("licKey")] = new JSONValue(_strLicenseKey);
	

	root[BODY]  = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	//DBGLOG(strReturn);
	delete value;

	return strReturn;
}

//hhh(2013.08.12) : 서버로 일부 백신정보 요청
CString CConnectParamInfo::CreateRequsetVaccineInfo(CString _strVaccineName)
{
	JSONObject root, root2, root3;
	JSONArray array;

	CString strReturn = _T("");

	if( m_strSiteIndex.IsEmpty() )
		GetSiteIndex();

	root3[SESSION] = new JSONValue(_T("0"));
	root3[CRYPTO] = new JSONValue(_T("0"));
	root3[DEVICEID] = new JSONValue(_T(""));
	root3[TRID] =  new JSONValue(_T("133075"));
	root3[SITEIDX] = new JSONValue((double)_ttoi(m_strSiteIndex));
	root3[TXID] = new JSONValue(_T("1"));
	root3[VERSION] = new JSONValue(_T(""));

	root[HEADER] = new JSONValue(root3);

	root2[_T("name")] =  new JSONValue(_strVaccineName);

	root[BODY]  = new JSONValue(root2);
	
	JSONValue *value = new JSONValue(root);

	strReturn.Format(_T("%s"), value->Stringify().c_str());

	delete value;

	return strReturn;
}
