
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      Impersonator.cpp 
 @brief     CImpersonator class 구현 파일

            현재 프로세스의 권한을 변경한다.

 @author    hang ryul lee 
 @date      create 2007.11.30  
*/
#include "StdAfx.h"
#include "UtilsFile.h"
#include "Impersonator.h"
#include "WinOSVersion.h"
#include "Shellapi.h"
//#include <Userenv.h>
#include <yvals.h>


/********************************************************************************
 @class     CImpersonator
 @brief     현재 프로세스를 주어진 권한으로 Impersonate 한다.

  @note      Impersonate된 권한으로 프로세스를 생성할 수 있다.
 @note      m_bLoadProfile 멤버 변수\n
            XP이하의 NT계열에서 Winlogon.exe 프로세스 Token으로 \n
            LoadUserProfile 할경우 이벤트로그에 오류가 기록되어 FLoadProfile \n
            을 구분자로하여 LoadUserProfile 사용 또는 사용하지 않도록 구분함.
 @note      Windows NT 이하에서는 Impersonate를 지원하지 않지만 사용의 편의를 위해\n
            내부에서 알아서 처리한다. (class 사용 가능)
 @note      class를 선언만 했을 경우에는 run 등의 함수를 호출해도 fail이 떨어진다.\n
            CreateAsSystem / CreateAsLogonUser 둘중 하나의 생성 함수를 호출하여야\n
            class 내 함수의 기능이 제대로 동작한다.\n
            (Create 함수 호출시 해당 프로세스가 Impersonate 된다. 사용이 끝나면\n
            close 함수를 호출하여 원래 권한으로 되돌리도록 한다.)
 @note      Impersonate 되었는지 아닌지 확인이 힘들어, 기능 Test 로 대체\n
            - Windows 98 확인\n
            - Windows Me 확인\n
            - Windows NT 확인 (LogOnUser로 실행하여, LogOnUser로 프로세스 실행시 Run 실패 : ErrorCode = 1314)\n
                (System 권한 실행시, 프로세스 실행 잘됨)\n
            - Windows 2000확인(LogOnUser로 실행하여, LogOnUser로 프로세스 실행시 Run 실패 : ErrorCode = 1314)\n
                (System 권한 실행시, 프로세스 실행 잘됨)\n
            - Windows XP 확인\n
            - Windows 2003 확인\n
            - Windows Vista 확인\n
*********************************************************************************/

/**
 @brief     기본생성자
 @author    hang ryul lee
 @date      2007.11.30
 @note      멤벼 변수들을 초기화 한다.
*/
CImpersonator::CImpersonator()
{
    m_hToken = INVALID_HANDLE_VALUE;
    m_bProfileLoaded = false;
    m_bImpersonated  = false;
    m_dwSessionID   = 0;
    m_sUserName     = _T("");
    m_bLoadProfile  = true;
    ::ZeroMemory(&m_ProfileInfo, sizeof(PROFILEINFO));

    CWinOsVersion osVer;
    m_bIsWinNT = osVer.IsWinNT();
    m_bIs64bit = osVer.Is64bit();
}

/**
 @brief     소멸자
 @author    hang ryul lee
 @date      2007.11.30
 @note      UserProfile을 UnLoad한다.
 @note      Impersonate된 것을 원래 권한으로 되돌린다.
 @note      열린 핸들을 닫는다.
*/
CImpersonator::~CImpersonator()
{
    /*if (m_bProfileLoaded)
        UserProfileUnLoad();

    if (m_bImpersonated)
        ::RevertToSelf();

    if (INVALID_HANDLE_VALUE != m_hToken)
        ::CloseHandle(m_hToken);*/
    Free();
}

/**
 @brief     System 권한으로 Impersonate 진행
 @author    hang ryul lee
 @date      2007.12.03
 @param     [in] _SessionID :Impersonate 진행할 session id
 @note      해당 프로세스를 System 권한으로 Impersonate 한다.
 @note      주어진 세션의 winlogon.exe 프로세스의 토큰을 얻어\n
            시스템 권한으로 Impersonate 한다.
*/
bool CImpersonator::CreateAsSystem(const int _nSessionID)
{
    Free();

    m_dwSessionID = _nSessionID;
    m_bLoadProfile = false;

    bool bResult = false;

    if (m_bIsWinNT)
    {
        CString sWinlogon = _T("winlogon.exe");

        m_hToken = GetProcessToken(sWinlogon); 
        if (INVALID_HANDLE_VALUE != m_hToken)
            bResult = DoImpersonate();
    }
    else //win9x
    {
        m_sUserName = GetCurrentUserName();
        bResult = true;
    }

    return bResult;
}

/**
 @brief     로그온 유저 권한으로 Impersonate 진행
 @author    hang ryul lee
 @date      2007.12.03
 @param     [in] _SessionID :Impersonate 진행할 session id
 @note      해당 프로세스를 로그온 유저 권한으로 Impersonate 한다.
 @note      주어진 세션의 쉘 프로세스(explorer.exe) 의 토큰을 얻어\n
            현재 활성화 세션의 유저 권한으로 Impersonate 한다.
*/
bool CImpersonator::CreateAsLogonUser(const int _nSessionID)
{
    Free();

    m_dwSessionID = _nSessionID;

    bool bResult = false;

    if (m_bIsWinNT)
    {
        CString sShellName = GetShellName();

        m_hToken = GetProcessToken(sShellName);
        if (INVALID_HANDLE_VALUE != m_hToken)
            bResult = DoImpersonate();
    }
    else //win9x
    {
        m_sUserName = GetCurrentUserName();
        bResult = true;
    }

    return bResult;
}

/**
 @brief     열린 핸들등을 닫는다.
 @author    hang ryul lee
 @date      2007.12.03
 @note      userprofile이 로드되어 있는 경우 언로드 한다.
 @note      Impersonate되어 있을 경우 원래권한으로 되돌린다.
 @note      Token이 열려있는 경우 닫는다.
*/
void CImpersonator::Free()
{
    if (m_bProfileLoaded)
    {
        UserProfileUnLoad();
        m_bProfileLoaded = false;
    }

    if (m_bImpersonated)
    {
        ::RevertToSelf();
        m_bImpersonated = false;
    }

    if (INVALID_HANDLE_VALUE != m_hToken)
    {
        ::CloseHandle(m_hToken);
        m_hToken = INVALID_HANDLE_VALUE;
    }
}

/**
 @brief     쉘 이름을 구한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    shell name
 @note      레지스트리값을 참조하여 shell name 을 구한다.
*/
CString CImpersonator::GetShellName() const
{
    const CString REG_PATH  = _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon");
    const CString SHELL_DEfAULT = _T("explorer.exe");
    const DWORD   nMAX_VALUE_LEN = 1024;

    CString sResult = _T("");

    if (!m_bIsWinNT) return sResult;

    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, REG_PATH, 0, KEY_READ, &hKey))
    {
        //dwLen = lpcbData 
        //Pointer to a variable that specifies the size of the buffer pointed to by the lpData parameter, in bytes.
        DWORD dwLen = (nMAX_VALUE_LEN +1) * sizeof(TCHAR);
        TCHAR szValue[nMAX_VALUE_LEN+1] = {0,};
        if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, _T("Shell"), NULL, NULL, (LPBYTE)&szValue, &dwLen))
            sResult = szValue;

        ::RegCloseKey(hKey);
    }

    if (0 != sResult.CompareNoCase(SHELL_DEfAULT))
    {
        CString sName = sResult;
        CString sDefault = SHELL_DEfAULT;
        sName.MakeUpper();
        sDefault.MakeUpper();

        if ((_T("") == sResult) || (-1 < sName.Find(SHELL_DEfAULT)))
            sResult = SHELL_DEfAULT;
    }

    return sResult;
}

/**
 @brief     프로세스가 실행된 세션 ID를 구한다.
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _dwProcessID   :세션을 구할 프로세스 ID
 @return    session id
 @note      주어진 process id 를 이용하여 session id를 구한다.
*/
DWORD CImpersonator::GetProcessSessionId(const DWORD &_dwProcessID) const
{
    typedef BOOL (WINAPI* fpProcessIdToSessionId)(DWORD, DWORD*);
    fpProcessIdToSessionId pProcessIdToSessionId = NULL;  
    DWORD dwResult = 0;

    HMODULE hModule = ::GetModuleHandle(_T("kernel32"));
    if (NULL == hModule)
        return 0;

    pProcessIdToSessionId = (fpProcessIdToSessionId)GetProcAddress(hModule, "ProcessIdToSessionId");
    if (NULL == pProcessIdToSessionId) return dwResult;
    if (0 == pProcessIdToSessionId(_dwProcessID, &dwResult)) return dwResult;

    return dwResult;     
}

/**
 @brief     실행중인 프로세스의 토큰을 구한다.
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _sProcessName  :토큰을 구할 프로세스 id
 @return    프로세스 토큰
 @note      Wow64 / NT를 구분하여 토큰을 구한다.
*/
HANDLE CImpersonator::GetProcessToken(const CString &_sProcessName)
{
    HANDLE hResult = INVALID_HANDLE_VALUE;

    if ((_T("") == _sProcessName) || (0 > m_dwSessionID)) return hResult;

    if (!m_bIsWinNT) return hResult;

    if (m_bIs64bit) 
        hResult = Get64ProcessToken(_sProcessName);
    else
        hResult = GetNTProcessToken(_sProcessName);

    return hResult;
}

/**
 @brief     WinNT 플랫폼의 프로세스 토큰을 구한다.
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _sProcessName  :토큰을 얻을 프로세스 id
 @return    프로세스 토큰
 @note      프로세스 이름을 이용하여 프로세스 id를 구한다.
 @note      프로세스 id를 이용하여 프로세스 핸들 및 프로세스 토큰을 구한다.
 @note      OpenProcessToken - PROCESS_ALL_ACCESS 로 토큰을 구할 경우 발생 문제\n
            : Win2000 이상 Os를 위해 TOKEN_ADJUST_SESSIONID 권한이 추가 되었는데,\n
              WinNT의 경우 해당 값이 없어서 PROCESS_ALL_ACCESS로 토큰을 구할 경우 \n
              ERROR_ACCESS_DENIED 오류 발생.\n
              (참고: http://support.microsoft.com/kb/325291 \n
                     http://support.microsoft.com/kb/225091/EN-US/ )
*/
HANDLE CImpersonator::GetNTProcessToken(const CString &_sProcessName)
{
    HANDLE hResult = INVALID_HANDLE_VALUE;

    typedef BOOL (WINAPI *fpEnumProcesses)(DWORD*, DWORD, DWORD*);
    typedef DWORD (WINAPI *fpGetModuleBaseName)(HANDLE, HMODULE, LPTSTR, DWORD);

    fpEnumProcesses   pEnumProcesses = NULL;
    fpGetModuleBaseName  pGetModuleBaseName = NULL;

    HINSTANCE hModule = ::LoadLibrary(_T("Psapi.dll"));
    if (NULL == hModule) return hResult;

    pEnumProcesses = (fpEnumProcesses) ::GetProcAddress(hModule, "EnumProcesses");
    pGetModuleBaseName = (fpGetModuleBaseName) ::GetProcAddress(hModule, GETMODULEBASENAME);
    if ((NULL == pEnumProcesses) || (NULL == pGetModuleBaseName))
    {
        ::FreeLibrary(hModule);
        return hResult;
    }
 
    CString sExeName = ExtractFileName(_sProcessName);

    DWORD dwProcessIds[2048] = { 0, };
    DWORD dwReturnByte = 0;
    pEnumProcesses(dwProcessIds, sizeof(dwProcessIds), &dwReturnByte);

    DWORD dwCurrentProcessID = ::GetCurrentProcessId();
    DWORD dwCount = dwReturnByte / sizeof(DWORD);

    for (DWORD i=0; i<dwCount; i++)
    {
        if (dwCurrentProcessID == dwProcessIds[i])  continue;

        //HANDLE hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, dwProcessIds[i]);
        HANDLE hProcess = ::OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcessIds[i]);

        if (NULL == hProcess) continue;

        TCHAR szName[MAX_PATH] = {0,};
        if (0 == pGetModuleBaseName(hProcess, 0, szName, MAX_PATH)) 
        {
            ::CloseHandle(hProcess);
            continue;
        }

        CString sBaseName = szName;
        if (0 == sBaseName.CompareNoCase(sExeName))
        {
            HANDLE hToken = INVALID_HANDLE_VALUE;

            //if (0 != ::OpenProcessToken(hProcess, TOKEN_ALL_ACCESS, &hToken))
            if (0!= ::OpenProcessToken(hProcess, TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE | TOKEN_READ, &hToken))
            { 
                DWORD dwSessionId = GetProcessSessionId(dwProcessIds[i]);
                ::CloseHandle(hProcess);

                if (m_dwSessionID != dwSessionId) 
                    continue;

                hResult = hToken;      
                break;
            }
        }
        ::CloseHandle(hProcess);
    }
    if (NULL != hModule)
        ::FreeLibrary(hModule);

    return hResult;
}

/**
 @brief     pid로 부터 모듈이 위치한 경로를 가져온다.
 @author    heo hyo haeng	
 @date      2013.07.24
 @param     [in] _dwPid : 경로를 가져올  pid
 @return    프로세스 전체경로
 @note      프로세스 EnumprocessMudule을 이용하여 인자로 들어온 pid의 경로를 리턴한다.
*/
CString CImpersonator::GetProcessFullPath(DWORD _dwPid)
{

	CString strRetPath =L"";
	typedef BOOL (WINAPI *fpEnumProcessModules)(HANDLE, HMODULE*, DWORD, LPDWORD);
    typedef DWORD (WINAPI *fpGetModuleFileNameEx)(HANDLE, HMODULE, LPTSTR, DWORD);

	fpEnumProcessModules   pEnumProcessModules = NULL;
    fpGetModuleFileNameEx  pGetModuleFileNameEx = NULL;

	 HINSTANCE hModule = ::LoadLibrary(_T("Psapi.dll"));
    if (NULL == hModule) return L"";

    pEnumProcessModules = (fpEnumProcessModules) ::GetProcAddress(hModule, "EnumProcessModules");
    pGetModuleFileNameEx = (fpGetModuleFileNameEx) ::GetProcAddress(hModule, "GetModuleFileNameExW");
    if ((NULL == pEnumProcessModules) || (NULL == pGetModuleFileNameEx))
    {
        ::FreeLibrary(hModule);
        return L"";
    }

	HANDLE hdle = ::OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, _dwPid);
	if(hdle)
	{
		HMODULE hmod;
		TCHAR szFilePath[MAX_PATH]={0,};
		DWORD dwSize = 0;
		if(pEnumProcessModules(hdle, &hmod, sizeof(hmod), &dwSize))
		{
			//프로세의 절대 경로를 구한다.
			pGetModuleFileNameEx(hdle, hmod, szFilePath, _countof(szFilePath));
			strRetPath.Format(L"%s", szFilePath);
		}


	}

	 ::FreeLibrary(hModule);
	 return strRetPath;

}

/**
 @brief     wow64 환경에서 주어진 프로세스의 토큰을 찾는다
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _sProcessName  :토큰을 얻을 프로세스 id
 @return    프로세스 토큰
 @note      스냅샷을 이용하여 프로세스 리스트를 얻는다.
 @note      프로세스 id를 이용하여 프로세스 핸들 및 프로세스 토큰을 구한다.
*/
HANDLE CImpersonator::Get64ProcessToken(const CString &_sProcessName)
{
    HANDLE hResult = INVALID_HANDLE_VALUE;

    CString sExeName = ExtractFileName(_sProcessName);

    typedef HANDLE (WINAPI *fpCreateToolhelp32Snapshot) (DWORD, DWORD);
    typedef BOOL (WINAPI *fpProcess32First) (HANDLE, LPPROCESSENTRY32);
    typedef BOOL (WINAPI *fpProcess32Next) (HANDLE, LPPROCESSENTRY32);

    fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
    fpProcess32First   pProcess32First = NULL;
    fpProcess32Next    pProcess32Next = NULL;

    HMODULE hModule = ::GetModuleHandle(_T("kernel32"));
    if (NULL == hModule)
	{
        return hResult;
	}

    pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(hModule, "CreateToolhelp32Snapshot");
    pProcess32First = (fpProcess32First) ::GetProcAddress(hModule, PROCESS32FIRST);
    pProcess32Next  = (fpProcess32Next) ::GetProcAddress(hModule, PROCESS32NEXT);

    if ((NULL == pCreateToolhelp32Snapshot) || (NULL == pProcess32First) ||
                (NULL == pProcess32Next))
	{
        return hResult;
	}

    HANDLE hSnap = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (INVALID_HANDLE_VALUE == hSnap)
	{
		return hResult;
	}

    PROCESSENTRY32 pcEntry;
    pcEntry.dwSize = sizeof(PROCESSENTRY32);

    if (!pProcess32First(hSnap, &pcEntry))
    {
        ::CloseHandle(hSnap);
        return hResult;
    }
  
    do
    {
        if (::GetCurrentProcessId() == pcEntry.th32ParentProcessID)
			continue;

        CString sExeFile = _T("");
        sExeFile = pcEntry.szExeFile;

        if (0 == sExeName.CompareNoCase(ExtractFileName(sExeFile)))
        {
            HANDLE hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, pcEntry.th32ProcessID);
            if (INVALID_HANDLE_VALUE == hProcess) 
			{
				continue;
			}

            HANDLE hToken = INVALID_HANDLE_VALUE;
            if (0 != ::OpenProcessToken(hProcess, TOKEN_ALL_ACCESS, &hToken))
            {
                DWORD dwSessionId = GetProcessSessionId(pcEntry.th32ProcessID);
                if (m_dwSessionID != dwSessionId)
				{
					continue;
				}

                hResult = hToken;
                ::CloseHandle(hProcess);
                break;
            }
            ::CloseHandle(hProcess);
        }
    }while (pProcess32Next(hSnap, &pcEntry));

    ::CloseHandle(hSnap);
    return hResult;
}

/**
@brief     백신 모듈 실행 여부
@author   jhlee
@date      2012.07.10
@param     [in] _sProcessName  :토큰을 얻을 프로세스명
@return    존재유무
@note      스냅샷을 이용하여 프로세스 리스트를 얻는다.
@note      프로세명으로 프로세스 리스트에 존재하는지 아닌지 확인한다.
*/
BOOL CImpersonator::GetProcessExist(const CString &_sProcessName)
{
    HANDLE hResult = INVALID_HANDLE_VALUE;

    CString sExeName = ExtractFileName(_sProcessName);

    typedef HANDLE (WINAPI *fpCreateToolhelp32Snapshot) (DWORD, DWORD);
    typedef BOOL (WINAPI *fpProcess32First) (HANDLE, LPPROCESSENTRY32);
    typedef BOOL (WINAPI *fpProcess32Next) (HANDLE, LPPROCESSENTRY32);

    fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
    fpProcess32First   pProcess32First = NULL;
    fpProcess32Next    pProcess32Next = NULL;

    HMODULE hModule = ::GetModuleHandle(_T("kernel32"));
    if (NULL == hModule)
	{
	//	UM_WRITE_LOG(_T("GetProcessExist - GetModuleHandle Error(_sProcessName)")+ sExeName);
        return FALSE;
	}

    pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(hModule, "CreateToolhelp32Snapshot");
    pProcess32First = (fpProcess32First) ::GetProcAddress(hModule, PROCESS32FIRST);
    pProcess32Next  = (fpProcess32Next) ::GetProcAddress(hModule, PROCESS32NEXT);

    if ((NULL == pCreateToolhelp32Snapshot) || (NULL == pProcess32First) ||
                (NULL == pProcess32Next))
	{
	//	UM_WRITE_LOG(_T("GetProcessExist - pCreateToolhelp32Snapshot is NULL or pProcess32Next is null"));
        return FALSE;
	}

    HANDLE hSnap = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (INVALID_HANDLE_VALUE == hSnap)	// CreateToolhelp32Snapshot 실패시 INVALID_HANDLE_VALUE 리턴
	{
	//	UM_WRITE_LOG(_T("GetProcessExist - Error 1"));
		return FALSE;
	}

    PROCESSENTRY32 pcEntry;
    pcEntry.dwSize = sizeof(PROCESSENTRY32);

    if (!pProcess32First(hSnap, &pcEntry))
    {
        ::CloseHandle(hSnap);
	//	UM_WRITE_LOG(_T("GetProcessExist - Error 2"));
        return FALSE;
    }
  
    do
    {
//        if (::GetCurrentProcessId() == pcEntry.th32ParentProcessID) //2013.1.21 ->삭제
//			continue;

        CString sExeFile = _T("");
        sExeFile = pcEntry.szExeFile;
	//	UM_WRITE_LOG(_T("sExeFile :")+ sExeFile+_T("sExeName:")+ sExeName);
		if (0 == sExeName.CompareNoCase(ExtractFileName(sExeFile)))
		{
			
			HANDLE hProcess = ::OpenProcess(PROCESS_ALL_ACCESS, FALSE, pcEntry.th32ProcessID);
			if (INVALID_HANDLE_VALUE == hProcess || NULL == hProcess) // 2017-05-11 kh.choi NULL 체크 추가. OpreProcess 실패시 NULL 리턴
			{
			//	UM_WRITE_LOG(_T("GetProcessExist - Error 3"));
				//::CloseHandle(hProcess);	// 2017-05-11 kh.choi 주석처리
				::CloseHandle(hSnap);	// 2017-05-11 kh.choi 핸들릭 수정
				return FALSE;
			}
			::CloseHandle(hProcess);
			::CloseHandle(hSnap);	// 2017-05-11 kh.choi 핸들릭 수정
			return TRUE;

		}
    }while (pProcess32Next(hSnap, &pcEntry));

//	UM_WRITE_LOG(_T("GetProcessExist - Error 4"));
    ::CloseHandle(hSnap);
    return FALSE;
}
/**
 @brief     impersonate 를 한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    true / false
 @note      구해진 토큰으로 사용자 이름을 구해 저장한다.
 @note      Current User 레지스트리 키 오픈을 위해 UserProfile을 로드한다.
 @note      m_bLoadProfile 멤버 변수\n
            XP이하의 NT계열에서 Winlogon.exe 프로세스 Token으로 \n
            LoadUserProfile 할경우 이벤트로그에 오류가 기록되어 FLoadProfile \n
            을 구분자로하여 LoadUserProfile 사용 또는 사용하지 않도록 구분함.
*/
bool CImpersonator::DoImpersonate()
{
    m_sUserName = GetUserNameFromToken(m_hToken);

    if (m_bLoadProfile)
    m_bProfileLoaded = UserProfileLoad();

    if (0 != ::ImpersonateLoggedOnUser(m_hToken))
    m_bImpersonated = true;

    return m_bImpersonated;
}

/**
 @brief     토큰을 이용하여 사용자 이름을 구한다.
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _hToken    :사용자 이름을 구하고자 하는 토큰
 @return    User name 
 @note      토큰을 이용하여 user의 sid를 구한다.
 @note      구해진 sid 를 이용하여 user name을 구한다.
*/
CString CImpersonator::GetUserNameFromToken(const HANDLE &_hToken) const
{
    CString sResult = _T("");

    DWORD dwSize = 0;
    BOOL re = ::GetTokenInformation(_hToken, TokenUser, NULL, 0, &dwSize);

    PTOKEN_USER pTokenUser = NULL;
    if (!re && (ERROR_INSUFFICIENT_BUFFER == ::GetLastError()))
    {
        pTokenUser = static_cast <PTOKEN_USER>(::malloc(dwSize));
        re = ::GetTokenInformation(_hToken, TokenUser, pTokenUser, dwSize, &dwSize);
    }

    if (!re || (NULL == pTokenUser)) 
    {
        ::free(pTokenUser);
        return sResult;
    }

    DWORD dwNameSize = 0, dwDomainSize = 0;
    LPTSTR szName= NULL, szDomain = NULL;
    SID_NAME_USE SidNameUse = SidTypeUser;

    ::LookupAccountSid(NULL, pTokenUser->User.Sid, NULL, &dwNameSize, NULL, &dwDomainSize, &SidNameUse);
    if ((0 == dwNameSize) || (0 == dwDomainSize)) 
    {
        ::free(pTokenUser);
        return sResult;
    }

    /*szName = new TCHAR[(dwNameSize +1) * sizeof(TCHAR)];
    szDomain = new TCHAR[(dwDomainSize +1) * sizeof(TCHAR)];*/

    //dwNameSize = cchName 
    //cchName receives the required buffer size(in TCHARs) including the terminating null character. 
    try
    {
        szName = new TCHAR[dwNameSize];
        szDomain = new TCHAR[dwDomainSize];

        if (0 != ::LookupAccountSid(NULL, pTokenUser->User.Sid, szName, &dwNameSize, szDomain, &dwDomainSize, &SidNameUse))
            sResult = szName;
    }
    catch (...)
    {
        //
    }

    if (NULL != szName)  delete [] szName;
    if (NULL != szDomain) delete [] szDomain;
    ::free(pTokenUser);

    return sResult;
}

/**
 @brief     user profile을 load 한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    true / false
 @note      LoadUserProfile 호출을 통해 사용자의 HKCU 레지스트리 키가 오픈된다.
 @note      후에 해당 사용자의 HKCU 레지스트리에 접근할 때에는 \n
            HKEY_CURRENT_USER key 핸들 대신에 오픈한 레지스트리 키 핸들을 사용한다.
*/
bool CImpersonator::UserProfileLoad()
{
    bool bResult = false;

    typedef enum PI_{PI_NOUI = 1, PI_APPLYPOLICY}PI_;

    typedef BOOL (WINAPI *fpLoadUserProfile)(HANDLE, LPPROFILEINFO);
    fpLoadUserProfile pLoadUserProfile = NULL;

    HINSTANCE hModule = ::LoadLibrary(_T("Userenv.dll"));
    if (NULL == hModule) return bResult;

    pLoadUserProfile = (fpLoadUserProfile) ::GetProcAddress(hModule, LOADUSERPROFILE);
    if (NULL == pLoadUserProfile)
    {
        ::FreeLibrary(hModule);
        return bResult;
    }

    ::ZeroMemory(&m_ProfileInfo, sizeof(m_ProfileInfo));
    m_ProfileInfo.dwSize = sizeof(PROFILEINFO);
    m_ProfileInfo.lpUserName = m_sUserName.GetBuffer();
    m_ProfileInfo.dwFlags = PI_NOUI;

    if (0 != pLoadUserProfile(m_hToken, &m_ProfileInfo))
        bResult = true;

    m_sUserName.ReleaseBuffer();

    if (NULL != hModule)
        ::FreeLibrary(hModule);

    return bResult;
}

/**
 @brief     UserProfile 을 unload한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    true / false
 @note      load를 통해 오픈된 HKCU 레지스트리 핸들이 close 된다.
*/
bool CImpersonator::UserProfileUnLoad()
{
    bool bResult = false;

    if (!m_bProfileLoaded) return bResult;

    typedef BOOL (WINAPI *fpUnloadUserProfile)(HANDLE, HANDLE);
    fpUnloadUserProfile pUnloadUserProfile = NULL;

    HINSTANCE hModule = ::LoadLibrary(_T("Userenv.dll"));
    if (NULL == hModule) return bResult;

    pUnloadUserProfile = (fpUnloadUserProfile) ::GetProcAddress(hModule, "UnloadUserProfile");
    if (NULL == pUnloadUserProfile)
    {
        ::FreeLibrary(hModule);
        return bResult;
    }

    if (0 != pUnloadUserProfile(m_hToken, m_ProfileInfo.hProfile))
    {
        bResult = true;
        m_bProfileLoaded = false;
    }

    if (NULL != hModule)
        ::FreeLibrary(hModule);

    return bResult;
}

/**
 @brief     해당 사용자의 HKCU 키 핸들을 반환한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    HKCU key handle
 @note      NT 이상일 경우 load userprofile을 통해 오픈된 key handle을 반환한다.
 @note      Profile을 load하지 않은 경우 HKEY_CURRENT_USER  값 반환.
*/
HKEY CImpersonator::GetHKCUKey()
{
    HKEY hKey = 0;

    if (m_bIsWinNT)
    {
        if (m_bProfileLoaded)
            hKey = static_cast <HKEY> (m_ProfileInfo.hProfile);
        else
            hKey = HKEY_CURRENT_USER;
    }
    else
        hKey = HKEY_CURRENT_USER;

    return hKey;
}

/**
 @brief     현재 사용자의 이름을 구한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    user name
 @note      현재 실행중인 사용자의 이름을 구한다.
*/
CString CImpersonator::GetCurrentUserName() const
{
    CString sResult = _T("");
    DWORD dwMAXLen = UNLEN; 
    LPTSTR szName = new TCHAR[dwMAXLen * sizeof(TCHAR)];
    ::ZeroMemory(szName, dwMAXLen * sizeof(TCHAR));

    ::GetUserName(szName, &dwMAXLen);
    sResult = szName;

    delete [] szName;

    return sResult;
}

/**
 @brief     구해진 토큰 권한으로 프로세스를 실행시킨다.
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _sExeName  :실행파일 경로
 @param     [in] _sParam    :실행 파라미터
 @param     [in] _swShowOpt :실행 옵션
 @return    true / false
 @note      프로세스 실행 후 바로 종료된다.
*/
bool CImpersonator::Run(const CString &_sExeName, const CString &_sParam/*= _T("")*/, const WORD &_wShowOpt/*= SW_SHOW*/)
{
    bool bResult = false;

    CString sExeName = _sExeName;
    CString sParam = _sParam;
    sExeName = sExeName.Trim();
    sParam = sParam.Trim();
    if (_T("") == sExeName)  return false;

    typedef BOOL (WINAPI *fpCreateEnvironmentBlock) (LPVOID*, HANDLE, BOOL);
    typedef BOOL (WINAPI *fpDestroyEnvironmentBlock) (LPVOID);

    fpCreateEnvironmentBlock pCreateEnvironmentBlock = NULL;
    fpDestroyEnvironmentBlock pDestroyEnvironmentBlock = NULL;	

    STARTUPINFO startup;
    PROCESS_INFORMATION pcInfo;

    ::ZeroMemory(&startup, sizeof(startup));
    ::ZeroMemory(&pcInfo, sizeof(pcInfo));

    startup.cb = sizeof(startup);
    startup.wShowWindow = _wShowOpt;
    startup.dwFlags = STARTF_USESHOWWINDOW | STARTF_USESTDHANDLES;
    startup.lpDesktop = _T("WinSta0\\Default");

    LPTSTR pszCmd = NULL;
    CString sCmd=_T(""), sExe=_T(""), sCurDir=_T("");

    if (_T("") != _sParam)
        sCmd = _T("\"")+ sExeName + _T("\" ") + sParam;

    sCurDir = ExtractFilePath(sExeName);

    LPVOID lpEnv = NULL;
    bool bCreateEnv = false;
    pszCmd = sCmd.GetBuffer();

    HINSTANCE hModule = ::LoadLibrary(_T("Userenv.dll"));

    if (m_bIsWinNT)
    {
        if (NULL != hModule)
            pCreateEnvironmentBlock = (fpCreateEnvironmentBlock) ::GetProcAddress(hModule, "CreateEnvironmentBlock");

        if ((NULL != pCreateEnvironmentBlock))
        {
            if (0 != pCreateEnvironmentBlock(&lpEnv, m_hToken, FALSE))
                bCreateEnv = true;
        }

        if (bCreateEnv)
        { 
            if ( 0 != ::CreateProcessAsUser(m_hToken, sExeName, pszCmd, NULL, NULL, FALSE, 
                            (CREATE_DEFAULT_ERROR_MODE | CREATE_UNICODE_ENVIRONMENT), lpEnv, sCurDir, &startup, &pcInfo))
            bResult = true;
        }
        else
        {
            if ( 0 != ::CreateProcessAsUser(m_hToken, sExeName, pszCmd, NULL, NULL, FALSE, 
                            CREATE_DEFAULT_ERROR_MODE, NULL, sCurDir, &startup, &pcInfo))
            bResult = true;
        }
    }
    else
    {
        if (0 != ::CreateProcess(sExeName, pszCmd, NULL, NULL, FALSE, 
                        CREATE_DEFAULT_ERROR_MODE, NULL, sCurDir, &startup, &pcInfo))
        bResult = true;
    }
    sCmd.ReleaseBuffer();

    if (bResult)
    {
        ::WaitForSingleObject(pcInfo.hProcess, 0);
        CloseHandle(pcInfo.hProcess);
        CloseHandle(pcInfo.hThread);
    } 

    if (bCreateEnv)
    {
        if (NULL != hModule)
            pDestroyEnvironmentBlock = (fpDestroyEnvironmentBlock) ::GetProcAddress(hModule, "DestroyEnvironmentBlock");
           
        if ((NULL != pDestroyEnvironmentBlock))
            pDestroyEnvironmentBlock(lpEnv);
    }

    if (NULL != hModule)
        ::FreeLibrary(hModule);

    return bResult;
}

/**
 @brief     구해진 토큰 권한으로 프로세스를 실행시킨다.(종료시 까지 대기)
 @author    hang ryul lee
 @date      2007.11.30
 @param     [in] _sExeName  :실행파일 경로
 @param     [in] _sParam    :실행 파라미터
 @param     [in] _swShowOpt :실행 옵션 
 @param     [out] _pExitCode:프로세스 종료코드 구함(default =NULL)
 @return    true / false
 @note      프로세스 실행 시키고 해당 프로세스가 종료될때까지 기다린다.
*/
bool CImpersonator::RunAndWait(const CString &_sExeName, const CString &_sParam/*= _T("")*/, const WORD &_wShowOpt/*= SW_SHOW*/, DWORD *_pExitCode /*= NULL*/)
{
    bool bResult = false;

    CString sExeName = _sExeName;
    CString sParam = _sParam;
    sExeName = sExeName.Trim();
    sParam = sParam.Trim();
    if (_T("") == sExeName)  return false;

    typedef BOOL (WINAPI *fpCreateEnvironmentBlock) (LPVOID*, HANDLE, BOOL);
    typedef BOOL (WINAPI *fpDestroyEnvironmentBlock) (LPVOID);

    fpCreateEnvironmentBlock pCreateEnvironmentBlock = NULL;
    fpDestroyEnvironmentBlock pDestroyEnvironmentBlock = NULL;

    STARTUPINFO startup;
    PROCESS_INFORMATION pcInfo;

    ::ZeroMemory(&startup, sizeof(startup));
    ::ZeroMemory(&pcInfo, sizeof(pcInfo));

    startup.cb = sizeof(startup);
    startup.wShowWindow = _wShowOpt;
    startup.lpDesktop = _T("WinSta0\\Default");

    LPTSTR pszCmd = NULL;
    CString sCmd=_T(""), sExe=_T(""), sCurDir=_T("");

    if (_T("") != _sParam)
    sCmd = _T("\"")+ sExeName + _T("\" ") + sParam;

    sCurDir = ExtractFilePath(sExeName);

    LPVOID lpEnv = NULL;
    bool bCreateEnv = false;
    pszCmd = sCmd.GetBuffer();

    HINSTANCE hModule = ::LoadLibrary(_T("Userenv.dll"));

    if (m_bIsWinNT)
    {
        if (NULL != hModule)
            pCreateEnvironmentBlock = (fpCreateEnvironmentBlock) ::GetProcAddress(hModule, "CreateEnvironmentBlock");

        if ((NULL != pCreateEnvironmentBlock))
        {
            if (0 != pCreateEnvironmentBlock(&lpEnv, m_hToken, FALSE))
            bCreateEnv = true;
        }

        if (bCreateEnv)
        { 
            if ( 0 != ::CreateProcessAsUser(m_hToken, sExeName, pszCmd, NULL, NULL, FALSE, 
                            (CREATE_DEFAULT_ERROR_MODE | CREATE_UNICODE_ENVIRONMENT), lpEnv, sCurDir, &startup, &pcInfo))
            bResult = true;
        }
        else
        {
            if ( 0 != ::CreateProcessAsUser(m_hToken, sExeName, pszCmd, NULL, NULL, FALSE, 
                            CREATE_DEFAULT_ERROR_MODE, NULL, sCurDir, &startup, &pcInfo))
            bResult = true;
        }
    }
    else
    {
        if (0 != ::CreateProcess(sExeName, pszCmd, NULL, NULL, FALSE, 
                        CREATE_DEFAULT_ERROR_MODE, NULL, sCurDir, &startup, &pcInfo))
        bResult = true;
    }
    sCmd.ReleaseBuffer();

    if (bResult)
    {
        ::WaitForSingleObject(pcInfo.hProcess, INFINITE);
        if (NULL != _pExitCode)
            ::GetExitCodeProcess(pcInfo.hProcess, _pExitCode);

        CloseHandle(pcInfo.hProcess);
        CloseHandle(pcInfo.hThread);
    }

    if (bCreateEnv)
    {
        if (NULL != hModule)
            pDestroyEnvironmentBlock = (fpDestroyEnvironmentBlock) ::GetProcAddress(hModule, "DestroyEnvironmentBlock");
           
        if ((NULL != pDestroyEnvironmentBlock))
            pDestroyEnvironmentBlock(lpEnv);
    }

    if (NULL != hModule)
        ::FreeLibrary(hModule);

    return bResult;
}

/**
 @brief     Impersonate 한 유저의 이름을 반환
 @author    hang ryul lee
 @date      2007.11.30
 @return    UserName
*/
CString CImpersonator::GetImpersonatedUserName() const
{
    return m_sUserName;
}

/**
 @brief     Impersonate 한 세션을 반환한다.
 @author    hang ryul lee
 @date      2007.11.30
 @return    session id
*/
DWORD CImpersonator::GetSessionID() const
{
    return m_dwSessionID;
}


/**
 @brief      선택한 권한으로 모듈 실행 
 @author    JHLEE
 @date      2011.06.08
 @return    bool(true / false)
 @param    [in]pFilePath - 프로세스 경로
 @param    [in]bSessionID - 현재 프로세스의 세션을 얻을지의 flag,
 @param   [in]dwILevel - 실행권한
*/
BOOL CImpersonator::ChangeCreateProcess(TCHAR *pFilePath, BOOL bSessionID,DWORD dwILevel, DWORD dwWait )
{

	HANDLE hProToken = NULL;
	HANDLE hChangeToken = NULL;
	TCHAR szTokenIntegrityLevel[30]={0,};
	TOKEN_MANDATORY_LABEL TML = {0};
	PROCESS_INFORMATION ProcInfo = {0};
	STARTUPINFO StartupInfo = {0};
	PSID pIntegritySid = NULL;
	DWORD dwSessionID = 0;
	BOOL bRet;

	if( bSessionID == FALSE )
	{
		dwSessionID = 0;
	}
	else 
	{
		DWORD	(__stdcall *pActiveSessionId)();

		HINSTANCE hInst;
		hInst = ::LoadLibrary( _T("Kernel32.dll") );

		if(hInst != NULL)
		{
			pActiveSessionId = (DWORD(__stdcall *)())::GetProcAddress(hInst, "WTSGetActiveConsoleSessionId");
			dwSessionID = pActiveSessionId();
		}

		if(hInst != NULL)
			::FreeLibrary(hInst);

	}
	
	if( dwILevel == 0 )		// system
	{
		_tcscpy_s(szTokenIntegrityLevel, _T("S-1-16-16384"));
	}
	else if( dwILevel ==1 )	// low
	{
		_tcscpy_s(szTokenIntegrityLevel, _T("S-1-16-4096"));		
	}
	else if( dwILevel == 2 )	// medium
	{
		_tcscpy_s(szTokenIntegrityLevel, _T("S-1-16-8192"));		
	}
	else if( dwILevel == 3 )	// high
	{
		_tcscpy_s(szTokenIntegrityLevel, _T("S-1-16-12288"));		
	}

	if( ::OpenProcessToken(GetCurrentProcess(), MAXIMUM_ALLOWED, &hProToken))
	{
		HINSTANCE hInst = NULL;
		hInst = ::LoadLibrary(_T("Advapi32.dll"));

		if( hInst != NULL )
		{
			typedef BOOL (WINAPI *fpConvertStringSidToSid)(LPCTSTR, PSID*);

			fpConvertStringSidToSid pConvertStringSidToSid = NULL;

#ifdef _UNICODE
			pConvertStringSidToSid = (fpConvertStringSidToSid)::GetProcAddress(hInst, "ConvertStringSidToSidW");
#else
			pConvertStringSidToSid = (fpConvertStringSidToSid)::GetProcAddress(hInst, "ConvertStringSidToSidA");
#endif
			if( pConvertStringSidToSid != NULL )
			{
				if( pConvertStringSidToSid(szTokenIntegrityLevel, &pIntegritySid))
				{
					TML.Label.Attributes = SE_GROUP_INTEGRITY;
					TML.Label.Sid = pIntegritySid;

					DuplicateTokenEx(hProToken, MAXIMUM_ALLOWED, NULL, SecurityImpersonation, TokenPrimary, &hChangeToken);

					//세션 권한 변경
					if( SetTokenInformation(hChangeToken, TokenSessionId, &dwSessionID, sizeof(DWORD)) )
					{
						// SACL 권한 변경
						if( SetTokenInformation(hChangeToken,(TOKEN_INFORMATION_CLASS)25, &TML, sizeof(TOKEN_MANDATORY_LABEL) + GetLengthSid(pIntegritySid)) )
						{
							TCHAR szDir[MAX_PATH]={0};
							_tcscpy_s(szDir, pFilePath);

							memset(&StartupInfo,0,sizeof(StartupInfo));
							memset(&ProcInfo,0,sizeof(ProcInfo));

							bRet = CreateProcessAsUser(hChangeToken, NULL, szDir, NULL, NULL, FALSE,0, NULL, NULL, &StartupInfo, &ProcInfo);

							if( bRet )
							{
								WaitForInputIdle(ProcInfo.hProcess,INFINITE);
								if( dwWait == 1 )
								{
									WaitForSingleObject( ProcInfo.hProcess, INFINITE );
								}

								LocalFree(pIntegritySid);						
								CloseHandle(hChangeToken);						
								CloseHandle(hProToken);						
								CloseHandle(ProcInfo.hProcess);						
								CloseHandle(ProcInfo.hThread);

								FreeLibrary(hInst);

								return bRet;						
							}
						}

						LocalFree(pIntegritySid);
					}

					CloseHandle(hChangeToken);
				}
			}
		}
		CloseHandle(hProToken);
		FreeLibrary(hInst);
	}

	return FALSE;
}

/**
 @brief      모듈 실행
 @author    JHLEE
 @date      2011.06.13
 @return    bool(true / false)
 @param    [in]strCommand - 프로세스 경로
 @param    [in]iShowMode -  실행 옵션
*/
BOOL CImpersonator::CreateProcessEx(CString strCommand, USHORT iShowMode,CString _strDeskTop)
{
		STARTUPINFO			si;
		PROCESS_INFORMATION pi;

		ZeroMemory(&si, sizeof(si));
		ZeroMemory(&pi, sizeof(pi));

		si.cb			= sizeof(si);

		if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;

		si.wShowWindow	= iShowMode;
		if( !_strDeskTop.IsEmpty() )
		{
			si.lpDesktop = (LPTSTR)(LPCTSTR)_strDeskTop;
		}
		CString strMsg;
		// Start the child process. 
		BOOL bResult = ::CreateProcess(/*NULL*/strCommand,				// No module name (use command line). 
			/*(LPTSTR)(LPCTSTR)strCommand*/NULL,		// Command line. 
			NULL,             // Process handle not inheritable. 
			NULL,             // Thread handle not inheritable. 
			FALSE,            // Set handle inheritance to FAL
			NORMAL_PRIORITY_CLASS,
			NULL,             // Use parent's environment block. 
			NULL,             // Use parent's starting directory. 
			&si,              // Pointer to STARTUPINFO structure.
			&pi);             // Pointer to PROCESS_INFORMATION structure.

		if (!bResult)			return FALSE;
		

		// Wait until child process exits

		DWORD dwExit=0;
		GetExitCodeProcess(pi.hProcess,&dwExit); 

		// Close process and thread handles.
		CloseHandle (pi.hProcess);
		CloseHandle	(pi.hThread);

		return TRUE;

}

/**
@brief      모듈 실행
@author    JHLEE
@date      2011.06.13
@return    bool(true / false)
@param    [in]strCommand - 프로세스 경로
@param    [in]iShowMode -  실행 옵션
*/
BOOL CImpersonator::CreateProcess(CString strCommand, USHORT iShowMode, CString _strDeskTop)
{
	STARTUPINFO			si;
	PROCESS_INFORMATION pi;

	ZeroMemory(&si, sizeof(si));
	ZeroMemory(&pi, sizeof(pi));

	si.cb			= sizeof(si);

	if (iShowMode == SW_HIDE) si.dwFlags = STARTF_USESHOWWINDOW;

	if( !_strDeskTop.IsEmpty() )
	{
		si.lpDesktop = (LPTSTR)(LPCTSTR)_strDeskTop;
	}

	si.wShowWindow	= iShowMode;
	

	CString strMsg;
	// Start the child process. 
	BOOL bResult = ::CreateProcess(NULL,				// No module name (use command line). 
		(LPTSTR)(LPCTSTR)strCommand,		// Command line. 
		NULL,             // Process handle not inheritable. 
		NULL,             // Thread handle not inheritable. 
		FALSE,            // Set handle inheritance to FAL
		NORMAL_PRIORITY_CLASS,
		NULL,             // Use parent's environment block. 
		NULL,             // Use parent's starting directory. 
		&si,              // Pointer to STARTUPINFO structure.
		&pi);             // Pointer to PROCESS_INFORMATION structure.

	if (!bResult)			return FALSE;


	// Wait until child process exits

	DWORD dwExit=0;
	GetExitCodeProcess(pi.hProcess,&dwExit); 

	// Close process and thread handles.
	CloseHandle (pi.hProcess);
	CloseHandle	(pi.hThread);

	return TRUE;

}

BOOL CImpersonator::RunProcessAndWait(CString sCmdLine,   CString sRunningDir, DWORD *nRetValue)
{
	int nRetWait;
	int nError;

	// That means wait 300 s before returning an error
	// You can change it to the value you need.
	// If you want to wait for ever just use 'dwTimeout = INFINITE'>
	DWORD dwTimeout = INFINITE; 
	CString strMsg;
	STARTUPINFO stInfo;
	PROCESS_INFORMATION prInfo;
	BOOL bResult;
	ZeroMemory( &stInfo, sizeof(stInfo) );
	stInfo.cb = sizeof(stInfo);
	stInfo.dwFlags=STARTF_USESHOWWINDOW;
	stInfo.wShowWindow=SW_HIDE;

	bResult = ::CreateProcess(NULL, 
		(LPTSTR)(LPCTSTR)sCmdLine, 
		NULL, 
		NULL, 
		TRUE,
		CREATE_NEW_CONSOLE 
		| NORMAL_PRIORITY_CLASS,
		NULL,
		(LPCTSTR)sRunningDir, //
		&stInfo, 
		&prInfo);


	if (!bResult)
	{
		nError = GetLastError();
		if(nRetValue)
			*nRetValue = nError;
		
		strMsg.Format(_T("[Setup] RunProcessAndWait ERROR(%d)"), nError);
		UM_WRITE_LOG(strMsg);
		return FALSE;
	}
	nRetWait =  WaitForSingleObject(prInfo.hProcess,dwTimeout);

	GetExitCodeProcess( prInfo.hProcess, nRetValue );

	//nError = GetLastError();
	CString str;


	//설치 실패 에러값 받기 위해 추가 
	int n = *nRetValue;


	if(n != 0)
	{
		if(n == 1)
		{
			*nRetValue = 0x7ffffff1;
		}
		else if( n == 3010)
		{
			CloseHandle(prInfo.hThread); 
			CloseHandle(prInfo.hProcess); 

			if (nRetWait == WAIT_TIMEOUT) 
			{
				strMsg.Format(_T("[Setup] - Error : WAIT_TIMEOUT(%d) - 1"), GetLastError());
				UM_WRITE_LOG(strMsg);
				return FALSE;
			}

			return TRUE;
		}
		CloseHandle(prInfo.hThread); 
		CloseHandle(prInfo.hProcess); 

		if (nRetWait == WAIT_TIMEOUT) 
		{
			strMsg.Format(_T("[Setup] - Error : WAIT_TIMEOUT(%d) - 2"), GetLastError());
			UM_WRITE_LOG(strMsg);
			return FALSE;
		}

		strMsg.Format(_T("[Setup] - Error : WAIT_TIMEOUT(GetLastError : %d) - 3, nRetWait : %d, nRetValue :%d "), GetLastError(), nRetWait, n);
		UM_WRITE_LOG(strMsg);
		return FALSE;
	}


	CloseHandle(prInfo.hThread); 
	CloseHandle(prInfo.hProcess); 

	if (nRetWait == WAIT_TIMEOUT) 
	{
		strMsg.Format(_T("[Setup] - Error : WAIT_TIMEOUT(%d) - 4"), GetLastError());
		UM_WRITE_LOG(strMsg);
		return FALSE;
	}

//	strMsg.Format(_T("[Setup] - rettrun : TRUE"), GetLastError());
//	UM_WRITE_LOG(strMsg);
	return TRUE;
}

/**
@brief      콘솔 프로세스 실행시 출력값을 받아 비교하여 처리하는 함수
@author    hhh
@param    [in]_strExeFullPath - 프로세스 경로
@param    [in]pszCompareStr -  콘솔창에 출력된 문자와 비교할 스트링
@param    [in]_strCmd -  콘솔 프로세스 실행시 들어갈 인자
@note		콘솔 프로그램을 구동시켜 출력된 문자열과 비교하여 처리시 사용
@date      2013.05.08
@return    BOOL(true / false)
*/
BOOL	 CImpersonator::CreatProcessAndCompareStr(CString _strExeFullPath, char* pszCompareStr, CString _strCmd)
{
	if(pszCompareStr == NULL)
		return FALSE;

	CString strPath;
	if(_strCmd.IsEmpty())
		strPath = _strExeFullPath;
	else
		strPath.Format(_T("%s %s"), _strExeFullPath, _strCmd);


	BOOL flag;
	HANDLE hwrite, hread;
	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = true;
	CString strLog, strDriverModulePath;
	// 어노니머스 파이프 생성
	flag = CreatePipe(&hread, &hwrite, &sa, 0);
	if (!flag)
	{
		strLog.Format(_T("[imp] .. CreatePipeError : %d"), GetLastError() );
		UM_WRITE_LOG(strLog);

		return FALSE;
	}

	// 콘솔어플리케이션 프로세스 실행을 위한 준비
	STARTUPINFO si;
	memset(&si, 0, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	si.hStdOutput = hwrite;  // 표준출력(stdout) 리다이렉션
	si.hStdError = hwrite;   // 표준에러(stderr) 리다이렉션
	PROCESS_INFORMATION pi;
	memset(&pi, 0, sizeof(PROCESS_INFORMATION));

	// 콘솔어플리케이션 프로세스 실행

	flag = ::CreateProcess(NULL, strPath.GetBuffer(0), NULL, NULL, TRUE, DETACHED_PROCESS,  //DETACHED_PROCESS
		NULL, NULL, &si, &pi);
	if(!flag)
	{
		strLog.Format(_T("[imp] . CreateProcess : %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
		return FALSE ;
	}

	CloseHandle(hwrite);//이것을 하지 않으면 프로세스가 block된다

	char buffer[512]={0,};
	DWORD BytesRead;
	CString ResultString;
	while(ReadFile(hread, buffer, sizeof(buffer)-1, &BytesRead, NULL) && BytesRead)
	{
		buffer[BytesRead] = '\0';
		
	}
	CloseHandle(hread);
//	OutputDebugStringA("[imp] "+buffer);
	if(strstr(buffer, pszCompareStr) != NULL)
	{
		CString strLog = _T("");
		strLog.Format(_T("[imp] TRUE %S"), buffer);
		UM_WRITE_LOG(strLog);
		return TRUE;		
	}
	else 
	{
		CString strLog = _T("");
		strLog.Format(_T("[imp] TRUE %S"), buffer);
		UM_WRITE_LOG(strLog);
		return TRUE;
	}
		
	
}

CString	 CImpersonator::CreatProcessAndGetConsole(CString _strExeFullPath, CString _strCmd)
{
	CString strPath;
	CString strConsolePrint = _T("");
	if(_strCmd.IsEmpty())
		strPath = _strExeFullPath;
	else
		strPath.Format(_T("%s %s"), _strExeFullPath, _strCmd);


	BOOL flag;
	HANDLE hwrite, hread;
	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = true;
	CString strLog, strDriverModulePath;
	DWORD dwFileSize;
	// 어노니머스 파이프 생성
	flag = CreatePipe(&hread, &hwrite, &sa, 0);
	if (!flag)
	{
		strLog.Format(_T("[imp] .. CreatePipeError : %d"), GetLastError() );
		UM_WRITE_LOG(strLog);

		return strLog;
	}

	// 콘솔어플리케이션 프로세스 실행을 위한 준비
	STARTUPINFO si;
	memset(&si, 0, sizeof(STARTUPINFO));
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
	si.hStdOutput = hwrite;  // 표준출력(stdout) 리다이렉션
	si.hStdError = hwrite;   // 표준에러(stderr) 리다이렉션
	PROCESS_INFORMATION pi;
	memset(&pi, 0, sizeof(PROCESS_INFORMATION));

	// 콘솔어플리케이션 프로세스 실행

	flag = ::CreateProcess(NULL, strPath.GetBuffer(0), NULL, NULL, TRUE, DETACHED_PROCESS,  //DETACHED_PROCESS
		NULL, NULL, &si, &pi);
	if(!flag)
	{
		strLog.Format(_T("[curltest] . CreateProcess : %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
		return strLog;
	} else {
		dwFileSize = GetFileSize(hread,  NULL);
	}

	CloseHandle(hwrite);//이것을 하지 않으면 프로세스가 block된다

	DWORD BytesRead;
	CString ResultString;
	char *pszBuffer = new char[dwFileSize + 1];
	memset(pszBuffer, 0x00, dwFileSize + 1);
	while(ReadFile(hread, pszBuffer, sizeof(pszBuffer)-1, &BytesRead, NULL) && BytesRead)
	{
		pszBuffer[BytesRead] = '\0';

	}
	CloseHandle(hread);
	//	OutputDebugStringA("[imp] "+buffer);
	strConsolePrint = (CStringW)pszBuffer;
	if(!strConsolePrint.IsEmpty())
	{
		CString strLog = _T("");
		strLog.Format(_T("[curltest] TRUE %S"), pszBuffer);
		UM_WRITE_LOG(strLog);
		return strConsolePrint;		
	}
	else 
	{
		CString strLog = _T("");
		strLog.Format(_T("[curltest] TRUE %S"), pszBuffer);
		UM_WRITE_LOG(strLog);
		return strLog;
	}


}

/**
@brief      관리자 권한으로 프로세스 구동
@author    hhh
@param    [in]_strPath - 프로세스 경로
@param    [in]_strParam -  인자
@param    [in]IsShow -  보일것인지, deafult 는 보임

@note		관리자 권한으로 프로세스를 구동
@date      2013.05.22
@return    BOOL(true / false)
*/
BOOL	 CImpersonator::RunAsExecuteProcess(CString _strPath, CString _strParam, BOOL IsShow, BOOL IsWait)
{
	
	BOOL bRet;
	SHELLEXECUTEINFO exe_info = {0,};
	exe_info.cbSize = sizeof(SHELLEXECUTEINFO);
	exe_info.hwnd = NULL;
	exe_info.fMask = SEE_MASK_FLAG_DDEWAIT | SEE_MASK_FLAG_NO_UI | SEE_MASK_NOCLOSEPROCESS;
	exe_info.lpVerb=_T("runas");
	exe_info.lpFile = _strPath;
	exe_info.lpParameters =_strParam;
	if( !IsShow)
		exe_info.nShow =SW_HIDE;
	else
		exe_info.nShow =SW_SHOWNORMAL;

	bRet = ::ShellExecuteEx(&exe_info);

	if( !bRet )
	{
		CString strLog;
		strLog.Format(_T("[Tray] RunAsExecuteProcess Fail : %d"), GetLastError());
		UM_WRITE_LOG(strLog);
	}

	//Process종료시 까지 대기 할때
	if(IsWait)
	{
		WaitForSingleObject(exe_info.hProcess, INFINITE);
	}
	
	return bRet;
}


/**
@brief    부모 프로세스의 Pid를 가져온다.
@author    hhh

@note	  부모 pid를 가져온다.
         
@date      2013.07.24

@return    DWORD - 부모 pid
*/
DWORD CImpersonator::GetParentPID()
{

	HANDLE hResult = INVALID_HANDLE_VALUE;
    typedef HANDLE (WINAPI *fpCreateToolhelp32Snapshot) (DWORD, DWORD);
    typedef BOOL (WINAPI *fpProcess32First) (HANDLE, LPPROCESSENTRY32);
    typedef BOOL (WINAPI *fpProcess32Next) (HANDLE, LPPROCESSENTRY32);

    fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
    fpProcess32First   pProcess32First = NULL;
    fpProcess32Next    pProcess32Next = NULL;

    HMODULE hModule = ::GetModuleHandle(_T("kernel32"));
    if (NULL == hModule)
	{
	//	UM_WRITE_LOG(_T("GetProcessExist - GetModuleHandle Error(_sProcessName)")+ sExeName);
        return -1;
	}

    pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(hModule, "CreateToolhelp32Snapshot");
    pProcess32First = (fpProcess32First) ::GetProcAddress(hModule, PROCESS32FIRST);
    pProcess32Next  = (fpProcess32Next) ::GetProcAddress(hModule, PROCESS32NEXT);

    if ((NULL == pCreateToolhelp32Snapshot) || (NULL == pProcess32First) ||
                (NULL == pProcess32Next))
	{
	//	UM_WRITE_LOG(_T("GetProcessExist - pCreateToolhelp32Snapshot is NULL or pProcess32Next is null"));
        return -2;
	}

    HANDLE hSnap = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (INVALID_HANDLE_VALUE == hSnap)
	{
	//	UM_WRITE_LOG(_T("GetProcessExist - Error 1"));
		return -3;
	}

    PROCESSENTRY32 pcEntry;
    pcEntry.dwSize = sizeof(PROCESSENTRY32);

    if (!pProcess32First(hSnap, &pcEntry))
    {
        ::CloseHandle(hSnap);
	//	UM_WRITE_LOG(_T("GetProcessExist - Error 2"));
        return -4;
    }

	DWORD dwCurrentPid = ::GetCurrentProcessId();
  
    do
    {
     //  if (::GetCurrentProcessId() == pcEntry.th32ParentProcessID) //
	//		continue;
		if(pcEntry.th32ProcessID == dwCurrentPid)
		{			
			return pcEntry.th32ParentProcessID;
			::CloseHandle(hSnap);		
		}
   
    }while (pProcess32Next(hSnap, &pcEntry));

//	UM_WRITE_LOG(_T("GetProcessExist - Error 4"));
    ::CloseHandle(hSnap);
    return 0;
}


/**
@brief     부모 프로세스가 정당한것이가 체크
@author    hhh
@param    [in]_strParentPath - 부모 프로세스의 실행 경로


@date      2013.07.24
@return    BOOL(true / false)
*/
BOOL CImpersonator::IsGoodParentProcess(CString _strParentPath)
{
	CString strGetParentPath = L"";
	CString strLog = _T("");
	DWORD dwParentPid = GetParentPID();
	strLog.Format(_T("[Impersonator] dwParentPid : %d [%d, %s, %s]"), dwParentPid, __LINE__, __FUNCTIONW__, __FILEW__);
	//UM_WRITE_LOG(strLog);

	if( dwParentPid > 0 )
	{
		strGetParentPath = GetProcessFullPath(dwParentPid);
		if(FALSE == strGetParentPath.IsEmpty() )
		{
			strLog = _T("");
			strLog.Format(_T("[Impersonator] strGetParentPath : %s, _strParentPath : %s [%d, %s, %s]"), strGetParentPath, _strParentPath, __LINE__, __FUNCTIONW__, __FILEW__);
			//UM_WRITE_LOG(strLog);

			// 2015-11-06 kh.choi 대소문자 구별없이 비교하도록
			strGetParentPath.MakeLower();
			_strParentPath.MakeLower();

			if( strGetParentPath.Find(_strParentPath) != -1)
				return TRUE;
			else 
				return FALSE;
		}
		else
			return FALSE;
	}
	else
		return FALSE;
}
