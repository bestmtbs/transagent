	
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
@file      PathInfo.h
@brief     CPathInfo 구현 파일
@author    hang ryul lee
@date      create 2011.04.07
*/

#include "StdAfx.h"
#include "PathInfo.h"
#include "UtilsReg.h"
#include "WinOsVersion.h"
#include "UtilsFile.h"
//#include "../Include/CommonDefine.h"
/**
@file       PathInfo.cpp 
@brief      현재 시스템 경로 / 서버 설치경로 / 클라이언트 설치경로을 가능하게 한다
@author     Hang Ryul Lee 
@date       create 2011.4.7 

@note       현재 시스템 경로를 가져올수 있다
@note       현재 설치된 클라이언트 경로를 가져올수 있다
@note       현재 설치된 서버 경로를 가져올수 있다
@note       각각의 주요 메소드는 static 으로 구현 되어있어 컴파일 시점에서 주소가 바인딩  \n 
            호출 필요시 클래스를 생성하지 않고 사용 할수있다 \n
@note       test 환경 \n
            Win 98 / 2000/ 2003 / XP / VISTA / WOW(64bit) \n
*/

#ifndef SE_ROOT_DIR
#define SE_ROOT_DIR											_T("TiorSaverConfig")
#endif

#ifdef _SETUP
#include "../TiorSaverSetup/TiorSaverSetup.h"
#endif

CPathInfo::CPathInfo(void)
{

}


CPathInfo::~CPathInfo(void)
{

}

/**
@brief     파일경로의 적절성 검사를 해주는 함수    
@author    Hang Ryul Lee
@date      2008.4.1 
@param       [IN] 파일경로
@note       경로명의 마지막에 \\를 포함 하지 않은 경우 FALSE를 Return 한다
@note       경로명 길이가 MAX_PATH(260) 를 초과 하는경우 FALSE를 Return 한다
@note      const char _cescape[] ={'*','?','"','<','>','|'} 특수 문자 있을 경우 잘못된 경로로 판단하여 FALSE 를 Return 한다\n
@return       TRUE / FALSE
*/

bool CPathInfo::IsProperFilePath(CString &_sSource)
{
    int nFileLen = _sSource.GetLength();

    if((0 == nFileLen) || (MAXLEN_PATH < (nFileLen +1)) || _sSource ==_T("")) return false;

    // 이부분에서는 경로에 특수문자가 있으면 return 하자
    const TCHAR _cescape[] ={'*','?','"','<','>','|'};  //<! 허용되지 않을 특수문자

    for (int i= 0 ; i < _sSource.GetLength() ; i++)
    {
        TCHAR s = _sSource.GetAt(i);
        if((_cescape[0] == s) || (_cescape[1] == s) || (_cescape[2] == s )
            || (_cescape[3] == s) || (_cescape[4] == s) || (_cescape[5] == s))
        {
            return false;
        }
        else
        {
            continue;
        }
    }
    return true;
}

/**
@brief     설치 볼륨를 구해주는 함수    
@author    Hang Ryul Lee
@date      2008.4.1
*/
CString CPathInfo::GetDisk()
{
    CString _sFilePath = GetWindows();
    int iFind = _sFilePath.Find('\\');
    if(-1 == iFind) _sFilePath = DEFAULT_DISK_PATH;
    else _sFilePath = _sFilePath.Left(iFind+1);
    return _sFilePath;
}


/**
@brief     레지스트리를 조사하여 서버파일의 설치 경로 를 메모리에 로드해 주는 함수    
@author    Hang Ryul Lee
@date      2008.4.1
*/
CString CPathInfo::GetServerInstall()
{

    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_VMS_SITESERVER_GENERAL, INSTALL_DIR,_T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_SERVER_INSTALL_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;
}


/**
@brief     레지스트리를 조사하여 서버파일의 DownLoad 경로 를 구하는 함수    
@author    Hang Ryul Lee
@date      2008.4.1
*/

CString CPathInfo::GetServerDown()
{
    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_VMS_SITESERVER_GENERAL, DOWN_DIR,_T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_SERVER_DOWN_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;
}



/**
@brief     레지스트리를 조사하여 에이전트파일의 설치 경로 를 구하는 함수
@author    Hang Ryul Lee
@date      2008.4.1     
*/
CString CPathInfo::GetClientInstall()
{
    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_VMS_SITECLIENT_GENERAL, INSTALL_DIR,_T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_CLIENT_INSTALL_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;

}



/**
@brief     레지스트리를 검사하여 에이전트 다운로드  경로를 구하는  함수
@author    Hang Ryul Lee
@date      2008.4.1    
*/

CString CPathInfo::GetClientDown()
{
    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_VMS_SITECLIENT_GENERAL, DOWN_DIR,_T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_CLIENT_DOWN_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;
}



/**
@brief     SHGetFolderPath API 에 타입을 넣어 폴더 경로를 구하는 함수
@author    Hang Ryul Lee
@date      2008.4.1
@note       SHGetFolderPath API 사용 
@note       Shell32.dll 환경 : Windows 2000 이상버전
@note       SHFolder.dll  환경 : Microsoft Internet Explore 4.0 and later version
@note      참고 msn 주소
@note      http://msdn2.microsoft.com/en-us/library/bb762181.aspx \n
@param     nType [IN]  구하고자 하는 폴더 타입 \n
           sFilePath [IN][OUT] 폴더 경로 반환 \n
*/

void CPathInfo::GetFolderPath(UINT _nType, CString &_sFilePath)
{
    _sFilePath = GetFilesFolder(_T("Shell32.dll"), _nType);
    if (_sFilePath.IsEmpty())
    {
        // 98Gold 이하나 NT4.0 이하 일 경우.
        _sFilePath = GetFilesFolder(_T("SHFolder.dll"), _nType);
    }

}


/**
@brief     DLL 에 LoadLibrary 하여 파일경로 구하기
@author    Hang Ryul Lee
@date      2008.4.1
@param     nType [IN]  구하고자 하는 폴더 타입 \n
_sDllName [IN] LoadLibrary 할 Dll 명
*/
CString CPathInfo::GetFilesFolder(CString _sDllName, UINT _nType)
{
    SHGETFOLDERPATH pfnSHGetFolderPath = NULL;
    HINSTANCE hModule = ::LoadLibrary(_sDllName);
    if (NULL == hModule) return _T("");

#ifdef UNICODE
    pfnSHGetFolderPath = (SHGETFOLDERPATH)GetProcAddress( hModule, "SHGetFolderPathW" );
#else
    pfnSHGetFolderPath = (SHGETFOLDERPATH)GetProcAddress( hModule, "SHGetFolderPathA" );
#endif

    if( pfnSHGetFolderPath == NULL )
    {
        FreeLibrary(hModule);
        return _T("");
    }

    TCHAR szPath[MAX_PATH+1] = {0,};
    HRESULT hResult = pfnSHGetFolderPath(NULL, _nType, NULL, 0, szPath);
    if( FAILED(hResult) )
    {
        FreeLibrary(hModule);
        return _T("");
    }
    FreeLibrary(hModule);
    return szPath;
}



/**
@brief     레지스트리를 검사하여 에이전트 다운로드  경로를 구하는  함수
@author    Hang Ryul Lee
@date      2008.4.4   
*/
CString CPathInfo::GetCommonProgram()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_PROGRAMS, _sFilePath);

    if(!_sFilePath.IsEmpty()) 
    {
        _sFilePath += _T("\\");
        return _sFilePath;
    }
    _sFilePath = GetDisk() + DEFAULT_START_MENU_PATH + _T("Programs\\");
    return _sFilePath;
}

/**
@brief     administrative tools directory for an individual user
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetAdminTool()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_ADMINTOOLS, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}

/**
@brief     the file system directory  that serves as a common repository for application-specific data
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetAppData()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_APPDATA, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}

/**
@brief     administrative tools directory for all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetCommonAdminTool()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_ADMINTOOLS, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");

}

/**
@brief     the file system directory that contains application data for all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetCommonAppData()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_APPDATA, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = GetDisk()+DEFAULT_ALL_USERS_PATH + _T("ApplicationData\\");
}


/**
@brief     The file system directory that contains files and folders that appear on the desktop for all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetCommonDeskTop()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_DESKTOPDIRECTORY, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}

/**
@brief     The file system directory that serves as a common repository for temporary Internet files
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetInternetCache()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_INTERNET_CACHE, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}

/**
@brief     The file system directory that serves as a data repository for local (nonroaming) applications
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetLocalAppData()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_LOCAL_APPDATA, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath +=_T("\\") : _sFilePath = _T("");
}

/**
@brief     The file system directory that contains documents for an individual user
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetDocument()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_PERSONAL, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}


/**
@brief     The file system directory that contains documents that are common to all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetCommonDocument()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_DOCUMENTS, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = GetDisk() + DEFAULT_ALL_USERS_PATH + _T("Documents\\");
}

/**
@brief     he file system directory that contains the templates that are available to all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/

CString CPathInfo::GetCommonTemplate()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_TEMPLATES, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = GetDisk() + DEFAULT_ALL_USERS_PATH + _T("Templates\\");
}

/**
@brief     The file system directory that contains the templates for an individual user
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetTemplate()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_TEMPLATES, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}

/**
@brief     The file system directory that serves as a common repository for Internet cookies
@author    Hang Ryul Lee
@date      2008.4.4    
*/

CString CPathInfo::GetCookie()
{
    CString _sFilePath=_T("");
    GetFolderPath(CSIDL_COOKIES, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");

}
/**
@brief     The file system directory used to physically store file objects on the desktop
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetDeskTop()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_DESKTOPDIRECTORY, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}


/**
@brief     the program files directory
@author    Hang Ryul Lee
@date      2008.4.4    
*/

CString CPathInfo::GetProgram()
{
    CString _sFilePath = _T("");
    //GetFolderPath(CSIDL_PROGRAM_FILES, _sFilePath);		//hhh - 2013.05.29 (64bit 환경에서 32bit 프로세스 구동시 x86위치를 가져오기 때문에 제거)
    //return !_sFilePath.IsEmpty() ? _sFilePath +=_T("\\") : _sFilePath = GetDisk() + DEFAULT_PROGRAM_PAHT;

	_sFilePath.Format(_T("%s\\%s"), CPathInfo::GetRoot(), DEFAULT_PROGRAM_PAHT);
	return _sFilePath;
	
}

/**
@brief     The file system directory that serves as a common repository for Internet history items
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetHistory()
{
    CString _sFilePath=_T("");
    GetFolderPath(CSIDL_HISTORY, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = _T("");
}


/**
@brief    The Windows System folder
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetSystem32()
{
    CString _sFilePath = _T("");
    UINT iBufferSize = ::GetSystemDirectory(NULL, 0);
    TCHAR *pszSystemPath = new TCHAR[iBufferSize];
    ::ZeroMemory(pszSystemPath, iBufferSize);

    if(!::GetSystemDirectory(pszSystemPath, iBufferSize))
    {
        delete[] pszSystemPath;
        return GetDisk() + DEFAULT_SYSTEM_FILES_PATH;
    }
    _sFilePath = pszSystemPath;
	delete[] pszSystemPath;
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = GetDisk() + DEFAULT_SYSTEM_FILES_PATH;
}

/**
@brief    The Windows System folder
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetSystem()
{
	CString _sFilePath = _T("");
	
	_sFilePath.Format(_T("%s%s"), GetWindows(), _T("system\\"));
	return _sFilePath;
}


/**
@brief     64bit the program files directory
@author    Hang Ryul Lee
@date      2008.4.4    
*/

CString CPathInfo::GetProgramX86()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_PROGRAM_FILESX86, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = GetDisk() + DEFAULT_PROGRAM_X86_PATH;
}

/**
@brief     64bit System Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetSystemX86()
{
    CString _sFilePath=_T("");
    GetFolderPath(CSIDL_SYSTEMX86, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath +=_T("\\") : _sFilePath = GetDisk() + DEFAULT_SYSTE_X86_FILES_PATH;
}

/**
@brief     The user's profile folder
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetProfile()
{
    CString _sFilePath=_T("");
    GetFolderPath(CSIDL_PROFILE, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath +=_T("\\") :_sFilePath = _T("");
}

/**
@brief     Windows Path
@author    Hang Ryul Lee
@date      2008.07.04
*/
CString CPathInfo::GetWindows()
{
    CString sFilePath = _T("");
    CWinOsVersion OsVersion;

    if(osWin2k > OsVersion.GetProduct())
    {
        sFilePath = GetWindowsFolder98();
    }
    else
    {
        sFilePath = GetWindowsFolder2000();
    }
    return sFilePath;
}


CString CPathInfo::GetWindowsFolder98()
{
    CString sFilePath = _T("");
    UINT iBufferSize = ::GetWindowsDirectory(NULL, 0);
    TCHAR *pszWindowsPath = new TCHAR[iBufferSize];
    ::ZeroMemory(pszWindowsPath, iBufferSize);

    if(!::GetWindowsDirectory(pszWindowsPath, iBufferSize))
    {
		delete[] pszWindowsPath;
        return GetDisk() + DEFAULT_WINDOWS_PATH;
    }

    sFilePath = pszWindowsPath;
    delete[] pszWindowsPath;

    return !sFilePath.IsEmpty() ? sFilePath += _T("\\") :sFilePath = GetDisk() + DEFAULT_WINDOWS_PATH;
}

/**
@brief     Multi-user system 에서 시스템 윈도우 디렉토리를 반환해주는 함수
@author    Hang Ryul Lee
@date      2008.07.08
@note      GetSystemWindowsDirectory  API 사용 
@note      Windows 2000 Server 이상 동작하며 Window 2003 MultiUser Windows Folder 지원
@note      참고 msdn 주소
@note      ms-help://MS.MSDNQTR.v90.ko/sysinfo/base/getsystemwindowsdirectory.htm 
@return    시스템 윈도우 디렉토리를 반환
*/
CString CPathInfo::GetWindowsFolder2000()
{

    CString _sFilePath = _T("");

    GETSYSTEMWINDOWSDIRECTORY pfnGetSystemWindowsDirectory = NULL;

    HINSTANCE hModule = ::LoadLibrary(_T("Kernel32.dll"));
    if (NULL == hModule) return _T("");

#ifdef UNICODE
    pfnGetSystemWindowsDirectory = (GETSYSTEMWINDOWSDIRECTORY)GetProcAddress( hModule, "GetSystemWindowsDirectoryW" );
#else
    pfnGetSystemWindowsDirectory = (GETSYSTEMWINDOWSDIRECTORY)GetProcAddress( hModule, "GetSystemWIndowsDirectoryA" );
#endif

    if(NULL == pfnGetSystemWindowsDirectory )
    {
        FreeLibrary(hModule);
        return GetDisk() + DEFAULT_WINDOWS_PATH;
    }
    UINT iBufferSize = pfnGetSystemWindowsDirectory(NULL, 0);
    TCHAR *pszWIndowsPath = new TCHAR[iBufferSize];
    ZeroMemory(pszWIndowsPath, iBufferSize);

    if( !pfnGetSystemWindowsDirectory(pszWIndowsPath, iBufferSize) )
    {
        FreeLibrary(hModule);
        delete[] pszWIndowsPath;
        return GetDisk() + DEFAULT_WINDOWS_PATH;
    }
    _sFilePath = pszWIndowsPath;

    delete[] pszWIndowsPath;
    FreeLibrary(hModule);

    return !_sFilePath.IsEmpty() ? _sFilePath+=_T("\\") : _sFilePath=GetDisk() + DEFAULT_WINDOWS_PATH;
}


CString CPathInfo::GetCommon()
{
    CString _sFilePath = _T("");
    _sFilePath = GetProgram() + DEFAULT_COMMON_PATH;
    return _sFilePath;


}
CString CPathInfo::GetCommonX86()
{
    CString _sFilePath = _T("");
    _sFilePath = GetProgramX86() + DEFAULT_COMMON_PATH;
    return _sFilePath;
}

/**
@brief     on the Start Menu for all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetCommonStartMenu()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_STARTMENU, _sFilePath);
    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") : _sFilePath = GetDisk() + DEFAULT_START_MENU_PATH;
}

/**
@brief     on the Startup folder for all users
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetCommonStartUp()
{
    CString _sFilePath = _T("");
    GetFolderPath(CSIDL_COMMON_STARTUP, _sFilePath);

    return !_sFilePath.IsEmpty() ? _sFilePath += _T("\\") :_sFilePath = GetDisk() + DEFAULT_START_MENU_PATH + _T("Startup\\");

}
CString CPathInfo::GetRoot()
{
	CString strWindow = _T(""), strRoot = _T("");
	
	strWindow = GetWindows();
	
	if( !strWindow.IsEmpty() )
	{
		strRoot = strWindow.Left(strWindow.Find(':')+1);
	}
	return strRoot;
}

/**
@brief     Hauri Server Deploy Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerDeploy()
{
    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_VMS_SITESERVER_GENERAL, DEPLOY_DIR,_T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_SERVER_DEPLOY_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;
}

/**
@brief     Hauri Server BackupDeploy Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerBackupDeploy()
{
    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_VMS_SITESERVER_GENERAL, BACKUP_DEPLOY_DIR, _T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_SERVER_BACKUP_DEPLOY_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;
}
/**
@brief     Hauri Client update Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetClientUpdate()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetClientInstall() + _T("Update\\");
}

/**
@brief     Hauri Server Pattern Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerPattern()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("Patterns\\");
}
/**
@brief     Hauri Server othDown Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerOthDown()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("OthDown\\");
}
/**
@brief     Hauri Server Engine Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerEngine()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("Engine\\");
}
/**
@brief     Hauri Server Component Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerComponent()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("Components\\");
}

/**
@brief     Hauri Server VISMS Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerVISMS()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("VISMSUpdate\\");
}

/**
@brief     Hauri Server Patch Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerPatch()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("Update\\");
}

/**
@brief     Hauri ICR업데이트시 사용하는 Client Update폴더
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetClientHauriUpdate()
{

    CString _sFilePath = GetRegStringValue(HKEY_LOCAL_MACHINE, SOFTWARE_HAURI_COMMON_UPDATE, INSTALL_DIR, _T(""));
    if(!IsProperFilePath(_sFilePath))
    {
        _sFilePath = GetProgram() + DEFAULT_HAURI_COMMON_UPDATE_PATH;
        return _sFilePath;
    }
    _sFilePath += _T("\\"); // 경로 뒤에는 항상 \가 붙도록 소스 통일
    return _sFilePath;
}

/**
@brief     Hauri Server AcRepair Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerAcRepair()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("AcRepair\\");
}
/**
@brief     Hauri Server AcRepair Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerAlert()
{
    CString _sFilePath = _T("");
    return _sFilePath = GetServerInstall() + _T("Alert\\");
}


/**
@brief     Hauri Server Plubgin Path
@author    Hang Ryul Lee
@date      2008.4.4    
*/
CString CPathInfo::GetServerPlugin()
{
    CString _sFilePath = _T("");
    return _sFilePath= GetServerInstall() + _T("PlugIns\\");
}

/**
@brief     주어진 포맷형식(Windows)을 define 된값과 비교하여 적절하게 변경 시켜주는 함수
@author    Hang Ryul Lee
@date      2008.4.6
@param       _sFilePath  [IN][OUT] 주어진 포맷 형식 
@note       주어진 포맷 형식이 대소문자인지 상관없이 define된값으로 변경함(2008.4.18 수정)
*/

void CPathInfo::ConvertWindowsFilePath(CString &_sFilePath)
{
    CString _sFilePathTemp = _T("");
    CString _sFormatPath = _T("");
    CString _sRemainPath = _T("");

    _sFilePathTemp = _sFilePath;
    int nPreIndex =0, nBackIndex =0;

    if(-1 != (nPreIndex = _sFilePath.Find('%') ))
    {
        if(-1 != (nBackIndex = _sFilePath.Find('%',nPreIndex+1)))
        {
            _sFormatPath = _sFilePath.Left(nBackIndex + 1);
            _sRemainPath = _sFilePath.Right(_sFilePath.GetLength() - (nBackIndex+1));
            while (_T('\\') == (_sRemainPath.Left(1) ))
            {
                _sRemainPath = _sRemainPath.Right(_sRemainPath.GetLength() - 1);
            }
            _sFormatPath.MakeUpper();

            if(_sFormatPath.Find(WINDOWS_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(WINDOWS_DIR_FORMAT, GetWindows());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(WINDOWS_SYSTEM32_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(WINDOWS_SYSTEM32_DIR_FORMAT, GetSystem32());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
			else if(_sFormatPath.Find(WINDOWS_SYSTEM_DIR_FORMAT) != -1)
			{
				_sFormatPath.Replace(WINDOWS_SYSTEM_DIR_FORMAT, GetSystem());
				_sFilePath = (_sFormatPath + _sRemainPath);
			}
            else if(_sFormatPath.Find(WINDOWS_SYSTEM86_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(WINDOWS_SYSTEM86_DIR_FORMAT, GetSystemX86());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(COMMON_FILES_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(COMMON_FILES_DIR_FORMAT, GetCommon());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(PROGRAM_FILES_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(PROGRAM_FILES_DIR_FORMAT, GetProgram());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(PROGRAM_FILES_X86_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(PROGRAM_FILES_X86_DIR_FORMAT, GetProgramX86());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(COMMON_DESKTOP_FORMAT) != -1)
            {
                _sFormatPath.Replace(COMMON_DESKTOP_FORMAT, GetCommonDeskTop());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(COMMON_PROGRAM_FORMAT) != -1)
            {
                _sFormatPath.Replace(COMMON_PROGRAM_FORMAT, GetCommonProgram());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(COMMON_START_MENU_FORMAT) != -1)
            {
                _sFormatPath.Replace(COMMON_START_MENU_FORMAT, GetCommonStartMenu());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(COMMON_STARTUP_FORMAT) != -1)
            {
                _sFormatPath.Replace(COMMON_STARTUP_FORMAT, GetCommonStartUp());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
			else if(_sFormatPath.Find(ROOT_DIR_FORMAT) != -1)
			{
				_sFormatPath.Replace(ROOT_DIR_FORMAT, GetRoot());
				_sFilePath = (_sFormatPath + _sRemainPath);
			}
			else if(_sFormatPath.Find(WINDOWS_RECYCLE_BIN) != -1)
			{
				_sFilePath = _T("recycle");
// 				_sFormatPath.Replace(ROOT_DIR_FORMAT, GetRoot());
// 				_sFilePath = (_sFormatPath + _sRemainPath);
			}
			else if(_sFormatPath.Find(INTERNET_TEMP_FILES) != -1)
			{
				_sFormatPath.Replace(INTERNET_TEMP_FILES, GetInternetCache());
				_sFilePath = (_sFormatPath + _sRemainPath);
			}
			else if(_sFormatPath.Find(INTERNET_COOKIES) != -1)
			{
				_sFormatPath.Replace(INTERNET_COOKIES, GetCookie());
				_sFilePath = (_sFormatPath + _sRemainPath);
			}
            else
            {
                _sFilePath = _sFilePathTemp;
            }
        }
        else
        {
            _sFilePath = _sFilePathTemp; //end of if((nBackIndex = _sFilePath.Find('%',nPreIndex+1)) != -1)
        }
    }
    else
    {
        _sFilePath = _sFilePathTemp; // end of     if((nPreIndex = _sFilePath.Find('%') )!= -1)
    }
    if(!IsProperFilePath(_sFilePath)) _sFilePath = _T("");
}

/**
@brief     주어진 포맷형식(Hauri)을 define 된값과 비교하여 적절하게 변경 시켜주는 함수
@author    Hang Ryul Lee
@date      2008.4.6
@param       _sFilePath  [IN][OUT] 주어진 포맷 형식  
@note       주어진 포맷 형식이 대소문자인지 상관없이 define된값으로 변경함(2008.4.18 수정)
*/
void  CPathInfo::ConvertHauriFilePath(CString &_sFilePath)
{    

    CString _sFilePathTemp = _T("");
    CString _sFormatPath = _T("");
    CString _sRemainPath = _T("");

    _sFilePathTemp = _sFilePath;
    int nPreIndex = 0,nBackIndex = 0;

    if((nPreIndex = _sFilePath.Find('%') ) != -1)
    {
        if((nBackIndex = _sFilePath.Find('%',nPreIndex+1)) != -1)
        {
            _sFormatPath = _sFilePath.Left(nBackIndex + 1);
            _sRemainPath =_sFilePath.Right(_sFilePath.GetLength() - (nBackIndex+1));
            while (_T('\\') == (_sRemainPath.Left(1) ))
            {
                _sRemainPath = _sRemainPath.Right(_sRemainPath.GetLength() - 1);
            }
            _sFormatPath.MakeUpper();
            if    (_sFormatPath.Find(INSTALL_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(INSTALL_DIR_FORMAT, GetServerInstall());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(AGENT_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(AGENT_DIR_FORMAT, GetClientInstall());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(DEPLOY_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(DEPLOY_DIR_FORMAT, GetServerDeploy());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(DEPLOY_BACKUP_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(DEPLOY_BACKUP_DIR_FORMAT, GetServerBackupDeploy());
                _sFilePath =(_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(AGENT_DOWN_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(AGENT_DOWN_DIR_FORMAT, GetClientDown());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(AGENT_UPDATE_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(AGENT_UPDATE_DIR_FORMAT, GetClientUpdate());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(TEMP_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(TEMP_DIR_FORMAT,GetCommonTemplate());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(PATTERN_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(PATTERN_DIR_FORMAT, GetServerPattern());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(OTHDOWN_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(OTHDOWN_DIR_FORMAT, GetServerOthDown());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(ENGINE_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(ENGINE_DIR_FORMAT, GetServerEngine());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(COMPONENTS_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(COMPONENTS_DIR_FORMAT,GetServerComponent());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(VISMS_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(VISMS_DIR_FORMAT, GetServerVISMS());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(DOWN_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(DOWN_DIR_FORMAT, GetServerDown());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(HAURI_UPDATE_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(HAURI_UPDATE_DIR_FORMAT, GetClientHauriUpdate());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(PATCH_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(PATCH_DIR_FORMAT,GetServerPatch());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(ACTIVE_REPAIR_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(ACTIVE_REPAIR_DIR_FORMAT, GetServerAcRepair());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if (_sFormatPath.Find(ALERT_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(ALERT_DIR_FORMAT, GetServerAlert());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else if(_sFormatPath.Find(PLUGIN_DIR_FORMAT) != -1)
            {
                _sFormatPath.Replace(PLUGIN_DIR_FORMAT, GetServerPlugin());
                _sFilePath = (_sFormatPath + _sRemainPath);
            }
            else
            {
                _sFilePath = _sFilePathTemp;
            }
        }
        else
        {
            _sFilePath = _sFilePathTemp; /**end of if((nBackIndex = _sFilePath.Find('%',nPreIndex+1)) != -1) */
        }
    }
    else
    {
        _sFilePath = _sFilePathTemp; /**end of if((nPreIndex = _sFilePath.Find('%') )!= -1) */
    }
    if(!IsProperFilePath(_sFilePath)) _sFilePath = _T("");
}

/**
@brief     주어진 포맷형식(Windows,Hauri)을 define 된값과 비교하여 적절하게 변경 시켜주는 함수
@author    odkwon
@date      2008.9.4
@param     _sFilePath  [in][out] 주어진 포맷 형식 
*/
void CPathInfo::ConvertDestPath(CString &_sFilePath)
{
    ConvertWindowsFilePath(_sFilePath);
    ConvertHauriFilePath(_sFilePath);    
}

/**
@brief      ITCMS 클라이언트 설치 경로
@author   hhh
@param	bLFC : line feed character (제일 끝에 개행문자 포함할것인가?)
@date      2013.05.28  ->
@note		//bLFC : 
@return   CString	

*/
CString CPathInfo::GetOfficeConfigPath(BOOL bLFC)
{
	CString strConfigPath = _T("");
	if(bLFC)
		strConfigPath.Format(_T("%s\\dsntech\\%s\\"),  CPathInfo::GetRoot(), SE_ROOT_DIR);
	else
		strConfigPath.Format(_T("%s\\dsntech\\%s"),  CPathInfo::GetRoot(), SE_ROOT_DIR);

	return strConfigPath;
}

/**
@brief      ITCMS 환경설정 PTP 경로
@author		  hhh
@param		bLFC : line feed character (제일 끝에 개행문자 포함할것인가?)
@date      2013.10.01  ->
@note		//bLFC : 
@return   CString	

*/
CString CPathInfo::GetOfficeConfig_PTP_Path(BOOL bLFC)
{
	CString strConfigPTP_Path = _T("");
	if(bLFC)
		strConfigPTP_Path.Format(_T("%s\\dsntech\\%s\\PTP\\"),  CPathInfo::GetRoot(), SE_ROOT_DIR);
	else
		strConfigPTP_Path.Format(_T("%s\\dsntech\\%s\\PTP"),  CPathInfo::GetRoot(), SE_ROOT_DIR);

	return strConfigPTP_Path;
}

/**
@brief      TiorSaverZone 경로
@author		  hhh
@param		bLFC : line feed character (제일 끝에 개행문자 포함할것인가?)
@date      2013.10.23  ->
@note		//bLFC : 
@return   CString	

*/
CString CPathInfo::GetTiorSaverZone_Path(BOOL bLFC)
{
	CString strPath = _T("");
	if(bLFC)
		strPath.Format(_T("%s\\TiorSaverZone\\"),  CPathInfo::GetRoot(), SE_ROOT_DIR);
	else
		strPath.Format(_T("%s\\TiorSaverZone"),  CPathInfo::GetRoot(), SE_ROOT_DIR);

	return strPath;
}

/**
@brief      All Users Path
@author		  hhh
@param		bLFC : line feed character (제일 끝에 개행문자 포함할것인가?)
@date      2013.10.28  ->
@return   CString
*/
CString CPathInfo::GetAllUserTempPath(BOOL bLFC)
{
	CString strAllUsers_Path = _T("");
	if(bLFC)
		strAllUsers_Path.Format(_T("%s\\Users\\All Users\\Temp\\"),  CPathInfo::GetRoot());
	else
		strAllUsers_Path.Format(_T("%s\\Users\\All Users\\Temp"),  CPathInfo::GetRoot());

	return strAllUsers_Path;
}


/**
@brief      ITCMS 클라이언트 설치 경로의 64비트
@author   JHLEE
@date      2012.03.08
@return   CString
*/
CString CPathInfo::GetClientInstallPath64()
{
	CString strInstallPath = _T("");

	strInstallPath.Format(_T("%s%s\\"), GetClientInstallPath(), _T("64"));

	return strInstallPath;
}

CString CPathInfo::GetClientInstallPath(BOOL bLFC)
{
	CString strInstallPath = _T("");
#ifdef _SETUP
	strInstallPath.Format(_T("%s\\"), theApp.m_strInstallPath);
#else
	strInstallPath.Format(_T("%s"), GetCurrentModulePath());
#endif
	return strInstallPath;
}

CString CPathInfo::GetTransferLogFilePath()
{
	CString strTransferLogPath = _T("");
	strTransferLogPath.Format(_T("%stransfer\\"), GetClientInstallPath());
	return strTransferLogPath;
}

/**
@brief      ITCMS 클라이언트 로그파일 경로
@author   JHLEE
@date      2011.06.09
@return   CString
*/
CString CPathInfo::GetLogFilePath()
{
	CString strLogFilePath = _T("");
	strLogFilePath.Format(_T("%s%s\\"), GetClientInstallPath(), _T("Log"));

	return strLogFilePath;
}

CString CPathInfo::GetJPGFilePath()
{
	CString strLogFilePath = _T("");
	strLogFilePath.Format(_T("%s%s\\%s\\"), GetClientInstallPath(), _T("Log"), _T("thumb"));

	return strLogFilePath;
}

/**
@brief      ITCMS 클라이언트 로컬 디비 경로
@author   JHLEE
@date      2011.06.21
@return   CString
*/
CString CPathInfo::GetDBFilePath()
{
	CString strDBFilePath = _T("");
	strDBFilePath.Format(_T("%sDB\\"), GetClientInstallPath());

	return strDBFilePath;
}

/**
@brief      윈도우 보안 업데이트가 필요한 항목을 다운받는 폴더
@author   JHLEE
@date      2011.10.04
@return   CString
*/
CString CPathInfo::GetWinUpdatePath()
{

	CString strUpdatePath = _T("");
	strUpdatePath.Format(_T("%s%s\\"), GetClientInstallPath(), _T("Update"));

	return strUpdatePath;
}

/**
@brief     백업에서 키 파일 생성 후 저장 폴더 
@author   JHLEE
@date      2012.02.06
@return   CString
*/
CString CPathInfo::GetKeyBackupPath()
{

	CString strBackupPath = _T("");
	strBackupPath.Format(_T("%s%s\\"), GetClientInstallPath(), _T("Backup"));

	return strBackupPath;
}

/**
@brief     휴지통 경로
@author    jhlee
@date      2008.4.4   
*/
CString CPathInfo::GetRecycle()
{
	CString _sFilePath = _T("");
	GetFolderPath(CSIDL_BITBUCKET, _sFilePath);

// 	if(!_sFilePath.IsEmpty()) 
// 	{
// 		_sFilePath += _T("\\");
// 		return _sFilePath;
// 	}
// 	_sFilePath = GetDisk() + DEFAULT_START_MENU_PATH + _T("Programs\\");
	return _sFilePath;
}

/**
@brief     에이전트 모듈 패치를 위한 폴더
@author   JHLEE
@date      2012.05.23
@return   CString
*/
CString CPathInfo::GetClientUpdatePath()
{

	CString strUpdatePath = _T("");
	strUpdatePath.Format(_T("%s%s\\"), GetClientInstallPath(), _T("AgentUpdate"));

	return strUpdatePath;
}

/**
@brief     64bit에서 Program files 폴더 경로 구하기 
@author   JHLEE
@date      2012.06.22
@return   CString
*/
CString CPathInfo::GetWow64Program()
{

	CString strProgram = _T("");
	TCHAR szBuffer[256] = {0,};
	::GetEnvironmentVariable(_T("ProgramW6432"), szBuffer, 256);

	strProgram.Format(_T("%s\\"), szBuffer);

	return strProgram;
}

/**
@brief      TiorSaver 설치 최상위 루트 폴더
@author   hhh
@param	bLFC : line feed character (제일 끝에 개행문자열을 포함할것인가?)
@date      2013.08.29  
@note	
@return   CString	

*/
CString CPathInfo::GetOfficeRootPath(BOOL bLFC)
{
	CString strInstallPath = _T("");
	if(bLFC)
		strInstallPath.Format(_T("%s\\%s\\"), CPathInfo::GetRoot(), _T("dsntech"));	
	else
		strInstallPath.Format(_T("%s\\%s"), CPathInfo::GetRoot(), _T("dsntech"));	

	return strInstallPath;
}
