
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      WTSSession.cpp
 @brief     Windows Terminal Service 기능의 Session Class 구현파일

            Local System의 Session 정보를 구한다
            개발자의 일관된 소스 관리를 위해 Windows Terminal Service 기능이
            지원하지 않는 OS에서 사용할 수 있도록 하였다.
            WTSSession Class에 ProcessExists, LoggedOn 추가
            활성세션이 존재하지 않을경우 -1을 리턴하도록 ActiveSessionId 수정

 @author    hang ryul lee8
 @date      create 2007.09.13 
*/

#include "stdafx.h"
#include <Psapi.h>
#include <Userenv.h>
//#include <Tlhelp32.h>
#include "ImpersonatorConst.h"
#include <WtsApi32.h>
#include "WTSSession.h"
#include "UtilsFile.h" 
#include "PathInfo.h"
//#include "CommonDefine.h"

#pragma comment(lib, "Wtsapi32.lib")
#pragma comment(lib, "Userenv.lib")


#define MAX_VALUE_LEN	  512
#define MAX_KEY_LEN		  256
#define BACK_SLASH        _T("\\")
#define SHELL             _T("Shell")
#define BOOT              _T("boot")

#define COMPARE_PROC_NAME				_T("explorer.exe")
#define RDP_LISTENING_SESSION	65536

#ifdef UNICODE
	#define GetModuleBaseNames     "GetModuleBaseNameW"
	#define WTSEnumerateSession    "WTSEnumerateSessionsW"
#else
	#define GetModuleBaseNames     "GetModuleBaseNameA"
	#define WTSEnumerateSession    "WTSEnumerateSessionsA"
#endif

#define STR_LAST_LOGOIN_SESSION_ID		_T("Tray_LoginSessionID")
#define STR_LOGIN_OUT_INFO			_T("login/logout")
#define	STR_NO_INFO						 _T("No Info")
#define STR_ITCMS_INI						_T("itcms.ini")

typedef BOOL  (WINAPI* fpEnumProcesses)(DWORD*, DWORD, DWORD*);
typedef DWORD (WINAPI* fpGetModuleBaseName)(HANDLE, HMODULE, LPTSTR, DWORD);
typedef HANDLE (WINAPI* fpCreateToolhelp32Snapshot)(DWORD, DWORD);
typedef BOOL (WINAPI* fpProcess32First)(HANDLE, LPPROCESSENTRY32);
typedef BOOL (WINAPI* fpProcess32Next)(HANDLE, LPPROCESSENTRY32);

/**************************************************************************************************
 @class     CWTSSession
 @brief     Network Session 정보를 수집하는 CWTSSession Class 구현
 @author    hang ryul lee
 @date      2007.09.13
***************************************************************************************************/

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2007.09.13
*/
CWTSSession::CWTSSession(void)
{
    m_pSessionInfo = new WTS_SESSION_INFO;
    ::ZeroMemory(m_pSessionInfo, sizeof(m_pSessionInfo));
    m_pSessionInfo->SessionId = GetCurrentSessionId();
    m_pSessionInfo->pWinStationName = new TCHAR[sizeof(TCHAR) * _tcslen(_T("Console")) + sizeof(TCHAR)];
    ::ZeroMemory(m_pSessionInfo->pWinStationName, sizeof(m_pSessionInfo->pWinStationName));
    _tcscpy_s(m_pSessionInfo->pWinStationName, sizeof(_T("Console")), _T("Console"));
    m_pSessionInfo->State = WTSActive;


// 	CString strLog;
// 	m_hMod = NULL;
// 	m_hMod = LoadLibrary(_T("kernel32.dll"));
// 	if (m_hMod != NULL)
// 	{
// 		m_WTSGetActiveConsoleSessionId = (fpWTSGetActiveConsoleSessionId)GetProcAddress(m_hMod, "WTSGetActiveConsoleSessionId");
// 		strLog.Format(_T("[ofsMgr] WTSGetActiveConsoleSessionId  : 0x%x"), m_WTSGetActiveConsoleSessionId);
// 		UM_WRITE_LOG(strLog);
// 
// 	}
}

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2007.09.13
 @param     PWTS_SESSION_INFO [in] 세션정보가 담긴 구조체
*/
CWTSSession::CWTSSession(PWTS_SESSION_INFO _pSessionInfo)
{
    if (_pSessionInfo)
    {
        m_pSessionInfo = new WTS_SESSION_INFO;
        ::ZeroMemory(m_pSessionInfo, sizeof(m_pSessionInfo));
        m_pSessionInfo->SessionId = _pSessionInfo->SessionId;
        m_pSessionInfo->pWinStationName = new TCHAR[sizeof(TCHAR)*_tcslen(_pSessionInfo->pWinStationName)+sizeof(TCHAR)];
        ::ZeroMemory(m_pSessionInfo->pWinStationName, sizeof(m_pSessionInfo->pWinStationName));
        _tcscpy_s(m_pSessionInfo->pWinStationName, sizeof(TCHAR)*_tcslen(_pSessionInfo->pWinStationName)+sizeof(TCHAR), _pSessionInfo->pWinStationName);
        m_pSessionInfo->State = _pSessionInfo->State;
    }
}

/**
 @brief     소멸자
 @author    hang ryul lee
 @date      2007.09.13
*/
CWTSSession::~CWTSSession(void)
{
    try
    {
        if (m_pSessionInfo)
        {
            if (m_pSessionInfo->pWinStationName)
                delete [] m_pSessionInfo->pWinStationName;
            delete m_pSessionInfo;
        }
    }
    catch(...)
    {
    }

//	if(m_hMod)
//		FreeLibrary(m_hMod);
}

/**
 @brief     Session Id 값을 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	Session Id
*/
DWORD CWTSSession::GetId() const
{
    if (m_pSessionInfo)
        return m_pSessionInfo->SessionId;
    else
        return GetCurrentSessionId();

}

/**
 @brief     Shell의 존재 유무를 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	true/false
*/
bool CWTSSession::IsLoggedOn()
{
    CString sShellName = GetShellName();
    return ProcessExists(sShellName);
}

/**
 @brief     ProcessId로 Session Id의 정보를 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @param		_dwProcessId   [in] Process ID
 @return	Session Id
*/
DWORD CWTSSession::GetProcessSessionId(DWORD _dwProcessId) const
{
    typedef BOOL (WINAPI* fpProcessIdToSessionId)(DWORD, DWORD*);
    fpProcessIdToSessionId pProcessIdToSessionId;  
    DWORD dwSessionId = 0;
    HINSTANCE hinstLib = NULL;
    hinstLib = ::LoadLibrary(_T("Kernel32"));
    if (NULL == hinstLib) return 0;

    pProcessIdToSessionId = (fpProcessIdToSessionId)::GetProcAddress(hinstLib, "ProcessIdToSessionId");

    if (!pProcessIdToSessionId) 
    {
        ::FreeLibrary(hinstLib);
        return 0;
    }

    if (!pProcessIdToSessionId(_dwProcessId, &dwSessionId)) 
    {
        ::FreeLibrary(hinstLib);
        return 0;
    }

    ::FreeLibrary(hinstLib);

    return dwSessionId;
}

/**
 @brief     현재의 Session Id의 정보를 구한다
 @author    hang ryul lee
 @date      2008.04.11
 @return	Session Id
*/
DWORD CWTSSession::GetCurrentSessionId() const
{
    return GetProcessSessionId(::GetCurrentProcessId());
}


/**
 @brief     OS가 WinNT 인지 판별한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	true/ false
*/
bool CWTSSession::IsWinNT() const
{
    OSVERSIONINFO vi;
    vi.dwOSVersionInfoSize = sizeof(vi);

    ::GetVersionEx(&vi);

    if( VER_PLATFORM_WIN32_NT == vi.dwPlatformId)
        return true;
    else
        return false;
}


/**
 @brief     OS가 WOW64인지 판별한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	true/ false
*/
bool CWTSSession::IsWow64() const
{
    typedef BOOL (WINAPI* fpIsWow64Process)(HANDLE, PBOOL);
    BOOL bWow64Process = FALSE;
    HINSTANCE hinstLib = NULL;

    fpIsWow64Process pIsWow64Process;
    hinstLib  = ::LoadLibrary(_T("Kernel32"));
    if (NULL == hinstLib) return false;

    pIsWow64Process = (fpIsWow64Process)::GetProcAddress(hinstLib, "IsWow64Process");

    if (NULL == pIsWow64Process) 
    {
        ::FreeLibrary(hinstLib);
        return false;
    }
    if (!pIsWow64Process(::GetCurrentProcess(), &bWow64Process)) 
    {
        ::FreeLibrary(hinstLib);
        return false;
    }

    ::FreeLibrary(hinstLib);

    return (bWow64Process ? true : false);
}

/**
 @brief     WindowsDirectory를 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	sWinDir
*/
CString CWTSSession::GetWinDir() const
{
    CString sWinDir = _T("");
    TCHAR  infoBuf[MAX_PATH] = {0,};
    UINT dwSize = MAX_PATH;
    if(NULL != ::GetWindowsDirectory(infoBuf, dwSize))
        sWinDir = infoBuf;

    return sWinDir;
}

/**
 @brief     Shell의 이름을 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	sWinDir
 @note      WinNT 이상의 경우 레지스트리 참조, 이하일 경우 Windows 폴더의 System.ini 파일 참조
*/
CString CWTSSession::GetShellName() const
{
    const CString SHELL_REG_PATH = _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon");
    const CString SHELL_DEFAULT = _T("explorer.exe");

    CString sShellName = _T("");
    HKEY hKey = NULL;

    if (IsWinNT())
    {
        if (ERROR_SUCCESS == ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, SHELL_REG_PATH, 0, KEY_ALL_ACCESS, &hKey))
        {
            DWORD	dwLen = MAX_VALUE_LEN;
            TCHAR	szValue[MAX_VALUE_LEN * sizeof(TCHAR)];
            if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, _T("Shells"), NULL, NULL, (LPBYTE)&szValue, &dwLen))
                sShellName = szValue;
            ::RegCloseKey(hKey);
        }
    }
    else
    {
        CIniFileEx SystemIni(GetWinDir() + BACK_SLASH + _T("System.ini"));
        sShellName = SystemIni.ReadString(BOOT, SHELL, SHELL_DEFAULT);
    }

    if (SHELL_DEFAULT != sShellName)
    {
        CString sTemp = sShellName;
        sTemp.MakeUpper();

        if ((_T("") == sShellName) || _T('E') == sTemp[0])
            sShellName = SHELL_DEFAULT;
    }

    return sShellName;
}



/**
 @brief     State를 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	WTS_CONNECTSTATE_CLASS
*/
WTS_CONNECTSTATE_CLASS CWTSSession::GetState() const
{
    if( m_pSessionInfo )
        return m_pSessionInfo->State;
    else
        return WTSDisconnected;
}

/**
 @brief     Windows Station의 Name을 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return	WTS_CONNECTSTATE_CLASS
*/
CString CWTSSession::GetWinStationName() const
{
    CString sWinStationName = _T("");
    if(NULL != m_pSessionInfo)
        sWinStationName = m_pSessionInfo->pWinStationName;

    return sWinStationName;
}


/**
 @brief     FileName으로 Process Exist 확인
 @author    hang ryul lee
 @date      2007.10.22
 @param     _sFileName [in] 파일이름
 @return    true  : 프로세스 존재함
            false : 프로세스 존재하지 않음
*/
bool CWTSSession::ProcessExists(CString &_sFileName)
{
    if (255 <= _sFileName.GetLength()) return false;

    bool bResult = false;
    if (IsWinNT())
    {
        if(IsWow64())
            bResult = ProcessExists9x(_sFileName);	
        else
            bResult = ProcessExistsNT(_sFileName);	
    }
    else
        bResult = ProcessExists9x(_sFileName);

    return bResult;
}

/**
 @brief     9x 계열에서 FileName으로 Process Exist 확인
 @author    hang ryul lee
 @date      2007.10.22
 @param     _sFileName [in] 파일이름
 @return    true  : 프로세스 존재함
            false : 프로세스 존재하지 않음
*/
bool CWTSSession::ProcessExists9x(CString &_sFilePath)
{
    CString  sExeName = _T("");
    CString	 sExeFile = _T("");
    HINSTANCE hinstLib = NULL;
    DWORD    dwSessionID = 0;
    HANDLE hSnapHandle = NULL;
    PROCESSENTRY32 Processentry32;
    bool bResult = false;
    fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
    fpProcess32First pProcess32First = NULL;
    fpProcess32Next  pProcess32Next = NULL;

    if (_T("") == _sFilePath) return false;

    hinstLib = ::LoadLibrary(_T("Kernel32"));
    if (NULL == hinstLib) return false;

    pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot)::GetProcAddress(hinstLib, "CreateToolhelp32Snapshot");
    pProcess32First = (fpProcess32First)::GetProcAddress(hinstLib, "Process32First");
    pProcess32Next = (fpProcess32Next)::GetProcAddress(hinstLib, "Process32Next");

    if (NULL == pCreateToolhelp32Snapshot || NULL == pProcess32First || NULL == pProcess32Next) 
    {
        ::FreeLibrary(hinstLib);
        return false;
    }

    sExeName = ExtractFileName(_sFilePath);
    sExeName.MakeUpper();

    hSnapHandle = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);

    if (INVALID_HANDLE_VALUE == hSnapHandle) 
    {
        ::FreeLibrary(hinstLib);
        return false;
    }
    Processentry32.dwSize = sizeof(Processentry32);

    if (!pProcess32First(hSnapHandle, &Processentry32))
    {
        ::CloseHandle(hSnapHandle);
        ::FreeLibrary(hinstLib);
        return false;
    }

    do
    {
        if (::GetCurrentProcessId() == Processentry32.th32ProcessID) continue;
        sExeFile = ExtractFileName(Processentry32.szExeFile);
        sExeFile.MakeUpper();
        if (0 ==  sExeFile.CompareNoCase(sExeName))
        {
            dwSessionID = GetProcessSessionId(Processentry32.th32ProcessID);
            if (dwSessionID == GetId()) 
            {
                bResult = true;
                break;
            }
        }
    }while (pProcess32Next(hSnapHandle, &Processentry32));

    ::CloseHandle(hSnapHandle);
    ::FreeLibrary(hinstLib);

    return bResult;
}

/**
 @brief     NT 계열에서 FileName으로 Process Exist 확인
 @author    hang ryul lee
 @date      2007.10.22
 @param     _sFileName [in] 파일이름
 @return    true  : 프로세스 존재함
            false : 프로세스 존재하지 않음
*/
bool CWTSSession::ProcessExistsNT(CString &_sFileName)
{
    CString  sExeName = _T("");
    CString	 sExeFile = _T("");
    HANDLE   hProcess = NULL;
    DWORD dwSessionID = 0;
    DWORD aProcesses[1024], cbNeeded, cbProcess;
    TCHAR szProcessName[MAX_PATH] = _T("");
    bool bResult = false;

    //동적할당 
    HINSTANCE hinstLib = NULL;
    fpEnumProcesses pEnumProcesses = NULL;
    fpGetModuleBaseName pGetModuleBaseName = NULL;

    if (!_sFileName) return false;

    sExeName = ExtractFileName(_sFileName);
    sExeName.MakeUpper();

    hinstLib = ::LoadLibrary(_T("Psapi"));
    if(NULL == hinstLib) return false;

    pEnumProcesses     = (fpEnumProcesses)::GetProcAddress(hinstLib, "EnumProcesses");
    pGetModuleBaseName = (fpGetModuleBaseName)::GetProcAddress(hinstLib, GetModuleBaseNames);

    if (NULL == pEnumProcesses || NULL == pGetModuleBaseName) return false;

    if (!pEnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded )) return false;

    cbProcess = cbNeeded / sizeof(DWORD);


    for(DWORD i = 0; i < cbProcess; i++)
    {
        if (::GetCurrentProcessId() == aProcesses[i]) continue;
        hProcess = ::OpenProcess( PROCESS_ALL_ACCESS, false, aProcesses[i]);
        if (NULL == hProcess) continue;
        if (0 == pGetModuleBaseName(hProcess, 0, szProcessName, sizeof(szProcessName)/sizeof(TCHAR))) continue;

        sExeFile = ExtractFileName(szProcessName);
        sExeFile.MakeUpper();

        if (0 == sExeFile.CompareNoCase(sExeName))
        {
            dwSessionID = GetProcessSessionId(aProcesses[i]);
            if (dwSessionID == GetId())
            {
                ::CloseHandle(hProcess);
                bResult = true;
                break;
            }
        }

        ::CloseHandle(hProcess);
    }
    ::FreeLibrary(hinstLib);

    return bResult;
}



/**************************************************************************************************
 @class     CWTSSessions
 @brief     수집된 Network Session 정보를 List로 관리하는 CWTSSessions Class 구현
 @author    hang ryul lee
 @date      2007.09.13
***************************************************************************************************/

/**
 @brief     생성자
 @author    hang ryul lee
 @date      2007.09.13
*/
CWTSSessions::CWTSSessions(void)
{
    typedef BOOL  (WINAPI* fpWTSEnumerateSessions)(HANDLE, DWORD, DWORD, PWTS_SESSION_INFO*, DWORD*);
    typedef void  (WINAPI* fpWTSFreeMemory)(PVOID);

    HMODULE hWtsApi32 = NULL;
    fpWTSEnumerateSessions pWTSEnumerateSessions = NULL; 
    fpWTSFreeMemory pWTSFreeMemory = NULL;

    PWTS_SESSION_INFO pSessionInfo = NULL;
    PWTS_SESSION_INFO pTempSession = NULL;
    DWORD dwCount;
    DWORD i;
    CWTSSession *Session = NULL;

    hWtsApi32 = ::LoadLibrary(_T("Wtsapi32.dll"));
    
    if(NULL == hWtsApi32) 
    {
        //WTS를 지원하지 않는 OS
        Session = new CWTSSession;
        m_List.AddTail(reinterpret_cast<CObject*>(Session));
        return;
    }

    pWTSEnumerateSessions = (fpWTSEnumerateSessions)::GetProcAddress(hWtsApi32, WTSEnumerateSession);

    if (NULL != pWTSEnumerateSessions)
    {
        //WTS 지원OS
        if (!pWTSEnumerateSessions(HANDLE WTS_CURRENT_SERVER_HANDLE, 0, 1, &pSessionInfo, &dwCount))
        {
            //Terminal service 가 시작되지 않았을때 세션정보 얻어오지 못하는 경우 0번세션값을 추가해 준다
            if (ERROR_APP_WRONG_OS == GetLastError() || RPC_S_INVALID_BINDING == GetLastError())
            {
                Session = new CWTSSession;
                m_List.AddTail(reinterpret_cast<CObject*>(Session));
            }
            ::FreeLibrary(hWtsApi32);
            return;
        }
        pTempSession = pSessionInfo;
        for(i = 0; i < dwCount; i++)
        {
            Session = new CWTSSession(pTempSession);
            m_List.AddTail(reinterpret_cast<CObject*>(Session));
            ++pTempSession;
        }
        pWTSFreeMemory = (fpWTSFreeMemory)::GetProcAddress(hWtsApi32, "WTSFreeMemory");
        if (pWTSFreeMemory)
            pWTSFreeMemory(pSessionInfo);
    }
    else
    {
        //WTS를 지원하지 않는 OS
        Session = new CWTSSession;
        m_List.AddTail(reinterpret_cast<CObject*>(Session));
    }

    ::FreeLibrary(hWtsApi32);	
}

/**
 @brief     소멸자
 @author    hang ryul lee
 @date      2007.09.13
*/
CWTSSessions::~CWTSSessions(void)
{
    ClearSessionList();	
}

/**
 @brief     세션 정보가 담긴 List를 지운다.
 @author    hang ryul lee
 @date      2007.09.13
*/
void CWTSSessions::ClearSessionList()
{
    if (0 >= m_List.GetCount()) return;
    POSITION pos = m_List.GetHeadPosition();

    while(pos != NULL)
    {
        CWTSSession *pInfo = reinterpret_cast<CWTSSession*>(m_List.GetNext(pos));
        if(pInfo != NULL)
        {
            delete pInfo;
            pInfo = NULL;
        }
    }
    m_List.RemoveAll();
}

/**
 @brief     Activae 된 Session Id를 구한다
 @author    hang ryul lee
 @date      2007.10.22
 @return    INT  : session Id
*/
INT CWTSSessions::GetActiveSessionId(void)
{
    if (m_List.GetCount() <= 0) return -1;
    POSITION pos = NULL;

    for (INT i = 0; i < m_List.GetCount(); i++)
    {
        pos = m_List.FindIndex(i);
        CWTSSession *pInfo = GetItems(i);
        if(WTSActive == pInfo->GetState()) 
            return pInfo->GetId();
    }
    return -1;
}

/**
 @brief     리스트에서 해당 Index의 포인터를 넘겨준다
 @author    hang ryul lee
 @date      2007.11.07
 @param     _nIndex [in] index
 @return    CWTSSession* [out] CWTSSession의 포인터
*/
CWTSSession* CWTSSessions::GetItems(INT _nIndex)
{
    if (_nIndex > m_List.GetCount()) return NULL;

    POSITION pos = m_List.FindIndex(_nIndex);
    if (NULL==pos) return NULL;

    CWTSSession *pInfo = reinterpret_cast<CWTSSession*>(m_List.GetAt(pos));
    return pInfo;
}

/**
 @brief     리스트의 Counter를 넘겨준다
 @author    hang ryul lee
 @date      2007.11.07
 @return    INT [out] count
*/
INT CWTSSessions::GetCount(void) const
{
    return (static_cast<INT>(m_List.GetCount()));
}

DWORD CWTSSession::GetCurrentLogonSessionID_Temp(DWORD* pFindPid)
{
	CString strLog;
	BOOL bFindSession = FALSE;

	DWORD dwCSID;
	
	//UM_WRITE_LOG(_T("[Wts] GetCurrentLogonSessionID start"));
	//UM_WRITE_LOG(_T("[Wts] GetCurrentLogonSessionID start"));
	dwCSID = GetRemoteCurrentSessionId();
	if(dwCSID == -1)		//원격으로 붙었을때
		dwCSID = WTSGetActiveConsoleSessionId();

	//strLog.Format(_T("[Wts] dwCSID : %d"), dwCSID);
	//UM_WRITE_LOG(strLog);
	if(dwCSID == -1)
		return -1;

	return dwCSID;
	
/*	HINSTANCE hinstLib;
	//PROCESSENTRY32W Processentry32;
	bool bResult = false;
	fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
	fpProcess32First pProcess32First = NULL;
	fpProcess32Next  pProcess32Next = NULL;

	hinstLib = ::LoadLibrary(_T("Kernel32"));
	if (NULL == hinstLib) return -2;

	pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot)::GetProcAddress(hinstLib, "CreateToolhelp32Snapshot");
	pProcess32First = (fpProcess32First)::GetProcAddress(hinstLib, "Process32FirstW");
	pProcess32Next = (fpProcess32Next)::GetProcAddress(hinstLib, "Process32NextW");

	if (NULL == pCreateToolhelp32Snapshot || NULL == pProcess32First || NULL == pProcess32Next) 
	{
		::FreeLibrary(hinstLib);
		return -2;
	}

	BOOL bGet = FALSE;  

	HANDLE hSnapshot;  
	PROCESSENTRY32W ppe;     
	hSnapshot = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
	ppe.dwSize = sizeof(PROCESSENTRY32W);                        //엔트리 구조체 사이즈를 정해준다.  

	CString strProcessName =_T(""), strPid;
	DWORD dwGetSessionID, dwRetSid = -1;

	bGet = pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  

	while (bGet)  
	{  
		strPid.Format(_T("%d"), ppe.th32ProcessID);
		//__asm int 3; //hhh_test
		strProcessName.Format(_T("%s"), ppe.szExeFile);

		UM_WRITE_LOG(_T("[Wts]  hh- proc_name : ") + strProcessName + _T("Pid : ") + strPid);
		strProcessName.MakeLower();

		//세션을 확인할 프로세스 이름 발견시
		if( strProcessName ==COMPARE_PROC_NAME ) 
		{	
			dwGetSessionID = GetProcessSessionId(ppe.th32ProcessID);
			strLog.Format(_T("[Wts] Pid:%d(SeesionID: %d) - _InSessionID:%d"), ppe.th32ProcessID, dwGetSessionID, dwGetSessionID);
			UM_WRITE_LOG(strLog);
			if(dwGetSessionID == dwCSID)	//현재의 세션ID를 가진 explorer.exe 가 존재한다면 윈도우 계정에 로그인한상태
			{
				if(pFindPid != NULL)
					*pFindPid = ppe.th32ProcessID;

				dwRetSid = dwCSID;
				break;
			}
		}

		bGet = pProcess32Next(hSnapshot, &ppe);  
	}  

	CloseHandle(hSnapshot);  
	::FreeLibrary(hinstLib);
	return dwRetSid;*/

}

/**
@brief      현재 윈도우 계정에 로그인한 세션 아이디 반환
@author    hhh
@date      2013.04.29
@return   Session ID , window에 로그인 안한상태일 때는 -1, 기타 에얼 -2
@note	  윈도우 계정에 로그인한 세션 ID, 윈도우 계정에 로그인하지 않은 상태라면 -1을 리턴
*/
DWORD CWTSSession::GetCurrentLogonSessionID(DWORD* pFindPid)
{
	//서버지원 방식으로 변경되어 그에 따라 처리한다.
	//로그인되어 있는 세션은 Tray에 로그인한 세션 id(Tray에서 ini파일에 세션값을 기록해둔다.)
/*	CString strLog;
	BOOL bFindSession = FALSE;

	DWORD dwCSID;
	
	//UM_WRITE_LOG(_T("[Wts] GetCurrentLogonSessionID start"));
	//UM_WRITE_LOG(_T("[Wts] GetCurrentLogonSessionID start"));
	dwCSID = GetRemoteCurrentSessionId();
	if(dwCSID == -1)		//원격으로 붙었을때
		dwCSID = WTSGetActiveConsoleSessionId();

	//strLog.Format(_T("[Wts] dwCSID : %d"), dwCSID);
	//UM_WRITE_LOG(strLog);
	if(dwCSID == -1)
		return -1;


	HINSTANCE hinstLib;
	//PROCESSENTRY32W Processentry32;
	bool bResult = false;
	fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
	fpProcess32First pProcess32First = NULL;
	fpProcess32Next  pProcess32Next = NULL;

	hinstLib = ::LoadLibrary(_T("Kernel32"));
	if (NULL == hinstLib) return -2;

	pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot)::GetProcAddress(hinstLib, "CreateToolhelp32Snapshot");
	pProcess32First = (fpProcess32First)::GetProcAddress(hinstLib, "Process32FirstW");
	pProcess32Next = (fpProcess32Next)::GetProcAddress(hinstLib, "Process32NextW");

	if (NULL == pCreateToolhelp32Snapshot || NULL == pProcess32First || NULL == pProcess32Next) 
	{
		::FreeLibrary(hinstLib);
		return -2;
	}

	BOOL bGet = FALSE;  

	HANDLE hSnapshot;  
	PROCESSENTRY32W ppe;     
	hSnapshot = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
	ppe.dwSize = sizeof(PROCESSENTRY32W);                        //엔트리 구조체 사이즈를 정해준다.  

	CString strProcessName =_T(""), strPid;
	DWORD dwGetSessionID, dwRetSid = -1;

	bGet = pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  

	while (bGet)  
	{  
		strPid.Format(_T("%d"), ppe.th32ProcessID);
		//__asm int 3; //hhh_test
		strProcessName.Format(_T("%s"), ppe.szExeFile);

		UM_WRITE_LOG(_T("[Wts]  hh- proc_name : ") + strProcessName + _T("Pid : ") + strPid);
		strProcessName.MakeLower();

		//세션을 확인할 프로세스 이름 발견시
		if( strProcessName ==COMPARE_PROC_NAME ) 
		{	
			dwGetSessionID = GetProcessSessionId(ppe.th32ProcessID);
			strLog.Format(_T("[Wts] Pid:%d(SeesionID: %d) - _InSessionID:%d"), ppe.th32ProcessID, dwGetSessionID, dwGetSessionID);
			UM_WRITE_LOG(strLog);
			if(dwGetSessionID == dwCSID)	//현재의 세션ID를 가진 explorer.exe 가 존재한다면 윈도우 계정에 로그인한상태
			{
				if(pFindPid != NULL)
					*pFindPid = ppe.th32ProcessID;

				dwRetSid = dwCSID;
				break;
			}
		}

		bGet = pProcess32Next(hSnapshot, &ppe);  
	}  

	CloseHandle(hSnapshot);  
	::FreeLibrary(hinstLib);
	return dwRetSid;
*/


	DWORD dwSession_ID = 0, dwTempval = 0;
	CString strIniPath = _T("");
	CString strLastLoginSessionID = _T("");
	CString strLog = _T("");
	dwTempval = GetCurrentLogonSessionID_Temp();	
		
	//실제 해당 세션이 존재하는지 확인하고 없으면 현재 활성화된 세션 id를 넘겨야 한다.
	dwSession_ID = RealLogin_SessionCheck(dwTempval);
	strLog.Format(_T("[Wts] dwTempval : %d,  dwSession_ID: %d "), dwTempval, dwSession_ID);
	UM_WRITE_LOG(strLog);

	if(pFindPid !=NULL)
	{
		HINSTANCE hinstLib;
		//PROCESSENTRY32W Processentry32;
		bool bResult = false;
		fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
		fpProcess32First pProcess32First = NULL;
		fpProcess32Next  pProcess32Next = NULL;

		hinstLib = ::LoadLibrary(_T("Kernel32"));
		if (NULL == hinstLib) return -2;

		pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot)::GetProcAddress(hinstLib, "CreateToolhelp32Snapshot");
		pProcess32First = (fpProcess32First)::GetProcAddress(hinstLib, "Process32FirstW");
		pProcess32Next = (fpProcess32Next)::GetProcAddress(hinstLib, "Process32NextW");

		if (NULL == pCreateToolhelp32Snapshot || NULL == pProcess32First || NULL == pProcess32Next) 
		{
			::FreeLibrary(hinstLib);
			return -2;
		}

		BOOL bGet = FALSE;  

		HANDLE hSnapshot;  
		PROCESSENTRY32W ppe;     
		hSnapshot = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
		ppe.dwSize = sizeof(PROCESSENTRY32W);                        //엔트리 구조체 사이즈를 정해준다.  

		CString strProcessName =_T(""), strPid;
		DWORD dwGetSessionID;

		bGet = pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  

		while (bGet)  
		{  
			//strPid.Format(_T("%d"), ppe.th32ProcessID);
			
			strProcessName.Format(_T("%s"), ppe.szExeFile);			
			strProcessName.MakeLower();

			//세션을 확인할 프로세스 이름 발견시
			if( strProcessName ==COMPARE_PROC_NAME ) 
			{	
				dwGetSessionID = GetProcessSessionId(ppe.th32ProcessID);
				strLog.Format(_T("[Wts] Pid:%d(SeesionID: %d) - _InSessionID:%d"), ppe.th32ProcessID, dwGetSessionID, dwSession_ID);
				UM_WRITE_LOG(strLog);
				if(dwGetSessionID == dwSession_ID)	//현재 Tray에 로그인한 세션
				{
					if(pFindPid != NULL)
						*pFindPid = ppe.th32ProcessID;
					
					break;
				}
			}

			bGet = pProcess32Next(hSnapshot, &ppe);  
		}  

		CloseHandle(hSnapshot);  
		::FreeLibrary(hinstLib);
	}


	return dwSession_ID;
	
	
}

CString CWTSSession::GetStringCurrentLogonSessID()
{
	CString strRet;
	DWORD dwSessionID = GetCurrentLogonSessionID();
	strRet.Format(_T("%d"), dwSessionID);
	UM_WRITE_LOG(_T("[tes] --- ")+ strRet);
	return strRet;
}

/**
@brief      원격세션일때 원격세션이름을 가진 explorer.exe의 세션 ID를 반환
@author    hhh
@date      2013.05.09

*/
DWORD CWTSSession::GetRemoteCurrentSessionId()
{
	PWTS_SESSION_INFO ppSessionInfo = NULL;
	DWORD     pCount = 0, dwRetSessionID = -1;
	WTS_SESSION_INFO  wts;
	CString strStationName;
	WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0, 1, &ppSessionInfo, &pCount );
	
	CString strLog;
	strLog.Format(_T("[Wts] Session Cnt : %d"), pCount);
	UM_WRITE_LOG(strLog);

	for( DWORD i = 0; i < pCount; i++ )
	{

		wts = ppSessionInfo[i];
		LPTSTR  ppBuffer        = NULL;
		DWORD   pBytesReturned  = 0;
		
		strLog.Format(_T("[Wts] Session Curretn i : %d, wts.SessionId:%d"), i, wts.SessionId);
		UM_WRITE_LOG(strLog);

		if(wts.SessionId == RDP_LISTENING_SESSION)			//해당 세션은 리모드 리스닝 세션이므로 무시.
			continue;

		if( WTSQuerySessionInformation( WTS_CURRENT_SERVER_HANDLE,
			wts.SessionId,
			WTSWinStationName,
			&ppBuffer,
			&pBytesReturned) )
		{
			strStationName.Format(_T("%s"), ppBuffer);
			if(strStationName.Find(_T("RDP")) != -1)		//원격데스크톱 session-name를 가진 세션ID를 리턴
			{			
				WTSFreeMemory( ppBuffer );
				dwRetSessionID = wts.SessionId;
				break;
			}

		}   

		strLog.Format(_T("[Wts] Session Curretn -------- : %d"), i);
		UM_WRITE_LOG(strLog);

		WTSFreeMemory( ppBuffer );

	}

	WTSFreeMemory(ppSessionInfo);

	strLog.Format(_T("[Wts] Session Curretn End ") );
	UM_WRITE_LOG(strLog);

	return dwRetSessionID;
}

/**
@brief		세션 ID가 타당한지 체크
@author		hhh
@param		_dwSessionID : TiorSaver ini에 기록된 세션 아이디
@return		인자로 들어온 세션이 실제 구동중이면 그 세션값을 그대로 전달하고 만약 세션이 없다면 현재 활성화된 세션값으로 리턴한다.
@note		TiorSaver ini에 기록된 로그인 세션값이 타당한지 체크한다.(실제 구동중인 세션과 비교)	
@date		2014.02.24
*/
DWORD CWTSSession::RealLogin_SessionCheck(DWORD _dwSessionID)
{
	CString strLog;
	PWTS_SESSION_INFO ppSessionInfo = NULL;
	DWORD     pCount = 0, dwRetSessionID = -1;
	WTS_SESSION_INFO  wts;
	CString strStationName;

	WTSEnumerateSessions( WTS_CURRENT_SERVER_HANDLE, 0, 1, &ppSessionInfo, &pCount );
	
	BOOL bExistSession = FALSE;
	for( DWORD i = 0; i < pCount; i++ )
	{

		wts = ppSessionInfo[i];
		strLog.Format(L"[Wts] wts.SessionId : %d, _dwSessionID :%d", wts.SessionId, _dwSessionID);
		UM_WRITE_LOG(strLog);

		if(wts.SessionId == _dwSessionID)
		{
			bExistSession =TRUE;
			break;
		}
	}

	WTSFreeMemory(ppSessionInfo);

	if(bExistSession == FALSE)	//
	{
		dwRetSessionID = GetRemoteCurrentSessionId();	//원격세션
		if(dwRetSessionID == -1)
			dwRetSessionID = WTSGetActiveConsoleSessionId();
	}		
	else
		dwRetSessionID = _dwSessionID;
		
	return dwRetSessionID;
}

/**
@brief      Process 실행
@author    hhh
@param	pid : 해당토큰을 가져올 프로세스 ID
@param	strExePath : 실행할 exe경로
@note		해당 PID 권한으로 프로세스를 실행시키나.
@date		 2013.05.06
*/
BOOL CWTSSession::StartProcessAsUser(DWORD pid, CString strExePath, CString _strCmd)
{
	CString strLog;
	HANDLE hd = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
	if(hd == NULL)
		return FALSE;

	HANDLE token = NULL;
	if(OpenProcessToken(hd, TOKEN_ASSIGN_PRIMARY | TOKEN_DUPLICATE, &token) == FALSE)
	{
		strLog.Format(_T("[WTSSession] OpenProcessToken Error - %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
		CloseHandle(hd);
		return FALSE;
	}

	HANDLE newtoken = NULL;
	if(::DuplicateTokenEx(token, TOKEN_ASSIGN_PRIMARY | TOKEN_ALL_ACCESS, NULL, 
		SecurityImpersonation, TokenPrimary, &newtoken) == FALSE)
	{
		strLog.Format(_T("[WTSSession] DuplicateTokenEx Error - %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
		CloseHandle(hd);
		CloseHandle(token);
		return FALSE;
	}

	CloseHandle(token);

	void* EnvBlock = NULL;
	CreateEnvironmentBlock(&EnvBlock, newtoken, FALSE);

	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi = {0};

	if(::CreateProcessAsUser(newtoken,
		strExePath,
		_strCmd.GetBuffer(0),
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS | CREATE_UNICODE_ENVIRONMENT |
		CREATE_NEW_CONSOLE  | CREATE_SEPARATE_WOW_VDM |
		CREATE_NEW_PROCESS_GROUP,
		EnvBlock,
		NULL,
		&si,
		&pi) == TRUE)
	{

		CloseHandle(hd);
		CloseHandle(newtoken);
		return TRUE;
	}

	strLog.Format(_T("[ofsMgr] CreateProcessAsUser Error - %d"), GetLastError() );
	UM_WRITE_LOG(strLog);

	CloseHandle(hd);
	CloseHandle(newtoken);
	return FALSE;

}

/**
@brief      Process 실행
@author    hhh
@param	dwSessionID : 세션ID
@param	_strCompareProcessName : 프로세스명
@note		인자로 들어온 세션 ID에 해당 프로세스가 존재하면 그 pid를 리턴
@date		 2013.05.06
*/
//인자로 들어온 세션ID를 가진 explorer.exe가 존재하는지 체크 
//존재 한다면 그 pid를 리턴 
/*
int CWTSSession::GetPID_From_SessionID(DWORD dwSessionID, CString _strCompareProcessName)
{

	typedef HANDLE (WINAPI *fpCreateToolhelp32Snapshot) (DWORD, DWORD);
	typedef BOOL (WINAPI *fpProcess32First) (HANDLE, LPPROCESSENTRY32);
	typedef BOOL (WINAPI *fpProcess32Next) (HANDLE, LPPROCESSENTRY32);

	fpCreateToolhelp32Snapshot pCreateToolhelp32Snapshot = NULL;
	fpProcess32First   pProcess32First = NULL;
	fpProcess32Next    pProcess32Next = NULL;

	HMODULE hModule = ::GetModuleHandle(_T("kernel32"));
	if (NULL == hModule)
	{
		return -1;
	}

	pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(hModule, "CreateToolhelp32Snapshot");
	pProcess32First = (fpProcess32First) ::GetProcAddress(hModule, PROCESS32FIRST);
	pProcess32Next  = (fpProcess32Next) ::GetProcAddress(hModule, PROCESS32NEXT);

	if ((NULL == pCreateToolhelp32Snapshot) || (NULL == pProcess32First) ||
		(NULL == pProcess32Next))
	{
		//	UM_WRITE_LOG(_T("GetProcessExist - pCreateToolhelp32Snapshot is NULL or pProcess32Next is null"));
		return -2;
	}

	BOOL bGet = FALSE;  

	HANDLE hSnapshot;  
	PROCESSENTRY32 ppe;     
	CWTSSession cWtsession;
	hSnapshot = pCreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//system 프로세서(pid=0)의 상태를 읽어 온다   
	ppe.dwSize = sizeof(PROCESSENTRY32);                        //엔트리 구조체 사이즈를 정해준다.  

	CString strProcessName =_T(""), strLog;
	DWORD dwGetSessionID, dwRetPid = 0;

	bGet = pProcess32First(hSnapshot, &ppe);                     //엔트리 중 자료를 가져온다.  

	while (bGet)  
	{  
		strProcessName.Format(_T("%s"), ppe.szExeFile);
		strProcessName.MakeLower();

		//세션을 확인할 프로세스 이름 발견시
		if( strProcessName ==_strCompareProcessName ) 
		{	
			dwGetSessionID = cWtsession.GetProcessSessionId(ppe.th32ProcessID);
			strLog.Format(_T("[WTSSession] Pid:%d(SeesionID: %d) - _InSessionID:%d"), ppe.th32ProcessID, dwGetSessionID, dwSessionID);
			UM_WRITE_LOG(strLog);
			if(dwGetSessionID == dwSessionID)
			{
				dwRetPid = ppe.th32ProcessID;							
				break;
			}
		}

		bGet = pProcess32Next(hSnapshot, &ppe);  
	}  

	CloseHandle(hSnapshot);  
	FreeLibrary(hModule);

	return dwRetPid;  
}*/