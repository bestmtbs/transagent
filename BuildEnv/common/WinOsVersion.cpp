

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      WinOsVersion.cpp 
 @brief     Windows OS Version 정보를 클래스화함.  
 @author    hang ryul lee
 @date      create 2007.09.03 

 @note      검증된 OS
            - Windows 98 SE                                     \n
            - Windows NT Workstation Service Pack 6             \n
            - Windows NT Server Service Pack 6                  \n
            - Windows 2000 Profesional Service Pack 4           \n
            - Windows 2000 Server Service Pack 4                \n
            - Windows XP Professional Service Pack 2            \n
            - Windows 2003 Enterprise Edition Service Pack 1    \n
            - Windows 2003 Standard x64 Edition Service Pack 1  \n
            - Windows Vista Ultimate Edition                    \n
			- Windows XP Tablet PC Edition 2005 Version 2002 Service Pack 2 \n
		      (IsTablet 함수 정상 동작함 확인)\n
            - Windows Vista Home Preminu K (Tablet PC)\n    
*/

#include "stdafx.h"
#include "WinOsVersion.h"
#include "Convert.h"


#define REG_MAX_BUFSIZE 256

#define PRODUCT_UNLICENSED                      0xABCDABCD //Unlicensed product
#define PRODUCT_BUSINESS                        0x00000006 //Business Edition
#define PRODUCT_BUSINESS_N                      0x00000010 //Business Edition
#define PRODUCT_CLUSTER_SERVER                  0x00000012 //Cluster Server Edition
#define PRODUCT_DATACENTER_SERVER               0x00000008 //Server Datacenter Edition (full installation)
#define PRODUCT_DATACENTER_SERVER_CORE          0x0000000C //Server Datacenter Edition (core installation)
#define PRODUCT_ENTERPRISE                      0x00000004 //Enterprise Edition
#define PRODUCT_ENTERPRISE_N                    0x0000001B //Enterprise Edition
#define PRODUCT_ENTERPRISE_SERVER               0x0000000A //Server Enterprise Edition(full installation)
#define PRODUCT_ENTERPRISE_SERVER_CORE          0x0000000E //Server Enterprise Edition(core installation)
#define PRODUCT_ENTERPRISE_SERVER_IA64          0x0000000F //Server Enterprise Edition for Itanium-based Systems
#define PRODUCT_HOME_BASIC                      0x00000002 //Home Basic Edition
#define PRODUCT_HOME_BASIC_N                    0x00000005 //Home Basic Edition
#define PRODUCT_HOME_PREMIUM                    0x00000003 //Home Premium Edition
#define PRODUCT_HOME_PREMIUM_N                  0x0000001A //Home Premium Edition
#define PRODUCT_HOME_SERVER                     0x00000013 //Home Server Edition
#define PRODUCT_SERVER_FOR_SMALLBUSINESS        0x00000018 //Server for Small Business Edition
#define PRODUCT_SMALLBUSINESS_SERVER            0x00000009 //Small Business Server
#define PRODUCT_SMALLBUSINESS_SERVER_PREMIUM    0x00000019 //Small Business Server Premium Edition
#define PRODUCT_STANDARD_SERVER                 0x00000007 //Server Standard Edition (full installation)
#define PRODUCT_STANDARD_SERVER_CORE            0x0000000D //Server Standard Edition (core installation)
#define PRODUCT_STARTER                         0x0000000B //Starter Edition
#define PRODUCT_STORAGE_ENTERPRISE_SERVER       0x00000017 //Storage Server Enterprise Edition
#define PRODUCT_STORAGE_EXPRESS_SERVER          0x00000014 //Storage Server Express Edition
#define PRODUCT_STORAGE_STANDARD_SERVER         0x00000015 //Storage Server Standard Edition
#define PRODUCT_STORAGE_WORKGROUP_SERVER        0x00000016 //Storage Server Workgroup Edition
#define PRODUCT_UNDEFINED                       0x00000000 //An unknown product
#define PRODUCT_ULTIMATE                        0x00000001 //Ultimate Edition
#define PRODUCT_ULTIMATE_N                      0x0000001C //Ultimate Edition
#define PRODUCT_WEB_SERVER                      0x00000011 //Web Server Edition


/**
 @brief     기본 생성자
 @author    hang ryul lee
 @date      2007.09.03
*/
CWinOsVersion::CWinOsVersion(void)
{
    m_bOsVerInfoExExist = FALSE;
	
    ::ZeroMemory(&m_OsVerInfoEx, sizeof(OSVERSIONINFOEX));
    m_OsVerInfoEx.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
    m_bOsVerInfoExExist = ::GetVersionEx(reinterpret_cast<OSVERSIONINFO *>(&m_OsVerInfoEx));
	
    if (!m_bOsVerInfoExExist)
    {
        m_OsVerInfoEx.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
        if (! ::GetVersionEx(reinterpret_cast<OSVERSIONINFO *>(&m_OsVerInfoEx)))
            return;
    }  

    HMODULE hModule = ::GetModuleHandle(_T("kernel32.dll"));
    if (NULL == hModule)
    {
        ::GetSystemInfo(&m_SystemInfo);
    }
    else
    {
        typedef void (WINAPI *fpGetNativeSystemInfo)(LPSYSTEM_INFO);
        fpGetNativeSystemInfo pGetNativeSystemInfo = NULL;
        pGetNativeSystemInfo = reinterpret_cast<fpGetNativeSystemInfo>(::GetProcAddress(hModule, "GetNativeSystemInfo"));
        if (NULL != pGetNativeSystemInfo)
            pGetNativeSystemInfo(&m_SystemInfo);
    }
}


/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2007.09.03
*/
CWinOsVersion::~CWinOsVersion(void)
{
}


/**
 @brief     OS 버전정보중 BuildNumber를 구한다.
 @author    hang ryul lee
 @date      2007.09.03
*/
DWORD CWinOsVersion::GetBuildNumber() const
{
    return m_OsVerInfoEx.dwBuildNumber;
}



/**
 @brief     OS 버전정보를 조합하여 구성한 Description 문자열을 리턴한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Description 문자열.
 @note      Description 구성 \n
            ProductName + Edition + Service Pack + BuildNumber
*/
CString CWinOsVersion::GetDescription() const
{
    CString sDesc = _T("");
    
    CString sEdition = GetEdition();
    CString sServicePack = GetServicePack(); 
    if (sEdition != _T(""))
        sEdition = _T(" ") + sEdition;
    if (sServicePack != _T(""))
        sServicePack = _T(" ") + sServicePack;
            
    try
    {
        sDesc.Format(_T("%s%s%s (Build %d)"), static_cast<LPCTSTR>(GetProductName()), static_cast<LPCTSTR>(sEdition), static_cast<LPCTSTR>(sServicePack), GetBuildNumber());         
    }
    catch(...)
    {
    }   
    return sDesc;
}


/**
 @brief     Platform이 Win9x계열인 OS의 Edition 문자열을 리턴한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Edition 문자열
*/
CString CWinOsVersion::GetWin9xEdition() const
{
    CString sEdition = _T("");

    switch (GetProduct())
    {
    case osWin95 :
        if (_T('B') == m_OsVerInfoEx.szCSDVersion[1] || _T('C') == m_OsVerInfoEx.szCSDVersion[1])
            sEdition = _T("OSR2");
        break;
    case osWin98 :
        if (_T('A') == m_OsVerInfoEx.szCSDVersion[1])
            sEdition = _T("SE");
        break;
    }

    return sEdition;
}


/**
 @brief     OS 버전정보에 주어진 SuiteMask값이 존재하는지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @param     _wSuiteMask [in] SuiteMask값
 @return    true/false
*/
BOOL CWinOsVersion::CheckSuiteMask(WORD _wSuiteMask) const
{
    return m_OsVerInfoEx.wSuiteMask & _wSuiteMask;
}


/**
 @brief     Platform이 WinNT계열인 OS의 Edition 문자열을 리턴한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Edition 문자열
*/
CString CWinOsVersion::GetWinNTEdition() const
{
    CString sEdition = _T("");

    if (!IsWinNT()) 
        return sEdition;
    
    //NT4 SP6a 이전 버전은 확장정보가 없으므로 레지스트리에서 얻어온다.
    if (!m_bOsVerInfoExExist)
        return GetEditionFromReg();

    //NT4 SP6 이후 버전
    if (IsServer())
    {
        //Server 제품군
        switch (GetProduct())
        {
        case osWinNT :
            if (CheckSuiteMask(VER_SUITE_ENTERPRISE))
                sEdition = _T("Enterprise Edition");
            else
                sEdition = _T("Server");
            break;
        case osWin2k :
            if (CheckSuiteMask(VER_SUITE_DATACENTER))
                sEdition = _T("Datacenter Server");
            else if (CheckSuiteMask(VER_SUITE_ENTERPRISE))
                sEdition = _T("Advanced Server");
            else
                sEdition = _T("Server");
            break;
        case osWinXP :  //Windows XP는 Server Edition이 존재하지 않음.
            sEdition = _T("");
            break;
        case osWinServer2003 :
            if (IsWinServer2003R2())
                sEdition = _T("R2");            
            else if (CheckSuiteMask(VER_SUITE_DATACENTER))
            {
                switch (m_SystemInfo.wProcessorArchitecture)
                {
                case PROCESSOR_ARCHITECTURE_IA64  : 
                    sEdition = _T("Datacenter Edition for Itanium-based Systems");
                    break;
                case PROCESSOR_ARCHITECTURE_AMD64 : 
                    sEdition = _T("Datacenter x64 Edition");
                    break;
                default :
                    sEdition = _T("Datacenter Edition");
                }
            }
            else if (CheckSuiteMask(VER_SUITE_ENTERPRISE))
            {
                switch (m_SystemInfo.wProcessorArchitecture)
                {
                case PROCESSOR_ARCHITECTURE_IA64  : 
                    sEdition = _T("Enterprise Edition for Itanium-based Systems");
                    break;
                case PROCESSOR_ARCHITECTURE_AMD64 :
                    sEdition = _T("Enterprise x64 Edition");
                    break;
                default :
                    sEdition = _T("Enterprise Edition");
                }
            }
            else if (CheckSuiteMask(VER_SUITE_BLADE))
                sEdition = _T("Web Edition");
            else 
            {
                if (PROCESSOR_ARCHITECTURE_AMD64 == m_SystemInfo.wProcessorArchitecture)
                    sEdition = _T("Standard x64 Edition");
                else
                    sEdition = _T("Standard Edition");
            }
            break;
        case osWinVista :
            break;
        case osWinServer2008 :
            sEdition = GetEditionFromProductInfo();
            break;
        case osWin7 :
            sEdition = GetEditionFromProductInfo();
			break;
		case osWin8 :
            sEdition = GetEditionFromProductInfo();
            break;
		case osWinServer2008R2 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWinServer2012 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWinServer2012R2 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWinServer2016 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWin10 :
			sEdition = GetEditionFromProductInfo();
			break;
        }
    }
    else
    {
        //Workstation 제품군
        switch (GetProduct())
        {
        case osWinNT :
            sEdition = _T("Workstation");
            break;
        case osWin2k :
            sEdition = _T("Professional");
            break;
        case osWinXP :
            if (IsMediaCenter())
                sEdition = _T("Media Center Edition");
            else if (IsTablet())
                sEdition = _T("Tablet PC Edition");
            else if (CheckSuiteMask(VER_SUITE_PERSONAL))
                sEdition = _T("Home Edition");
            else
                sEdition = _T("Professional");
            break;
        case osWinServer2003 :              
            if (PROCESSOR_ARCHITECTURE_AMD64 == m_SystemInfo.wProcessorArchitecture)
            {
                //Windows XP Professional x64 Edition
                //MajorVersion = 5
                //MinorVersion = 2
                //ProductType = VER_NT_workstation
                //ProcessorArchitecture = PROCESSOR_ARCHITECTURE_AMD64
                sEdition = _T("Professional x64 Edition");
            }
            else
            {
                sEdition = _T("");
            }
            break;
        case osWinVista :
            sEdition = GetEditionFromProductInfo();
            break;
        case osWinServer2008 :  //Windows Server 2008은 Workstation Edition이 없음.
            break;
        case osWin7 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWin8 :
            sEdition = GetEditionFromProductInfo();
            break;
		case osWinServer2008R2 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWinServer2012 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWinServer2012R2 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWinServer2016 :
			sEdition = GetEditionFromProductInfo();
			break;
		case osWin10 :
			sEdition = GetEditionFromProductInfo();
			break;
        }
    }

    return sEdition;
}


/**
 @brief     OS 버전정보로부터 Edition 문자열값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Edition 문자열
*/
CString CWinOsVersion::GetEdition() const
{
    if (IsWin9x())
        return GetWin9xEdition();
    else if (IsWinNT())
        return GetWinNTEdition();
    else
        return _T("");
}


/**
 @brief     Windows Platform이 Win9x계열인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsWin9x() const
{
    return VER_PLATFORM_WIN32_WINDOWS == m_OsVerInfoEx.dwPlatformId;
}


/**
 @brief     Windows Platform이 WinNT계열인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsWinNT() const
{
    return VER_PLATFORM_WIN32_NT == m_OsVerInfoEx.dwPlatformId;
}

/**
 @brief     Windows Platform이 Win32s계열인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsWin32s() const
{
    return VER_PLATFORM_WIN32s == m_OsVerInfoEx.dwPlatformId;
}


/**
 @brief     현재 모듈이 Wow64bit 환경에서 실행중인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsWow64() const
{
    HMODULE hModule = ::GetModuleHandle(_T("kernel32.dll"));
    if (NULL == hModule)
        return false;

    typedef BOOL (WINAPI *fpIsWow64Process)(HANDLE, PBOOL);
    fpIsWow64Process pIsWow64Process;
    pIsWow64Process = reinterpret_cast<fpIsWow64Process>(::GetProcAddress(hModule, "IsWow64Process"));
    if (NULL == pIsWow64Process) return false;
    BOOL bWow64Process;
    if (!pIsWow64Process(::GetCurrentProcess(), &bWow64Process)) return false;
    return (TRUE == bWow64Process);
}


/**
 @brief     Processor Architecture가 32bit계열인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::Is32bit() const
{
    return !Is64bit();
}


/**
 @brief     Processor Architecture가 64bit계열인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::Is64bit() const
{
    return (PROCESSOR_ARCHITECTURE_IA64 == m_SystemInfo.wProcessorArchitecture ||
        PROCESSOR_ARCHITECTURE_AMD64 == m_SystemInfo.wProcessorArchitecture ||
        PROCESSOR_ARCHITECTURE_ALPHA64 == m_SystemInfo.wProcessorArchitecture);
}


/**
 @brief     Windows ProductType이 Server계열인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsServer() const
{
    if (IsWin9x()) return false;

    if (m_bOsVerInfoExExist)
        return (VER_NT_WORKSTATION != m_OsVerInfoEx.wProductType);
    else
    {
        CString sProductType = GetProductTypeFromReg();        
        return (0 != _tcsicmp(sProductType, _T("WINNT")));
    }
}

/**
 @brief     레지스트리로부터 ProductType을 읽어온다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    "WINNT" = WorkStation \n
            "LANMANNT" = Server \n
            "SERVERNT" = Advanced Server \n
            "" = 레지스트리 읽기 실패 또는 키/값이 존재하지 않을 경우.
 @note      Windows NT4 SP5 또는 이전 버전에 대해서 사용한다.
*/
CString CWinOsVersion::GetProductTypeFromReg() const
{
    HKEY hKey = NULL;
    TCHAR szProductType[REG_MAX_BUFSIZE] = {0, };
	DWORD dwBuffer = REG_MAX_BUFSIZE;
	LONG lResult;

    lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Control\\ProductOptions"), 0, KEY_QUERY_VALUE, &hKey);
	if(ERROR_SUCCESS != lResult)
		return _T("");

    lResult = ::RegQueryValueEx(hKey, _T("ProductType"), NULL, NULL, (LPBYTE)szProductType, &dwBuffer);
	if( (ERROR_SUCCESS != lResult) || (REG_MAX_BUFSIZE < dwBuffer) )
		return _T("");

    if (hKey)
        ::RegCloseKey(hKey);
    
    return CString(szProductType);
}


/**
 @brief     레지스트리로부터 Edition값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Edition 문자열 값.
 @note      Windows NT4 SP6 이전버전에 대해서 사용한다.
*/
CString CWinOsVersion::GetEditionFromReg() const
{
    CString sProductType = GetProductTypeFromReg();
    if (sProductType = _T("")) 
        return _T("");

    if (0 == _tcsicmp(sProductType, _T("WINNT")))
        return _T("WorkStation");
    else if (0 == _tcsicmp(sProductType, _T("LANMANNT")))
        return _T("Server");
    else if (0 == _tcsicmp(sProductType, _T("SERVERNT")))
        return _T("Advanced Server");
    else
        return _T("");
}


/**
 @brief     Edition값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
 @note      Windows Vista/2008 이후 버전에서만 사용가능하다.
*/
CString CWinOsVersion::GetEditionFromProductInfo() const
{
    CString sEdition = _T("");

    HMODULE hModule = ::GetModuleHandle(_T("kernel32.dll"));
    if (NULL == hModule)
        return sEdition;

    typedef BOOL (WINAPI *fpGetProductInfo)(DWORD, DWORD, DWORD, DWORD, PDWORD);
    fpGetProductInfo GetProductInfo;
    GetProductInfo = (fpGetProductInfo)::GetProcAddress(hModule, "GetProductInfo");
    if (NULL == GetProductInfo) return sEdition;

    DWORD dwProductType = 0;
	if(!GetProductInfo(GetMajorVersion(), GetMinorVersion(), GetServicePackMajor(), GetServicePackMinor(), &dwProductType)) 
        return sEdition;

	switch(dwProductType)
	{
	case PRODUCT_UNLICENSED:
        sEdition = _T("Unlicensed");		
		break;
	case PRODUCT_BUSINESS: 
    case PRODUCT_BUSINESS_N:
		sEdition = _T("Business Edition");
		break;
	case PRODUCT_ULTIMATE:
    case PRODUCT_ULTIMATE_N:
		sEdition = _T("Ultimate Edition");
		break;
	case PRODUCT_ENTERPRISE:
    case PRODUCT_ENTERPRISE_N:    
		sEdition = _T("Enterprise Edition");
		break;
	case PRODUCT_HOME_BASIC: 
    case PRODUCT_HOME_BASIC_N:
		sEdition = _T("Home Basic Edition");
		break;
	case PRODUCT_HOME_PREMIUM:
    case PRODUCT_HOME_PREMIUM_N:
		sEdition = _T("Home Premium Edition");
		break;
	case PRODUCT_STARTER:
		sEdition = _T("Starter Edition");
		break;
	case PRODUCT_DATACENTER_SERVER:
		sEdition = _T("Server Datacenter Edition (full installation)");
		break;
	case PRODUCT_DATACENTER_SERVER_CORE:
		sEdition = _T("Server Datacenter Edition (core installation)");
		break;
	case PRODUCT_ENTERPRISE_SERVER:
		sEdition = _T("Server Enterprise Edition (full installation)");
		break;
	case PRODUCT_ENTERPRISE_SERVER_CORE:
		sEdition = _T("Server Enterprise Edition (core installation)");
		break;
	case PRODUCT_ENTERPRISE_SERVER_IA64:
		sEdition = _T("Server Enterprise Edition for Itanium-based Systems");
		break;
	case PRODUCT_STANDARD_SERVER:
		sEdition = _T("Server Standard Edition (full installation)");
		break;
	case PRODUCT_STANDARD_SERVER_CORE:
		sEdition = _T("Server Standard Edition (core installation)");
		break;
	case PRODUCT_WEB_SERVER:
		sEdition = _T("Web Server Edition");
		break;
	case PRODUCT_PROFESSIONAL:
		sEdition = _T("Professional");
		break;
	}
    return sEdition;
}


/**
 @brief     레지스트리로부터 NT4 Service Pack 6a 설치유무를 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsNT4SP6a() const
{
    if (osWinNT != GetProduct())
        return false;

    if (4 != GetMajorVersion())
        return false;

    if (0 != _tcsicmp(m_OsVerInfoEx.szCSDVersion, _T("Service Pack 6")))
        return false;

    HKEY hKey = NULL;
    LONG lResult;

    lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Hotfix\\Q246009"), 0, KEY_QUERY_VALUE, &hKey);
    return NULL != hKey;
}

/**
 @brief     Windows XP MediaCenter Edition인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsMediaCenter() const
{
    return ::GetSystemMetrics(SM_MEDIACENTER) != 0; 
}


/**
 @brief     Windows XP TablePC Edition인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsTablet() const
{
    return ::GetSystemMetrics(SM_TABLETPC) != 0;
}


/**
 @brief     Windows Server 2003 R2 Edition인지 확인한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    true/false
*/
bool CWinOsVersion::IsWinServer2003R2() const
{
    return ::GetSystemMetrics(SM_SERVERR2) != 0;
}


/**
 @brief     Windows 버전 정보중 Major Version값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Major Version값
*/
DWORD CWinOsVersion::GetMajorVersion() const
{
    return m_OsVerInfoEx.dwMajorVersion;
}


/**
 @brief     Windows 버전 정보중 Minor Version값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Minor Version값
*/
DWORD CWinOsVersion::GetMinorVersion() const
{
    return m_OsVerInfoEx.dwMinorVersion;
}


/**
 @brief     Windows 버전 정보중 ProductId값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    ProductId값
*/
DWORD CWinOsVersion::GetPlatformId() const
{
    return m_OsVerInfoEx.dwPlatformId;
}


/**
 @brief     Windows Platform Name을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Platform Name값
*/
CString CWinOsVersion::GetPlatformName() const
{
    CString sPlatformName = _T("");

    switch (GetPlatformId()) 
    {
    case VER_PLATFORM_WIN32s        : sPlatformName = _T("Win32s"); break;
    case VER_PLATFORM_WIN32_WINDOWS : sPlatformName = _T("Win9x"); break;
    case VER_PLATFORM_WIN32_NT      : sPlatformName = _T("WinNT"); break;
    }
    return sPlatformName;
}


/**
 @brief     Platform Win32s 계열의 Windows Product를 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Windows Product
*/
CWinOsProduct CWinOsVersion::GetWin32sProduct() const
{
    return osWin32s;
}


/**
 @brief     Platform Win9x 계열의 Windows Product를 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Windows Product
*/
CWinOsProduct CWinOsVersion::GetWin9xProduct() const
{
    if (4 == m_OsVerInfoEx.dwMajorVersion)
    {
        switch (m_OsVerInfoEx.dwMinorVersion)
        {
        case  0 : return osWin95; 
        case 10 : return osWin98;
        case 90 : return osWinMe;
        }
    }
    return osUnknown;
}


/**
 @brief     Platform Win32s 계열의 Windows Product를 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Windows Product
 //https://msdn.microsoft.com/ko-kr/library/windows/desktop/ms724833(v=vs.85).aspx
*/
CWinOsProduct CWinOsVersion::GetWinNTProduct() const
{
	CString strLog;
	//strLog.Format(_T("m_OsVerInfoEx.dwMajorVersion : %d, m_OsVerInfoEx.dwMinorVersion: %d"), m_OsVerInfoEx.dwMajorVersion, m_OsVerInfoEx.dwMinorVersion);
	//AfxMessageBox(strLog); //hhh_log
    switch (m_OsVerInfoEx.dwMajorVersion)
    {
		

    case 3 :    //NT 3
    case 4 :    //NT 4
        switch (m_OsVerInfoEx.dwMinorVersion)
        {
        case 0 : return osWinNT;
        }
        break;
    case 5 :    //Windows 2000, XP, 2003
        switch (m_OsVerInfoEx.dwMinorVersion)
        {
        case 0 : return osWin2k;
        case 1 : return osWinXP;
        case 2 : 
            if (VER_NT_WORKSTATION == m_OsVerInfoEx.wProductType && PROCESSOR_ARCHITECTURE_AMD64 == m_SystemInfo.wProcessorArchitecture)
            {
                //Windows XP Professional x64 Edition
                //MajorVersion = 5
                //MinorVersion = 2
                //ProductType = VER_NT_WORKSTATION
                //ProcessorArchitecture = PROCESSOR_ARCHITECTURE_AMD64
                return osWinXP;
            }
            else
            {
                return osWinServer2003;
            }
        }
        break;
    case 6 :    //Windows Vista, 2008, 7
        switch (m_OsVerInfoEx.dwMinorVersion)
        {
        case 0 :    //Windows Vista, 2008
            switch (m_OsVerInfoEx.wProductType)
            {
            case VER_NT_WORKSTATION : return osWinVista;
			case VER_NT_DOMAIN_CONTROLLER :	// msdn 페이지 설명으로보아 서버 2008
			case VER_NT_SERVER : return osWinServer2008;
            }
        case 1:
			switch (m_OsVerInfoEx.wProductType)
			{
			case VER_NT_WORKSTATION : return osWin7;
			case VER_NT_DOMAIN_CONTROLLER :	// msdn 페이지 설명으로보아 서버 2008 R2
			case VER_NT_SERVER : return osWinServer2008R2;
			}
		case 2:
			switch (m_OsVerInfoEx.wProductType)
			{
			case VER_NT_WORKSTATION : return osWin8;
			case VER_NT_DOMAIN_CONTROLLER :	// msdn 페이지 설명으로보아 서버 2012
			case VER_NT_SERVER : return osWinServer2012;
			}
		case 3:
			switch (m_OsVerInfoEx.wProductType)
			{
			case VER_NT_WORKSTATION : return osWin81;
			case VER_NT_DOMAIN_CONTROLLER :	// msdn 페이지 설명으로보아 서버 2012 R2
			case VER_NT_SERVER : return osWinServer2012R2;
			}
        }
        break;
	case 10:	//Windows 10, Windows Server 2016 Technical Preview
		switch (m_OsVerInfoEx.dwMinorVersion)
		{
		case 0 :	//Windows 10, Windows Server 2016 Technical Preview
			switch (m_OsVerInfoEx.wProductType)
			{
			case VER_NT_WORKSTATION : return osWin10;
			case VER_NT_DOMAIN_CONTROLLER :	// msdn 페이지 설명으로보아 서버 2016
			case VER_NT_SERVER : return osWinServer2016;
			}
		}
		break;
    }
    return osUnknown;
}


/**
 @brief     Windows Product를 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Windows Product
*/
CWinOsProduct CWinOsVersion::GetProduct() const
{
	CString strLog;
    CWinOsProduct Product = osUnknown;
    switch (GetPlatformId())
    {
    case VER_PLATFORM_WIN32s        : Product = GetWin32sProduct(); break;  //Win32s
    case VER_PLATFORM_WIN32_WINDOWS : Product = GetWin9xProduct(); break;   //Win9x
    case VER_PLATFORM_WIN32_NT      : Product= GetWinNTProduct(); break;    //WinNT
    }

    return Product;
}

/**
 @brief     Windows 아키텍처를 구한다.
 @author    odkwon
 @date      2007.10.03
 @return    Windows 아키텍처
*/
CWinOsArchitecture CWinOsVersion::GetArchitecture() const
{
    CWinOsArchitecture Architecture = oaUnknown;

    if (Is32bit()) 
        Architecture = oaX86;
    else if (Is64bit()) 
        Architecture = oaX64;
    
    return Architecture;
}

/**
 @brief     Windows Product Name을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Windows Product Name
*/
CString CWinOsVersion::GetProductName() const
{
    CString sProductName = _T("");

    switch (GetProduct())
    {
    case osUnknown         : sProductName = _T("Unknown Product"); break;
    case osWin95           : sProductName = _T("Windows 95"); break;
    case osWin98           : sProductName = _T("Windows 98"); break;
    case osWinMe           : sProductName = _T("Windows Me"); break;
    case osWin32s          : sProductName = _T("Win32s"); break;
    case osWinNT           : sProductName = _T("Windows NT"); break;
    case osWin2k           : sProductName = _T("Windows 2000"); break;
    case osWinXP           : sProductName = _T("Windows XP"); break;
    case osWinServer2003   : sProductName = _T("Windows Server 2003"); break;
    case osWinVista        : sProductName = _T("Windows Vista"); break;
    case osWinServer2008   : sProductName = _T("Windows Server 2008"); break;
    case osWin7            : sProductName = _T("Windows 7"); break;
    case osWin8            : sProductName = _T("Windows 8"); break;
    case osWinServer2008R2 : sProductName = _T("Windows Server 2008 R2"); break;
    case osWinServer2012   : sProductName = _T("WIndows Server 2012"); break;
    case osWinServer2012R2 : sProductName = _T("WIndows Server 2012 R2"); break;
    case osWin81           : sProductName = _T("Windows 8.1"); break;
    case osWinServer2016   : sProductName = _T("Windows Server 2016 Technical Preview"); break;
    case osWin10           : sProductName = _T("Windows 10"); break;
    }

    return sProductName;
}



/**
 @brief     Windows Service Pack을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Windows Service Pack
*/
CString CWinOsVersion::GetServicePack() const
{
    CString sServicePack = _T("");

    if (!IsWinNT()) return sServicePack;

    if (IsNT4SP6a())
        sServicePack = _T("Service Pack 6a");
    else
        sServicePack = m_OsVerInfoEx.szCSDVersion;
   
    return sServicePack;
}

/**
 @brief     Windows Service Pack Major값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Service Pack Major값
*/
WORD CWinOsVersion::GetServicePackMajor() const
{
    return m_OsVerInfoEx.wServicePackMajor;
}


/**
 @brief     Windows Service Pack Minor값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    Service Pack Minor값
*/
WORD CWinOsVersion::GetServicePackMinor() const
{
    return m_OsVerInfoEx.wServicePackMinor;
}

/**
 @brief     CPU의 Type를 구한다. 
 @author    Ray
 @date      2013,05.14
 @return    SYSTEM_INFO의 wProcessorArchitecture값
*/
WORD CWinOsVersion::GetProcessorType() const
{
    return  m_SystemInfo.wProcessorArchitecture;
}



/**
 @brief     Windows ProductId값을 구한다.
 @author    hang ryul lee
 @date      2007.09.03
 @return    ProductId값
*/
CString CWinOsVersion::GetProductId() const
{
    CString sProductId = _T("");
    CString sSubKey = _T("");

    if (IsWinNT())
        sSubKey = _T("Software\\Microsoft\\Windows NT\\CurrentVersion");
    else
        sSubKey = _T("Software\\Microsoft\\Windows\\CurrentVersion");

    HKEY hKey = NULL;
    TCHAR szProductId[REG_MAX_BUFSIZE] = {0, };
	DWORD dwBuffer = REG_MAX_BUFSIZE;
	LONG lResult;

    lResult = ::RegOpenKeyEx(HKEY_LOCAL_MACHINE, sSubKey, 0, KEY_QUERY_VALUE, &hKey);
	if(ERROR_SUCCESS != lResult)
		return _T("");

    lResult = ::RegQueryValueEx(hKey, _T("ProductId"), NULL, NULL, (LPBYTE)szProductId, &dwBuffer);
	if( (ERROR_SUCCESS != lResult) || (REG_MAX_BUFSIZE < dwBuffer) )
		return _T("");

    if (hKey)
        ::RegCloseKey(hKey);

    return CString(szProductId);
}

/**
 @brief     OS LangeID값을 구한다.
 @author    odkwon
 @date      2007.10.31
 @return    LangeID값
 @return    LangeID값 define은 WinNT.h -> Primary language IDs. 참조
*/
WORD CWinOsVersion::GetLangeId() const
{
    return LANGIDFROMLCID(::GetSystemDefaultLCID());
}







