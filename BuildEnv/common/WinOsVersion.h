

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      WinOsVersion.h 
 @brief     Windows OS Version 정보를 클래스화함.  
 @author    hang ryul lee
 @date      create 2007.09.03 

 @note      검증된 OS
            - Windows 98 SE                                     \n
            - Windows NT Workstation Service Pack 6             \n
            - Windows NT Server Service Pack 6                  \n
            - Windows 2000 Profesional Service Pack 4           \n
            - Windows 2000 Server Service Pack 4                \n
            - Windows XP Professional Service Pack 2            \n
            - Windows 2003 Enterprise Edition Service Pack 1    \n
            - Windows 2003 Standard x64 Edition Service Pack 1  \n
            - Windows Vista Ultimate Edition                    \n
			- Windows XP Tablet PC Edition 2005 Version 2002 Service Pack 2 \n
		      (IsTablet 함수 정상 동작함 확인)\n
            - Windows Vista Home Preminu K (Tablet PC)\n 
*/
#pragma once

#define  PRODUCT_PROFESSIONAL 0x00000030

typedef enum CWinOsProduct {osUnknown = 0,
                            osWin95,            //Windows 95
                            osWin98,            //Windows 98
                            osWinMe,            //Windows Me
                            osWin32s,           //Win32s
                            osWinNT,            //Windows NT(up to v4)
                            osWin2k,            //Windows 2000
                            osWinXP,            //Windows XP
                            osWinServer2003,    //Windows Server 2003
                            osWinVista,         //Windows Vista
                            osWinServer2008,    //Windows Server 2008
                            osWin7,             //Windows 7
                            osWin8,             //Windows 8
                            osWinServer2008R2,  //Windows Server 2008 R2
                            osWinServer2012,    //WIndows Server 2012
                            osWinServer2012R2,  //WIndows Server 2012 R2
                            osWin81,            //Windows 8.1
                            osWinServer2016,    //Windows Server 2016 Technical Preview
                            osWin10             //Windows 10
                            }CWinOsProduct;

//todo odkwon 현재 x64, x86만 지원함, 아이테니엄은 지원안함
typedef enum CWinOsArchitecture {oaUnknown = 0,
                                 oaX86,         //x86
                                 oaX64,         //x64
                                //oaItanium,    //Itanium
                                 }CWinOsArchitecture;

class CWinOsVersion
{
public:
    CWinOsVersion(void);
    virtual ~CWinOsVersion(void);

    DWORD GetBuildNumber() const;    
    CString GetDescription() const;
    CString GetEdition() const;
    bool IsWin9x() const;
    bool IsWinNT() const;
    bool IsWin32s() const;
    bool IsWow64() const;
    bool Is32bit() const;
    bool Is64bit() const;
    bool IsServer() const;
    bool IsTablet() const;
    DWORD GetMajorVersion() const;
    DWORD GetMinorVersion() const;

    DWORD GetPlatformId() const;
    CString GetPlatformName() const;
    CWinOsProduct GetProduct() const;
    CString GetProductName() const;
    CString GetProductId() const;

    CString GetServicePack() const;
    WORD GetServicePackMajor() const;
    WORD GetServicePackMinor() const;

    CWinOsArchitecture GetArchitecture() const;
    WORD GetLangeId() const;

	WORD GetProcessorType() const; 
private:
    BOOL m_bOsVerInfoExExist;
    OSVERSIONINFOEX m_OsVerInfoEx;
    SYSTEM_INFO m_SystemInfo;

    CString GetProductTypeFromReg() const;
    CWinOsProduct GetWin32sProduct() const;
    CWinOsProduct GetWin9xProduct() const;
    CWinOsProduct GetWinNTProduct() const;
    CString GetWin9xEdition() const;
    CString GetWinNTEdition() const;
    CString GetEditionFromReg() const;
    CString GetEditionFromProductInfo() const;
    BOOL CheckSuiteMask(WORD _wSuiteMask) const;
    bool IsNT4SP6a() const;
    bool IsMediaCenter() const;
    bool IsWinServer2003R2() const;

};
