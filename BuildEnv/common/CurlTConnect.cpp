#include "stdafx.h"
#include "CurlTConnect.h"
#include "UtilsFile.h"
#include "PathInfo.h"
#include <vector>
#include <string>


CCurlTConnect::CCurlTConnect(void)
{
	m_strTimeFromServer = _T("");
}

CCurlTConnect::~CCurlTConnect(void)
{
}


static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
	((std::string *)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

CStringA CCurlTConnect::Utf8_Encode(CStringW strData)
{
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed + 1, 0);
	WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), &strTo[0], size_needed, NULL, NULL);
	return strTo.c_str();
}

CStringW CCurlTConnect::Utf8_Decode(CStringA strData)
{
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, strData.GetString(), -1, NULL, 0);
	std::wstring wstrTo(size_needed + 1, 0);
	wstrTo.clear();
	MultiByteToWideChar(CP_UTF8, 0, strData.GetString(), -1, &wstrTo[0], size_needed + 1);
	return wstrTo.c_str();
}

CString CCurlTConnect::CurlDelete(CString _strParam, CString _strUrl, CString _strToken)
{
	BOOL bResult = FALSE;
	std::string readBuffer;
	readBuffer.clear();
	CURL *curl;
	CURLcode res;
	//html_context_data data = {0, 0};

	CStringA strUrlA = Utf8_Encode(_strUrl);
	CStringA strReadBufferA;
	CString strReadBufferW = _T("");

	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();

	if (curl)
	{
		CString strTokenHeader = _T("");
		curl_slist* header = NULL ;
		strTokenHeader.Format(_T("Authorization: Bearer %s"), _strToken);
		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
		CStringA strDataA = Utf8_Encode(_strParam);
		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
		header = curl_slist_append( header ,  strTokenHeaderA);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strDataA);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		//curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

		/* Now run off and do what you've been told! */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if (readBuffer.size() > 0)
		{
			strReadBufferA = readBuffer.c_str();
			if (strReadBufferA.GetLength() > 0) {
				strReadBufferW = Utf8_Decode(strReadBufferA);
			}
			//SetToken((CString)cs);
		}
		if(res != CURLE_OK) {
			m_LastErrorCode = res;
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_deletefail"));
			strCount.Format(_T("[CurlDelete]URL: %s, Param: %s res: %d [line: %d, function: %s, file: %s]"), _strUrl, _strParam, res, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_deletefail"), strCount);
			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
		} else {
			m_LastErrorCode = res;
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_deletesuccess"));
			strCount.Format(_T("[CurlDelete]URL: %s, Param: %s, Return: %s [line: %d, function: %s, file: %s]"), _strUrl, _strParam, strReadBufferW, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_deletesuccess"), strCount);
		}
		curl_slist_free_all(header) ;
		curl_easy_cleanup(curl);
	}
	curl_global_cleanup();
	return strReadBufferW;
}

CString CCurlTConnect::CurlGET(CString _strUrl, CString _strToken)
{
	BOOL bResult = FALSE;
	std::string readBuffer;
	readBuffer.clear();
	CURL *curl;
	CURLcode res;
	//html_context_data data = {0, 0};

	CStringA strUrlA = Utf8_Encode(_strUrl);
	CStringA strReadBufferA;
	CString strReadBufferW = _T("");

	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();

	if(curl)
	{
		CString strTokenHeader = _T("");
		curl_slist* header = NULL ;
		strTokenHeader.Format(_T("Authorization: Bearer %s"), _strToken);
		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
		header = curl_slist_append( header ,  strTokenHeaderA);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		//curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

		/* Now run off and do what you've been told! */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if (readBuffer.size() > 0)
		{
			strReadBufferA = readBuffer.c_str();
			strReadBufferW = Utf8_Decode(strReadBufferA);
			//SetToken((CString)cs);
		}
		if(res != CURLE_OK) {
			m_LastErrorCode = res;
			//CString strCountFile = _T("");
			CString strCount = _T("");
			DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_getfail"));
			strCount.Format(_T("[CurlGET]%s res: %d [line: %d, function: %s, file: %s]"), _strUrl, res, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_getfail"), strCount);
			UM_WRITE_LOG(strCount);
			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
		} else {
			m_LastErrorCode = res;
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_getsuccess"));
			strCount.Format(_T("[CurlGET]URL: %s, Return: %s [line: %d, function: %s, file: %s]"), _strUrl, strReadBufferW, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_getsuccess"), strCount);
			UM_WRITE_LOG(strCount);
		}
		curl_slist_free_all(header) ;
		curl_easy_cleanup(curl);
	}
	curl_global_cleanup();
	return strReadBufferW;
}

CString CCurlTConnect::CurlPost(CString _strParam, CString _strUrl, CString _strToken)
{
	BOOL bResult = FALSE;
	std::string readBuffer;
	readBuffer.clear();
	CURL *curl;
	CURLcode res;
	//html_context_data data = {0, 0};

	CStringA strUrlA = Utf8_Encode(_strUrl);
	CStringA strReadBufferA;
	CString strReadBufferW = _T("");

	curl_global_init(CURL_GLOBAL_ALL);
	curl = curl_easy_init();

    if (curl)
	{
		CString strTokenHeader = _T("");
		curl_slist* header = NULL ;
		strTokenHeader.Format(_T("Authorization: Bearer %s"), _strToken);
		CStringA strTokenHeaderA = Utf8_Encode(strTokenHeader);
		CStringA strDataA = Utf8_Encode(_strParam);
		header = curl_slist_append( header , "Content-Type: application/x-www-form-urlencoded" ) ;
		header = curl_slist_append( header ,  strTokenHeaderA);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header) ;
		curl_easy_setopt(curl, CURLOPT_URL, strUrlA);
		curl_easy_setopt(curl, CURLOPT_POST, 1);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strDataA);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, CURL_REQUEST_TIMEOUT);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
		//curl_easy_setopt(curl, CURLOPT_PROXYPORT, "443");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

		/* Now run off and do what you've been told! */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if (readBuffer.size() > 0)
		{
			strReadBufferA = readBuffer.c_str();
			if (strReadBufferA.GetLength() > 0) {
				strReadBufferW = Utf8_Decode(strReadBufferA);
			}
			//SetToken((CString)cs);
		}
		if(res != CURLE_OK) {
			m_LastErrorCode = res;
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_postfail"));
			strCount.Format(_T("[CurlPost]URL: %s, res: %d [line: %d, function: %s, file: %s]"), _strUrl, res, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_postfail"), strCount);
			UM_WRITE_LOG(strCount);
			//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
		} else {
			m_LastErrorCode = res;
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_postsuccess"));
			strCount.Format(_T("[CurlPost]URL: %s, StrLen: %d, Return: %s [line: %d, function: %s, file: %s]"), _strUrl, _strParam.GetLength(), strReadBufferW, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strCount);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_postsuccess"), strCount);
		}
		curl_slist_free_all(header) ;
		curl_easy_cleanup(curl);
	}
	curl_global_cleanup();
	return strReadBufferW;
}

DWORD CCurlTConnect::GetCurlLastError() {
	return m_LastErrorCode;
}