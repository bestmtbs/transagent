
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      ImpersonatorConst.h 
 @brief     Impersonator Class에서 사용하는 구조체 정의 

            사용 함수 이름 정의 

 @author    hang ryul lee 
 @date      create 2007.11.30 
*/
#pragma once

//Lmcons.h 에 정의되어 있음
#define UNLEN       256                 // 사용자 이름 MAX size

//user profile
#if _MSC_VER <= 1400        // VC2005 이하
typedef struct _PROFILEINFO {  
    DWORD dwSize;  DWORD dwFlags;  
    LPTSTR lpUserName;  
    LPTSTR lpProfilePath;  
    LPTSTR lpDefaultPath;  
    LPTSTR lpServerName;  
    LPTSTR lpPolicyPath;  
    HANDLE hProfile;
} PROFILEINFO, *LPPROFILEINFO;
#else
#include <profinfo.h>
#endif

//snap shot 에 이용
//UNICODE : PROCESSENTRY32W
//ANSICODE : PROCESSENTRY32
typedef struct tagPROCESSENTRY32W
{
    DWORD   dwSize;
    DWORD   cntUsage;
    DWORD   th32ProcessID;          // this process
    ULONG_PTR th32DefaultHeapID;
    DWORD   th32ModuleID;           // associated exe
    DWORD   cntThreads;
    DWORD   th32ParentProcessID;    // this process's parent process
    LONG    pcPriClassBase;         // Base priority of process's threads
    DWORD   dwFlags;
    WCHAR   szExeFile[MAX_PATH];    // Path
} PROCESSENTRY32W;
typedef PROCESSENTRY32W *  PPROCESSENTRY32W;
typedef PROCESSENTRY32W *  LPPROCESSENTRY32W;

typedef struct tagPROCESSENTRY32
{
    DWORD   dwSize;
    DWORD   cntUsage;
    DWORD   th32ProcessID;          // this process
    ULONG_PTR th32DefaultHeapID;
    DWORD   th32ModuleID;           // associated exe
    DWORD   cntThreads;
    DWORD   th32ParentProcessID;    // this process's parent process
    LONG    pcPriClassBase;         // Base priority of process's threads
    DWORD   dwFlags;
    CHAR    szExeFile[MAX_PATH];    // Path
} PROCESSENTRY32;
typedef PROCESSENTRY32 *  PPROCESSENTRY32;
typedef PROCESSENTRY32 *  LPPROCESSENTRY32;


//function name 정의
#ifdef UNICODE 
    #define GETMODULEBASENAME "GetModuleBaseNameW"
    #define LOADUSERPROFILE  "LoadUserProfileW"
    #define GETPROFILESDIR  "GetProfilesDirectoryW"
    #define PROCESS32FIRST  "Process32FirstW"
    #define PROCESS32NEXT  "Process32NextW"

    #define PROCESSENTRY32  PROCESSENTRY32W
    #define PPROCESSENTRY32  PPROCESSENTRY32W
    #define LPPROCESSENTRY32 LPPROCESSENTRY32W
#else
    #define GETMODULEBASENAME "GetModuleBaseNameA"
    #define LOADUSERPROFILE  "LoadUserProfileA"
    #define GETPROFILESDIR  "GetProfilesDirectoryA"
    #define PROCESS32FIRST  "Process32First"
    #define PROCESS32NEXT  "Process32Next"
#endif

HANDLE
WINAPI
CreateToolhelp32Snapshot(
						 DWORD dwFlags,
						 DWORD th32ProcessID
						 );

// CreateToolhelp32Snapshot dwFlags
#define TH32CS_SNAPHEAPLIST 0x00000001
#define TH32CS_SNAPPROCESS  0x00000002
#define TH32CS_SNAPTHREAD   0x00000004
#define TH32CS_SNAPMODULE   0x00000008
#define TH32CS_SNAPMODULE32 0x00000010
#define TH32CS_SNAPALL      (TH32CS_SNAPHEAPLIST | TH32CS_SNAPPROCESS | TH32CS_SNAPTHREAD | TH32CS_SNAPMODULE)
#define TH32CS_INHERIT      0x80000000