/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      Auth.h
 @brief     CProcessAuth / CSession class 정의 파일
 
            권한 검사 관련 기능을 한다.

 @author    hang ryul lee 
 @date      create 2011.05.29
*/
#pragma once

#ifdef UNICODE
#define GetModuleBaseNames       "GetModuleBaseNameW"
#else
#define GetModuleBaseNames       "GetModuleBaseNameA"
#endif

typedef HRESULT (WINAPI *fpNetApiBufferFree)(LPVOID);
typedef HRESULT (WINAPI *fpNetUserGetInfo)(LPCWSTR, LPCWSTR, DWORD, LPBYTE*);
typedef BOOL  (WINAPI* fpEnumProcesses)(DWORD*, DWORD, DWORD*);
typedef DWORD (WINAPI* fpGetModuleBaseName)(HANDLE, HMODULE, LPTSTR, DWORD);

/**
 @class     CProcessAuth  
 @brief     현재 실행중인 Process 권한 구함.
 @author    hang ryul lee 
 @date      create 2011.05.29
 @note      - 현재 프로세스의 실행권한에 대한 검사를 한다.\n
            - Windows 9x 의 경우 권한 개념이 없으므로 무조건 True 반환. (항상 권한 있음)
*/
class CProcessAuth
{
public:
    static bool IsAdmin();
    static bool IsAdminByUserName();
    static bool IsSystem();
    static bool IsExcuteAuth();
private:
    static bool IsAdminGroup(const CString &_sServer, const CString &_sUser);
};

/**
 @class     CSessionAuth 
 @brief     현재 활성화 세션에 대한 권한 구함.
 @author    hang ryul lee 
 @date      create 2011.05.29
 @note      - 현재 활성화 세션의 유저 권한에 대한 검사를 한다.\n
            - Windows 9x 의 경우 권한 개념이 없으므로 무조건 True 반환. (항상 권한 있음)
*/
class CSessionAuth
{
public:
    static bool IsAdmin();
    static bool IsSystem();
private:
    static CString GetShellName();
    static DWORD   GetProcSessionID(const DWORD _dwProcessId);
};