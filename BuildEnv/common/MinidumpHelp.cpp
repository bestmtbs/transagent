#include "StdAfx.h"
#include "MinidumpHelp.h"
#include <time.h>
#include <DbgHelp.h>


#pragma comment(lib,"DbgHelp.Lib")

//hhh(2013.06.28) stackover 인경우 덤프생성을 못하므로 쓰레드에서 따로 덤프를 생성할 수 있도록 한다.
DWORD WINAPI CrashDumpProcThread(void* param)
{
	PEXCEPTION_POINTERS pinfo = (PEXCEPTION_POINTERS)param;
//	pMinidumpHelp->WrtiteDumpInfo();

	MinidumpHelp mini;
		MINIDUMP_EXCEPTION_INFORMATION MinidumpExceptionInformation;
		std::wstring dump_filename;

		MinidumpExceptionInformation.ThreadId = ::GetCurrentThreadId();
		MinidumpExceptionInformation.ExceptionPointers = pinfo;
		MinidumpExceptionInformation.ClientPointers = FALSE;


		dump_filename = mini.get_dump_filename();
		if (dump_filename.empty() == true)
		{
			::TerminateProcess(::GetCurrentProcess(), 0);
		}

		HANDLE hDumpFile = ::CreateFileW(dump_filename.c_str(),
			GENERIC_WRITE, 
			FILE_SHARE_WRITE, 
			NULL, 
			CREATE_ALWAYS,
			FILE_ATTRIBUTE_NORMAL, NULL);

		MiniDumpWriteDump(GetCurrentProcess(),
			GetCurrentProcessId(),
			hDumpFile,
			MiniDumpNormal,
			&MinidumpExceptionInformation,
			NULL,
			NULL);
		::TerminateProcess(::GetCurrentProcess(), 0);
	return 0;	
}

MinidumpHelp::MinidumpHelp(void)
{
}

MinidumpHelp::~MinidumpHelp(void)
{
}

std::string MinidumpHelp::format_arg_list(const char *fmt, va_list args)
{
	if (!fmt) return "";
	int   result = -1, length = 256;
	char *buffer = 0;
	while (result == -1)
	{
		if (buffer) delete [] buffer;
		buffer = new char [length + 1];
		memset(buffer, 0, length + 1);
		result = _vsnprintf_s(buffer, length, _TRUNCATE, fmt, args);
		length *= 2;
	}
	std::string s(buffer);
	delete [] buffer;
	return s;
}

std::string MinidumpHelp::format_string(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	std::string s = format_arg_list(fmt, args);
	va_end(args);

	return s;
}

std::wstring MinidumpHelp::s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = ::MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	::MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}

std::wstring MinidumpHelp::get_dump_filename()
{
	time_t rawtime;
	struct tm timeinfo;

	std::string date_string;
	std::wstring date_wstring;

	std::wstring module_path;
	std::wstring dump_filename;

	static WCHAR ModulePath[1024];

	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	date_string = format_string("%d.%02d.%02d", 
		timeinfo.tm_year + 1900,
		timeinfo.tm_mon + 1,
		timeinfo.tm_mday
		/*
		timeinfo.tm_hour,
		timeinfo.tm_min,
		timeinfo.tm_sec
		*/);
	date_wstring = s2ws(date_string);

	if (::GetModuleFileNameW(0, ModulePath, sizeof(ModulePath) /sizeof(WCHAR)) == 0)
	{
		return std::wstring();
	}

	module_path = ModulePath;
	int nFindPos = module_path.rfind(L"\\");
	int nTotalLen = module_path.length();
	
	dump_filename.assign(module_path, nFindPos + 1, nTotalLen - nFindPos - 5);	//hhh(2013.07.30) : 파일명만 가져오도록 처리
//	dump_filename.assign(module_path, 0, module_path.rfind(L"\\") + 1);

	dump_filename = dump_filename + date_wstring + L".dmp";

	return dump_filename;
}

LONG WINAPI my_top_level_filter(__in PEXCEPTION_POINTERS pExceptionPointer)
{

	if(pExceptionPointer->ExceptionRecord->ExceptionCode == EXCEPTION_STACK_OVERFLOW)
    {
		//::MessageBox(NULL,_T("TTT"), L"Cap", MB_OK); 
		HANDLE hThread = ::CreateThread(NULL, 1024000, CrashDumpProcThread, (void*)pExceptionPointer, 0, NULL);		//스택사이즈는 2mb
        WaitForSingleObject(hThread, INFINITE);
        CloseHandle(hThread);
		::TerminateProcess(::GetCurrentProcess(), 0);
	}
	else
	{
		//WrtiteDumpInfo(pExceptionPointer);
	
		MinidumpHelp mini;
		MINIDUMP_EXCEPTION_INFORMATION MinidumpExceptionInformation;
		std::wstring dump_filename;

		MinidumpExceptionInformation.ThreadId = ::GetCurrentThreadId();
		MinidumpExceptionInformation.ExceptionPointers = pExceptionPointer;
		MinidumpExceptionInformation.ClientPointers = FALSE;


		dump_filename = mini.get_dump_filename();
		if (dump_filename.empty() == true)
		{
			::TerminateProcess(::GetCurrentProcess(), 0);
		}

		HANDLE hDumpFile = ::CreateFileW(dump_filename.c_str(),
			GENERIC_WRITE, 
			FILE_SHARE_WRITE, 
			NULL, 
			CREATE_ALWAYS,
			FILE_ATTRIBUTE_NORMAL, NULL);

		MiniDumpWriteDump(GetCurrentProcess(),
			GetCurrentProcessId(),
			hDumpFile,
			MiniDumpNormal,
			&MinidumpExceptionInformation,
			NULL,
			NULL);
		::TerminateProcess(::GetCurrentProcess(), 0);
	}



	return 0;
}

void MinidumpHelp::install_self_mini_dump()
{
	SetUnhandledExceptionFilter(my_top_level_filter);
}

/*
void MinidumpHelp::WrtiteDumpInfo(PEXCEPTION_POINTERS pIfno)
{
	MinidumpHelp mini;
	MINIDUMP_EXCEPTION_INFORMATION MinidumpExceptionInformation;
	std::wstring dump_filename;

	MinidumpExceptionInformation.ThreadId = ::GetCurrentThreadId();
	MinidumpExceptionInformation.ExceptionPointers = pIfno;
	MinidumpExceptionInformation.ClientPointers = FALSE;


	dump_filename = mini.get_dump_filename();
	if (dump_filename.empty() == true)
	{
		::TerminateProcess(::GetCurrentProcess(), 0);
	}

	HANDLE hDumpFile = ::CreateFileW(dump_filename.c_str(),
		GENERIC_WRITE, 
		FILE_SHARE_WRITE, 
		NULL, 
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL, NULL);

	MiniDumpWriteDump(GetCurrentProcess(),
		GetCurrentProcessId(),
		hDumpFile,
		MiniDumpNormal,
		&MinidumpExceptionInformation,
		NULL,
		NULL
		);
	::TerminateProcess(::GetCurrentProcess(), 0);
}*/
/*
UINT WINAPI MinidumpHelp::CrashDuupThreadProc(LPVOID lpParam)
{
	//2. 해당 인스턴스의 실제 thread 용 함수 호출
	CTest* Test = (CTest*)lpParam;
	return Test->RealThreadProc();
}*/