/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2012.05.24	
********************************************************************************/

/**
@file      CATORPipeServer.cpp
@brief    Pipe 정의 클래스

@author   jhlee
@date      create 2012.05.24	
@note      
*/
#include "StdAfx.h"
#include "Impersonator.h"
#include "ATORPipeServer.h"
#include "../../TiorSaverReload/TiorSaverReload.h"
#include "PathInfo.h"
#include "UtilsFile.h"
/**
@brief     생성자
@author    jhlee
@date      2012.05.24	
*/
extern CTiorSaverReloadApp theApp;
CATORPipeServer::CATORPipeServer(void)
{

}

/**
@brief     생성자
@author    jhlee
@date      2012.05.24	
*/
CATORPipeServer::CATORPipeServer(TCHAR* _pszPipeName) : CServerPipe(_pszPipeName)
{

}

/**
@brief      소멸자
@author    jhlee
@date      2012.05.24	
*/
CATORPipeServer::~CATORPipeServer(void)
{
}
/**
@brief      가상함수 OnReceiveData()  정의
@author    JHLEE
@date      2012.05.24	
*/
BOOL CATORPipeServer::OnReceiveData()
{
	
	BOOL bRes = FALSE;
	DWORD nReadLen = 0, nLastErr = 0;
	int nLen = sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) +  (4096 *2);
	TCHAR szBuf[sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) +  (4096 *2)]={0,};

	memset( szBuf, 0x00, sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) +  (4096 *2));

	bRes = ReadFile(m_hPipe, szBuf,nLen, &nReadLen, NULL);

	nLastErr = GetLastError();

	if( (bRes == FALSE) && (nLastErr != ERROR_MORE_DATA) )
		return FALSE;


	SHARE_DATA*	pShareData = reinterpret_cast<SHARE_DATA*>(szBuf);

	//update 프로세스로 부터 업데이트가 완료되었고 변경된 모듈을 바꿔라는 의미
	if(pShareData->dwAct == ACT_KILL_PROCESS)
	{
		BOOL bResult = FALSE;
		if (FileExists(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_RELOAD_NAME)) {
			theApp.ChangeFileName(WTIOR_AGENT_RELOAD_NAME);
		}
		if (TRUE == theApp.KillTiorProcess(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_RELOAD_NAME)) {
			if (theApp.DeleteService()) {
				bResult = TRUE;
			}
		}
		if (!bResult) {
			theApp.KillTiorProcess(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_RELOAD_NAME);
			theApp.KillTiorProcess(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_SERVICE_NAME);
		}
	}
	if (pShareData->dwAct == ACT_KILL_COLLECT_AGENT) {
		CString strSysAgent = _T("");
		CString strUsrAgent = _T("");
		if (theApp.m_bIs64bit) {
			strSysAgent = CPathInfo::GetClientInstallPath()+WTIOR_SYS_AGENT_64_NAME;
			strUsrAgent = CPathInfo::GetClientInstallPath()+WTIOR_USR_AGENT_64_NAME;
		} else {
			strSysAgent = CPathInfo::GetClientInstallPath()+WTIOR_SYS_AGENT_32_NAME;
			strUsrAgent = CPathInfo::GetClientInstallPath()+WTIOR_USR_AGENT_32_NAME;
		}
		theApp.KillTiorProcess(strSysAgent);
		theApp.KillTiorProcess(strUsrAgent);
		
	}
	return TRUE;
}

/**
@brief      가상함수 OnSendData()  정의
@author    JHLEE
@date      2012.05.24	
*/
BOOL CATORPipeServer::OnSendData()
{

	TCHAR zOutBuf[4096] ={0,};
	DWORD nWriteLen = 0;
	BOOL bRes = FALSE;

	//_tcscpy_s(zOutBuf,sizeof(zOutBuf), SE_RETURN_OK);
	_tcscpy_s(zOutBuf,4096, SE_RETURN_OK);

	bRes = WriteFile( m_hPipe, zOutBuf, ( _tcslen( zOutBuf) * sizeof( TCHAR)), &nWriteLen, NULL);

	if( bRes == FALSE) 
	{
		//TODO
		// 로그 

		return bRes;

	}
	return bRes;
}

/**
@brief      가상함수 OnSetData()  정의
@author   JHLEE
@date      2012.05.24	
*/
BOOL CATORPipeServer::OnSetData(void* pData)
{

	return TRUE;
}

/**
@brief      가상함수 OnReceiveHeader()  정의
@author    hang ryul lee
@date      2012.05.24	
*/
BOOL CATORPipeServer::OnReceiveHeader()
{

	BOOL bRes = FALSE;


	return TRUE;
}


/**
@brief      가상함수 OnReceiveBody()  정의
@author    hang ryul lee
@date      2012.05.24	
*/
BOOL CATORPipeServer::OnReceiveBody()
{
	BOOL bRes = FALSE;


	return TRUE;
}
