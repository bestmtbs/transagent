/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2012.05.24	
********************************************************************************/

/**
@file      ATORPipeServer.h 
@brief    ATORPipeServer 정의 클래스

@author    jhlee
@date      create 2012.05.24
@note      
*/
#pragma once
#include "ServerPipe.h"

class CATORPipeServer :public CServerPipe
{
public:
	CATORPipeServer(void);
	CATORPipeServer(TCHAR* _pszPipeName);
	virtual ~CATORPipeServer(void);


public:
	virtual BOOL OnReceiveData();
	virtual BOOL OnSendData();
	virtual BOOL OnSetData(void * pData);
	virtual BOOL OnReceiveHeader();
	virtual BOOL OnReceiveBody();

private:
	CATORPipeServer(const CATORPipeServer &_rPipeServer);										 /**< 복사 생성자 */
	CATORPipeServer& operator = (const CATORPipeServer &_rPipeServer);					/**< 대입 연산자  */

};
