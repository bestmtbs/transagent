
/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2012.05.24		
********************************************************************************/

/**
@file      ATORPipeClient.cpp
@brief    Pipe 정의 클래스

@author   jhlee
@date      create 2012.05.24
@note      
*/

#include "StdAfx.h"
#include "ATORPipeClient.h"


/**
@brief     생성자
@author    jhlee
@date      2012.05.24
*/
CATORPipeClient::CATORPipeClient(void)
{
}

/**
@brief     생성자
@author    jhlee
@date      2012.05.24
*/
CATORPipeClient::CATORPipeClient(TCHAR* _pszPipeName)  : CClientPipe(_pszPipeName)
{

}


/**
@brief      소멸자
@author    jhlee
@date      2012.05.24
*/
CATORPipeClient::~CATORPipeClient(void)
{
}
/**
@brief     가상함수 OnSetSendData()  정의
@author   JHLEE
@date      2012.05.24
*/
BOOL CATORPipeClient::OnSetSendData(void* pData)
{
	if(pData == NULL)
		return FALSE;

	m_pShareData = reinterpret_cast<SHARE_DATA*>(pData);

	return TRUE;
}


/**
@brief      가상함수 OnReceiveData()  정의
@author    JHLEE
@date      2012.05.24
*/
BOOL CATORPipeClient::OnReceiveData()
{
	BOOL bRes = FALSE;
	TCHAR zInBuf[4096] ={0,};
	DWORD nReadLen = 0;
	DWORD nLastErr = 0;

	bRes = ReadFile( m_hPipe, zInBuf, 4096 * sizeof( TCHAR), &nReadLen, NULL);

	nLastErr = GetLastError( );

	if( ( bRes == FALSE) && ( nLastErr != ERROR_MORE_DATA))
	{
		SetFunctionErrorCode(GetLastError());
		return FALSE;
	}

	if(_tcscmp(zInBuf,SE_RETURN_ERR) == 0 || _tcscmp(zInBuf, SE_RETURN_NULL) == 0)
	{
		SetFunctionErrorCode(SE_RETURN_ERROR_CODE);
		return FALSE;
	}


	return TRUE;
}

/**
@brief      가상함수 OnSendData()  정의
@author    JHLEE
@date      2012.05.24
*/
BOOL CATORPipeClient::OnSendData()
{

	BOOL bRes = FALSE;
	DWORD nWriteLen = 0;


	/******************************* 보낼 데이타 조작*************************************************/
	int nLen =sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) +  (4096 *2);

	bRes = WriteFile( m_hPipe, m_pShareData, nLen, &nWriteLen, NULL);

	if(bRes == FALSE)
	{
		SetFunctionErrorCode(GetLastError());
		return bRes;
	}	

	return bRes;

}


BOOL CATORPipeClient::OnSendBody()
{

	BOOL bRes = FALSE;

	return bRes;

}


/**
@brief      가상함수 OnSendHeader()  정의
@author    hang ryul lee
@date      2012.05.24
*/
BOOL CATORPipeClient::OnSendHeader()
{
	BOOL bRes = FALSE;

	return bRes;

}

BOOL CATORPipeClient::OnSetHeader(PIPE_HEADER* _pPipeHeader)
{

	return TRUE;

}