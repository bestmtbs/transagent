/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2012.05.24	
********************************************************************************/

/**
@file      PTOAPipeServer.h 
@brief    PTOAPipeServer 정의 클래스

@author    jhlee
@date      create 2012.05.24
@note      
*/
#pragma once
#include "ServerPipe.h"

class CPTOAPipeServer :public CServerPipe
{
public:
	CPTOAPipeServer(void);
	CPTOAPipeServer(TCHAR* _pszPipeName);
	virtual ~CPTOAPipeServer(void);


public:

	virtual BOOL OnReceiveData();
	virtual BOOL OnSendData();
	virtual BOOL OnSetData(void * pData);
	virtual BOOL OnReceiveHeader();
	virtual BOOL OnReceiveBody();

private:
	CPTOAPipeServer(const CPTOAPipeServer &_rPipeServer);										 /**< 복사 생성자 */
	CPTOAPipeServer& operator = (const CPTOAPipeServer &_rPipeServer);					/**< 대입 연산자  */

};
