/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2012.05.24	
********************************************************************************/

/**
@file      CPTOAPipeServer.cpp
@brief    Pipe 정의 클래스

@author   jhlee
@date      create 2012.05.24	
@note      
*/
#include "StdAfx.h"
#include "Impersonator.h"
#include "PTOAPipeServer.h"
#include "../../TiorSaverTransfer/TiorSaverTransfer.h"
/**
@brief     생성자
@author    jhlee
@date      2012.05.24	
*/
extern CTiorSaverTransferApp theApp;
CPTOAPipeServer::CPTOAPipeServer(void)
{

}

/**
@brief     생성자
@author    jhlee
@date      2012.05.24	
*/
CPTOAPipeServer::CPTOAPipeServer(TCHAR* _pszPipeName) : CServerPipe(_pszPipeName)
{

}

/**
@brief      소멸자
@author    jhlee
@date      2012.05.24	
*/
CPTOAPipeServer::~CPTOAPipeServer(void)
{
}
/**
@brief      가상함수 OnReceiveData()  정의
@author    JHLEE
@date      2012.05.24	
*/
BOOL CPTOAPipeServer::OnReceiveData()
{
	
	BOOL bRes = FALSE;
	DWORD nReadLen = 0, nLastErr = 0;
	/*	
	LONG           dwAct;                                                   // 타입 정의
	LONG           dwMsg;                                                 // 메시지 정의
	DWORD          dwBufferSize;                                        // 데이타 싸이즈
	TCHAR          szBuffer[4096*2];
	*/
	int nLen = sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) +  (4096 *2);
	TCHAR* pszBuf = new TCHAR[sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) + (4096 * 2) + 1];

	ZeroMemory(pszBuf, sizeof(TCHAR) * (sizeof(LONG_PTR) + sizeof(LONG_PTR) + sizeof(DWORD) + (4096 * 2) + 1));

	bRes = ReadFile(m_hPipe, pszBuf,nLen, &nReadLen, NULL);

	nLastErr = GetLastError();

	if ((bRes == FALSE) && (nLastErr != ERROR_MORE_DATA)) {
		SE_MemoryDelete(pszBuf);
		return FALSE;
	}

	SHARE_DATA*	pShareData = reinterpret_cast<SHARE_DATA*>(pszBuf);

	//update 프로세스로 부터 업데이트가 완료되었고 변경된 모듈을 바꿔라는 의미
	if(pShareData->dwAct == ACT_UPDATE_COMPLETE_RESTART)
	{
		UM_WRITE_LOG(L"[OfsAgent] ACT_UPDATE_COMPLETE_RESTART");
		if(theApp)
		{
			CString strLog = _T("");
			CString strBuffer = _T("");
			theApp.m_nRestartProcessType = pShareData->dwMsg;
			strBuffer.Format(_T("%s"), pShareData->szBuffer);
			//theApp.m_strNewVersion = strBuffer;

			strLog.Format(L"[OfsAgent] ACT_UPDATE_COMPLETE_RESTART - Recv Msg: %d", theApp.m_nRestartProcessType );
			UM_WRITE_LOG(strLog);

			theApp.m_pWnd->PostMessage(MSG_UPDATE_COMPLETE_RESTART, NULL, NULL);
		}		
	}
	if(pShareData->dwAct == ACT_AGENT_UPDATE_FAIL)
	{
	}

	SE_MemoryDelete(pszBuf);
	return TRUE;
}
/**
@brief      가상함수 OnSendData()  정의
@author    JHLEE
@date      2012.05.24	
*/
BOOL CPTOAPipeServer::OnSendData()
{

	TCHAR zOutBuf[4096] ={0,};
	DWORD nWriteLen = 0;
	BOOL bRes = FALSE;

	//_tcscpy_s(zOutBuf,sizeof(zOutBuf), SE_RETURN_OK);
	_tcscpy_s(zOutBuf,4096, SE_RETURN_OK);

	bRes = WriteFile( m_hPipe, zOutBuf, ( _tcslen( zOutBuf) * sizeof( TCHAR)), &nWriteLen, NULL);

	if( bRes == FALSE) 
	{
		//TODO
		// 로그 

		return bRes;

	}
	return bRes;
}

/**
@brief      가상함수 OnSetData()  정의
@author   JHLEE
@date      2012.05.24	
*/
BOOL CPTOAPipeServer::OnSetData(void* pData)
{

	return TRUE;
}

/**
@brief      가상함수 OnReceiveHeader()  정의
@author    hang ryul lee
@date      2012.05.24	
*/
BOOL CPTOAPipeServer::OnReceiveHeader()
{

	BOOL bRes = FALSE;


	return TRUE;
}


/**
@brief      가상함수 OnReceiveBody()  정의
@author    hang ryul lee
@date      2012.05.24	
*/
BOOL CPTOAPipeServer::OnReceiveBody()
{
	BOOL bRes = FALSE;


	return TRUE;
}
