/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2012.05.24		
********************************************************************************/

/**
@file      PTOAPipeClient .h 
@brief    PTOAPipeClient  정의 클래스

@author    jhlee
@date      create 2012.05.24
@note      
*/

#pragma once
//#include "../../../General/ClientPipe.h"
//#include "../../../General/pipe.h"
#include "ClientPipe.h"
#include "pipe.h"

class CPTOAPipeClient :public CClientPipe
{
public:
	CPTOAPipeClient(void);
	CPTOAPipeClient(TCHAR* _pszPipeName);	
	virtual ~CPTOAPipeClient(void);

public:

	SHARE_DATA * m_pShareData;

	virtual BOOL  OnSetSendData(void* pData);
	virtual BOOL OnReceiveData();
	virtual BOOL OnSendData();

	virtual BOOL  OnSetHeader(PIPE_HEADER* _pPipeHeader);
	virtual BOOL  OnSendBody();
	virtual BOOL  OnSendHeader();


private:
	CPTOAPipeClient(const CPTOAPipeClient &_rPipeClient);										 /**< 복사 생성자 */
	CPTOAPipeClient& operator = (const CPTOAPipeClient &_rPipeClient);					/**< 대입 연산자  */


};
