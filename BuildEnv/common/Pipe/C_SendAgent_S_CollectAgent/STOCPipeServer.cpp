
#include "stdafx.h"
#include "STOCPipeServer.h"

CSTOCPipeServer::CSTOCPipeServer(void)
{
}

CSTOCPipeServer::CSTOCPipeServer(TCHAR* _pszPipeName) : CServerPipe(_pszPipeName)
{
}

CSTOCPipeServer::~CSTOCPipeServer()
{
}

BOOL CSTOCPipeServer::OnReceiveData()
{
	BOOL bRes = FALSE;
	DWORD nReadLen = 0;
	DWORD nLastErr = 0;
	TCHAR* pzInBuf = new TCHAR[sizeof(SHARE_DATA_WITH_COLLECT_AGENT)];
	
	/*	
	DWORD		dwAct;
	BOOL		bState;
	TCHAR		szCollectType[12*2];	//	"FL"|"URL"
	DWORD		dwCollectDataSize;	//	szCollectType의 길이
	TCHAR		szDomain[4096*2];	//	"naver.com"|"google.com"
	DWORD		dwDomainSize;	//	szDomain의 길이
	DWORD64		dw64TimeOnServer;	// timestamp: yyyymmddhhmmssccc 최초 실행 후에는 받지 않으므로 default 0.
	*/
	ZeroMemory(pzInBuf, sizeof(SHARE_DATA_WITH_COLLECT_AGENT) * sizeof(TCHAR));

	bRes = ReadFile(m_hPipe, pzInBuf, sizeof(SHARE_DATA_WITH_COLLECT_AGENT), &nReadLen, NULL);

	nLastErr = GetLastError();

	if((bRes == FALSE) &&( nLastErr != ERROR_MORE_DATA)) {
		SE_MemoryDelete(pzInBuf);
		return FALSE;
	}

	/****************** zInBuf에 따라서 행동 *********************/


	SHARE_DATA_WITH_COLLECT_AGENT* pShareData;

	pShareData = reinterpret_cast<SHARE_DATA_WITH_COLLECT_AGENT*>(pzInBuf);

	if (pShareData->dwAct == ACT_SEND_THUMBNAIL)	{
	}
	else if (pShareData->dwAct == ACT_EXIT_COLLECT_AGENT) {
	}
	SE_MemoryDelete(pzInBuf);
	return TRUE;
}

BOOL CSTOCPipeServer::OnSendData()
{
	TCHAR zOutBuf[4096] ={0,};
	DWORD nWriteLen = 0;
	BOOL bRes = FALSE;

	_tcscpy(zOutBuf, SE_RETURN_OK);

	bRes = WriteFile( m_hPipe, zOutBuf, ( _tcslen( zOutBuf) * sizeof( TCHAR)), &nWriteLen, NULL);

	if( bRes == FALSE) 
	{
		return bRes;
	}
	return bRes;

}

BOOL CSTOCPipeServer::OnSetData(void* pData)
{
	if(pData == NULL)
		return FALSE;

	return TRUE;
}

BOOL CSTOCPipeServer::OnReceiveHeader()
{

	BOOL bRes = FALSE;
	return TRUE;
}

BOOL CSTOCPipeServer::OnReceiveBody()
{
	BOOL bRes = FALSE;
	return TRUE;
}
