#pragma once
#include "ClientPipe.h"
#include "Pipe.h"


class CSTOCPipeClient : public CClientPipe
{
public:
	CSTOCPipeClient();
	CSTOCPipeClient(TCHAR* _pszPipeName);
    virtual ~CSTOCPipeClient();

public:
	SHARE_DATA_WITH_COLLECT_AGENT * m_pShareData;
	//SHARE_DELETE_FILE_DATA * m_pShareDeleteData;

	virtual BOOL OnSetSendData(void* pData);
	//BOOL OnSetSendDeleteData(void* pData);
	virtual BOOL OnReceiveData();
	virtual BOOL OnSendData();
	//BOOL OnSendDeleteData();

	virtual BOOL  OnSetHeader(PIPE_HEADER* _pPipeHeader);
	virtual BOOL  OnSendBody();
	virtual BOOL  OnSendHeader();
	BOOL SendKillCollectAgentMsg();
	BOOL SendCollectInfo(SHARE_DATA_WITH_COLLECT_AGENT* ShareCollectInfo);

private:
    CSTOCPipeClient(const CSTOCPipeClient &_rPipeClient);
    CSTOCPipeClient& operator = (const CSTOCPipeClient &_rPipeClient);

};