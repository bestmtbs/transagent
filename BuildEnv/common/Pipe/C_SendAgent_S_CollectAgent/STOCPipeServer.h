#pragma once
#include "pipe.h"
#include "ServerPipe.h"

class CSTOCPipeServer : public CServerPipe
{
public:
	CSTOCPipeServer();
	CSTOCPipeServer(TCHAR* _pszPipeName);
    virtual ~CSTOCPipeServer();
public:
/*****************Server ******************************************************/

	virtual BOOL OnReceiveData();
	virtual BOOL OnSendData();

	virtual BOOL OnSetData(void* pData);
	virtual BOOL OnReceiveHeader();
	virtual BOOL OnReceiveBody();

private:
    CSTOCPipeServer(const CSTOCPipeServer &_rPipeServer);
    CSTOCPipeServer& operator = (const CSTOCPipeServer &_rPipeServer);


};