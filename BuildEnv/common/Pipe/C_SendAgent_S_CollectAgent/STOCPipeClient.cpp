
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE :  2011.10.19	
 ********************************************************************************/

/**
 @file      ATOMPipeClient.cpp
 @brief    Pipe 정의 클래스

 @author    JHLEE
 @date      create 2011.10.19
 @note      
*/

#include "stdafx.h"

#include "STOCPipeClient.h"


/**
 @brief     생성자
 @author    JHLEE
 @date       2011.10.19
*/
CSTOCPipeClient::CSTOCPipeClient(void)
{



}

/**
 @brief     생성자
 @author    JHLEE
 @date       2011.10.19
*/
CSTOCPipeClient::CSTOCPipeClient(TCHAR* _pszPipeName) : CClientPipe(_pszPipeName)
{
	m_pShareData = NULL;
	//m_pShareDeleteData = NULL;
}

/**
 @brief      소멸자
 @author    JHLEE
 @date       2011.10.19
*/
CSTOCPipeClient::~CSTOCPipeClient()
{




}


/**
 @brief      가상함수 OnSetSendData()  정의
 @author    JHLEE
 @date       2011.10.19
*/
BOOL CSTOCPipeClient::OnSetSendData(void* pData)
{
	if(pData == NULL)
		return FALSE;

	m_pShareData = reinterpret_cast<SHARE_DATA_WITH_COLLECT_AGENT*>(pData);

	return TRUE;
}

//BOOL CSTOCPipeClient::OnSetSendDeleteData(void* pData)
//{
//	if (pData == NULL)
//		return FALSE;
//
//	m_pShareDeleteData = reinterpret_cast<SHARE_DELETE_FILE_DATA*>(pData);
//
//	return TRUE;
//}

/**
 @brief      가상함수 OnReceiveData()  정의
 @author    JHLEE
 @date       2011.10.19
*/
BOOL CSTOCPipeClient::OnReceiveData()
{
	BOOL bRes = FALSE;
	TCHAR zInBuf[4096] ={0,};
	DWORD nReadLen = 0;
	DWORD nLastErr = 0;

	bRes = ReadFile( m_hPipe, zInBuf, 4096 * sizeof( TCHAR), &nReadLen, NULL);

	nLastErr = GetLastError( );

	if( ( bRes == FALSE) && ( nLastErr != ERROR_MORE_DATA))
	{
		SetFunctionErrorCode(GetLastError());
		return FALSE;
	}

	if(_tcscmp(zInBuf,SE_RETURN_ERR) == 0 || _tcscmp(zInBuf, SE_RETURN_NULL) == 0)
	{
		SetFunctionErrorCode(SE_RETURN_ERROR_CODE);
		return FALSE;
	}


	return TRUE;
}

/**
 @brief      가상함수 OnSendData()  정의
 @author    JHLEE
 @date       2011.10.19
*/
BOOL CSTOCPipeClient::OnSendData()
{

	//if (NULL == m_pShareData && NULL != m_pShareDeleteData) {
	//	BOOL bResult = FALSE;
	//	bResult = OnSendDeleteData();
	//	return bResult;
	//}
	BOOL bRes = FALSE;
	DWORD nWriteLen = 0;

	/*	
	DWORD		dwAct;
	BOOL		bState;
	DWORD		dwScreenIdx;
	TCHAR		szCollectType[64];	//	"FL"|"URL"
	DWORD		dwCollectDataSize;	//	szCollectType의 길이
	TCHAR		szExtionsionToIgnore[4096*2];	//	무시할 확장자 목록
	DWORD		dwExtionsionToIgnore;			//	길이
	TCHAR		szDomain[4096*2];	//	"naver.com"|"google.com"
	DWORD		dwDeleteFileSize;
	TCHAR		szDeleteFile[MAX_PATH];
	DWORD		dwDomainSize;		//	szDomain의 길이
	TCHAR		szTimeOnServer[SERVERTIME_SIZE];	// timestamp: yyyymmddhhmmssccc 최초 실행 후에는 받지 않으므로 default "".
	DWORD		dwUrlProcNameSize;		//	szUrlProcName의 길이
	TCHAR		szUrlProcName[4096];	// 필터링할 브라우저 이름
	DWORD		dwSplitSize;
	*/

	int nLen = sizeof(DWORD) + sizeof(BOOL) + sizeof(DWORD) + (64) * sizeof(TCHAR) + sizeof(DWORD) + (4096 * 2) * sizeof(TCHAR)
		+ sizeof(DWORD) + (4096 * 2) * sizeof(TCHAR) + sizeof(DWORD) + (MAX_PATH) * sizeof(TCHAR) + sizeof(DWORD)
		+ (SERVERTIME_SIZE) * sizeof(TCHAR) + sizeof(DWORD) + (4096) * sizeof(TCHAR) + sizeof(DWORD);
	/******************************* 보낼 데이타 조작*************************************************/

	bRes = WriteFile( m_hPipe, m_pShareData, nLen, &nWriteLen, NULL);

	if(bRes == FALSE)
	{
		SetFunctionErrorCode(GetLastError());
		return bRes;
	}	

	return bRes;

}
//
//BOOL CSTOCPipeClient::OnSendDeleteData()
//{
//
//	BOOL bRes = FALSE;
//	DWORD nWriteLen = 0;
//
//	/*
//	DWORD           dwAct;
//	DWORD              dwDeleteFileSize;
//	TCHAR              szDeleteFile[MAX_PATH * 2];
//	*/
//
//	int nLen = sizeof(DWORD) + sizeof(DWORD) + (MAX_PATH * 2) * sizeof(TCHAR);
//	/******************************* 보낼 데이타 조작*************************************************/
//
//	bRes = WriteFile(m_hPipe, m_pShareData, nLen, &nWriteLen, NULL);
//
//	if (bRes == FALSE)
//	{
//		SetFunctionErrorCode(GetLastError());
//		return bRes;
//	}
//
//	return bRes;
//
//}





BOOL CSTOCPipeClient::OnSendBody()
{

	BOOL bRes = FALSE;

	return bRes;

}
\

/**
@brief      가상함수 OnSendHeader()  정의
@author    hang ryul lee
@date      2011.08.13
*/
BOOL CSTOCPipeClient::OnSendHeader()
{
	BOOL bRes = FALSE;

	return bRes;

}

BOOL CSTOCPipeClient::OnSetHeader(PIPE_HEADER* _pPipeHeader)
{

	return TRUE;

}