#pragma once
#include "pipe.h"
#include "ServerPipe.h"

class CCTOSPipeServer : public CServerPipe
{
public:
	CCTOSPipeServer();
	CCTOSPipeServer(TCHAR* _pszPipeName);
    virtual ~CCTOSPipeServer();
public:
/*****************Server ******************************************************/

	virtual BOOL OnReceiveData();
	virtual BOOL OnSendData();

	virtual BOOL OnSetData(void* pData);
	virtual BOOL OnReceiveHeader();
	virtual BOOL OnReceiveBody();

private:
    CCTOSPipeServer(const CCTOSPipeServer &_rPipeServer);
    CCTOSPipeServer& operator = (const CCTOSPipeServer &_rPipeServer);


};