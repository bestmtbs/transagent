
#include "stdafx.h"
#include "CTOSPipeServer.h"
#include "../../TiorSaverTransfer/TiorSaverTransfer.h"

CCTOSPipeServer::CCTOSPipeServer(void)
{
}

CCTOSPipeServer::CCTOSPipeServer(TCHAR* _pszPipeName) : CServerPipe(_pszPipeName)
{
}

CCTOSPipeServer::~CCTOSPipeServer()
{
}

BOOL CCTOSPipeServer::OnReceiveData()
{
	BOOL bRes = FALSE;
	DWORD nReadLen = 0;
	DWORD nLastErr = 0;
	BYTE* pzInBuf = new BYTE[sizeof(_SHARE_THUMBNAIL_DATA)];
	
	/*	
	DWORD		dwAct;
	BOOL		bState;
	TCHAR		szCollectType[12*2];	//	"FL"|"URL"
	DWORD		dwCollectDataSize;	//	szCollectType의 길이
	TCHAR		szDomain[4096*2];	//	"naver.com"|"google.com"
	DWORD		dwDomainSize;	//	szDomain의 길이
	DWORD64		dw64TimeOnServer;	// timestamp: yyyymmddhhmmssccc 최초 실행 후에는 받지 않으므로 default 0.
	*/
	ZeroMemory(pzInBuf, sizeof(_SHARE_THUMBNAIL_DATA));
	bRes = ReadFile(m_hPipe, pzInBuf, sizeof(_SHARE_THUMBNAIL_DATA), &nReadLen, NULL);

	nLastErr = GetLastError();

	if((bRes == FALSE) &&( nLastErr != ERROR_MORE_DATA)) {
		SE_MemoryDelete(pzInBuf);
		return FALSE;
	}

	/****************** zInBuf에 따라서 행동 *********************/


	_SHARE_THUMBNAIL_DATA* pShareData;

	pShareData = reinterpret_cast<_SHARE_THUMBNAIL_DATA*>(pzInBuf);

	DBGLOG(_T("[%s] Got %d"), CTOS_PIPE_NAME, pShareData->dwBufferSize);
	if (pShareData->dwAct == ACT_SEND_THUMBNAIL){
		if (0 < pShareData->dwBufferSize) {
			theApp.SendThumb(pShareData->szBuffer, pShareData->dwBufferSize);
		}
	}
	else if (pShareData->dwAct == ACT_EXIT_COLLECT_AGENT) {
		//TODO
	}
	SE_MemoryDelete(pzInBuf);
	return TRUE;
}

BOOL CCTOSPipeServer::OnSendData()
{
	TCHAR zOutBuf[4096] ={0,};
	DWORD nWriteLen = 0;
	BOOL bRes = FALSE;

	_tcscpy(zOutBuf, SE_RETURN_OK);

	bRes = WriteFile( m_hPipe, zOutBuf, ( _tcslen( zOutBuf) * sizeof( TCHAR)), &nWriteLen, NULL);

	if( bRes == FALSE) 
	{
		return bRes;
	}
	return bRes;

}

BOOL CCTOSPipeServer::OnSetData(void* pData)
{
	if(pData == NULL)
		return FALSE;

	return TRUE;
}

BOOL CCTOSPipeServer::OnReceiveHeader()
{

	BOOL bRes = FALSE;
	return TRUE;
}

BOOL CCTOSPipeServer::OnReceiveBody()
{
	BOOL bRes = FALSE;
	return TRUE;
}
