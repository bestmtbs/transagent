
/**
 @file      RegUtils.cpp 
 @brief     Registry 관련 function 구현 파일 
 @author    hang ryul lee
 @date      create 2007.10.08 
*/

#include "stdafx.h"
#include <WinDNS.h>
#include <atlsecurity.h>
#include "Auth.h"
#include "Convert.h"
#include "Impersonator.h"
#include "UtilsReg.h"
#include "UtilsFile.h"
#include "WinOsVersion.h"
#include "WTSSession.h"
#define TAB            _T('\t')
#define NEWLINE        _T('\n')

/**
 @brief     registry 다중문자열 값을 write한다
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @param     [in] _sValName  Value Name
 @param     [in] _sValue    write할 값
 @param     [in] _bForce    Key가 존재하지 않을경우 자동생성 여부(default = false)
 @return    true/false
 @note      저장할 string에 존재하는NEWLINE 문자를 '\0'으로 변경한다,\n
            (문자열과 다중문자열을 NEWLINE으로 구분한다.\n
            해당문자가 하나라도 포함이 되어 있으면 다중문자열이라 인식.)
*/
bool SetRegMultiStringValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue, const bool _bForce /*= false*/)
{
    bool bResult = false;

    CString sKey = ExcludeTrailingBackslash(_sKey);
    CString sName = _sValName;
    CString sValue = _sValue;
    
    sKey.Trim();
    sValue.Trim();
    sName.Trim();

    HKEY hKey;

    if (MAX_KEYNAME_LEN < sKey.GetLength())     return false;
    if (MAX_VALUENAME_LEN < sName.GetLength())  return false;
    if (MAX_VALUE_LEN < sValue.GetLength())     return false;
    
    if (!IsRegKeyExists(_hRoot, sKey))
    {
        if (_bForce)
            CreateRegKey(_hRoot, sKey);
        else return false;
    }

    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_WRITE, &hKey))
            return false;

    DWORD   dwSize = (sValue.GetLength()+1) * sizeof(TCHAR);
    sValue.Replace(NEWLINE, _T('\0'));
    LPTSTR  szValue = sValue.GetBuffer(sValue.GetLength());

    if (ERROR_SUCCESS == ::RegSetValueEx(hKey, sName, 0, REG_MULTI_SZ, (LPBYTE)szValue, dwSize))
        bResult = true;

    sValue.ReleaseBuffer();
    ::RegCloseKey(hKey);
    return bResult;
}

/**
 @brief     registry String 값을 write한다
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in]    _hRoot         :Root Key
 @param     [in]    _sKey         :Sub Key
 @param     [in]    _sValName     :Value Name
 @param     [in]    _sValue         :write할 값
 @param     [in]    _bForce         :Key가 존재하지 않을경우 자동생성 여부(default = false)
 @return    true/false
 @note      update 2008.03.04 yunmi\n
            .유니코드에서 문자열 길이가 잘못구해서 문자열이 잘려 저장되는경우 발생.\n
            .문자열 길에 조사시 TCHAR의 사이즈를 곱해준다.\n
 @note      update 2008.03.05 yunmi\n
            .value name의 경우 유니코드이든 안시코드이든, 255까지만 지정가능함.(XP에서 test)\n
 @note      update 2008.04.24 yunmi\n
            .Key가 존재하지 않을 경우 처리에 대한 파라미터 추가.\n
            .default 값으로는 false 를 반환하고 종료한다.
*/
bool SetRegStringValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue, const bool _bForce /*= false*/)
{
    bool bResult = false;

    CString sKey = ExcludeTrailingBackslash(_sKey);
    CString sName = _sValName;
    CString sValue = _sValue;
    
    sKey.Trim();
    sValue.Trim();
    sName.Trim();

    HKEY hKey;

    if (MAX_KEYNAME_LEN < sKey.GetLength())    return false;
    if (MAX_VALUENAME_LEN < sName.GetLength()) return false;
    if (MAX_VALUE_LEN < sValue.GetLength())    return false;
    
    if (!IsRegKeyExists(_hRoot, sKey))
    {
        if (_bForce)
            CreateRegKey(_hRoot, sKey);
        else return false;
    }

    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_WRITE, &hKey))
            return false;

    LPTSTR   szValue = sValue.GetBuffer(sValue.GetLength());
    DWORD    dwSize = (sValue.GetLength()+1) * sizeof(TCHAR);
    if (ERROR_SUCCESS == ::RegSetValueEx(hKey, sName, 0, REG_SZ, (LPBYTE)szValue, dwSize))
        bResult = true;

    sValue.ReleaseBuffer();
    ::RegCloseKey(hKey);
    return bResult;
}

/**
 @brief     registry DWORD 값을 write한다
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @param     [in] _sValName  Value Name
 @param     [in] _sValue    write할 값
 @param     [in] _bForce    Key가 존재하지 않을경우 자동생성 여부(default = false)
 @return    true/false   
 @note      update 2008.04.24 yunmi\n
            .Key가 존재하지 않을 경우 처리에 대한 파라미터 추가.\n
            .default 값으로는 false 를 반환하고 종료한다.
*/
bool SetRegDWORDValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const DWORD &_dwValue, const bool _bForce)
{
    bool bResult = false;

    CString sKey = ExcludeTrailingBackslash(_sKey);
    CString sName = _sValName;
    
    sKey.Trim();
    sName.Trim();

    HKEY hKey;

    if (MAX_KEYNAME_LEN < sKey.GetLength())    return false;
    if (MAX_VALUENAME_LEN < _sValName.GetLength())    return false;

    if (!IsRegKeyExists(_hRoot, sKey))
    {
        if (_bForce)
            CreateRegKey(_hRoot, sKey);
        else return false;
    }

    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_WRITE, &hKey))
            return false;

    if (ERROR_SUCCESS == ::RegSetValueEx(hKey, sName, 0, REG_DWORD, (LPBYTE)&_dwValue, sizeof(DWORD)))
        bResult = true;

    ::RegCloseKey(hKey);
    return bResult;
}

/**
 @brief     특정키에 sub키를 추가한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      추가하려는 key의 부모키
 @param     [in] _sAddKey   추가할 key name
 @param     [in] _bForce    _sKey가 존재하지 않을 경우의 처리 (default = false)
 @return    update 2008.04.28 yunmi \n
            ._bForce값에 따라서 추가하려는 키의 부모키가 존재하지 않을 경우에 대한 action이 달라진다. \n
            ._bForce = true : 해당키 생성 후 추가한다. \n
            ._bForce = false: 실패를 반환한다. \n
*/
bool AddRegKey(const HKEY _hRoot, const CString &_sKey, const CString &_sAddKey, const bool _bForce)
{
    CString sSubKey = _sKey;
    CString sAddKey = ExcludeTrailingBackslash(_sAddKey);
    sSubKey.Trim();
    sAddKey.Trim();

    if (-1 < sAddKey.Find(_T('\\')))    // '\'는 키값에 포함될 수 없음.
        return false;

    if ((HKEY_LOCAL_MACHINE == _hRoot) || (HKEY_USERS == _hRoot))
    {
        if (_T("") == sSubKey)    
            return false;
    }

    if (!IsRegKeyExists(_hRoot, sSubKey))    
    {
        if (!_bForce)
            return false;
    }

    sSubKey = IncludeTrailingBackslash(sSubKey) + sAddKey;
    return CreateRegKey(_hRoot, sSubKey);
}

/**
 @brief     registry key를 생성한다.
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot        :Root Key
 @param     [in] _sKey         :생성할 키 path
 @return    true / false
 @note      생성할 키의 부모키의 존재유무에 상관없이 무조건 생성한다.
*/
bool CreateRegKey(const HKEY _hRoot, const CString &_sKey)
{
    HKEY hKey;
    DWORD dwDisp=0;
    
    CString sKey = _sKey;
    
    sKey.Trim();
    sKey = ExcludeTrailingBackslash(sKey);
    sKey.Trim();

    if ((HKEY_LOCAL_MACHINE == _hRoot) || (HKEY_USERS == _hRoot))
    {
        if (0 > sKey.Find(_T('\\')))
            return false;
    }

    if (MAX_KEYNAME_LEN < sKey.GetLength())    
        return false;
    
    if (ERROR_SUCCESS != ::RegCreateKeyEx(_hRoot, sKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE,
                                            NULL, &hKey, &dwDisp))
        return false;

    ::RegCloseKey(hKey);
    return true;
}

/**
 @brief     registry string 값을 구한다.
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot        :Root Key
 @param     [in] _sKey         :Sub Key
 @param     [in] _sValName     :Value Name
 @param     [in] _sDef         :default = _T("")
 @return    읽은 레지스트리값
 @note      update 2008.04.24 yunmi\n
            .MULTI_SZ의 경우 NULL문자열을 NEWLINE으로 변환하여 string을 반환한다.
*/
CString GetRegStringValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sDef, BOOL bFroceStr)
{
    CString sResult = _sDef;

    CString sKey = _sKey;
    CString sName = _sValName;
    sKey.Trim();
    sName.Trim();

    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
    {

        DWORD    dwSize = (MAX_VALUE_LEN +1) * sizeof(TCHAR);
        TCHAR    szValue[MAX_VALUE_LEN +1] = {0,};
        DWORD    dwType = REG_NONE;

        if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, sName, NULL, &dwType, (LPBYTE)&szValue, &dwSize))
        {
            if ((REG_SZ == dwType) || (REG_EXPAND_SZ == dwType))
                sResult = szValue;

			if(bFroceStr)
				sResult.Format(L"%s", szValue);

            else if (REG_MULTI_SZ == dwType)
            {
                DWORD i=0;
                for (i = 0; i< (dwSize/sizeof(TCHAR)); i++)
                {
                    if (_T('\0') == szValue[i])
                        szValue[i] = NEWLINE;
                }
                szValue[i-1] = _T('\0');
                sResult = szValue;
            }
        }

        ::RegCloseKey(hKey);
    }
    return sResult;
}

/**
 @brief     registry DWORD 값을 구한다.
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @param     [in] _sValName  Value Name
 @param     [in] _dwDef     default = 0
 @return    읽은 레지스트리값    
*/
DWORD GetRegDWORDValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const DWORD &_dwDef)
{
    DWORD dwResult = _dwDef;
    DWORD dwSize = sizeof(DWORD);

    CString sKey = _sKey;
    CString sName = _sValName;
    sKey.Trim();
    sName.Trim();

    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
    {
        DWORD dwTmp = 0;
        DWORD dwType = REG_NONE;
        if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, sName, NULL, &dwType, (LPBYTE)&dwTmp, &dwSize))
        {
            if (REG_DWORD == dwType)
                dwResult = dwTmp;
        }
        ::RegCloseKey(hKey);
    }
    return dwResult;
}

/**
 @brief     type에 상관없이 특정 value값을 구한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub key 값
 @param     [in] _sValName  ValueName
 @param     [out]_sRe       구한 value값
 @return    true / false
 @note      .불특정 type에 대한 함수이므로 반환 string에 type을 지정해 준다.\n
             ex) _sRe = "valueName" + TAB + "type string" + TAB + "value data" + TAB\n
            .MULTI_SZ의 경우 value data에 NULL값이 여러번 들어가 있으므로 해당 값을 TAB으로 변경하여 저장한다.\n
             ex) _sRe = "valueName" + TAB + "type string" + TAB + "value data" + NEWLINE + "value data" + NEWLINE + .....\n
*/
bool GetRegValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, CString &_sRe)
{
    _sRe = _T("");

    CString sKey = _sKey;
    CString sName = _sValName;
    sKey.Trim();
    sName.Trim();

    HKEY hKey;
    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
        return false;

    DWORD    dwLen = (MAX_VALUE_LEN +1) * sizeof(TCHAR);
    BYTE     buffer[(MAX_VALUE_LEN +1) * sizeof(TCHAR)] = { 0,};
    DWORD    dwType = REG_NONE;
    
    if (ERROR_SUCCESS != ::RegQueryValueEx(hKey, sName, NULL, &dwType, buffer, &dwLen))
    {
        ::RegCloseKey(hKey);
        return false;
    }

    CString sVal = _T(""), temp = _T("");
    DWORD i = 0;

    _sRe = sName + TAB + RegValueTypeToText(dwType) + TAB;
    switch (dwType)
    {
    case REG_NONE                :    
    case REG_BINARY              :    
    case REG_RESOURCE_LIST       :
    case REG_FULL_RESOURCE_DESCRIPTOR    : 
        for (i = 0; i < dwLen ; i++)
        {
            temp.Format(_T("%02X"), buffer[i]);
            sVal+= temp;
        }
        break;
    case REG_SZ                  :    
    case REG_EXPAND_SZ           :
        sVal = reinterpret_cast<LPTSTR>(buffer);
        break;
    case REG_MULTI_SZ            : 
        {
            LPTSTR szTmp = reinterpret_cast<LPTSTR>(buffer);
            for (i = 0; i< (dwLen/sizeof(TCHAR)); i++)
            {
                if (_T('\0') == szTmp[i])
                    szTmp[i] = NEWLINE;
            }
            szTmp[i-1] = '\0';
            sVal = szTmp;
            break;
        }
    case REG_DWORD               :
    case REG_DWORD_BIG_ENDIAN    :
        {
            DWORD *pdwVal = reinterpret_cast<LPDWORD>(buffer);
            sVal.Format(_T("0x%0.8x (%u)"), *pdwVal,*pdwVal);
            break;
        }
    case REG_QWORD               :
        {
            QWORD *pqwVal = reinterpret_cast<QWORD *>(buffer);
            CString sValue = CConvert::ToString(static_cast<INT64>(*pqwVal));
            DWORD dwHead = static_cast<DWORD>(*pqwVal>>32);
            DWORD dwTail = static_cast<DWORD>(*pqwVal);
            sVal.Format(_T("0x%0.8x%0.8x (%s)"), dwHead, dwTail, sValue.GetBuffer(0));
            sValue.ReleaseBuffer();
            break;
        }
    default    :
        sVal = _T("ERROR - VALUE NOT READ");
        break;
    }
    _sRe = _sRe + sVal + TAB;

    ::RegCloseKey(hKey);

    return true;
}

/**
 @brief     특정 Key 밑의 value Name list를 구한다
 @author    hang ryul lee
 @date      2008.04.14
 @param     [in] _hRoot             Root Key
 @param     [in] _sKey              Sub Key
 @param     [out]_sVlaNameLsit      구한 Value Name List 저장 
 @return    구한 valueName count
*/
INT GetRegValueNameList(const HKEY _hRoot, const CString &_sKey, CStringList &_sValNameList)
{
    LONG    lRet = 0;
    DWORD    dwIndex = 0;
    DWORD    dwNameLen = MAX_VALUENAME_LEN+1;
    TCHAR    szValueName[MAX_VALUENAME_LEN+1] = {0,};

    CString sKey = _sKey;
    sKey.Trim();

    if (0 < _sValNameList.GetCount())    _sValNameList.RemoveAll();

    HKEY hKey;
    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
        return -1;
    
    //dwNameLen = lpcName
    //lpcName : ValueName character count
    while (ERROR_NO_MORE_ITEMS != (lRet = ::RegEnumValue(hKey, dwIndex, szValueName, &dwNameLen, NULL, NULL, NULL, NULL)))
    {
        if (ERROR_SUCCESS==lRet)
        {
            _sValNameList.AddTail(szValueName);
        }
        dwIndex++;
        dwNameLen = MAX_KEYNAME_LEN +1;
    }
    ::RegCloseKey(hKey);

    return static_cast<INT>(_sValNameList.GetCount());
}

/**
 @brief     Registry 특정 key의 value list를 구한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      구하려는 key
 @param     [out]_sValList  구한 valueList
 @param     [in] _bForce    _sKey가 존재하지 않을 경우의 처리 (default = false)
 @return    value count
 @note      update 2008.04.28 yunmi \n
            . value List의 각 항목은 다음과 같이 구성된다.\n
             (valueName[TAB]valueType[TAB]valueData[TAB]) \n
            . MULTI_SZ의 경우 약간 다르게 구성된다.\n
             (valueName[TAB]valueType[TAB]valueData[NEWLINE]valueData[NEWLINE].......[TAB]) 
*/
INT GetRegValueList(const HKEY _hRoot, const CString &_sKey, CStringList &_sValList)
{
    CString sKey = _sKey;
    sKey.Trim();

    if (0 < _sValList.GetCount())    _sValList.RemoveAll();

    CStringList sValNames;
    if (0 >= GetRegValueNameList(_hRoot, sKey, sValNames))        return -1;

    POSITION pos = sValNames.GetHeadPosition();
    while (NULL != pos)
    {
        CString sValueStr = _T("");
        CString sNames = sValNames.GetNext(pos);
        GetRegValue(_hRoot, sKey, sNames, sValueStr);
        _sValList.AddTail(sValueStr);
    }

    return static_cast<INT>(_sValList.GetCount());
}

/**
 @brief     Root key 리스트를 반환한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [out] _sRootlist    Root Key List
 @return    RootKey List count
*/
INT GetRegRootKeyList(CStringList &_sRootList)
{
    if (0 < _sRootList.GetCount())
        _sRootList.RemoveAll();

    _sRootList.AddTail(_T("HKEY_CLASSES_ROOT"));
    _sRootList.AddTail(_T("HKEY_CURRENT_USER"));
    _sRootList.AddTail(_T("HKEY_LOCAL_MACHINE"));
    _sRootList.AddTail(_T("HKEY_USERS"));
    _sRootList.AddTail(_T("HKEY_CURRENT_CONFIG"));
    

    /*if (IsWinNT())
        _sRootList.AddTail(_T("HKEY_PERFORMANCE_DATA"));
    else
        _sRootList.AddTail(_T("HKEY_DYN_DATA"));*/

    return static_cast<INT>(_sRootList.GetCount());
}

/**
 @brief     특정 Key 밑의 subkey list를 구한다
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @param     [out]_sKeyList  구한 Key List 저장 
 @return    구한 key count
*/
INT GetRegSubKeyList(const HKEY _hRoot, const CString &_sKey, CStringList &_sKeyList)
{
    LONG    lRet = 0;
    DWORD   dwIndex = 0;
    DWORD   dwNameLen = MAX_KEYNAME_LEN+1;
    TCHAR   szSubKeyName[MAX_KEYNAME_LEN+1] = {0,};

    CString sKey = _sKey;
    sKey.Trim();

    if (0 < _sKeyList.GetCount())    _sKeyList.RemoveAll();

    HKEY hKey;
    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
        return -1;
    
    //dwNameLen = lpcName
    //lpcName : subKeyName character count
    while (ERROR_NO_MORE_ITEMS != (lRet = ::RegEnumKeyEx(hKey, dwIndex, szSubKeyName, &dwNameLen, NULL, NULL, NULL, NULL)))
    {
        if (ERROR_SUCCESS==lRet)
        {
            _sKeyList.AddTail(szSubKeyName);
        }
        dwIndex++;
        dwNameLen = MAX_KEYNAME_LEN +1;
    }
    ::RegCloseKey(hKey);

    return static_cast<INT>(_sKeyList.GetCount());
}

/**
 @brief     특정 Key의 subKey List를 구한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      구하려는 key
 @param     [out]_sKeyList  구한 KeyList
 @return    Key count
 @note      update 2008.04.28 yunmi \n
            . 해당키의 서브키와, 그 서브키가 또다른 서브키를 가지고 있는지 여부를 함께 저장한다.\n
            . ex) sub1\\sub2\\sub3\n
              위의 경우sub1로 GetRegKeysWithSubCount함수를 호출하면 \n
              sub1의 서브키인 "sub2+sub2의서브키존재 유무" 를 함께 넘겨 받는다.\n
*/
INT GetRegSubKeyWithSubCount(const HKEY _hRoot, const CString &_sKey, CStringList &_sKeyList)
{
    if ( 0 < _sKeyList.GetCount())
        _sKeyList.RemoveAll();

    CString sKey = _sKey;
    sKey.Trim();

    CStringList sKeyList;
    if (0 > GetRegSubKeyList(_hRoot, sKey, sKeyList))
        return -1;

    POSITION pos = sKeyList.GetHeadPosition();
    while (NULL != pos)
    {
        CString sSub = sKeyList.GetNext(pos);
        CString sTmpKey = IncludeTrailingBackslash(sKey)+sSub;

        if (0 < GetSubKeyCount(_hRoot, sTmpKey))
            sSub = sSub + TAB + _T('1') + TAB;
        else
            sSub = sSub + TAB + _T('0') + TAB;

        _sKeyList.AddTail(sSub);
    }

    return static_cast<INT>(_sKeyList.GetCount());
}

/**
 @brief     Key의 subKey 개수를 구한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      구하려는 key
 @return    subkey count
*/
INT GetSubKeyCount(const HKEY _hRoot, const CString &_sKey)
{
    CString sKey = _sKey;
    sKey.Trim();

    HKEY hKey;
    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_QUERY_VALUE, &hKey))
        return -1;

    DWORD dwSubKeys = 0;
    DWORD dwRet = 0;
    dwRet = ::RegQueryInfoKey(hKey, NULL, NULL, NULL, &dwSubKeys, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

    ::RegCloseKey(hKey);
        
    return dwSubKeys;
}

/**
 @brief     특정 키가 존재하는지 확인
 @author    hang ryul lee
 @date      2007.10.08
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @return    true/false
*/
bool IsRegKeyExists(const HKEY _hRoot, const CString &_sKey)
{
    CString sKey = _sKey;
    sKey.Trim();

    HKEY hKey;
    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
        return false;

    ::RegCloseKey(hKey);
    return true;
}

/**
 @brief     특정 키의 값이 존재하는지 확인
 @author    hang ryul lee 
 @date      2008.04.15
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @param     [in] _sValName  Value Name
 @return    true/false
*/
bool IsRegValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName)
{
    HKEY hKey;
    bool bResult = false;

    CString sKey = _sKey;
    CString sName = _sValName;
    sKey.Trim();
    sName.Trim();

    if (MAX_KEYNAME_LEN < sKey.GetLength())    return false;
    if (MAX_VALUENAME_LEN < sName.GetLength())    return false;

    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ, &hKey))
    {        
        if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, sName, NULL, NULL, NULL, NULL))    
            bResult = true;    
        
        ::RegCloseKey(hKey);
    }
    return bResult;
}

/**
 @brief     특정 키 삭제
 @author    hang ryul lee
 @date      2008.03.04
 @param     [in] _hRoot     Root Key
 @param     [in] _sDelKey   삭제하려는 key path
 @param     [in] _bForce    subkey가 존재할때의 강제 삭제유무 (default = false)
 @return    true/false
 @note      update 2008.04.28 yunmi \n
            ._bForce 값에 따라서 서브키가 존재할때의 action을 다르게 한다. \n
            ._bForce = true : sub키가 존재할경우 전부 삭제한다. \n
            ._bForce = false: sub키가 존재할경우 실패를 반환한다. \n
*/
bool DelRegKey(const HKEY _hRoot, const CString &_sDelKey, const bool _bForce)
{
    bool bResult = false;

    CString sKey = _sDelKey;
    sKey.Trim();

    HKEY hKey;
    CStringList sSubKeys;
    GetRegSubKeyList(_hRoot, sKey, sSubKeys);
    if (0 < sSubKeys.GetCount())
    {
        if (!_bForce)        return false;

        POSITION pos = sSubKeys.GetHeadPosition();
        while (NULL != pos)
        {
            CString sSubKey = sSubKeys.GetNext(pos);
            if (!DelRegKey(_hRoot, IncludeTrailingBackslash(sKey)+sSubKey, true))
                return false;
        }
    }

    CString sFullKey = ExcludeTrailingBackslash(sKey);
    CString sParent = sFullKey.Left(sFullKey.ReverseFind(_T('\\')));
    CString sDel = sFullKey.Mid(sFullKey.ReverseFind(_T('\\'))+1, sFullKey.GetLength());

    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, sParent, 0, KEY_READ | KEY_WRITE, &hKey))
    {
        if (ERROR_SUCCESS == ::RegDeleteKey(hKey, sDel))
            bResult = true;
        ::RegCloseKey(hKey);
    }
    return bResult;
}

/**
 @brief     특정 값 삭제
 @author    hang ryul lee
 @date      2008.03.04
 @param     [in] _hRoot         Root Key
 @param     [in] _sKey          Sub Key
 @param     [in] _sValueName    Value Name
 @return    true/false
*/
bool DelRegValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValueName)
{
    bool bResult = false;

    CString sKey = _sKey;
    CString sName = _sValueName;
    sKey.Trim();
    sName.Trim();

    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ | KEY_WRITE, &hKey))
    {
        if (ERROR_SUCCESS == ::RegDeleteValue(hKey, sName))
            bResult = true;
        ::RegCloseKey(hKey);
    }
    return bResult;
}

/**
 @brief     Registry Key 를 rename한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sOldKey   변경하기 이전키 값 
 @param     [in] _sNewKey   변경하려는 키 값
 @return    true / false
 @note      이전 key를 새로운 key로 복사한 후 이전키를 삭제한다.
 @note      같은 이름의 key가 존재할 경우 실패 반환.
*/
bool RenameRegKey(const HKEY _hRoot, const CString &_sOldKey, const CString &_sNewKey)
{
    CString sOld = _sOldKey;
    CString sNew = ExcludeTrailingBackslash(_sNewKey);

    if ((_T("") == sNew.Trim()) || (_T("") == sOld.Trim()))
        return false;

    if (0 == sOld.CompareNoCase(sNew))
        return true;

    if (MAX_KEYNAME_LEN < sNew.GetLength())    return false;

    if ((IsRegKeyExists(_hRoot, sNew)) || (!IsRegKeyExists(_hRoot, sOld)))
        return false;

    if (!CopyRegKeys(_hRoot, sOld, sNew))
    {
        DelRegKey(_hRoot, sNew, true);
        return false;
    }

    if (!DelRegKey(_hRoot, sOld, true))
    {
        DelRegKey(_hRoot, sNew, true);
        return false;
    }

    return true;
}

/**
 @brief     Registry valueName 을 rename한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sSubKey   sub Key
 @param     [in] _sOldVal   이전값
 @param     [in] _sNewVal   변경하려는 값
 @return    true / false
 @note      이전 값을 새로운 값으로 복사한 후 이전값을 삭제한다.
*/
bool RenameRegValue(const HKEY _hRoot, const CString &_sSubKey, const CString &_sOldVal, const CString &_sNewVal)
{
    CString sKey = _sSubKey;
    CString sOld = _sOldVal;
    CString sNew = _sNewVal;
    sKey.Trim();
    sOld.Trim();
    sNew.Trim();

    if (0 == sOld.CompareNoCase(sNew))
        return true;

    if (MAX_VALUENAME_LEN < sNew.GetLength())    return false;

    if (!IsRegKeyExists(_hRoot, sKey))
        return false;

    if ((IsRegValueExists(_hRoot, sKey, sNew)) || (!IsRegValueExists(_hRoot, sKey, sOld)))
        return false;

    if (!CopyRegValue(_hRoot, sKey, sOld, sKey, sNew))
        return false;

    if (!DelRegValue(_hRoot, sKey, sOld))
    {
        DelRegValue(_hRoot, sKey, sNew);
        return false;
    }

    return true;
}

/**
 @brief     Key를 복사한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sOldKe    이전 key 값
 @param     [in] _sNewKey   destination key값
 @return    true / false
 @note      복사대상 key에 subkey가 존재할 경우 재귀적으로 호출한다.
 @note      subkey 복사실패 했을 경우 실패를 반환한다.\n
            실패 이전에 복사된 값에 대해서는 삭제를 진행하지 않는다.
 @note      주의!! 길이제한은 복사대상 key length + subkey length 이다.\n
            subkey의 길이를 포함한 값이 MAX_KEYNAME_LEN을 넘지 않도록 한다.
 @note      복사하려는 대상 키 이름과 동일한 키가 이미 존재할 경우 실패 반환.
*/
bool CopyRegKeys(const HKEY _hRoot, const CString &_sOldKey, const CString &_sNewKey)
{
    CString sNew = ExcludeTrailingBackslash(_sNewKey);
    CString sOld = _sOldKey;

    if ((_T("") == sNew.Trim()) || (_T("") == sOld.Trim()))
        return false;

    if (0 == sOld.CompareNoCase(sNew))
        return true;

    if (MAX_KEYNAME_LEN < sNew.GetLength())    return false;

    if ((IsRegKeyExists(_hRoot, sNew)) || (!IsRegKeyExists(_hRoot, sOld)))
        return false;

    HKEY hKey;
    DWORD dwDisp=0;
    if (ERROR_SUCCESS != ::RegCreateKeyEx(_hRoot, sNew, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE,
                                            NULL, &hKey, &dwDisp))
        return false;
    ::RegCloseKey(hKey);
    
    if (!CopyRegValues(_hRoot, sOld, sNew))
        return false;

    CStringList sKeyList;
    GetRegSubKeyList(_hRoot, sOld, sKeyList);
    POSITION posKey = sKeyList.GetHeadPosition();
    while (NULL != posKey)
    {
        CString sKey = sKeyList.GetNext(posKey);
        if (!CopyRegKeys(_hRoot, IncludeTrailingBackslash(sOld)+sKey, IncludeTrailingBackslash(sNew)+sKey))
            return false;
    }

    return true;
}

/**
 @brief     해당 키의 value값들을 모두 복사한다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sOldKe    이전 key 값
 @param     [in] _sNewKey   destination key값
 @return    true / false
 @note      Oldkey에 대한 값들을 모두 newKey에 복사한다.
 @note      newKey가 없는경우 실패 반환.
*/
bool CopyRegValues(const HKEY _hRoot, const CString &_sOldKey, const CString &_sNewKey)
{
    bool bResult = true;

    CString sOld = _sOldKey;
    CString sNew = _sNewKey;

    sOld.Trim();
    sNew.Trim();

    if (0 == sOld.CompareNoCase(sNew))
        return true;

    if ((!IsRegKeyExists(_hRoot, sNew)) || (!IsRegKeyExists(_hRoot, sOld)))
        return false;

    if (MAX_KEYNAME_LEN < sNew.GetLength())    return false;

    CStringList sValNames;
    GetRegValueNameList(_hRoot, sOld, sValNames);

    POSITION pos = sValNames.GetHeadPosition();
    while(NULL != pos)
    {
        CString sName = sValNames.GetNext(pos);

        if ((!CopyRegValue(_hRoot, sOld, sName, sNew, sName)) && (_T("") != sName))
        {
            bResult = false;
            break;
        }
    }
 
    return bResult;
}

/**
 @brief     특정 value값 복사
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _hRoot     Root Key
 @param     [in] _sOldKey   복사대상 key
 @param     [in] _sOldVal   복사대상 Value Name
 @param     [in] _sNewKey   destination key
 @param     [in] _sNewVal   destination Value Name
 @return    true/false
 @note      해당 value값을 복사한다.\n
            .복사 대상과 목적지의 Key, Valuename이 동일할 경우 true 반환.\n
            .복사 대상과 목적지의 키가 존재하지않을 경우 실패를 반환한다..\n
            .동일한 이름의 값이 기존에 존재할 경우 실패를 반환한다.
*/
bool CopyRegValue(const HKEY _hRoot, const CString &_sOldKey, const CString &_sOldVal, const CString &_sNewKey, const CString &_sNewVal)
{
    bool bResult = false;

    CString sOld = _sOldKey;
    CString sNew = ExcludeTrailingBackslash(_sNewKey);
    CString sOldName = _sOldVal;
    CString sNewName = _sNewVal;

    sOld.Trim();
    sNew.Trim();
    sOldName.Trim();
    sNewName.Trim();

    if ((0 == sOld.CompareNoCase(sNew)) && (0 == sOldName.CompareNoCase(sNewName)))
        return true;

    if ((!IsRegKeyExists(_hRoot, sNew)) || (!IsRegKeyExists(_hRoot, sOld)))
        return false;

    if ((IsRegValueExists(_hRoot, sNew, sNewName)) || (!IsRegValueExists(_hRoot, sOld, sOldName)))
        return false;

    if (MAX_KEYNAME_LEN < sNew.GetLength())    return false;
    if (MAX_VALUENAME_LEN < sNewName.GetLength())    return false;

    HKEY hOldKey, hNewKey;
    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sOld, 0, KEY_READ, &hOldKey))
        return false;

    DWORD   dwLen = (MAX_VALUE_LEN +1) * sizeof(TCHAR);
    BYTE    buffer[(MAX_VALUE_LEN +1) * sizeof(TCHAR)] = { 0,};
    DWORD   dwType = REG_NONE;
    DWORD   dwRet;

    dwRet = ::RegQueryValueEx(hOldKey, sOldName, NULL, &dwType, buffer, &dwLen);
    ::RegCloseKey(hOldKey);
    if (ERROR_SUCCESS != dwRet)        return false;

    if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sNew, 0, KEY_WRITE, &hNewKey))
        return false;

    if (ERROR_SUCCESS == ::RegSetValueEx(hNewKey, sNewName, 0, dwType, buffer, dwLen))
        bResult = true;

    ::RegCloseKey(hNewKey);

    return bResult;
}

/**
 @brief     64비트 환경에서 32비트 registry string 값을 구한다.
 @author    hang ryul lee
 @date      2008.04.18
 @param     [in] _hRoot     Root Key
 @param     [in] _sKey      Sub Key
 @param     [in] _sValName  Value Name
 @param     [in] _sDef      default = _T("")
 @return    읽은 레지스트리값
*/
CString GetRegStringWow64Value(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sDef)
{
    CString sResult = _sDef;
#if defined(_WIN64)
    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, _sKey, 0, KEY_READ | KEY_WOW64_32KEY, &hKey))
    {
        DWORD    dwLen = (MAX_VALUE_LEN +1)* sizeof(TCHAR);
        TCHAR    szValue[MAX_VALUE_LEN +1] = {0,};
        DWORD    dwType = REG_NONE;

        if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, _sValName, NULL, &dwType, reinterpret_cast<LPBYTE>(&szValue), &dwLen))
        {
            if (REG_SZ == dwType)
                sResult = szValue;
        }
        ::RegCloseKey(hKey);
    }
#else
    UNUSED_ALWAYS(_hRoot);
    UNUSED_ALWAYS(_sKey);
    UNUSED_ALWAYS(_sValName);
    UNUSED_ALWAYS(_sDef);
#endif
    return sResult;
}

/**
 @brief     64비트 환경에서 32비트 registry String 값을 write한다
 @author    hang ryul lee
 @date      2008.04.18
 @param    [in]    _hRoot         :Root Key
 @param    [in]    _sKey         :Sub Key
 @param    [in]    _sValName     :Value Name
 @param    [in]    _sValue         :write할 값
 @return    true/false
 @note        -Key가 존재하지 않을 경우에는 생성한다\n
            -2008.03.04 yunmi\n
             유니코드에서 문자열 길이가 잘못구해서 문자열이 잘려 저장되는경우 발생.\n
             문자열 길에 조사시 TCHAR의 사이즈를 곱해준다.\n
            -2008.03.05 yunmi\n
             value name의 경우 유니코드이든 안시코드이든, 255까지만 지정가능함.(XP에서 test)
*/
bool SetRegStringWow64Value(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue)
{
    bool bResult = false;
#if defined(_WIN64)
    CString sValue = _sValue;
    HKEY hKey;
    DWORD dwDisp=0;

    if (MAX_KEYNAME_LEN < _sKey.GetLength())    return false;
    if (MAX_VALUENAME_LEN < _sValName.GetLength())    return false;
    if (MAX_VALUE_LEN < _sValue.GetLength())    return false;

    if (ERROR_SUCCESS != ::RegCreateKeyEx(_hRoot, _sKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_WOW64_32KEY,
                                            NULL, &hKey, &dwDisp))
        return false;

    LPTSTR    szValue = sValue.GetBuffer(sValue.GetLength());
    DWORD    dwSize = (sValue.GetLength()+1) * sizeof(TCHAR);
    if (ERROR_SUCCESS == ::RegSetValueEx(hKey, _sValName, 0, REG_SZ, (LPBYTE)szValue, dwSize))
        bResult = true;

    sValue.ReleaseBuffer();
    ::RegCloseKey(hKey);
#else
    UNUSED_ALWAYS(_hRoot);
    UNUSED_ALWAYS(_sKey);
    UNUSED_ALWAYS(_sValName);
    UNUSED_ALWAYS(_sValue);
#endif
    return bResult;
}

/**
 @brief     64비트 환경에서 32비트 레지스트리의 특정 키가 존재하는지 확인
 @author    hang ryul lee
 @date      2008.04.18
 @param    [in]    _hRoot         :Root Key
 @param    [in]    _sKey         :Sub Key
 @return    true/false
*/
bool IsRegWow64KeyExists(const HKEY _hRoot, const CString &_sKey)
{
    bool bResult = false;
#if defined(_WIN64)
    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, _sKey, 0, KEY_READ |  KEY_WOW64_32KEY, &hKey))
        bResult = true;
    ::RegCloseKey(hKey);
#else
    UNUSED_ALWAYS(_hRoot);
    UNUSED_ALWAYS(_sKey);
#endif
    return bResult;
}

/**
 @brief     64비트 환경에서 32비트 레지스트리의 특정 키의 값이 존재하는지 확인
 @author    hang ryul lee 
 @date      2008.04.18
 @param    [in]    _hRoot         :Root Key
 @param    [in]    _sKey         :Sub Key
 @param    [in]    _sValName     :Value Name
 @return    true/false
*/
bool IsRegWow64ValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName)
{
    bool bResult = false;
#if defined(_WIN64)
    HKEY hKey;
    if (MAX_KEYNAME_LEN < _sKey.GetLength())    return false;
    if (MAX_VALUENAME_LEN < _sValName.GetLength())    return false;

    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, _sKey, 0, KEY_READ | KEY_WOW64_32KEY, &hKey))
    {        
        if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, _sValName, NULL, NULL, NULL, NULL))    
            bResult = true;        
        ::RegCloseKey(hKey);
    }
#else
    UNUSED_ALWAYS(_hRoot);    
    UNUSED_ALWAYS(_sKey);    
    UNUSED_ALWAYS(_sValName);    
#endif
    return bResult;
}

/**
 @brief     64비트 환경에서 32비트 레지스트리의 특정 키 삭제
 @author    hang ryul lee
 @date      2008.04.18
 @param    [in]    _hRoot         :Root Key
 @param    [in]    _sKey         :삭제하려는 key의 부모키
 @param    [in]    _sDelKey     :삭제하려는 key
 @return    true/false
 @note        지정한 키 뿐 아니라 키에 포함된 모든 값들을 삭제한다.\n
            서브키의 삭제 여부는 운영체제에 따라 다르다.
*/
bool DelRegWow64Key(const HKEY _hRoot, const CString &_sKey, const CString &_sDelKey)
{
    bool bResult = false;
#if defined(_WIN64)
    HKEY hKey;
    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, _sKey, 0, KEY_READ | KEY_WRITE | KEY_WOW64_32KEY, &hKey))
    {
        if (ERROR_SUCCESS == ::RegDeleteKey(hKey, _sDelKey))
            bResult = true;
        ::RegCloseKey(hKey);
    }
#else
    UNUSED_ALWAYS(_hRoot);
    UNUSED_ALWAYS(_sKey);
    UNUSED_ALWAYS(_sDelKey);
#endif
    return bResult;
}

/**
 @brief     64비트 환경에서 32비트 레지스트리의 특정 값 삭제
 @author    hang ryul lee
 @date      2008.04.18
 @param    [in]    _hRoot         :Root Key
 @param    [in]    _sKey         :Sub Key
 @param    [in]    _sValueName  :Value Name
 @return    true/false
*/
bool DelRegWow64Value(const HKEY _hRoot, const CString &_sKey, const CString &_sValueName)
{
    bool bResult = false;
#if defined(_WIN64)
    HKEY hKey;

    if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, _sKey, 0, KEY_READ | KEY_WRITE | KEY_WOW64_32KEY, &hKey))
    {
        if (ERROR_SUCCESS == ::RegDeleteValue(hKey, _sValueName))
            bResult = true;
        ::RegCloseKey(hKey);
    }
#else
    UNUSED_ALWAYS(_hRoot);
    UNUSED_ALWAYS(_sKey);
    UNUSED_ALWAYS(_sValueName);
#endif
    return bResult;
}

/**
 @brief     value Type을 string으로 변환해 준다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _dwType    value type
 @return    stirng
*/
CString RegValueTypeToText(const DWORD _dwType)
{
    switch (_dwType)
    {
    case    REG_NONE                :    return _T("REG_NONE");
    case    REG_SZ                  :    return _T("REG_SZ");
    case    REG_EXPAND_SZ           :    return _T("REG_EXPAND_SZ");
    case    REG_BINARY              :    return _T("REG_BINARY");
    case    REG_DWORD               :    return _T("REG_DWORD");
    case    REG_DWORD_BIG_ENDIAN    :    return _T("REG_DWORD_BIG_ENDIAN");
    case    REG_QWORD               :    return _T("REG_QWORD");
    case    REG_LINK                :    return _T("REG_LINK");
    case    REG_MULTI_SZ            :    return _T("REG_MULTI_SZ");
    case    REG_RESOURCE_LIST       :    return _T("REG_RESOURCE_LIST");
    case    REG_FULL_RESOURCE_DESCRIPTOR    :    return _T("REG_FULL_RESOURCE_DESCRIPTOR");
    case    REG_RESOURCE_REQUIREMENTS_LIST  :    return _T("REG_RESOURCE_REQUIREMENTS_LIST");
    default:    return _T("");
    }
}

/**
 @brief     string을 value type으로 전환해 준다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in] _sType string type
 @return    DWORD
*/
DWORD    TextToRegValueType(const CString &_sType)
{
    if (0 == _sType.CompareNoCase(_T("REG_NONE")))
        return REG_NONE;
    if (0 == _sType.CompareNoCase(_T("REG_SZ")))
        return REG_SZ;
    if (0 == _sType.CompareNoCase(_T("REG_EXPAND_SZ")))
        return REG_EXPAND_SZ;
    if (0 == _sType.CompareNoCase(_T("REG_BINARY")))
        return REG_BINARY;
    if (0 == _sType.CompareNoCase(_T("REG_DWORD")))
        return REG_DWORD;
    if (0 == _sType.CompareNoCase(_T("REG_DWORD_BIG_ENDIAN")))
        return REG_DWORD_BIG_ENDIAN;
    if (0 == _sType.CompareNoCase(_T("REG_QWORD")))
        return REG_QWORD;
    if (0 == _sType.CompareNoCase(_T("REG_LINK")))
        return REG_LINK;
    if (0 == _sType.CompareNoCase(_T("REG_MULTI_SZ")))
        return REG_MULTI_SZ;
    if (0 == _sType.CompareNoCase(_T("REG_RESOURCE_LIST")))
        return REG_RESOURCE_LIST;
    if (0 == _sType.CompareNoCase(_T("REG_FULL_RESOURCE_DESCRIPTOR")))
        return REG_FULL_RESOURCE_DESCRIPTOR;
    if (0 == _sType.CompareNoCase(_T("REG_RESOURCE_REQUIREMENTS_LIST")))
        return REG_RESOURCE_REQUIREMENTS_LIST;

    return REG_NONE;
}

/**
 @brief     string 을 HKEY로 변환.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in]  _sText    string
 @note      변환한 HKEY
*/
HKEY TextToHKEY(const CString &_sText)
{
    CString sText = _sText;
    sText.Trim();
    sText = sText.MakeUpper();

    if (0 == sText.CompareNoCase(_T("HKEY_CLASSES_ROOT")))          return HKEY_CLASSES_ROOT;
    else if (0 == sText.CompareNoCase(_T("HKEY_CURRENT_USER")))     return HKEY_CURRENT_USER;
    else if (0 == sText.CompareNoCase(_T("HKEY_LOCAL_MACHINE")))    return HKEY_LOCAL_MACHINE;
    else if (0 == sText.CompareNoCase(_T("HKEY_USERS")))            return HKEY_USERS;
    else if (0 == sText.CompareNoCase(_T("HKEY_PERFORMANCE_DATA"))) return HKEY_PERFORMANCE_DATA;
    else if (0 == sText.CompareNoCase(_T("HKEY_CURRENT_CONFIG")))   return HKEY_CURRENT_CONFIG;
    else if (0 == sText.CompareNoCase(_T("HKEY_DYN_DATA")))         return HKEY_DYN_DATA;
    else throw (_sText + _T("Invalid HKEY description"));
}

/**
 @brief     HKEY를 string으로 변환.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in]  _sText    string
 @note      변환한 문자열
*/
CString HKEYToText(const HKEY _hKey)
{
    if (HKEY_CLASSES_ROOT == _hKey)             return _T("HKEY_CLASSES_ROOT");
    else if (HKEY_CURRENT_USER == _hKey)        return _T("HKEY_CURRENT_USER");
    else if (HKEY_LOCAL_MACHINE == _hKey)       return _T("HKEY_LOCAL_MACHINE");
    else if (HKEY_USERS == _hKey)               return _T("HKEY_USERS");
    else if (HKEY_PERFORMANCE_DATA == _hKey)    return _T("HKEY_PERFORMANCE_DATA");
    else if (HKEY_CURRENT_CONFIG == _hKey)      return _T("HKEY_CURRENT_CONFIG");
    else if (HKEY_DYN_DATA == _hKey)            return _T("HKEY_DYN_DATA");
    else throw (_T("Invalide HKEY"));
}

/**
 @brief     RootKey를 로그온 사용자에게 맞게 맵핑해준다.
 @author    hang ryul lee
 @date      2009.01.09
 @param     [in] _hRoot: RootKey
 @return	맵핑된 Root Key
 @note      루트키가 HKEY_CURRENT_USER 일 경우에만 수행.
 @note		현재 프로세스 실행 권한이 시스템권한이라면, HKEY_CURRENT_USER 키를 현재세션의\n
			로그온 유저에 맞게끔 맵핑한다. (HKEY_USERS 로 설정)
*/
HKEY RootKeyMapping(const HKEY _hRoot)
{
	HKEY hMappingRoot = _hRoot;
	if (HKEY_CURRENT_USER != hMappingRoot)
		return hMappingRoot;
	
	CWinOsVersion OsVer;
	if ((!OsVer.IsWinNT()) || (!CProcessAuth::IsSystem()))
		return hMappingRoot;

	hMappingRoot = HKEY_USERS;
	return hMappingRoot;
}

/**
 @brief     SubKey를 로그온 사용자에게 맞게 맵핑해준다.
 @author    hang ryul lee
 @date      2009.01.09
 @param     [in] _hRoot: RootKey
 @param		[in] _sSubKey: SubKey
 @return	맵핑된 SubKey
 @note      루트키가 HKEY_CURRENT_USER 일 경우에만 수행.
 @note		현재 프로세스 실행 권한이 시스템권한이라면, 서브키를 현재 로그온 세션의 \n
			로그온 유저에 맞게끔 맵핑한다. \n
			(로그온 유저 SID\SubKey)
*/
CString SubKeyMapping(const HKEY _hRoot, const CString &_sSubKey)
{
	CString sMappingKey = _sSubKey;
	if (HKEY_CURRENT_USER != _hRoot)
		return sMappingKey;

	CWinOsVersion OsVer;
	if ((!OsVer.IsWinNT()) || (!CProcessAuth::IsSystem()))
		return sMappingKey;

	CWTSSessions Sessions;
	CImpersonator Impersonator;
	if (!Impersonator.CreateAsLogonUser(Sessions.GetActiveSessionId()))
		return sMappingKey;

	CString sLogonUserName = Impersonator.GetImpersonatedUserName();
	if (_T("") == sLogonUserName)
		return sMappingKey;

    CSid Sid(sLogonUserName);
    CString sSid = Sid.Sid();
    if (_T("") != _sSubKey)
        sMappingKey = IncludeTrailingBackslash(sSid) + _sSubKey;
    else
        sMappingKey = sSid;

	Impersonator.Free();
	return sMappingKey;
}

/**
 @brief     value string을 항목에 맞게 parse 해준다.
 @author    hang ryul lee
 @date      2008.04.28
 @param     [in]  _sValueStr    string type
 @param     [out] _pName        Value Name
 @param     [out] _pType        value type
 @param     [out] _pData        value Data
 @note      MULTI_SZ의 경우 multi string 이 TAB 으로 구분된 string으로 pData에 저장된다.
 @note      필요없는 값의 경우 NULL값을 전달하여 필요한 값만 받을 수 있다.
*/
void ParseValueString(const CString &_sValueStr, CString *_pName, CString *_pType, CString *_pData)
{
    if ((NULL == _pName) && (NULL == _pType) && (NULL == _pData))
        return ;

    CString sStr = _sValueStr;
    if (_T("") == sStr)
        return ;
    
    INT nPos = sStr.Find(TAB);
    if (-1 >= nPos)
        return ;
        
    if (NULL != _pName)        
        *_pName = sStr.Left(nPos);;
    sStr = sStr.Right(sStr.GetLength() - (nPos+1));
    
    
    nPos = sStr.Find(TAB);
    if (-1 >= nPos)
        return ;

    if (NULL != _pType)        
        *_pType = sStr.Left(nPos);
    sStr = sStr.Right(sStr.GetLength() - (nPos+1));
    
    
    nPos = sStr.ReverseFind(TAB);
    if (-1 >= nPos)
        return ;

    if (NULL != _pData)        
        *_pData = sStr.Left(nPos);;
}

/**
@brief     registry String 값을 write한다 (64bit OS에서 32bit 프로세스가 레지스트리에 값을 write)
@author    hhheo
@date      2010.10.10
@param     [in]    _hRoot         :Root Key
@param     [in]    _sKey         :Sub Key
@param     [in]    _sValName     :Value Name
@param     [in]    _sValue         :write할 값
@param     [in]    _bForce         :Key가 존재하지 않을경우 자동생성 여부(default = false)
@return    true/false
@note      update 2008.03.04 yunmi\n
				기존 SetRegStringValue  함수에서 KEY_WOW64_64KEY 옵션만 추가됨

*/
bool    SetRegStringValueWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue, const bool _bForce)
{
	bool bResult = false;

	CString sKey = ExcludeTrailingBackslash(_sKey);
	CString sName = _sValName;
	CString sValue = _sValue;

	sKey.Trim();
	sValue.Trim();
	sName.Trim();

	HKEY hKey;

	if (MAX_KEYNAME_LEN < sKey.GetLength())    return false;
	if (MAX_VALUENAME_LEN < sName.GetLength()) return false;
	if (MAX_VALUE_LEN < sValue.GetLength())    return false;

	if (!IsRegKeyExistsWow64(_hRoot, sKey))
	{
		if (_bForce)
			CreateRegKeyWow64(_hRoot, sKey);
		else return false;
	}

	if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_WRITE | KEY_WOW64_64KEY, &hKey))
		return false;

	LPTSTR   szValue = sValue.GetBuffer(sValue.GetLength());
	DWORD    dwSize = (sValue.GetLength()+1) * sizeof(TCHAR);
	if (ERROR_SUCCESS == ::RegSetValueEx(hKey, sName, 0, REG_SZ, (LPBYTE)szValue, dwSize))
		bResult = true;

	sValue.ReleaseBuffer();
	::RegCloseKey(hKey);
	return bResult;
}

/**
@brief     registry string 값을 구한다.
@author    hhheo
@date      2010.10.10
@param     [in] _hRoot        :Root Key
@param     [in] _sKey         :Sub Key
@param     [in] _sValName     :Value Name
@param     [in] _sDef         :default = _T("")
@return    읽은 레지스트리값
@note      update 2008.04.24 yunmi\n
				.MULTI_SZ의 경우 NULL문자열을 NEWLINE으로 변환하여 string을 반환한다.
				기존 GetRegStringValue  함수에서 KEY_WOW64_64KEY 옵션만 추가됨

*/
CString GetRegStringValueWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sDef)
{
	CString sResult = _sDef;

	CString sKey = _sKey;
	CString sName = _sValName;
	sKey.Trim();
	sName.Trim();

	HKEY hKey;
	if (ERROR_SUCCESS == ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ | KEY_WOW64_64KEY , &hKey))
	{

		DWORD    dwSize = (MAX_VALUE_LEN +1) * sizeof(TCHAR);
		TCHAR    szValue[MAX_VALUE_LEN +1] = {0,};
		DWORD    dwType = REG_NONE;

		if (ERROR_SUCCESS == ::RegQueryValueEx(hKey, sName, NULL, &dwType, (LPBYTE)&szValue, &dwSize))
		{
			if ((REG_SZ == dwType) || (REG_EXPAND_SZ == dwType))
				sResult = szValue;

			else if (REG_MULTI_SZ == dwType)
			{
				DWORD i=0;
				for (i = 0; i< (dwSize/sizeof(TCHAR)); i++)
				{
					if (_T('\0') == szValue[i])
						szValue[i] = NEWLINE;
				}
				szValue[i-1] = _T('\0');
				sResult = szValue;
			}
		}

		::RegCloseKey(hKey);
	}
	return sResult;
}

/**
@brief     특정 키가 존재하는지 확인(64bit OS에서 32bit 프로세스가 레지스트리 확인)
@author   hhheo
@date      2010.10.10
@param     [in] _hRoot     Root Key
@param     [in] _sKey      Sub Key
@return    true/false
@note     기존 IsRegKeyExists  함수에서 KEY_WOW64_64KEY 옵션만 추가됨
*/
bool IsRegKeyExistsWow64(const HKEY _hRoot, const CString &_sKey)
{
	CString sKey = _sKey;
	sKey.Trim();

	HKEY hKey;
	if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_READ | KEY_WOW64_64KEY , &hKey))
		return false;

	::RegCloseKey(hKey);
	return true;
}

/**
@brief     registry key를 생성한다.(64bit OS에서 32bit 프로세스가 레지스트리 생성시)
@author    hhheo
@date      2010.10.10
@param     [in] _hRoot        :Root Key
@param     [in] _sKey         :생성할 키 path
@return    true / false
@note      생성할 키의 부모키의 존재유무에 상관없이 무조건 생성한다.
			  기존 CreateRegKey  함수에서 KEY_WOW64_64KEY 옵션만 추가됨
*/
bool CreateRegKeyWow64(const HKEY _hRoot, const CString &_sKey)
{
	HKEY hKey;
	DWORD dwDisp=0;

	CString sKey = _sKey;

	sKey.Trim();
	sKey = ExcludeTrailingBackslash(sKey);
	sKey.Trim();

	if ((HKEY_LOCAL_MACHINE == _hRoot) || (HKEY_USERS == _hRoot))
	{
		if (0 > sKey.Find(_T('\\')))
			return false;
	}

	if (MAX_KEYNAME_LEN < sKey.GetLength())    
		return false;

	if (ERROR_SUCCESS != ::RegCreateKeyEx(_hRoot, sKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_WOW64_64KEY ,
		NULL, &hKey, &dwDisp))
		return false;

	::RegCloseKey(hKey);
	return true;
}

/**
@brief     registry DWORD 값을 write한다 (64bit OS에서 32bit 프로세스가 레지스트리에 값을 write)
@author    heo hyo haeng
@date      2012.11.20
@param     [in] _hRoot     Root Key
@param     [in] _sKey      Sub Key
@param     [in] _sValName  Value Name
@param     [in] _sValue    write할 값
@param     [in] _bForce    Key가 존재하지 않을경우 자동생성 여부(default = false)
@return    true/false   

.Key가 존재하지 않을 경우 처리에 대한 파라미터 추가.\n
.default 값으로는 false 를 반환하고 종료한다.
*/
bool SetRegDWORDValueWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const DWORD &_dwValue, const bool _bForce)
{
	bool bResult = false;

	CString sKey = ExcludeTrailingBackslash(_sKey);
	CString sName = _sValName;

	sKey.Trim();
	sName.Trim();

	HKEY hKey;

	if (MAX_KEYNAME_LEN < sKey.GetLength())    return false;
	if (MAX_VALUENAME_LEN < _sValName.GetLength())    return false;

	if (!IsRegKeyExistsWow64(_hRoot, sKey))
	{
		if (_bForce)
			CreateRegKeyWow64(_hRoot, sKey);
		else return false;
	}

	if (ERROR_SUCCESS != ::RegOpenKeyEx(_hRoot, sKey, 0, KEY_WRITE | KEY_WOW64_64KEY, &hKey))
		return false;

	if (ERROR_SUCCESS == ::RegSetValueEx(hKey, sName, 0, REG_DWORD, (LPBYTE)&_dwValue, sizeof(DWORD)))
		bResult = true;

	::RegCloseKey(hKey);
	return bResult;
}