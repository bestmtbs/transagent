
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      RunsRegistry.h 
 @brief     LOCAL_MACHINE의 Run, RunOnce, RunOnceEx, RunServices 를 관리하는 
            Class 정의 파일 (자동 시작 프로그램)
 @author    hang ryul lee
 @date      create 2011.04.15 
*/

#pragma once

/**
 @brief     CRun Class
 @author    hang ryul lee
 @note      HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\CurrentVersion\\Run                        \n 
            (The command is this key is executed every time the computer is booted for all user)        \n 
            => 부팅시 모든 사용자를 위해 실행된다.                                                      \n 
	        HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\CurrentVersion\\RunOnce                    \n 
			(The command in this key is executed only one time no matter how many different             \n  
			 people log onto the computer. When the command has completed, it is never executed again)  \n 
            => 부팅시 로그온 하는 사람과는 상관없이 단 한번만 실행된다.                                 \n 
               실행후 레지스트리 값은 삭제된다.                                                         \n 
            HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx                 \n 
			=> 설치후 윈도우를 처음 실행시 실행되는 값, 사용자 설정 이전 및 윈도우 바탕화면 이전에      \n 
			   실행되는 콤포너트들을 의미 RunOnceEx 설정시 키값을 무조건 설정해야 하며 비스타의 경우    \n 
			   RunOnceEx, RunOnce는 관리자 권한으로 실행되고 Run은 user 권한으로 실행된다.              \n 
			   Vista에서 RunOnceEx 키가 정의되어 있지 않음                                              \n
            HKEY_LOCAL_MACHINE\\Software\\Microsoft\\Windows\CurrentVersion\\RunServices                \n 
			(The command in this key is executed as a background process when the logon dialog          \n 
			  box appears. It runs every time the computer is booted)                                   \n 
            => 부팅시 백그라운드 프로세스로 실행된다. 사용자가 로그온 하기전에 로드                     \n 
			   로그인전 자동으로 실행                                                                   \n 
			   Windows95, Windows98, Windows Me 에만 적용, 그 이후버전의 상위이 기술 적용               \n 
			   Vista에서 RunOnce의 값이 정의되어 있지 않음
			부팅순서 RunServices => 로그인 => RunOnce => Run  => 시작프로그램                           \n 
			http://support.microsoft.com/kb/232509  => RunOnceEx 사용법                                 \n  
			http://support.microsoft.com/kb/179365  => RunService 사용법                                \n 

           OS별 실행순서
            - Windows 98 SE                                                                             \n 
			  : RunServices => 로그인 => RunOnceEx =>RunOnce => 쉘 생성 => Run                          \n 
            - Windows NT Workstation Service Pack 6                                                     \n 
			  : 서비스 => 로그인 => RunOnce => 쉘 생성 => Run                                           \n 
			    NT는 RunOnceEx 값을 실행하지 못함.                                                      \n 
            - Windows 2000 Profesional Service Pack 4                                                   \n 
			  : 서비스 => 로그인 => RunOnceEx => RunOnce => 쉘 생성 => Run                              \n 
            - Windows 2000 Server Service Pack 4                                                        \n 
			  : 서비스 => 로그인 => RunOnceEx => RunOnce => 쉘 생성 =>  Run                             \n 
            - Windows XP Professional Service Pack 2                                                    \n
			  : 서비스 => 로그인 => RunOnceEx => RunOnce => 쉘 생성 =>  Run                             \n 
            - Windows 2003 Enterprise x86                                                               \n 
			  : 서비스 => 로그인 => RunOnceEx => RunOnce => 쉘 생성 =>  Run                             \n 
            - Windows Vista Home Basic                                                                  \n 
			  : 서비스 => 로그인 => RunOnceEx => RunOnce => 쉘 생성 =>  Run                             \n 

*/

class CRunRegistry
{
public:
	CRunRegistry(void);
	virtual ~CRunRegistry(void);

	//32, 64bit Registry 접근함수
	static bool SetRun(const HKEY _hRoot, const CString &_sValName, const CString &_sValue);
	static bool SetRunOnce(const HKEY _hRoot, const CString &_sValName, const CString &_sValue);
	static bool SetRunOnceEx(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue);
	static bool SetRunServices(const HKEY _hRoot, const CString &_sValName, const CString &_sValue);

	static bool DeleteRun(const HKEY _hRoot, const CString &_sValName);
	static bool DeleteRunOnce(const HKEY _hRoot, const CString &_sValName);
	static bool DeleteRunOnceEx(const HKEY _hRoot, const CString &_sKey);
	static bool DeleteRunServices(const HKEY _hRoot, const CString &_sValName);

	static bool IsRunValueExists(const HKEY _hRoot, const CString &_sValName);
	static bool IsRunOnceValueExists(const HKEY _hRoot, const CString &_sValName);
	static bool IsRunOnceExValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName);
	static bool IsRunServicesValueExist(const HKEY _hRoot, const CString &_sValName);

	static CString GetRunValue(const HKEY _hRoot, const CString &_sValName);
	static CString GetRunOnceValue(const HKEY _hRoot, const CString &_sValName);
	static CString GetRunOnceExValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName);
	static CString GetRunServicesValue(const HKEY _hRoot, const CString &_sValName);

	//64bit 환경에서 32bit Registry 접근함수, RunService 키는 64bit OS에서 존재하지 않음
	static bool SetRunWow64(const HKEY _hRoot, const CString &_sValName, const CString &_sValue);
	static bool SetRunOnceWow64(const HKEY _hRoot, const CString &_sValName, const CString &_sValue);
	static bool SetRunOnceExWow64(const HKEY _hRoot, const CString &_sKey, const CString &_sValName, const CString &_sValue);

	static bool DeleteRunWow64(const HKEY _hRoot, const CString &_sValName);
	static bool DeleteRunOnceWow64(const HKEY _hRoot, const CString &_sValName);
	static bool DeleteRunOnceExWow64(const HKEY _hRoot, const CString &_sKey);

	static bool IsRunWow64ValueExists(const HKEY _hRoot, const CString &_sValName);
	static bool IsRunOnceWow64ValueExists(const HKEY _hRoot, const CString &_sValName);
	static bool IsRunOnceExWow64ValueExists(const HKEY _hRoot, const CString &_sKey, const CString &_sValName);

	static CString GetRunWow64Value(const HKEY _hRoot, const CString &_sValName);
	static CString GetRunOnceWow64Value(const HKEY _hRoot, const CString &_sValName);
	static CString GetRunOnceWow64ExValue(const HKEY _hRoot, const CString &_sKey, const CString &_sValName);
private:
	CRunRegistry(const CRunRegistry &_runs);                 /**< 복사 생성자 : 사용금지를 위해 private에 정의 */
	CRunRegistry& operator = (const CRunRegistry &_runs);    /**< 대입 연산자 : 사용금지를 위해 private에 정의 */
};
