#include "StdAfx.h"
#include "RegistryQueryForAppList.h"
#include "WinOsVersion.h"

CRegistryQueryForAppList::CRegistryQueryForAppList(void)
{
}

CRegistryQueryForAppList::~CRegistryQueryForAppList(void)
{
}

void CRegistryQueryForAppList::RegistryEnum(vector<INSTALLAPPINFO>& vecAppList)
{
	INT iSrcList = 0;

	if (m_bSrcList == TRUE)
	{
		iSrcList = 1;
	}

	HKEY hKey;
	LONG ret = ::RegOpenKeyEx(
		HKEY_LOCAL_MACHINE,     // local machine hive
		_T("Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), // uninstall key
		0,                      // reserved
		KEY_READ,               // desired access
		&hKey                   // handle to the open key
		);

	if(ret != ERROR_SUCCESS)
		return;

	DWORD dwIndex = 0;
	DWORD cbName = 1024;
	TCHAR szSubKeyName[1024];

	while ((ret = ::RegEnumKeyEx(
		hKey, 
		dwIndex, 
		szSubKeyName, 
		&cbName, 
		NULL,
		NULL, 
		NULL, 
		NULL)) != ERROR_NO_MORE_ITEMS)
	{
		if (ret == ERROR_SUCCESS)
		{
			HKEY hItem;
			if (::RegOpenKeyEx(hKey, szSubKeyName, 0, KEY_READ, &hItem) != ERROR_SUCCESS)
				continue;


			BOOL bSystemComponent = IsRegistryExistValue(hItem, _T("SystemComponent"));
			BOOL bDispName = IsRegistryExistValue(hItem, _T("DisplayName"));

			tstring name = RegistryQueryValue(hItem, _T("DisplayName"));
			tstring publisher = RegistryQueryValue(hItem, _T("Publisher"));
			tstring version = RegistryQueryValue(hItem, _T("DisplayVersion"));
			tstring location = RegistryQueryValue(hItem, _T("InstallLocation"));
			tstring installdate = RegistryQueryValue(hItem, _T("InstallDate"));

			if (bDispName == TRUE && bSystemComponent == FALSE)
			{
				INSTALLAPPINFO info;
				info.strAppName = name.c_str();
				info.strAppCompany = publisher.c_str();
				info.strAppVersion = version.c_str();
				info.strAppLocation = location.c_str();
				info.strAppInsDate = installdate.c_str();

				m_vecInstallInfo[iSrcList].push_back(info);

				ATLTRACE(_T("install list count = %d\n"),m_vecInstallInfo[iSrcList].size());

			}
			::RegCloseKey(hItem);
		}
		dwIndex++;
		cbName = 1024;
	}
	::RegCloseKey(hKey);

	m_bSrcList = !m_bSrcList;

	vecAppList = m_vecInstallInfo[iSrcList];
}
void CRegistryQueryForAppList::RegistryEnum()
{
	INT iSrcList = 0;

	if (m_bSrcList == TRUE)
	{
		iSrcList = 1;
	}

	HKEY hKey;
	LONG ret = ::RegOpenKeyEx(
		HKEY_LOCAL_MACHINE,     // local machine hive
		_T("Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), // uninstall key
		0,                      // reserved
		KEY_READ,               // desired access
		&hKey                   // handle to the open key
		);

	if(ret != ERROR_SUCCESS)
		return;

	DWORD dwIndex = 0;
	DWORD cbName = 1024;
	TCHAR szSubKeyName[1024];

	while ((ret = ::RegEnumKeyEx(
		hKey, 
		dwIndex, 
		szSubKeyName, 
		&cbName, 
		NULL,
		NULL, 
		NULL, 
		NULL)) != ERROR_NO_MORE_ITEMS)
	{
		if (ret == ERROR_SUCCESS)
		{
			HKEY hItem;
			if (::RegOpenKeyEx(hKey, szSubKeyName, 0, KEY_READ, &hItem) != ERROR_SUCCESS)
				continue;


			BOOL bSystemComponent = IsRegistryExistValue(hItem, _T("SystemComponent"));
			BOOL bDispName = IsRegistryExistValue(hItem, _T("DisplayName"));

			tstring name = RegistryQueryValue(hItem, _T("DisplayName"));
			tstring publisher = RegistryQueryValue(hItem, _T("Publisher"));
			tstring version = RegistryQueryValue(hItem, _T("DisplayVersion"));
			tstring location = RegistryQueryValue(hItem, _T("InstallLocation"));
			tstring installdate = RegistryQueryValue(hItem, _T("InstallDate"));

			if (bDispName == TRUE && bSystemComponent == FALSE)
			{
				INSTALLAPPINFO info;
				info.strAppName = name.c_str();
				info.strAppCompany = publisher.c_str();
				info.strAppVersion = version.c_str();
				info.strAppLocation = location.c_str();
				info.strAppInsDate = installdate.c_str();

				m_vecInstallInfo[iSrcList].push_back(info);

				ATLTRACE(_T("install list count = %d\n"),m_vecInstallInfo[iSrcList].size());

			}
			::RegCloseKey(hItem);
		}
		dwIndex++;
		cbName = 1024;
	}
	::RegCloseKey(hKey);

	m_bSrcList = !m_bSrcList;

}

void CRegistryQueryForAppList::RegistryEnumWow6432(vector<INSTALLAPPINFO>& vecAppList)
{
	INT iSrcList = 0;

	if (m_bSrcWowList == TRUE)
	{
		iSrcList = 1;
	}

	HKEY hKey;
	LONG ret = ::RegOpenKeyEx(
		HKEY_LOCAL_MACHINE,     // local machine hive
		_T("Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), // uninstall key
		0,                      // reserved
		KEY_READ,               // desired access
		&hKey                   // handle to the open key
		);

	if(ret != ERROR_SUCCESS)
		return;

	DWORD dwIndex = 0;
	DWORD cbName = 1024;
	TCHAR szSubKeyName[1024];

	while ((ret = ::RegEnumKeyEx(
		hKey, 
		dwIndex, 
		szSubKeyName, 
		&cbName, 
		NULL,
		NULL, 
		NULL, 
		NULL)) != ERROR_NO_MORE_ITEMS)
	{
		if (ret == ERROR_SUCCESS)
		{
			HKEY hItem;
			if (::RegOpenKeyEx(hKey, szSubKeyName, 0, KEY_READ, &hItem) != ERROR_SUCCESS)
				continue;


			BOOL bSystemComponent = IsRegistryExistValue(hItem, _T("SystemComponent"));
			BOOL bDispName = IsRegistryExistValue(hItem, _T("DisplayName"));

			tstring name = RegistryQueryValue(hItem, _T("DisplayName"));
			tstring publisher = RegistryQueryValue(hItem, _T("Publisher"));
			tstring version = RegistryQueryValue(hItem, _T("DisplayVersion"));
			tstring location = RegistryQueryValue(hItem, _T("InstallLocation"));
			tstring installdate = RegistryQueryValue(hItem, _T("InstallDate"));

			if (bDispName == TRUE && bSystemComponent == FALSE)
			{
				INSTALLAPPINFO info;
				info.strAppName = name.c_str();
				info.strAppCompany = publisher.c_str();
				info.strAppVersion = version.c_str();
				info.strAppLocation = location.c_str();
				info.strAppInsDate = installdate.c_str();

				m_vecInstallInfoWow[iSrcList].push_back(info);

				ATLTRACE(_T("install list count = %d\n"),m_vecInstallInfoWow[iSrcList].size());

			}
			// 
			// 			if(!name.empty())
			// 			{
			// 				tcout << name << endl;
			// 				tcout << "  - " << publisher << endl;
			// 				tcout << "  - " << version << endl;
			// 				tcout << "  - " << location << endl;
			// 				tcout << endl;
			// 			}

			::RegCloseKey(hItem);

			// 			ATLTRACE(_T("%d : %s - %s - %s - %s\n"),dwIndex,name.c_str(),publisher.c_str(),version.c_str(),location.c_str());
		}
		dwIndex++;
		cbName = 1024;
	}
	::RegCloseKey(hKey);

	m_bSrcWowList = !m_bSrcWowList;

	vecAppList = m_vecInstallInfoWow[iSrcList];
}

void CRegistryQueryForAppList::RegistryEnumWow6432()
{
	INT iSrcList = 0;

	if (m_bSrcWowList == TRUE)
	{
		iSrcList = 1;
	}
	HKEY hKey;
	LONG ret = ::RegOpenKeyEx(
		HKEY_LOCAL_MACHINE,     // local machine hive
		_T("Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall"), // uninstall key
		0,                      // reserved
		KEY_READ,               // desired access
		&hKey                   // handle to the open key
		);

	if(ret != ERROR_SUCCESS)
		return;

	DWORD dwIndex = 0;
	DWORD cbName = 1024;
	TCHAR szSubKeyName[1024];

	while ((ret = ::RegEnumKeyEx(
		hKey, 
		dwIndex, 
		szSubKeyName, 
		&cbName, 
		NULL,
		NULL, 
		NULL, 
		NULL)) != ERROR_NO_MORE_ITEMS)
	{
		if (ret == ERROR_SUCCESS)
		{
			HKEY hItem;
			if (::RegOpenKeyEx(hKey, szSubKeyName, 0, KEY_READ, &hItem) != ERROR_SUCCESS)
				continue;


			BOOL bSystemComponent = IsRegistryExistValue(hItem, _T("SystemComponent"));
			BOOL bDispName = IsRegistryExistValue(hItem, _T("DisplayName"));

			tstring name = RegistryQueryValue(hItem, _T("DisplayName"));
			tstring publisher = RegistryQueryValue(hItem, _T("Publisher"));
			tstring version = RegistryQueryValue(hItem, _T("DisplayVersion"));
			tstring location = RegistryQueryValue(hItem, _T("InstallLocation"));
			tstring installdate = RegistryQueryValue(hItem, _T("InstallDate"));

			if (bDispName == TRUE && bSystemComponent == FALSE)
			{
				INSTALLAPPINFO info;
				info.strAppName = name.c_str();
				info.strAppCompany = publisher.c_str();
				info.strAppVersion = version.c_str();
				info.strAppLocation = location.c_str();
				info.strAppInsDate = installdate.c_str();

				m_vecInstallInfoWow[iSrcList].push_back(info);

				ATLTRACE(_T("install list count = %d\n"),m_vecInstallInfoWow[iSrcList].size());

			}
			// 
			// 			if(!name.empty())
			// 			{
			// 				tcout << name << endl;
			// 				tcout << "  - " << publisher << endl;
			// 				tcout << "  - " << version << endl;
			// 				tcout << "  - " << location << endl;
			// 				tcout << endl;
			// 			}

			::RegCloseKey(hItem);

			// 			ATLTRACE(_T("%d : %s - %s - %s - %s\n"),dwIndex,name.c_str(),publisher.c_str(),version.c_str(),location.c_str());
		}
		dwIndex++;
		cbName = 1024;
	}

	::RegCloseKey(hKey);

	m_bSrcWowList = !m_bSrcWowList;

}

CString CRegistryQueryForAppList::GetInstallAppList()
{
	CString strResult = _T("");
	vector<INSTALLAPPINFO> appWowList;
	CWinOsVersion osVer;

	if (osVer.Is32bit()) {
		RegistryEnum(appWowList);
	} else {
		RegistryEnum(appWowList);
		RegistryEnumWow6432(appWowList);
	}

	vector<INSTALLAPPINFO>::iterator iter;

	for (iter = appWowList.begin(); iter != appWowList.end(); ++iter)
	{
		strResult += iter->strAppName;
		strResult += _T("|");
	}
	return strResult;
}

BOOL CRegistryQueryForAppList::IsRegistryExistValue(HKEY hKey,LPCTSTR szName)
{
	DWORD dwType;
	DWORD dwSize = 0;

	if( RegQueryValueEx(hKey,szName,NULL,&dwType,NULL,&dwSize) == ERROR_SUCCESS)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

tstring CRegistryQueryForAppList::RegistryQueryValue(HKEY hKey,LPCTSTR szName)
{
	tstring value;

	DWORD dwType;
	DWORD dwSize = 0;

	if (::RegQueryValueEx(
		hKey,                   // key handle
		szName,                 // item name
		NULL,                   // reserved
		&dwType,                // type of data stored
		NULL,                   // no data buffer
		&dwSize                 // required buffer size
		) == ERROR_SUCCESS && dwSize > 0)
	{
		value.resize(dwSize);

		::RegQueryValueEx(
			hKey,                   // key handle
			szName,                 // item name
			NULL,                   // reserved
			&dwType,                // type of data stored
			(LPBYTE)&value[0],      // data buffer
			&dwSize                 // available buffer size
			);
	}

	return value;
}