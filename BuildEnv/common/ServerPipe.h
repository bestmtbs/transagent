
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.08.16		
 ********************************************************************************/

/**
 @file      ServerPipe.h 
 @brief    CServerPipe 정의 클래스

 @author    hang ryul lee
 @date      create 2011.08.16
 @note      
*/

#pragma once
#include "Pipe.h"


#define		PIPE_BUF_SIZE			4096
#define		PIPE_TIME_WAIT		5*1000



class CServerPipe
{
public:
	CServerPipe();																	             		 /**< 기본 생성자 */
	CServerPipe(TCHAR* _pszPipeName);											/**< 생성자 */
    virtual ~CServerPipe();																/**< 파괴자 */

	HANDLE m_hPipeEvent;// Kevin(2013-5-27)
	HANDLE m_hPipeNewEvent;// Kevin(2013-5-27)
	
public:
/*****************Server ******************************************************/
	int InitializeNamedPipe();
	int ListenNamedPipe();
	
	void PipeServerStartUp();
	void PipeServerNewStartUp();
	void SetExitPipeServer();
	

	virtual BOOL OnSetData(void * pData) = 0;

	virtual BOOL OnReceiveData() = 0;
	virtual BOOL OnReceiveHeader() = 0;
	virtual BOOL OnReceiveBody() = 0;
	virtual BOOL OnSendData() = 0;

	HANDLE		m_hPipe;							// Pipe Handle
	TCHAR	m_szPipeName[256];					// Pipe Name
private:
	BOOL		m_bPipeServerExit;			//hhh(2013.06.03)- pipe서버 종료 플래그

    CServerPipe(const CServerPipe &_rPipeServer);										 /**< 복사 생성자 */
    CServerPipe& operator = (const CServerPipe &_rPipeServer);					/**< 대입 연산자 */
	int  ClientConnectNamedPipe();

};