/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      Auth.cpp 
 @brief     CProcessAuth / CSession class 구현 파일
 
            권한 검사 관련 기능을 한다.

 @author    hang ryul lee 
 @date      create 2011.05.29
*/
#include "stdafx.h"
#include "Lm.h"
#include "Psapi.h"
#include "Auth.h"
#include "WinOSVersion.h"
#include "WTSSession.h"
#include "UtilsReg.h"
#include "UtilsFile.h"

/********************************************************************************
 @class     CProcessAuth  
 @brief     현재 실행중인 Process 권한 구함.
 @note      - 현재 프로세스의 실행권한에 대한 검사를 한다.\n
            - Windows 9x 의 경우 권한 개념이 없으므로 무조건 True 반환. (항상 권한 있음)
 @note      검증된 Os\n
            - Windows XP    (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows Vista (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows 2000  (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows NT    (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows 98\n
*********************************************************************************/

#define UNLEN       256                 // Maximum user name length (define "Lmcons.h")
#define MAX_PROCESS_COUNT   1024

/**
 @brief     현재 프로세스가 Admin 권한으로 실행되는지 확인
 @author    hang ryul lee
 @date      2008.05.08
 @return    true / false
 @note      현재 실행 Process의 토큰정보를 이용하여, 해당토큰에 Admin권한이 있는지 확인.
 @note      System의 경우 Admin 권한을 포함하기 때문에 True가 반환됨.
*/
bool CProcessAuth::IsAdmin()
{
    CWinOsVersion Os;
    // 9x 버전은 권한이 없으므로 무조건 true이다.
    if (!Os.IsWinNT()) return true;

    HANDLE hToken = NULL;
    BOOL bSuccess = FALSE;
    bool bResult = false;

    bSuccess = ::OpenThreadToken(::GetCurrentThread(), TOKEN_QUERY, TRUE, &hToken);
    if (!bSuccess)
    {
        if (ERROR_NO_TOKEN == ::GetLastError())
            bSuccess = ::OpenProcessToken(::GetCurrentProcess(), TOKEN_QUERY, &hToken);
    }

    if (!bSuccess) return bResult;

    DWORD dwSize = 0;
    PTOKEN_GROUPS Groups;
    ::GetTokenInformation(hToken, TokenGroups, 0, 0, &dwSize);
    Groups = reinterpret_cast<TOKEN_GROUPS *>(malloc(dwSize));
    ::ZeroMemory(Groups, dwSize);

    bSuccess = ::GetTokenInformation(hToken, TokenGroups, Groups, dwSize, &dwSize);
    if (bSuccess && (NULL != Groups))
    {
        PSID pSid = NULL;
        SID_IDENTIFIER_AUTHORITY ntauth = SECURITY_NT_AUTHORITY;
        ::AllocateAndInitializeSid (&ntauth, 2, SECURITY_BUILTIN_DOMAIN_RID, 
                                    DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &pSid);

        for (DWORD i = 0; i< Groups->GroupCount; i++)
        {
            if (::EqualSid(pSid, Groups->Groups[i].Sid))
            {
                bResult = true;
                break;
            }
        }
        ::FreeSid(pSid);
    }

    if (NULL != hToken)   
        ::CloseHandle(hToken);

    if (NULL != Groups)  ::free(Groups);

    return bResult;
}

/**
 @brief     현재 프로세스가 Admin 권한으로 실행되는지 UserName을 이용하여 확인.
 @author    hang ryul lee
 @date      2008.05.08
 @return    true / false
 @note      현재 실행된 프로세스의 user name을 얻어와 AdminGroup에 포함되는지 확인
 @note      System의 경우 UserName이 'SYSTEM'으로 구해짐.\n
            AdminGroup List에 해당 이름은 없으므로 false가 반환된다.
 @note      기존로직에 의해, 해당 세션 권한을 구하지 못할때 사용된다.\n
            (해당 함수는 프로세스의 권한 정보를 구하나, 기존로직을 따라 세션권한 구할때 사용)
*/
bool CProcessAuth::IsAdminByUserName()
{
    CWinOsVersion Os;
    // 9x 버전은 권한이 없으므로 무조건 true이다.
    if (!Os.IsWinNT()) return true;

    TCHAR szUserName[UNLEN +1] = {0,};
    DWORD dwSize = UNLEN +1;

    //해당 UserName은 현재 실행중인 프로세스의 UserName이다.
    ::GetUserName(szUserName, &dwSize);
    return IsAdminGroup(_T(""), szUserName);
}

/**
 @brief     현재 프로세스가 SYSTEM으로 실행되는지 확인
 @author    hang ryul lee
 @date      2008.05.08
 @return    true / false
 @note      현재 실행된 프로세스의 user name을 얻어와 SYSTEM인지 확인\n
            시스템으로 실행중일 경우 'SYSTEM'이라 나온다.
*/
bool CProcessAuth::IsSystem()
{
    CWinOsVersion Os;
    // 9x 버전은 권한이 없으므로 무조건 true이다.
    if (!Os.IsWinNT()) return true;

    TCHAR szUserName[UNLEN +1] = {0,};
    DWORD dwSize = UNLEN +1;

    ::GetUserName(szUserName, &dwSize);
    if (0 == _tcscmp(szUserName, _T("SYSTEM")))  return true;
    else return false;
}

/**
 @brief     현재 프로세스가 실행 권한이 있는지 확인
 @author    hang ryul lee
 @date      2008.05.08
 @return    true / false
 @note      File작업 등의 작업을 문제없이 실행할수있는 권한이 존재하는지 여부를 판단.
 @note      Windows Vista 의 경우 System 권한만 실행권한 가짐.\n
            그외의 경우 Admin 권한또는 System 권한일 경우 실행권한 가짐.
*/
bool CProcessAuth::IsExcuteAuth()
{
    //혹시 모를 예외를 위해아 true기반으로 한다.
    // 중요 모듈임으로 실행되어야한다.
    bool bResult = true;

    CWinOsVersion Os;
    // 9x 버전은 권한이 없으므로 무조건 true이다.
    if (!Os.IsWinNT()) return bResult;

    if (osWinVista == Os.GetProduct())
    {
        if (!IsSystem()) 
            bResult = false;
    }
    else
    {
        if ((!IsAdmin()) && (!IsSystem()))  
            bResult = false;
    }

    return bResult;
}

/**
 @brief     해당 UserName이 Admin 그룹에 속하는지 확인
 @author    hang ryul lee
 @date      2008.05.08
 @return    true / false
 @note      NetUserGetInfo 함수를 통해 UserName을 이용하여 정보를 구한 후\n
            해당 사용자가 Admin 권한인지 확인한다.
 @note      기존로직에 의해 DLL / 함수 로드실패시 true 반환함.
*/
bool CProcessAuth::IsAdminGroup(const CString &_sServer, const CString &_sUser)
{
    bool bResult = false;

    CWinOsVersion Os;
    if (!Os.IsWinNT())   return bResult;

    HINSTANCE hInst = NULL;
    hInst = ::LoadLibrary(_T("Netapi32.dll"));
    if (NULL == hInst)    return true;

    fpNetApiBufferFree pNetApiBufferFree = NULL;
    fpNetUserGetInfo pNetUserGetInfo = NULL;

    pNetUserGetInfo = (fpNetUserGetInfo) ::GetProcAddress(hInst, "NetUserGetInfo");;
    pNetApiBufferFree = (fpNetApiBufferFree) ::GetProcAddress(hInst, "NetApiBufferFree");
    if ((NULL == pNetUserGetInfo || NULL == pNetApiBufferFree))
    {
        if (NULL != hInst)
            ::FreeLibrary(hInst);
        return true;
    }
    
    NET_API_STATUS nGet_Status = ERROR_ACCESS_DENIED;
    LPUSER_INFO_1 pInfoBuf = NULL;
    #ifdef UNICODE
        nGet_Status = pNetUserGetInfo(_sServer, _sUser, 1, reinterpret_cast<LPBYTE *>(&pInfoBuf));
    #else
        LPWSTR szwUserName = NULL;
        LPWSTR szwServer = NULL;
        int nSizeUser=0, nSizeServer=0;

        if (0 < _sUser.GetLength())
         nSizeUser = _sUser.GetLength()+1;
        if (0 < _sServer.GetLength())
            nSizeServer = _sServer.GetLength()+1;

        try
        {
            if (0 != nSizeUser)
            {
             szwUserName = new WCHAR[nSizeUser];
                ::ZeroMemory(szwUserName, nSizeUser *sizeof(WCHAR));
             MultiByteToWideChar(CP_ACP, 0, _sUser, -1, szwUserName, nSizeUser);
            }
            if (0 != nSizeServer)
            {
                szwServer = new WCHAR[nSizeServer];
                ::ZeroMemory(szwServer, nSizeServer *sizeof(WCHAR));
             MultiByteToWideChar(CP_ACP, 0, _sServer, -1, szwServer, nSizeServer);
            }
         
         nGet_Status = pNetUserGetInfo(szwServer, szwUserName, 1, (LPBYTE *) &pInfoBuf);
        }
        catch(...)
        {
        }
        if (NULL != szwUserName)    delete [] szwUserName;
        if (NULL != szwServer)      delete [] szwServer;
    #endif

    if (NERR_Success != nGet_Status) return  bResult;

    if (NULL != pInfoBuf)
    {
        if (USER_PRIV_ADMIN == pInfoBuf->usri1_priv)
            bResult = true;
        
        pNetApiBufferFree(pInfoBuf);
        pInfoBuf = NULL;
    }

    if (NULL != hInst)  ::FreeLibrary(hInst);
    hInst = NULL;

 return bResult;

}

/********************************************************************************
 @class     CSessionAuth  
 @brief     현재 실행중인 Process 권한 구함.
 @note      - 현재 활성화 세션의 유저 권한에 대한 검사를 한다.\n
            - Windows 9x 의 경우 권한 개념이 없으므로 무조건 True 반환. (항상 권한 있음)
 @note      검증된 Os\n
            - Windows XP    (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows Vista (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows 2000  (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows NT    (사용자 권한: 사용자/system 실행, Admin권한 : 사용자/system 실행)\n
            - Windows 98\n
*********************************************************************************/

/**
 @brief     현재 활성화 세션에 대한 권한 구함.
 @author    hang ryul lee
 @date      2008.04.10
 @return    true / false
 @note      현재 Session의 쉘을 찾아 Token을 구한 후 Admin 권한이 포함되는지 확인.\n
 @note      기존 로직의 경우 Process Handle 얻기 실패시 IsAdminByUserName()함수 통함.\n
            -IsAdminByUserName() 함수 확인 결과 현재 세션의 user 확인하는것 아니라 현재 프로세스의\n
             User 이름으로 Admin Group인지 확인함.\n
            -현재 프로세스가 Admin인지 확인하는 함수로 확인됨.\n
            -IsAdminByUserName()함수 CProcessAuth class로 이동.\n
            -내부 오류 발생시 CProcessAuth::IsAdminByUserName() 호출.
 @note      기존 로직의 경우, shell Process를 PROCESS_ALL_ACCESS으로 새로 open하여 token구함.\n
            -PROCESS_QUERY_INFORMATION 권한이 있을경우 token open이 가능하여 생략함.
*/
bool CSessionAuth::IsAdmin()
{
    bool bResult = false;

    CWinOsVersion Os;
    if (!Os.IsWinNT())  return true;

    CString sShellName = _T("");
   

    sShellName = GetShellName();

    if (_T("") == sShellName)   return CProcessAuth::IsAdminByUserName();
    
    DWORD dwProcessIDs[MAX_PROCESS_COUNT] = {0,};
    DWORD dwSizeIDs= 0, dwCnt = 0; 
    fpEnumProcesses  pEnumProcesses = NULL;
    fpGetModuleBaseName pGetModuleBaseName = NULL;

    HINSTANCE hInst = ::LoadLibrary(_T("Psapi.dll"));
    if(NULL == hInst) return CProcessAuth::IsAdminByUserName();

    pEnumProcesses = (fpEnumProcesses)::GetProcAddress(hInst, "EnumProcesses");
    pGetModuleBaseName = (fpGetModuleBaseName)::GetProcAddress(hInst, GetModuleBaseNames);
    if ((NULL == pEnumProcesses) || (NULL == pGetModuleBaseName)) 
    {
        if(NULL != hInst)
            ::FreeLibrary(hInst);
        return CProcessAuth::IsAdminByUserName();
    }

    if (0 == pEnumProcesses(dwProcessIDs, MAX_PROCESS_COUNT, &dwSizeIDs))  
    {
        if(NULL != hInst)
            ::FreeLibrary(hInst);
        return CProcessAuth::IsAdminByUserName();
    }
    dwCnt = dwSizeIDs / sizeof(DWORD);

    for (DWORD i = 0; i< dwCnt; i++)
    {
        TCHAR szProcessName[MAX_PATH] = _T("unknown");
        HANDLE hProcess = NULL;

        hProcess = ::OpenProcess (PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwProcessIDs[i]);
        if (NULL == hProcess)   continue;
        
        if (0 == pGetModuleBaseName(hProcess, 0, szProcessName, MAX_PATH)) 
        {
            ::CloseHandle(hProcess);
            ::FreeLibrary(hInst);
            continue;
        }

        CString sExeFile = ExtractFileName(szProcessName);

        if ((0 == sExeFile.CompareNoCase(sShellName)) && (GetProcSessionID(::GetCurrentProcessId()) == GetProcSessionID(dwProcessIDs[i])))
        {
            /*HANDLE hShell = ::OpenProcess(PROCESS_ALL_ACCESS , FALSE, dwProcessIDs[i]);
            if (NULL == hShell)
            {
                ::CloseHandle(hProcess);
                return false;
            }*/
         
            HANDLE hToken = NULL;
            //if (0 == ::OpenProcessToken(hShell, TOKEN_QUERY | TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY, &hToken))
            if (0 == ::OpenProcessToken(hProcess, TOKEN_QUERY | TOKEN_DUPLICATE | TOKEN_ASSIGN_PRIMARY, &hToken))
            {
                ::CloseHandle(hProcess);
                //::CloseHandle(hShell);
                ::FreeLibrary(hInst);
                return CProcessAuth::IsAdminByUserName();
            }
          
            DWORD dwSize = 0;
            PTOKEN_GROUPS Groups;
            ::GetTokenInformation(hToken, TokenGroups, 0, 0, &dwSize);
            Groups = reinterpret_cast<TOKEN_GROUPS *>(::malloc(dwSize));
            ::ZeroMemory(Groups, dwSize);

            if (0 == ::GetTokenInformation(hToken, TokenGroups, Groups, dwSize, &dwSize))   
            {
                ::free(Groups);
                //::CloseHandle(hShell); 
                ::CloseHandle(hToken);
                ::CloseHandle(hProcess); 
                ::FreeLibrary(hInst);
                return CProcessAuth::IsAdminByUserName();
            }
            
            if (NULL != Groups)
            {
                PSID pSid = NULL;
                SID_IDENTIFIER_AUTHORITY ntauth = SECURITY_NT_AUTHORITY;
                ::AllocateAndInitializeSid (&ntauth, 2, SECURITY_BUILTIN_DOMAIN_RID, 
                                            DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &pSid);

                for (DWORD j = 0; j< Groups->GroupCount; j++)
                {
                    if (::EqualSid(pSid, Groups->Groups[j].Sid))
                    {
                        bResult = true;
                        break;
                    }
                }
                ::FreeSid(pSid);
            }
            ::free(Groups);
            ::CloseHandle(hToken);
            //::CloseHandle(hShell);
            //hToken = hShell = NULL;
            hToken = NULL;
            break;
        }
        ::CloseHandle(hProcess);
        hProcess = NULL;
    }

    if (NULL != hInst)
        ::FreeLibrary(hInst);
    hInst = NULL;

    return bResult;
}

/**
 @brief     현재 세션이 시스템인지 확인
 @author    hang ryul lee
 @date      2008.04.10
 @return    true / false
 @note      Systme 세션은 무조건 0.
 @note      현재 세션Id가 0이 아니면 false
 @note      로그온 되어 있지 않을 경우 시스템이라 간주함.
*/
bool CSessionAuth::IsSystem()
{
    CWinOsVersion Os;
    if (!Os.IsWinNT())  return true;

    bool bResult = false;
    CWTSSession session;
    if (0 != GetProcSessionID(::GetCurrentProcessId()))
        return false;

    if (!session.IsLoggedOn())
        bResult = true;

    return bResult;
}

/**
 @brief     현재 세션의 쉘이름 구함
 @author    hang ryul lee
 @date      2008.04.10
 @return    shell name
 @note      현재 쉘 name을 구한다.
*/
CString CSessionAuth::GetShellName()
{
    const CString SHELL_REG_PATH = _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon");
    const CString SHELL_DEFAULT = _T("explorer.exe");

    CString sShellName = _T("");

    CWinOsVersion Os;
    if (Os.IsWinNT())
        sShellName = GetRegStringValue(HKEY_LOCAL_MACHINE, SHELL_REG_PATH, _T("Shells"), _T(""));
    else
    {
        CString sWinDir;
        TCHAR  szBuf[MAX_PATH] = {0,};
        if(0 != ::GetWindowsDirectory(szBuf, MAX_PATH))
            sWinDir = szBuf;
        else 
            sWinDir = _T("c:\\windows");

        CIniFileEx IniFile(sWinDir+_T("\\System.ini"));
        sShellName = IniFile.ReadString(_T("boot"), _T("Shell"), SHELL_DEFAULT);
    }

    //대문자로 바꿔서 첫번째 문제만 같을경우(첫번째 문자비교하서 같으면 1리턴함수사용)
    if (SHELL_DEFAULT != sShellName)
    {
        CString sTemp = sShellName;
        sTemp.MakeUpper();

        if ((_T("") == sShellName) || _T('E') == sTemp[0])
            sShellName = SHELL_DEFAULT;     
    }   

    return sShellName;
}

/**
 @brief     Process가 실행중인 SessionID를 구한다.
 @author    hang ryul lee
 @date      2008.04.10
 @param     [in] _dwProcessId   :session을 구하려는 Process의 id
 @return    SessionId
 @note  ProcessIdToSessionId 함수를 이용하여 Process의 session id를 구한다.\n
*/
DWORD CSessionAuth::GetProcSessionID(const DWORD _dwProcessId)
{
    typedef BOOL (WINAPI* fpProcessIdToSessionId)(DWORD, DWORD*);
    fpProcessIdToSessionId pProcessIdToSessionId;  
    DWORD dwSessionId =0;

    HMODULE hModule = ::GetModuleHandle(_T("kernel32"));
    if (NULL == hModule)
        return 0;

    pProcessIdToSessionId = (fpProcessIdToSessionId)::GetProcAddress(hModule, "ProcessIdToSessionId");
    if (!pProcessIdToSessionId) return 0;
    if (!pProcessIdToSessionId(_dwProcessId, &dwSessionId)) return 0;

    return dwSessionId;
}
