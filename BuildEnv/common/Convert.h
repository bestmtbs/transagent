
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      Convert.h 
 @brief     Convert Class 정의 파일

            자료형 변환 클래스.

 @author    hang ryul lee
 @date      create 2011.05.06
 @note      모든 변환 메서드가 static형이므로 인스턴스를 생성하지 않고 사용하면 된다.
 @note      다음의 자료형들을 CString형으로 변환하는 기능을 제공한다.\n
            INT, UINT, INT64, UINT64, LONG, DWORD, WORD, DOUBLE, FLOAT, COleDateTime, _variant_t
 @note      다음의 자료형들을 INT형으로 변환하는 기능을 제공한다.\n
            CString
 @note      다음의 자료형들을 LONG형으로 변환하는 기능을 제공한다. \n
            CString
 @note      다음의 자료형들을 DOUBLE형으로 변환하는 기능을 제공한다. \n
            CString
 @note      다음의 자료형들을 FLOAT형으로 변환하는 기능을 제공한다. \n
            CString
*/

#pragma once

#include <math.h>
#include "comutil.h"


#define SE_MemoryFree( _X) {		\
			if( _X != NULL)	{		\
				free( _X);			    \
				_X = NULL;			\
			}						        \
}

#define ENCRYPT_FILE_HASH_MOD_VALUE 10000000000

class CConvert
{
public:
    static CString ToString(INT _nValue);    
    static CString ToString(UINT _nValue);
    static CString ToString(INT64 _nValue);
    static CString ToString(UINT64 _nValue);
    static CString ToString(LONG _lValue);
    static CString ToString(DWORD _lValue);
    static CString ToString(WORD _wValue);    
    static CString ToString(DOUBLE _dValue);
    static CString ToString(FLOAT _fValue);
    static CString ToString(COleDateTime _dtTime, const CString &_sFormat = _T("%Y-%m-%d %H:%M:%S"));
    static CString ToString(_variant_t &_vtValue);

	static CHAR* ToChar(const CString& _sValue);


    static INT ToInt(const CString &_sValue, INT _nDefault = 0);
    static UINT ToUInt(const CString &_sValue, UINT _nDefault = 0);
    static INT64 ToInt64(const CString &_sValue, INT64 _nDefault = 0);
    static UINT64 ToUInt64(const CString &_sValue, UINT64 _nDefault = 0);
    static LONG ToLong(const CString &_sValue, LONG _lDefault = 0);
    static DWORD ToDWORD(const CString &_sValue, DWORD _dwDefault = 0);
    static WORD ToWord(const CString &_sValue, WORD _wDefault = 0);    
    static DOUBLE ToDouble(const CString &_sValue, DOUBLE _dDefault = 0.0);
    static COleDateTime ToDateTime(const CString &_sValue, COleDateTime _dtDefault = COleDateTime());

	static LPSTR SE_UniCodeToAnsiCode(LPWSTR pwzStr);
	static LPWSTR SE_AnsiCodeToUniCode(char* _pzText);
	// Kevin Start(2013-4-18)
	static int UnicodeToAnsi2(WCHAR *pszW, CHAR *pszA);
	static int AnsiToUnicode2(char *strAnsi, TCHAR *strUnicode);
// Kevin End(2013-4-18)

	static unsigned int hash2int(CString word);	// 2016-10-17 kh.choi 문자열로 unsigned int 형 해쉬값 만드는 함수. http://www.sysnet.pe.kr/2/0/1223

private:
    CConvert();                                         /**< 기본 생성자 */
    virtual ~CConvert();                                /**< 파괴자 */
    
    CConvert(const CConvert &_rConvert);                /**< 복사 생성자 */
    CConvert& operator = (const CConvert &_rConvert);   /**< 대입 연산자 */

    static bool IsNumber(CString _sValue, bool _bSigned = true);

    static const INT INT_MAX_LEN    = 12;               /**< INT형 최대값의 길이  INT_MAX    = 2147483647               */
    static const INT UINT_MAX_LEN   = 10;               /**< UNIT형 최대값의 길이 UINT_MAX   = 4294967295 (0xffffffff)  */
    static const INT I64_MAX_LEN    = 22;               /**< I64형 최대값의 길이  I64_MAX    = 9223372036854775807      */
    static const INT UI64_MAX_LEN   = 22;               /**< UI64형 최대값의 길이  UI64_MAX  = 18446744073709551615     */
    static const INT LONG_MAX_LEN   = 12;               /**< LONG형 최대값의 길이  LONG_MAX  = 2147483647               */
    static const INT ULONG_MAX_LEN  = 10;               /**< ULONG형 최대값의 길이 ULONG_MAX = 4294967295 (0xffffffff)  */
};