
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      UnInstallInfo.h 
 @brief     CUnInstallInfoList class 정의 파일

            Registry UnInstall 관련 값 얻음

 @author    hang ryul lee 
 @date      create 2007.10.22  
*/
#pragma once
#include "SingleInfoList.h"
/**
 @class     CSoftwareData 
 @brief     CSoftwareData class\n
            소프트웨정보를 담고 있는 data클래스 VMS통신시 Save,Load시 사용된다.

 @author    odkwon
 @date      create 2007.11.07
*/
class CSoftwareData
{
public:
    CSoftwareData();
    virtual ~CSoftwareData();

    CString  m_sKeyName;           /**< UnInstall Registry 키이름 */
    CString  m_sDisplayName;       /**< 제품명 */   
    CString  m_sDisplayVersion;    /**< 버전 */
    CString  m_sPublisher;         /**< 게시자 */
    CString  m_sInstallDate;       /**< 설치 일자*/
    CString  m_sInstallLocation;   /**< 설치 경로 */
    CString  m_sHelpLink;          /**< 지원 정보 */
    CString  m_sURLUpdateInfo;     /**< 제품 업데이트 링크 */
    CString  m_sComments;          /**< 설명 */
    CString  m_sContact;           /**< 연락처 */
    CString  m_sModifyPath;        /**< 수정 경로 */
    CString  m_sReadme;            /**< 도움말 경로 */
    CString  m_sUninstallString;   /**< 삭제 프로그램및 파라미터문자열 */
    CString  m_sDisplayIcon;       /**< 대표 아이콘 */
    bool  m_bIsWow64;              /**< 와우64로 실행되는 program 인지 여부 */

    void CopyData(const CSoftwareData &_rSoftwareData);
    bool LoadFromStream(CMemFileEx *_pStream);
    bool SaveToStream(CMemFileEx *_pStream);
private:
    CSoftwareData(const CSoftwareData &_rSoftwareData);                 /**< 복사 생성자 : 사용금지를 위해 private에 정의 */
    CSoftwareData& operator = (const CSoftwareData &_rSoftwareData);    /**< 대입 연산자 : 사용금지를 위해 private에 정의 */
};

/**
 @class     CSoftwareInfoList 
 @brief     UnInstall Registry 소프트웨어 관련 정보 얻는 class 

 @author    hang ryul lee 
 @date      create 2007.10.22   
 @note      .각각의 UnInstall 레지스트리 상의 값을 구할 수 있다. \n
            .UnInstall을 수행할 수 있다.
*/

class CSoftwareInfoList
{
public:
    CSoftwareInfoList();
    virtual ~CSoftwareInfoList();

    bool GetSwInfos();

    void AddTail(CSoftwareData &_rSoftwareInfo);
    CSoftwareData *GetItem(const INT_PTR _nIndex) const;
    INT_PTR  GetCount() const;
    void RemoveAll();
    POSITION GetHeadPosition() const;
    CSoftwareData *GetNext(POSITION &_rCurrentPostion) const;
    bool SaveToStream(CMemFileEx *_pStream);
    bool LoadFromStream(CMemFileEx *_pStream);


    bool GetUnInstInfoByIndex(const INT _nIndex, CSoftwareData &_SWInfo) const;
    bool IsInstalledProgram(const CString &_sSWName) const;
    bool IsExistUnInstRegKey(const CString &_sSubKey) const;
    bool IsExistDisplayName(const CString &_sDPName) const;
    bool RunUnInstall(const CString &_sSWName, bool _bWait = false) const;
    CString GetUnInstStrValueByKey(const CString &_sKey, const CString &_sValueName) const;
    DWORD GetUnInstDWORDValueByKey(const CString &_sKey, const CString &_sValueName) const;

    void GetDPNameList(CStringList &_sDPList) const;

private:
    CSingleInfoList<CSoftwareData> m_SoftwareList;

    CStringList m_KeyList;
    CStringList m_Wow64KeyList;
    bool m_bIs64bit;

    bool ReadAllUnInstValueByKey(const CString &_sKey, const CStringList &_sKeyList);
    bool ReadUnInstKeyList();
    bool ReadUnInstValue(const CString &_sUnInstKey, const CString &_sSWKey, CSoftwareData &_SWInfo) const;
    CString GetUnInstPathByDPName(const CString &_sDPName) const;

    CSoftwareInfoList(CSoftwareInfoList &_rSoftwareInfoList);   //복사생성자 사용금지.
};

