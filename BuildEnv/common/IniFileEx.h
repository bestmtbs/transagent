
/**
 @file      IniFileEx.h 
 @brief     IniFile Wrapper Class 정의 파일 
 @author    hang ryul lee
 @date      create 2007.07.16 

 @note      64K 이상의 Ini형식의 파일을 관리 할 수 있다.
 @note      한라인에 문자열 길이가 256이상의 값도 Read/Write 할 수 있다.
 @note      UpdateFile()을 호출해야 수정내용이 파일에 적용된다.
*/

#pragma once


class CIniFileEx : public CObject
{
protected:
    CIniFileEx();   //기본생성자 사용금지.
public:	
	CIniFileEx(const CString &_sFileName);
	virtual ~CIniFileEx();

	void Clear();
	void DeleteKey(const CString &_sSection, const CString &_sIdent);
	void EraseSection(const CString &_sSection);
	void GetStrings(CStringList &_rList);
	void ReadSection(const CString &_sSection, CStringList &_rList);
	void ReadSections(CStringList &_rList);
	void ReadSectionValues(const CString &_sSection, CStringList &_rList);
	bool ReadBool(const CString &_sSection, const CString &_sIdent, const bool &_bDefault);
	INT ReadInt(const CString &_sSection, const CString &_sIdent, const INT &_nDefault);
	CString ReadString(const CString &_sSection, const CString &_sIdent, const CString &_sDefault);
	bool SectionExists(const CString &_sSection);
	void SetStrings(CStringList &_rList);
	void UpdateFile();
	bool ValueExists(const CString &_sSection, const CString &_sIdent);
	void WriteBool(const CString &_sSection, const CString &_sIdent, const bool &_bValue);
    void WriteInt(const CString &_sSection, const CString &_sIdent, const INT &_nValue);
    void WriteString(const CString &_sSection, const CString &_sIdent, const CString &_sValue);

    CString GetFileName() const;
private:
	CString m_sFileName;        /**< IniFile Name 값 */ 

    /**
    @brief Section별 정보를 포함하는 구조체

    Section Name과 해당 Section에 포함되는 항목들을 관리하는 구조체
    */
	typedef struct
	{
		CString		sName;      /**< Section Name 값 */ 
		CStringList *pData;     /**< Section에 포함된 항목을 포함하는 리스트 */ 
	} SECTION_INFO;
	CObList *m_pSections;       /**< 각각의 Section을 관리하는 리스트 */ 

	void LoadValues();
	CStringList *AddSection(const CString &_sSection);
	POSITION FindSection(const CString &_sSection);
	POSITION FindByName(CStringList *_pList, const CString &_sIdent);

    bool FileExists(const CString &_sFileName);
};


