
#include "stdafx.h"
#include "DBgLog.h"
#include "UtilsFile.h"
#include <strsafe.h>
#include <atlbase.h>
//#include "UtilsReg.h"
#define DEFAULT_PATH		"c:\\dsntech\\"
#define DEFAULT_FNAME	"Dbg2.log"
#define DEFAULT_FNAMEW	L"Dbg2.log"

#pragma comment (lib, "strsafe.lib")

ULONG g_ulDBGPrevTickCount = 0;

#define DEFAULT_CHECK_INTERVAL	30 // 60초

BOOL CheckTickCount(ULONG ulSec=DEFAULT_CHECK_INTERVAL);
// Kevin(2013-4-15)
CDbgLog g_cDbgLog;

CDbgLog::CDbgLog()
{
	m_ulTickInterval = DEFAULT_CHECK_INTERVAL;

	m_bInitCriticalSection = 0;
	m_strLogFilePath = _T(DEFAULT_PATH);
	m_strLogFileName = _T(DEFAULT_FNAME);
	m_strLogRegName = _T(DEFAULT_FNAME);
	m_strPrefixString = _T("");

	m_strLogFileFullName = m_strLogFilePath + m_strLogFileName;
	m_bWriteFileFlag = FALSE;
	DWORD dwRet = 0, dwDefault = 0;
	CString strFileName;
	strFileName = _T("DEFAULT_FNAME");
	//hhh - 레지스트리를 읽어 파일로 쓸것인지 여부를 판단한다.
	//dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech\\TiorSaver\\LogOption"), strFileName, dwDefault) ;
	HKEY hKey2 = NULL;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, DBG_LOG_DEFAULT_KEY, 0, KEY_READ|KEY_QUERY_VALUE, &hKey2) == ERROR_SUCCESS)
	{
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);
		RegQueryValueEx(hKey2, m_strLogRegName.GetBuffer(), NULL, (LPDWORD)&value_type, (LPBYTE)&dwRet, &value_length);
		RegCloseKey(hKey2);

		switch(dwRet)
		{
		case 1: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 1;
			break;
		case 2: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 2;
			break;
		default:
			m_bWriteFileFlag = FALSE;
			break;
		}
	}
	OutputDebugString(L"pass");
	// Kevin(2013-4-15)
	GetCurProcessName();
	CreateLogDir();
}

// safecom.log
CDbgLog::CDbgLog(CString strFileName)
{
	m_ulTickInterval = DEFAULT_CHECK_INTERVAL;
	m_strLogFilePath = _T(DEFAULT_PATH);
	m_strLogFileName = strFileName;
	m_strLogRegName = strFileName;
	m_strPrefixString = _T("");

	if(m_strLogFilePath.Right(1) != _T("\\"))		
		m_strLogFilePath += _T("\\");
	m_strLogFileFullName = m_strLogFilePath + m_strLogFileName;
	
	m_bWriteFileFlag = FALSE;

	DWORD dwRet = 0, dwDefault = 0;
	//hhh - 레지스트리를 읽어 파일로 쓸것인지 여부를 판단한다.
	//dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech\\TiorSaver\\LogOption"), strFileName, dwDefault) ;
	HKEY hKey2 = NULL;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, DBG_LOG_DEFAULT_KEY, 0, KEY_READ|KEY_QUERY_VALUE, &hKey2)== ERROR_SUCCESS)
	{
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);
		RegQueryValueEx(hKey2, m_strLogRegName, NULL, (LPDWORD)&value_type, (LPBYTE)&dwRet, &value_length);
		RegCloseKey(hKey2);

		switch(dwRet)
		{
		case 1: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 1;
			break;
		case 2: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 2;
			break;
		default:
			m_bWriteFileFlag = FALSE;
			break;
		}
	}

	// Kevin(2013-4-15)
	GetCurProcessName();

	CreateLogDir();
}

CDbgLog::CDbgLog(CString strPath, CString strFileName)
{	
	m_ulTickInterval = DEFAULT_CHECK_INTERVAL;

	m_strLogFilePath = strPath;
	m_strLogFileName = strFileName;
	m_strLogRegName = strFileName;
	m_strPrefixString = _T("");
	DWORD dwRet = 0, dwDefault = 0;
	//hhh - 레지스트리를 읽어 파일로 쓸것인지 여부를 판단한다.
//	dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech\\TiorSaver\\LogOption"), strFileName, dwDefault) ;
	HKEY hKey2 = NULL;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, DBG_LOG_DEFAULT_KEY, 0, KEY_READ|KEY_QUERY_VALUE, &hKey2)== ERROR_SUCCESS)
	{
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);
		RegQueryValueEx(hKey2, m_strLogRegName, NULL, (LPDWORD)&value_type, (LPBYTE)&dwRet, &value_length);
		RegCloseKey(hKey2);

		switch(dwRet)
		{
		case 1: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 1;
			break;
		case 2: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 2;
			break;
		default:
			m_bWriteFileFlag = FALSE;
			break;
		}
	}

	if(m_strLogFilePath.Right(1) != _T("\\"))		
		m_strLogFilePath += _T("\\");

	m_strLogFileFullName = m_strLogFilePath + m_strLogFileName;

	GetCurProcessName();

	CreateLogDir();

}

CDbgLog::CDbgLog(CString strPath, CString strFileName, CString strPrefix)
{
	m_strLogRegName = strFileName;
	m_ulTickInterval = DEFAULT_CHECK_INTERVAL;
	m_strLogFilePath = strPath;
	m_strLogFileName = strFileName;
	m_strPrefixString = strPrefix;

	if(m_strLogFilePath.Right(1) != "\\")		m_strLogFilePath += "\\";
	m_strLogFileFullName = m_strLogFilePath + m_strLogFileName;
	
	m_bWriteFileFlag = FALSE;

	// Kevin(2013-4-15)
	GetCurProcessName();
	
	CreateLogDir();
}

CDbgLog::~CDbgLog()
{
	::DeleteCriticalSection(&ownCriticalSection);
}

void CDbgLog::SetPrefix(CString str)
{
	m_strPrefixString = str;
}
void CDbgLog::SetLogFileName(CString str)
{
	m_strLogRegName = str;
	DWORD dwSid = 0;
	ProcessIdToSessionId(GetCurrentProcessId(), &dwSid);

	WCHAR strSPath[MAX_PATH] = {0};
	StringCchCopy(strSPath, MAX_PATH, str.GetBuffer());
	WCHAR *pExt = wcsstr(strSPath, L".log");
	if(pExt != NULL)
	{
		StringCchPrintf(pExt, MAX_PATH/2, L"-%02d.log", dwSid);
	}
	m_strLogFileName = strSPath;
	m_strLogFileFullName = m_strLogFilePath + m_strLogFileName;
	
}

void CDbgLog::SetLogFilePath(CString str)
{
	m_strLogFilePath = str;

	if(m_strLogFilePath.Right(1) != "\\")		m_strLogFilePath += "\\";
	m_strLogFileFullName = m_strLogFilePath + m_strLogFileName;
}

CString CDbgLog::GetPrefix()
{
	return m_strPrefixString;
}

void CDbgLog::WriteTraceLog(CString strLog)
{
	//UM_WRITE_LOG(L"[Tray] Log : " + strLog);
	//로그파일 쓰기 옵션이 설정되어 있지 않다면 쓰지 않는다.

	// Kevin(2013-6-26)
	// 로그 찍는 기능 설정 체크하는 함수 60초에 한번 수행하도록 수정함.
	if(CheckTickCount(m_ulTickInterval) == TRUE)
	{
		InitLog(0);
	}

	if( FALSE == m_bWriteFileFlag )
	{
		return;
	}

	if(m_bOutType == 2 )
	{
		OutputDebugString(strLog.GetBuffer(0));
		return ;
	}


	//OutputDebugString(L"[Tray] Log : " + strLog);
	CTime cTime = CTime::GetCurrentTime();
	CFile cFile,cFile2;
	if(m_bInitCriticalSection == FALSE)
	{
		::InitializeCriticalSection(&ownCriticalSection);
		m_bInitCriticalSection = TRUE;
	}

	EnterCriticalSection(&ownCriticalSection);
	if (!DirectoryExists(m_strLogFilePath)) {
		ForceCreateDir(m_strLogFilePath);
	}
	if (!cFile.Open(m_strLogFileFullName, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite|CFile::shareExclusive   ))
	{
		OutputDebugString(L"[CDbgLog]  Open Fail");
		OutputDebugString(m_strLogFileFullName);


		::LeaveCriticalSection(&ownCriticalSection);
		return;
	}

	cFile.SeekToEnd();

	USHORT nShort = 0xfeff;  // 유니코드 바이트 오더마크

	DWORD dwTID = GetCurrentThreadId();
	DWORD dwPID = GetCurrentProcessId();

	cFile.Write(&nShort,2);
	cFile.SeekToEnd();

	CString strMessage;
	strMessage.Format(_T("[%s][%s][PID:%04d][TID:%05d]\t%s\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), m_strProcess, dwPID, dwTID, strLog);
	cFile.Write(strMessage, strMessage.GetLength()  *2);
	cFile.Flush();
	cFile.Close();
	::LeaveCriticalSection(&ownCriticalSection);
}

void CDbgLog::WriteTraceLog(TCHAR* fmt, ...)
{
	//OutputDebugString(L"[hh]2 Log : ");
	CString strLog = L"";
	va_list        args; 
	TCHAR		szTrace[8192*5]={0,};
	// Kevin(2013-6-26)
	// 로그 찍는 기능 설정 체크하는 함수 60초에 한번 수행하도록 수정함.
	if(CheckTickCount(m_ulTickInterval) == TRUE)
	{
		InitLog(0);
	}

		//로그파일 쓰기 옵션이 설정되어 있지 않다면 쓰지 않는다.
	if( FALSE == m_bWriteFileFlag )
	{
		return;
	}
		
	va_start(args, fmt);
	/* change by wsjang 20080317 *
	vsprintf(szTrace, fmt, args );
	* change by wsjang 20080317 */
//	_vsnprintf(szTrace, sizeof(szTrace), fmt, args );
	//kw 20090108 _vsnprintf error 로 인한 StringCchVPrintf 적용.
	StringCchVPrintfW(szTrace, 8192*5, fmt, args);
	/* change by wsjang 20080317 */
	va_end(args);
	strLog.Format(_T("%s"), szTrace);


	if(m_bOutType == 1)
		WriteTraceLog(strLog);
	else
		OutputDebugString(strLog.GetBuffer());
}



// Kevin(2013-4-15)
// 
/**
@fn			CDbgLog::WriteLogW(
				LPWSTR ptcpFormat,
				 ...)
@brief		DbgLog.cpp
@param		ptcpFormat
@param		...
@return		void
@sa			
@callgraph	@date			2013년 4월 15일
*/
void CDbgLog::WriteLogW(  LPWSTR ptcpFormat, ... )
{
	WCHAR			tszString[ eMAX_LOG ]={0};
	WCHAR			*ptszString = tszString;
	WCHAR			*ptszStr = NULL;
	va_list			stVa;
	int					iWrittenSize = 0;
	int					nSize = eMAX_LOG;

	// Kevin(2013-6-26)
	// 로그 찍는 기능 설정 체크하는 함수 60초에 한번 수행하도록 수정함.
	if(CheckTickCount(m_ulTickInterval) == TRUE)
	{
		InitLog(0);
	}
	if(m_bWriteFileFlag==FALSE)
	{
		return;
	}
	
	try
	{
		va_start( stVa, ptcpFormat );
		iWrittenSize = swprintf_s(tszString, eMAX_LOG, L"[%s-%s]", m_strProcess, (LPCTSTR)m_strLogFileName);
		StringCchVPrintfW(tszString+iWrittenSize, eMAX_LOG-iWrittenSize, ptcpFormat,stVa);
		//iWrittenSize = _vsnwprintf_s( ptszString + iWrittenSize, nSize - iWrittenSize, eMAX_LOG, ptcpFormat, stVa );
		//iWrittenSize += swprintf_s( tszString + iWrittenSize, eMAX_LOG - iWrittenSize, _T("\n") );
		va_end( stVa );

		if(m_bOutType == 1)
			WriteTraceLog(ptszString);
		else
			OutputDebugStringW( ptszString );
	}
	catch(...)
	{
		OutputDebugStringW( L"Exception of GD_DebugPrintW" );
	}
}

void CDbgLog::WriteLogA(LPSTR ptcpFormat, ... )
{
	CHAR			tszStringA[ eMAX_LOG ]={0};
	WCHAR		tszString[ eMAX_LOG ]={0};
	va_list		stVa;
	int				iWrittenSize = 0;

	// Kevin(2013-6-26)
	// 로그 찍는 기능 설정 체크하는 함수 60초에 한번 수행하도록 수정함.
	if(CheckTickCount(m_ulTickInterval) == TRUE)
	{
		InitLog(0);
	}

	if(m_bWriteFileFlag==FALSE)
	{
		return;
	}

	va_start( stVa, ptcpFormat );
	//iWrittenSize = sprintf_s( tszString, DBGOUT_BUF_SIZE, "[%s-%s-%s-A]", DBG_HEADER_STRINGA, pcModuleName, strAppNameA);
	iWrittenSize = sprintf_s( tszStringA, eMAX_LOG, "[%s]", m_strProcessA);
	StringCchVPrintfA(tszStringA+iWrittenSize, eMAX_LOG-iWrittenSize, ptcpFormat, stVa);
	//iWrittenSize = _vsnprintf_s( tszStringA + iWrittenSize, eMAX_LOG - iWrittenSize, eMAX_LOG - iWrittenSize, ptcpFormat, stVa );
	//iWrittenSize += sprintf_s( tszString + iWrittenSize, DBGOUT_BUF_SIZE - iWrittenSize, "\n" );
	va_end( stVa );

	
	ULONG cbAnsi=0, cCharacters = eMAX_LOG;
	int nRet = eMAX_LOG;

	if(m_bOutType == 1)
	{
	// Convert to ANSI.
		cCharacters = strlen(tszStringA);
		MultiByteToWideChar(CP_ACP, 0, tszStringA, cCharacters, tszString, nRet);
		WriteTraceLog(tszString);
	}
	else
		OutputDebugStringA( tszStringA );
}


void CDbgLog::WriteLogToDVW(  LPWSTR ptcpFormat, ... )
{
	WCHAR			tszString[ eMAX_LOG ] ={0};
	va_list			stVa;
	int					iWrittenSize = 0;

	// Kevin(2013-6-26)
	// 로그 찍는 기능 설정 체크하는 함수 60초에 한번 수행하도록 수정함.
	if(CheckTickCount(m_ulTickInterval) == TRUE)
	{
		InitLog(0);
	}

	if(m_bWriteFileFlag==FALSE)
	{
		return;
	}

	try
	{
		va_start( stVa, ptcpFormat );
		iWrittenSize = swprintf_s( tszString, eMAX_LOG, L"[%s-%s]", m_strProcess, (LPCTSTR)m_strLogFileName);
		StringCchVPrintfW(tszString+iWrittenSize, eMAX_LOG-iWrittenSize, ptcpFormat,stVa);
//		iWrittenSize = _vsnwprintf_s( tszString + iWrittenSize, eMAX_LOG - iWrittenSize, eMAX_LOG, ptcpFormat, stVa );
		//iWrittenSize += swprintf_s( tszString + iWrittenSize, eMAX_LOG - iWrittenSize, _T("\n") );
		va_end( stVa );

	if(m_bOutType == 1)
		WriteTraceLog(tszString);
	else
		OutputDebugStringW( tszString );
	}
	catch(...)
	{
		OutputDebugStringW( L"Exception of GD_DebugPrintW" );
	}
}

void CDbgLog::WriteLogToDVA(LPSTR ptcpFormat, ... )
{
	CHAR			tszStringA[ eMAX_LOG ]={0};
	WCHAR		tszString[ eMAX_LOG ]={0};
	va_list		stVa;
	int				iWrittenSize = 0;
	// Kevin(2013-6-26)
	// 로그 찍는 기능 설정 체크하는 함수 60초에 한번 수행하도록 수정함.
	if(CheckTickCount(m_ulTickInterval) == TRUE)
	{
		InitLog(0);
	}
	if(m_bWriteFileFlag==FALSE)
	{
		return;
	}

	va_start( stVa, ptcpFormat );
	iWrittenSize = sprintf_s( tszStringA, eMAX_LOG, "[%s]", m_strProcessA);
	StringCchVPrintfA(tszStringA+iWrittenSize, eMAX_LOG-iWrittenSize, ptcpFormat, stVa);
	//iWrittenSize = sprintf_s( tszString, DBGOUT_BUF_SIZE, "[%s-%s-%s-A]", DBG_HEADER_STRINGA, pcModuleName, strAppNameA);
	//iWrittenSize = _vsnprintf_s( tszStringA + iWrittenSize, eMAX_LOG - iWrittenSize, eMAX_LOG - iWrittenSize, ptcpFormat, stVa );
	//iWrittenSize += sprintf_s( tszString + iWrittenSize, DBGOUT_BUF_SIZE - iWrittenSize, "\n" );
	va_end( stVa );

	
	//ULONG cbAnsi=0, cCharacters = eMAX_LOG;
	//int nRet = eMAX_LOG;
	//// Convert to ANSI.
	//cCharacters = eMAX_LOG;
	//WideCharToMultiByte(CP_ACP, 0, tszString, cCharacters, tszStringA, nRet, NULL, NULL);
	if(m_bOutType == 1)
	{
	// Convert to ANSI.
		ULONG nRet = 0;
		ULONG cCharacters = strlen(tszStringA);
		MultiByteToWideChar(CP_ACP, 0, tszStringA, cCharacters, tszString, nRet);
		WriteTraceLog(tszString);
	}
	else
		OutputDebugStringA( tszStringA );
}

// Kevin(2013-4-15)
// 현재 프로세스 정보 얻기
/**DbgLog.cpp
@fn			CDbgLog::GetCurProcessName()
@brief		
@return		BOOL
@sa			
@callgraph	@date			2013년 4월 15일
*/
BOOL CDbgLog::GetCurProcessName()
{
	WCHAR			wcModuleName[MAX_PATH] = {0};
	WCHAR			*pwcModuleName=NULL;

	memset( wcModuleName, 0x00, sizeof( wcModuleName ) );
	::GetModuleFileNameW(NULL, wcModuleName, sizeof(wcModuleName));
	if(lstrlen(wcModuleName) > 0)
	{
		pwcModuleName = (WCHAR*)wcsrchr( wcModuleName, L'\\' );
		pwcModuleName++;
		WCHAR *pEnd = wcsstr(pwcModuleName, L".exe");
		*pEnd = L'\0';
		StringCchCopy(m_strProcess, MAX_PATH, pwcModuleName);
		
		ULONG cbAnsi=0, cCharacters = MAX_PATH;
		int nRet = MAX_PATH;
		// Convert to ANSI.
		cCharacters = MAX_PATH;
		WideCharToMultiByte(CP_ACP, 0, m_strProcess, cCharacters, m_strProcessA, nRet, NULL, NULL);
		return TRUE;
	}
	return FALSE;
}


/**DbgLog.cpp
@fn			CDbgLog::CreateLogDir()
@brief		
@return		BOOL
@sa			
@callgraph	@date			2013년 4월 15일
*/
BOOL CDbgLog::CreateLogDir()
{
	//CString strPath;
	WCHAR strPath[MAX_PATH] = {0};
	GetWindowsDirectory(strPath, MAX_PATH);
	if(lstrlen(strPath) > 0)
	{
		m_strLogFilePath.Format(L"%c:\\dsntech\\TiorSaver\\Log", strPath[0]);
	}

	CreateDirectory(m_strLogFilePath, NULL);
	m_strLogFilePath += L"\\";

	OutputDebugString(m_strLogFilePath);

	return TRUE;
}

/**
@fn			CDbgLog::InitLog(
				BOOL bDelPrevLog)DbgLog.cpp
@brief		
@param		bDelPrevLog
@return		void
@sa			
@callgraph	@date			2013년 4월 15일
*/
void CDbgLog::InitLog(BOOL bDelPrevLog)
{
	OutputDebugString(L"InitLog()");
	OutputDebugString(L"RegValue = ");
	OutputDebugString(m_strLogFileName.GetBuffer());
	OutputDebugString(m_strLogFilePath.GetBuffer());

	DWORD dwRet = 0, dwDefault = 0;
	SetRegPath();
	//dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech\\TiorSaver\\LogOption"), m_strLogFileName, dwDefault) ;
	HKEY hKey2 = NULL;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\dsntech\\WTiorTest", 0, KEY_READ|KEY_QUERY_VALUE, &hKey2)== ERROR_SUCCESS)
	{
		DWORD value_type = REG_DWORD;
		DWORD value_length = sizeof(DWORD);
		RegQueryValueEx(hKey2, m_strLogRegName.GetBuffer(), 0, (LPDWORD)&value_type, (LPBYTE)&dwRet, &value_length);
		RegCloseKey(hKey2);
		
		switch(dwRet)
		{
		case 1: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 1;
			break;
		case 2: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 2;
			break;
		default:
			m_bWriteFileFlag = FALSE;
			m_bOutType = 2;
			break;
		}

		if(bDelPrevLog == TRUE)
		{
			DeleteFile(m_strLogFileFullName.GetBuffer());
		}

		OutputDebugStringW(L"========================[Start Log]===================\n");
		DBGOUT(L"%s=%d, regval=%d", m_strLogFileName.GetBuffer(), m_bWriteFileFlag, m_bOutType);
	}
}

void CDbgLog::SetOutType(int nOutType)
{
	switch(nOutType)
	{
		case 1: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 1;
			
			break;
		case 2: 		//1이면 파일 기록을 진행한다.
			m_bWriteFileFlag = TRUE;
			m_bOutType = 2;
			
			break;
		default:
			m_bWriteFileFlag = FALSE;
			m_bOutType = 2;
			

			break;
	}

}


void CDbgLog::SetRegPath()
//static void GETDBGAPP(LPCWSTR pAppName)
{
	HKEY hKey=NULL;
	HKEY hKey2=NULL;
	DWORD value = FALSE;
	DWORD value_type;
	DWORD value_length;
	DWORD dwDisp;
	
	OutputDebugStringW(L"SetRegPath() start");

	value_type = REG_DWORD;
	value_length = sizeof(DWORD);
	
	try
	{
		if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\dsntech\\WTiorTest", 0, KEY_READ|KEY_QUERY_VALUE, &hKey2) != ERROR_SUCCESS)
		{
			RegCreateKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisp);
			if(hKey != NULL)
			{
				RegCloseKey(hKey);
				hKey = NULL;
			}
			RegCreateKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech\\"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisp);
			if(hKey != NULL)
			{
				RegCloseKey(hKey);
				hKey = NULL;
			}
		}
		else
		{
			if(hKey2 != NULL)
			{
				RegCloseKey(hKey2);
				hKey2 = NULL;
			}
		}
		
		RegCreateKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\dsntech\\WTiorTest"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisp);
		if(hKey != NULL)
		{
			DWORD dwData = 0;
			if(RegQueryValueEx(hKey, m_strLogRegName.GetBuffer(), 0, (LPDWORD)&value_type, (LPBYTE)&dwData, &value_length) == ERROR_FILE_NOT_FOUND)
			{
				value_type = REG_DWORD;
				value_length = sizeof(DWORD);

				RegSetValueEx(hKey, m_strLogRegName.GetBuffer(), 0, value_type, (const BYTE *)&dwData, value_length);
			}
			RegCloseKey(hKey);
			hKey = NULL;
		}
	}
	catch(...)
	{
		OutputDebugStringW(L"SetRegPath() exception");
	}
	OutputDebugStringW(L"SetRegPath() End");
}

BOOL CheckTickCount(ULONG ulSec)
{
	ULONG ulCurTick = GetTickCount();
	if(g_ulDBGPrevTickCount == 0)
		g_ulDBGPrevTickCount = ulCurTick;

	ULONG ulTick = (ulCurTick - g_ulDBGPrevTickCount) / 1000;
	if(ulTick >= ulSec)
	{
		g_ulDBGPrevTickCount = ulCurTick;
		return TRUE;
	}
	return FALSE;
}