#include "stdafx.h"
#include "CmsDBManager.h"
//#include "..\..\05_Agent\CmsAgent\CmsDefine.h"
#include "yvals.h"

// Kevin(2013-5-7)
#ifndef SE_MemoryFree
#define SE_MemoryFree( _X) {		\
			if( _X != NULL)	{		\
				free( _X);			    \
				_X = NULL;			\
			}						        \
}
#endif
#ifndef SE_MemoryDelete
#define SE_MemoryDelete( _X) {		\
	if( _X != NULL)	{		\
	delete  _X;			    \
	_X = NULL;			\
	}						        \
}
#endif

CCmsDBManager::CCmsDBManager(void)
{
}

CCmsDBManager::CCmsDBManager(CString _strType)
{
	CString strDBName = _T("");
	if (0 == _strType.CompareNoCase(DB_SEND_NCVI_LIST)) {
		strDBName = WTIOR_NCVI_DATABASE;
	} else if (0 == _strType.CompareNoCase(DB_SEND_NKFI_LIST)) {
		strDBName = WTIOR_NKFI_DATABASE;
	} else if (0 == _strType.CompareNoCase(DB_SEND_LOG_LIST)) {
		strDBName = WTIOR_LOG_DATABASE;
	} else {
		strDBName = WTIOR_SETTING_DATABASE;
	}
	m_pDBManage = NULL;
	if( _strType == DB_AGENT_INFO )
		m_pDBManage = new  CTBAgentInfo;
	if( _strType == DB_PC_INFO )
		m_pDBManage = new  CTBUserInfo;
	if (_strType == DB_SEND_LOG_LIST) {
		m_pDBManage = new CTBSendLogList;
	}
	if (_strType == DB_SEND_NCVI_LIST) {
		m_pDBManage = new CTBSendNcviList;
	}
	if (_strType == DB_SEND_NKFI_LIST) {
		m_pDBManage = new CTBSendNkfiList;
	}
	if( _strType == DB_COLLECT_POLICY )
		m_pDBManage = new  CTBCollectPolicy;
	if( _strType == DB_NOTICE )
		m_pDBManage = new  CTBNotice;
	if( _strType == DB_STATE_POLICY )
		m_pDBManage = new  CTBStatePolicy;
	if( _strType == DB_URL_POLICY )
		m_pDBManage = new  CTBUrlPolicy;
	if( _strType == DB_SERVER_INFO )
		m_pDBManage = new  CTBServerInfo;
	if (m_pDBManage) {
		m_pDBManage->m_strFilePath = strDBName;
		m_pDBManage->Open();
	}
}

CCmsDBManager::~CCmsDBManager(void)
{
}

BOOL CCmsDBManager::CreateQuery()
{

	if( m_pDBManage->QueryCreate() == TRUE )
		return TRUE;

	return FALSE;
}

/**
@brief     ExecQuery
@author   hrlee
@date      2012.04.05
@param	[in] _strQuery : 실행할 쿼리문
@return		BOOL(FALSE/TRUE)
@NOTE		Exec문을 실행하기 위한 함수
*/
BOOL CCmsDBManager::ExecQuery(CString _strQuery)
{
	BOOL bResult = FALSE;

//	TCHAR *pszBuffer = new TCHAR[4096];
//	ZeroMemory(pszBuffer,  4096);

//	lstrcpy(pszBuffer, _strQuery);

	if( m_pDBManage )
	{
		if( m_pDBManage->ExecQuery(_strQuery) == true )
			bResult  = TRUE;
	}
	
//	SE_MemoryDelete(pszBuffer);

	return bResult;

}

BOOL CCmsDBManager::InsertQuery(CString _strQuery, int nTryCnt)
{

	BOOL bResult = FALSE;


//	TCHAR *pszBuffer = new TCHAR[1024];

//	lstrcpy(pszBuffer, _strQuery);

	if (!m_pDBManage) {		// 2017-05-23 kh.choi m_pDBManage 가 널포인터인 경우 에러 출력하고 FALSE 리턴
		//////ezLog(_T("[OfsDB] [ERROR] m_pDBManage is NULL. will return FALSE. [line: %d, function: %s, file: %s]\r\n"), __LINE__, __FUNCTIONW__, __FILEW__);	// 2017-05-23 kh.choi 파일로그
		return FALSE;
	}

	if( m_pDBManage )
	{
		if( m_pDBManage->QueryInsert( (TCHAR*)(LPCTSTR)_strQuery ) == true )
			bResult  = TRUE;
	}

//	if( pszBuffer)
//		delete [] pszBuffer;
	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( false == bResult)
		{
			Sleep(500);
			//bResult = m_pDBManage->QueryInsert( (TCHAR*)(LPCTSTR)_strQuery );
			if( m_pDBManage->QueryInsert( (TCHAR*)(LPCTSTR)_strQuery ) == true )
				bResult  = TRUE;

			nCrrentTryCnt++;
			if(bResult==bResult) {
				CString strLog= _T("");
				strLog.Format(_T("[Test] QueryInsert Fail.. %s %d"), _strQuery, nCrrentTryCnt);
				UM_WRITE_LOG(strLog);
			}
			else
				UM_WRITE_LOG(_T("[Test] QueryInsert"));
		}
		else
			break;
	}



	return bResult;
}

BOOL CCmsDBManager::UpdateQuery(CString _strQuery, int nTryCnt)
{

	BOOL bResult = FALSE;
//	TCHAR *pszBuffer = new TCHAR[1024];
//	lstrcpy(pszBuffer, _strQuery);
   //UM_WRITE_LOG(_T("[CmsTray] UpdateQuery STart"));
	if( m_pDBManage )
	{
		if( m_pDBManage->QueryUpdate( (TCHAR*)(LPCTSTR)_strQuery) == true )
		{
			bResult  = TRUE;
		}
	}
	//UM_WRITE_LOG(_T("[CmsTray] UpdateQuery End"));

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( false == bResult)
		{
			Sleep(500);
			//bResult = m_pDBManage->QueryUpdate( (TCHAR*)(LPCTSTR)_strQuery );
			if( m_pDBManage->QueryUpdate( (TCHAR*)(LPCTSTR)_strQuery) == true )
			{
				bResult  = TRUE;
			}

			nCrrentTryCnt++;
			{
				CString strLog= _T("");
				strLog.Format(_T("[Test] QueryUpdate Fail.. nCurrentTryCnt: %d"), nCrrentTryCnt);
				UM_WRITE_LOG(strLog);
			}
		}
		else
			break;
	}

//	if( pszBuffer)
//	{
//		delete [] pszBuffer;
//	}
		
	return bResult;
}

BOOL CCmsDBManager::SelectAllQuery(CString _strQuery, CStringArray* _arrList, int nTryCnt)
{

	BOOL bResult = FALSE;

	//TCHAR *pszBuffer = new TCHAR[1024];

	//lstrcpy(pszBuffer, _strQuery);

	if( m_pDBManage )
	{
		if( m_pDBManage->QueryAllSelect( (TCHAR*)(LPCTSTR)_strQuery, _arrList) == true )
				bResult  = TRUE;
	}

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( FALSE == bResult)
		{
			Sleep(500);
			//bResult =m_pDBManage->QueryAllSelect( (TCHAR*)(LPCTSTR)_strQuery, _arrList);
			if( m_pDBManage->QueryAllSelect( (TCHAR*)(LPCTSTR)_strQuery, _arrList) == true )
				bResult  = TRUE;

			nCrrentTryCnt++;
			{
				CString strLog= _T("");
				strLog.Format(_T("[Test] QueryAllSelect Fail..nCrrentTryCnt : %d"), nCrrentTryCnt);
				UM_WRITE_LOG(strLog);
			}
		}
		else
			break;
	}

	//if( pszBuffer)
	//	delete [] pszBuffer;

	return bResult;
}

BOOL CCmsDBManager::SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField, int nTryCnt)
{

	BOOL bResult = FALSE;

//	TCHAR *pszBuffer = new TCHAR[1024];

//	lstrcpy(pszBuffer, _strQuery);

	UM_WRITE_LOG(_T("[DBConnect] 4"));

	if( m_pDBManage )
	{
		if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField) == true )
				bResult  = TRUE;
	}

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( FALSE == bResult)
		{
			Sleep(500);
			//bResult =m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField) ;
			if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField) == true )
				bResult  = TRUE;

			nCrrentTryCnt++;
			//UM_WRITE_LOG(_T("[Test] QuerySelect Fail.. ")+ _strQuery);
			{
				CString strLog = _T("");
				strLog.Format(_T("[Test] QuerySelect Fail.. _strQuery: %s [line: %d, function: %s, file: %s]\n"), _strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
				UM_WRITE_LOG(strLog);
			}
		}
		else
			break;
	}
	

//	if( pszBuffer)
//		delete [] pszBuffer;

	return bResult;
}


BOOL CCmsDBManager::SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField, CString _strField2, int nTryCnt)
{

	BOOL bResult = FALSE;

//	TCHAR *pszBuffer = new TCHAR[1024];

//	lstrcpy(pszBuffer, _strQuery);

	if( m_pDBManage )
	{
		if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2) == true )
				bResult  = TRUE;
	}

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( FALSE == bResult)
		{
			Sleep(500);
			//bResult = m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2) ;
			if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2) == true )
				bResult  = TRUE;

			nCrrentTryCnt++;
			//UM_WRITE_LOG(_T("[Test] QuerySelect Fail.. ")+ _strQuery);
			{
				CString strLog = _T("");
				strLog.Format(_T("[Test] QuerySelect Fail.. _strQuery: %s [line: %d, function: %s, file: %s]\n"), _strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
				UM_WRITE_LOG(strLog);
			}
		}
		else
			break;
	}

//	if( pszBuffer)
//		delete [] pszBuffer;

	return bResult;
}



BOOL CCmsDBManager::SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField, CString _strField2, CString _strField3, int nTryCnt)
{

	BOOL bResult = FALSE;

//	TCHAR *pszBuffer = new TCHAR[1024];

//	lstrcpy(pszBuffer, _strQuery);

	if( m_pDBManage )
	{
		if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2, _strField3) == true )
				bResult  = TRUE;
	}

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( FALSE == bResult)
		{
			Sleep(500);
			//bResult = m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2, _strField3);
			if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2, _strField3) == true )
				bResult  = TRUE;

			nCrrentTryCnt++;
			//UM_WRITE_LOG(_T("[Test] QuerySelect Fail.. ")+ _strQuery);
			{
				CString strLog = _T("");
				strLog.Format(_T("[Test] QuerySelect Fail.. _strQuery: %s [line: %d, function: %s, file: %s]\n"), _strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
				UM_WRITE_LOG(strLog);
			}
		}
		else
			break;
	}

//	if( pszBuffer)
//		delete [] pszBuffer;

	return bResult;
}



BOOL CCmsDBManager::SelectQuery(CString _strQuery, CStringArray* _arrList, CString _strField, CString _strField2, CString _strField3, CString _strField4, int nTryCnt)
{

	BOOL bResult = FALSE;

//	TCHAR *pszBuffer = new TCHAR[1024];

//	lstrcpy(pszBuffer, _strQuery);

	if( m_pDBManage )
	{
		if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2, _strField3, _strField4) == true )
				bResult  = TRUE;
	}

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( FALSE == bResult)
		{
			Sleep(500);
//			bResult =m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2, _strField3, _strField4);
			if( m_pDBManage->QuerySelect((TCHAR*)(LPCTSTR)_strQuery, _arrList, _strField, _strField2, _strField3, _strField4) == true )
				bResult  = TRUE;

			nCrrentTryCnt++;
			//UM_WRITE_LOG(_T("[Test] QuerySelect Fail.. ")+ _strQuery);
			{
				CString strLog = _T("");
				strLog.Format(_T("[Test] QuerySelect Fail.. _strQuery: %s [line: %d, function: %s, file: %s]\n"), _strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
				UM_WRITE_LOG(strLog);
			}
		}
		else
			break;
	}

//	if( pszBuffer)
//		delete [] pszBuffer;

	return bResult;
}

BOOL CCmsDBManager::DeleteQuery(CString _strQuery, int nTryCnt)
{
	BOOL bResult = FALSE;

	//TCHAR *pszBuffer = new TCHAR[1024];

	//lstrcpy(pszBuffer, _strQuery);

	if( m_pDBManage )
	{
		if( m_pDBManage->QueryDelete((TCHAR*)(LPCTSTR)_strQuery) == true )
		{
				bResult  = TRUE;
		}
	}

	int nCrrentTryCnt = 0;
	while(TRUE)
	{

		if(nTryCnt != -1)		//-1인 경우 무한 재시도를 할때 이다.
		{
			if(nCrrentTryCnt > nTryCnt)
				return bResult;
		}

		if( FALSE == bResult)
		{
			Sleep(500);
			if( m_pDBManage->QueryDelete((TCHAR*)(LPCTSTR)_strQuery) == true )
			{
					bResult  = TRUE;
			}
			nCrrentTryCnt++;
			UM_WRITE_LOG(_T("[Test] DeleteQuery Fail.."));
		}
		else
			break;
	}
//	if( pszBuffer)
//		delete [] pszBuffer;

	return bResult;
}

void CCmsDBManager::Free()
{
	SE_MemoryDelete(m_pDBManage);
}