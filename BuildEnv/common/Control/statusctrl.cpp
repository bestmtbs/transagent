#include "stdafx.h"
#include "statusctrl.h"


//////////////////////////////////////
// CStatusProgressControl

BEGIN_MESSAGE_MAP(CStatusProgressControl, CProgressCtrl)
	//{{AFX_MSG_MAP(CStatusProgressControl)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CStatusProgressControl::CStatusProgressControl() : m_nNumBlocks(0), m_dwStyle(SP_USE_PBS_STYLE), m_strMsg("")
{
}

void CStatusProgressControl::SetNumBlocks(UINT nNumBlocks)
{
    m_nNumBlocks = nNumBlocks;
}

void CStatusProgressControl::SetStyle(DWORD dwStyle)
{
    m_dwStyle = dwStyle;
}

void CStatusProgressControl::SetMessage(LPCSTR lpszMsg)
{
    m_strMsg = lpszMsg;
}

void CStatusProgressControl::OnPaint()
{
    // If we're using the default progressbar style...
    if ((m_dwStyle & SP_USE_PBS_STYLE) == SP_USE_PBS_STYLE)
    {
        CProgressCtrl::OnPaint();
    }
    // If we're using a custom style...
    else
    {
        // Get the client area...
        RECT rect;
        GetClientRect(&rect);

        // Get the progress status...
        int nMin, nMax, nPos;
        int nCutOff;
        double dPercent;
        GetRange(nMin, nMax);
        nPos = GetPos();
        dPercent = (double)nPos / (double)(nMax - nMin);
        nCutOff = rect.right * dPercent;

        // Setup an in-memory device context...
        CPaintDC dcPaint(this);
        CDC dc;
        dc.CreateCompatibleDC(&dcPaint);
        CBitmap bmpTemp;
        bmpTemp.CreateCompatibleBitmap(&dcPaint, rect.right, rect.bottom);
        CBitmap* pOldBitmap = dc.SelectObject(&bmpTemp);

        // Paint the control to our specs...
        CBrush brushBlock(GetSysColor(COLOR_HIGHLIGHT));
        CBrush brushBack(GetSysColor(COLOR_SCROLLBAR));
        dc.FillRect(&rect, &brushBack);

        CString strText = _T("");
        bool bPrintText = false;

        // If we're printing the percentage...
        if ((m_dwStyle & SP_SHOWPERCENT) == SP_SHOWPERCENT)
        {
            // Create the percentage text...
            int nPercent = (100 * dPercent);
            strText.Format(_T("%d%%"), nPercent);
            bPrintText = true;
        }
        // If we're printing the message...
        if ((m_dwStyle & SP_SHOWMSG) == SP_SHOWMSG)
        {
            // Copy the message...
            strText = m_strMsg;
            bPrintText = true;
        }

        // If we're printing blocks...
        if ((m_dwStyle & SP_BLOCK_STYLE) == SP_BLOCK_STYLE)
        {
            int nNumBlocks = (m_nNumBlocks ? m_nNumBlocks : (rect.bottom / 2));
            int nBlockSize = (rect.right / nNumBlocks);
            RECT rectBlock;
            rectBlock.top = 1;
            rectBlock.bottom = rect.bottom - 1;
            for (int nBlock = 0; nBlock < nNumBlocks; nBlock++)
            {
                rectBlock.left = nBlock * nBlockSize;
                rectBlock.right = rectBlock.left + nBlockSize - 1;
                if (rectBlock.right >= nCutOff)
                    break;
                dc.FillRect(&rectBlock, &brushBlock);
            }
        }
        // If we're printing smooth...
        else if ((m_dwStyle & SP_SMOOTH_STYLE) == SP_SMOOTH_STYLE)
        {
            // Draw the block...
            RECT rectBlock;
            rectBlock.top = 0;
            rectBlock.left = 0;
            rectBlock.bottom = rect.bottom;
            rectBlock.right = nCutOff;
            dc.FillRect(&rectBlock, &brushBlock);
        }

        // If we're printing text...
        if (bPrintText)
        {
            // Add the text...
            CSize size;
            int nLength;
            CDC dcText;
            dcText.CreateCompatibleDC(&dcPaint);
            CBitmap bmpText;
            bmpText.CreateCompatibleBitmap(&dcPaint, rect.right, rect.bottom);
            CBitmap* pOldBitmap = dcText.SelectObject(&bmpText);
            int nOldBkMode = dcText.SetBkMode(TRANSPARENT);
            CFont* pFont = GetFont();
            dcText.SelectObject(pFont);
            nLength = strText.GetLength();
            for (size = dcText.GetTextExtent(strText); (size.cx > rect.right) && (nLength > 4); size = dcText.GetTextExtent(strText))
            {
                nLength = strText.GetLength();
                strText.Delete(nLength - 1);
                strText.SetAt(nLength - 2, '.');
                strText.SetAt(nLength - 3, '.');
                strText.SetAt(nLength - 4, '.');
            }
            dcText.SetTextColor(((~GetSysColor(COLOR_HIGHLIGHT)) & 0x00ffffff));
            dcText.TextOut((rect.right >> 1) - (size.cx >> 1), (rect.bottom >> 1) - (size.cy >> 1), strText);
            dcText.SetBkMode(nOldBkMode);
            dc.BitBlt(0, 0, rect.right, rect.bottom, &dcText, 0, 0, SRCINVERT);
        }

        // Dump the paint...
        dcPaint.BitBlt(0, 0, rect.right, rect.bottom, &dc, 0, 0, SRCCOPY);
        dc.SelectObject(pOldBitmap);
    }
}

//////////////////////////////////////
// CStatusControl

IMPLEMENT_DYNAMIC(CStatusControl, CWnd)

CString CStatusControl::m_strClassName;

// Construction/destruction...
CStatusControl::CStatusControl()
{
    m_bCancelled = FALSE;
    if (m_strClassName.IsEmpty())
        m_strClassName = AfxRegisterWndClass(NULL, NULL, NULL, NULL);
}

CStatusControl::~CStatusControl()
{
}

// Creation...
BOOL CStatusControl::CreateInPlaceOf(CWnd* pWnd, DWORD dwStyle)
{
    // Check args...
    if (pWnd == NULL)
        return FALSE;

    // Create the status control window...
    CWnd* pParentWnd = pWnd->GetParent();
    RECT rect;
    pWnd->GetClientRect(&rect);
    pWnd->MapWindowPoints(pParentWnd, &rect);
    UINT nID = pWnd->GetDlgCtrlID();
    CString strWindowName;
    pWnd->GetWindowText(strWindowName);
    pWnd->DestroyWindow();
    if (!CWnd::Create(m_strClassName, strWindowName, dwStyle, rect, pParentWnd, nID, NULL))
        return FALSE;
    // Add the control's children...
    return CreateChildWindows();
}

BOOL CStatusControl::Create(LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
    // Create the status control window...
    if (!CWnd::Create(m_strClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext))
        return FALSE;
    // Add the control's children...
    return CreateChildWindows();
}

BOOL CStatusControl::CreateEx(DWORD dwExStyle, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, LPVOID lpParam)
{
    // Create the status control window...
    if (!CWnd::CreateEx(dwExStyle, m_strClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, lpParam))
        return FALSE;
    // Add the control's children...
    return CreateChildWindows();
}

BOOL CStatusControl::CreateChildWindows()
{
    RECT rect;
    GetClientRect(&rect);

    // Get the parent...
    CWnd* pWndParent = GetParent();

    // Create the cancel button...
	/*
    RECT rectCancel;
    rectCancel.left = 0;
    rectCancel.top = 0;
    rectCancel.right = 50;
    rectCancel.bottom = rect.bottom;
    if (!m_CancelButton.Create(_T("Cancel"), WS_CHILD, rectCancel, this, _SC_ID_CANCEL_))
        return FALSE;
    if (pWndParent)
        m_CancelButton.SetFont(pWndParent->GetFont());
    m_CancelButton.ShowWindow(SW_SHOW);

	*/
    // Create the progress control...
    RECT rectProgress;
    rectProgress.left = 0;
    rectProgress.top = 0;
    rectProgress.right = rect.right;
    rectProgress.bottom = rect.bottom;
    if (!m_ProgressCtrl.Create(GetStyle(), rectProgress, this, _SC_ID_PROGRESS_))
        return FALSE;
    if (pWndParent)
        m_ProgressCtrl.SetFont(pWndParent->GetFont());
    m_ProgressCtrl.ShowWindow(SW_SHOW);

    // Initial reset...
    Reset();

    // Display the status control...
    ShowWindow(SW_SHOW);

    return TRUE;
}

// Configuration...
void CStatusControl::SetProgressCtrlText(LPCSTR lpszText)
{
    m_ProgressCtrl.SetMessage(lpszText);
}

void CStatusControl::SetProgressCtrlStyle(DWORD dwStyle)
{
    m_ProgressCtrl.SetStyle(dwStyle);
}

void CStatusControl::SetProgressCtrlNumBlocks(UINT nNumBlocks)
{
    m_ProgressCtrl.SetNumBlocks(nNumBlocks);
}

BOOL CStatusControl::MoveProgressCtrl(RECT& rect)
{
    // Move the progress control...
    m_ProgressCtrl.MoveWindow(&rect);
    // Ensure the window spans its controls...
    RECT rectWnd;
    GetClientRect(&rectWnd);
    if (rect.bottom > rectWnd.bottom)
        rectWnd.bottom = rect.bottom;
    if (rect.right > rectWnd.right)
        rectWnd.right = rect.right;
    MapWindowPoints(GetParent(), &rectWnd);
    MoveWindow(rectWnd.left, rectWnd.top, rectWnd.right - rectWnd.left, rectWnd.bottom - rectWnd.top);

    return TRUE;
}

BOOL CStatusControl::MoveCancelButton(RECT& rect)
{
    // Move the cancel button...
    m_CancelButton.MoveWindow(&rect);
    // Ensure the window spans its controls...
    RECT rectWnd;
    GetClientRect(&rectWnd);
    if (rect.bottom > rectWnd.bottom)
        rectWnd.bottom = rect.bottom;
    if (rect.right > rectWnd.right)
        rectWnd.right = rect.right;
    MapWindowPoints(GetParent(), &rectWnd);
    MoveWindow(rectWnd.left, rectWnd.top, rectWnd.right - rectWnd.left, rectWnd.bottom - rectWnd.top);

    return TRUE;
}

// Progress...
void CStatusControl::SetSteps(int nNumSteps, CStatusProgress& retSubProgress)
{
    // Divide progress bar into number of requested steps (subbars).
    // Each step can, in turn, be recursively divided to more steps.
    // (See CStatusProgress)
    retSubProgress.m_dAbsoluteCurPos = 0;
    retSubProgress.m_dCurPos = 0;
    retSubProgress.m_dEndPos = INT_MAX;
    retSubProgress.m_nNumSteps = nNumSteps;
    retSubProgress.m_dStepInc = retSubProgress.m_dEndPos / nNumSteps;
    retSubProgress.Attach(this);
}

void CStatusControl::SetProgressPos(int nPos, BOOL bDispatchAllPendingMessages)
{
    // Update the progress control's position...
    m_ProgressCtrl.SetPos(nPos);
	DWORD dwThreadID = GetCurrentThreadId();
	DWORD dwMainThreadID = AfxGetApp()->m_nThreadID;
    // If we want to dispatch messages for all windows...
    if (bDispatchAllPendingMessages)
    {
        // Dispatch any pending messages...
        MSG msg;
	    while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	    {
            if ((dwThreadID != dwMainThreadID) ||
		        (!AfxGetApp() -> PreTranslateMessage(&msg)))
		    {
			    ::TranslateMessage(&msg);
			    ::DispatchMessage(&msg);
		    }
	    }
    }
    // If we do not want to dispatch messages for all windows...
    else
    {
        // Dispatch any pending messages for this control...
        MSG msg;
	    while (::PeekMessage(&msg, GetSafeHwnd(), 0, 0, PM_REMOVE))
	    {
            if ((dwThreadID != dwMainThreadID) ||
		        (!AfxGetApp() -> PreTranslateMessage(&msg)))
		    {
			    ::TranslateMessage(&msg);
			    ::DispatchMessage(&msg);
		    }
	    }
    }
}

// Status...
void CStatusControl::Reset(void)
{
    // Reset the progress control...
    m_ProgressCtrl.SetRange32(0, INT_MAX);
    m_ProgressCtrl.SetPos(0);
    m_ProgressCtrl.SetStep(1);
    // Reset the cancelled status...
    m_bCancelled = FALSE;
}

void CStatusControl::Cancel(void)
{
    // Set the cancelled status...
    m_bCancelled = TRUE;
}

BOOL CStatusControl::IsCancelled()
{
    return m_bCancelled;
}

void CStatusControl::PostNcDestroy()
{
    // Destroy yourself if no one else is using you...
    if (GetRefCount() == 0)
        delete this;
}

// Message handlers...
BEGIN_MESSAGE_MAP(CStatusControl, CWnd)
	//{{AFX_MSG_MAP(CStatusControl)
	ON_BN_CLICKED(_SC_ID_CANCEL_, OnButtonCancel)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CStatusControl::OnButtonCancel() 
{
    m_bCancelled = TRUE;
}

void CStatusControl::OnPaint() 
{
/*
	CPaintDC dc(this);
    RECT rect;
    GetClientRect(&rect);
	CBrush bg(0xffffff);
	dc.FillRect(&rect,&bg);
*/
    CWnd::OnPaint();
}

//////////////////////////////////////
// CStatusProgess

CStatusProgress::CStatusProgress()
{
    m_dStepInc = 0;
    m_dEndPos = 0;
    m_dCurPos = 0;
    m_dAbsoluteCurPos = 0;
    m_nNumSteps = 0;
}

CStatusProgress::~CStatusProgress()
{
}

void CStatusProgress::Init(void)
{
    m_dStepInc = 0;
    m_dEndPos = 0;
    m_dCurPos = 0;
    m_dAbsoluteCurPos = 0;
    m_nNumSteps = 0;
}

void CStatusProgress::Attach(CStatusControlPtr pStatusCtrl)
{
    m_pStatusCtrl = pStatusCtrl;
}

void CStatusProgress::Cancel(void)
{
    if ((CStatusControl*) m_pStatusCtrl != NULL)
        m_pStatusCtrl->Cancel();
}

BOOL CStatusProgress::IsCancelled(void)
{
    if ((CStatusControl*) m_pStatusCtrl != NULL)
        return m_pStatusCtrl->IsCancelled();
    return FALSE;
}

void CStatusProgress::SetSteps(int nNumSteps, CStatusProgress& retSubProgress)
{
    // If step has passed its range...
    if (((CStatusControl*) m_pStatusCtrl == NULL) || (m_dCurPos >= m_dEndPos))
    {
        // There can be no substeps...
        retSubProgress.m_dAbsoluteCurPos = 0;
        retSubProgress.m_dCurPos = 0;
        retSubProgress.m_dEndPos = 0;
        retSubProgress.m_nNumSteps = 0;
        retSubProgress.m_dStepInc = 0;
        return;
    }
    // Set the new substep range...
    retSubProgress.m_dAbsoluteCurPos = m_dAbsoluteCurPos;
    retSubProgress.m_dCurPos = 0;
    retSubProgress.m_dEndPos = m_dStepInc;
    retSubProgress.m_nNumSteps = nNumSteps;
    retSubProgress.m_dStepInc = (m_dStepInc / (double)nNumSteps);
    if ((CStatusControl*) retSubProgress.m_pStatusCtrl == NULL)
        retSubProgress.Attach(m_pStatusCtrl);
}

void CStatusProgress::StepIt(BOOL bDispatchAllPendingMessages)
{
    if ((CStatusControl*) m_pStatusCtrl == NULL)
        return;
    // Update the current position...
    m_dCurPos += m_dStepInc;
    if (m_dCurPos >= m_dEndPos) 
        return;
    // Update progress bar...
    m_dAbsoluteCurPos += m_dStepInc;
    // Set the progress bar's new position...
    m_pStatusCtrl->SetProgressPos((int)m_dAbsoluteCurPos, bDispatchAllPendingMessages);
}

void CStatusProgress::SetText(LPCSTR lpszText)
{
    if ((CStatusControl*) m_pStatusCtrl == NULL)
        return;
    m_pStatusCtrl->SetProgressCtrlText(lpszText);
}

//////////////////////////////////////
// CStatusDlg

BEGIN_MESSAGE_MAP(CStatusDlg, CDialog)
	//{{AFX_MSG_MAP(CStatusDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CStatusDlg::CStatusDlg(DWORD dwStyle)
{
    // Create a dialog template for the modeless dialog...
    int nDlgTmpltSize = sizeof(DLGTEMPLATE) + (sizeof(DWORD) * 3);
    m_pDlgTmplt = new BYTE[nDlgTmpltSize];
    memset(m_pDlgTmplt, 0, nDlgTmpltSize);
    DLGTEMPLATE* pDlgTmplt = (DLGTEMPLATE*) m_pDlgTmplt;
    pDlgTmplt->style = WS_DLGFRAME | WS_POPUP | WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | dwStyle;
    pDlgTmplt->dwExtendedStyle = NULL;
    pDlgTmplt->cdit = 0;
    pDlgTmplt->x = 0; 
    pDlgTmplt->y = 0;
    pDlgTmplt->cx = _SC_DLG_WIDTH_;
    pDlgTmplt->cy = _SC_DLG_HEIGHT_;
}

CStatusDlg::~CStatusDlg()
{
    // Destroy the window...
    CWnd::DestroyWindow();

    // Delete our dialog's template...
    delete [] m_pDlgTmplt;
}

int CStatusDlg::DoModal()
{
    // NOTE: The status dialog should be a modeless dialog.
    //       There is no point to having a modal status dialog.
    return -1;
}

void CStatusDlg::DoModeless(CWnd* pParent, LPCSTR lpszTitle, DWORD dwProgressStyle, UINT nNumBlocks, LPCSTR lpszText)
{
    // Create a modeless dialog...
    // NOTE: The dialog should be destroyed when this object is destroyed.
    CreateIndirect(m_pDlgTmplt, pParent);

    // Set the title...
    SetWindowText((LPCTSTR)lpszTitle);

    // Create a status control...
    m_pStatusCtrl = new CStatusControl;

    // Place the status control in the dialog...
    CRect rectClient(_SC_DLG_CONTROL_XPOS_, _SC_DLG_CONTROL_YPOS_,
                     _SC_DLG_CONTROL_WIDTH_ + _SC_DLG_CONTROL_XPOS_,
                     _SC_DLG_CONTROL_HEIGHT_ + _SC_DLG_CONTROL_YPOS_);
    MapDialogRect(rectClient);
    m_pStatusCtrl->CreateEx(NULL, _T("STATUS"), WS_CHILD, rectClient, this, 0x200, NULL);

    // Tell the progress control how it should repaint itself...
    m_pStatusCtrl->SetProgressCtrlStyle(dwProgressStyle);
    m_pStatusCtrl->SetProgressCtrlNumBlocks(nNumBlocks);
    m_pStatusCtrl->SetProgressCtrlText(lpszText);

    // Resize the status control's controls...
    //CRect rectCancel(0, 0, _SC_DLG_CONTROL_CANCEL_WIDTH_, _SC_DLG_CONTROL_CANCEL_HEIGHT_);
    //MapDialogRect(rectCancel);
    CRect rectProgress(0, 0, _SC_DLG_CONTROL_PROGRESS_WIDTH_, _SC_DLG_CONTROL_PROGRESS_HEIGHT_);
    MapDialogRect(rectProgress);
   // rectProgress.left += rectCancel.right;
    //rectProgress.right += rectCancel.right;
   // m_pStatusCtrl->MoveCancelButton(rectCancel);
    m_pStatusCtrl->MoveProgressCtrl(rectProgress);
}

// Control access...
CStatusControlPtr CStatusDlg::GetStatusControlPtr()
{
    return m_pStatusCtrl;
}

// Progress...
void CStatusDlg::SetSteps(int nNumSteps, CStatusProgress& retSubProgress)
{
    // Forward the request to the status control...
    if ((CStatusControl*) m_pStatusCtrl == NULL)
        return;
    m_pStatusCtrl->SetSteps(nNumSteps, retSubProgress);
}

// Status...
void CStatusDlg::Reset(void)
{
    // Forward the request to the status control...
    if ((CStatusControl*) m_pStatusCtrl == NULL)
        return;
    m_pStatusCtrl->Reset();
}

void CStatusDlg::Cancel(void)
{
    // Forward the request to the status control...
    if ((CStatusControl*) m_pStatusCtrl == NULL)
        return;
    m_pStatusCtrl->Cancel();
}

BOOL CStatusDlg::IsCancelled()
{
    // Forward the request to the status control...
    if ((CStatusControl*) m_pStatusCtrl == NULL)
        return FALSE;
    return m_pStatusCtrl->IsCancelled();
}

BOOL CStatusDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

    // Set the icon...
    //m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    //SetIcon(m_hIcon, FALSE);

    // Show the dialog...
    ShowWindow(SW_SHOW);

    return TRUE;
}
