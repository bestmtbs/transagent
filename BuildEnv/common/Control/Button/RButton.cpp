// RButton.cpp : implementation file
//

#include "stdafx.h"
#include "RButton.h"



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/*
HBITMAP  LoadBitmapImage(char* filename);

BOOL     ImageDrawAt(HDC hdc,HBITMAP hBitmap,int x,int y);

BOOL     ImageStretchBlt(HDC hdc, HBITMAP hBitmap,int x, int y, int x2, int y2);

BOOL     ImageTransStretchBlt(HDC hdc, HBITMAP hBitmap,int x, int y, int x2, int y2,UINT Color);





HBITMAP  LoadBitmapImage(char* filename)
{
	HANDLE hFile;
	HBITMAP hBmp;
	DWORD   FileSize, dwRead;
	BITMAPFILEHEADER   fh;
	BITMAPINFO* ih;

	PVOID   pRaster;

	//Open File
	hFile = CreateFile(filename, GENERIC_READ,0,NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);

	if(hFile == INVALID_HANDLE_VALUE){ 
//		LOGOUT("NULL");
		DWORD Ret = GetLastError();
		if(ERROR_PATH_NOT_FOUND == Ret)
		{
	//		LOGOUT("PATH_NOT_FOUND");
		}
//		LOGOUT("%d",Ret);
		return	NULL;
	}

	//READ FILE HEADER & COLOR TABLE
	ReadFile(hFile,&fh,sizeof(BITMAPFILEHEADER),&dwRead,NULL);

	FileSize = fh.bfOffBits - sizeof(BITMAPFILEHEADER);
	ih	= (BITMAPINFO*)malloc( FileSize);
	ReadFile(hFile,ih,FileSize,&dwRead,NULL);

	//Create DIB Section  malloc buffer memory
	hBmp = CreateDIBSection(NULL,ih, DIB_RGB_COLORS,&pRaster,NULL,0);
	//Read Raster Data
	ReadFile(hFile,pRaster,fh.bfSize - fh.bfOffBits,&dwRead,NULL);
	free(ih);
	CloseHandle(hFile);

	return hBmp;
}




BOOL  ImageDrawAt(HDC hdc,HBITMAP hBitmap, int x, int y)
{
	HDC MemDC;
	HBITMAP OldBitmap;
	int bx, by;
	BITMAP bit;

	MemDC = CreateCompatibleDC(hdc);
	OldBitmap = (HBITMAP)SelectObject(MemDC,hBitmap);

	GetObject(hBitmap,sizeof(BITMAP),&bit);
	bx = bit.bmWidth;
	by = bit.bmHeight;

	BitBlt(hdc,x,y,bx,by,MemDC,0,0,SRCCOPY);

	SelectObject(MemDC,OldBitmap);
	DeleteDC(MemDC);

	return TRUE;

}


BOOL     ImageStretchBlt(HDC hdc, HBITMAP hBitmap,int x, int y, int x2, int y2)
{
	HDC MemDC;
	HBITMAP OldBitmap;
	int bx,by;
	BITMAP bit;

	MemDC = CreateCompatibleDC(hdc);
	OldBitmap = (HBITMAP)SelectObject(MemDC,hBitmap);

	GetObject(hBitmap,sizeof(BITMAP),&bit);
	bx = bit.bmWidth;
	by = bit.bmHeight;

	StretchBlt(hdc,x,y,x2,y2,MemDC,x,y,bx,by,SRCCOPY);

	SelectObject(MemDC,OldBitmap);
	DeleteDC(MemDC);

	return TRUE;
}



BOOL     ImageTransStretchBlt(HDC hdc, HBITMAP hBitmap,int x, int y, int x2, int y2,UINT Color)
{
	HDC MemDC;
	HBITMAP OldBitmap;
	int bx,by;
	BITMAP bit;
	MemDC = CreateCompatibleDC(hdc);
	OldBitmap = (HBITMAP)SelectObject(MemDC,hBitmap);
	GetObject(hBitmap,sizeof(BITMAP),&bit);
	bx = bit.bmWidth;
	by = bit.bmHeight;
//	TransparentBlt(hdc,x,y,x2,y2,MemDC,x,y,bx,by,Color);
	SelectObject(MemDC,OldBitmap);
	DeleteDC(MemDC);
	return TRUE;
}

*/
 

/////////////////////////////////////////////////////////////////////////////
// RButton

RButton::RButton()
{
	m_CheckedButton = m_MouseOnButton = FALSE;

}

RButton::~RButton()
{
	DeleteObject(m_Bitmap[0]);
	DeleteObject(m_Bitmap[1]);
	DeleteObject(m_Bitmap[2]);
}


BEGIN_MESSAGE_MAP(RButton, CButton)
	//{{AFX_MSG_MAP(RButton)
	ON_WM_MOUSEMOVE()
	ON_WM_CAPTURECHANGED()
	ON_WM_KILLFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// RButton message handlers

void RButton::LoadButtonImage(char* filename,int mode)
{
/*	CString RealPath = _T("");

	DWORD length = 2048;
	TCHAR buffer[2408] = "0";
	CString path = _T("");
	GetWindowsDirectory(buffer,length);
	

	
	RealPath += buffer;
	RealPath += "\\nics\\";
	RealPath += filename;

	switch(mode)
	{
	case 0://default
		m_Bitmap[0] = LoadBitmapImage(filename);
		break;
	case 1://onfocus
		m_Bitmap[1] = LoadBitmapImage(filename);
		break;
	case 2:
		m_Bitmap[2] = LoadBitmapImage(filename);
		break;
	default:
		break;
	}

*/
}

BOOL RButton::ImageDrawAt(HDC hdc,HBITMAP hBitmap, int x, int y)
{
	HDC MemDC;
	HBITMAP OldBitmap;
	int bx, by;
	BITMAP bit;

	MemDC = CreateCompatibleDC(hdc);
	OldBitmap = (HBITMAP)SelectObject(MemDC,hBitmap);

	GetObject(hBitmap,sizeof(BITMAP),&bit);
	bx = bit.bmWidth;
	by = bit.bmHeight;

	BitBlt(hdc,x,y,bx,by,MemDC,0,0,SRCCOPY);

	SelectObject(MemDC,OldBitmap);
	DeleteDC(MemDC);

	return TRUE;

}

void RButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	// TODO: Add your code to draw the specified item
	UINT State = lpDrawItemStruct->itemState;
	CRect rc;
	GetClientRect(rc);

	/*
	if(State & ODS_FOCUS)
	{
		if(State & ODS_SELECTED)
		{
			ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[0],rc.left,rc.top,rc.right,rc.bottom);
		}
		else 
		{
            ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[1],rc.left,rc.top,rc.right,rc.bottom);
		}
	}
	else
	{
		  ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[0],rc.left,rc.top,rc.right,rc.bottom);
	}

	*/
	BOOL bIsPressed  = (lpDrawItemStruct->itemState & ODS_SELECTED);
	BOOL bIsFocused  = (lpDrawItemStruct->itemState & ODS_FOCUS);
	BOOL bIsDisabled = (lpDrawItemStruct->itemState & ODS_DISABLED);

//	if (!m_MouseOnButton)
//			ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[1],rc.left,rc.top,rc.right,rc.bottom);
	
     if (bIsPressed)
	 {
           //ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[2],rc.left,rc.top,rc.right,rc.bottom);
		 HDC MemDC;
		 HBITMAP OldBitmap;

		 MemDC = CreateCompatibleDC(lpDrawItemStruct->hDC);
		 OldBitmap = (HBITMAP)SelectObject(MemDC,m_Bitmap[1]);
		 StretchBlt(lpDrawItemStruct->hDC,rc.left,rc.top,rc.right,rc.bottom,
					MemDC,0,0,m_BitmapInfo[1].bmWidth,m_BitmapInfo[1].bmHeight,SRCCOPY);
		 SelectObject(MemDC,OldBitmap);
		 DeleteDC(MemDC);

	 }
	else if (m_MouseOnButton)
	{
		 // ::SendMessage(this->GetParent()->GetSafeHwnd(),NICS_MOUSEON,0,(LPARAM)MAKELPARAM(Id,0));
		::SendMessage(this->GetParent()->GetSafeHwnd(),NICS_MOUSEON,0,(LPARAM)Id);
         //  ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[1],rc.left,rc.top,rc.right,rc.bottom);
		HDC MemDC;
		HBITMAP OldBitmap;
		
		MemDC = CreateCompatibleDC(lpDrawItemStruct->hDC);
		OldBitmap = (HBITMAP)SelectObject(MemDC,m_Bitmap[2]);
		StretchBlt(lpDrawItemStruct->hDC,rc.left,rc.top,rc.right,rc.bottom,
			MemDC,0,0,m_BitmapInfo[2].bmWidth,m_BitmapInfo[2].bmHeight,SRCCOPY);
		SelectObject(MemDC,OldBitmap);
		DeleteDC(MemDC);
	}
	else if (bIsDisabled)
	{
     //      ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[0],rc.left,rc.top,rc.right,rc.bottom);
		HDC MemDC;
		HBITMAP OldBitmap;
		
		MemDC = CreateCompatibleDC(lpDrawItemStruct->hDC);
		OldBitmap = (HBITMAP)SelectObject(MemDC,m_Bitmap[0]);
		StretchBlt(lpDrawItemStruct->hDC,rc.left,rc.top,rc.right,rc.bottom,
			MemDC,0,0,m_BitmapInfo[0].bmWidth,m_BitmapInfo[0].bmHeight,SRCCOPY);
		SelectObject(MemDC,OldBitmap);
		DeleteDC(MemDC);
	}
	else
	{
     //      ImageStretchBlt(lpDrawItemStruct->hDC,m_Bitmap[0],rc.left,rc.top,rc.right,rc.bottom);
		HDC MemDC;
		HBITMAP OldBitmap;
		
		MemDC = CreateCompatibleDC(lpDrawItemStruct->hDC);
		OldBitmap = (HBITMAP)SelectObject(MemDC,m_Bitmap[0]);
		StretchBlt(lpDrawItemStruct->hDC,rc.left,rc.top,rc.right,rc.bottom,
			MemDC,0,0,m_BitmapInfo[0].bmWidth,m_BitmapInfo[0].bmHeight,SRCCOPY);
		SelectObject(MemDC,OldBitmap);
		DeleteDC(MemDC);
	}

}

void RButton::CreateButton(int left, int top, int right, int bottom ,CWnd* hwnd,int id)
{
	CRect t_Rect;
	SetRect(&t_Rect,left,top,right,bottom);
	Create(NULL,BS_OWNERDRAW,t_Rect, hwnd, id);
	Id = id;
	ShowWindow( SW_SHOW );
}


void RButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CWnd* pWnd;
	CWnd* pParent;

	CButton::OnMouseMove(nFlags,point);

	if( nFlags & MK_LBUTTON && m_MouseOnButton == FALSE) 
	{
		return;
	}

	pWnd = GetActiveWindow();
	pParent = GetOwner();

	if((GetCapture() != this) && (pWnd != NULL) && (pParent != NULL))
	{

		m_MouseOnButton = TRUE;
		SetCapture();
		Invalidate();
	}
	else
	{
		POINT p2 = point;
		ClientToScreen(&p2);
		CWnd* wndUnderMouse = WindowFromPoint(p2);
		if(wndUnderMouse && wndUnderMouse->m_hWnd != this->m_hWnd)
		{
			if(m_MouseOnButton == TRUE)
			{
				m_MouseOnButton = FALSE;
				Invalidate();
			}
			if(!(nFlags & MK_LBUTTON))
			{
				ReleaseCapture();
			}
		}
	}
}
void RButton::OnCaptureChanged(CWnd *pWnd) 
{
	// TODO: Add your message handler code here
	if(m_MouseOnButton == TRUE)
	{
		ReleaseCapture();
		Invalidate();
	}

	CButton::OnCaptureChanged(pWnd);
}

void RButton::OnKillFocus(CWnd* pNewWnd) 
{
	CButton::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
	m_MouseOnButton = FALSE;
	Invalidate();
	
}

void RButton::LoadButtonImage(UINT id, int mode)
{
	switch(mode)
	{
	case 0:
		m_Bitmap[0].LoadBitmap(id);
		m_Bitmap[0].GetObject(sizeof(BITMAP),&m_BitmapInfo[0]);
		break;
	case 1:
		m_Bitmap[1].LoadBitmap(id);
		m_Bitmap[1].GetObject(sizeof(BITMAP),&m_BitmapInfo[1]);
		break;
	case 2:
		m_Bitmap[2].LoadBitmap(id);
		m_Bitmap[2].GetObject(sizeof(BITMAP),&m_BitmapInfo[2]);
		break;
	}
}
