#if !defined(AFX_IMAGEBUTTON_H__A4DF5FE8_87D2_4599_B03D_01D2B93E4E76__INCLUDED_)
#define AFX_IMAGEBUTTON_H__A4DF5FE8_87D2_4599_B03D_01D2B93E4E76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ImageButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CImageButton window

class CImageButton : public CBitmapButton
{
	DECLARE_DYNAMIC(CImageButton);
// Construction
public:
	CImageButton();
	CImageButton(UINT id1, UINT id2, UINT id3, UINT id4 = 0, UINT id5 = 0, CString sTxt = _T(""), int offset = 0);
	CImageButton(UINT id1, UINT id2, UINT id3, UINT id4 = 0, UINT id5 = 0, int nType =0, CString sTxt = _T(""), int offset = 0);
// Attributes
public:
	CBitmap	m_State1;  // 버튼에 대한 상태 (노말)
	CBitmap	m_State2;  // 버튼에 대한 상태 (온)
	CBitmap	m_State3;  // 버튼에 대한 상태 (다운)
	CBitmap m_State4;	// 체크버튼
	CBitmap m_State5;	// disable

	UINT    m_id1;
	UINT    m_id2;
	UINT    m_id3;
	UINT    m_id4;
	UINT    m_id5;


	BOOL	m_bChecked;
	BOOL m_bHover;						// indicates if mouse is over the button
	CSize m_ButtonSize;					// width and height of the button
	CBitmap mybitmap;
	BOOL m_bTracking;
	UINT m_resCursor;
	CString m_sTitle;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CImageButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDis);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CImageButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CImageButton)
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg LRESULT OnMouseLeave(WPARAM wparam, LPARAM lparam);
	afx_msg LRESULT OnMouseHover(WPARAM wparam, LPARAM lparam) ;
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	
	void	SetCheck(BOOL bCheck);
	BOOL	GetCheck() { return m_IsChecked; };
	void	SetTitle(TCHAR * szTitle)	{ m_sTitle = szTitle; };
	void	SetNotiParent(int nMod)	{ m_nNotiMode = nMod; };
	void	SetFontFace(DWORD dwFace) { m_dwFace = dwFace; };
	void	SetTexClr(COLORREF dwClr) { m_dwClr = dwClr; };
	void	SetFontSize( LONG lfSize ){ m_lfFontSize = lfSize ; } ;
	int		GetHeight();
	int		GetWidth();
	void	SizeToContent() ;
	void	ReLoadImage( UINT id1, UINT id2, UINT id3, UINT id4 = 0, UINT id5 = 0 ) ;

	void	ReLoadImage( CString id1, CString id2, CString id3, CString id4 = _T(""), CString id5 = _T("") ) ;
	BOOL    LoadImg(CBitmap& cBmp, CString strImg);
	
	LOGFONT GetWmLogFont(LPCTSTR font, int size, int Weight);

protected:
	int		m_nNotiMode;
	BOOL	m_IsChecked;
	LOGFONT m_lf;
	int		m_nOffset ;
	DWORD   m_dwFace;
	LONG	m_lfFontSize;		
	COLORREF	m_dwClr;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
#endif // !defined(AFX_IMAGEBUTTON_H__A4DF5FE8_87D2_4599_B03D_01D2B93E4E76__INCLUDED_)