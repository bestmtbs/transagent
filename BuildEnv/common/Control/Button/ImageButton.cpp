// ImageButton.cpp : implementation file
//

#include "stdafx.h"
#include "ImageButton.h"
#include <windows.h>



#include "shellapi.h"
#include "Shlwapi.h"
#pragma comment(lib, "Shlwapi.lib")
//#pragma comment(lib, "coredll.lib")



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#include "../../Include/MultiLang.h"
/////////////////////////////////////////////////////////////////////////////
// CImageButton
CImageButton::CImageButton()
{
	m_nNotiMode = 0;
	m_IsChecked = FALSE;
}

CImageButton::CImageButton(UINT id1, UINT id2, UINT id3, UINT id4, UINT id5 /*=0*/, CString sTxt, int offset)
{
	m_dwClr = RGB(0, 0, 0);
	m_dwFace = FW_BOLD;
	m_nOffset = offset; 





	if(NULL != m_State1.LoadBitmap(id1))
		m_id1 = id1;

	if(NULL != m_State2.LoadBitmap(id2))
		m_id2 = id2;

	if(NULL != m_State3.LoadBitmap(id3))
		m_id3 = id3;

	if(id4 > 0){
		if(NULL != m_State4.LoadBitmap(id4))
			m_id4 = id4;
	}
	if(id5 > 0){
		if(NULL != m_State5.LoadBitmap(id5))
			m_id5 = id5;
	}
	m_bHover = FALSE;
	m_bTracking = FALSE;
	m_bChecked = FALSE;
	m_sTitle = sTxt;
	m_resCursor = 0;
	m_IsChecked = FALSE;
	m_lf = GetWmLogFont(_T("����ü"), 12, 17);
	m_lfFontSize = m_lf.lfHeight ;
}

CImageButton::CImageButton(UINT id1, UINT id2, UINT id3, UINT id4, UINT id5 /*=0*/ ,int nType, CString sTxt, int offset)
{
	m_dwClr = RGB(0, 0, 0);
	m_dwFace = FW_NORMAL;
	m_nOffset = offset; 

	if(NULL != m_State1.LoadBitmap(id1))
		m_id1 = id1;

	if(NULL != m_State2.LoadBitmap(id2))
		m_id2 = id2;

	if(NULL != m_State3.LoadBitmap(id3))
		m_id3 = id3;

	if(id4 > 0){
		if(NULL != m_State4.LoadBitmap(id4))
			m_id4 = id4;
	}
	if(id5 > 0){
		if(NULL != m_State5.LoadBitmap(id5))
			m_id5 = id5;
	}

	m_bHover = FALSE;
	m_bTracking = FALSE;
	m_bChecked = FALSE;
	m_sTitle = sTxt;
	m_resCursor = 0;
	m_IsChecked = FALSE;
	m_lf = GetWmLogFont(_T("����"), 11, 11);
	m_lfFontSize = m_lf.lfHeight ;
}

CImageButton::~CImageButton()
{
}

IMPLEMENT_DYNAMIC(CImageButton, CBitmapButton)

BEGIN_MESSAGE_MAP(CImageButton, CButton)
	//{{AFX_MSG_MAP(CImageButton)
	ON_WM_SETCURSOR()
	ON_WM_MOUSEMOVE()
	ON_MESSAGE(WM_MOUSELEAVE, OnMouseLeave)
	ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover)
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_CTLCOLOR() //test
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImageButton message handlers
void CImageButton::SetCheck(BOOL bCheck)
{
	m_bChecked = bCheck;
	CBitmap* bitmap;
	if(bCheck)
		bitmap = &m_State5;
	else
		bitmap = &m_State1;
	

	CDC *pDC = GetDC();

	BITMAP bm;
	bitmap->GetBitmap(&bm);

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* _bitmap = memDC.SelectObject(bitmap);

	HBITMAP hBitmap = *bitmap;
	pDC->BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);

	ReleaseDC(pDC);
	m_IsChecked = bCheck;
	Invalidate();
}

void CImageButton::DrawItem(LPDRAWITEMSTRUCT lpDis) 
{
	// TODO: Add your code to draw the specified item
	CBitmap* bitmap;
	if(lpDis->itemState & ODS_SELECTED)
	{
		if(m_bChecked)
			bitmap = &m_State5;
		else
			bitmap = &m_State3;
	}
	else
	{
		if(lpDis->itemState & ODS_DISABLED)
			bitmap = &m_State4;
		else
		{		
			if(m_bHover)
			{
				if(m_bChecked)
					bitmap = &m_State5;
				else
					bitmap = &m_State2;
			}
			else
			{
				if(m_bChecked)
					bitmap = &m_State5;
				else
					bitmap = &m_State1;
			}		
		}
	}

	CDC *pDC = CDC::FromHandle(lpDis->hDC);

	BITMAP bm;
	bitmap->GetBitmap(&bm);

	CDC memDC;
	memDC.CreateCompatibleDC(pDC);
	CBitmap* _bitmap = memDC.SelectObject(bitmap);


	HBITMAP hBitmap = *bitmap;
	pDC->BitBlt(0, 0, bm.bmWidth, bm.bmHeight, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(_bitmap);
	if(m_sTitle.IsEmpty())
		return;
	
	CFont fname;
	CFont * pf = NULL;
	m_lf.lfWeight = m_dwFace;
	m_lf.lfHeight = m_lfFontSize ;
	fname.CreateFontIndirect(&m_lf);
	pf = (CFont*)(pDC->SelectObject(&fname));

	TEXTMETRIC tm;
	pDC->GetTextMetrics(&tm);

	int nTop = (bm.bmHeight / 2) - (tm.tmHeight / 2) - 1 + m_nOffset;
	pDC->SetTextAlign(TA_CENTER);
	pDC->SetBkMode(TRANSPARENT);
	pDC->SetTextColor(m_dwClr);
	int nfind = m_sTitle.Find('\n');
	if(nfind > 0)
	{
		pDC->TextOut(lpDis->rcItem.right / 2, 2, m_sTitle.Left(nfind));
		pDC->TextOut(lpDis->rcItem.right / 2, m_lf.lfHeight-2, m_sTitle.Right(m_sTitle.GetLength()-nfind-1));
	}
	else
	{
	//	pDC->TextOut(lpDis->rcItem.right / 2  + 5, nTop, m_sTitle);
		pDC->TextOut(lpDis->rcItem.right / 2, nTop, m_sTitle);
	}
	
	pDC->SelectObject(pf);
	fname.DeleteObject();
}

BOOL CImageButton::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	if (m_resCursor)
		::SetCursor(AfxGetApp()->LoadCursor(m_resCursor));
	return TRUE;
}

LRESULT CImageButton::OnMouseHover(WPARAM wparam, LPARAM lparam) 
{
	// TODO: Add your message handler code here and/or call default
	m_bHover=TRUE;
	Invalidate();
	switch(m_nNotiMode)
	{
		case 10:
			GetParent()->PostMessage(10);
			break;
		case 100:
			GetParent()->PostMessage(100);
			break;
	}
	return 0;
}


LRESULT CImageButton::OnMouseLeave(WPARAM wparam, LPARAM lparam)
{
	m_bTracking = FALSE;
	m_bHover=FALSE;
	Invalidate();
	switch(m_nNotiMode)
	{
		case 10:
			GetParent()->PostMessage(10);
			break;
		case 100:
			GetParent()->PostMessage(100);
			break;
	}
	return 0;
}

void CImageButton::OnMouseMove(UINT nFlags, CPoint point) 
{
	//	TODO: Add your message handler code here and/or call default

	if (!m_bTracking)
	{
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE|TME_HOVER;
		tme.dwHoverTime = 1;
		m_bTracking = _TrackMouseEvent(&tme);
	}
	CBitmapButton::OnMouseMove(nFlags, point);
}

void CImageButton::OnSetFocus(CWnd* pOldWnd) 
{
	CBitmapButton::OnSetFocus(pOldWnd);
	
	m_bHover=TRUE;
	Invalidate();
}

void CImageButton::OnKillFocus(CWnd* pNewWnd) 
{
	CBitmapButton::OnKillFocus(pNewWnd);
	
	m_bTracking = FALSE;
	m_bHover=FALSE;
	Invalidate();
}

int CImageButton::GetHeight()
{
	BITMAP bm;
	CDC memDC;
	// Top left
	m_State1.GetBitmap(&bm);
	return bm.bmHeight;
}

int CImageButton::GetWidth()
{
	BITMAP bm;
	CDC memDC;
	// Top left
	m_State1.GetBitmap(&bm);
	return bm.bmWidth;
}

void CImageButton::SizeToContent()
{

	BITMAP bmpinfo ;
	m_State1.GetBitmap( &bmpinfo ) ;

	SetWindowPos( NULL , 0 , 0 , bmpinfo.bmWidth , bmpinfo.bmHeight , SWP_NOMOVE|SWP_NOZORDER ) ;

}

void CImageButton::ReLoadImage( UINT id1, UINT id2, UINT id3, UINT id4 , UINT id5 )
{
	m_State1.DeleteObject() ;
	m_State2.DeleteObject() ;
	m_State3.DeleteObject() ;
	m_State4.DeleteObject() ;
	m_State5.DeleteObject() ;

	m_State1.LoadBitmap(id1);
	m_State2.LoadBitmap(id2);
	m_State3.LoadBitmap(id3);
	if(id4 > 0)
		m_State4.LoadBitmap(id4);
	if(id5 > 0)
		m_State5.LoadBitmap(id5);

	Invalidate();
}




void	CImageButton::ReLoadImage( CString id1, CString id2, CString id3, CString id4 , CString id5  )
{
	BOOL bResult  = TRUE;

	bResult =  PathFileExists(id1);

	if(TRUE == bResult)
		bResult =  PathFileExists(id2);

	if(TRUE == bResult)
		bResult =  PathFileExists(id3);

	if(id4.GetLength() >0 && TRUE == bResult)
		bResult =  PathFileExists(id4);

	if(id5.GetLength() >0 && TRUE == bResult)
		bResult =  PathFileExists(id5);

	if(TRUE != bResult)
		return;

/*	m_State1.DeleteObject() ;
	m_State2.DeleteObject() ;
	m_State3.DeleteObject() ;
	m_State4.DeleteObject() ;
	m_State5.DeleteObject() ;*/

	
	bResult = LoadImg(m_State1,id1);
	bResult = LoadImg(m_State2,id2);
	bResult = LoadImg(m_State3,id3);
	if(id4.GetLength() > 0)
		LoadImg(m_State4,id4);

	if(id5.GetLength() > 0)
		LoadImg(m_State5,id5);

	Invalidate();
}




LOGFONT CImageButton::GetWmLogFont(LPCTSTR font, int size, int Weight)
{
 HFONT hFont = (HFONT)GetStockObject(ANSI_FIXED_FONT);
 LOGFONT  LogFont;
 GetObject(hFont, sizeof(LOGFONT), &LogFont);

 LogFont.lfWeight = Weight;
 LogFont.lfCharSet = DEFAULT_CHARSET;
 LogFont.lfOutPrecision = OUT_DEFAULT_PRECIS;
 LogFont.lfClipPrecision = CLIP_DEFAULT_PRECIS;
 LogFont.lfQuality = ANTIALIASED_QUALITY;
 LogFont.lfPitchAndFamily = FF_DONTCARE;
 _tcscpy_s(LogFont.lfFaceName, font);   // ITS4

 LogFont.lfHeight = size;
 LogFont.lfWidth  = 0;

 return LogFont;
}

HBRUSH CImageButton::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CBitmapButton::OnCtlColor(pDC, pWnd, nCtlColor);
	return (HBRUSH)::GetStockObject(WHITE_BRUSH);	

	return hbr;
}

BOOL    CImageButton::LoadImg(CBitmap& cBmp, CString strImg)
{

	
	HBITMAP hBmp = (HBITMAP)LoadImage(NULL,strImg,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);

	if(NULL == hBmp)
		return FALSE;


	  if(cBmp.DeleteObject())
		  cBmp.Detach();
	
	  
	  cBmp.Attach(hBmp);


	  return TRUE;
}