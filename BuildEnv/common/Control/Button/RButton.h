#if !defined(AFX_RBUTTON_H__4B01C938_6B81_463F_9E17_D993EB0DB114__INCLUDED_)
#define AFX_RBUTTON_H__4B01C938_6B81_463F_9E17_D993EB0DB114__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// RButton window

#define NICS_MOUSEON	WM_USER + 99

class RButton : public CButton
{
// Construction
public:
	RButton();

// Attributes
public:

	BOOL  m_CheckedButton, m_CheckButton;

protected:
//	HBITMAP m_Bitmap[3];
	BOOL    m_MouseOnButton;
	CBitmap m_Bitmap[3];
	BITMAP	m_BitmapInfo[3];
	int		Id;




// Operations

public:
	BOOL  ImageDrawAt(HDC hdc,HBITMAP hBitmap, int x, int y);
	void  LoadButtonImage(char* filename, int mode);
	void  CreateButton(int left, int top, int right, int bottom,CWnd* hwnd, int id);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RButton)
	public:
		virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_VIRTUAL

// Implementation
public:
	void LoadButtonImage(UINT id,int mode);
	virtual ~RButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(RButton)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RBUTTON_H__4B01C938_6B81_463F_9E17_D993EB0DB114__INCLUDED_)
