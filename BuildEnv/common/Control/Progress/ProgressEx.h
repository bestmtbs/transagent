#if !defined(AFX_PROGRESSEX_H__24A15692_B120_41B4_9D6C_236E4E639A61__INCLUDED_)
#define AFX_PROGRESSEX_H__24A15692_B120_41B4_9D6C_236E4E639A61__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgressEx.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProgressEx window

class CProgressEx : public CProgressCtrl
{
// Construction
public:
	CProgressEx();
	virtual ~CProgressEx();

// Attributes
public:

protected:
    int      m_nMin, m_nMax, m_nWidth, m_nHeight, m_nPercent, m_nStep;
    BOOL     m_bInit, m_bDrawPercent, m_bSetBitmap;
    CRect    m_rcClient;
    CBitmap  m_bmpBar;
    COLORREF m_colorPercent, m_colorBack, m_colorBorder, m_colorBar;
	TCHAR		m_czPer[60];
	void		DoPaint(); 
	
	TCHAR		m_czFile[50];

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgressEx)
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	int     SetStep(int nStep);
	int     StepIt (DWORD flen, BOOL IsInc=FALSE);
	void    SetPercent(BOOL bDrawPercent, const COLORREF colorPercent = 0);
	void    SetColors (const COLORREF colorBack, const COLORREF colorBorder,
                       const COLORREF colorBar);
	int     SetPos(DWORD nPos);
	int     GetPos();
	void    SetRange(int nLower, int nUpper);
	void	SetFile(TCHAR * sz) { if(sz) _tcscpy(m_czFile, sz); };
	void	SetFLen(DWORD fl) { dwFLen = fl; };
	// Generated message map functions
protected:
	//{{AFX_MSG(CProgressEx)
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	LOGFONT			m_lfFont;
	DWORD	dwFLen;
	DWORD	dwFInc;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MIRPROGRESS_H__24A15692_B120_41B4_9D6C_236E4E639A61__INCLUDED_)
