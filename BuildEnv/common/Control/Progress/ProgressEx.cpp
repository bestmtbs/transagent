// ProgressEx.cpp : implementation file
//

#include "stdafx.h"
#include "ProgressEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProgressEx

CProgressEx::CProgressEx()
{
    m_nMin     = 0;
    m_nMax     = 100;
    m_nPercent   = 0;
    m_nStep      = 1;
    m_bInit      = FALSE;
    m_bSetBitmap = FALSE;
	memset((void*)m_czPer, 0, sizeof(m_czPer));
	dwFInc = 0;
	dwFLen = 0;
	m_bDrawPercent = TRUE; //bDrawPercent;
  //  m_colorPercent = RGB(119,223,44);//colorPercent;
//	m_colorBack   = RGB(0, 0, 0);
 //   m_colorBar    = RGB(0, 0, 0);
//	m_colorBorder = RGB(119,223,44);//RGB(59, 59, 59);
}

CProgressEx::~CProgressEx()
{
}


BEGIN_MESSAGE_MAP(CProgressEx, CProgressCtrl)
	//{{AFX_MSG_MAP(CProgressEx)
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgressEx message handlers

void CProgressEx::PreSubclassWindow() 
{
    ModifyStyle(0, PBS_SMOOTH);
	
	CProgressCtrl::PreSubclassWindow();
}

void CProgressEx::SetRange(int nMin, int nMax)
{
    m_nMin     = nMin;
    m_nMax     = nMax;
    m_nPercent   = 0;
	
	_stprintf(m_czPer, _T("%s"), _T("0 %"));

    CProgressCtrl::SetRange(nMin, nMax);
}

int CProgressEx::SetPos(DWORD nPos)
{
    m_nPercent = (int)(dwFInc * 100.0f / (float)m_nMax);
	if(m_nPercent > 100)
		m_nPercent = 100;
    if(m_bDrawPercent)
    {
		if(m_nPercent >= 0)
		{
			_stprintf(m_czPer, _T("%d%s"), m_nPercent, _T("%"), dwFInc/1024, dwFLen/1024);
			Invalidate();
			DoPaint();
		}
		//
    }

    return CProgressCtrl::SetPos(nPos);
}

int CProgressEx::StepIt(DWORD flen, BOOL IsInc)
{
	dwFInc = IsInc ? (dwFInc+flen) : flen;
    return SetPos(flen);
}

int CProgressEx::SetStep(int nStep)
{
    m_nStep = nStep;

    return CProgressCtrl::SetStep(nStep);
}

void CProgressEx::SetPercent(BOOL bDrawPercent, const COLORREF colorPercent)
{
    m_bDrawPercent = bDrawPercent;
    m_colorPercent = colorPercent;
}

void CProgressEx::SetColors(const COLORREF colorBack, const COLORREF colorBorder, 
                             const COLORREF colorBar)
{
    m_colorBack   = colorBack;
    m_colorBorder = colorBorder;
    m_colorBar    = colorBar;
	//m_colorBorder = RGB(198,223,44);//RGB(59, 59, 59);
}

void CProgressEx::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	CBrush		GkBrush, * pGkOldBrush = NULL;
    CBrush   Brush, * pOldBrush = NULL;
    CRect    rcBar;

    if(!m_bInit)
    {
        GetClientRect(&m_rcClient);
        m_nWidth  = m_rcClient.Width();
        m_nHeight = m_rcClient.Height();
        m_bInit   = TRUE;
    }

    // 바탕 그리기
	GkBrush.CreateSolidBrush(m_colorBack);
    pGkOldBrush = (CBrush *)dc.SelectObject(&GkBrush);
    dc.Rectangle(m_rcClient);
    dc.SelectObject(pGkOldBrush);
    Brush.DeleteObject();
	
    // Bar 그리기
    rcBar = m_rcClient;
    rcBar.DeflateRect(1, 1, 1, 1);
    rcBar.right = rcBar.left + m_nWidth * m_nPercent / 100;
    if(rcBar.right >= m_rcClient.right)
        rcBar.right = m_rcClient.right - 1;  
	dc.FillSolidRect(&rcBar, m_colorBorder);
    // Percentage 표시
    if(m_bDrawPercent)
    {
        CFont fname;
		CFont* _font = NULL;
		memset(&m_lfFont, 0, sizeof(LOGFONT));      
		_tcscpy(m_lfFont.lfFaceName, _T("Arial"));        
		m_lfFont.lfHeight = 15;
		m_lfFont.lfWeight = FW_NORMAL;	
		fname.CreateFontIndirect(&m_lfFont);
		_font = (CFont*)(dc.SelectObject(&fname));
        dc.SetBkMode(TRANSPARENT);
		dc.SetTextAlign(TA_CENTER);
        //dc.SetTextColor(m_colorPercent);
		dc.SetTextColor(RGB(119, 119, 119));
        //dc.DrawText(m_czPer, m_rcClient, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
		dc.TextOut(m_rcClient.Width() / 2, 0, m_czPer);
		dc.SelectObject(_font);
		_font = NULL;
		fname.DeleteObject();
#ifdef _DEBUG
//		CDebug::dprintf("Recv File %s", m_czPer);
#endif
    }
}

void CProgressEx::DoPaint() 
{
	CPaintDC dc(this); // device context for painting
	CBrush		GkBrush, * pGkOldBrush = NULL;
    CBrush   Brush, * pOldBrush = NULL;
    CRect    rcBar;

    if(!m_bInit)
    {
        GetClientRect(&m_rcClient);
        m_nWidth  = m_rcClient.Width();
        m_nHeight = m_rcClient.Height();
        m_bInit   = TRUE;
    }

    // 바탕 그리기
	GkBrush.CreateSolidBrush(m_colorBack);
    pGkOldBrush = (CBrush *)dc.SelectObject(&GkBrush);
    dc.Rectangle(m_rcClient);
    dc.SelectObject(pGkOldBrush);
    Brush.DeleteObject();
	
    // Bar 그리기
    rcBar = m_rcClient;
    rcBar.DeflateRect(1, 1, 1, 1);
    rcBar.right = rcBar.left + m_nWidth * m_nPercent / 100;
    //if(rcBar.right >= m_rcClient.right)
    //    rcBar.right = m_rcClient.right - 1;
   
	dc.FillSolidRect(&rcBar, m_colorBorder);

    // Percentage 표시
    if(m_bDrawPercent)
    {
		CFont fname;
		CFont* _font = NULL;
		memset(&m_lfFont, 0, sizeof(LOGFONT));      
		_tcscpy(m_lfFont.lfFaceName, _T("Arial"));        
		m_lfFont.lfHeight = 14;
		m_lfFont.lfWeight = FW_NORMAL;	
		fname.CreateFontIndirect(&m_lfFont);
		_font = (CFont*)(dc.SelectObject(&fname));
        dc.SetBkMode(TRANSPARENT);
		dc.SetTextAlign(TA_CENTER);
        dc.SetTextColor(m_colorPercent);
		//dc.SetTextColor(RGB(255, 255, 255));
        //dc.DrawText(m_czPer, m_rcClient, DT_SINGLELINE | DT_VCENTER | DT_CENTER);
		dc.TextOut(m_rcClient.Width() / 2, 0, m_czPer);
		dc.SelectObject(_font);
		_font = NULL;
		fname.DeleteObject();
#ifdef _DEBUG
	//	CDebug::dprintf("Recv File %s", m_czPer);
#endif
    }
}
int CProgressEx::GetPos()
{
	return m_nPercent;
}