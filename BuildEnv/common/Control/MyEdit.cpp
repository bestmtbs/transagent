// MyEdit.cpp : implementation file
//

#include "stdafx.h"

#include "MyEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMyEdit

CMyEdit::CMyEdit()
{
	m_BackColor = RGB(255,255,255);
  m_Brush.CreateSolidBrush(m_BackColor);
}

CMyEdit::~CMyEdit()
{
  m_Brush.DeleteObject();
}


BEGIN_MESSAGE_MAP(CMyEdit, CEdit)
	//{{AFX_MSG_MAP(CMyEdit)
	ON_WM_SETFOCUS()
	ON_WM_KILLFOCUS()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyEdit message handlers


void CMyEdit::OnSetFocus(CWnd* pOldWnd) 
{
	CEdit::OnSetFocus(pOldWnd);
	
	// TODO: Add your message handler code here
	m_BackColor = RGB(255,255,0);
  Invalidate(FALSE);
}

void CMyEdit::OnKillFocus(CWnd* pNewWnd) 
{
	CEdit::OnKillFocus(pNewWnd);
	
	// TODO: Add your message handler code here
	m_BackColor = RGB(255,255,255);
  Invalidate(FALSE);
}


void CMyEdit::SetBkGrndColor()
{
  // Delete the old brush
  m_Brush.DeleteObject();
  m_Brush.CreateSolidBrush(m_BackColor);
  CDC* pDC = GetDC();
  pDC->SetBkMode(OPAQUE);
  pDC->SetBkColor(m_BackColor);
  pDC->SelectObject(&m_Brush);
	CRect rc;
	GetClientRect(&rc);
	ScreenToClient(&rc);
  pDC->Rectangle(0, 0, rc.Width(), rc.Height());
  pDC->SetTextColor(RGB(0, 0, 0));
  pDC->TextOut(2, 2, m_Text.GetBuffer(m_Text.GetLength()));


}

void CMyEdit::SetControlFocus()
{
	m_BackColor = RGB(255,255,0);
}

BOOL CMyEdit::OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult) 
{
	// TODO: Add your specialized code here and/or call the base class
  if (message != WM_CTLCOLOREDIT) {
	  return CEdit::OnChildNotify(message, wParam, lParam, pLResult);
  }
  HDC hdcChild = (HDC)wParam;
  SetTextColor(hdcChild, RGB(0,0,0));
  SetBkColor(hdcChild, m_BackColor);
  return TRUE;
}

void CMyEdit::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
  GetWindowText(m_Text);
  SetBkGrndColor();
	
	// Do not call CEdit::OnPaint() for painting messages
}

