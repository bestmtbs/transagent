#ifndef _STATUSCTRL_H_
#define _STATUSCTRL_H_

#include <map>
#include <afxmt.h>
#include "referencecount.h"
#include "smartptr.h"
                                            
#define _SC_ID_PROGRESS_                    0x0100
#define _SC_ID_CANCEL_                      0x0101
#define _SC_DLG_WIDTH_                      146
#define _SC_DLG_HEIGHT_                     28
#define _SC_DLG_CONTROL_XPOS_               7
#define _SC_DLG_CONTROL_YPOS_               7
#define _SC_DLG_CONTROL_WIDTH_              130
#define _SC_DLG_CONTROL_HEIGHT_             14
#define _SC_DLG_CONTROL_CANCEL_WIDTH_       50
#define _SC_DLG_CONTROL_CANCEL_HEIGHT_      14
#define _SC_DLG_CONTROL_PROGRESS_WIDTH_     80
#define _SC_DLG_CONTROL_PROGRESS_HEIGHT_    14

#define SP_USE_PBS_STYLE                    0x0100
#define SP_BLOCK_STYLE                      0x0200
#define SP_SMOOTH_STYLE                     0x0400
#define SP_SHOWPERCENT                      0x0001
#define SP_SHOWMSG                          0x0002

//////////////////////////////////////
// Forward declarations...

class CStatusProgress;

//////////////////////////////////////
// CStatusProgressControl

class CStatusProgressControl : public CProgressCtrl
{
public:
    CStatusProgressControl();

    void SetNumBlocks(UINT nNumBlocks);
    void SetStyle(DWORD dwStyle);
    void SetMessage(LPCSTR lpszMsg);

protected:
	//{{AFX_MSG(CStatusProgressControl)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    UINT    m_nNumBlocks;
    DWORD   m_dwStyle;
    CString m_strMsg;
    CFont   m_font;
};

//////////////////////////////////////
// CStatusControl

class CStatusControl : public CWnd, public CReferenceCount
{
protected:
	DECLARE_DYNCREATE(CStatusControl)

public:
    // Construction/destruction...
    CStatusControl();
    ~CStatusControl();

    // Creation...
    BOOL CreateInPlaceOf(CWnd* pWnd, DWORD dwStyle = NULL);
    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
    BOOL CreateEx(DWORD dwExStyle, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, LPVOID lpParam = NULL);
protected:
    BOOL CreateChildWindows();

public:
    // Configuration...
    void SetProgressCtrlText(LPCSTR lpszText);
    void SetProgressCtrlStyle(DWORD dwStyle);
    void SetProgressCtrlNumBlocks(UINT nNumBlocks);
    BOOL MoveProgressCtrl(RECT& rect);
    BOOL MoveCancelButton(RECT& rect);

    // Progress...
    void SetSteps(int nNumSteps, CStatusProgress& retSubProgress);
    void SetProgressPos(int nPos, BOOL bDispatchAllPendingMessages = TRUE);

    // Status...
    void Reset(void);
    void Cancel(void);
    BOOL IsCancelled();

public:

	//{{AFX_VIRTUAL(CStatusControl)
	public:
    virtual void PostNcDestroy();
	protected:
	//}}AFX_VIRTUAL

protected:

	//{{AFX_MSG(CStatusControl)
    virtual afx_msg void OnButtonCancel();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
    // Controls...
    CStatusProgressControl  m_ProgressCtrl;
    CButton                 m_CancelButton;

    // Misc.
    static CString  m_strClassName;

    // Status...
    BOOL            m_bCancelled;
};

//////////////////////////////////////
// CStatusControlPtr

typedef CSmartPtr<CStatusControl> CStatusControlPtr;

//////////////////////////////////////
// CStatusProgess

class CStatusProgress
{
public:
    CStatusProgress();
    ~CStatusProgress();

    void Init(void);
    void Attach(CStatusControlPtr pStatusCtrl);
    void Cancel(void);
    BOOL IsCancelled(void);
    void SetSteps(int nNumSteps, CStatusProgress& retSubProgress);
    void StepIt(BOOL bDispatchAllPendingMessages = TRUE);
    void SetText(LPCSTR lpszText);

    double m_dStepInc;
    double m_dEndPos;
    double m_dCurPos;
    double m_dAbsoluteCurPos;
    int m_nNumSteps;

protected:
    CStatusControlPtr m_pStatusCtrl;
};

//////////////////////////////////////
// CStatusDlg

class CStatusDlg : public CDialog
{
// Construction
public:
	CStatusDlg(DWORD dwStyle = 0);
    ~CStatusDlg();

    int DoModal();
    void DoModeless(CWnd* pParent = NULL, LPCSTR lpszTitle = "", DWORD dwProgressStyle = SP_USE_PBS_STYLE, UINT nNumBlocks = 0, LPCSTR lpszText = "");

    // Control access...
    CStatusControlPtr GetStatusControlPtr();

    // Progress...
    void SetSteps(int nNumSteps, CStatusProgress& retSubProgress);

    // Status...
    void Reset(void);
    void Cancel(void);
    BOOL IsCancelled();

protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CStatusDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    CStatusControlPtr m_pStatusCtrl;
    void*             m_pDlgTmplt;
};

#endif//#ifndef _STATUSCTRL_H_