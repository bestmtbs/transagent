#if !defined(AFX_MYEDIT_H__A6EDB6D3_E5F9_416B_A512_A62AF6146819__INCLUDED_)
#define AFX_MYEDIT_H__A6EDB6D3_E5F9_416B_A512_A62AF6146819__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MyEdit.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMyEdit window

class CMyEdit : public CEdit
{
// Construction
public:
	CMyEdit();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyEdit)
	public:
	virtual BOOL OnChildNotify(UINT message, WPARAM wParam, LPARAM lParam, LRESULT* pLResult);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetControlFocus();
	void SetBkGrndColor();
	virtual ~CMyEdit();

	// Generated message map functions
protected:
	CBrush m_Brush;
	//{{AFX_MSG(CMyEdit)
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	CString m_Text;
	COLORREF m_BackColor;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MYEDIT_H__A6EDB6D3_E5F9_416B_A512_A62AF6146819__INCLUDED_)
