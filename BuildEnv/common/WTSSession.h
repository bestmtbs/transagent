
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      WTSSession.h
 @brief     Windows Terminal Service 기능의 Session Class 정의파일

            Local System의 Session 정보를 구한다
            개발자의 일관된 소스 관리를 위해 Windows Terminal Service 기능이
            지원하지 않는 OS에서 사용할 수 있도록 하였다.
            WTSSession Class에 ProcessExists, LoggedOn 추가
            활성세션이 존재하지 않을경우 -1을 리턴하도록 ActiveSessionId 수정

 @author    hang ryul lee
 @date      create 2007.09.13 
*/

#pragma once

#include  <WtsApi32.h>
#include  "IniFileEx.h"


typedef DWORD (WINAPI *fpWTSGetActiveConsoleSessionId)();

/**
 @class     CWTSSession
 @brief     WTSSession Class

            Network Session 정보를 수집하는 클래스 \n

 @author    hang ryul lee
*/
class CWTSSession
{
public:
    CWTSSession(void);
    CWTSSession(PWTS_SESSION_INFO _pSessionInfo);
    virtual ~CWTSSession(void);

    DWORD GetId() const;
    bool IsLoggedOn();
    bool ProcessExists(CString &_sFileName);
    CString GetWinStationName() const;
    WTS_CONNECTSTATE_CLASS GetState() const;
    DWORD GetProcessSessionId(DWORD _dwProcessId) const;
	DWORD GetCurrentLogonSessionID(DWORD* pFindPid=NULL);			//윈도우 계정에 로그인한 세션 ID
	CString  GetStringCurrentLogonSessID();	
	DWORD	 GetRemoteCurrentSessionId();
	
	BOOL    StartProcessAsUser(DWORD pid, CString strExePath, CString _strCmd=_T(""));

	DWORD GetCurrentLogonSessionID_Temp(DWORD* pFindPid=NULL);			//외주 모듈과 연동을 하기 위한것으로 추후 사용하지 않을 예정
	DWORD	RealLogin_SessionCheck(DWORD _dwSessionID);
	
	//int			GetPID_From_SessionID(DWORD dwSessionID, CString _strCompareProcessName);
private:
	
	HMODULE m_hMod;
	fpWTSGetActiveConsoleSessionId m_WTSGetActiveConsoleSessionId;
	
    bool ProcessExists9x(CString &_sFileName);
    bool ProcessExistsNT(CString &_sFileName);
    DWORD GetCurrentSessionId() const;
    CString GetShellName() const;
    bool IsWow64() const;
    bool IsWinNT() const;
    CString GetWinDir() const;

    PWTS_SESSION_INFO  m_pSessionInfo; /**< Session 정보가 담겨진 구조체*/
};


/**
 @class     CWTSSessions
 @brief     WTSSessions Class

            수집된 Network Session 정보를 List로 관리하는 클래스 \n

 @author    hang ryul lee
*/
class CWTSSessions
{
public:
    CWTSSessions();
    virtual ~CWTSSessions();

    INT GetActiveSessionId();
    INT GetCount() const;
    CWTSSession* GetItems(INT _nIndex);
private:
    CObList m_List;
    void ClearSessionList();
};