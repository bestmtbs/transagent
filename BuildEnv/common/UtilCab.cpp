#include "StdAfx.h"
#include "UtilCab.h"

#include "cab/CompressT.hpp"
#include "cab/ExtractResourceT.hpp"





BYTE DeCab(CHAR * szdir, CHAR * szcab, CHAR * szkey)
{
	char *s8_ErrMsg;
	Cabinet::CExtract         i_ExtrDecrypt;

	if(szkey != NULL)
		i_ExtrDecrypt.SetDecryptionKey(szkey);

	if (!i_ExtrDecrypt.CreateFDIContext()) 
	{
		i_ExtrDecrypt.GetLastError(&s8_ErrMsg);
		//CDebug::dprintf("Decrypt ERROR: Could not create FDI context: %s\n", s8_ErrMsg);
		return 1;
	}	

	// Now extract into subdirectory "_Decrypted" and the corresponding subdirectories in the CAB file
	if (!i_ExtrDecrypt.ExtractFile(szcab, szdir)) //if (!i_ExtrDecrypt.ExtractFile("d:\\22294.cab", "d:\\"))
	{
		i_ExtrDecrypt.GetLastError(&s8_ErrMsg);
		//CDebug::dprintf("Decrypt ERROR: Not all files could be extracted: %s\n", s8_ErrMsg);
		return 2;
	}
	return 0;
}


void Cab(CHAR * szcab, CHAR * szsrc, CHAR * name, CHAR * key)
{
	char *s8_ErrMsg;
	Cabinet::CCompress        i_Compress;
	char* s8_EncryptionKey = "" ;//"#gkruatkfkd";
	char s8_CompressFile[MAX_PATH];
	UINT u32_SplitSize = 0x7FFFFFFF;
	UINT u32_CabID = 12345;

	strcpy(s8_CompressFile, szcab);
	//sprintf(s8_CompressFile, "c:\\msbiz.cab");

	if(NULL != key)
	i_Compress.SetEncryptionKey(key);


	// ########################## Compress demo ############################

	// This will pack Explorer.exe and Notepad.exe into a CAB file with subfolders

	//CDebug::dprintf("%s -- %s -- %s\n\n", s8_CompressFile, szsrc, name);

	// Create a subdirectory "_Compressed" under the current working directory
	if (!i_Compress.CreateFCIContext(s8_CompressFile, u32_SplitSize, u32_CabID))
	{
		i_Compress.GetLastError(&s8_ErrMsg);
	//	CDebug::dprintf("Compress ERROR: Could not create FCI context: %s\n", s8_ErrMsg);
		//goto _RESOURCE;
	}

	if (!i_Compress.AddFile(szsrc, name, 0))
	{
		i_Compress.GetLastError(&s8_ErrMsg);
	//	CDebug::dprintf("Compress ERROR: Could not add Notepad.exe to cabinet: %s\n", s8_ErrMsg);
		//goto _RESOURCE;
	}

	if (!i_Compress.FlushCabinet(FALSE))
	{
		i_Compress.GetLastError(&s8_ErrMsg);
	//	CDebug::dprintf("Compress ERROR: Could not flush Cabinet: %s\n", s8_ErrMsg);
		//goto _RESOURCE;
	}
	
}



BYTE CabDir(CHAR * szcab, CHAR * szdir, CHAR * key)
{
	char *s8_ErrMsg;
	Cabinet::CCompress        i_Compress;
	//char* s8_EncryptionKey = "1234";//"";//"#gkruatkfkd";
	char s8_CompressFile[MAX_PATH];
	UINT u32_SplitSize = 0x7FFFFFFF;
	UINT u32_CabID = 12345;

	strcpy(s8_CompressFile, szcab);
	//sprintf(s8_CompressFile, "c:\\msbiz.cab");

	

	if(NULL != key)
		i_Compress.   SetEncryptionKey(key);


	// ########################## Compress demo ############################

	// This will pack Explorer.exe and Notepad.exe into a CAB file with subfolders

	//CDebug::dprintf("%s -- %s -- %s\n\n", s8_CompressFile, szsrc, name);

	// Create a subdirectory "_Compressed" under the current working directory
	if (!i_Compress.CreateFCIContext(s8_CompressFile, u32_SplitSize, u32_CabID))
	{
		i_Compress.GetLastError(&s8_ErrMsg);
		//CDebug::dprintf("Compress ERROR: Could not create FCI context: %s\n", s8_ErrMsg);
		return 1;
	}

	if (!i_Compress.AddFolder(szdir))
	{
		i_Compress.GetLastError(&s8_ErrMsg);
		//CDebug::dprintf("Compress ERROR: Could not add Notepad.exe to cabinet: %s\n", s8_ErrMsg);
		return 1;
	}

	if (!i_Compress.FlushCabinet(FALSE))
	{
		i_Compress.GetLastError(&s8_ErrMsg);
		//CDebug::dprintf("Compress ERROR: Could not flush Cabinet: %s\n", s8_ErrMsg);
		return 1;
	}
	return 0;
}
