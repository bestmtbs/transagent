
/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.05.29		
********************************************************************************/
/**
@file      CParseUtil.h 
@brief     CParseUtil  정의 파일  
@author   jhlee
@date      create 2011.08.09
*/
#ifndef __UTIL_H__
#define __UTIL_H__

class CParseUtil
{
public:
	CParseUtil() {}
	virtual ~CParseUtil() {}

public:
	static void ParseStringToStringArray(TCHAR* pszDiv, CString strLog, CStringArray* pstrArryLog);
	static CString RemoveSpecialCharacterForQuery(CString _strName);//2016-05-20 sy.choi DB 쿼리 실행 시 특수문자 제거
	static CString ReplaceSpecialCharacterForQuery(CString _strName);

};
#endif