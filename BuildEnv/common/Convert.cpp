

/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      Convert.h 
 @brief     Convert Class 구현 파일

            자료형 변환 클래스.

 @author    hang ryul lee
 @date      create 2011.05.06
 @note      모든 변환 메서드가 static형이므로 인스턴스를 생성하지 않고 사용하면 된다.
 @note      다음의 자료형들을 CString형으로 변환하는 기능을 제공한다.\n
            INT, UINT, INT64, UINT64, LONG, DWORD, WORD, DOUBLE, FLOAT, COleDateTime, _variant_t
 @note      다음의 자료형들을 INT형으로 변환하는 기능을 제공한다.\n
            CString
 @note      다음의 자료형들을 LONG형으로 변환하는 기능을 제공한다. \n
            CString
 @note      다음의 자료형들을 DOUBLE형으로 변환하는 기능을 제공한다. \n
            CString
 @note      다음의 자료형들을 FLOAT형으로 변환하는 기능을 제공한다. \n
            CString
*/

#include "stdafx.h"
#include "Convert.h"


/**
 @brief     INT형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 INT값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(INT _nValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[INT_MAX_LEN + 1] = {0, };
	
    _itot_s(_nValue, szBuffer, 10);
    sValue = szBuffer;
    
	return sValue;
}

/**
 @brief     UINT형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 UINT값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(UINT _nValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[UINT_MAX_LEN + 1] = {0, };
	
    _ultot_s(_nValue, szBuffer, 10);
    sValue = szBuffer;

	return sValue;
}

/**
 @brief     INT64형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 INT64값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(INT64 _nValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[I64_MAX_LEN + 1] = {0, };
	
    _i64tot_s(_nValue, szBuffer, I64_MAX_LEN, 10);
	sValue = szBuffer;
	return sValue;
}

/**
 @brief     UINT64형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 UINT64값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(UINT64 _nValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[UI64_MAX_LEN + 1] = {0, };
	
    _ui64tot_s(_nValue, szBuffer, UI64_MAX_LEN, 10);
	sValue = szBuffer;
	return sValue;
}

/**
 @brief     LONG형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 LONG값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(LONG _lValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[LONG_MAX_LEN + 1] = {0, };

    _ltot_s(_lValue, szBuffer, 10);
    sValue = szBuffer;
    return sValue;
}

/**
 @brief     DWORD형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 DWORD값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(DWORD _lValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[ULONG_MAX_LEN + 1] = {0, };

    _ultot_s(_lValue, szBuffer, 10);
    sValue = szBuffer;
    return sValue;
}

/**
 @brief     WORD형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 WORD값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(WORD _wValue)
{
    CString sValue = _T("");
    TCHAR szBuffer[INT_MAX_LEN + 1] = {0, };

    _itot_s(_wValue, szBuffer, 10);
    sValue = szBuffer;
    return sValue;
}

/**
 @brief     DOUBLE형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 DOUBLE값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(DOUBLE _dValue)
{
    CString sValue = _T("");
    CHAR szBuffer[_CVTBUFSIZE + 1] = {0, };

    _gcvt_s(szBuffer, sizeof(szBuffer), _dValue, 20);
    sValue = szBuffer;
    return sValue;
}

/**
 @brief     FLOAT형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 FLOAT값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(FLOAT _fValue)
{
    CString sValue = _T("");
    CHAR szBuffer[_CVTBUFSIZE + 1] = {0, };

    _gcvt_s(szBuffer, sizeof(szBuffer), _fValue, 8);
	sValue = szBuffer;
    return sValue;
}

/**
 @brief     COleDateTime형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 COleDateTime값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(COleDateTime _dtTime, const CString &_sFormat /* = _T("%Y-%m-%d %H:%M:%S") */)
{
    return _dtTime.Format(_sFormat);
}

/**
 @brief     _variant_t형을 CString형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _nValue    변환할 _variant_t값
 @param     [in] _sDefault  변환실패시 반환할 기본값(Default는 _T("")이다)
 @return    변환된 CString값
*/
CString CConvert::ToString(_variant_t &_vtValue)
{
    CString sValue = _T("");
    try
    {        
        switch(_vtValue.vt) 
		{
		case VT_R4:
            sValue = CConvert::ToString(_vtValue.fltVal);
			break;
		case VT_R8:
            sValue = CConvert::ToString(_vtValue.dblVal);
			break;
		case VT_BSTR:
			sValue = _vtValue.bstrVal;
			break;
		case VT_I2:
		case VT_UI1:
            sValue = CConvert::ToString(_vtValue.iVal);
			break;
		case VT_INT:
            sValue = CConvert::ToString(_vtValue.intVal);  
			break;
		case VT_I4:
            sValue = CConvert::ToString(_vtValue.lVal);
			break;
		case VT_DECIMAL:
            {
                double dValue = _vtValue.decVal.Lo32;
			    dValue *= (128 == _vtValue.decVal.sign) ? -1 : 1;
			    dValue /= pow(static_cast<double>(10), static_cast<double>(_vtValue.decVal.scale)); 
                sValue = CConvert::ToString(dValue);
			}
			break;
		case VT_DATE:
			{
				COleDateTime dt(_vtValue);				
                sValue = CConvert::ToString(dt);
			}
			break;        
		case VT_EMPTY:
		case VT_NULL:
			sValue.Empty();
			break;
        case VT_BOOL:
			sValue = (VARIANT_TRUE == _vtValue.boolVal) ? 'T':'F';
			break;
		default:
			sValue.Empty();
		}
    }
    catch(...)
    {
    }

    return sValue;
}

/**
 @brief     CString형이 숫자로만 구성되어있는지 확인한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    확인할 CString값
 @param     [in] _bSigned   +,- 부호를 무시할지 결정(Default true이므로 부호를 무시한다)
 @return    true/false
 @note      문자열의 앞/뒤에 공백이 존재할경우 공백은 비교대상에서 제외된다.
 @note      문자열이 "+" 또는 "-"로 시작될경우 해당 기호는 비교대상에서 제외된다.
*/
bool CConvert::IsNumber(CString _sValue, bool _bSigned /* = true */)
{
    _sValue = _sValue.Trim();

    if (_bSigned)
    {
        if ((_T("+") == _sValue.Left(1)) || (_T("-") == _sValue.Left(1)))
        {
            _sValue = _sValue.Mid(1, _sValue.GetLength() - 1);
        }
    }
    
    int nLen = _sValue.GetLength();
    if (0 == nLen)
        return false;

    for (int i = 0; i < nLen; i++)
    {
        if (('0' > _sValue[i]) || ('9' < _sValue[i]))
            return false;
    }
    return true;
}

/**
 @brief     CString형을 INT형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _nDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 INT값
 @note      변환할 문자열값이 INT_MAX, INT_MIN을 초과할경우 INT_MAX, INT_MIN값을 리턴한다.
*/
INT CConvert::ToInt(const CString &_sValue, INT _nDefault /* = 0 */)
{
    int nValue = _nDefault;
    
    if (!IsNumber(_sValue))
        return nValue;

    nValue = _tstoi(_sValue);

    return nValue;
}

/**
 @brief     CString형을 UINT형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _nDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 UINT값
 @note      변환할 문자열값이 UINT_MAX을 초과할경우 UINT_MAX값을 리턴한다.
*/
UINT CConvert::ToUInt(const CString &_sValue, UINT _nDefault /* = 0 */)
{
    UINT nValue = _nDefault;
    
    if (!IsNumber(_sValue, false))
        return nValue;

    nValue = _tcstoul(_sValue, NULL, 10);

    return nValue;
}

/**
 @brief     CString형을 INT64형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _nDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 INT64값
 @note      변환할 문자열값이 _I64_MAX, _I64_MIN을 초과할경우 _I64_MAX, _I64_MIN값을 리턴한다.
*/
INT64 CConvert::ToInt64(const CString &_sValue, INT64 _nDefault /* = 0 */)
{
    INT64 nValue = _nDefault;
    
    if (!IsNumber(_sValue))
        return nValue;

    nValue = _tcstoi64(_sValue, NULL, 10);

    return nValue;
}

/**
 @brief     CString형을 UINT64형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _nDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 UINT64값
 @note      변환할 문자열값이 _UI16_MAX을 초과할경우 _UI16_MAX값을 리턴한다.
*/
UINT64 CConvert::ToUInt64(const CString &_sValue, UINT64 _nDefault /* = 0 */)
{
    UINT64 nValue = _nDefault;
    
    if (!IsNumber(_sValue, false))
        return nValue;

    nValue = _tcstoui64(_sValue, NULL, 10);

    return nValue;
}

/**
 @brief     CString형을 LONG형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _lDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 LONG값
 @note      변환할 문자열값이 LONG_MAX, LONG_MIN을 초과할경우 LONG_MAX, LONG_MIN값을 리턴한다.
*/
LONG CConvert::ToLong(const CString &_sValue, LONG _lDefault /* = 0 */)
{
    LONG lValue = _lDefault;
    
    if (!IsNumber(_sValue))
        return lValue;

    lValue = _tstoi(_sValue);

    return lValue;
}

/**
 @brief     CString형을 DWORD형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _dwDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 DWORD값
 @note      변환할 문자열값이 ULONG_MAX를 초과할경우 ULONG_MAX값을 리턴한다.
*/
DWORD CConvert::ToDWORD(const CString &_sValue, DWORD _dwDefault /* = 0 */)
{
    DWORD dwValue = _dwDefault;
    
    if (!IsNumber(_sValue, false))
        return dwValue;

    dwValue = _tcstoul(_sValue, NULL, 10);

    return dwValue;
}

/**
 @brief     CString형을 WORD형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _wDefault  변환실패시 반환할 기본값(Default는 0이다)
 @return    변환된 WORD값
 @note      변환할 문자열값이 USHRT_MAX를 초과할경우 USHRT_MAX값을 리턴한다.
*/
WORD CConvert::ToWord(const CString &_sValue, WORD _wDefault /* = 0 */)
{
    WORD wValue = _wDefault;
    
    if (!IsNumber(_sValue, false))
        return wValue;

    DWORD dwValue = _tcstoul(_sValue, NULL, 10);
    if (USHRT_MAX < dwValue)
    {
        wValue = USHRT_MAX;
    }
    else
    {
        wValue = static_cast<WORD>(dwValue);
    }

    return wValue;
}

/**
 @brief     CString형을 DOUBLE형으로 변환한다.
 @author    hang ryul lee
 @date      2008.05.06
 @param     [in] _sValue    변환할 CString값
 @param     [in] _dDefault  변환실패시 반환할 기본값(Default는 0.0이다)
 @return    변환된 DOUBLE값
*/
DOUBLE CConvert::ToDouble(const CString &_sValue, DOUBLE _dDefault /* = 0.0 */)
{
    DOUBLE dValue = _dDefault;

    dValue = _tstof(_sValue);
    
    return dValue;
}

/**
 @brief     CString형을 COleDateTime형으로 변환한다.
 @author    hang ryul lee
 @date      2008.11.03
 @param     [in] _sValue    변환할 CString값
 @param     [in] _dDefault  변환실패시 반환할 기본값(Default는 "1899년 12월 30일 0시 0분 0초")
 @return    변환된 COleDateTime값
*/
COleDateTime CConvert::ToDateTime(const CString &_sValue, COleDateTime _dtDefault /* = COleDateTime() */)
{
    COleDateTime dtValue;
    if (!dtValue.ParseDateTime(_sValue))    
    {
        dtValue = _dtDefault;
    }
    return dtValue;
}

/**
 @brief     CString형을 Char* 형으로 변환한다.
 @author    hang ryul lee
 @date      2011.11.03
 @param     [in] _sValue    변환할 CString값
 @return    변환된 char*형 값
*/
CHAR* CConvert::ToChar(const CString& _sValue)
{
	CString strValue = _T("");

	strValue = _sValue;

	CHAR* pzData = NULL;
	TCHAR* pszData = NULL;

	pszData = strValue.GetBuffer(strValue.GetLength()+1);

	pzData = CConvert::SE_UniCodeToAnsiCode(pszData);

	strValue.ReleaseBuffer();

	return pzData;

}


/**
 @brief      
 @author    hang ryul lee
 @date       2011.08.01
 @param     
*/
LPSTR CConvert::SE_UniCodeToAnsiCode(LPWSTR pwzStr)
{
	int		nRes = 0;
	int		nUniLen = 0;
	int		nAnsiLen = 0;
	LPSTR		pzStr = NULL;

	if((pwzStr == NULL) || ((nUniLen = wcslen(pwzStr)) == 0))
		return NULL;
	
	nAnsiLen = WideCharToMultiByte(CP_ACP, 0, pwzStr, nUniLen, 0, 0, 0, 0);

	pzStr = (LPSTR) calloc(1, sizeof(char) *(nAnsiLen + 1));
	if(pzStr == NULL)
		return NULL;

	nRes = WideCharToMultiByte(CP_ACP, 0, pwzStr, nUniLen, pzStr, nAnsiLen +1 , 0, 0);

	if(nRes == 0)
	{
		SE_MemoryFree(pzStr);
		return NULL;
	}
	else if((nRes > 0) &&(nAnsiLen != 0))
	{
		 pzStr[nAnsiLen] = 0;
		 return pzStr;
	}
	else
	{
		SE_MemoryFree(pzStr);
		return NULL;
	}
}


// Kevin(2013-4-18)
/**
@fn			CConvert::UnicodeToAnsi(
				WCHAR *pszW,
				CHAR *pszA)
@brief		Convert.cpp
@param		*pszW
@param		*pszA
@return		int
@sa			
@callgraph	@date			2013년 4월 18일
*/
int CConvert::UnicodeToAnsi2(WCHAR *pszW, CHAR *pszA)
{

    ULONG cbAnsi=0, cCharacters = lstrlen(pszW);
    DWORD dwError;
	int nRet=0;

    nRet = WideCharToMultiByte(CP_ACP, 0, pszW, cCharacters, pszA, cbAnsi, NULL, NULL);

    // Convert to ANSI.
    if (0 == WideCharToMultiByte(CP_ACP, 0, pszW, cCharacters, pszA, nRet, NULL, NULL))
    {
        dwError = GetLastError();
        return -1;//HRESULT_FROM_WIN32(dwError);
    }
    return nRet;
}

// Kevin(2013-4-18)
/**
@fn			CConvert::AnsiToUnicode(
				char *strAnsi,
				TCHAR *strUnicode)
@brief		Convert.cpp
@param		*strAnsi
@param		*strUnicode
@return		int
@sa			
@callgraph	@date			2013년 4월 18일

*/
int CConvert::AnsiToUnicode2(char *strAnsi, TCHAR *strUnicode)
{
	BOOL bRet = TRUE;
	DWORD lenA;
	lenA = (DWORD)strlen(strAnsi);

	ULONG lenW = ::MultiByteToWideChar(CP_ACP, 0, strAnsi, lenA, 0, 0);
	if (lenW > 0)
	{
	  // Check whether conversion was successful
	  ::MultiByteToWideChar(CP_ACP, 0, strAnsi, lenA, (LPWSTR)strUnicode, lenW);
	}
	else
	{
	  // handle the error
       DWORD dwError = GetLastError();
        return -1;//HRESULT_FROM_WIN32(dwError);
	}

	return lenW;
}
/**
 @brief      
 @author    hang ryul lee
 @date       2011.08.01
 @param     
*/
LPWSTR CConvert::SE_AnsiCodeToUniCode(char* _pzText)
{
	int				nRes = 0;
	int				nAnsiLen = 0;
	int				nUniLen = 0;
	LPWSTR			pwzNewText = NULL;

	
	if( ( _pzText == NULL) || ( ( nAnsiLen = strlen( _pzText)) == 0)) return NULL;


	nUniLen = MultiByteToWideChar( CP_ACP, 0, _pzText, nAnsiLen, 0, 0);

	pwzNewText = ( LPWSTR) calloc( 1, sizeof( WCHAR) * ( nUniLen + 1));
	if( pwzNewText == NULL) return NULL;

	//
	// parameter : (b) 가 -1 이면 길이 자동계산
	//			   (d) 가 0 이면 필요로 하는 버퍼 크기 리턴
	//
	// return	 : (성공) 이고 (d) 가 0 이 아니면 리턴값은 쓰여진 uni 길이
	//			 : (성공) 이고 (d) 가 0 이면 리턴값은 요구되는 버퍼길이
	//			 : (실패) 면 리턴값 0
	//
	//										(a)     (b) (c)         (d)
	nRes = MultiByteToWideChar( CP_ACP, 0L, _pzText, -1, pwzNewText, nUniLen + 1);
	//
	// fail
	//
	if( nRes == 0) {

		SE_MemoryFree( pwzNewText);

		return NULL;
	}
	else if( ( nRes > 0) && ( nUniLen != 0)) {

		pwzNewText[ nUniLen] = 0;

		return pwzNewText;

	}
	else {

		SE_MemoryFree( pwzNewText);

		return NULL;
	}

}

/**
 @brief     CString형 문자열로 int형 해쉬값을 만든다.
 @author    kwang ho choi
 @date      2016-10-17
 @param     [in] word    해쉬값을 생성할 CString 값
 @return    int
 @note      http://www.sysnet.pe.kr/2/0/1223 에서 배껴옴
 */
unsigned int CConvert::hash2int(CString word)
{
	unsigned int hash = 0;
	int len = word.GetLength();
	int ch = 0;

	//unchecked
	{
		unsigned int poly = 0xEDB8832F;
		for (int i = 0; i < len; i++)
		{
			hash = (hash << 1) | (hash >> (32 - 1));

			ch = word[i];
			hash = (unsigned int)(poly * hash + ch);
		}
	}

	return hash;
}