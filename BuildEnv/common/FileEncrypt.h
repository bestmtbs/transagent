#pragma once
#include "stdAfx.h"

#define SEUDOKEY_BUFFER_SIZE 33
// Kevin(2013-5-7)
// 파일 헤더 암호화를 위한 키값 추가
#define KEY_ENC_KEY						L"C^87~a!Fe5b6"
#define KEY_ENC_KEY2					L"C^87~a!Fe5b6guY@q(x-"

// Kevin End(2013-5-7)
#define	ITCMS_SIG						"ITCMS"
#define	ITCMS_SIG2						"OfsEnc"

#define DEFAULT_HEADER_EXTRA_SIZE		16-2	// yjLee(2015-03-13) : 16byte - 2byte(encKeyType)
#define	ENC_BLOCK_LEN					16// Kevin(2013-6-4)
#define FILE_ENC_KEY_LEN				15// Kevin(2013-6-4)

#define FILE_ENC_TYPE_DEFAULT			"of"	// default reserve sig // Kevin(2013-6-5)
#define FILE_ENC_TYPE_SELF				"sf"	// selfenc sig // Kevin(2013-6-5)

#define ENC_TYPE_DEFAULT				0
#define ENC_TYPE_PART					1		// 1M이상의 경우 분할암호화 방식 적용예정



// yjLee(2015-03-10) : 암호화 헤더의 pseudoKey를 암호화 하는데 사용되는 암호화 키의 종류
#define ENC_KEY_TYPE_USERID				0		// yjLee(2015-03-10) : 사용자 ID + file hash로 암호화 헤더의 pseudoKey를 암호화
#define ENC_KEY_TYPE_UUID				1		// yjLee(2015-03-10) : 서버 UUID + file hash로 암호화 헤더의 pseudoKey를 암호화


//#define CRYPT_12
//#define _NEW_HEADER_V11_
#pragma pack(1)// Kevin(2013-6-3)

// Base header
typedef struct _DEFAULT_HEADER_ //32byte
{
	char	caption[6];
	DWORD	headersize;
	DWORD	headeroffset;
	BYTE	reserved[2];
	WORD	encKeyType;					// yjLee(2015-03-11) : 암호화 키 종류 (사용자 ID, 사이트 키)
	BYTE	reserved2[DEFAULT_HEADER_EXTRA_SIZE];	// 기타 필요시 추가할 수 있는 블럭(16byte) - encKeyType(2byte)
} DEFAULTENCHEADER, *PDEFAULTENCHEADER;

// Encrypt header
typedef struct _ENCRYPT_HEADER // 3904byte -> 파일로 저장시 3920byte
{
	char	sig[16];
	WCHAR	FileFullPathName[1024];		// Kevin(2013-6-3)
	WCHAR	FileExt[16];				// Kevin(2013-6-3)
	WCHAR	FileHash[64];				// Kevin(2013-6-3)
	SYSTEMTIME FileCreateTime;			// Kevin(2013-6-3)
	WCHAR	UserID[MAX_PATH];			// Kevin(2013-6-3)
	WCHAR	SiteIndex[16];				// Kevin(2013-6-3)
	WCHAR	Version[32];
	DWORD	length;						// PLAIN FILE LENGTH
	DWORD	dwKeyLen;					// 
	unsigned char pseudoKey[FILE_ENC_KEY_LEN+1];		//15 byte로 하면 16바이트의 키값이 나옴
// add new key
	//WCHAR	FileEncHash[64];// Kevin(2013-11-29)
	DWORD	dwExpireDate; // Kevin(2013-12-4) 파기일자
	WCHAR	SiteUUID[40];
#define ORIGIN_ENCRYPT_HEADER_RESERVED_SIZE 1024
#define EXPIRE_DATE_SIZE sizeof(DWORD)
#define SITEUUID_SIZE sizeof(WCHAR[40])
#define ENCRYPT_HEADER_RESERVED_SIZE ORIGIN_ENCRYPT_HEADER_RESERVED_SIZE - EXPIRE_DATE_SIZE - SITEUUID_SIZE
	BYTE	reserved[ENCRYPT_HEADER_RESERVED_SIZE]; // 향 후 구조 변경시 사용할 수 있도록 추가(1024byte) - fileEncHash - ExpireDate
}	Encrypt_Header, *PEncrypt_Header;


//typedef struct __ENCRYPT_TAIL
//{
//	unsigned char mac[16];
//}Encrypt_Tail;
#pragma pack()

class AFX_EXT_CLASS CFileEncrypt
{
public:
	CFileEncrypt(void);
	virtual ~CFileEncrypt(void);
	
	// Kevin(2013-5-13)
	CString m_strDecPathName;
	CString m_strEncPathName;

	BOOL SetDecFilePathName(CString strDecPathName); // Kevin(2013-5-13) 필요시 매 decfile()함수 호출전에 셋팅해야함. 내부에서 초기화함.
	BOOL SetEncFilePathName(CString strEncPathName);
	BOOL IsEncFile(CString _strEncFilePath, CString _strEncKey);
	// Kevin End(2013-5-13)

	BOOL EncFile(int _iInsUsrIdx, CString _strUserID, CString _strFilePath, CString _strEncFilePath, CString& _strKey, CString& _strHash,  CString _strSiteUUID=_T(""), BOOL _isSiteEncrypt=FALSE, DWORD* pRetCode= NULL, DWORD* pErrPos= NULL);
	BOOL DecFile(CString _strEncFilePath,CString _strEncKey,  DWORD* pRetCode= NULL, DWORD* pErrPos= NULL,  BOOL _bChangeName= TRUE, BOOL bOrgFileDel=TRUE);
	BOOL DecFile2(CString _strEncFilePath, CString _strDecPath, CString _strEncKey,  DWORD* pRetCode= NULL, DWORD* pErrPos= NULL,  BOOL _bChangeName= TRUE, BOOL bOrgFileDel=TRUE);
	
//	BOOL EncKeyBackupFile(DWORD* pRetCode = NULL, DWORD* pErrPos= NULL);			//key값을 저장한 파일을 암호화
//	BOOL DecKeyBackupFile(DWORD* pRetCode = NULL, DWORD* pErrPos= NULL);			//key값을 저장한 파일을 복호화 
	
	BOOL DecUseTheBackupFile();		//DB가 아니 key백업파일로 부터 key값을 가져와 복호화를 수행한다.
	
	BOOL GetEncFileHash(CString _strFilePath, CString& _strHash);

	BOOL SelfEncFile(CString _strUserID,CString _strFilePath,  DWORD* pRetCode= NULL, DWORD* pErrPos= NULL);
	BOOL SelfDecFile( CString _strUserID, CString _strEncFilePath, DWORD* pRetCode= NULL, DWORD* pErrPos= NULL);

	CString CreateFileHash(CString _strFilePath);
	CString GetFileHashFromHeader(CString _strEncFilePath);

	CString GetEncryptFileFullPath(CString _strFilePath);
	CString GetFileFullPath(CString _strEncFilePath);
	CString GetSelfEncryptFileFullPath(CString _strFilePath);
	CString GetSelfFileFullPath(CString _strEncFilePath, CString _strExt);

	CString SimpleXor(CString _strData);
	CString EncStrData(CString _strData, CString _strKey, DWORD* pdwErrorCode= NULL , DWORD* pdwErrPos= NULL);
	CString DecStrData(CString _strData, CString _strKey, DWORD* pdwErrorCode= NULL , DWORD* pdwErrPos= NULL);

	BOOL AES256_Enc(LPTSTR _pKey, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos);
	BOOL AES256_Dec(LPTSTR _pKey, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos);
	BOOL AES256_Enc2(LPBYTE _pKey, DWORD dwKeyLen, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos);// Kevin(2013-6-4)
	BOOL AES256_Dec2(LPBYTE _pKey, DWORD dwKeyLen, BYTE* _pDataBuff, DWORD _dwDataBuffLen, DWORD& _dwDataLen, DWORD* pdwErrorCode , DWORD* pdwErrPos);// Kevin(2013-6-4)
	BOOL SetTiorSaverVersion(CString strVer);
	BOOL SetReserveData(BYTE *Reserve);
	BOOL SetEncInfo(CString strVer, BYTE *Reserve);
	BOOL GetFileHeader(CString _strEncFilePath, BYTE *pHeader, ULONG ulHeaderLen);
	BOOL GetFileAllHeader(CString _strEncFilePath, DEFAULTENCHEADER& _pBaseHeader, Encrypt_Header& _pEncHeader);	// yjLee(2015-03-17) : base/encrypt header 정보 가져오는 함수
	BOOL CreateKey(CString _strUserID,CString& _strKey, CString _strHash);

	// Kevin(2013-6-3)
	CString GetSiteIdx() { return m_strSiteIdx; };

	//CString SimpleDecXor(CString _strDecData);
private:

	//멤버 변수 형식으로 잡아 코드를 간결하게 처리 하려 했으나 이런 방식으로 처리해 놓으면 ThreadSafe하지 않다.
	//사용하는 입장에서 ThreadSafe하게 만들어야 써야 하므로 각 암복호화 함수시 독립적으로 처리 할수 있도록 작업함
	// 코드를 간결하게 할 수 있지만 ThreadSafe에 대해 신경써서 추후 필요시 수정하도록 한다.
/*	HCRYPTPROV m_hCryptProv;		
	HCRYPTKEY m_hKey; 
	HCRYPTHASH m_hHash ; 

	BOOL AES256_Init(CString _strKey);*/ 
	
	BOOL LoadSiteIdx();

	CString m_strSiteIdx;
	CString m_strProductVersion;
	BYTE m_Reserve[2];
	
	// Kevin End(2013-6-3)
};

BOOL IsFileExist2(WCHAR *pFileName);