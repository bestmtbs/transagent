#pragma once

#include <iostream>
#include <string>

#include <vector>
using namespace std;

#ifdef _UNICODE
#define tcout       wcout
#define tstring     wstring
#else
#define tcout       cout
#define tstring     string
#endif

typedef struct _installappinfo
{
	CString strAppName;
	CString strAppCompany;
	CString strAppVersion;
	CString strAppLocation;
	CString strAppInsDate;
	CString strStatus;
	_installappinfo() {
		strAppName = _T("");
		strAppCompany = _T("");
		strAppVersion = _T("");
		strAppLocation = _T("");
		strAppInsDate = _T("");
		strStatus = _T("");
	}
}INSTALLAPPINFO;

class CRegistryQueryForAppList
{
public:
	CRegistryQueryForAppList(void);
	~CRegistryQueryForAppList(void);

	vector<INSTALLAPPINFO> m_vecInstallInfo[2];
	vector<INSTALLAPPINFO> m_vecInstallInfoWow[2];
	BOOL m_bSrcList;
	BOOL m_bSrcWowList;

	void RegistryEnum(vector<INSTALLAPPINFO>& vecAppList);
	void RegistryEnum();
	void RegistryEnumWow6432(vector<INSTALLAPPINFO>& vecAppList);
	void RegistryEnumWow6432();
	CString GetInstallAppList();
	BOOL IsRegistryExistValue(HKEY hKey,LPCTSTR szName);
	tstring RegistryQueryValue(HKEY hKey,LPCTSTR szName);
};
