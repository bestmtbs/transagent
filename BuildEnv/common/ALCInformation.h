#pragma once

#include "StdAfx.h"
#include <vector>


#define READ_PERMISSIONS (FILE_READ_DATA | \
						  FILE_READ_ATTRIBUTES)
#define WRITE_PERMISSIONS (FILE_WRITE_DATA | \
						   FILE_APPEND_DATA | \
						   FILE_WRITE_ATTRIBUTES | \
						   FILE_WRITE_EA)
#define EXECUTE_PERMISSIONS (FILE_READ_DATA | \
							 FILE_EXECUTE)


using namespace std;

typedef struct _ACE_INFO
{
	ACCESS_ALLOWED_ACE* ace;
	BOOL			allowed;
} ACE_INFO, *PACE_INFO;

typedef struct _ACL_USER
{
	TCHAR domain[MAX_PATH];
	TCHAR username[MAX_PATH];
	BOOL	read;
	BOOL	write;
	BOOL	execute;
} ACL_USER, *PACL_USER;


class CALCInformation
{
public:
	CALCInformation(void);
	~CALCInformation(void);

	void	SetPath(CString _strPath);
	BOOL	GetALCInfo(vector<ACL_USER>& _vtActUser);
	BOOL	ChangeFileSecurity(CString path,CStringArray& arrAccountList, DWORD _dwAccessOption);
private:
	CString m_strPath;
	vector<ACE_INFO> m_vtAcl_into;
	
	HRESULT Acl_Query(BYTE** pSecDescriptor);
	void AddToInfo(ACCESS_ALLOWED_ACE* pAce);
	BOOL AddToACL(PACL& pACL, CString AccountName, DWORD AccessOption);

};
