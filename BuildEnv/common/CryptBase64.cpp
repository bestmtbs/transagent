
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/

/**
 @file      CryptBase64.cpp 
 @brief     CCryptBase64 Class 구현 파일

            Base64 Encoding/Decoding 기능을 구현하였음.\n
            ISMS 3.0/3.5에서 사용한 DCPcrypt v2.0의 DCPBase64 소스를 
            기반으로 작성하였음.
            http://www.cityinthesky.co.uk/cryptography.html

 @author    hang ryul lee
 @date      create 2011.07.21 
 @note      Base64 테이블 구성시 'A'~'Z', 'a' ~ 'z', 0 ~ 9, +, - 의값을 char로형으로
            사용하였을 경우 영문 대문자 'O'와 숫자 0을 구분하지 못하여 인코딩에 문제가
            발생하여 모두 실제 Byte값으로 구성하였음.
*/

#include "stdafx.h"
#include "CryptBase64.h"

const BYTE CCryptBase64::B64[64] = {
    65, 66, 67,  68,  69,  70,   71,  72,  73,  74,  75,  76,  77,  78,  79,  80,   81,  82,  83,  84,   85,  86,  87,  88,  89,  90, //'A' ~ 'Z'
    97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, //'a' ~ 'z'
    48, 49, 50,   51,  52,  53,  54,  55,  56,  57,  //0 ~ 9
    43,  47};  //+, -


/**
 @brief     생성자
 @author    hang ryul lee
 @date      2008.07.21
*/
CCryptBase64::CCryptBase64()
{
}

/**
 @brief     파괴자
 @author    hang ryul lee
 @date      2008.07.21
*/
CCryptBase64::~CCryptBase64()
{
}

/**
 @brief     주어진 원본 데이터 포인터의 내용을 Base64로 인코딩한다.
 @author    hang ryul lee
 @date      2008.07.21
 @param     [in] _pInput    인코딩할 원본 데이터에대한 포인터
 @param     [out] _pOutput  인코딩된 데이터를 포함할 포인터
 @param     [in] _dwSize    인코딩할 원본 데이터의 크기
 @return    인코딩된 데이터의 크기
*/
DWORD CCryptBase64::Encode(const PVOID _pInput, PVOID _pOutput, DWORD _dwSize)
{
    if ((NULL == _pInput) || (NULL == _pOutput))
        return 0;

    PBYTE pIn  = static_cast<PBYTE>(_pInput);
    PBYTE pOut = static_cast<PBYTE>(_pOutput);
    DWORD dwIn = 0;
    DWORD dwOut = 0;
    for (DWORD i = 1; i <= (_dwSize / 3); i++)
    {
        pOut[dwOut + 0] = B64[pIn[dwIn] >> 2];
        pOut[dwOut + 1] = B64[((pIn[dwIn] & 3) << 4) + (pIn[dwIn + 1] >> 4)];
        pOut[dwOut + 2] = B64[((pIn[dwIn + 1] & 15) << 2) + (pIn[dwIn + 2] >> 6)];
        pOut[dwOut + 3] = B64[pIn[dwIn + 2] & 63];
        dwOut += 4;
        dwIn += 3;
    }

    switch(_dwSize % 3)
    {
    case 1 :
        pOut[dwOut + 0] = B64[pIn[dwIn] >> 2];
        pOut[dwOut + 1] = B64[(pIn[dwIn] & 3) << 4];
        pOut[dwOut + 2] = '=';
        pOut[dwOut + 3] = '=';
        break;
    case 2 :
        pOut[dwOut + 0] = B64[pIn[dwIn] >> 2];
        pOut[dwOut + 1] = B64[((pIn[dwIn] & 3) << 4) + (pIn[dwIn + 1] >> 4)];
        pOut[dwOut + 2] = B64[(pIn[dwIn + 1] & 15) << 2];
        pOut[dwOut + 3] = '=';
        break;
    }

    return ((_dwSize + 2) / 3) * 4;
}

/**
 @brief     주어진 인코딩된 데이터 포인터의 내용을 Base64로 디코딩한다.
 @author    hang ryul lee
 @date      2008.07.21
 @param     [in] _pInput    디코딩할 원본 데이터에대한 포인터
 @param     [out] _pOutput  디코딩된 데이터를 포함할 포인터
 @param     [in] _dwSize    디코딩할 원본 데이터의 크기
 @return    디코딩된 데이터의 크기   
*/
DWORD CCryptBase64::Decode(const PVOID _pInput, PVOID _pOutput, DWORD _dwSize)
{

    if ((NULL == _pInput) || (NULL == _pOutput))
        return 0;

    PBYTE pIn  = static_cast<PBYTE>(_pInput);
    PBYTE pOut = static_cast<PBYTE>(_pOutput);
    DWORD dwIn = 0;
    DWORD dwOut = 0;
    BYTE byTemp[4] = {0, };
    DWORD dwResult = 0;

    for (DWORD i = 1; i <= (_dwSize / 4); i++)
    {
        for (DWORD j = 0; j <= 3; j++)
        {
            if ((65 <= pIn[dwIn]) && (90 >= pIn[dwIn]))        //'A' ~ 'Z'
                byTemp[j] = pIn[dwIn] - 65;         //65 = 'A'
            else if ((97 <= pIn[dwIn]) && (122 >= pIn[dwIn]))  //'a' ~ 'z'
                byTemp[j] = pIn[dwIn] - 97 + 26;   // 97 = 'a'
            else if ((48 <= pIn[dwIn]) && (57 >= pIn[dwIn]))   //0 ~ 9
                byTemp[j] = pIn[dwIn] - 48 + 52;   //0 = 48 
            else if ('+' == pIn[dwIn])
                byTemp[j] = 62; 
            else if ('/' == pIn[dwIn])
                byTemp[j] = 63;
            else if (61 == pIn[dwIn])
                byTemp[j] = 0xFF;

            dwIn += 1;
        }

        pOut[dwOut] = (byTemp[0] << 2) | (byTemp[1] >> 4);
        dwResult = dwOut + 1;
        if ((0xFF != byTemp[2]) && (0xFF == byTemp[3]))
        {
            pOut[dwOut + 1] = (byTemp[1] << 4) | (byTemp[2] >> 2);
            dwResult = dwOut + 2;
            dwOut += 1;
        }
        else if (0xFF != byTemp[2])
        {
            pOut[dwOut + 1] = (byTemp[1] << 4) | (byTemp[2] >> 2);
            pOut[dwOut + 2] = (byTemp[2] << 6) | byTemp[3];
            dwResult = dwOut + 3;
            dwOut += 2;
        }
        dwOut += 1;
    }

    return dwResult;
}

/**
 @brief     문자열을 Base64로 인코딩한다.
 @author    hang ryul lee
 @date      2008.07.21
 @param     [in] _sValue    인코딩할 문자열
 @return    인코딩된 문자열
 @note      인코딩할 문자열이 공백인경우 인코딩을 수행하지 않고 공백문자열을 리턴한다.
 @note      소스 문자열의값이 Unicode일 경우 MBCS로 변경후 인코딩 작업을 진행한다.
*/
CString CCryptBase64::EncodeStr(const CString &_sValue)
{
    if (_T("") == _sValue)
        return _T("");

    CT2AEX<> sInValue(_sValue);
    size_t nLen = strlen(sInValue.m_psz);   //데이터의 크기를 byte단위로 구해야해서 strlen()을 사용하였음.
    size_t nEncodeLen = ((nLen + 2) / 3) * 4;
    
    PBYTE pEncodeBuf = new BYTE[nEncodeLen];
    ::ZeroMemory(pEncodeBuf, nEncodeLen);
    DWORD dwCnt = Encode(sInValue.m_psz, pEncodeBuf, static_cast<DWORD>(nLen));
    CString sEncode(pEncodeBuf);
    delete [] pEncodeBuf;

    return sEncode.Left(dwCnt);
}

/**
 @brief     인코딩 되어 있는 문자열을 Base64로 디코딩한다.
 @author    hang ryul lee
 @date      2008.07.21
 @param     [in] _sValue    디코딩할 문자열
 @note      디코딩할 문자열이 공백인 경우 디코딩을 수행하지 않고 공백문자열을 리턴한다.
 @note      디코딩할 문자열의값이 Unicode일 경우 MBCS로 변경후 디코딩 작업을 진행한다.
*/
CString CCryptBase64::DecodeStr(const CString &_sValue)
{
    if (_T("") == _sValue)
        return _T("");

    CT2AEX<> sInValue(_sValue);
    size_t nLen = strlen(sInValue.m_psz);   //데이터의 크기를 byte단위로 구해야해서 strlen()을 사용하였음.
    size_t nDecodeLen = ((nLen + 2) / 3) * 4;
    
    PBYTE pDecodeBuf = new BYTE[nDecodeLen];
    ::ZeroMemory(pDecodeBuf, nDecodeLen);
    DWORD dwCnt = Decode(sInValue.m_psz, pDecodeBuf, static_cast<DWORD>(nLen));
    CString sDecode(pDecodeBuf);
    delete [] pDecodeBuf;
 
    return sDecode.Left(dwCnt);
}

/**
 @brief     원본 data size값을이용하여 인코딩에 필요한 buffer size를 구한다.
 @author    hang ryul lee
 @date      2008.11.11
 @param     [in] _dwSize    원본 data size
 @return    인코딩에 필요한 buffer size
*/
size_t CCryptBase64::GetEncodeBufferSize(DWORD _dwSize) const
{
    return ((_dwSize + 2) / 3) * 4;
}

/**
 @brief     원본 data size값을이용하여 디코딩에 필요한 buffer size를 구한다.
 @author    hang ryul lee
 @date      2008.11.11
 @param     [in] _dwSize    원본 data size
 @return    디코딩에 필요한 buffer size
*/
size_t CCryptBase64::GetDecodeBufferSize(DWORD _dwSize) const
{
    return ((_dwSize + 2) / 3) * 4;
}