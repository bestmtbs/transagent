
/*******************************************************************************             
  PROJECT  :    ITCMS                
  
  PRODUCTION COMPANY : DSNTCH  digital solution & technology partner
  
  URL : www.dsntech.com
  
  DIVISION : Business Department Div
  
  DATE : 2011.05.29		
 ********************************************************************************/
/**
 @file      SQLite.h 
 @brief     SQLite Class 정의 파일

            SQLite를 Wrapping한 클래스.

 @author    hang ryul lee
 @date      create 2011.05.12 
 @note      sqlite 3.5.8 버전을 기반으로 작성하였음.(http://www.sqlite.org/)
 @note      sqlite3.h. sqlite3ext.h, sqlite3.c 파일이 필요함.
 @note      sqlite3.c 파일에 다음 내용을 추가함. \n
            #define SQLITE_THREADSAFE   1
 @note      sqlite3.c 파일에 컴파일 경고 수준4(/W4) 환경에서 발생하는 경고를 제거하기 위해 추가함. \n
            #pragma warning(disable:4018) \n
            #pragma warning(disable:4055) \n
            #pragma warning(disable:4100) \n
            #pragma warning(disable:4127) \n
            #pragma warning(disable:4132) \n
            #pragma warning(disable:4152) \n
            #pragma warning(disable:4244) \n
            #pragma warning(disable:4389) \n
            #pragma warning(disable:4701) \n
            #pragma warning(disable:4996) \n
 @note      sqlite3.c 파일에 코드분석에서 발생하는 경고를 제거하기 위해 추가함.\n
            #pragma warning(disable:6001) \n
            #pragma warning(disable:6011) \n
            #pragma warning(disable:6235) \n
            #pragma warning(disable:6239) \n
            #pragma warning(disable:6244) \n
            #pragma warning(disable:6246) \n
            #pragma warning(disable:6295) \n
            #pragma warning(disable:6326) \n
            #pragma warning(disable:6328) \n
            #pragma warning(disable:6385) \n
            #pragma warning(disable:6386) \n
 @note      sqlite3.c 속성을 다음과 같이 설정함. \n
            sqlite3.c 속성 페이지 창에서 설정한다. \n
            "구성 속성>C/C++>미리 컴파일된 헤더>미리 컴파일된 헤더 만들기/사용" 의 값을 
            "미리 컴파일된 헤더 사용 안 함"으로 설정한다.
*/

#pragma once

#include "sqlite/sqlite3.h"
#include "DB.h"
#include "Convert.h"

/**
 @class     CSQLiteConnection 
 @brief     SQLite Database와의 연결을 제공하는 Class

            SQLite의 Connection Object는 Single Transction과 Single Connection을 제공한다. \n

 @author    hang ryul lee
 @date      create 2011.05.12
 @note      SQLite는 연결할 database이름에 ":memory:" 또는 공백문자열("")을 사용하여in-memory database를 생성한다.
 @note      in-memory database는 그것을 생성한 connection에서만 접근이 가능하다. 다른 connection들과는 공유할수 없다.
 @note      in-memory database는 해당 connection이 유지되는 동안만 유효하고 connection이 close되면 memory에서 제거된다.
*/
class CSQLiteConnection : public CDbConnection
{
    friend class CSQLiteCommand;    
public:
    CSQLiteConnection();
    virtual ~CSQLiteConnection();

    virtual long BeginTrans();
    virtual void Close();
    virtual long CommitTrans();
    virtual bool Execute(const CString &_sCommandText);
    virtual bool Open();
	virtual bool Open(CString &_sConnectionStr);
    virtual long RollbackTrans();

    virtual long GetCommandTimeout() const;
    virtual void SetCommandTimeout(long _lCommandTimeout);
    virtual bool IsConnected() const;
    virtual CString GetConnectionString() const;
    virtual void SetConnectionString(const CString &_sConnectionStr);
    virtual long GetConnectionTimeout() const;
    virtual void SetConnectionTimeout(long _lConnectionTimeout);

    virtual DWORD GetError() const;
    virtual CString GetErrorMessage() const;
private:
    sqlite3 *m_pConnection;         /**< SQLite Database Connection Handle */
    long m_lCommandTimeout;         /**< SQL Command 실행 Timeout값(초) */
    bool m_bConnected;              /**< Database 접속 유무 */
    CString m_sConnectionStr;       /**< Database 접속을 위한 문자열 정보 */
    long m_lConnectionTimeout;      /**< Database 접속 Timeout값(초) */
    long m_lNestLevel;              /**< Transaction 중첩 Level값 */
    DWORD m_dwError;                /**< 최근 오류 코드 */

    sqlite3_stmt* Prepare(const CString &_sSql);
    static int OnBusyHandler(void *_pConnection, int _nCount);
};


/**
 @class     CSQLiteDataSet 
 @brief     SQL 명령의 실행으로 생성된 레코드의 집합을 Class화함.

            한번에 한 레코드씩 읽을수 있다.

 @author    hang ryul lee
 @date      create 2011.05.12
*/
class CSQLiteDataSet : public CDbDataSet
{
    friend class CSQLiteCommand;
public:
    CSQLiteDataSet();
    virtual ~CSQLiteDataSet();

    virtual void Next();

    virtual int GetIntField(const CString &_sFieldName, int _nDefault = 0);
    virtual double GetDoubleField(const CString &_sFieldName, double _dDefault = 0.0); 
    virtual CString GetStringField(const CString &_sFieldName, CString _sDefault = _T(""));
    virtual COleDateTime GetDateTimeField(const CString &_sFieldName, COleDateTime _dtDefault = COleDateTime());

    virtual bool IsBof();
    virtual bool IsEof();
    virtual bool IsEmpty();
    virtual bool IsFieldNull(const CString &_sFieldName);

    virtual DWORD GetError() const;
    virtual CString GetErrorMessage() const;
private:
    sqlite3_stmt *m_pStmt;      /**< SQLite Statement Object */
    bool m_bBof;                /**< BOF 구분값 */
    bool m_bEof;                /**< EOF 구분값 */
    DWORD m_dwError;            /**< 최근 오류 코드 */

    void Close();
    int GetFieldIndex(const CString &_sFieldName);
    int GetFieldDataType(int _nColumn);
};


/**
 @class     CSQLiteCommand 
 @brief     SQL문을 실행하는 Class

            CSQLConnection을 사용하여 SQL문을 실행하고 결과 레코드 집합인 CSQLiteDataSet을 생성한다.

 @author    hang ryul lee
 @date      create 2011.05.12
 @note      매개변수를 가지는 SQL문을 처리할 수 있다.
 @note      SQL Query를 재사용하거나 매개변수 처리를 해야 할 경우 효율적인다.
 @note      결과 레코드 집합이 필요하지 않을 경우 생성자의 CSQLiteDataSet파라미터에 NULL을 설정하면 된다.
*/
class CSQLiteCommand : public CDbCommand
{
public:
    CSQLiteCommand(CSQLiteConnection *_pConnection, CSQLiteDataSet *_pDataSet = NULL);
    virtual ~CSQLiteCommand();

    virtual bool Execute(int &_nRecordsAffected);

    virtual bool AddParameter(const CString &_sName, int _nValue, int _nDirection = 1);
	virtual bool AddParameter(const CString &_sName, double _dValue, int _nDirection = 1, BYTE _nPrecision = 0, BYTE _nScale = 0);
    virtual bool AddParameter(const CString &_sName, CString _sValue, int _nDirection = 1, long _lSize = 256);
    virtual bool AddParameter(const CString &_sName, COleDateTime _dtValue, int _nDirection = 1);
	bool ResetParameters();

    virtual long GetCommandTimeout() const;
    virtual void SetCommandTimeout(long _lCommandTimeout);
    virtual CString GetCommandText() const;
    virtual void SetCommandText(const CString &_rCommandText);  

    virtual DWORD GetError() const;
    virtual CString GetErrorMessage() const;
private:
    CSQLiteConnection *m_pSQLiteConnection; /**< Database접속을 관리하는 CSQLiteConnection Class 포인터 */
    CSQLiteDataSet *m_pDataSet;             /**< 결과집합을 관리하는 CSQLiteDataSet Class 포인터 */
    CString m_sCommandText;                 /**< 실행할 SQL문 */
    bool m_bInternalCreated;                /**< 내부에서 DataSet Class 생성했는지 판단 */
    int m_nPrepared;                        /**< prepare result code를 저장 */
    DWORD m_dwError;                        /**< 최근 오류 코드 */
};

