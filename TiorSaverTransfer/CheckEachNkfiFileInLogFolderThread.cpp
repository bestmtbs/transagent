/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.11.03	
********************************************************************************/
/**
@file       CLogSendThread.cpp
@brief     LogSendThread  구현 파일
@author	 jhlee
@date      create 2011.11.03
*/
// LogSendThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CheckEachNkfiFileInLogFolderThread.h"
#include "TiorSaverTransfer.h"
#include "Impersonator.h"
#include "UtilParse.h"
#include "UtilsFile.h"
#include "ParamParser.h"
#include "CmsDBManager.h"
#include "PathInfo.h"
#include "Crypto/Base64.h"

#include <yvals.h>

// CLogSendThread

IMPLEMENT_DYNCREATE(CCheckEachNkfiFileInLogFolderThread, CWinThread)


#pragma warning(disable:4706)
/********************************************************************************
@class     CLogSendThread
@brief     로그 전송 
*********************************************************************************/


CCheckEachNkfiFileInLogFolderThread::CCheckEachNkfiFileInLogFolderThread()
{
	m_bCheckEachNkfiFileInLogFolderThread  = FALSE;
	//m_nScanSleep = 5;
	//m_bSetServerIP = FALSE;
	//m_dwFileSize = 0;
	m_bSendThumbThreadStart = FALSE;
	//m_bIsNewJPG = FALSE;
}

CCheckEachNkfiFileInLogFolderThread::~CCheckEachNkfiFileInLogFolderThread()
{
	m_bCheckEachNkfiFileInLogFolderThread = FALSE;
}

BOOL CCheckEachNkfiFileInLogFolderThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	CString strQuery = _T("");
	strQuery.Format(_T("update %s set thread_idx = 0 where thread_idx != 0"), DB_SEND_NKFI_LIST);
#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbNkfi) {
		theApp.m_pDbNkfi->UpdateQuery(strQuery);
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManage(DB_SEND_NKFI_LIST);
	dbManage.UpdateQuery(strQuery);
	dbManage.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	return TRUE;
}

int CCheckEachNkfiFileInLogFolderThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCheckEachNkfiFileInLogFolderThread, CWinThread)
END_MESSAGE_MAP()


int CCheckEachNkfiFileInLogFolderThread::Run()
{
	//UM_WRITE_LOG(_T("CLogSendThread::Run()"));
	m_bCheckEachNkfiFileInLogFolderThread = TRUE;
	CStringArray arrList;
	CString strCmd = _T("");
	CString strQuery = _T("");
	CString strLog = _T("");
	arrList.RemoveAll();
	//CCmsDBManager dbManage;
	//dbManage.SelectAllQuery(_T("select * from DB_SEND_NKFI_LIST;"), arrList);
	m_nMaxIdx = arrList.GetSize();
	m_nMaxIdx = 0;	// TODO: 스레드 관리 죽었을때 디비에 있는건 어찌해야해..
	//arrTemp.RemoveAll();
	theApp.m_arrNKFISendingThreads.RemoveAll();
	strCmd.Format(_T("%s"), CPathInfo::GetLogFilePath());
	UM_WRITE_LOG(strQuery);
	
	while (TRUE)
	{
		//m_bIsNewJPG = FALSE;
		strLog.Format(_T("[CCheckEachNkfiFileInLogFolderThread] loop per %d seconds"), theApp.m_nScanDelay);
		UM_WRITE_LOG(strLog);
		CheckOffset();
		Sleep(theApp.m_nScanDelay * 1000);
		if (!theApp.m_bIsCollecting) {
			TerminateAllLogSizeThreads();
			break;
		}
	}
	m_bCheckEachNkfiFileInLogFolderThread = FALSE;
	return 0;
}

void CCheckEachNkfiFileInLogFolderThread::CheckOffset()
{
	CString strTargetLog = _T("");
	CString strLog = _T("");
	CString strQuery = _T("");
	CStringArray arrFiles;
	//CUIntArray arrSizeOfFiles;
	INT nCurrentOffset;
	LONGLONG nFileLength;
	arrFiles.RemoveAll();
	//arrSizeOfFiles.RemoveAll();
	//CTime ctDateBefore5Days = CTime::GetCurrentTime();
	//CString strDateBefore5Days = _T("");
	//ctDateBefore5Days += CTimeSpan(-1, 0, 0, 0);
	//strDateBefore5Days = ctDateBefore5Days.Format(_T("%Y-%m-%d %H:%M:%S"));
	//strQuery.Format(_T("select * from %s where last_write_date > \"%s\""), DB_SEND_NKFI_LIST, strDateBefore5Days);

	strQuery.Format(_T("select full_path, file_size, current_offset from %s where file_size != current_offset and \
		datetime(strftime('%%s', last_scan_time)+ each_scan_delay, 'unixepoch') < datetime('now','localtime') or last_scan_time is null ORDER BY full_path ASC;"), DB_SEND_NKFI_LIST);

#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbNkfi) {
		theApp.m_pDbNkfi->SelectQuery(strQuery, &arrFiles, _T("full_path"), _T("file_size"), _T("current_offset"));
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManage(DB_SEND_NKFI_LIST);
	dbManage.SelectQuery(strQuery, &arrFiles, _T("full_path"), _T("file_size"), _T("current_offset"));
	dbManage.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	strLog.Format(_T("%s result size is %d [line: %d, function: %s, file: %s]"), strQuery, arrFiles.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);
	for (int i = 0; i < arrFiles.GetSize(); i++)
	{
		if (nullptr != theApp.m_pMemberData) {
			INT nCurCpuUsage = theApp.m_pMemberData->GetCpuUsage();
			if (MAX_CPU_USAGE <  nCurCpuUsage && MIN_SEND_LIMIT < theApp.m_nMaxSendLimit) {
				theApp.m_nMaxSendLimit--;
			} else if (MAX_CPU_USAGE > nCurCpuUsage && MAX_SEND_LIMIT >theApp.m_nMaxSendLimit) {
				theApp.m_nMaxSendLimit++;
			}
		}
		if (theApp.m_nMaxSendLimit <= theApp.m_nNkfiArrayMaxSizeFull && theApp.m_nMaxSendLimit < theApp.m_arrNKFISendingThreads.GetSize()) {
			DBGLOG(_T("[CheckOffset] ScrArrayMaxSizeFull. MaxLimit: %d, cursize: %d [line: %d, fuction: %s, file: %s]"), theApp.m_nMaxSendLimit, theApp.m_nNkfiArrayMaxSizeFull, __LINE__, __FUNCTIONW__, __FILEW__);
			continue;
		}
		CString strData = arrFiles.GetAt(i);
		CStringArray arrData;
		CString strCurrentFile = _T("");
		arrData.RemoveAll();
		CParseUtil::ParseStringToStringArray(_T("|"), strData, &arrData);
		strCurrentFile = arrData.GetAt(0);
		//TODO: DB에 넣어야 할 거..modified date가 5일 이내에...지금은 다 넣자
		if (TRUE == FileExists(strCurrentFile) ) {
			DBGLOG(_T("[CheckOffset] %s is exist. [line: %d, fuction: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
			if (0 == ExtractFileExt(strCurrentFile).CompareNoCase(COLLECT_FILE_EXTENSION_NKFI)) {
				strTargetLog = strCurrentFile;
				nFileLength = _ttoi(arrData.GetAt(1));
				int nOffsetJustBefore = _ttoi(arrData.GetAt(2));
				int nEachFileDelay = 0;
				if (nOffsetJustBefore < nFileLength) {
					CStringArray arrSelect;
					strQuery = _T("");
					strQuery.Format(_T("select thread_idx from %s where full_path like '%%%s%%' and thread_idx != 0"), DB_SEND_NKFI_LIST, strCurrentFile);
					if (theApp.m_pDbNkfi) {
						theApp.m_pDbNkfi->SelectQuery(strQuery, &arrSelect, _T("thread_idx"));
					}
					if (0 < arrSelect.GetSize()) {
						if (theApp.m_arrNKFISendingThreads.GetAt(_ttoi(arrSelect.GetAt(0)) - 1))
							if (theApp.m_arrNKFISendingThreads.GetAt(_ttoi(arrSelect.GetAt(0)) - 1)->m_bCheckLogSizeThreadSending)
								continue;
					}
					if (theApp.GetOffsetFromServer(strTargetLog, nCurrentOffset, nEachFileDelay)) {
						if (-2 == nCurrentOffset) {
							DBGLOG(_T("[CCheckEachNcviFileInLogFolderThread] [%s][%d][%lld]"), strCurrentFile, nCurrentOffset, nFileLength);
							continue;
						}
						strLog.Format(_T("[CCheckEachNkfiFileInLogFolderThread] [%s][%d][%lld]"), strCurrentFile, nCurrentOffset, nFileLength);
						UM_WRITE_LOG(strLog);
						if (nCurrentOffset) {	// 2017-10-12 sy.choi 왜 여기에 있지?
							strQuery = _T("");
							strQuery.Format(_T("update %s set current_offset = %d, last_scan_time = datetime('now','localtime'), each_scan_delay = %d where full_path like '%%%s%%'"), DB_SEND_NKFI_LIST, nCurrentOffset, nEachFileDelay, strCurrentFile);
							strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
							UM_WRITE_LOG(strLog);
#ifdef _MGCHOI_TEST_
							if (theApp.m_pDbNkfi) {
								theApp.m_pDbNkfi->UpdateQuery(strQuery);
							}
#else	//#ifdef _MGCHOI_TEST_
							CCmsDBManager dbManageOffsetUpdate(DB_SEND_NKFI_LIST);
							dbManageOffsetUpdate.UpdateQuery(strQuery);
							dbManageOffsetUpdate.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
						}
						if (nCurrentOffset < nFileLength) {
							CString strThreadHandle = _T("");
							CStringArray arrSelected;
							CString strQuery = _T("");
							strQuery.Format(_T("select thread_idx from %s where full_path like '%%%s%%' and thread_idx > 0 and thread_idx is not null;"), DB_SEND_NKFI_LIST, strCurrentFile);
							////UM_WRITE_LOG(strQuery);
							arrSelected.RemoveAll();
#ifdef _MGCHOI_TEST_
							if (theApp.m_pDbNkfi) {
								theApp.m_pDbNkfi->SelectQuery(strQuery, &arrSelected, _T("thread_idx"));
							}
#else	//#ifdef _MGCHOI_TEST_
							CCmsDBManager dbManage2(DB_SEND_NKFI_LIST);
							dbManage2.SelectQuery(strQuery, &arrSelected, _T("thread_idx"));
							dbManage2.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
							BOOL bFoundExistedThreadIdx = FALSE;
							if ((0 < arrSelected.GetSize())) {
								DBGLOG(_T("[CCheckLogSizeThread] %s thread_idx != 0 size: %d... [line: %d, function: %s, file: %s]"), strCurrentFile, arrSelected.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
								if (0 < theApp.m_arrNKFISendingThreads.GetSize()) {
									int nThreadIdx = _ttoi(arrSelected.GetAt(0));
									if (theApp.m_arrNKFISendingThreads.GetSize() >= nThreadIdx) {	// 2017-09-14 sy.choi getat 했을 때 혹시 죽지 않도록.
										if (theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)) {
											if (theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->m_bCheckLogSizeThreadStart) {
												DBGLOG(_T("[CCheckLogSizeThread] resend %s [line: %d, function: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
												bFoundExistedThreadIdx = TRUE;
												// 2017-09-12 sy.choi 서버에서 온 offset이 -1일 경우 스레드 종료하도록 수정
												if (-1 == nCurrentOffset) {
													DBGLOG(_T("[CCheckLogSizeThread] %s offset from server problem. reset request server required [line: %d, function: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
													BOOL bQueryResult = FALSE;
													strQuery = _T("");
													strQuery.Format(_T("update %s set thread_idx = %d where full_path like '%%%s%%'"), DB_SEND_NKFI_LIST, 0, strCurrentFile);
													UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
													if (theApp.m_pDbNkfi) {
														theApp.m_pDbNkfi->UpdateQuery(strQuery);
													}
#else	//#ifdef _MGCHOI_TEST_
													CCmsDBManager dbManage3(DB_SEND_NKFI_LIST);
													bQueryResult = dbManage3.UpdateQuery(strQuery);
													dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
													UM_WRITE_LOG(strQuery);
													if (bQueryResult) {
														theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->SetStopEvent();
														theApp.m_arrNKFISendingThreads.RemoveAt(nThreadIdx-1);
														for (int j = nThreadIdx; j < theApp.m_nMaxSendLimit; j++)
														{
															strQuery = _T("");
															strLog = _T("");
															strQuery.Format(_T("update %s set thread_idx = %d where thread_idx = %d"), DB_SEND_NKFI_LIST, j, j+1);
															int nCount = 0;
															while (1)
															{
																BOOL bQueryUpdateResult = FALSE;
																if (QUERY_TRY_MAX_LIMIT < nCount) {
																	strLog.Format(_T("%s fail times. so massed up"), strQuery);
																	UM_WRITE_LOG(strLog);
																	break;
																}														
																strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
																UM_WRITE_LOG(strLog);

#ifdef _MGCHOI_TEST_
																if (theApp.m_pDbNkfi) {
																	bQueryUpdateResult = theApp.m_pDbNkfi->UpdateQuery(strQuery);
																}
#else	//#ifdef _MGCHOI_TEST_
																CCmsDBManager dbManage3(DB_SEND_NKFI_LIST);
																bQueryUpdateResult = dbManage3.UpdateQuery(strQuery);
																dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
																if (bQueryUpdateResult) {
																	break;
																} else {
																	strLog.Format(_T("%s fail.."), strQuery);
																	UM_WRITE_LOG(strLog);
																	nCount++;
																}
															}
															UM_WRITE_LOG(strQuery);
														}
													}
												} else if (FALSE == theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->m_bCheckLogSizeThreadSending || FALSE == theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->m_bCheckLogSizeThreadStart) {
													theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->SetCurrentOffset(nCurrentOffset);
													theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->SetFileSize((UINT)nFileLength);
													theApp.m_arrNKFISendingThreads.GetAt(nThreadIdx-1)->SetResendEvent();
													continue;
												}
											}
										}
									}
									if (!bFoundExistedThreadIdx) {	// 2017-09-14 send_list에서 select하면 있는데 스레드는 없을 경우 
										// 2017-09-22 sy.choi fatal error
										DBGLOG(_T("[CCheckLogSizeThread] %s send_list exist but no thread [line: %d, function: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
									}
								}
							} 
							if ((FALSE == bFoundExistedThreadIdx) && (nCurrentOffset != -1)) {	// 2017-09-14 sy.choi select해서 없을 때 && select해서 존재하는데 array에 스레드가 없어서 delete했을 때 db에 update하고 array에 추가
								int nArrSize = theApp.m_arrNKFISendingThreads.GetSize();
								if (nArrSize > theApp.m_nMaxSendLimit) {	// 2017-09-12 sy.choi 최대 크기 이상이면 stop event를 보낼 것임.
									BOOL bResult = FALSE;
									strLog.Format(_T("[CheckOffset] %d [line: %d, fuction: %s, file: %s]"), theApp.m_arrNKFISendingThreads.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
									UM_WRITE_LOG(strLog);
									for (int i = 0; i < nArrSize; i++)
									{
										/*if (theApp.m_arrNKFISendingThreads.GetAt(i)->m_hStopEvent) */{	// 2017-09-12 sy.choi stop event 핸들 확인
											if (FALSE == theApp.m_arrNKFISendingThreads.GetAt(i)->m_bCheckLogSizeThreadStart || FALSE == theApp.m_arrNKFISendingThreads.GetAt(i)->m_bCheckLogSizeThreadSending) {
												BOOL bQueryResult = FALSE;
												strQuery = _T("");
												strQuery.Format(_T("update %s set thread_idx = %d where thread_idx = %d"), DB_SEND_NKFI_LIST, 0, i+1);
#ifdef _MGCHOI_TEST_
												if (theApp.m_pDbNkfi) {
													bQueryResult = theApp.m_pDbNkfi->UpdateQuery(strQuery);
												}
#else	//#ifdef _MGCHOI_TEST_
												CCmsDBManager dbManage3(DB_SEND_NKFI_LIST);
												bQueryResult = dbManage3.UpdateQuery(strQuery);
												dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
												if (bQueryResult) {
													theApp.m_arrNKFISendingThreads.GetAt(i)->SetStopEvent();
													theApp.m_arrNKFISendingThreads.RemoveAt(i);
													for (int j = i+1; j <= theApp.m_nMaxSendLimit; j++)
													{
														BOOL bQueryUpdateResult = FALSE;
														int nCount = 0;
														strQuery = _T("");
														strQuery.Format(_T("update %s set thread_idx = %d where thread_idx = %d"), DB_SEND_NKFI_LIST, j, j+1);
														while (1)
														{
															strLog = _T("");
															if (QUERY_TRY_MAX_LIMIT < nCount) {
																strLog.Format(_T("%s fail times. so massed up"), strQuery);
																UM_WRITE_LOG(strLog);
																break;
															}
															strLog = _T("");
															strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
															UM_WRITE_LOG(strLog);
#ifdef _MGCHOI_TEST_
															if (theApp.m_pDbNkfi) {
																bQueryUpdateResult = theApp.m_pDbNkfi->UpdateQuery(strQuery);
															}
#else	//#ifdef _MGCHOI_TEST_
															CCmsDBManager dbManage3(DB_SEND_NKFI_LIST);
															bQueryUpdateResult = dbManage3.UpdateQuery(strQuery);
															dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
															if (bQueryUpdateResult) {
																break;
															} else {
																strLog.Format(_T("%s fail.."), strQuery);
																UM_WRITE_LOG(strLog);
																nCount++;
															}
														}
													}
												}
												//arrTemp.RemoveAt(i);
												bResult = TRUE;
												break;
											}
										}
									}
									if (bResult) {
										BOOL bQueryResult = FALSE;
										strQuery = _T("");
										strQuery.Format(_T("update %s set thread_idx = %d where full_path like '%%%s%%'"), DB_SEND_NKFI_LIST, theApp.m_arrNKFISendingThreads.GetSize()+1, strTargetLog);
										UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
										if (theApp.m_pDbNkfi) {
											bQueryResult = theApp.m_pDbNkfi->UpdateQuery(strQuery);
										}
#else	//#ifdef _MGCHOI_TEST_
										CCmsDBManager dbManage4(DB_SEND_NKFI_LIST);
										bQueryResult = dbManage4.UpdateQuery(strQuery);
										dbManage4.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_

										if (bQueryResult) {
											CCheckLogSizeThread* pSendThread = (CCheckLogSizeThread*)AfxBeginThread(RUNTIME_CLASS(CCheckLogSizeThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
											if (pSendThread) {
												strLog = _T("");
												strLog.Format(_T("[CheckOffset] %s SetResendEvent [line: %d, fuction: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
												UM_WRITE_LOG(strLog);
												pSendThread->SetTargetFile(strTargetLog);
												pSendThread->SetCurrentOffset(nCurrentOffset);
												//pSendThread->SetDelay(nDelay);
												pSendThread->SetFileSize((UINT)nFileLength);
												pSendThread->ResumeThread();
												pSendThread->SetResendEvent();
												theApp.m_arrNKFISendingThreads.Add(pSendThread);
											}
										}
									}
								} else {
									strLog.Format(_T("[CheckOffset] %d [line: %d, fuction: %s, file: %s]"), theApp.m_arrNKFISendingThreads.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
									BOOL bQueryResult = FALSE;
									strQuery = _T("");
									strQuery.Format(_T("update %s set thread_idx = %d where full_path like '%%%s%%'"), DB_SEND_NKFI_LIST, theApp.m_arrNKFISendingThreads.GetSize()+1, strTargetLog);
#ifdef _MGCHOI_TEST_
									if (theApp.m_pDbNkfi) {
										bQueryResult = theApp.m_pDbNkfi->UpdateQuery(strQuery);
									}
#else	//#ifdef _MGCHOI_TEST_
									CCmsDBManager dbManage4(DB_SEND_NKFI_LIST);
									bQueryResult = dbManage4.UpdateQuery(strQuery);
									dbManage4.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
									UM_WRITE_LOG(strQuery);
									if (bQueryResult) {
										CCheckLogSizeThread* pSendThread = (CCheckLogSizeThread*)AfxBeginThread(RUNTIME_CLASS(CCheckLogSizeThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
										if (pSendThread) {
											strLog = _T("");
											strLog.Format(_T("[CheckOffset] %s SetResendEvent [line: %d, fuction: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
											UM_WRITE_LOG(strLog);
											pSendThread->SetTargetFile(strTargetLog);
											pSendThread->SetCurrentOffset(nCurrentOffset);
											//pSendThread->SetDelay(nDelay);
											pSendThread->SetFileSize((UINT)nFileLength);
											pSendThread->ResumeThread();
											pSendThread->SetResendEvent();
											theApp.m_arrNKFISendingThreads.Add(pSendThread);
										}
									}
								}
							}
						} else if (nCurrentOffset > GetFileSize(strCurrentFile)) {
							theApp.PostResetOffsetToServer(strCurrentFile);
							//DBGLOG(_T("[PostResetOffsetToServer] %s nOffsetJustBefore: %d %lld [line: %d, fuction: %s, file: %s]"), strCurrentFile, nCurrentOffset, nFileLength, __LINE__, __FUNCTIONW__, __FILEW__);
						}
					}
				} else if (nOffsetJustBefore > GetFileSize(strCurrentFile)) {
					theApp.PostResetOffsetToServer(strCurrentFile);
					//DBGLOG(_T("[PostResetOffsetToServer] %s nOffsetJustBefore: %d %lld [line: %d, fuction: %s, file: %s]"), strCurrentFile, nOffsetJustBefore, nFileLength, __LINE__, __FUNCTIONW__, __FILEW__);
				}
			}
		} else {
			strLog = _T("");
			strLog.Format(_T("[CheckOffset] %s is not exist. [line: %d, fuction: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
			CString strQuery = _T("");
			strQuery.Format(_T("delete from %s where full_path like '%%%s%%'"), DB_SEND_NKFI_LIST, strCurrentFile);
#ifdef _MGCHOI_TEST_
			if (theApp.m_pDbNkfi) {
				theApp.m_pDbNkfi->DeleteQuery(strQuery);
			}
#else	//#ifdef _MGCHOI_TEST_
			CCmsDBManager dbManage4(DB_SEND_NKFI_LIST);
			dbManage4.DeleteQuery(strQuery);
			dbManage4.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
		}
	}
}

BOOL CCheckEachNkfiFileInLogFolderThread::TerminateAllLogSizeThreads()
{
	CString strLog = _T("");
	strLog.Format(_T("TerminateAllLogSizeThreads start...[line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);
	BOOL bResult = FALSE;
	CString strQuery = _T("");
	strQuery.Format(_T("update %s set thread_idx = %d where thread_idx != %d or thread_idx is not null"), DB_SEND_NKFI_LIST, 0, 0);
#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbNkfi) {
		theApp.m_pDbNkfi->UpdateQuery(strQuery);
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManage4(DB_SEND_NKFI_LIST);
	dbManage.UpdateQuery(strQuery);
	dbManage.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	UM_WRITE_LOG(strQuery);
	for (int i = 0; i < theApp.m_arrNKFISendingThreads.GetSize(); i++)
	{
		if (theApp.m_arrNKFISendingThreads.GetAt(i)->m_hStopEvent) {
			if (FALSE == theApp.m_arrNKFISendingThreads.GetAt(i)->m_bCheckLogSizeThreadStart) {
				theApp.m_arrNKFISendingThreads.GetAt(i)->SetStopEvent();
				theApp.m_arrNKFISendingThreads.RemoveAt(i);
			}
		}

	}
	bResult = TRUE;
	return bResult;
}