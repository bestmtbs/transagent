#ifndef _SEND_AGENT__COMMON_DEFINE_H
#define _SEND_AGENT__COMMON_DEFINE_H

//#define CPathInfo::GetClientInstallPath()	_T("C:\\DSNTECH\\WinTior\\Log\\")
//Process Name
#define	WTIOR_SYS_AGENT_32_NAME			_T("TiorSaverSysAgent_x86.exe")
#define	WTIOR_USR_AGENT_32_NAME			_T("TiorSaverUsrAgent_x86.exe")
#define	WTIOR_SYS_AGENT_XP_32_NAME		_T("TiorSaverSysAgent_xp_x86.exe")
#define	WTIOR_USR_AGENT_XP_32_NAME		_T("TiorSaverUsrAgent_xp_x86.exe")
#define	WTIOR_SYS_AGENT_64_NAME			_T("TiorSaverSysAgent_x64.exe")
#define	WTIOR_USR_AGENT_64_NAME			_T("TiorSaverUsrAgent_x64.exe ")
#define	WTIOR_SYS_AGENT_XP_64_NAME		_T("TiorSaverSysAgent_xp_x64.exe")
#define	WTIOR_USR_AGENT_XP_64_NAME		_T("TiorSaverUsrAgent_xp_x64.exe")
#define	WTIOR_TRANS_AGENT_NAME			_T("TiorSaverTransfer.exe")
#define	WTIOR_AGENT_SERVICE_NAME		_T("TiorSaverService.exe")
#define WTIOR_AGENT_DUMMY_NAME			_T("TiorSaverDummy")
#define	WTIOR_UPDATE_AGENT_NAME			_T("TiorSaverUpdate.exe")
#define	WTIOR_DOWNLOAD_AGENT_NAME		_T("TiorSaverDownload.exe")
#define	WTIOR_UNINSTALL_AGENT_NAME		_T("TiorSaverUninstall.exe")
#define	WTIOR_TRAY_NAME					_T("TiorSaverTray.exe")
#define	WTIOR_INSTALL_AGENT_NAME		_T("TiorSaverSetup.exe")
#define EXPLORER_PROC_NAME				_T("explorer.exe")

#define	COLLECT_FILE_EXTENSION_NCVI			_T(".ncvi")
#define	COLLECT_FILE_EXTENSION_NKFI			_T(".nkfi")
#define	COLLECT_FILE_EXTENSION_LOG			_T(".log")

//Mutex Name
#define	WTIOR_SYS_AGENT_32_MUTEX_NAME		_T("Global\\BBSAVER_CSA_x86")
#define	WTIOR_USR_AGENT_32_MUTEX_NAME		_T("Global\\BBSAVER_CUA_x86")
#define	WTIOR_SYS_AGENT_64_MUTEX_NAME		_T("Global\\BBSAVER_CSA_x64")
#define	WTIOR_USR_AGENT_64_MUTEX_NAME		_T("Global\\BBSAVER_CUA_x64")
#define	WTIOR_TRANS_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_TA")
#define	WTIOR_TRAY_MUTEX_NAME				_T("Global\\BBSAVER_TRAY")
#define	WTIOR_UPDATE_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_UPA")
#define	WTIOR_DOWNLOAD_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_DWNA")
#define	WTIOR_UNINSTALL_AGENT_MUTEX_NAME	_T("Global\\BBSAVER_UINSA")
#define	WTIOR_INSTALL_AGENT_MUTEX_NAME		_T("Global\\BBSAVER_INSA")

#define WTIOR_SETTING_DATABASE	_T("test.db")
#define WTIOR_NCVI_DATABASE		_T("ncvi.db")
#define WTIOR_NKFI_DATABASE		_T("nkfi.db")
#define WTIOR_LOG_DATABASE		_T("log.db")

#define SERVERTIME_SIZE 22
#define CURL_REQUEST_TIMEOUT	30L
#define SEND_TYPE_CURL	0
#define SEND_TYPE_HTTPSTCONNECT 1
#define QUERY_TRY_MAX_LIMIT 5
#define	SLEEP_REQUEST_POLICY_IF_STOP_PER_SECONDS	600000	// 2017-09-27 sy.choi stop일 경우 10분에 한번씩 정책 요청

#define TS_SERVICE_NAME			_T("TiorSaver SERVICE")
#define STR_CMS_VER_SECTION		_T("TSVersion")
#define SEND_NKFI_MAX_SIZE 1048576
#define GIGABYTES_BYTES	1073741824
#define SEND_JPG_MAX_SIZE 1048576*2
#define SEND_LOG_MAX_SIZE 500000
#define MAX_SEND_LIMIT	3
#define MIN_SEND_LIMIT	2
#define MAX_CPU_USAGE	10

#ifndef SE_MemoryFree
#define SE_MemoryFree( _X) {		\
	if( _X != NULL)	{		\
	free( _X);			    \
	_X = NULL;			\
	}						        \
}
#endif
#ifndef SE_MemoryDelete
#define SE_MemoryDelete( _X) {		\
	if( _X != nullptr)	{		\
	delete  _X;			    \
	_X = nullptr;			\
	}						        \
}
#endif

#define AGENT_ACTION_UPDATE 1
#define AGENT_ACTION_FORCE_UPDATE 2
#define AGENT_ACTION_DELETE 3

#define AGENT_STATE_START 1
#define AGENT_STATE_STOP 0

#define DB_AGENT_INFO		_T("db_agent_info")
#define DB_PC_INFO			_T("db_pc_info")
#define DB_SEND_LOG_LIST		_T("db_send_log_list")
#define DB_SEND_NCVI_LIST		_T("db_send_ncvi_list")
#define DB_SEND_NKFI_LIST		_T("db_send_nkfi_list")
#define DB_COLLECT_POLICY	_T("db_collect_policy")
#define DB_NOTICE			_T("db_notice")
#define DB_STATE_POLICY		_T("db_state_policy")
#define DB_URL_POLICY		_T("db_url_policy")
#define DB_SERVER_INFO		_T("db_server_info")

#define KILL_ALL					0
#define	KILL_COLLECT_AGENT			1
#define	KILL_TRANS_AGENT_NAME		2
#define	KILL_UPDATE_AGENT_NAME		4
#define	KILL_DOWNLOAD_AGENT_NAME	8
#define	KILL_UNINSTALL_AGENT_NAME	16

typedef struct _TIME_SET
{
	CString strTimeFromServer;
	int nScanDelay;
	int nDeleteDelay;
	CTime ctScanDate;
	CTime ctDeleteDate;
	int nPolicyRetry;
	_TIME_SET() {
		strTimeFromServer = _T("");
		nScanDelay = 5;
		nDeleteDelay = -1;
		nPolicyRetry = 0;
	}
}TIME_SET;

typedef struct _UPDATE_SET
{
	CString strCurrVersion;
	CString strNewVersion;
	CStringArray arrPath;
	CStringArray arrVersion;
	CStringArray arrHash;
	CString strMessage;
	_UPDATE_SET() {
		strCurrVersion = _T("");
		strNewVersion = _T("");
		arrPath.RemoveAll();
		arrVersion.RemoveAll();
		arrHash.RemoveAll();
		strMessage = _T("");
	}
}UPDATE_SET;

typedef struct _POLICY_SET
{
	BOOL bIsPolicyExist;
	BOOL bStart;
	BOOL bDelete;
	BOOL bNotice;
	CStringArray arrCollect;
	CStringArray arrIgnore;
	CStringArray arrBrowser;
	int nAction;
	CStringArray arrDomain;
	//CStringArray arrNotice;
	TIME_SET* TimeSet;
	UPDATE_SET* UpdateSet;
	UINT nDiskFree;
	DWORD dwSplitSize;
	_POLICY_SET() {
		bIsPolicyExist = TRUE;
		bStart = AGENT_STATE_START;
		bDelete = FALSE;
		bNotice = FALSE;
		arrCollect.RemoveAll();
		arrIgnore.RemoveAll();
		arrBrowser.RemoveAll();
		nAction = 0;
		arrDomain.RemoveAll();
		//arrNotice.RemoveAll();
		TimeSet = new TIME_SET;
		UpdateSet = new UPDATE_SET;
		nDiskFree = 0;
		dwSplitSize = 30;
	}
}POLICY_SET;

typedef struct _NOTICE_SET
{
	INT nID;
	CString strTitle;
	CString strDescr;
	CString strType;
	BOOL bIsShow;
	CString strCorpName;
	CString strDeviceUUID;
	CString strCreateAt;
	CString strUpdateAt;
	_NOTICE_SET() {
		nID = 0;
		strTitle = _T("");
		strDescr = _T("");
		strType = _T("");
		bIsShow = TRUE;
		strCorpName = _T("");
		strDeviceUUID = _T("");
		strCreateAt = _T("");
		strUpdateAt = _T("");
	}
}NOTICE_SET;

typedef struct _LOG_DATA
{
	CString strPostData;
	_LOG_DATA() {
		strPostData = _T("");
	}

}LOG_DATA;

typedef struct _DEPARTMENT
{
	CString strDepartmentName;
	int nDepartmentID;
	_DEPARTMENT() {
		strDepartmentName = _T("");
		nDepartmentID = 0;
	}

}DEPARTMENT;

enum OFS_CHECK_ERR
{
	OFS_CHECK_SUCCESS = 0,
	OFS_CHECK_GET_FILE_HASH	 =1,
	OFS_CHECK_HASH_ELSE = 2,
	OFS_CHECK_DECODE_HASH = 3,
	OFS_CHECK_FILE_EXIST = 4,
	OFS_CHECK_PE_FILE = 5,
	OFS_CHECK_INI_FILE= 6,
	OFS_CHECK_TARGET_PATH = 7,
	OFS_CHECK_INI_SECTION = 8,
	OFS_CHECK_INI_VALUE = 9,
	OFS_CHECK_CODESIGN = 10,
	OFS_CHECK_INI_VALUE_HASH = 11,
	OFS_CHECK_FILE_INTEGRITY = 12,
	OFS_CHECK_INI_EXIST = 13,
	OFS_CHECK_INI_HASH_INFO = 14,
	OFS_CHECK_INI_DRV_SECTION = 15,
	OFS_CHECK_INI_HASH_KEY = 16,
	OFS_CHECK_INI_VALUE_HASH_KEY = 17,

};

#define STR_CODE_INTEGRITY_CHECK_POLICY		_T("200001")				
#define STR_CODE_INTEGRITY_CHECK_FILE		_T("200002")

#endif	//#ifndef _SEND_AGENT__COMMON_DEFINE_H

//TIMER
#define	TIMER_CREATE_TRAY_ICON			4
#define TIMER_SEND_TO_EXIT_CMD			10
#define TIMER_EXIT_AGENT_PROCESS		11
#define TIMER_ALL_THREAD_EXIT_CHECK		12
#define TIMER_START_UNINSTALL_PROC_UNTIL_SUCCESS 13
#define TIMER_POP_UP_PROCESS			14
#define TIMER_PAUSE_COMMUNICATE			15
//#define TIMER_RESUME_COMMUNICATE		16
#define EXIT_COLLECT_AGENT_RETRY_TIME	1000 * 1				//Transfer 종료시 모든 쓰레드가 정상 종료되었는지 체클할 타이머 시간
#define CHECK_ALL_THREAD_END			1000 * 6				//Transfer 종료시 모든 쓰레드들이 정상적으로 빠져나왔는지 체크   
#define EXIT_AGENT_RETRY_TIME			1000 * 3			//Transfer 종료시 모든 쓰레드가 정상 종료되었는지 체클할 타이머 시간
#define POP_UP_PROCESS_RETRY_TIME		1000 * 1

//MSG
#define MSG_UPDATE_COMPLETE_RESTART		WM_USER + 100
#define MSG_DELETE_START				WM_USER + 101
#define MSG_POP_UP_START				WM_USER + 102
#define MSG_PAUSE_COMMUNICATE			WM_USER + 103

//#define SERVER_HOST_URL					_T("http://192.168.100.112")