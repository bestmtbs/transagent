#pragma once

#include "CpuUsage.h"

class CCpuUsageThread : public CWinThread
{
	DECLARE_DYNCREATE(CCpuUsageThread)

public:
	CCpuUsageThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CCpuUsageThread();

	BOOL m_bCheckPolicy;
	BOOL m_bCpuUsageThread;
	INT m_nOsProduct;
	
private:
	CCpuUsage m_CpuUsage;
	INT	m_nCurCpuUsage;
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	INT GetCpuUsage();

protected:
	DECLARE_MESSAGE_MAP()
};


