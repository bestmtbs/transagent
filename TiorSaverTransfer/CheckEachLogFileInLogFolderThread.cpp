/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.11.03	
********************************************************************************/
/**
@file       CLogSendThread.cpp
@brief     LogSendThread  구현 파일
@author	 jhlee
@date      create 2011.11.03
*/
// LogSendThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CheckEachLogFileInLogFolderThread.h"
#include "TiorSaverTransfer.h"
#include "Impersonator.h"
#include "UtilParse.h"
#include "UtilsFile.h"
#include "ParamParser.h"
#include "CmsDBManager.h"
#include "PathInfo.h"
#include "Crypto/Base64.h"

#include <yvals.h>

// CLogSendThread

IMPLEMENT_DYNCREATE(CCheckEachLogFileInLogFolderThread, CWinThread)


#pragma warning(disable:4706)
/********************************************************************************
@class     CLogSendThread
@brief     로그 전송 
*********************************************************************************/


CCheckEachLogFileInLogFolderThread::CCheckEachLogFileInLogFolderThread()
{
	m_bCheckEachLogFileInLogFolderThread  = FALSE;
	//m_nScanSleep = 5;
	//m_bSetServerIP = FALSE;
	//m_dwFileSize = 0;
	m_bSendThumbThreadStart = FALSE;
	//m_bIsNewJPG = FALSE;
	m_strMaxIdx = _T("");
}

CCheckEachLogFileInLogFolderThread::~CCheckEachLogFileInLogFolderThread()
{
	m_bCheckEachLogFileInLogFolderThread = FALSE;
}

BOOL CCheckEachLogFileInLogFolderThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
		CString strQuery = _T("");
		strQuery.Format(_T("update %s set thread_idx = 0 where thread_idx != 0"), DB_SEND_LOG_LIST);
#ifdef _MGCHOI_TEST_
		if (theApp.m_pDbLog) {
			theApp.m_pDbLog->UpdateQuery(strQuery);
		}
#else	//#ifdef _MGCHOI_TEST_
		CCmsDBManager dbManage3(DB_SEND_LOG_LIST);
		dbManage3.UpdateQuery(strQuery);
		dbManage3.Free();
#endif
	return TRUE;
}

int CCheckEachLogFileInLogFolderThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCheckEachLogFileInLogFolderThread, CWinThread)
END_MESSAGE_MAP()


int CCheckEachLogFileInLogFolderThread::Run()
{
	//UM_WRITE_LOG(_T("CLogSendThread::Run()"));
	m_bCheckEachLogFileInLogFolderThread = TRUE;
	CStringArray arrList;
	CString strCmd = _T("");
	CString strQuery = _T("");
	CString strLog = _T("");
	arrList.RemoveAll();
	//CCmsDBManager dbManage;
	//dbManage.SelectAllQuery(_T("select * from DB_SEND_LOG_LIST;"), arrList);
	m_nMaxIdx = arrList.GetSize();
	m_nMaxIdx = 0;	// TODO: 스레드 관리 죽었을때 디비에 있는건 어찌해야해..
	//arrTemp.RemoveAll();
	theApp.m_arrLogSendingThreads.RemoveAll();
	strCmd.Format(_T("%s"), CPathInfo::GetLogFilePath());
	UM_WRITE_LOG(strQuery);
	
	while (TRUE)
	{
		//m_bIsNewJPG = FALSE;
		strLog.Format(_T("[CCheckEachLogFileInLogFolderThread] loop per %d seconds"), theApp.m_nScanDelay);
		UM_WRITE_LOG(strLog);

		CTime ctScanStartTime = CTime::GetCurrentTime();
		CString strScanStartTime = _T("");
		strScanStartTime = ctScanStartTime.Format(_T("%Y-%m-%d %H:%M:%S"));
		CString strQuery = _T("");
		if (!m_strMaxIdx.IsEmpty() && !theApp.m_bIsDeleteRequestPipeStart) {
			CStringArray arrDeleteList;
			CStringArray arrNkfiDeleteList;
			arrDeleteList.RemoveAll();
			arrNkfiDeleteList.RemoveAll();
			strQuery.Format(_T("select full_path from %s where full_path not like '%%%s%%' and file_size = current_offset and last_scan_time < '%s'"), DB_SEND_NCVI_LIST, m_strMaxIdx, strScanStartTime);
#ifdef _MGCHOI_TEST_
			if (theApp.m_pDbNcvi) {
				theApp.m_pDbNcvi->SelectQuery(strQuery, &arrDeleteList, _T("full_path"));
			}
#else	//#ifdef _MGCHOI_TEST_
			CCmsDBManager dbManageDelete(DB_SEND_NCVI_LIST);
			dbManageDelete.SelectQuery(strQuery, &arrDeleteList, _T("full_path"));
			dbManageDelete.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
			strQuery.Format(_T("select full_path from %s where full_path not like '%%%s%%' and file_size = current_offset and last_scan_time < '%s'"), DB_SEND_NKFI_LIST, m_strMaxIdx, strScanStartTime);
#ifdef _MGCHOI_TEST_
			if (theApp.m_pDbNkfi) {
				theApp.m_pDbNkfi->SelectQuery(strQuery, &arrNkfiDeleteList, _T("full_path"));
			}
#else	//#ifdef _MGCHOI_TEST_
			CCmsDBManager dbManageNkfiDelete(DB_SEND_NKFI_LIST);
			dbManageNkfiDelete.SelectQuery(strQuery, &arrNkfiDeleteList, _T("full_path"));
			dbManageNkfiDelete.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
			arrDeleteList.Append(arrNkfiDeleteList);
			theApp.AddDeleteFileRequestArray(arrDeleteList);
			if (0 < theApp.m_arrDeleteList.GetSize()) {
				theApp.PipeDeleteFileRequest();
			}
			if (!theApp.m_bIsDeleteRequestPipeStart) {
				strQuery.Format(_T("delete from %s where full_path not like '%%%s%%' and file_size = current_offset and last_scan_time < '%s'"), DB_SEND_NCVI_LIST, m_strMaxIdx, strScanStartTime);
#ifdef _MGCHOI_TEST_
				if (theApp.m_pDbNcvi) {
					theApp.m_pDbNcvi->DeleteQuery(strQuery);
				}
				strQuery.Format(_T("delete from %s where full_path not like '%%%s%%' and file_size = current_offset and last_scan_time < '%s'"), DB_SEND_NKFI_LIST, m_strMaxIdx, strScanStartTime);
				if (theApp.m_pDbNkfi) {
					theApp.m_pDbNkfi->DeleteQuery(strQuery);
			}
#else	//#ifdef _MGCHOI_TEST_
				CCmsDBManager dbManageDelete2(DB_SEND_NCVI_LIST);
				dbManageDelete2.DeleteQuery(strQuery);
				dbManageDelete2.Free();
				strQuery.Format(_T("delete from %s where full_path not like '%%%s%%' and file_size = current_offset and last_scan_time < '%s'"), DB_SEND_NKFI_LIST, m_strMaxIdx, strScanStartTime);
				CCmsDBManager dbManageDelete2(DB_SEND_NKFI_LIST);
				dbManageDelete2.DeleteQuery(strQuery);
				dbManageDelete2.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
			}
		}
		FindOldestWrittenFile(strCmd/*, arrFiles, arrSizeOfFiles*/);
		CheckOffset();
		Sleep(theApp.m_nScanDelay * 1000);
		if (!theApp.m_bIsCollecting) {
			TerminateAllLogSizeThreads();
			break;
		}
	}
	m_bCheckEachLogFileInLogFolderThread = FALSE;
	return 0;
}

void CCheckEachLogFileInLogFolderThread::FindOldestWrittenFile(CString _strPath/*, CStringArray& arrFiles, CUIntArray& arrSizeOfFiles*/)
{
	CFileFind Finder;
	CString strPath = _strPath + _T("\\*.*");
	BOOL bFind = Finder.FindFile(strPath);
	CString strQuery = _T("");
	while (bFind)
	{
		bFind = Finder.FindNextFile();
		if (Finder.IsDots())
			continue;

		if (Finder.IsDirectory() && !Finder.IsDots())
		{
			CString strFolder = Finder.GetFilePath();
			FindOldestWrittenFile(strFolder/*, arrFiles, arrSizeOfFiles*/);
		} else if (!Finder.IsDots())
		{
			CTime ctLastWriteTime;
			CString strFilePath = Finder.GetFilePath();
			UINT nFileSize = (UINT)Finder.GetLength();
			CString strLastWriteTime = _T("");
			Finder.GetLastWriteTime(ctLastWriteTime);
			strLastWriteTime = ctLastWriteTime.Format(_T("%Y-%m-%d %H:%M:%S"));
			CString strTableName = _T("");
			CCmsDBManager* dbManager = NULL;
			if (0 == ExtractFileExt(strFilePath).CompareNoCase(COLLECT_FILE_EXTENSION_LOG)) {
				strTableName.Format(_T("%s"), DB_SEND_LOG_LIST);
				dbManager = theApp.m_pDbLog;
			}
			else if (0 == ExtractFileExt(strFilePath).CompareNoCase(COLLECT_FILE_EXTENSION_NCVI)) {
				strTableName.Format(_T("%s"), DB_SEND_NCVI_LIST);
				dbManager = theApp.m_pDbNcvi;
				if (theApp.m_pCheckEachNcviFileInLogFolderThread) {
					if (theApp.m_nMaxSendLimit <= theApp.m_nNcviArrayMaxSizeFull) {
						continue;
					}
				}
			}
			else if ( 0 == ExtractFileExt(strFilePath).CompareNoCase(COLLECT_FILE_EXTENSION_NKFI)) {
				strTableName.Format(_T("%s"), DB_SEND_NKFI_LIST);
				dbManager = theApp.m_pDbNkfi;
				if (theApp.m_pCheckEachNkfiFileInLogFolderThread) {
					if (theApp.m_nMaxSendLimit <= theApp.m_nNkfiArrayMaxSizeFull) {
						continue;
					}
				}
			}
			if (!strTableName.IsEmpty()) {
				CStringArray arrSendList;
				arrSendList.RemoveAll();
				strQuery = _T("");
				//strQuery.Format(_T("select file_size, current_offset from %s where full_path like '%%%s%%'"), strTableName, strFilePath);
				strQuery.Format(_T("select file_size from %s where full_path like '%%%s%%'"), strTableName, strFilePath);
#ifdef _MGCHOI_TEST_
				if (dbManager) {
					dbManager->SelectQuery(strQuery, &arrSendList, _T("file_size")/*, _T("current_offset")*/);
				}
#else	//#ifdef _MGCHOI_TEST_
				CCmsDBManager dbManage1(strTableName);
				dbManage1.SelectQuery(strQuery, &arrSendList, _T("file_size")/*, _T("current_offset")*/);
				dbManage1.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
				////UM_WRITE_LOG(strQuery);
				if (0 >= arrSendList.GetSize()) {
					strQuery = _T("");
					strQuery.Format(_T("insert into %s (full_path, thread_idx, file_size, last_write_date, last_scan_time, each_scan_delay) values ('%s', %d, %d, '%s', datetime('now','localtime'), 0)"), strTableName, strFilePath, 0, nFileSize, strLastWriteTime);
#ifdef _MGCHOI_TEST_
					if (dbManager) {
						dbManager->InsertQuery(strQuery);
					}
#else	//#ifdef _MGCHOI_TEST_
					CCmsDBManager dbManage2(strTableName);
					dbManage2.InsertQuery(strQuery);
					dbManage2.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
					UM_WRITE_LOG(strQuery);
					if (0 == strTableName.CompareNoCase(DB_SEND_NCVI_LIST)) {
						/*if (m_strMaxIdx.IsEmpty())*/ {
							CStringArray arrMaxIdx;
							arrMaxIdx.RemoveAll();
							strQuery = _T("");
							strQuery.Format(_T("SELECT MAX(substr(full_path, %d, 10)) as result FROM %s where full_path not like '%%thumb%%'"),
								(CPathInfo::GetLogFilePath().GetLength() + 21), DB_SEND_NCVI_LIST);
#ifdef _MGCHOI_TEST_
							if (theApp.m_pDbNcvi) {
								theApp.m_pDbNcvi->SelectQuery(strQuery, &arrMaxIdx, _T("result"));
							}
#else	//#ifdef _MGCHOI_TEST_
							CCmsDBManager dbManageSelectMAX(DB_SEND_NCVI_LIST);
							dbManageSelectMAX.SelectQuery(strQuery, &arrMaxIdx, _T("result"));
							dbManageSelectMAX.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
							if (0 < arrMaxIdx.GetSize()) {
								CStringArray arrMaxIdx2;
								arrMaxIdx2.RemoveAll();
								strQuery = _T("");
								strQuery.Format(_T("SELECT MAX(CAST(substr(selected, %d) as INTEGER)) as result FROM (select rtrim(full_path, '.ncvi')\
									as selected from %s where full_path like '%%%s%%')")
									, CPathInfo::GetLogFilePath().GetLength() + 32, DB_SEND_NCVI_LIST, arrMaxIdx.GetAt(0));
#ifdef _MGCHOI_TEST_
								if (theApp.m_pDbNcvi) {
									theApp.m_pDbNcvi->SelectQuery(strQuery, &arrMaxIdx2, _T("result"));
								}
#else	//#ifdef _MGCHOI_TEST_
								CCmsDBManager dbManageSelectMAX2(DB_SEND_NCVI_LIST);
								dbManageSelectMAX2.SelectQuery(strQuery, &arrMaxIdx2, _T("result"));
								dbManageSelectMAX2.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
								if (0 < arrMaxIdx2.GetSize()) {
									m_strMaxIdx.Format(_T("%s_%s"), arrMaxIdx.GetAt(0), arrMaxIdx2.GetAt(0));
								}
							}
						}
					}
				} else {
					if (nFileSize != _ttoi(arrSendList.GetAt(0))) {
						strQuery = _T("");
						strQuery.Format(_T("update %s set file_size = %d, last_write_date = '%s' where full_path like '%%%s%%'"), strTableName, nFileSize, strLastWriteTime, strFilePath);
#ifdef _MGCHOI_TEST_
						if (dbManager) {
							dbManager->UpdateQuery(strQuery);
						}
#else	//#ifdef _MGCHOI_TEST_
						CCmsDBManager dbManage3(strTableName);
						dbManage3.UpdateQuery(strQuery);
						dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
						UM_WRITE_LOG(strQuery);
					}
				}
			}
		}
	}
	Finder.Close();
}

void CCheckEachLogFileInLogFolderThread::CheckOffset()
{
	CString strTargetLog = _T("");
	CString strLog = _T("");
	CString strQuery = _T("");
	CStringArray arrFiles;
	//CUIntArray arrSizeOfFiles;
	INT nCurrentOffset;
	LONGLONG nFileLength;
	arrFiles.RemoveAll();
	//arrSizeOfFiles.RemoveAll();
	
	strLog.Format(_T("[curltest] %d[line: %d, fuction: %s, file: %s]"), arrFiles.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);
	strQuery.Format(_T("select full_path, file_size, current_offset from %s where file_size != current_offset and datetime(strftime('%%s', last_scan_time)+ each_scan_delay, 'unixepoch') <	datetime('now','localtime') or last_scan_time is null"), DB_SEND_LOG_LIST);

#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbLog) {
		theApp.m_pDbLog->SelectQuery(strQuery, &arrFiles, _T("full_path"), _T("file_size"), _T("current_offset"));
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManage(DB_SEND_LOG_LIST);
	dbManage.SelectQuery(strQuery, &arrFiles, _T("full_path"), _T("file_size"), _T("current_offset"));
	dbManage.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	strLog.Format(_T("%s result size is %d [line: %d, function: %s, file: %s]"), strQuery, arrFiles.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);

	//if (0 >= arrFiles.GetSize()) {	// 2017-11-30 sy.choi 정책이 start인 상태에서 아무것도 파일이 변동이 없으면 생성agent에 policy 재전송
	//	PipeResendPolicy();
	//}

	for (int i = 0; i < arrFiles.GetSize(); i++)
	{
		//if (nullptr != theApp.m_pMemberData) {
		//	if (MAX_CPU_USAGE < theApp.m_pMemberData->GetCpuUsage() && MIN_SEND_LIMIT < m_nMaxSendLimit) {
		//		m_nMaxSendLimit--;
		//	}
		//}
		CString strData = arrFiles.GetAt(i);
		CStringArray arrData;
		CString strCurrentFile = _T("");
		arrData.RemoveAll();
		CParseUtil::ParseStringToStringArray(_T("|"), strData, &arrData);
		strCurrentFile = arrData.GetAt(0);
		//TODO: DB에 넣어야 할 거..modified date가 5일 이내에...지금은 다 넣자
		if (TRUE == FileExists(strCurrentFile) ) {
			strLog = _T("");
			strLog.Format(_T("[curltest] %s is exist. [line: %d, fuction: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
			if (0 == ExtractFileExt(strCurrentFile).CompareNoCase(COLLECT_FILE_EXTENSION_LOG)) {
				strTargetLog = strCurrentFile;
				nFileLength = _ttoi(arrData.GetAt(1));
				int nOffsetJustBefore = _ttoi(arrData.GetAt(2));
				int nEachFileDelay = 0;
				if (nOffsetJustBefore < nFileLength) {
					CStringArray arrSelect;
					strQuery = _T("");
					strQuery.Format(_T("select thread_idx from %s where full_path like '%%%s%%' and thread_idx != 0"), DB_SEND_LOG_LIST, strCurrentFile);
					if (theApp.m_pDbLog) {
						theApp.m_pDbLog->SelectQuery(strQuery, &arrSelect, _T("thread_idx"));
					}
					if (0 < arrSelect.GetSize()) {
						if (theApp.m_arrLogSendingThreads.GetAt(_ttoi(arrSelect.GetAt(0)) - 1))
							if (theApp.m_arrLogSendingThreads.GetAt(_ttoi(arrSelect.GetAt(0)) - 1)->m_bCheckLogSizeThreadSending)
								continue;
					}
					if (theApp.GetOffsetFromServer(strTargetLog, nCurrentOffset, nEachFileDelay)) {
						if (-2 == nCurrentOffset) {
							DBGLOG(_T("[CCheckEachNcviFileInLogFolderThread] [%s][%d][%lld]"), strCurrentFile, nCurrentOffset, nFileLength);
							continue;
						}
						strLog.Format(_T("[CCheckEachLogFileInLogFolderThread] [%s][%d][%lld]"), strCurrentFile, nCurrentOffset, nFileLength);
						UM_WRITE_LOG(strLog);
						if (nCurrentOffset) {	// 2017-10-12 sy.choi 왜 여기에 있지?
							strQuery = _T("");
							strQuery.Format(_T("update %s set current_offset = %d, last_scan_time = datetime('now','localtime'), each_scan_delay = %d where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, nCurrentOffset, nEachFileDelay, strCurrentFile);
							strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
							UM_WRITE_LOG(strLog);
#ifdef _MGCHOI_TEST_
							if (theApp.m_pDbLog) {
								theApp.m_pDbLog->UpdateQuery(strQuery);
							}
#else	//#ifdef _MGCHOI_TEST_
							CCmsDBManager dbManageOffsetUpdate(DB_SEND_LOG_LIST);
							dbManageOffsetUpdate.UpdateQuery(strQuery);
							dbManageOffsetUpdate.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
						}
						if (nCurrentOffset < nFileLength) {
							CString strThreadHandle = _T("");
							CStringArray arrSelected;
							CString strQuery = _T("");
							arrSelected.RemoveAll();
							strQuery.Format(_T("select * from %s where full_path like '%%%s%%' and thread_idx > 0 and thread_idx is not null;"), DB_SEND_LOG_LIST, strCurrentFile);
#ifdef _MGCHOI_TEST_
							if (theApp.m_pDbLog) {
								theApp.m_pDbLog->SelectQuery(strQuery, &arrSelected, _T("thread_idx"));
							}
#else	//#ifdef _MGCHOI_TEST_
							CCmsDBManager dbManage2(DB_SEND_LOG_LIST);
							dbManage2.SelectQuery(strQuery, &arrSelected, _T("thread_idx"));
							dbManage2.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
							UM_WRITE_LOG(strQuery);
							BOOL bFoundExistedThreadIdx = FALSE;
							if ((0 < arrSelected.GetSize())) {
								if (0 < theApp.m_arrLogSendingThreads.GetSize()) {
									int nThreadIdx = _ttoi(arrSelected.GetAt(0));
									if (theApp.m_arrLogSendingThreads.GetSize() >= nThreadIdx) {	// 2017-09-14 sy.choi getat 했을 때 혹시 죽지 않도록.
										if (theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)) {
											if (theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->m_bCheckLogSizeThreadStart) {
/*												CString strTargetFileNameFromPostThread = _T("");
												strTargetFileNameFromPostThread = theApp.m_arrSendingThreads.GetAt(nThreadIdx-1)->GetTargetFile();
												if (0 == strTargetLog.CompareNoCase(strTargetFileNameFromPostThread)) */{	//2017-09-14 sy.choi array의 스레드와 파일 이름이다를 수도? 그러면 삭제
													bFoundExistedThreadIdx = TRUE;
													// 2017-09-12 sy.choi 서버에서 온 offset이 -1일 경우 스레드 종료하도록 수정
													if (-1 == nCurrentOffset) {
														BOOL bQueryResult = FALSE;
														strQuery = _T("");
														strQuery.Format(_T("update %s set thread_idx = %d where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, 0, strCurrentFile);
														UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
														if (theApp.m_pDbLog) {
															theApp.m_pDbLog->UpdateQuery(strQuery);
														}
#else	//#ifdef _MGCHOI_TEST_
														CCmsDBManager dbManage3(DB_SEND_LOG_LIST);
														bQueryResult = dbManage3.UpdateQuery(strQuery);
														dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
														UM_WRITE_LOG(strQuery);
														if (bQueryResult) {
															theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->SetStopEvent();
															theApp.m_arrLogSendingThreads.RemoveAt(nThreadIdx-1);
															for (int j = nThreadIdx; j < theApp.m_arrLogSendingThreads.GetSize(); j++)
															{
																strQuery = _T("");
																strLog = _T("");
																strQuery.Format(_T("update %s set thread_idx = %d where thread_idx = %d"), DB_SEND_LOG_LIST, j, j+1);
																int nCount = 0;
																while (1)
																{
																	BOOL bUpdateQueryResult = FALSE;
																	if (QUERY_TRY_MAX_LIMIT < nCount) {
																		strLog.Format(_T("%s fail times. so massed up"), strQuery);
																		UM_WRITE_LOG(strLog);
																		break;
																	}														
																	strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
																	UM_WRITE_LOG(strLog);
#ifdef _MGCHOI_TEST_
																	if (theApp.m_pDbLog) {
																		bUpdateQueryResult = theApp.m_pDbLog->UpdateQuery(strQuery);
																	}
#else	//#ifdef _MGCHOI_TEST_
																	CCmsDBManager dbManage3(DB_SEND_LOG_LIST);
																	bUpdateQueryResult = dbManage3.UpdateQuery(strQuery);
																	dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
																	if (bUpdateQueryResult) {
																		break;
																	} else {
																		strLog.Format(_T("%s fail.."), strQuery);
																		UM_WRITE_LOG(strLog);
																		nCount++;
																	}
																}
																UM_WRITE_LOG(strQuery);
															}
														}
													} else if (FALSE == theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->m_bCheckLogSizeThreadSending || FALSE == theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->m_bCheckLogSizeThreadStart) {
														theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->SetCurrentOffset(nCurrentOffset);
														theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->SetFileSize((UINT)nFileLength);
														theApp.m_arrLogSendingThreads.GetAt(nThreadIdx-1)->SetResendEvent();
														continue;
													}
												}
											}
										}
									}
									if (!bFoundExistedThreadIdx) {	// 2017-09-14 send_list에서 select하면 있는데 스레드는 없을 경우 
										// 2017-09-22 sy.choi fatal error
									}
								}
							} 
							if ((FALSE == bFoundExistedThreadIdx) && (nCurrentOffset != -1)) {	// 2017-09-14 sy.choi select해서 없을 때 && select해서 존재하는데 array에 스레드가 없어서 delete했을 때 db에 update하고 array에 추가
								int nArrSize = theApp.m_arrLogSendingThreads.GetSize();
								if (nArrSize >theApp.m_nMaxSendLimit) {	// 2017-09-12 sy.choi 최대 크기 이상이면 stop event를 보낼 것임.
									BOOL bResult = FALSE;
									strLog.Format(_T("[curltest] %d [line: %d, fuction: %s, file: %s]"), theApp.m_arrLogSendingThreads.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
									UM_WRITE_LOG(strLog);
									for (int i = 0; i < nArrSize; i++)
									{
										/*if (theApp.m_arrSendingThreads.GetAt(i)->m_hStopEvent) */{	// 2017-09-12 sy.choi stop event 핸들 확인
											if (FALSE == theApp.m_arrLogSendingThreads.GetAt(i)->m_bCheckLogSizeThreadStart || FALSE == theApp.m_arrLogSendingThreads.GetAt(i)->m_bCheckLogSizeThreadSending) {
												BOOL bQueryResult = FALSE;
												strQuery = _T("");
												strQuery.Format(_T("update %s set thread_idx = %d where thread_idx = %d"), DB_SEND_LOG_LIST, 0, i+1);
												UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
													if (theApp.m_pDbLog) {
														bQueryResult = theApp.m_pDbLog->UpdateQuery(strQuery);
													}
#else	//#ifdef _MGCHOI_TEST_
												CCmsDBManager dbManage3(DB_SEND_LOG_LIST);
												bQueryResult = dbManage3.UpdateQuery(strQuery);
												dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
												if (bQueryResult) {
													theApp.m_arrLogSendingThreads.GetAt(i)->SetStopEvent();
													theApp.m_arrLogSendingThreads.RemoveAt(i);
													for (int j = i+1; j <= theApp.m_nMaxSendLimit; j++)
													{
														int nCount = 0;
														BOOL bUpdateQueryResult = FALSE;
														strQuery = _T("");
														strQuery.Format(_T("update %s set thread_idx = %d where thread_idx = %d"), DB_SEND_LOG_LIST, j, j+1);
														while (1)
														{
															strLog = _T("");
															if (QUERY_TRY_MAX_LIMIT < nCount) {
																strLog.Format(_T("%s fail times. so massed up"), strQuery);
																UM_WRITE_LOG(strLog);
																break;
															}
															strLog = _T("");
															strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
															UM_WRITE_LOG(strLog);
#ifdef _MGCHOI_TEST_
															if (theApp.m_pDbLog) {
																bUpdateQueryResult = theApp.m_pDbLog->UpdateQuery(strQuery);
															}
#else	//#ifdef _MGCHOI_TEST_
															CCmsDBManager dbManage4(DB_SEND_LOG_LIST);
															bUpdateQueryResult = dbManage4.UpdateQuery(strQuery);
															dbManage4.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
															if (bUpdateQueryResult) {
																break;
															} else {
																strLog.Format(_T("%s fail.."), strQuery);
																UM_WRITE_LOG(strLog);
																nCount++;
															}
														}
													}
												}
												//arrTemp.RemoveAt(i);
												bResult = TRUE;
												break;
											}
										}
									}
									if (bResult) {
										BOOL bQueryResult = FALSE;
										strQuery = _T("");
										strQuery.Format(_T("update %s set thread_idx = %d, current_offset = %d where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, theApp.m_arrLogSendingThreads.GetSize()+1, nCurrentOffset, strTargetLog);
#ifdef _MGCHOI_TEST_
										if (theApp.m_pDbLog) {
											bQueryResult = theApp.m_pDbLog->UpdateQuery(strQuery);
										}
#else	//#ifdef _MGCHOI_TEST_
										CCmsDBManager dbManage4(DB_SEND_LOG_LIST);
										bQueryResult = dbManage4.UpdateQuery(strQuery);
										dbManage4.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
										UM_WRITE_LOG(strQuery);

										if (bQueryResult) {
											CCheckLogSizeThread* pSendThread = (CCheckLogSizeThread*)AfxBeginThread(RUNTIME_CLASS(CCheckLogSizeThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
											if (pSendThread) {
												pSendThread->SetTargetFile(strTargetLog);
												pSendThread->SetCurrentOffset(nCurrentOffset);
												//pSendThread->SetDelay(nDelay);
												pSendThread->SetFileSize((UINT)nFileLength);
												pSendThread->ResumeThread();
												pSendThread->SetResendEvent();
												theApp.m_arrLogSendingThreads.Add(pSendThread);
											}
										}
									}
								} else {
									strLog.Format(_T("[curltest] %d [line: %d, fuction: %s, file: %s]"), theApp.m_arrLogSendingThreads.GetSize(), __LINE__, __FUNCTIONW__, __FILEW__);
									BOOL bQueryResult = FALSE;
									strQuery = _T("");
									strQuery.Format(_T("update %s set thread_idx = %d, current_offset = %d where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, theApp.m_arrLogSendingThreads.GetSize()+1, nCurrentOffset, strTargetLog);
#ifdef _MGCHOI_TEST_
									if (theApp.m_pDbLog) {
										bQueryResult = theApp.m_pDbLog->UpdateQuery(strQuery);
									}
#else	//#ifdef _MGCHOI_TEST_
									CCmsDBManager dbManage5(DB_SEND_LOG_LIST);
									bQueryResult = dbManage5.UpdateQuery(strQuery);
									dbManage5.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
									UM_WRITE_LOG(strQuery);
									if (bQueryResult) {
										CCheckLogSizeThread* pSendThread = (CCheckLogSizeThread*)AfxBeginThread(RUNTIME_CLASS(CCheckLogSizeThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
										if (pSendThread) {
											pSendThread->SetTargetFile(strTargetLog);
											pSendThread->SetCurrentOffset(nCurrentOffset);
											//pSendThread->SetDelay(nDelay);
											pSendThread->SetFileSize((UINT)nFileLength);
											pSendThread->ResumeThread();
											pSendThread->SetResendEvent();
											theApp.m_arrLogSendingThreads.Add(pSendThread);
										}
									}
								}
								//strQuery.Format(_T("insert into DB_SEND_LOG_LIST (thread_idx, full_path, current_offset, last_write_date) values (%d, %s, %d, %s)"), m_nMaxIdx, strTargetLog, nCurrentOffset, _T(""));
								//CCmsDBManager dbManage;
								//dbManage.InsertQuery(strQuery);
							}
						} else if (nCurrentOffset > GetFileSize(strCurrentFile)) {
							theApp.PostResetOffsetToServer(strCurrentFile);
						}
					}
				} else if (nOffsetJustBefore > GetFileSize(strCurrentFile)) {
					theApp.PostResetOffsetToServer(strCurrentFile);
				}
			}
		} else {
			strLog = _T("");
			strLog.Format(_T("[curltest] %s is not exist. [line: %d, fuction: %s, file: %s]"), strCurrentFile, __LINE__, __FUNCTIONW__, __FILEW__);
			CString strQuery = _T("");
			strQuery.Format(_T("delete from %s where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, strCurrentFile);
#ifdef _MGCHOI_TEST_
			if (theApp.m_pDbLog) {
				theApp.m_pDbLog->DeleteQuery(strQuery);
			}
#else	//#ifdef _MGCHOI_TEST_
			CCmsDBManager dbManageNotExist(DB_SEND_LOG_LIST);
			dbManageNotExist.DeleteQuery(strQuery);
			dbManageNotExist.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
		}
	}
}

BOOL CCheckEachLogFileInLogFolderThread::TerminateAllLogSizeThreads()
{
	CString strLog = _T("");
	strLog.Format(_T("TerminateAllLogSizeThreads start...[line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);
	BOOL bResult = FALSE;
	CString strQuery = _T("");
	strQuery.Format(_T("update %s set thread_idx = %d where thread_idx != %d or thread_idx is not null"), DB_SEND_LOG_LIST, 0, 0);
#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbLog) {
		theApp.m_pDbLog->UpdateQuery(strQuery);
}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManage(DB_SEND_LOG_LIST);
	dbManage.UpdateQuery(strQuery);
	dbManage.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	UM_WRITE_LOG(strQuery);
	for (int i = 0; i < theApp.m_arrLogSendingThreads.GetSize(); i++)
	{
		if (theApp.m_arrLogSendingThreads.GetAt(i)->m_hStopEvent) {
			if (FALSE == theApp.m_arrLogSendingThreads.GetAt(i)->m_bCheckLogSizeThreadStart) {
				theApp.m_arrLogSendingThreads.GetAt(i)->SetStopEvent();
				theApp.m_arrLogSendingThreads.RemoveAt(i);
			}
		}

	}
	bResult = TRUE;
	return bResult;
}

//void CCheckEachLogFileInLogFolderThread::SendThumb()
//{
//	if (m_bSendThumbThreadStart)
//		return;
//	m_bSendThumbThreadStart = TRUE;
//	HANDLE hThread;
//	DWORD dwUIThreadID = 0;
//
//	hThread= ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)SendThumbThread, this, 0, &dwUIThreadID);
//	CloseHandle(hThread);
//}
//
//BOOL WINAPI SendThumbThread(LPVOID lpParam)
//{
//	CCheckEachLogFileInLogFolderThread* pWnd = reinterpret_cast<CCheckEachLogFileInLogFolderThread*>(lpParam);
//	CString strThumbFile = pWnd->m_strJPG;
//	if (strThumbFile.IsEmpty()) {
//		pWnd->m_bSendThumbThreadStart = FALSE;
//		return FALSE;
//	}
//	BOOL bResult= FALSE;
//	CFile cFile;
//	CString strUrl = _T("");
//	CParamParser parser;
//	CString strParam = _T("");
//	CString strReturn = _T("");
//	CString strParameter = _T("");
//	if (cFile.Open(strThumbFile, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone)) {
//	strParam = parser.CreateKeyValueScreenRequest(strThumbFile);
//	strUrl.Format(_T("%s/v1/trans/last_image.dsntech"), theApp.m_pMemberData->GetServerHostURL());
//	DWORD nBytesRead = 1;
//
//	BYTE* pFileData = new BYTE[SEND_JPG_MAX_SIZE];
//	memset(pFileData, 0x00, SEND_JPG_MAX_SIZE);
//	nBytesRead = cFile.Read(pFileData, SEND_JPG_MAX_SIZE);
//	if (0 < nBytesRead) {
//		CBase64 base64;
//		CString strEncodedFileData = _T("");
//		CString strUrlEncodedData = _T("");
//		strEncodedFileData = base64.base64encode(pFileData, nBytesRead);
//		strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
//		strParameter = strParam + strUrlEncodedData;
//	} else {
//		cFile.Close();
//		SE_MemoryDelete(pFileData);
//		pWnd->m_bSendThumbThreadStart = FALSE;
//		return FALSE;
//	}
//	SE_MemoryDelete(pFileData);
//	int nErr = 0;
//	strReturn = theApp.CurlPost(strParameter, strUrl, nErr);
//	{
//		CString strLog = _T("");
//		strLog.Format(_T("[SendThumbThread] %s %s[line: %d, function: %s, file: %s]"), strParameter, strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
//		UM_WRITE_LOG(strLog);
//	}
//	CString strTokenFromReturn = _T("");
//	POLICY_SET* pPolicySet = new POLICY_SET;
//	if (parser.ParserPolicySet(strReturn, /*nCurrentOffset, */strTokenFromReturn, pPolicySet)) {
//		theApp.SetToken(strTokenFromReturn);
//		if (NULL != pPolicySet) {
//			if (pPolicySet->bIsPolicyExist) {
//				theApp.ChangePolicy(pPolicySet);
//			}
//		}
//		bResult = TRUE;
//	}
//	SE_MemoryDelete(pPolicySet);
//	}
//	cFile.Close();
//	pWnd->m_bSendThumbThreadStart = FALSE;
//	return TRUE;
//}