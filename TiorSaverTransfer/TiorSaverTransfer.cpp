
// TiorSaverTransfer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TiorSaverTransfer.h"
//#include "TiorSaverTransferPopupDlg.h"
#include "..\BuildEnv\common\UtilsFile.h"
#include "../BuildEnv/common/UtilParse.h"
#include "../BuildEnv/common/UtilsUnicode.h"
#include "Impersonator.h"
#include "ParamParser.h"
#include "PathInfo.h"
#include "Crypto/Base64.h"
#include "Sntp.h"
#include <fstream>
#include "UtilsProcess.h"
#include "WTSSession.h"
//#include "MainFrm.h"
//#include "SendAgentCommonDefine.h"
#include <cstring>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CTiorSaverTransferApp

BEGIN_MESSAGE_MAP(CTiorSaverTransferApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
	//ON_WM_TIMER()
END_MESSAGE_MAP()

CTiorSaverTransferApp::CTiorSaverTransferApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_pMemberData = new CMemberData;
	//m_bComplete = TRUE;
	m_nTempCurrentPos = 0;
	m_bIsRandom = FALSE;
	//m_strUrl = STR_SEND_URL;
	//m_strUrl = _T("");
	m_bIsCollecting = AGENT_STATE_STOP;
	m_pCheckEachLogFileInLogFolderThread = NULL;
	m_pCheckEachNcviFileInLogFolderThread = NULL;
	m_pCheckEachNkfiFileInLogFolderThread = NULL;
	//m_pCheckJPGFileInLogFolderThread = NULL;
	m_pCheckPolicyThread = NULL;
	m_pPTOAPipeServer = NULL;
	m_pCTOSPipeServer = NULL;
	m_nScanDelay = 5;
	m_bIsFirstUser = TRUE;
	m_nDeleteDelay = -1;
	m_strDeleteDate = _T("");
	//m_strTimeFromServer = _T("");
	m_bIsPipeStart = FALSE;
	m_bUninstallStart = FALSE;
	m_nPolicyRetry = SLEEP_REQUEST_POLICY_IF_STOP_PER_SECONDS;
	m_pWebsocketClientThread = NULL;
	m_pCSTOUCPipeClient = NULL;
	m_pCSTOSCPipeClient = NULL;
	m_pReloadThread = nullptr;
	m_bInitialized = FALSE;
	m_arrDeleteList.RemoveAll();
	m_bChangingPolicy = FALSE;
	m_bIsDeleteRequestPipeStart = FALSE;
	m_nNcviArrayMaxSizeFull = 0;
	m_nNkfiArrayMaxSizeFull = 0;
	m_nMaxSendLimit = MAX_SEND_LIMIT;
	m_bDeleteOldFilesStart = FALSE;
	m_pDbSetting = NULL;
#ifdef _MGCHOI_TEST_
	m_pDbNcvi = NULL;
	m_pDbNkfi = NULL;
	m_pDbLog = NULL;
#endif
	m_nTransMaxScreenSize = 10485760;
	m_nTransMaxLogLines = 3000;
	m_pTokenMutex = new std::mutex;
	m_strToken = _T("");
	m_bSendThumbThreadStart = FALSE;
	m_hCTOSPipeServer = NULL;
	m_bUpdateAfterReboot = TRUE;
}

/**
 @brief     소멸자
 @author    kh.choi
 @date      2017-03-02
*/
CTiorSaverTransferApp::~CTiorSaverTransferApp(void)
{
	// 2017-01-20 kh.choi 파일로그 닫기 [FileLogSet]
	//SingletonFor//ezLog::DeleteSingleton();
	//CloseLogFile();
	if (!FreeConsole())
	{
		TRACE("Failed to free the console!");
	}
	SE_MemoryDelete(m_pReloadThread);
	CloseHandle(m_hPTOAPipeServer);
	SE_MemoryDelete(m_pCSTOSCPipeClient);
	SE_MemoryDelete(m_pCSTOUCPipeClient);
	SE_MemoryDelete(m_pMemberData);
	SE_MemoryDelete(m_pMemberData);
	SE_MemoryDelete(m_pWebsocketClientThread);
	m_pDbSetting->Free();
	SE_MemoryDelete(m_pDbSetting);
	SE_MemoryDelete(m_pTokenMutex);
}


// The one and only CTiorSaverTransferApp object

CTiorSaverTransferApp theApp;


// CTiorSaverTransferApp initialization

BOOL CTiorSaverTransferApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	AfxEnableControlContainer();

	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	m_Minidump.install_self_mini_dump();

	CString strMutextname = _T("");
	strMutextname.Format(_T("%s"), WTIOR_TRANS_AGENT_MUTEX_NAME);
	if( CProcess::ProcessCreateMutex(strMutextname) == false ) {
		CString strLog = _T("");
		strLog.Format(_T("createmutex fail %d"), GetLastError());
		//MessageBox(NULL, strLog, _T("TiorSaver"), MB_OK);
		exit(0);
	}
	RegisterStartProgram();
	g_cDbgLog.SetLogFileName(L"TiorSaverTransfer.log");
	g_cDbgLog.InitLog();

	CString strTestDBFileName = _T("");
	strTestDBFileName.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), WTIOR_SETTING_DATABASE);
	if (!FileExists(strTestDBFileName)) {
		m_bIsFirstUser = TRUE;
		//CreateDB();
		//CTiorSaverTransferRegisterDlg registerDlg;
		//registerDlg.DoModal();
	} else {
		m_bIsFirstUser = FALSE;
	}
	m_pDbSetting = new CCmsDBManager(DB_AGENT_INFO);
	CString strCountFile = _T("");
	strCountFile.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("reboot.require"));
	if (FileExists(strCountFile)) {
		ForceDeleteFile(strCountFile);
		return TRUE;
	} else {
		m_pReloadThread = (CReloadThread*)AfxBeginThread(RUNTIME_CLASS(CReloadThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
		m_pReloadThread->ResumeThread();

		CString strFilesDBFileName = _T("");
		strFilesDBFileName.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), WTIOR_LOG_DATABASE);
		if (!FileExists(strFilesDBFileName)) {
			CCmsDBManager dbManage(DB_SEND_LOG_LIST);
			dbManage.CreateQuery();
			dbManage.Free();
		}
		strFilesDBFileName.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), WTIOR_NCVI_DATABASE);
		if (!FileExists(strFilesDBFileName)) {
			CCmsDBManager dbManage(DB_SEND_NCVI_LIST);
			dbManage.CreateQuery();
			dbManage.Free();
		}
		strFilesDBFileName.Format(_T("%s%s"), CPathInfo::GetDBFilePath(), WTIOR_NKFI_DATABASE);
		if (!FileExists(strFilesDBFileName)) {
			CCmsDBManager dbManage(DB_SEND_NKFI_LIST);
			dbManage.CreateQuery();
			dbManage.Free();
		}

#ifdef _MGCHOI_TEST_
		theApp.m_pDbLog = new CCmsDBManager(DB_SEND_LOG_LIST);
		theApp.m_pDbNcvi = new CCmsDBManager(DB_SEND_NCVI_LIST);
		theApp.m_pDbNkfi = new CCmsDBManager(DB_SEND_NKFI_LIST);
#endif	//#ifdef _MGCHOI_TEST_
		DWORD dwUIPTOAThreadID = 0;
		m_hPTOAPipeServer = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)StartPTOAPipeServerThread, this, 0, &dwUIPTOAThreadID);
		DWORD dwIOCTOSThreadID = 0;
		m_hCTOSPipeServer = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)StartCTOSPipeServerThread, this, 0, &dwIOCTOSThreadID);
		m_pWebsocketClientThread = (CWebsocketClient*)AfxBeginThread(RUNTIME_CLASS(CWebsocketClient), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
#ifdef _TEST_CHOI_SSL_
		m_pWebsocketClientThread->InitDomain(_T("wss://52.78.113.115:3000/agent"));
#else
		m_pWebsocketClientThread->InitDomain(_T("ws://52.78.113.115:3001/agent"));
#endif
		m_pWebsocketClientThread->ResumeThread();
		m_pWebsocketClientThread->SetConnectEvent();

		m_pWnd = new CTiorSaverTransferWnd;
		m_pMainWnd = m_pWnd;
		m_pWnd->CreateEx(0, AfxRegisterWndClass(NULL, NULL, NULL, NULL), _T("TIORTransfer"), WS_POPUP, 0, 0, 100, 100, GetDesktopWindow(), NULL);
		//m_pWnd->LoadFrame(IDR_MAINFRAME, WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, m_pMainWnd->GetDesktopWindow(), NULL);
		m_pWnd->ShowWindow(SW_HIDE);
		CString strSTOCSysPipeName = _T("");
		CString strSTOCUsrPipeName = _T("");
		strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
		strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, GetUsrAgentSessionID());

		m_pCSTOUCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
		m_pCSTOSCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);

		m_pMainWnd->UpdateWindow();
		InitAgent();
		if (GIGABYTES_BYTES < GetDirectorySize(CPathInfo::GetTransferLogFilePath())) {
			ForceDeleteDir(CPathInfo::GetTransferLogFilePath());
		}
	}
	return TRUE;
}

BOOL WINAPI StartPTOAPipeServerThread(LPVOID lpParam)
{
	CTiorSaverTransferApp* pWnd = reinterpret_cast<CTiorSaverTransferApp*>(lpParam);

	pWnd->m_bThreadPTOAPipeServerExit = FALSE;

	pWnd->m_pPTOAPipeServer = new CPTOAPipeServer(PTOA_PIPE_NAME);
	pWnd->m_pPTOAPipeServer->PipeServerStartUp();

	pWnd->m_bThreadPTOAPipeServerExit = TRUE;

	return TRUE;
}

BOOL WINAPI StartCTOSPipeServerThread(LPVOID lpParam)
{
	CTiorSaverTransferApp* pWnd = reinterpret_cast<CTiorSaverTransferApp*>(lpParam);

	pWnd->m_bThreadCTOSPipeServerExit = FALSE;

	pWnd->m_pCTOSPipeServer = new CCTOSPipeServer(CTOS_PIPE_NAME);
	pWnd->m_pCTOSPipeServer->PipeServerStartUp();

	pWnd->m_bThreadCTOSPipeServerExit = TRUE;

	return TRUE;

}

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp) {
	((std::string *)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}


//BOOL CTiorSaverTransferApp::CreateDB(CString _strName)
//{
//	BOOL bCreate;
//	CCmsDBManager dbManage(_strName);
//	bCreate = dbManage.CreateQuery();
//
//	dbManage.Free();
//
//	return bCreate;
//}

void CTiorSaverTransferApp::SetToken(CString _strToken)
{
	scoped_lock(*m_pTokenMutex);
	DBGLOG(_T("[SetToken] %s [line: %d, function: %s, file: %s]"), _strToken, __LINE__, __FUNCTIONW__, __FILEW__);
	CString strValue = _strToken;
	if (strValue.GetLength() <= 0) {
		CString strLog = _T("");
		strLog.Format(_T("[curltest]invalid token [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
		return;
	}
	if (0 != m_strToken.Compare(_strToken))
	{
		m_strToken.Format(_T("%s"), _strToken);
		CString strTokenFilePath = _T("");
		strTokenFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("my.token"));
		DWORD dwBytesWritten = 0;
		int nCnt = 0;
		while(!WriteFileExample(strTokenFilePath, _strToken, dwBytesWritten) || 10 == nCnt) {
			nCnt++;
			CString strLog = _T("");
			strLog.Format(_T("[curltest]invalid token file [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
	}
	if (!m_curl.m_strTimeFromServer.IsEmpty()) {
		PipeServerTime();
	}
}

CString CTiorSaverTransferApp::GetToken()
{
	scoped_lock(*m_pTokenMutex);
	if (m_strToken.IsEmpty())
	{
		CString strTokenFilePath = _T("");
		CString strReadData = _T("");
		strTokenFilePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("my.token"));
		ReadAnsiFileTxt(strTokenFilePath, strReadData);
		m_strToken = strReadData;
	}
	return m_strToken;
}

BOOL CTiorSaverTransferApp::GetOffsetFromServer(CString _strTargetFile, INT& _nCurrentOffset, int& _nDelay)
{
	CString strParam = _T("");
	CString strReturn = _T("");
	CString strUrl = _T("");
	CString strToken = _T("");
	INT nCurrentOffset = 0;
	int nDelay = 0;

	CParamParser parse;
	strParam = parse.CreateOffsetKeyValueRequest(_strTargetFile);
	if (COLLECT_FILE_EXTENSION_LOG == ExtractFileExt(_strTargetFile)) {
		strUrl.Format(_T("%s/v1/trans/event?%s"), m_pMemberData->GetServerHostURL(), strParam);
	} else {
		strUrl.Format(_T("%s/v1/trans/screen?%s"), m_pMemberData->GetServerHostURL(), strParam);
	}
	int nErr = 0;
	//if (SEND_TYPE_CURL == theApp.m_nSendType) {
		strReturn = m_curl.CurlGET(strUrl, GetToken());
	/*} else if (SEND_TYPE_HTTPSTCONNECT == theApp.m_nSendType) {
		strReturn = theApp.HttpsGET(theApp.GetToken(), strUrl);
	}*/
		INT nParseResult = parse.ParserGetOffsetReturn(strReturn, nCurrentOffset, strToken, nDelay);
	if (1 == nParseResult) {
		SetToken(strToken);
		_nCurrentOffset = nCurrentOffset;
		_nDelay = nDelay;
		return TRUE;
	} else {
		if (CURLE_OK != m_curl.GetCurlLastError()) {
			strToken.Format(_T("%d"), m_curl.GetCurlLastError());
		} else {
			strToken.Format(_T("%s"), strReturn);
			if (-1 != strReturn.Find(_T("html"))) {
				_nCurrentOffset = -2;
			}
		}
		return FALSE;
	}
}

BOOL CTiorSaverTransferApp::PostResetOffsetToServer(CString _strTargetFile)
{
	BOOL bResult = FALSE;
	INT nErr = 0;
	CString strParam = _T("");
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strTokenFromReturn = _T("");
	CParamParser parser;
	CString strLog = _T("");
	CString strQuery = _T("");
	CString strTableName = _T("");
	CCmsDBManager * dbManage = NULL;
	DBGLOG(_T("[PostResetOffsetToServer] %s [line: %d, fuction: %s, file: %s]"), _strTargetFile, __LINE__, __FUNCTIONW__, __FILEW__);
	if (0 == ExtractFileExt(_strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_NCVI)) {
		strTableName.Format(_T("%s"), DB_SEND_NCVI_LIST);
		dbManage = theApp.m_pDbNcvi;
	} else if (0 == ExtractFileExt(_strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_NKFI)) {
		strTableName.Format(_T("%s"), DB_SEND_NKFI_LIST);
		dbManage = theApp.m_pDbNkfi;
	} else if (0 == ExtractFileExt(_strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_LOG)) {
		strTableName.Format(_T("%s"), DB_SEND_LOG_LIST);
		dbManage = theApp.m_pDbLog;
	}
	{	// 2017-10-12 sy.choi 서버에서 받은 filesize가 실제 filesize보다 클 경우 초기화 post하도록 추가
		strQuery.Format(_T("update %s set current_offset = %d where full_path like '%%%s%%'"), strTableName, 0, _strTargetFile);
		strLog.Format(_T("%s [line: %d, function: %s, file: %s]"), strQuery, __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
#ifdef _TIOR_TRANSFER_
		if (dbManage) {
			dbManage->UpdateQuery(strQuery);
		}
#else
		CCmsDBManager dbManageOffsetUpdate(strTableName);
		dbManageOffsetUpdate.UpdateQuery(strQuery);
		dbManageOffsetUpdate.Free();
#endif
	}
	strParam = parser.CreateFileResetRequest(_strTargetFile);
	strUrl.Format(_T("%s/v1/trans/init"), theApp.m_pMemberData->GetServerHostURL());
	strReturn = m_curl.CurlPost(strParam, strUrl, GetToken());
	INT nParseResult = 0;
	nParseResult = parser.ParserOnlyToken(strReturn, strTokenFromReturn);
	if (1 == nParseResult) {
		bResult = TRUE;
		SetToken(strTokenFromReturn);
	} else if (-1 == nParseResult) {
		// 2017-11-27 sy.choi
		m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
	} else {
		if (strTokenFromReturn.IsEmpty()) {
			strTokenFromReturn.Format(_T("%d"), m_curl.GetCurlLastError());
		}
		//MessageBox(NULL, strToken, _T("TiorSaver"), MB_OK);
	}
	return bResult;
}

BOOL CTiorSaverTransferApp::TerminateFileTransfer()
{
	BOOL bResult = TRUE;
	CString strLog = _T("");
	strLog.Format(_T("TerminateCommunication start...[line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
	UM_WRITE_LOG(strLog);
	m_bIsCollecting = AGENT_STATE_STOP;
	Sleep(m_nScanDelay * 1000);
	if (m_pCheckEachLogFileInLogFolderThread) {
		//delete m_pCheckEachLogFileInLogFolderThread;
		m_pCheckEachLogFileInLogFolderThread = NULL;
	}
	if (m_pCheckEachNcviFileInLogFolderThread) {
		m_pCheckEachNcviFileInLogFolderThread = NULL;
	}
	if (m_pCheckEachNkfiFileInLogFolderThread) {
		m_pCheckEachNkfiFileInLogFolderThread = NULL;
	}
	//if (m_pCheckJPGFileInLogFolderThread) {
	//	m_pCheckJPGFileInLogFolderThread = NULL;
	//}
	return bResult;
}

void CTiorSaverTransferApp::SynchronziationTime(CString _strRecvServerTime)
{
	CString strFirstData, strSecondData;
	CString strYear, strMonth, strDay, strHour, strMin, strSec;
	AfxExtractSubString(strFirstData,_strRecvServerTime, 0, ' ');
	AfxExtractSubString(strSecondData,_strRecvServerTime, 1, ' ');

	AfxExtractSubString(strYear,strFirstData, 0,'-');
	AfxExtractSubString(strMonth,strFirstData, 1, '-');
	AfxExtractSubString(strDay,strFirstData, 2, '-');

	AfxExtractSubString(strHour,strSecondData, 0,':');
	AfxExtractSubString(strMin,strSecondData, 1, ':');
	AfxExtractSubString(strSec,strSecondData, 2, ':');	



	CTime tmSynTime = CTime(_ttoi(strYear.GetBuffer(0)), 
		_ttoi(strMonth.GetBuffer(0)), 
		_ttoi(strDay.GetBuffer(0)), 
		_ttoi(strHour.GetBuffer(0)), 
		_ttoi(strMin.GetBuffer(0)), 
		_ttoi(strSec.GetBuffer(0)));

	CSNTPClient sntp;
	if( FALSE == sntp.SetSyncTime(tmSynTime) )
	{
		UM_WRITE_LOG(L"Time Synchronization Fail.")
	}

}

BOOL CTiorSaverTransferApp::ComparePolicy(POLICY_SET* pPolicy, BOOL _bIsFirstStart)
{
	CString strQuery = _T("");
	CString strCollect = _T("");
	CString strDomain = _T("");
	CString strIgnore = _T("");
	CString strBrowser = _T("");
	BOOL bStateResult = TRUE;
	BOOL bCollectResult = TRUE;
	BOOL bDomainResult = TRUE;
	BOOL bIsNeedShareWithCollectAgent = FALSE;
	BOOL bResult = TRUE;
	int nCnt = 0;

	if ((0 < pPolicy->arrCollect.GetSize()) || (0 < pPolicy->arrDomain.GetSize())/* || (0 < pPolicy->arrNotice.GetSize())*/) {
		//m_pFrame->m_Tray.ShowBalloon(_T("Policy change"), _T("정책 적용중..."));
	}
	if (m_bIsCollecting != pPolicy->bStart || _bIsFirstStart) {
		m_bIsCollecting = pPolicy->bStart;
		bIsNeedShareWithCollectAgent = TRUE;

		BOOL bQueryResult = FALSE;
		if (FALSE == pPolicy->bStart) {
			strQuery.Format(_T("update %s set state = %d where target_id = \"%s\""), DB_STATE_POLICY, AGENT_STATE_STOP, m_pMemberData->GetDeviceUUID());
			TerminateFileTransfer();
			if (nullptr == m_pCheckEachLogFileInLogFolderThread) {
				if (m_pCheckEachLogFileInLogFolderThread->m_bCheckEachLogFileInLogFolderThread)
					if (!TerminateFileTransfer())
						bStateResult = FALSE;
			}
			if (nullptr == m_pCheckEachNcviFileInLogFolderThread) {
				if (m_pCheckEachNcviFileInLogFolderThread->m_bCheckEachNcviFileInLogFolderThread)
					if (!TerminateFileTransfer())
						bStateResult &= FALSE;
			}
			if (nullptr == m_pCheckEachNkfiFileInLogFolderThread) {
				if (m_pCheckEachNkfiFileInLogFolderThread->m_bCheckEachNkfiFileInLogFolderThread)
					if (!TerminateFileTransfer())
						bStateResult &= FALSE;
			}
			if (m_pCheckPolicyThread) {
				if (FALSE == m_pCheckPolicyThread->m_bCheckPolicyThread) {
					delete m_pCheckPolicyThread;
					m_pCheckPolicyThread = NULL;
					m_pCheckPolicyThread = (CCheckPolicyThread*)AfxBeginThread(RUNTIME_CLASS(CCheckPolicyThread), THREAD_PRIORITY_ABOVE_NORMAL, 0,CREATE_SUSPENDED);
					m_pCheckPolicyThread->ResumeThread();
				}
			} else {
				m_pCheckPolicyThread = (CCheckPolicyThread*)AfxBeginThread(RUNTIME_CLASS(CCheckPolicyThread), THREAD_PRIORITY_ABOVE_NORMAL, 0,CREATE_SUSPENDED);
				m_pCheckPolicyThread->ResumeThread();
			}
		} else if (TRUE == pPolicy->bStart) {
			if (nullptr == m_pCheckEachLogFileInLogFolderThread) {
				m_pCheckEachLogFileInLogFolderThread = (CCheckEachLogFileInLogFolderThread*)AfxBeginThread(RUNTIME_CLASS(CCheckEachLogFileInLogFolderThread), THREAD_PRIORITY_ABOVE_NORMAL, 0,CREATE_SUSPENDED);
				m_pCheckEachLogFileInLogFolderThread->ResumeThread();
			} else {
				if (!m_pCheckEachLogFileInLogFolderThread->m_bCheckEachLogFileInLogFolderThread) {
					m_pCheckEachLogFileInLogFolderThread->ResumeThread();
				}
			}
			if (nullptr == m_pCheckEachNcviFileInLogFolderThread) {
				m_pCheckEachNcviFileInLogFolderThread = (CCheckEachNcviFileInLogFolderThread*)AfxBeginThread(RUNTIME_CLASS(CCheckEachNcviFileInLogFolderThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
				m_pCheckEachNcviFileInLogFolderThread->ResumeThread();
			}
			else {
				if (!m_pCheckEachNcviFileInLogFolderThread->m_bCheckEachNcviFileInLogFolderThread) {
					m_pCheckEachNcviFileInLogFolderThread->ResumeThread();
				}
			}
			if (nullptr == m_pCheckEachNkfiFileInLogFolderThread) {
				m_pCheckEachNkfiFileInLogFolderThread = (CCheckEachNkfiFileInLogFolderThread*)AfxBeginThread(RUNTIME_CLASS(CCheckEachNkfiFileInLogFolderThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
				m_pCheckEachNkfiFileInLogFolderThread->ResumeThread();
			}
			else {
				if (!m_pCheckEachNkfiFileInLogFolderThread->m_bCheckEachNkfiFileInLogFolderThread) {
					m_pCheckEachNkfiFileInLogFolderThread->ResumeThread();
				}
			}
			//if (nullptr == m_pCheckJPGFileInLogFolderThread) {
			//	m_pCheckJPGFileInLogFolderThread = (CCheckJPGFileInLogFolderThread*)AfxBeginThread(RUNTIME_CLASS(CCheckJPGFileInLogFolderThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
			//	m_pCheckJPGFileInLogFolderThread->ResumeThread();
			//}
			//else {
			//	if (!m_pCheckJPGFileInLogFolderThread->m_bCheckJPGFileInLogFolderThread) {
			//		m_pCheckJPGFileInLogFolderThread->ResumeThread();
			//	}
			//}

			if (m_pCheckEachLogFileInLogFolderThread) {
				if (!m_pCheckEachLogFileInLogFolderThread->m_bCheckEachLogFileInLogFolderThread)
					bStateResult = FALSE;
			} else {
				bStateResult = FALSE;
			}
			if (m_pCheckEachNcviFileInLogFolderThread) {
				if (!m_pCheckEachNcviFileInLogFolderThread->m_bCheckEachNcviFileInLogFolderThread)
					bStateResult &= FALSE;
			}
			if (m_pCheckEachNkfiFileInLogFolderThread) {
				if (!m_pCheckEachNkfiFileInLogFolderThread->m_bCheckEachNkfiFileInLogFolderThread)
					bStateResult &= FALSE;
			}
			else {
				bStateResult &= FALSE;
			}
			strQuery.Format(_T("update %s set state = %d where target_id = \"%s\""), DB_STATE_POLICY, AGENT_STATE_START, m_pMemberData->GetDeviceUUID());
		}
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->UpdateQuery(strQuery);
		}
#else
		CCmsDBManager dbManage(DB_STATE_POLICY);
		bQueryResult = dbManage.UpdateQuery(strQuery);
		dbManage.Free();
#endif
		if (bQueryResult) {
			bStateResult = TRUE;
		} else {
			bStateResult = FALSE;
		}
	}
	nCnt = 0;
	strQuery = _T("");
	strQuery.Format(_T("delete from %s where device_uuid = \"%s\""), DB_COLLECT_POLICY, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->DeleteQuery(strQuery);
	}
#else
	CCmsDBManager dbManageDelete(DB_COLLECT_POLICY);
	dbManageDelete.DeleteQuery(strQuery);
	dbManageDelete.Free();
#endif
	for (int i = 0; i < pPolicy->arrCollect.GetSize(); i++)
	{
		bIsNeedShareWithCollectAgent = TRUE;
		BOOL bQueryResult = FALSE;
		CStringArray arrSelected;
		CString strCollectType = _T("");
		arrSelected.RemoveAll();
		strCollectType = pPolicy->arrCollect.GetAt(i);
		strQuery = _T("");
		strQuery.Format(_T("select collect from %s where collect = %s and device_uuid = \"%s\""), DB_COLLECT_POLICY, strCollectType, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->SelectQuery(strQuery, &arrSelected, _T("collect"));
		}
#else
		CCmsDBManager dbManage(DB_COLLECT_POLICY);
		dbManage.SelectQuery(strQuery, &arrSelected, _T("collect"));
		dbManage.Free();
#endif
		if (arrSelected.GetSize() < 1) {
			strQuery = _T("");
			strQuery.Format(_T("insert into %s (device_uuid, collect) values (\"%s\", %s)"), DB_COLLECT_POLICY, m_pMemberData->GetDeviceUUID(), strCollectType);
			while (1)
			{
				if (QUERY_TRY_MAX_LIMIT < nCnt) {
					bCollectResult = FALSE;
					break;
				}
#ifdef _TIOR_TRANSFER_
				if (theApp.m_pDbSetting) {
					bQueryResult = theApp.m_pDbSetting->InsertQuery(strQuery);
				}
#else
				CCmsDBManager dbManage2(DB_COLLECT_POLICY);
				bQueryResult = dbManage2.InsertQuery(strQuery);
				dbManage2.Free();
#endif
				if (bQueryResult) {
					bCollectResult = TRUE;
					break;
				} else
					nCnt++;
			}
		}
		strCollect += (strCollectType + _T("|"));
	}
	if (0 != pPolicy->nAction) {
		int nAction = pPolicy->nAction;
		switch (nAction)
		{
		case AGENT_ACTION_UPDATE: break;
		case AGENT_ACTION_FORCE_UPDATE: break;
		case AGENT_ACTION_DELETE: break;
		}
	}

	nCnt = 0;
	strQuery = _T("");
	strQuery.Format(_T("delete from %s where device_uuid = \"%s\""), DB_URL_POLICY, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->DeleteQuery(strQuery);
	}
#else
	CCmsDBManager dbManageDomainDelete(DB_COLLECT_POLICY);
	dbManageDomainDelete.DeleteQuery(strQuery);
	dbManageDomainDelete.Free();
#endif
	for (int i = 0; i < pPolicy->arrDomain.GetSize(); i++)
	{
		bIsNeedShareWithCollectAgent = TRUE;
		BOOL bQueryResult = FALSE;
		{
			CStringArray arrSelected;
			strQuery = _T("");
			arrSelected.RemoveAll();
			strQuery.Format(_T("select domain from %s where domain = %s and device_uuid = \"%s\""), DB_URL_POLICY, pPolicy->arrDomain.GetAt(i), m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
			if (theApp.m_pDbSetting) {
				bQueryResult = theApp.m_pDbSetting->SelectQuery(strQuery, &arrSelected, _T("domain"));
			}
#else
			CCmsDBManager dbManage(DB_COLLECT_POLICY);
			dbManage.SelectQuery(strQuery, &arrSelected, _T("domain"));
			dbManage.Free();
#endif
			if (arrSelected.GetSize() < 1) {
				strQuery.Format(_T("insert into %s (device_uuid, domain) values (\"%s\", %s)"), DB_URL_POLICY, m_pMemberData->GetDeviceUUID(), pPolicy->arrDomain.GetAt(i));
				while (1)
				{
					if (QUERY_TRY_MAX_LIMIT < nCnt) {
						bDomainResult = FALSE;
						break;
					}
#ifdef _TIOR_TRANSFER_
					if (theApp.m_pDbSetting) {
						bQueryResult = theApp.m_pDbSetting->InsertQuery(strQuery);
					}
#else
					CCmsDBManager dbManage2(DB_URL_POLICY);
					bQueryResult = dbManage2.InsertQuery(strQuery);
					dbManage2.Free();
#endif
					if (bQueryResult) {
						bDomainResult = TRUE;
						break;
					} else
						nCnt++;
				}
			}
			strDomain += (pPolicy->arrDomain.GetAt(i) + _T("|"));
		}
	}

	for (int i = 0; i < pPolicy->arrIgnore.GetSize(); i++)
	{
		bIsNeedShareWithCollectAgent = TRUE;
		{
			strIgnore += (pPolicy->arrIgnore.GetAt(i) + _T("|"));
		}
	}
	for (int i = 0; i < pPolicy->arrBrowser.GetSize(); i++)
	{
		bIsNeedShareWithCollectAgent = TRUE;
		{
			strBrowser += (pPolicy->arrBrowser.GetAt(i) + _T("|"));
		}
	}

	BOOL bIsExistServerTime = FALSE;
	if (NULL != pPolicy->TimeSet) {
		if (!pPolicy->TimeSet->strTimeFromServer.IsEmpty()) {
			bIsNeedShareWithCollectAgent = TRUE;
			bIsExistServerTime = TRUE;
		}
	}

	if (TRUE == bIsNeedShareWithCollectAgent || TRUE == _bIsFirstStart) {
		BOOL bSysPipeSuccess = FALSE;
		BOOL bUsrPipeSuccess = FALSE;
		int nCnt = 0;
		while (FALSE == bSysPipeSuccess || FALSE == bUsrPipeSuccess)
		{
			if (10 < nCnt) {
				SE_MemoryDelete(m_pCSTOUCPipeClient);
				SE_MemoryDelete(m_pCSTOSCPipeClient);
				CString strSTOCSysPipeName = _T("");
				CString strSTOCUsrPipeName = _T("");
				strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
				strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, GetUsrAgentSessionID());

				m_pCSTOUCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
				m_pCSTOSCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
				break;
			}
			if (nullptr == m_pCSTOSCPipeClient || NULL == m_pCSTOUCPipeClient) {
				Sleep(100);
				continue;
			}
			strCollect.Remove(_T('\"'));
			strIgnore.Remove(_T('\"'));
			strBrowser.Remove(_T('\"'));
			SHARE_DATA_WITH_COLLECT_AGENT* pShareCollectData = new SHARE_DATA_WITH_COLLECT_AGENT;
			//CSTOCPipeClient STOCSysPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
			//CSTOCPipeClient STOCUsrPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
			pShareCollectData->dwAct = ACT_POLICY_CHANGE;
			pShareCollectData->bState = pPolicy->bStart;
			pShareCollectData->dwCollectDataSize = strCollect.GetLength() * sizeof(TCHAR);
			//memcpy(pShareCollectData->szCollectType, strCollect.GetBuffer(0), pShareCollectData->dwCollectDataSize);
					_tcscpy_s(pShareCollectData->szCollectType, strCollect.GetString());
			pShareCollectData->dwDomainSize = strDomain.GetLength() * sizeof(TCHAR);
			pShareCollectData->dwExtionsionToIgnore = strIgnore.GetLength() * sizeof(TCHAR);
			pShareCollectData->dwUrlProcNameSize = strBrowser.GetLength() * sizeof(TCHAR);
			//memcpy(pShareCollectData->szDomain, strDomain.GetBuffer(0), pShareCollectData->dwDomainSize);
					_tcscpy_s(pShareCollectData->szDomain, strDomain.GetString());
					_tcscpy_s(pShareCollectData->szExtionsionToIgnore, strIgnore.GetString());
					_tcscpy_s(pShareCollectData->szUrlProcName, strBrowser.GetString());
			if (bIsExistServerTime) {
					_tcscpy_s(pShareCollectData->szTimeOnServer, pPolicy->TimeSet->strTimeFromServer.GetString());
			}
			pShareCollectData->dwSplitSize = pPolicy->dwSplitSize;
			{
				CString strLog = _T("");
				strLog.Format(_T("[STOCPipeClient] bState: %d, szExtionsionToIgnore: %s, szCollectType: %s, szDomain: %s, szTimeOnServer: %s"),  pShareCollectData->bState, pShareCollectData->szExtionsionToIgnore, strCollect, strDomain, pShareCollectData->szTimeOnServer);
				UM_WRITE_LOG(strLog);
			}
			if (TRUE == m_pCSTOSCPipeClient->OnSetSendData(pShareCollectData)) {
				if (FALSE == m_pCSTOSCPipeClient->PipeClientStartUp()) {
					bResult = FALSE;
				} else {
					bSysPipeSuccess = TRUE;
				}
			} else {
				bResult = FALSE;
			}
			if (m_pCSTOSCPipeClient && m_pCSTOUCPipeClient) {
				if (TRUE == m_pCSTOUCPipeClient->OnSetSendData(pShareCollectData)) {
					if (FALSE == m_pCSTOUCPipeClient->PipeClientStartUp()) {
						bResult = FALSE;
					}
					else {
						bUsrPipeSuccess = TRUE;
					}
				}
				else {
					bResult = FALSE;
				}
			}
			SE_MemoryDelete(pShareCollectData);
			Sleep(1000);
			nCnt++;
		}

			if (TRUE == bResult && (bStateResult && bCollectResult && bDomainResult)) {
				//m_pFrame->m_Tray.ShowBalloon(_T("Policy change"), _T("정책 적용 완료!")); else {
				SendPolicyChanged();
			} else {
				//m_pFrame->m_Tray.ShowBalloon(_T("Policy change"), _T("정책 적용 실패!"));
			}
		}
	return bResult;
}

BOOL CTiorSaverTransferApp::SendPolicyChanged()
{
	BOOL bResult = FALSE;
	CParamParser parser;
	CString strParam = _T("");
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strToken = _T("");
	strParam = parser.CreateKeyValueRequest();
	strUrl.Format(_T("%s/v1/policy/apply"), theApp.m_pMemberData->GetServerHostURL(), strParam);
	int nErr = 0;
		strReturn = m_curl.CurlPost(strParam, strUrl, GetToken());
		{
			CString strLog = _T("");
			strLog.Format(_T("[CCheckLogSizeThread] %s %s[line: %d, function: %s, file: %s]"), strParam, strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
		INT nParseResult = 0;
		nParseResult = parser.ParserOnlyToken(strReturn, strToken);
	if (1 == nParseResult) {
		bResult = TRUE;
		SetToken(strToken);
	} else if (-1 == nParseResult) {
		// 2017-11-27 sy.choi
		m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
	} else {
		if (strToken.IsEmpty()) {
			strToken.Format(_T( "%d"), m_curl.GetCurlLastError());
		}
	}
	return bResult;
}

BOOL CTiorSaverTransferApp::SendInitScreenIdx()
{
	BOOL bResult = TRUE;
	CString strQuery = _T("");
	INT nScreenIdx = -1;
	CStringArray arrList;
	arrList.RemoveAll();
	strQuery.Format(_T("select init_screen_idx from %s where target_id = \"%s\""), DB_STATE_POLICY, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("init_screen_idx"));
	}
#else
	CCmsDBManager dbManage(DB_STATE_POLICY);
	dbManage.SelectQuery(strQuery, &arrList, _T("init_screen_idx"));
	dbManage.Free();
#endif
	if (0 < arrList.GetSize()) {
		nScreenIdx = _ttoi(arrList.GetAt(0));
	}
	if (-1 == nScreenIdx) {
		return TRUE;
	}

	BOOL bUsrPipeSuccess = FALSE;
	BOOL bSysPipeSuccess = FALSE;
	SHARE_DATA* pShareCollectData = new SHARE_DATA;
	pShareCollectData->dwAct = ACT_INITIAL_FILE_INDEX;
	pShareCollectData->dwMsg = nScreenIdx;
	{
		CString strLog = _T("");
		strLog.Format(_T("[STOCPipeName] dwMsg: %d"), pShareCollectData->dwMsg);
		UM_WRITE_LOG(strLog);
	}
	int nCnt = 0;
	while ((FALSE == bUsrPipeSuccess) || (FALSE == bSysPipeSuccess)) {
		if (10 < nCnt) {
			SE_MemoryDelete(m_pCSTOUCPipeClient);
			SE_MemoryDelete(m_pCSTOSCPipeClient);
			CString strSTOCSysPipeName = _T("");
			CString strSTOCUsrPipeName = _T("");
			strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
			strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, GetUsrAgentSessionID());

			m_pCSTOUCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
			m_pCSTOSCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
			break;
		}
		if (nullptr == m_pCSTOSCPipeClient || NULL == m_pCSTOUCPipeClient) {
			Sleep(100);
			continue;
		}
		if (TRUE == m_pCSTOSCPipeClient->OnSetSendData(pShareCollectData)) {
			if (FALSE == m_pCSTOSCPipeClient->PipeClientStartUp()) {
				bResult = FALSE;
			} else {
				bSysPipeSuccess = TRUE;
			}
		} else {
			bResult = FALSE;
		}
		if (m_pCSTOSCPipeClient && m_pCSTOUCPipeClient) {
			if (TRUE == m_pCSTOUCPipeClient->OnSetSendData(pShareCollectData)) {
				if (FALSE == m_pCSTOUCPipeClient->PipeClientStartUp()) {
					bResult = FALSE;
				}
				else {
					bUsrPipeSuccess = TRUE;
				}
			} else {
				bResult = FALSE;
			}
		}
		nCnt++;
		Sleep(100);
	}
	SE_MemoryDelete(pShareCollectData);
	if (bResult) {
		strQuery = _T("");
		strQuery.Format(_T("update %s set init_screen_idx = %d where target_id = \"%s\""), DB_STATE_POLICY, -1, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->UpdateQuery(strQuery);
		}
#else
		CCmsDBManager dbManage2(DB_STATE_POLICY);
		dbManage2.UpdateQuery(strQuery);
		dbManage2.Free();
#endif
	}
	return bResult;
}


BOOL CTiorSaverTransferApp::CompareVersion(CString _strCurrVersion, CString _strNewVersion)
{
	CStringArray arrCurrVersion;
	CStringArray arrNewVersion;
	arrCurrVersion.RemoveAll();
	arrNewVersion.RemoveAll();
	CParseUtil::ParseStringToStringArray(_T("."), _strCurrVersion, &arrCurrVersion);
	CParseUtil::ParseStringToStringArray(_T("."), _strNewVersion, &arrNewVersion);
	if (3 == arrCurrVersion.GetSize() && 3 == arrNewVersion.GetSize()) {
		for (int i = 0; i < 3; i++)
		{
			if (arrCurrVersion.GetAt(i) < arrNewVersion.GetAt(i))
				return TRUE;
		}
	}
	return FALSE;
}

BOOL CTiorSaverTransferApp::InitAgent()	// 2017-09-13 sy.choi 업데이트하는 부분 여기있음
{
	CString strParam = _T("");
	CString strReturn = _T("");
	CString strUrl = _T("");
	CString strToken = _T("");
	UINT nCurrentOffset = 0;
	int nDelay = 0;
	BOOL bResult = FALSE;

	CParamParser parse;
	strParam = parse.CreateKeyValueRequest();
	strUrl.Format(_T("%s/v1/init?%s"), theApp.m_pMemberData->GetServerHostURL(), strParam);

	int nErr = 0;
	//if (SEND_TYPE_CURL == theApp.m_nSendType) {
	POLICY_SET* pPolicySet = new POLICY_SET;
	BOOL bRequestResult = FALSE;
	while (TRUE) {
		strReturn = m_curl.CurlGET(strUrl, GetToken());
		bRequestResult = parse.ParserPolicySet(strReturn, strToken, pPolicySet);
		if (1 == bRequestResult) {
			break;
		} else if (-1 == bRequestResult) {
			theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
			break;
		} else {
			Sleep(1000);
		}
	}
	/*} else if (SEND_TYPE_HTTPSTCONNECT == theApp.m_nSendType) {
		strReturn = theApp.HttpsGET(theApp.GetToken(), strUrl);
	}*/
	if (1 == bRequestResult) {
		theApp.SetToken(strToken);
		if (!DirectoryExists(CPathInfo::GetLogFilePath())) {
			SendInitScreenIdx();
		}
		if (NULL != pPolicySet) {
			if (pPolicySet->bIsPolicyExist) {
				theApp.ChangePolicy(pPolicySet, TRUE);
			}
			if (0 < theApp.m_nDeleteDelay && !theApp.m_strDeleteDate.IsEmpty()) {
				theApp.StartDeleteOldFiles();
			}
			if (NULL != pPolicySet->UpdateSet) {
				UpdateStart(pPolicySet->UpdateSet);
			}
		}
		bResult = TRUE;
	} else {
		if (strToken.IsEmpty()) {
			strToken.Format(_T("%d"), nErr);
			//MessageBox(NULL, strToken, _T("TiorSaver"), MB_OK);
		}
		bResult = FALSE;
	}
	SE_MemoryDelete(pPolicySet);
	return bResult;
}

void CTiorSaverTransferApp::Write_Index(CString _strPath, CString _strHash, CString _strVersion)
{
	BOOL bRet ;
	CString strIni_Path=L"", strIndex, strSection = _T("");
	strSection.Format( L"%s", ExtractFileName(_strPath));
	strSection.Remove(_T('\"'));
	strIni_Path.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("UDownload\\TSVersion.ini"));
	bRet = WritePrivateProfileString( strSection, _T("path"), _strPath.GetBuffer(0) ,strIni_Path);
	bRet = WritePrivateProfileString( strSection, _T("hash"), _strHash.GetBuffer(0) ,strIni_Path);
	bRet = WritePrivateProfileString( strSection, _T("ver"), _strVersion.GetBuffer(0) ,strIni_Path);
	if( !bRet )
	{
		//필요시 로그를 남길것
	}
}

BOOL CTiorSaverTransferApp::CallUpdate(CString _strVersion)
{
	BOOL bResult = FALSE;
	CString strAgentUpdate = _T("");
	CString strLog = _T("");
	CString strDownloadMutex = _T("");

	m_strNewVersion = _strVersion;
	strAgentUpdate.Format(_T("%s%s -v %s"), CPathInfo::GetClientInstallPath(), WTIOR_DOWNLOAD_AGENT_NAME, m_strNewVersion);
	strDownloadMutex.Format(_T("%s"), WTIOR_DOWNLOAD_AGENT_MUTEX_NAME);

	if(false == CProcess::IsExistMutex(strDownloadMutex))	{
		CImpersonator imp;
		BOOL bExecute = imp.CreateProcess(strAgentUpdate, SW_HIDE);
		imp.Free();
		if( FALSE == bExecute)
		{
			strLog.Format(_T("[update] CreateProcess Fail..%d"), GetLastError());
			//MessageBox(NULL, strLog, _T("TiorSaver"), MB_OK);
			return FALSE;
		}
		else
			return TRUE;
	}
	return bResult;
}

void CTiorSaverTransferApp::SetUpdateCompleteOnDB()
{
	BOOL bQueryResult = FALSE;
	int nCnt = 0;
	CString strQuery = _T("");
	strQuery.Format(_T("update %s set ver = \"%s\" where device_uuid = \"%s\""), DB_AGENT_INFO, m_strNewVersion, m_pMemberData->GetDeviceUUID());
	while (FALSE == bQueryResult)
	{ 
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			bQueryResult = theApp.m_pDbSetting->UpdateQuery(strQuery);
		}
#else
		CCmsDBManager dbManage(DB_AGENT_INFO);
		bQueryResult = dbManage.UpdateQuery(strQuery);
		dbManage2.Free();
#endif
		if (10 == nCnt++)
			break;
	}
}

BOOL CTiorSaverTransferApp::ChangePolicy(POLICY_SET* pPolicySet, BOOL _bIsFirstStart)
{
	if (TRUE == m_bChangingPolicy) {
		return FALSE;
	}
	m_bChangingPolicy = TRUE;
	BOOL bResult = FALSE;
	if (NULL != pPolicySet) {
		if (pPolicySet->bIsPolicyExist) {
			if (TRUE == pPolicySet->bDelete) {	// 2017-09-14 sy.choi 삭제가 넘어왔을 경우
				StartUninstall();
			}
			if (TRUE == pPolicySet->bNotice) {
				CTime cTime = CTime::GetCurrentTime();
				CString strToday = _T("");
				strToday.Format(_T("%s"), cTime.Format(_T("%Y-%m-%d")));
				CString strQuery = _T("");
				CStringArray arrList;
				arrList.RemoveAll();
				strQuery.Format(_T("select notice from %s where notice like '%s%%'"), DB_NOTICE, strToday);
#ifdef _TIOR_TRANSFER_
				if (m_pDbSetting) {
					m_pDbSetting->SelectQuery(strQuery, &arrList, _T("notice"));
				}
#else
				CCmsDBManager dbSelectManage(DB_NOTICE);
				dbSelectManage.SelectQuery(strQuery, &arrList, _T("notice"));
				dbSelectManage.Free();
#endif
				if (0 >= arrList.GetSize()) {
					strQuery.Format(_T("insert or replace into %s (notice) values ( \"%s\")"), DB_NOTICE, strToday);
#ifdef _TIOR_TRANSFER_
					if (m_pDbSetting) {
						m_pDbSetting->InsertQuery(strQuery);
					}
#else
					CCmsDBManager dbManage(DB_NOTICE);
					dbManage.InsertQuery(strQuery);
					dbManage.Free();
#endif
					StartTray();
				}

			}
			if (_bIsFirstStart) {
				bResult = ComparePolicy(pPolicySet, TRUE);
			} else {
				bResult = ComparePolicy(pPolicySet);
			}
			if (!bResult) {
				m_bChangingPolicy = FALSE;
				return FALSE;
			}
		} else {
			m_bChangingPolicy = FALSE;
			return FALSE;
		}
	} else {
		m_bChangingPolicy = FALSE;
		return FALSE;
	}
	m_bChangingPolicy = FALSE;
	return bResult;
}

void CTiorSaverTransferApp::StartUninstall()
{
	if (m_bUninstallStart) {
		return;
	}
	m_bUninstallStart = TRUE;
	HANDLE hThread;
	DWORD dwUIThreadID = 0;

	hThread= ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)StartUninstallThread, this, 0, &dwUIThreadID);
	CloseHandle(hThread);
}

BOOL WINAPI StartUninstallThread(LPVOID lpParam)
{
	CTiorSaverTransferApp* pWnd = reinterpret_cast<CTiorSaverTransferApp*>(lpParam);

	//BOOL bResult = FALSE;
	CString strLog = _T("");

	if (NULL != pWnd->m_pCheckPolicyThread) {
		delete pWnd->m_pCheckPolicyThread;
		pWnd->m_pCheckPolicyThread = NULL;
	}

	pWnd->TerminateFileTransfer();
	theApp.m_pWnd->PostMessage(MSG_DELETE_START, NULL, NULL);	
	//return bResult;
	return TRUE;
}

void CTiorSaverTransferApp::StartTray()
{
	CProcess proc;
	CString strTrayName = _T("");
	CString strTrayMutexName = _T("");
	HMODULE hMod = LoadLibrary(_T("kernel32.dll"));
	proc.m_pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(hMod, "CreateToolhelp32Snapshot");
	proc.m_pProcess32First = (fpProcess32First) ::GetProcAddress(hMod, PROCESS32FIRST);
	proc.m_pProcess32Next  = (fpProcess32Next) ::GetProcAddress(hMod, PROCESS32NEXT);
	if(proc.IsWindowLogin()) {
		if (hMod != NULL) {
			strTrayName = CPathInfo::GetClientInstallPath() + WTIOR_TRAY_NAME;
			strTrayMutexName.Format(_T("%s"), WTIOR_TRAY_MUTEX_NAME);
			proc.CheckTiorProcess(strTrayName, strTrayMutexName);  //hhh: test
		}
	}
	FreeLibrary(hMod);
}

void CTiorSaverTransferApp::StartDeleteOldFiles()
{
	if (TRUE == m_bDeleteOldFilesStart) {
		return;
	}
	HANDLE hThread;
	DWORD dwUIThreadID = 0;

	hThread= ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)StartDeleteOldFilesThread, this, 0, &dwUIThreadID);
	CloseHandle(hThread);
}

BOOL WINAPI StartDeleteOldFilesThread(LPVOID lpParam)
{
	CTiorSaverTransferApp* pWnd = reinterpret_cast<CTiorSaverTransferApp*>(lpParam);
	pWnd->m_bDeleteOldFilesStart = TRUE;
	CString strQuery = _T("");
	CStringArray arrList;
	CStringArray arrScreen;
	arrList.RemoveAll();
	arrScreen.RemoveAll();
	Sleep(pWnd->m_nDeleteDelay * 1000);
	strQuery.Format(_T("select full_path from %s where last_write_date <= '%s'"), DB_SEND_LOG_LIST, theApp.m_strDeleteDate);
#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbLog) {
		theApp.m_pDbLog->SelectQuery(strQuery, &arrList, _T("full_path"));
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManageSelect(DB_SEND_LOG_LIST);
	dbManageSelect.SelectQuery(strQuery, &arrList, _T("full_path"));
	dbManageSelect.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	strQuery.Format(_T("select full_path from %s where last_write_date <= '%s'"), DB_SEND_NCVI_LIST, theApp.m_strDeleteDate);
#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbNcvi) {
		theApp.m_pDbNcvi->SelectQuery(strQuery, &arrScreen, _T("full_path"));
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManageNcviSelect(DB_SEND_NCVI_LIST);
	dbManageNcviSelect.SelectQuery(strQuery, &arrScreen, _T("full_path"));
	dbManageNcviSelect.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	arrList.Append(arrScreen);
	arrScreen.RemoveAll();
	strQuery.Format(_T("select full_path from %s where last_write_date <= '%s'"), DB_SEND_NKFI_LIST, theApp.m_strDeleteDate);
#ifdef _MGCHOI_TEST_
	if (theApp.m_pDbNkfi) {
		theApp.m_pDbNkfi->SelectQuery(strQuery, &arrScreen, _T("full_path"));
	}
#else	//#ifdef _MGCHOI_TEST_
	CCmsDBManager dbManageScrSelect(DB_SEND_NKFI_LIST);
	dbManageScrSelect.SelectQuery(strQuery, &arrScreen, _T("full_path"));
	dbManageScrSelect.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
	arrList.Append(arrScreen);
	for (int i = 0; i < arrList.GetSize(); i++) {
		BOOL bResult = ForceDeleteFile(arrList.GetAt(i));
		::MoveFileEx(arrList.GetAt(i), NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
		if (bResult) {
			strQuery.Format(_T("delete from %s where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, arrList.GetAt(i));
#ifdef _MGCHOI_TEST_
			if (theApp.m_pDbLog) {
				theApp.m_pDbLog->DeleteQuery(strQuery);
			}
#else	//#ifdef _MGCHOI_TEST_
			CCmsDBManager dbManage(DB_SEND_LOG_LIST);
			dbManage.DeleteQuery(strQuery);
			dbManage.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
		}
	}
	theApp.m_strDeleteDate = _T("");
	pWnd->m_bDeleteOldFilesStart = FALSE;
	return TRUE;
}
BOOL CTiorSaverTransferApp::PipeServerTime()
{
	if (m_curl.m_strTimeFromServer.IsEmpty())
		return FALSE;
	if (m_bIsPipeStart) {
		return FALSE;
	}
	HANDLE hThread;
	DWORD dwUIThreadID = 0;
	hThread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PipeServerTimeThread, this, 0, &dwUIThreadID);
	CloseHandle(hThread);
	return TRUE;
}

BOOL WINAPI PipeServerTimeThread(LPVOID lpParam)
{
	CTiorSaverTransferApp* pWnd = reinterpret_cast<CTiorSaverTransferApp*>(lpParam);
	pWnd->m_bIsPipeStart = TRUE;
	BOOL bUsrPipeSuccess = FALSE;
	BOOL bSysPipeSuccess = FALSE;
	int nCnt = 0;
	while ((FALSE == bUsrPipeSuccess) || (FALSE == bSysPipeSuccess))
	{
		BOOL bResult = TRUE;
		SHARE_DATA_WITH_COLLECT_AGENT* pShareCollectData = new SHARE_DATA_WITH_COLLECT_AGENT;
		pShareCollectData->dwAct = ACT_SYNC_SERVER_TIME;
		_tcscpy_s(pShareCollectData->szTimeOnServer, pWnd->m_curl.m_strTimeFromServer.GetString());
		{
			CString strLog = _T("");
			strLog.Format(_T("[CSTOCPipeClient] szTimeOnServer: %s"), pShareCollectData->szTimeOnServer);
			OutputDebugString(strLog);
		}
		if (3 < nCnt) {
			SE_MemoryDelete(pWnd->m_pCSTOUCPipeClient);
			SE_MemoryDelete(pWnd->m_pCSTOSCPipeClient);
			CString strSTOCSysPipeName = _T("");
			CString strSTOCUsrPipeName = _T("");
			strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
			strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, pWnd->GetUsrAgentSessionID());

			pWnd->m_pCSTOUCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
			pWnd->m_pCSTOSCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
		}
		if (nullptr == pWnd->m_pCSTOSCPipeClient || NULL == pWnd->m_pCSTOUCPipeClient) {
			Sleep(100);
			continue;
		}
		if (TRUE == pWnd->m_pCSTOSCPipeClient->OnSetSendData(pShareCollectData)) {
			if (FALSE == pWnd->m_pCSTOSCPipeClient->PipeClientStartUp()) {
				bResult = FALSE;
			} else {
				bSysPipeSuccess = TRUE;
			}
		} else {
			bResult = FALSE;
		}
		if (pWnd->m_pCSTOSCPipeClient && pWnd->m_pCSTOUCPipeClient) {
			if (TRUE == pWnd->m_pCSTOUCPipeClient->OnSetSendData(pShareCollectData)) {
				if (FALSE == pWnd->m_pCSTOUCPipeClient->PipeClientStartUp()) {
					bResult = FALSE;
				}
				else {
					bUsrPipeSuccess = TRUE;
				}
			}
			else {
				bResult = FALSE;
			}
		}
		Sleep(1000);
		if (bResult) {
			{
				CString strLog = _T("");
				strLog.Format(_T("[CSTOCPipeClient] pipe success stoc"));
				UM_WRITE_LOG(strLog);
			}
		} else {
			{
				CString strLog = _T("");
				strLog.Format(_T("[CSTOCPipeClient] pipe fail stoc"));
				UM_WRITE_LOG(strLog);
			}
			nCnt++;
		}
		pWnd->m_curl.m_strTimeFromServer.Format(_T(""));
		SE_MemoryDelete(pShareCollectData);
	}
	pWnd->m_bIsPipeStart = FALSE;
	return TRUE;
}

void CTiorSaverTransferApp::DeleteMyself(CString _strPath)
{
	CString strFilePath = _T("");

	CFileFind finder;

	BOOL bWorking = finder.FindFile(_strPath + _T("\\*.*"));

	while (bWorking)
	{
		bWorking = finder.FindNextFile();

		if (finder.IsDots())
			continue;

		if (finder.IsDirectory())
		{
			strFilePath = finder.GetFilePath();
			DeleteMyself(strFilePath);
		}
		else
		{
			strFilePath = finder.GetFilePath();
			MoveFileEx(strFilePath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);

			ReleaseFileStatus(strFilePath, FILE_ATTRIBUTE_READONLY);

		}
	}

	MoveFileEx(_strPath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
}

BOOL CTiorSaverTransferApp::ReleaseFileStatus(CString strFile, BYTE Attribute)
{
	CFile cfile;
	CFileStatus status;
	if (cfile.GetStatus(strFile, status))
	{
		if (status.m_attribute & Attribute)
		{
			status.m_attribute &= ~Attribute;
			CFile::SetStatus(strFile, status);
		}
		else
			return FALSE;
	}
	else
		return FALSE;

	return TRUE;
}

BOOL CTiorSaverTransferApp::MoveFile()
{
	CString strRootDir = _T("");
	strRootDir = CPathInfo::GetClientInstallPath();
	DWORD dwErrorCode = 0;
	DWORD dwError = 0;
	CString strCfgPath = _T("");
	CString strDBPath = _T("");
	CString strLogPath = _T("");
	CString strOffsetPath = _T("");
	CString strUpdatePath = _T("");
	CString strBackupPath = _T("");
	CString strDownloadPath = _T("");
	strCfgPath.Format(_T("%scfg"), strRootDir);
	strDBPath.Format(_T("%sDB"), strRootDir);
	strLogPath.Format(_T("%slog"), strRootDir);
	strOffsetPath.Format(_T("%soffset"), strRootDir);
	strUpdatePath.Format(_T("%sUpdate"), strRootDir);
	strDownloadPath.Format(_T("%sUDownload"), strRootDir);
	strBackupPath.Format(_T("%sUBackup"), strRootDir);


	CString strCmd = _T("");
	strCmd.Format(_T("taskkill /F /T /IM TiorSaver*\r\n"));
	WinExec((LPCSTR)(LPCTSTR)strCmd, SW_HIDE);

	UM_WRITE_LOG(_T("MoveFile() Start..1"))
	ForceDeleteDir(strCfgPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strDBPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strLogPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strOffsetPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strUpdatePath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strDownloadPath, &dwErrorCode);
	dwError += dwErrorCode;
	ForceDeleteDir(strBackupPath, &dwErrorCode);
	dwError += dwErrorCode;
	UM_WRITE_LOG(_T("MoveFile() Start..2"))
	DeleteMyself(strRootDir);
	UM_WRITE_LOG(_T("MoveFile() Start..3"))
	ForceDeleteDir(strRootDir, &dwErrorCode);
	dwError += dwErrorCode;
	UM_WRITE_LOG(_T("MoveFile() Start..4"));

	CFile unFile;
	CString strFile = CPathInfo::GetTransferLogFilePath() + _T("TiorSaver_Unsuccess.cms");
	if (unFile.Open(strFile, CFile::modeCreate | CFile::modeWrite)) {
		unFile.Close();
	}
	::MoveFileEx(strFile, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	::MoveFileEx(strCfgPath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	::MoveFileEx(strDBPath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	::MoveFileEx(strOffsetPath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);		//만약 삭제하지 못했다면 재부팅시 삭제되도록 한다.
	::MoveFileEx(strUpdatePath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);		//만약 삭제하지 못했다면 재부팅시 삭제되도록 한다.
	::MoveFileEx(strDownloadPath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);		//만약 삭제하지 못했다면 재부팅시 삭제되도록 한다.
	::MoveFileEx(strBackupPath, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);		//만약 삭제하지 못했다면 재부팅시 삭제되도록 한다.
	::MoveFileEx(strRootDir, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);

	if (dwError != 0) {
		return FALSE;
	} else {
		return TRUE;
	}
}

INT CTiorSaverTransferApp::GetUsrAgentSessionID()
{
	CWTSSession wts;
	INT nCurrentSessionID = 0;
	nCurrentSessionID = wts.GetCurrentLogonSessionID();
	return nCurrentSessionID;
}

BOOL CTiorSaverTransferApp::AddColumn(CString _strTableName, CString _strColumnName, CString _strDataType)
{
	CString strQuery = L"", strLog;
	strQuery.Format(L"select sql from sqlite_master WHERE name='%s' AND sql LIKE '%%%s%%'", _strTableName, _strColumnName);
	//strQuery = L"select sql from sqlite_master WHERE name='ltb_config' AND sql LIKE '%updateVersion%'";

	CCmsDBManager* dbManage = NULL;
	dbManage = new CCmsDBManager(_strTableName);
	//	//dbManage = &dbManage_Log;
	//}

	CStringArray arrField;
	arrField.RemoveAll();

	//	CCmsDBManager dbManage(DB_CONFIG);
	dbManage->SelectQuery(strQuery, &arrField, L"sql");
	dbManage->Free();
	if (dbManage)
		delete dbManage;

	int nTryCnt;
	//필드가 미존재시 
	if (arrField.GetSize() < 1) {
		//strQuery = L"ALTER TABLE ltb_config ADD COLUMN 'updateVersion' varchar(260)" ;
		strQuery.Format(L"ALTER TABLE %s ADD COLUMN '%s' %s default %d", _strTableName, _strColumnName, _strDataType, 0);
		strLog.Format(L"[AddSendResultColumn] Add Column query: %s", strQuery);
		OutputDebugString(strLog);
		nTryCnt = 0;

		while (TRUE)
		{
			if (nTryCnt > 5) {
				strLog.Format(L"Add Column Fail- table name:%s, column name:%s", _strTableName, _strColumnName);
				OutputDebugString(strLog);
				return FALSE;
			}

			CCmsDBManager* dbManage2 = NULL;
			dbManage2 = new CCmsDBManager(_strTableName);
			BOOL bResult = dbManage2->ExecQuery(strQuery);
			dbManage2->Free();
			if (dbManage2)
				delete dbManage2;
			if (bResult) {
				return TRUE;
			}
			else {
				Sleep(500);
				nTryCnt++;
			}
		}
	}
	return TRUE;
}
BOOL CTiorSaverTransferApp::PipeThumnailAction(BOOL _bStart)
{
	BOOL bResult = FALSE;
	INT nCnt = 0;
	CString strQuery = _T("");
	CStringArray arrList;
	arrList.RemoveAll();
	strQuery.Format(_T("select thumnail_start from %s where target_id = \"%s\""), DB_STATE_POLICY, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (m_pDbSetting) {
		m_pDbSetting->SelectQuery(strQuery, &arrList, _T("thumnail_start"));
	}
#else
	CCmsDBManager dbManageThumSelect(DB_STATE_POLICY);
	dbManageThumSelect.SelectQuery(strQuery, &arrList, _T("thumnail_start"));
	dbManageThumSelect.Free();
#endif
	if (0 < arrList.GetSize()) {
		if (_bStart == arrList.GetAt(0))
			return TRUE;
	}
	SHARE_DATA_WITH_COLLECT_AGENT sendData;
	if (_bStart) {
		sendData.dwAct = ACT_THUMBNAIL_START;
	} else {
		sendData.dwAct = ACT_THUMBNAIL_STOP;
	}
	{
		CString strLog = _T("");
		strLog.Format(_T("[PipeThumnailAction] dwAct: %d"), sendData.dwAct);
		UM_WRITE_LOG(strLog);
	}
	while (TRUE) {
		if (bResult)
			break;
		if (10 < nCnt) {
			SE_MemoryDelete(m_pCSTOUCPipeClient);
			SE_MemoryDelete(m_pCSTOSCPipeClient);
			CString strSTOCSysPipeName = _T("");
			CString strSTOCUsrPipeName = _T("");
			strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
			strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, GetUsrAgentSessionID());

			m_pCSTOUCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
			m_pCSTOSCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
			break;
		}
		if (nullptr == m_pCSTOSCPipeClient || NULL == m_pCSTOUCPipeClient) {
			Sleep(100);
			continue;
		}
		if (theApp.m_pCSTOSCPipeClient->OnSetSendData(&sendData) == TRUE) {
			if (!theApp.m_pCSTOSCPipeClient->PipeClientStartUp()) {
				bResult = FALSE;
			} else {
				bResult = TRUE;
			}
		}
		else {
			bResult = FALSE;
		}
		if (theApp.m_pCSTOSCPipeClient && theApp.m_pCSTOUCPipeClient) {
			if (theApp.m_pCSTOUCPipeClient->OnSetSendData(&sendData) == TRUE) {
				if (!theApp.m_pCSTOUCPipeClient->PipeClientStartUp()) {
					bResult = FALSE;
				}
				else {
					bResult = TRUE;
				}
			}
			else {
				bResult = FALSE;
			}
		}
	}
	strQuery = _T("");
	strQuery.Format(_T("update %s set thumnail_start = %d where target_id = \"%s\""), DB_STATE_POLICY, _bStart, m_pMemberData->GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (m_pDbSetting) {
		m_pDbSetting->UpdateQuery(strQuery);
	}
#else
	CCmsDBManager dbManageThumUpdate(DB_STATE_POLICY);
	dbManageThumUpdate.UpdateQuery(strQuery);
	dbManageThumUpdate.Free();
#endif
	return bResult;
}

BOOL CTiorSaverTransferApp::PolicyRequest(POLICY_SET* pPolicySet)
{
	BOOL bResult = FALSE;
	CParamParser parser;
	CString strParam = _T("");
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strToken = _T("");
	strParam = parser.CreateKeyValueRequest();
	//strUrl.Format(_T("http://192.168.100.112/v1/policy?%s"), strParam);
	strUrl.Format(_T("%s/v1/policy?%s"), theApp.m_pMemberData->GetServerHostURL(), strParam);
	int nErr = 0;
	/*if (theApp)*/ {
		strReturn = theApp.m_curl.CurlGET(strUrl, theApp.GetToken());
		INT nParseResult = 0;
		nParseResult = parser.ParserPolicySet(strReturn, strToken, pPolicySet);
		if (1 == nParseResult) {
			bResult = TRUE;
			theApp.SetToken(strToken);
		}
		else if (-1 == nParseResult) {
			// 2017-11-27 sy.choi
			theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
		}
		else {
			if (strToken.IsEmpty()) {
				if (0 == nErr) {
					strToken.Format(_T("%s"), strReturn);
				}
				else {
					strToken.Format(_T("%d"), nErr);
				}
			}
			//MessageBox(NULL, strToken, _T("TiorSaver"), MB_OK);
		}
	}
	return bResult;
}

BOOL CTiorSaverTransferApp::UpdateStart(UPDATE_SET* _pUpdateSet)
{
	CString strNewVer = _T("");
	CString strIni_Path = _T("");
	CString strCurVersion = _T("");
	strCurVersion = m_pMemberData->GetVersion();

	if (/*0 != pPolicySet->UpdateSet->strNewVersion.CompareNoCase(strCurVersion)*/CompareVersion(strCurVersion, _pUpdateSet->strNewVersion)) {
		strIni_Path.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), _T("UDownload\\TSVersion.ini"));
		//다운받을 폴더가 존재하는지 체크
		int iPos = strIni_Path.ReverseFind(_T('\\'));
		CString sDownPath = strIni_Path.Left(iPos);
		if (FALSE == FileExists(sDownPath)) {
			if (FALSE == ForceCreateDir(sDownPath)) {

				CString sLog;
				sLog.Format(_T("Fail(ForceCreateDir) - Filename : %s"), sDownPath);
				//UM_WRITE_LOG(sLog);
				return FALSE;
			}
		}
		else if (FileExists(strIni_Path)) {
			ForceDeleteFile(strIni_Path);
		}
		strNewVer = _pUpdateSet->strNewVersion.GetBuffer(0);
		WritePrivateProfileString(STR_CMS_VER_SECTION, STR_CMS_VER_SECTION, _pUpdateSet->strNewVersion.GetBuffer(0), strIni_Path);
		for (int i = 0; i < _pUpdateSet->arrPath.GetSize(); i++)
		{
			CString strUpdatePath = _pUpdateSet->arrPath.GetAt(i);
			CString strUpdateHash = _pUpdateSet->arrHash.GetAt(i);
			CString strUpdateVersion = _T("");
			Write_Index(strUpdatePath, strUpdateHash, strUpdateVersion);
		}
		CallUpdate(_pUpdateSet->strNewVersion);
	}
}

BOOL WINAPI PipeDeleteFileRequestThread(LPVOID lpParam)
{
	CTiorSaverTransferApp* pWnd = reinterpret_cast<CTiorSaverTransferApp*>(lpParam);
	pWnd->m_bIsDeleteRequestPipeStart = TRUE;
	BOOL bResult = TRUE;
	int nCnt = 0;
	/*if (theApp)*/ {
		for (int i = 0; i < theApp.m_arrDeleteList.GetSize(); i++)
		{
			CString strFile = _T("");
			strFile = theApp.m_arrDeleteList.GetAt(i);
			BOOL bUsrPipeSuccess = FALSE;
			while (FALSE == bUsrPipeSuccess)
			{
				{
					CString strLog = _T("");
					strLog.Format(_T("[PipeDeleteFileRequestThread] %s"), strFile);
					UM_WRITE_LOG(strLog);
				}
				CString strMaxIdx = _T("");
				CStringArray arrCurData;
				CStringArray arrCurMaxData;
				arrCurData.RemoveAll();
				arrCurMaxData.RemoveAll();
				if (nullptr == pWnd->m_pCheckEachLogFileInLogFolderThread) {
					pWnd->m_bIsDeleteRequestPipeStart = FALSE;
					return TRUE;
				}
				else
					strMaxIdx = pWnd->m_pCheckEachLogFileInLogFolderThread->m_strMaxIdx;
				if (strMaxIdx.IsEmpty()) {
					break;
				}
				CParseUtil::ParseStringToStringArray(_T("_"), ExtractBaseFileName(strFile), &arrCurData);
				CParseUtil::ParseStringToStringArray(_T("_"), strMaxIdx, &arrCurMaxData);
				if (_ttoi(arrCurMaxData.GetAt(1)) < _ttoi(arrCurData.GetAt(2))) {
					break;
				}
				SHARE_DATA_WITH_COLLECT_AGENT* pShareCollectData = new SHARE_DATA_WITH_COLLECT_AGENT;
				pShareCollectData->dwAct = ACT_DELETE_FILE;
				pShareCollectData->dwDeleteFileSize = strFile.GetLength() * sizeof(TCHAR);
				_tcscpy_s(pShareCollectData->szDeleteFile, strFile.GetString());
				{
					CString strLog = _T("");
					strLog.Format(_T("[STOCPipeClient] dwAct: %d, dwDeleteFileSize: %d, szDeleteFile: %s"), pShareCollectData->dwAct, pShareCollectData->dwDeleteFileSize, pShareCollectData->szDeleteFile);
					UM_WRITE_LOG(strLog);
				}
				if (3 < nCnt) {
					SE_MemoryDelete(pWnd->m_pCSTOUCPipeClient);
					CString strSTOCUsrPipeName = _T("");
					strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, pWnd->GetUsrAgentSessionID());

					pWnd->m_pCSTOUCPipeClient = new CSTOCPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
					break;
				}
				if (NULL == pWnd->m_pCSTOUCPipeClient) {
					Sleep(100);
					continue;
				}
				if (pWnd->m_pCSTOUCPipeClient) {
					if (TRUE == pWnd->m_pCSTOUCPipeClient->OnSetSendData(pShareCollectData)) {
						if (FALSE == pWnd->m_pCSTOUCPipeClient->PipeClientStartUp()) {
							bResult = FALSE;
						}
						else {
							bUsrPipeSuccess = TRUE;
						}
					}
					else {
						bResult = FALSE;
					}
				}
				Sleep(1000);
				if (bResult) {
					{
						CString strLog = _T("");
						strLog.Format(_T("[CSTOUPipeClient] pipe success stoc"));
						//UM_WRITE_LOG(strLog);
					}
				}
				else {
					{
						CString strLog = _T("");
						strLog.Format(_T("[CSTOUPipeClient] pipe fail stoc"));
						//UM_WRITE_LOG(strLog);
					}
				}
				SE_MemoryDelete(pShareCollectData);
			}
			if (FileExists(strFile)) {
				ForceDeleteFile(strFile);
			}
		}
		theApp.m_arrDeleteList.RemoveAll();
	}
	pWnd->m_bIsDeleteRequestPipeStart = FALSE;
	return TRUE;
}

BOOL CTiorSaverTransferApp::PipeDeleteFileRequest()
{
	if (m_bIsDeleteRequestPipeStart) {
		return FALSE;
	}
	HANDLE hThread;
	DWORD dwUIThreadID = 0;
	hThread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PipeDeleteFileRequestThread, this, 0, &dwUIThreadID);
	CloseHandle(hThread);
	return TRUE;
}

BOOL CTiorSaverTransferApp::AddDeleteFileRequestArray(CStringArray& _arrFilePath)
{
	if (nullptr != m_pCheckEachLogFileInLogFolderThread) {
		theApp.m_arrDeleteList.Append(_arrFilePath);
		//CString strMaxIdx = _T("");
		//for (int i = 0; i < _arrFilePath.GetSize(); i++) {
		//	strMaxIdx = m_pCheckEachLogFileInLogFolderThread->m_strMaxIdx;
		//	if (-1 != _arrFilePath.GetAt(i).Find(strMaxIdx) || strMaxIdx.IsEmpty()) {
		//		return  TRUE;	//지금 만들고 있는 파일은 제외
		//	}
		//	theApp.m_arrDeleteList.Add(_arrFilePath.GetAt(i));
		//}
	}
	return TRUE;
}

BOOL CTiorSaverTransferApp::RegisterStartProgram()
{
	HKEY hkey;
	CString strKeyName, strFileName;
	CString strKeyPath = _T("");
	CString strFilePath = _T("");

	// 레지스트리에 등록하고자 하는 키 이름
	strKeyName = _T("TiorSaver");
	strKeyPath = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
	strFilePath = CPathInfo::GetClientInstallPath() + WTIOR_TRANS_AGENT_NAME;

	// 해당 경로의 레지스트리를 open 한다.
	LONG reg = RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		strKeyPath.GetString(), 0L,KEY_WRITE,&hkey);

		// 레지스트리를 성공적으로 open 하였다면 ERROR_SUCCESS값이 reg 에 저장된다.
		if (ERROR_SUCCESS == reg) {
			// 레지스트리의 run 항목에 자동 실행될 프로그램의 경로를 저장한다.
			reg = RegSetValueEx(hkey, strKeyName.GetString(), 0, REG_SZ, (BYTE*)strFilePath.GetString(), REG_SZ);

			if (reg == ERROR_SUCCESS) return TRUE;
			else return FALSE;

			// 오픈한 키를 닫는다.
			RegCloseKey(hkey);
		}
}

VOID CTiorSaverTransferApp::SendThumb(BYTE* pFileData, DWORD _dwSize)
{
	if (m_bSendThumbThreadStart)
		return;
	m_bSendThumbThreadStart = TRUE;
	CString strUrl = _T("");
	CParamParser parser;
	CString strParam = _T("");
	CString strReturn = _T("");
	CString strParameter = _T("");
	CBase64 base64;
	CString strEncodedFileData = _T("");
	CString strUrlEncodedData = _T("");
	strParam = parser.CreateKeyValueScreenRequest(_T("thumb.jpg"));
	strUrl.Format(_T("%s/v1/trans/last_image"), theApp.m_pMemberData->GetServerHostURL());
	strEncodedFileData = base64.base64encode(pFileData, _dwSize);
	strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
	strParameter = strParam + strUrlEncodedData;
	strReturn = theApp.m_curl.CurlPost(strParameter, strUrl, theApp.GetToken());
	{
		CString strLog = _T("");
		strLog.Format(_T("[SendThumbThread] %s %s[line: %d, function: %s, file: %s]"), strParameter, strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
	}
	CString strTokenFromReturn = _T("");
	POLICY_SET* pPolicySet = new POLICY_SET;
	INT nParseResult = 0;
	nParseResult = parser.ParserPolicySet(strReturn, /*nCurrentOffset, */strTokenFromReturn, pPolicySet);

	if (1 == nParseResult) {
		theApp.SetToken(strTokenFromReturn);
		if (NULL != pPolicySet) {
			if (pPolicySet->bIsPolicyExist) {
				theApp.ChangePolicy(pPolicySet);
			}
		}
	}
	else if (-1 == nParseResult) {
		// 2017-11-27 sy.choi
		theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
	}
	SE_MemoryDelete(pPolicySet);
	m_bSendThumbThreadStart = FALSE;
	return;
}

VOID CTiorSaverTransferApp::VerifyLicense()
{
	CString strUrl = _T("");
	CString strReturn = _T("");
	CString strParam = _T("");
	CString strToken = _T("");
	CTypedPtrArray <CPtrArray, DEPARTMENT*> arrDepartment;

	CParamParser parser;
	arrDepartment.RemoveAll();
	strParam = parser.CreateInstallKeyValueRequest(m_pMemberData->GetLicenseCode());
	strUrl.Format(_T("%s/v1/agent/license"), m_pMemberData->GetServerHostURL());
	strReturn = m_curl.CurlPost(strParam, strUrl, GetToken());
	if (TRUE == parser.ParserIdentifyLicense(strReturn, &arrDepartment, strToken)) {
		SetToken(strToken);
	}
}