#pragma once
//#include "CommonDefine.h"

class CParamParser
{
public:
	CParamParser(void);
	virtual ~CParamParser(void);
	INT ParserGetOffsetReturn(CString _strRecvData, INT& nCurrentOffset, CString& strToken, int& nDelay);
	INT ParserOnlyToken(CString _strRecvData, /*UINT& nCurrentOffset, */CString& strToken);
	INT ParserTotalPolicyIndex(CString _strRecvData, LOG_DATA* pIndex);
	INT ParserIdentifyLicense(CString _strRecvData, CTypedPtrArray <CPtrArray, DEPARTMENT*>* arrDepartment, CString& strToken);
	//CString CreateJSONRequest(CString _strText, CString _strTrid, CString _strLicenseKey);
	INT ParserPolicySet(CString _strRecvData, CString& strToken, POLICY_SET* pPolicySet);
	CString CreateKeyValueRequest();
	CString CreateFileResetRequest(CString _strFileName);
	CString CreateKeyValueLogRequest(CString _strFileName);
	CString CreateAgentRegisterRequest(int _nDepartmentID, CString _strAgentName);
	CString CreateKeyValueScreenRequest(CString _strFileName);
	CString CreateOffsetKeyValueRequest(CString _strFileName);
	CString CreateInstallKeyValueRequest(CString _strLicenseCode);
	INT GetErrorCheck(CString _strRecvData, CString strMessage);
	CString CreateClientID(CString _strClientID);
	CString ParserClientID(CString _strData);
	BOOL ParserPolicySetForWebSocket(CString _strRecvData, CString& strToken, POLICY_SET* pPolicySet);
	BOOL ParserThumnailAction(CString _strRecvData, BOOL& _bStart);
};
