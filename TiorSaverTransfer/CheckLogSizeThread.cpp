/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.11.03	
********************************************************************************/
/**
@file       CLogSendThread.cpp
@brief     LogSendThread  구현 파일
@author	 jhlee
@date      create 2011.11.03
*/
// LogSendThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CheckLogSizeThread.h"
#include "../BuildEnv/common/SecurityControl.h"
#include "../BuildEnv/common/UtilsProcess.h"
#include "TiorSaverTransfer.h"
#include "../BuildEnv/common/UtilParse.h"
#include "../BuildEnv/common/PCInfo.h"
#include "../BuildEnv/common/PathInfo.h"
#include "../BuildEnv/common/UtilsFile.h"
#include "ParamParser.h"
#include "../BuildEnv/common/UtilsUnicode.h"
#include "Crypto/Base64.h"
#include "Convert.h"
//#include "MainFrm.h"
//#include "SendAgentCommonDefine.h"

#include <yvals.h>
#include <codecvt>

#include <boost/locale/encoding.hpp>
#include <codecvt>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/insert_linebreaks.hpp>
#include <boost/archive/iterators/remove_whitespace.hpp>
#include <iostream>
//#include <string>

using namespace boost::archive::iterators;
using namespace std;

// CLogSendThread

IMPLEMENT_DYNCREATE(CCheckLogSizeThread, CWinThread)


#pragma warning(disable:4706)
/********************************************************************************
@class     CLogSendThread
@brief     로그 전송 
*********************************************************************************/

/**
@brief     생성자
@author   JHLEE
@date      2011.11.03
@note      초기화 작업
*/
CCheckLogSizeThread::CCheckLogSizeThread()
{
	m_bSetServerIP = FALSE;
	m_dwFileSize = 0;
	m_strTargetFile = _T("");
	m_nCurrentOffset = 0;
	m_hResendEvent = NULL;
	m_hStopEvent = NULL;
	CreateStopEvent();
	CreateResendEvent();
	m_bCheckLogSizeThreadSending = FALSE;
	m_bCheckLogSizeThreadStart  = FALSE;
	m_strUrl = _T("");
}

/**
@brief     소멸자
@author   JHLEE
@date      2011.11.03
*/
CCheckLogSizeThread::~CCheckLogSizeThread()
{
	m_bCheckLogSizeThreadStart = FALSE;
	m_bCheckLogSizeThreadSending = FALSE;
	m_strTargetFile = _T("");
}

BOOL CCheckLogSizeThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CCheckLogSizeThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	m_bCheckLogSizeThreadStart = FALSE;
	m_bCheckLogSizeThreadSending = FALSE;
	m_strTargetFile = _T("");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCheckLogSizeThread, CWinThread)
END_MESSAGE_MAP()

int CCheckLogSizeThread::Run()
{
	m_bCheckLogSizeThreadStart = TRUE;
	{
		CString strLog = _T("");
		strLog.Format(_T("[CCheckLogSizeThread] CCheckLogSizeThread::Run() [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
		UM_WRITE_LOG(strLog);
	}
	CString strTargetFile = GetTargetFile();
	if (0 == ExtractFileExt(strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_LOG)) {
		FILE *fStream;
		fStream = _wfsopen(strTargetFile, _T("r, ccs=UTF-8"), _SH_DENYNO);
		if (NULL == fStream)
		{
			m_bCheckLogSizeThreadStart = FALSE;
			ResetEvent(this->m_hStopEvent);
			ResetEvent(this->m_hResendEvent);
			CloseHandle(this->m_hStopEvent);
			CloseHandle(this->m_hResendEvent);
			DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s open error : %d [line: %d, function: %s, file: %s]"), strTargetFile, GetLastError(), __LINE__, __FUNCTIONW__, __FILEW__);
			return -1;	// 2017-08-02 sy.choi 탈출
		}
		CStdioFile m_cstdLogFile(fStream);
		HANDLE hEvent[2] = {this->m_hResendEvent, this->m_hStopEvent};
		try
		{
			ULONGLONG lActual;
			lActual = m_cstdLogFile.Seek(GetCurrentOffset(), CFile::begin);
		}
		catch(CFileException* e)
		{
			if( e->m_cause == CFileException::fileNotFound)
				TRACE(_T("ERROR: File not found\n"));
			e->Delete();
			m_cstdLogFile.Close();
			fclose(fStream);
			m_bCheckLogSizeThreadStart = FALSE;
			ResetEvent(this->m_hStopEvent);
			ResetEvent(this->m_hResendEvent);
			CloseHandle(this->m_hStopEvent);
			CloseHandle(this->m_hResendEvent);
			DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s Seek error : %d [line: %d, function: %s, file: %s]"), strTargetFile, e->m_cause, __LINE__, __FUNCTIONW__, __FILEW__);
			return -1;
		}
		//SetFilePointer(hFile, GetCurrentOffset(), NULL, FILE_BEGIN);
		
		while (TRUE)
		{
			DWORD dwResult = WaitForMultipleObjects(2, hEvent, FALSE, INFINITE);
			if (WAIT_OBJECT_0 + 1 == dwResult) {

				DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s got stop event[line: %d, function: %s, file: %s]"), strTargetFile, __LINE__, __FUNCTIONW__, __FILEW__);
				m_strTargetFile = _T("");
				m_bCheckLogSizeThreadStart = FALSE;
				ResetEvent(this->m_hStopEvent);
				ResetEvent(this->m_hResendEvent);
				m_cstdLogFile.Close();
				fclose(fStream);
				CloseHandle(this->m_hStopEvent);
				CloseHandle(this->m_hResendEvent);
				return 1;	// 2017-08-02 sy.choi 탈출
			} else if (WAIT_OBJECT_0 == dwResult) {
				m_bCheckLogSizeThreadSending = TRUE;
				if (1 != LogFiles(m_cstdLogFile))
					break;
				m_bCheckLogSizeThreadSending = FALSE;
			}
		}
	}
	else {
		if (m_cScreenFile.Open(strTargetFile, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone)) {
			HANDLE hEvent[2] = {this->m_hResendEvent, this->m_hStopEvent};
			m_cScreenFile.Seek(GetCurrentOffset(), CFile::begin);
			DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s open error : %d [line: %d, function: %s, file: %s]"), strTargetFile, GetLastError(), __LINE__, __FUNCTIONW__, __FILEW__);

			while (TRUE)
			{
				DWORD dwResult = WaitForMultipleObjects(2, hEvent, FALSE, INFINITE);
				if (WAIT_OBJECT_0 + 1 == dwResult) {
					DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s got stop event [line: %d, function: %s, file: %s]"), strTargetFile, __LINE__, __FUNCTIONW__, __FILEW__);
					m_bCheckLogSizeThreadStart = FALSE;
					ResetEvent(this->m_hStopEvent);
					ResetEvent(this->m_hResendEvent);
					m_cScreenFile.Close();
					CloseHandle(this->m_hStopEvent);
					CloseHandle(this->m_hResendEvent);
					return 1;	// 2017-08-02 sy.choi 탈출
				} else if (WAIT_OBJECT_0 == dwResult) {
					{
						if (0 == ExtractFileExt(strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_NCVI)) {
							CString strLog = _T("");
							
							strLog.Format(_T("[CheckOffset] %s ncviArrayMaxSizeFull: %d [line: %d, fuction: %s, file: %s]"), strTargetFile, theApp.m_nNcviArrayMaxSizeFull, __LINE__, __FUNCTIONW__, __FILEW__);
							UM_WRITE_LOG(strLog);
							theApp.m_nNcviArrayMaxSizeFull++;
						}
						else {
							DBGLOG(_T("[CCheckLogSizeThread] %s nkfiArrayMaxSizeFull: %d  [line: %d, function: %s, file: %s]"), strTargetFile, theApp.m_nNkfiArrayMaxSizeFull, __LINE__, __FUNCTIONW__, __FILEW__);
							theApp.m_nNkfiArrayMaxSizeFull++;
						}
					}
					m_bCheckLogSizeThreadSending = TRUE;
					INT nResult = 0;
					nResult = ScreenFiles();
					if (0 == ExtractFileExt(strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_NCVI)) {
						DBGLOG(_T("[CCheckLogSizeThread] %s nkfiArrayMaxSizeFull: %d  [line: %d, function: %s, file: %s]"), strTargetFile, theApp.m_nNcviArrayMaxSizeFull, __LINE__, __FUNCTIONW__, __FILEW__);
						theApp.m_nNcviArrayMaxSizeFull--;
					}
					else {
						DBGLOG(_T("[CCheckLogSizeThread] %s nkfiArrayMaxSizeFull: %d  [line: %d, function: %s, file: %s]"), strTargetFile, theApp.m_nNkfiArrayMaxSizeFull, __LINE__, __FUNCTIONW__, __FILEW__);
						theApp.m_nNkfiArrayMaxSizeFull--;
					}
					m_bCheckLogSizeThreadSending = FALSE;

				}
			}
		}
	}
	m_bCheckLogSizeThreadStart = FALSE;
	return 0;
}

CString CCheckLogSizeThread::GetTargetFile()
{
	return m_strTargetFile;
}

void CCheckLogSizeThread::SetTargetFile(CString strTargetFile)
{
	m_strTargetFile = strTargetFile;
}

UINT CCheckLogSizeThread::GetCurrentOffset()
{
	return m_nCurrentOffset;
}

void CCheckLogSizeThread::SetCurrentOffset(UINT _nCurrentOffset)
{
	m_nCurrentOffset = _nCurrentOffset;
}
//int CCheckLogSizeThread::GetDelay()
//{
//	return m_nDelay;
//}
//void CCheckLogSizeThread::SetDelay(int _nDelay)
//{
//	m_nDelay = _nDelay;
//}

UINT CCheckLogSizeThread::GetFileSize()
{
	return m_dwFileSize;
}

void CCheckLogSizeThread::SetFileSize(UINT _dwFileSize)
{
	m_dwFileSize = _dwFileSize;
}

INT CCheckLogSizeThread::ScreenFiles()
{
	CParamParser parser;
	CString strParam = _T("");
	strParam = parser.CreateKeyValueScreenRequest(m_strTargetFile);
	CString strUrl = _T("");
	DWORD nBytesRead = 1;
	CString strReturn = _T("");
	strUrl.Format(_T("%s/v1/trans/screen"), theApp.m_pMemberData->GetServerHostURL());
	if (INVALID_HANDLE_VALUE == m_cScreenFile) {
		return -1;
	}
	try
	{
		ULONGLONG lActual;
		lActual = m_cScreenFile.Seek(0, CFile::current);
		if (GetCurrentOffset() != lActual) {
			if (0 == GetCurrentOffset()) {
				m_cScreenFile.Seek(0, CFile::begin);
			} else {
				DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s cur: %I64u, sent: %d[line: %d, function: %s, file: %s]"), m_strTargetFile, lActual, m_nCurrentOffset, __LINE__, __FUNCTIONW__, __FILEW__);
				theApp.PostResetOffsetToServer(m_strTargetFile);
				return -1;
			}
		} else if (0 == GetCurrentOffset()) {
			m_cScreenFile.Seek(0, CFile::begin);
		}
	}
	catch (CFileException* e)
	{
		if (e->m_cause == CFileException::fileNotFound)
			TRACE(_T("ERROR: File not found\n"));
		e->Delete();
		DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s Seek error %d[line: %d, function: %s, file: %s]"), m_strTargetFile, e->m_cause, __LINE__, __FUNCTIONW__, __FILEW__);
		return -1;
	}
	CString strParameter = _T("");
	INT nSendMaxSize = theApp.m_nTransMaxScreenSize;
	if (0 == ExtractFileExt(m_strTargetFile).CompareNoCase(COLLECT_FILE_EXTENSION_NKFI)) {
		nSendMaxSize = SEND_NKFI_MAX_SIZE;
	}

	do
	{
		BYTE* pFileData = new BYTE[nSendMaxSize];
		memset(pFileData, 0x00, (nSendMaxSize) * sizeof(BYTE));
		nBytesRead = m_cScreenFile.Read(pFileData, nSendMaxSize);
		if (0 < nBytesRead) {
			CBase64 base64;
			CString strEncodedFileData = _T("");
			CString strUrlEncodedData = _T("");
			strEncodedFileData = base64.base64encode(pFileData, nBytesRead);
			//strUrlEncodedData = base64.UrlEncode(CConvert::ToChar(strEncodedFileData));
			strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
			strParameter = strParam + strUrlEncodedData;
		}
		else {	// 2017-09-22 sy.choi 읽은거 없으면 탈출
			SE_MemoryDelete(pFileData);
			DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s read done. readbyte: %d [line: %d, function: %s, file: %s]"), m_strTargetFile, nBytesRead, __LINE__, __FUNCTIONW__, __FILEW__);
			break;
		}
		SE_MemoryDelete(pFileData);
		int nErr = 0;
		DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s read per 100MB readbyte: %d [line: %d, function: %s, file: %s]"), m_strTargetFile, nBytesRead, __LINE__, __FUNCTIONW__, __FILEW__);
		strReturn = theApp.m_curl.CurlPost(strParameter, strUrl, theApp.GetToken());
		{
			CString strLog = _T("");
			strLog.Format(_T("[CCheckLogSizeThread] strTargetFile %s ,%s Length: %d strReturn: %s[line: %d, function: %s, file: %s]"), m_strTargetFile, strParam, strParameter.GetLength(), strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
		CString strTokenFromReturn = _T("");
		POLICY_SET* pPolicySet = new POLICY_SET;
		INT nParseResult = 0;
		nParseResult = parser.ParserPolicySet(strReturn, /*nCurrentOffset, */strTokenFromReturn, pPolicySet);
		DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s parse result: %d from %s [line: %d, function: %s, file: %s]"), m_strTargetFile, nParseResult, strTokenFromReturn, __LINE__, __FUNCTIONW__, __FILEW__);
		if (1 == nParseResult) {
			//m_nCurrentPos += m_nTempCurrentPos;
			theApp.SetToken(strTokenFromReturn);
			if (NULL != pPolicySet) {
				DBGLOG(_T("[CCheckLogSizeThread] pPolicySet is not null [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
				if (pPolicySet->bIsPolicyExist) {
					DBGLOG(_T("[CCheckLogSizeThread] change policy... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
					theApp.ChangePolicy(pPolicySet);
				}
				if (0 < theApp.m_nDeleteDelay && !theApp.m_strDeleteDate.IsEmpty()) {
					DBGLOG(_T("[CCheckLogSizeThread] start delete olds... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
					theApp.StartDeleteOldFiles();
				}
				if (NULL != pPolicySet->UpdateSet) {
					DBGLOG(_T("[CCheckLogSizeThread] start update... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
					theApp.UpdateStart(pPolicySet->UpdateSet);
				}
				SE_MemoryDelete(pPolicySet);
			}
		}
		else {
			if (-1 == nParseResult) {
				// 2017-11-27 sy.choi
				DBGLOG(_T("[CCheckLogSizeThread] start MSG_PAUSE_COMMUNICATE... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
				theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
			}
			if (strTokenFromReturn.IsEmpty()) {
				if (0 == nErr) {
					strTokenFromReturn.Format(_T("%s"), strReturn);
				}
				else {
					strTokenFromReturn.Format(_T("%d"), nErr);
				}
			}
			return -1;
		}
		if (nBytesRead != nSendMaxSize) {
			break;
		}
	} while (nBytesRead > 0);
	return 1;
}

INT CCheckLogSizeThread::LogFiles(CStdioFile& m_cstdLogFile)
{
	CString strUrl = _T("");
	CParamParser parser;
	CString strParam = _T("");
	strUrl.Format(_T("%s/v1/trans/event"), theApp.m_pMemberData->GetServerHostURL());
	if (INVALID_HANDLE_VALUE == m_cstdLogFile) {
		return -1;
	}
	try
	{
		ULONGLONG lActual;
		lActual = m_cstdLogFile.Seek(0, CFile::current);
		DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s cur: %I64u, sent: %d[line: %d, function: %s, file: %s]"), m_strTargetFile, lActual, m_nCurrentOffset, __LINE__, __FUNCTIONW__, __FILEW__);
		if (GetCurrentOffset() != lActual) {
			if (0 == GetCurrentOffset()) {
				m_cstdLogFile.Seek(0, CFile::begin);
			} else {
				DBGLOG(_T("[PostResetOffsetToServer] strTargetFile %s cur: %I64u, sent: %d[line: %d, function: %s, file: %s]"), m_strTargetFile, lActual, m_nCurrentOffset, __LINE__, __FUNCTIONW__, __FILEW__);
				theApp.PostResetOffsetToServer(m_strTargetFile);
				return -1;
			}
		}
	}
	catch (CFileException* e)
	{
		if (e->m_cause == CFileException::fileNotFound)
			TRACE(_T("ERROR: File not found\n"));
		e->Delete();
		DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s close fail...%d[line: %d, function: %s, file: %s]"), m_strTargetFile, e->m_cause, __LINE__, __FUNCTIONW__, __FILEW__);
		return -1;
	}
	CString strParameter = _T("");
	CString strFullData = _T("");
	CString strReturn = _T("");
	int nCnt = 0;
	TCHAR* pFileData = new TCHAR[4096];
	while (1)
	{
		CString strData = _T("");
		memset(pFileData, 0x00, 4096);
		try
		{
			if (NULL == m_cstdLogFile.ReadString(pFileData, 4096)) {
				break;
			}
		}
		catch (CFileException* e)
		{
			if (e->m_cause == CFileException::fileNotFound)
				TRACE(_T("ERROR: File not found\n"));
			e->Delete();
			DBGLOG(_T("[CCheckLogSizeThread] strTargetFile %s read fail...%d[line: %d, function: %s, file: %s]"), m_strTargetFile, e->m_cause, __LINE__, __FUNCTIONW__, __FILEW__);
			return -1;
		}
		if (theApp.m_nTransMaxLogLines <= nCnt || SEND_LOG_MAX_SIZE < strFullData.GetLength()) {
			strData.Format(_T("%s"), pFileData);
			strFullData += strData;
			break;
		}
		strData = pFileData;
		nCnt++;
		strFullData += strData;
	}
	UINT nToReadMax = GetFileSize();
	if (0 < strFullData.GetLength()) {
		//CBase64 base64;
		CString strEncodedFileData = _T("");
		CString strUrlEncodedData = _T("");

		int size_needed = WideCharToMultiByte(CP_UTF8, 0, strFullData.GetBuffer(), (int)strFullData.GetLength(), NULL, 0, NULL, NULL);
		std::wstring strWTo(strFullData);
		string utf8_input = boost::locale::conv::utf_to_utf<char>(strWTo);

		//typedef transform_width< binary_from_base64<remove_whitespace<string::const_iterator> >, 8, 6 > it_binary_t;
		typedef insert_linebreaks<base64_from_binary<transform_width<string::const_iterator, 6, 8> >, 72 > it_base64_t;

		// Encode
		unsigned int writePaddChars = (3 - utf8_input.length() % 3) % 3;
		string base64(it_base64_t(utf8_input.begin()), it_base64_t(utf8_input.end()));
		base64.append(writePaddChars, '=');

		wstring base64W = boost::locale::conv::utf_to_utf<wchar_t>(base64);
		//strEncodedFileData = base64.base64EncodeW(strWTo, size_needed).c_str();
		strEncodedFileData = base64W.c_str();
		strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
		strParam = parser.CreateKeyValueLogRequest(m_strTargetFile);
		strParameter = strParam + strUrlEncodedData;
		DBGLOG(_T("[CCheckLogSizeThread] strParameter %s send start...%s[line: %d, function: %s, file: %s]"), m_strTargetFile, strParam, __LINE__, __FUNCTIONW__, __FILEW__);
		int nErr = 0;
		/*if (theApp)*/ {
			strReturn = theApp.m_curl.CurlPost(strParameter, strUrl, theApp.GetToken());
		}
		if (1024000 < strParameter.GetLength()) {
			CString strLog = _T("");
			strLog.Format(_T("[CCheckLogSizeThread] %s %s %d %s[line: %d, function: %s, file: %s]"), m_strTargetFile, strParam, strParameter.GetLength(), strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
		else {
			CString strLog = _T("");
			strLog.Format(_T("[CCheckLogSizeThread] %s %s[line: %d, function: %s, file: %s]"), strParameter, strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
		CString strTokenFromReturn = _T("");
		POLICY_SET* pPolicySet = new POLICY_SET;

		INT nParseResult = 0;
		nParseResult = parser.ParserPolicySet(strReturn, strTokenFromReturn, pPolicySet);
		DBGLOG(_T("[CCheckLogSizeThread] strReturn %s parse result: %d[line: %d, function: %s, file: %s]"), strReturn, nParseResult, __LINE__, __FUNCTIONW__, __FILEW__);
		if (1 == nParseResult) {
			theApp.SetToken(strTokenFromReturn);
			if (NULL != pPolicySet) {
				DBGLOG(_T("[CCheckLogSizeThread] pPolicySet is not null [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
				if (pPolicySet->bIsPolicyExist) {
					DBGLOG(_T("[CCheckLogSizeThread] change policy... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
					theApp.ChangePolicy(pPolicySet);
				}
				if (0 < theApp.m_nDeleteDelay && !theApp.m_strDeleteDate.IsEmpty()) {
					DBGLOG(_T("[CCheckLogSizeThread] start delete olds... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
					theApp.StartDeleteOldFiles();
				}
				if (NULL != pPolicySet->UpdateSet) {
					DBGLOG(_T("[CCheckLogSizeThread] start update... [line: %d, function: %s, file: %s]"), __LINE__, __FUNCTIONW__, __FILEW__);
					theApp.UpdateStart(pPolicySet->UpdateSet);
				}
				SE_MemoryDelete(pPolicySet);
			}
			return 1;
		} else if (-1 == nParseResult) {
			// 2017-11-27 sy.choi 서버 정기점검 중
			theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
		} else {
			if (strTokenFromReturn.IsEmpty()) {
				if (0 == nErr) {
					strTokenFromReturn.Format(_T("%s"), strReturn);
				}
				else {
					strTokenFromReturn.Format(_T("%d"), nErr);
				}
			}
			try
			{
				ULONGLONG lActual;
				lActual = m_cstdLogFile.Seek(GetCurrentOffset(), CFile::begin);
			}
			catch (CFileException* e)
			{
				if (e->m_cause == CFileException::fileNotFound)
					TRACE(_T("ERROR: File not found\n"));
				e->Delete();
			}
		}
	}
	return -1;
	DBGLOG(_T("[CreateKeyValueLogRequest] full request: %s"), strParameter);
}