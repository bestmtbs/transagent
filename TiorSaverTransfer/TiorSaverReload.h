#pragma once
#include "resource.h"		// 주 기호입니다.
#include "Impersonator.h"
#include "UtilsProcess.h"
class CReloadThread : public CWinThread
{
	DECLARE_DYNCREATE(CReloadThread)
public:
	CReloadThread();
	virtual ~CReloadThread();
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();

private:
	CProcess m_Process;
	BOOL m_bStop;
	BOOL m_bIs64bit;
	INT m_nOsProduct;
	HMODULE m_hMod;
	BOOL StopAT_Suspend();			//지정된 레지스트리 파일에 값이 존재하면 ResumeThread 처리르 하지 않도록 한다.
// 재정의입니다.
public:
	//BOOL KillTiorProcess(CString _strFIlePath);
	//BOOL ChangeFileName(CString _strChangeFileName);
	BOOL SetPrivilege(HANDLE hToken, LPCTSTR Privilege, BOOL bEnablePrivilege);
	//BOOL DeleteService();
	BOOL CheckVaildServiceName(const CString &_sServiceName);
	BOOL ExistsService(const CString &_sSvcName);
	void SetStop();
	BOOL GetStop();
	void ServiceCheck();
	DECLARE_MESSAGE_MAP()
};