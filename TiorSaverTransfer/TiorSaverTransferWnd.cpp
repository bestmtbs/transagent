#include "StdAfx.h"
#include "TiorSaverTransferWnd.h"
#include "TiorSaverTransfer.h"
//#include "TiorSaverTransferCommonDefine.h"
#include "pipe/C_SendAgent_S_CollectAgent/STOCPipeClient.h"
#include "UtilsProcess.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include "ParamParser.h"

#include <winsvc.h>

//IMPLEMENT_DYNAMIC(CTiorSaverTransferWnd, CFrameWndEx)
IMPLEMENT_DYNCREATE(CTiorSaverTransferWnd, CFrameWndEx)

CTiorSaverTransferWnd::CTiorSaverTransferWnd(void)
{
	m_bIsUpdate = TRUE;
	m_bPauseCommStart = FALSE;
	m_nPauseCommSecond = 10000;
	m_bSendToCollect_Kill = FALSE;
}

CTiorSaverTransferWnd::~CTiorSaverTransferWnd(void)
{
	/*if (theApp)*/ {
		SE_MemoryDelete(theApp.m_pWebsocketClientThread);
	}
	m_bIsUpdate = TRUE;
}
//BEGIN_MESSAGE_MAP(CTiorSaverTransferWnd, CFrameWndEx)
BEGIN_MESSAGE_MAP(CTiorSaverTransferWnd, CFrameWndEx)
	ON_MESSAGE_VOID( MSG_UPDATE_COMPLETE_RESTART , OnUpdateComplete) ///업데이트 완료옴, 종료 시켜할 것들은 종료한다.
	ON_MESSAGE_VOID( MSG_DELETE_START , OnDeleteStart) ///업데이트 완료옴, 종료 시켜할 것들은 종료한다.
	ON_MESSAGE_VOID(MSG_PAUSE_COMMUNICATE, OnPauseCommunicate)
	ON_WM_TIMER()
	ON_WM_ENDSESSION()
	ON_WM_QUERYENDSESSION()
	ON_WM_WTSSESSION_CHANGE()
	ON_WM_CREATE()
END_MESSAGE_MAP()


void CTiorSaverTransferWnd::OnUpdateComplete()
{
	theApp.SetUpdateCompleteOnDB();
	if (FALSE == theApp.m_bUpdateAfterReboot) {
		m_bIsUpdate = TRUE;
		SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
	}
}

void CTiorSaverTransferWnd::OnDeleteStart()
{
	m_bIsUpdate = FALSE;
	if (theApp.m_pReloadThread) {
		theApp.m_pReloadThread->SetStop();
	}
	SetTimer(TIMER_START_UNINSTALL_PROC_UNTIL_SUCCESS, EXIT_AGENT_RETRY_TIME, NULL);
	//SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
}

void CTiorSaverTransferWnd::OnPauseCommunicate()
{
	if (m_bPauseCommStart) {
		return;
	}
	//SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
	PauseCommunicate();
}

BOOL CTiorSaverTransferWnd::ChangeFileName(CString _strChangeFileName)
{
	BOOL bResult = FALSE;
	CString strFileFullpath, strTempName, strLog;
	try
	{
		strFileFullpath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), _strChangeFileName) ;

		strTempName.Format( _T("%s_%s"), CPathInfo::GetClientInstallPath(), _strChangeFileName) ;  //_가 붙은 파일이름으로 변경
		//이미 백업된 파일이 있다면 삭제 후 ReName한다.

		UM_WRITE_LOG(strFileFullpath + _T("  ->") + strTempName);

		CFile::Rename( strFileFullpath, strTempName);
		bResult = TRUE;
	}
	catch (CFileException* pEx)
	{
		strLog.Format(_T("ChangeFileName Error : %d"), pEx->m_cause);
		UM_WRITE_LOG(strLog);
		pEx->Delete();
		bResult = FALSE;
	}
	return bResult;
}

void CTiorSaverTransferWnd::SendToExitCmd()
{
	// 2017-09-07 sy.choi svc와 reload는 update에서 update마치고 죽임
	//if(FALSE == m_bSendToCollect_Kill) {
	//	if (TRUE == SendToKillCollectAgent()) {
	//		m_bSendToCollect_Kill = TRUE;
	//		SetTimer(TIMER_EXIT_AGENT_PROCESS, EXIT_AGENT_RETRY_TIME, NULL);
	//	}
	//m_bSendToCollect_Kill = TRUE;
	if (FALSE == m_bIsUpdate) {
		WinExec((LPCSTR)(LPCTSTR)(CPathInfo::GetClientInstallPath() + WTIOR_AGENT_SERVICE_NAME + _T(" u")), SW_HIDE);
	}
	if (SendToKillCollectAgent()) {
		SetTimer(TIMER_EXIT_AGENT_PROCESS, EXIT_AGENT_RETRY_TIME, NULL);
	}
	else {
		SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
	}
	//if(FALSE == m_bSendToCollect_Kill) {
	//	//타이머 설정			
	//	SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
	//}
}

void CTiorSaverTransferWnd::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == TIMER_SEND_TO_EXIT_CMD) {
		KillTimer(TIMER_SEND_TO_EXIT_CMD);
		SendToExitCmd();
	}
	else if (nIDEvent == TIMER_EXIT_AGENT_PROCESS) {
		KillTimer(TIMER_EXIT_AGENT_PROCESS);
		ExitAgentPocess();		
	}
	else if (nIDEvent == TIMER_ALL_THREAD_EXIT_CHECK) {
		KillTimer(TIMER_ALL_THREAD_EXIT_CHECK);
		KillReloadAndServiceUnregister();
	}
	else if (nIDEvent == TIMER_START_UNINSTALL_PROC_UNTIL_SUCCESS){
		KillTimer(TIMER_START_UNINSTALL_PROC_UNTIL_SUCCESS);
		StartUninstallProc();
	} 
	else if (TIMER_PAUSE_COMMUNICATE == nIDEvent) {
		//KillTimer(TIMER_PAUSE_COMMUNICATE);
		PauseCommunicate();
	}
}

void CTiorSaverTransferWnd::KillReloadAndServiceUnregister()
{
	static int nTryCnt = 0;
	//if (FileExists(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_RELOAD_NAME)) {
	//	ChangeFileName(WTIOR_AGENT_RELOAD_NAME);
	//}
	theApp.m_pReloadThread->SetStop();
	if (DeleteService()) {
		SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
		return;
	}
	else {
		SetTimer(TIMER_ALL_THREAD_EXIT_CHECK, CHECK_ALL_THREAD_END, NULL);
		return;
	}
	if (nTryCnt > 60) {
		//KillTiorProcess(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_RELOAD_NAME);
		KillTiorProcess(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_SERVICE_NAME);
		SetTimer(TIMER_SEND_TO_EXIT_CMD, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
	} else {
		nTryCnt++;
	}
}

void CTiorSaverTransferWnd::ExitAgentPocess()
{
	static int nTryCnt = 0;
	if (theApp.m_pCheckPolicyThread) {
		delete theApp.m_pCheckPolicyThread;
		theApp.m_pCheckPolicyThread = NULL;
	}
	if (FALSE == m_bIsUpdate) {
		theApp.MoveFile();
	}
	if ((TRUE == theApp.TerminateFileTransfer()) && (NULL == theApp.m_pCheckPolicyThread)) {
		if (TRUE == m_bIsUpdate) {
			WinExec((LPCSTR)(LPCTSTR)(CPathInfo::GetClientInstallPath() + WTIOR_AGENT_SERVICE_NAME + _T(" s")), SW_HIDE);
		}
		AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
	} else {
		if (nTryCnt > 60) {	// 2017-09-15 sy.choi 60번 해도 안죽으면 죽음.
			if (TRUE == m_bIsUpdate) {
				WinExec((LPCSTR)(LPCTSTR)(CPathInfo::GetClientInstallPath() + WTIOR_AGENT_SERVICE_NAME + _T(" s")), SW_HIDE);
			}
			AfxGetApp()->m_pMainWnd->PostMessage(WM_CLOSE);
		}
		else {
			nTryCnt++;
			SetTimer(TIMER_EXIT_AGENT_PROCESS, EXIT_AGENT_RETRY_TIME, NULL);
		}
	}
}

BOOL CTiorSaverTransferWnd::SendToKillCollectAgent()
{
	static int nTryCnt = 0;
	BOOL bResult = FALSE;
	CString strUsrAgentName = _T("");
	CString strSysAgentName = _T("");
	//CString strSTOCSysPipeName = _T("");
	//CString strSTOCUsrPipeName = _T("");
	//strSTOCSysPipeName.Format(_T("%s"), STOC_SYS_PIPE_NAME);
	//strSTOCUsrPipeName.Format(_T("%s_%d"), STOC_USR_PIPE_NAME, theApp.GetUsrAgentSessionID());
	//CSTOCPipeClient STOCSysPipeClient((TCHAR*)(LPCTSTR)strSTOCSysPipeName);
	//CSTOCPipeClient STOCUsrPipeClient((TCHAR*)(LPCTSTR)strSTOCUsrPipeName);
	SHARE_DATA_WITH_COLLECT_AGENT sendData;
	sendData.dwAct = ACT_EXIT_COLLECT_AGENT;
	if (theApp.m_pCSTOSCPipeClient) {
		if (theApp.m_pCSTOSCPipeClient->OnSetSendData(&sendData) == TRUE) {
			theApp.m_pCSTOSCPipeClient->PipeClientStartUp();
			bResult = TRUE;
		}
	}
	if (theApp.m_pCSTOUCPipeClient) {
		if (theApp.m_pCSTOUCPipeClient->OnSetSendData(&sendData) == TRUE) {
			theApp.m_pCSTOUCPipeClient->PipeClientStartUp();
			if (TRUE == bResult) {
				m_bSendToCollect_Kill = TRUE;
			}
		}
	}
	if (TRUE == m_bIsUpdate) {
		KillTiorProcess(CPathInfo::GetClientInstallPath()+WTIOR_AGENT_SERVICE_NAME);	//	2017-09-26 sy.choi
	}
	if (m_bSendToCollect_Kill) {
		return TRUE;
	}
	if (60 < nTryCnt) {
		CWinOsVersion osVer;
		BOOL bIs64bit = osVer.Is64bit();
		if (bIs64bit) {
			if (osWinVista > osVer.GetProduct()) {
				strUsrAgentName = WTIOR_USR_AGENT_XP_64_NAME;
				strSysAgentName = WTIOR_SYS_AGENT_XP_64_NAME;
			} else {
				strUsrAgentName = WTIOR_USR_AGENT_64_NAME;
				strSysAgentName = WTIOR_SYS_AGENT_64_NAME;
			}
		} else {
			if (osWinVista > osVer.GetProduct()) {
				strUsrAgentName = WTIOR_USR_AGENT_XP_32_NAME;
				strSysAgentName = WTIOR_SYS_AGENT_XP_32_NAME;
			}
			else {
				strUsrAgentName = WTIOR_USR_AGENT_32_NAME;
				strSysAgentName = WTIOR_SYS_AGENT_32_NAME;
			}
		}
		KillTiorProcess(CPathInfo::GetClientInstallPath() + strUsrAgentName);
		KillTiorProcess(CPathInfo::GetClientInstallPath() + strSysAgentName);
		return TRUE;
	} else {
		nTryCnt++;
		return FALSE;
	}
}

BOOL CTiorSaverTransferWnd::SetPrivilege(
									HANDLE hToken,   // 토큰 핸들
									LPCTSTR Privilege,   // 활성/비활성화할 권한
									BOOL bEnablePrivilege  // 권한 활성화 여부?
									)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;
	TOKEN_PRIVILEGES tpPrevious;
	DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);

	if (!LookupPrivilegeValue( NULL, Privilege, &luid ))
		return FALSE;

	// 현재의 권한 설정 얻기
	tp.PrivilegeCount   = 1;
	tp.Privileges[0].Luid   = luid;
	tp.Privileges[0].Attributes = 0;

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		&tpPrevious,
		&cbPrevious
		);

	if (GetLastError() != ERROR_SUCCESS)
		return FALSE;

	// 이전의 권한 설정에 따라 권한 설정하기
	tpPrevious.PrivilegeCount       = 1;
	tpPrevious.Privileges[0].Luid   = luid;

	if (bEnablePrivilege) {
		tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
	}
	else {
		tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
			tpPrevious.Privileges[0].Attributes);
	}

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tpPrevious,
		cbPrevious,
		NULL,
		NULL
		);

	if (GetLastError() != ERROR_SUCCESS)
		return FALSE;

	return TRUE;
}

BOOL CTiorSaverTransferWnd::KillTiorProcess(CString _strFIlePath)
{
	CString strLog = _T("");
	CString strCmd = _T("");

	HANDLE hToken;
	OpenProcessToken(GetCurrentProcess(),
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
        &hToken);

	SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);

	BOOL bRet = CProcess::KillProcessByName(_strFIlePath);	// 2017-09-15 sy.choi 프로세스가 실행중이 아닐때에도 TRUE 리턴하므로
	strLog.Format(_T("Kill ret : %d"), bRet);
	UM_WRITE_LOG(strLog);
	strCmd.Format(_T("taskkill /F /T /IM %s"), ExtractFileName(_strFIlePath));
	WinExec((LPCSTR)(LPCTSTR)strCmd, SW_HIDE);
	
	SetPrivilege(hToken, SE_DEBUG_NAME, FALSE);
    CloseHandle(hToken);
	return TRUE;
}

BOOL CTiorSaverTransferWnd::DeleteService()
{
	if (FALSE == CheckVaildServiceName(TS_SERVICE_NAME)) {
		return TRUE;
	}
	if (FALSE == ExistsService(TS_SERVICE_NAME)) {
		return TRUE;
	}
	CImpersonator imp;
	CString strServiceName = _T("");
	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
	if( imp.CreatProcessAndCompareStr(strServiceName,  "ok", _T("u")) )
	{
		imp.Free();
		return TRUE;
	}
	else
	{
		imp.Free();
		return FALSE;
	}
}
#define MAXLEN_SERVICENAME              182        /**< 서비스 이름 최대 길이NT이상OS (NULL 종료문자를 제외한 최대 길이)      */
#define MAXLEN_NTSERVICENAME            80         /**< 서비스 이름 최대 길이 NT에서만(NULL 종료문자를 제외한 최대 길이)      */
BOOL CTiorSaverTransferWnd::CheckVaildServiceName(const CString &_sServiceName)
{    
	if (_T("") == _sServiceName) 
		return false;

	CWinOsVersion OSVersion;
	if (osWinNT == OSVersion.GetProduct()) 
	{
		if (MAXLEN_NTSERVICENAME < _sServiceName.GetLength()) 
			return false;
	}
	else
	{
		if (MAXLEN_SERVICENAME < _sServiceName.GetLength()) 
			return false;
	}

	if (-1 != _sServiceName.Find(_T("/"))) 
		return false;    
	if (-1 != _sServiceName.Find(_T("\\")))
		return false;

	return true;
}

BOOL CTiorSaverTransferWnd::ExistsService(const CString &_sSvcName)
{

	SC_HANDLE schService = NULL;
	SC_HANDLE schSCManager = NULL;

	if (!CheckVaildServiceName(_sSvcName)) 
		return false;    

	schSCManager = ::OpenSCManager(NULL, NULL, GENERIC_READ);
	if (NULL == schSCManager)
		return false;

	schService = ::OpenService(schSCManager, _sSvcName, SERVICE_QUERY_STATUS);        
	if (NULL == schService)    
	{
		::CloseServiceHandle(schSCManager);
		return false;
	}

	::CloseServiceHandle(schService);
	::CloseServiceHandle(schSCManager);

	return true;
}

BOOL CTiorSaverTransferWnd::StartUninstallProc()
{
	BOOL bResult = FALSE;
	CString strLog = _T("");
	CString strUninstall = _T("");
	strUninstall.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_UNINSTALL_AGENT_NAME);
		CString strUrl = _T("");
		CString strReturn = _T("");
		CString strParam = _T("");
		CString strTokenFromReturn = _T("");
		CParamParser parser;
		INT nErr = 0;

		strParam = parser.CreateKeyValueRequest();	// 2017-10-12 sy.choi 삭제 완료 서버로 전송. 서버의 return과 상관없이 삭제해야 함.
		strUrl.Format(_T("%s/v1/agent/remove"), theApp.m_pMemberData->GetServerHostURL());
		/*if (theApp)*/ {
			strReturn = theApp.m_curl.CurlDelete(strParam, strUrl, theApp.GetToken());
			if (parser.ParserOnlyToken(strReturn, strTokenFromReturn)) {
				theApp.SetToken(strTokenFromReturn);
			} else {
				if (strTokenFromReturn.IsEmpty()) {
					if (0 == nErr) {
						strTokenFromReturn.Format(_T("%s"), strReturn);
					} else {
						strTokenFromReturn.Format(_T("%d"), nErr);
					}
				}
			}
		}
	SetTimer(TIMER_ALL_THREAD_EXIT_CHECK, CHECK_ALL_THREAD_END, NULL);
	return bResult;
}


void CTiorSaverTransferWnd::OnEndSession(BOOL bEnding)
{
	/*if (theApp)*/ {
		if (theApp.m_pReloadThread)
			theApp.m_pReloadThread->SetStop();
		SE_MemoryDelete(theApp.m_pWebsocketClientThread);
	}
	CFrameWndEx::OnEndSession(bEnding);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


LRESULT CTiorSaverTransferWnd::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	if (WM_QUERYENDSESSION == message) {
		{
			CString strCountFile = _T("");
			CString strCount = _T("");
			DWORD dwBytesWritten = 0;
			strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
			CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("[%s][%d][%d][line: %d, function: %s, file: %s]\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), message, __LINE__, __FUNCTIONW__, __FILEW__);
			WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
		}
		/*if (theApp)*/ {
			{
				CString strCountFile = _T("");
				CString strCount = _T("");
				DWORD dwBytesWritten = 0;
				strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
				CTime cTime = CTime::GetCurrentTime();
				strCount.Format(_T("[%s][%d][%d][line: %d, function: %s, file: %s]\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), message, __LINE__, __FUNCTIONW__, __FILEW__);
				WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			}
			if (theApp.m_pReloadThread)
				theApp.m_pReloadThread->SetStop();
			SE_MemoryDelete(theApp.m_pWebsocketClientThread);
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
			//CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("[%d][line: %d, function: %s, file: %s]"), message, __LINE__, __FUNCTIONW__, __FILEW__);
			DROP_TRACE_LOG(_T("TiorSaver_PWOFF"), strCount);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
		}
	}
	if (WM_ENDSESSION == message) {
		{
			CString strCountFile = _T("");
			CString strCount = _T("");
			DWORD dwBytesWritten = 0;
			strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
			CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("[%s][%d][%d][line: %d, function: %s, file: %s]\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), message, __LINE__, __FUNCTIONW__, __FILEW__);
			WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
		}
		/*if (theApp)*/ {
			{
				CString strCountFile = _T("");
				CString strCount = _T("");
				DWORD dwBytesWritten = 0;
				strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
				CTime cTime = CTime::GetCurrentTime();
				strCount.Format(_T("[%s][%d][%d][line: %d, function: %s, file: %s]\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), message, __LINE__, __FUNCTIONW__, __FILEW__);
				WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			}
			if (theApp.m_pReloadThread)
				theApp.m_pReloadThread->SetStop();
			SE_MemoryDelete(theApp.m_pWebsocketClientThread);
			//tring strCountFile = _T("");
			CString strCount = _T("");
			//ORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
			//CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("[%d][line: %d, function: %s, file: %s]"), message, __LINE__, __FUNCTIONW__, __FILEW__);
			//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			DROP_TRACE_LOG(_T("TiorSaver_PWOFF"), strCount);
		}
	}
	if (WM_WTSSESSION_CHANGE == message) {
		{
			CString strCountFile = _T("");
			CString strCount = _T("");
			DWORD dwBytesWritten = 0;
			strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
			CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("[%s][%d][%d][line: %d, function: %s, file: %s]\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), message, __LINE__, __FUNCTIONW__, __FILEW__);
			WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
		}
		/*if (theApp)*/ {
			{
				CString strCountFile = _T("");
				CString strCount = _T("");
				DWORD dwBytesWritten = 0;
				strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
				CTime cTime = CTime::GetCurrentTime();
				strCount.Format(_T("[%s][%d][%d][line: %d, function: %s, file: %s]\r\n"), cTime.Format(_T("%Y-%m-%d %H:%M:%S")), _getpid(), message, __LINE__, __FUNCTIONW__, __FILEW__);
				WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
			}
			if (theApp.m_pReloadThread)
				theApp.m_pReloadThread->SetStop();
			SE_MemoryDelete(theApp.m_pWebsocketClientThread);
			//CString strCountFile = _T("");
			CString strCount = _T("");
			//DWORD dwBytesWritten = 0;
			//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_PWOFF"));
			//CTime cTime = CTime::GetCurrentTime();
			strCount.Format(_T("[%d][line: %d, function: %s, file: %s]"), message, __LINE__, __FUNCTIONW__, __FILEW__);
			DROP_TRACE_LOG(_T("TiorSaver_PWOFF"), strCount);
		}
	}
	return CFrameWndEx::WindowProc(message, wParam, lParam);
}


BOOL CTiorSaverTransferWnd::OnQueryEndSession()
{
	/*if (theApp)*/ {
		if (theApp.m_pReloadThread)
			theApp.m_pReloadThread->SetStop();
		SE_MemoryDelete(theApp.m_pWebsocketClientThread);
	}
	if (!CFrameWndEx::OnQueryEndSession())
		return FALSE;

	// TODO:  여기에 특수화된 쿼리 종료 세션 코드를 추가합니다.

	return TRUE;
}


int CTiorSaverTransferWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	WTSRegisterSessionNotification(theApp.m_pWnd->GetSafeHwnd(), NOTIFY_FOR_ALL_SESSIONS);
	if (CFrameWndEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  여기에 특수화된 작성 코드를 추가합니다.

	return 0;
}

BOOL CTiorSaverTransferWnd::PauseCommunicate()
{
	if (FALSE == m_bPauseCommStart)	{
		m_bPauseCommStart = TRUE;
		if (0 < m_nPauseCommSecond)
			SetTimer(TIMER_PAUSE_COMMUNICATE, m_nPauseCommSecond * 1000, NULL);
	} else {
		if (nullptr == theApp.m_pCheckPolicyThread) {
			//SetTimer(TIMER_RESUME_COMMUNICATE, EXIT_COLLECT_AGENT_RETRY_TIME, NULL);
			theApp.m_pCheckPolicyThread = (CCheckPolicyThread*)AfxBeginThread(RUNTIME_CLASS(CCheckPolicyThread), THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED);
			theApp.m_pCheckPolicyThread->m_bCheckPolicy = TRUE;
			theApp.m_pCheckPolicyThread->ResumeThread();
			KillTimer(TIMER_PAUSE_COMMUNICATE);
			return TRUE;
		}
	}
	theApp.TerminateFileTransfer();
	while (TRUE)
	{
		if (theApp.m_pCheckPolicyThread) {
			if (theApp.m_pCheckPolicyThread->m_bCheckPolicyThread) {
				theApp.m_pCheckPolicyThread->m_bCheckPolicy = FALSE;
				Sleep(CHECK_ALL_THREAD_END);
			} else {
				SE_MemoryDelete(theApp.m_pCheckPolicyThread);
				break;
			}
		} else {
			break;
		}
	}
	return TRUE;
}