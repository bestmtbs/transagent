#include "stdafx.h"
#include "CheckPolicyThread.h"
#include "TiorSaverTransfer.h"
#include "../BuildEnv/common/UtilsFile.h"
#include "../BuildEnv/common/UtilParse.h"
#include "ParamParser.h"
#include "CmsDBManager.h"

#include <yvals.h>

IMPLEMENT_DYNCREATE(CCheckPolicyThread, CWinThread)


#pragma warning(disable:4706)
/********************************************************************************
@class     CLogSendThread
@brief     로그 전송 
*********************************************************************************/

/**
@brief     생성자
@author   JHLEE
@date      2011.11.03
@note      초기화 작업
*/
CCheckPolicyThread::CCheckPolicyThread()
{
	m_bCheckPolicyThread = FALSE;
	m_bCheckPolicy = FALSE;
	//m_bSetServerIP = FALSE;
	//m_dwFileSize = 0;
}

/**
@brief     소멸자
@author   JHLEE
@date      2011.11.03
*/
CCheckPolicyThread::~CCheckPolicyThread()
{
	m_bCheckPolicyThread = FALSE;
}

BOOL CCheckPolicyThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CCheckPolicyThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCheckPolicyThread, CWinThread)
END_MESSAGE_MAP()


int CCheckPolicyThread::Run()
{
	UM_WRITE_LOG(_T("CLogSendThread::Run()"));
	m_bCheckPolicyThread = TRUE;
	CString strLog = _T("");

	while (TRUE)
	{
		strLog.Format(_T("[CCheckEachLogFileInLogFolderThread] loop per %d seconds"), 60000);
		UM_WRITE_LOG(strLog);
		// 2017-09-13 sy.choi 수집폴더확인스레드가 돌고있지 않으면 정책 체크, 있으면 정책 체크하지 않음.
		if (nullptr == theApp.m_pCheckEachLogFileInLogFolderThread) {
			if (!theApp.m_pCheckEachLogFileInLogFolderThread->m_bCheckEachLogFileInLogFolderThread) {
				m_bCheckPolicy = TRUE;
			} else {
				m_bCheckPolicy = FALSE;
				break;
			}
		} else {
			m_bCheckPolicy = TRUE;
		}
		if (m_bCheckPolicy) {
			POLICY_SET* pPolicySet = new POLICY_SET;
				if (theApp.PolicyRequest(pPolicySet)) {
					if (NULL != pPolicySet) {
						if (pPolicySet->bIsPolicyExist) {
							if (TRUE != theApp.ChangePolicy(pPolicySet)) {
								Sleep(SLEEP_REQUEST_POLICY_IF_STOP_PER_SECONDS / 10);
							}
						}
						if (0 < theApp.m_nDeleteDelay && !theApp.m_strDeleteDate.IsEmpty()) {
							theApp.StartDeleteOldFiles();
						}
						if (NULL != pPolicySet->UpdateSet) {
							theApp.UpdateStart(pPolicySet->UpdateSet);
						}
					}
				}
			Sleep(theApp.m_nPolicyRetry * 1000);
		}
	}
	m_bCheckPolicyThread = FALSE;
	//CTime tmSendTime = CTime::GetCurrentTime();
		
	UM_WRITE_LOG(_T(">>>> LogSendThreadEnd.. <<<<"))
	return 0;
}

//CStringA Utf8_Encode(CStringW strData)
//{
//	int size_needed = WideCharToMultiByte(CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), NULL, 0, NULL, NULL);
//	std::string strTo( size_needed+1, 0 );
//	WideCharToMultiByte                  (CP_UTF8, 0, strData.GetString(), (int)strData.GetLength(), &strTo[0], size_needed, NULL, NULL);
//	return strTo.c_str();
//}

//BOOL CCheckPolicyThread::PolicyRequest(POLICY_SET* pPolicySet)
//{
//	BOOL bResult = FALSE;
//	CParamParser parser;
//	CString strParam = _T("");
//	CString strUrl = _T("");
//	CString strReturn = _T("");
//	CString strToken = _T("");
//	strParam = parser.CreateKeyValueRequest();
//	//strUrl.Format(_T("http://192.168.100.112/v1/policy?%s"), strParam);
//	strUrl.Format(_T("%s/v1/policy?%s"), theApp.m_pMemberData->GetServerHostURL(), strParam);
//	int nErr = 0;
//	/*if (theApp)*/ {
//		strReturn = theApp.m_curl.CurlGET(strUrl, theApp.GetToken());
//		INT nParseResult = 0;
//		nParseResult = parser.ParserPolicySet(strReturn, strToken, pPolicySet);
//		if (1 == nParseResult) {
//			bResult = TRUE;
//			theApp.SetToken(strToken);
//		}
//		else if (-1 == nParseResult) {
//			// 2017-11-27 sy.choi
//			theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
//		}
//		else {
//			if (strToken.IsEmpty()) {
//				if (0 == nErr) {
//					strToken.Format(_T("%s"), strReturn);
//				}
//				else {
//					strToken.Format(_T("%d"), nErr);
//				}
//			}
//			//MessageBox(NULL, strToken, _T("TiorSaver"), MB_OK);
//		}
//	}
//	return bResult;
//}