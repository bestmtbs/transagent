#pragma once


class CTiorSaverTransferWnd : public CFrameWndEx
{
	//DECLARE_DYNAMIC(CTiorSaverTransferWnd)
	DECLARE_DYNCREATE(CTiorSaverTransferWnd)
public:
	BOOL m_bSendToCollect_Kill;
	BOOL m_bIsUpdate;
	BOOL m_bPauseCommStart;
	INT	 m_nPauseCommSecond;
	CTiorSaverTransferWnd(void);
//	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
//	virtual BOOL LoadFrame(UINT nIDResource, DWORD dwDefaultStyle = WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, CWnd* pParentWnd = NULL, CCreateContext* pContext = NULL);
	virtual ~CTiorSaverTransferWnd(void);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnUpdateComplete();
	afx_msg void OnDeleteStart();
	afx_msg void OnPauseCommunicate();
	void SendToExitCmd();
	BOOL SendToKillCollectAgent();
	void ExitAgentPocess();
	BOOL ChangeFileName(CString _strChangeFileName);
	BOOL SetPrivilege(HANDLE hToken, LPCTSTR Privilege, BOOL bEnablePrivilege);
	BOOL KillTiorProcess(CString _strFIlePath);
	BOOL DeleteService();
	BOOL CheckVaildServiceName(const CString &_sServiceName);
	BOOL ExistsService(const CString &_sSvcName);
	void KillReloadAndServiceUnregister();
	BOOL StartUninstallProc();
	BOOL PauseCommunicate();
	DECLARE_MESSAGE_MAP()
	//afx_msg void OnClose();
//	afx_msg void OnDestroy();
	afx_msg void OnEndSession(BOOL bEnding);
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnQueryEndSession();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
