
// TiorSaverReload.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.


#include "stdafx.h"
#include "TiorSaverReload.h"

#include "UtilsProcess.h"
#include "WinOsVersion.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include "GeneralUtil.h"
#include "UtilsReg.h"

#include <winsvc.h>
#include "yvals.h"


#pragma  comment(lib, "Wtsapi32.lib")

IMPLEMENT_DYNCREATE(CReloadThread, CWinThread)

// CTiorSaverReloadApp 생성

CReloadThread::CReloadThread()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
	m_bIs64bit = FALSE;
	m_bStop = FALSE;
	m_nOsProduct = 9;
}

/**
 @brief     소멸자
 @author    kh.choi
 @date      2017-05-17
*/
CReloadThread::~CReloadThread(void)
{
	INT nCnt = 0;
}

BOOL CReloadThread::InitInstance()
{
	CWinOsVersion osVer;
	m_bIs64bit = osVer.Is64bit();
	m_nOsProduct = osVer.GetProduct();
	BOOL bFirstStartFlag= TRUE;
	m_hMod = LoadLibrary(_T("kernel32.dll"));
	if (m_hMod != NULL)
	{
		m_Process.m_pCreateToolhelp32Snapshot = (fpCreateToolhelp32Snapshot) ::GetProcAddress(m_hMod, "CreateToolhelp32Snapshot");
		m_Process.m_pProcess32First = (fpProcess32First) ::GetProcAddress(m_hMod, PROCESS32FIRST);
		m_Process.m_pProcess32Next = (fpProcess32Next) ::GetProcAddress(m_hMod, PROCESS32NEXT);
	} else {
		return FALSE;
	}
	return TRUE;
}

int CReloadThread::ExitInstance()
{
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CReloadThread, CWinThread)
END_MESSAGE_MAP()

int CReloadThread::Run()
{
	static BOOL bFirst = TRUE;
	static BOOL bFirstUpgrade = TRUE;
	while (TRUE)
	{
		CString strSysAgent = _T("");
		CString strSysAgentMutex = _T("");
		CString strUsrAgent = _T("");
		CString strUsrAgentMutex = _T("");
		//CString strTransferPath = _T("");
		//CString strTransferMutex = _T("");

		CImpersonator imp;

		//나머지 프로세스들은 윈도우에 로그인하면 구동시키도록한다.
		BOOL bIsWindowLogin = m_Process.IsWindowLogin();
		DBGLOG(_T("[bIsWindowLogin] %d[line: %d, function: %s, file: %s]"), bIsWindowLogin, __LINE__, __FUNCTIONW__, __FILEW__);
		if (bIsWindowLogin)
		{
				if (m_bIs64bit) {
					if (m_nOsProduct < osWinVista)
						strUsrAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_USR_AGENT_XP_64_NAME);
					else
						strUsrAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_USR_AGENT_64_NAME);
					strUsrAgentMutex.Format(_T("%s"), WTIOR_USR_AGENT_64_MUTEX_NAME);
				}
				else {
					if (m_nOsProduct < osWinVista)
						strUsrAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_USR_AGENT_XP_32_NAME);
					else
						strUsrAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_USR_AGENT_32_NAME);
					strUsrAgentMutex.Format(_T("%s"), WTIOR_USR_AGENT_32_MUTEX_NAME);
				}
				m_Process.CheckTiorProcess(strUsrAgent, strUsrAgentMutex);  //hhh: test
				if (bFirst)
				{
					bFirst = FALSE;
					Sleep(10000);
				}

				if (m_bIs64bit) {
					strSysAgentMutex.Format(_T("%s"), WTIOR_SYS_AGENT_64_MUTEX_NAME);
					if (m_nOsProduct < osWinVista)
						strSysAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_SYS_AGENT_XP_64_NAME);
					else
						strSysAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_SYS_AGENT_64_NAME);
				}
				else {
					strSysAgentMutex.Format(_T("%s"), WTIOR_SYS_AGENT_32_MUTEX_NAME);
					if (m_nOsProduct < osWinVista)
						strSysAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_SYS_AGENT_XP_32_NAME);
					else
						strSysAgent.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_SYS_AGENT_32_NAME);
				}

				if (false == CProcess::IsExistMutex(strSysAgentMutex)) {
					imp.CreateProcessEx(strSysAgent);
					Sleep(500);
				}

				if (bFirstUpgrade)
				{
					CString strUpdate = _T("");
					CString strUpdateMutex = _T("");
					strUpdate.Format(L"%s%s -a", CPathInfo::GetClientInstallPath(), WTIOR_UPDATE_AGENT_NAME);
					strUpdateMutex.Format(_T("%s"), WTIOR_UPDATE_AGENT_MUTEX_NAME);

					if (false == CProcess::IsExistMutex(strUpdateMutex)) {
						BOOL bExecute = imp.CreateProcess(strUpdate, SW_HIDE);
						if (bExecute)
							bFirstUpgrade = FALSE;
					}
				}
			//}
		}
		if (FALSE == GetStop()) {
			ServiceCheck();
		}
		imp.Free();
		Sleep(2000);
	}
}
void CReloadThread::SetStop()
{
	m_bStop = TRUE;
}

BOOL CReloadThread::GetStop()
{
	return m_bStop;
}

void CReloadThread::ServiceCheck()
{
	if (TRUE == GetStop())
		return;
	//서비스의 프로세스의 Thread를 resumeThread로 전환한다.
	DWORD dwOfsSvcPid = GetProcessID(WTIOR_AGENT_SERVICE_NAME);
	if(dwOfsSvcPid > 0)
	{
		if(FALSE == StopAT_Suspend() )
			ResumeProcess(dwOfsSvcPid);
	}
	

	CString strLog;
	CImpersonator imp;
	CString strServicePath = _T("");
	strServicePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);

	UM_WRITE_LOG(_T("[TiorSaverReload_s] --ServiceCheck start"));	

	SC_HANDLE hScManage_Handle=  NULL, hService = NULL;
	hScManage_Handle = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);

	if(hScManage_Handle != NULL)
	{
		UM_WRITE_LOG(_T("[TiorSaverReload_s] --OpenSCManager is Good"));	
		DWORD dwError;
		hService = OpenService(hScManage_Handle, TS_SERVICE_NAME, SERVICE_ALL_ACCESS);
		
		if (hService == NULL) 
		{
			dwError = GetLastError();
			
			//서비스가 설치되어 있지 않다면 재설치를 진행한다.
			if(dwError == ERROR_SERVICE_DOES_NOT_EXIST)
			{									
				if( imp.CreatProcessAndCompareStr(strServicePath,  "ok", _T("i")) )
				{
					strLog.Format(_T("[TiorSaverReload_s] Service Reinstall start.."));
					UM_WRITE_LOG(strLog);			
				}
				else
				{
					strLog.Format(_T("[TiorSaverReload_s] Service Reinstall start Fail : %d"), GetLastError() );
					UM_WRITE_LOG(strLog);									
				}						
			}					
		}
		else			//서비스가 있다면 동작상태 체크
		{
			if (TRUE == GetStop()) {
				imp.Free();
				if (hScManage_Handle)
					CloseServiceHandle(hScManage_Handle);
				if (hService)
					CloseServiceHandle(hService);
				return;
			}
			strLog.Format(_T("[TiorSaverReload_s] OpenService sSuccess") );
			UM_WRITE_LOG(strLog);	

			SERVICE_STATUS ss;
			if( ControlService(hService, SERVICE_CONTROL_INTERROGATE, &ss) )
			{
				strLog.Format(_T("[TiorSaverReload_s] ControlService sSuccess: %d"), ss.dwCurrentState );
				UM_WRITE_LOG(strLog);	

				if(ss.dwCurrentState == SERVICE_PAUSED)		//서비스가 일시중지 라면
				{
					strLog.Format(_T("[TiorSaverReload_s] Service -- SERVICE_PAUSED") );
					UM_WRITE_LOG(strLog);	

					if( false == imp.CreatProcessAndCompareStr(strServicePath,  "ok", _T("c")) )
					{
						strLog.Format(_T("[TiorSaverReload_s] Service continueFail : %d"), GetLastError() );
						UM_WRITE_LOG(strLog);	
					}
				}
			}
			else
			{	
				strLog.Format(_T("[TiorSaverReload_s] ControlService Fail") );
				UM_WRITE_LOG(strLog);	

				DWORD dwError =GetLastError();
				if(dwError == ERROR_SERVICE_NOT_ACTIVE)		//서비스가 중지된 상태
				{
					strLog.Format(_T("[TiorSaverReload_s] Service -- SERVICE_STOPPED") );
					UM_WRITE_LOG(strLog);	
					//strServicePath.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), SVC_PROC_NAME);
					if( false == imp.CreatProcessAndCompareStr(strServicePath,  "ok", _T("s")) )
					{
						strLog.Format(_T("[TiorSaverReload_s] Service start Fail : %d"), GetLastError() );
						UM_WRITE_LOG(strLog);	
					}				
				}
				
			}

			LPQUERY_SERVICE_CONFIG lpsc; 
			BYTE ServeConfig[400] ={0,};
			DWORD dwBytesNeeded, cbBufSize, dwError; 
			cbBufSize = 400;
			lpsc = (LPQUERY_SERVICE_CONFIG)&ServeConfig;
			//서비스의 시작 상태값 확인
			/*if( !QueryServiceConfig(hService, NULL, 0, &dwBytesNeeded))
			{
				strLog.Format(L"[TiorSaverReload_s] dwBytesNeeded : %d", dwBytesNeeded);
				UM_WRITE_LOG(strLog);
				dwError = GetLastError();
				if( ERROR_INSUFFICIENT_BUFFER == dwError )
				{
					cbBufSize = dwBytesNeeded;
					lpsc = (LPQUERY_SERVICE_CONFIG) LocalAlloc(LMEM_FIXED, cbBufSize);
				}				
			}*/
  
			if( !QueryServiceConfig( hService, lpsc, cbBufSize, &dwBytesNeeded) ) 
			{				
				dwError = GetLastError();
				strLog.Format(L"[TiorSaverReload_s] QueryServiceConfig  2 Error : %d", dwError);
				UM_WRITE_LOG(strLog);	
			}

			strLog.Format(L"[TiorSaverReload_s] - QueryServiceConfig Result: %d", lpsc->dwStartType);
			UM_WRITE_LOG(strLog);

			//시작 유형이 바꼈다면 원래대로 돌려놓는다.
			if(lpsc->dwStartType != SERVICE_AUTO_START)
			{
				CString strServicePath;
				strServicePath.Format(L"%s%s", CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
				if(	FALSE == ChangeServiceConfig(hService, 
										SERVICE_WIN32_OWN_PROCESS | SERVICE_INTERACTIVE_PROCESS,
										SERVICE_AUTO_START,
										SERVICE_ERROR_NORMAL,
										strServicePath.GetBuffer(0),
										NULL,
										NULL,
										NULL,
										NULL,
										NULL,
										L"TiorSaver SERVICE") )
				{
					dwError = GetLastError();
					strLog.Format(L"[TiorSaverReload_s] ChangeServiceConfig  Error : %d", dwError);
					UM_WRITE_LOG(strLog);	
				}



			}

		}

		imp.Free();		
		if(hScManage_Handle)
			CloseServiceHandle(hScManage_Handle);
		if(hService)
			CloseServiceHandle(hService);
	}
	else
	{
		strLog.Format(_T("[TiorSaverReload_s] OpenSCManager Fail.. Error: %d"), GetLastError() );
		UM_WRITE_LOG(strLog);
	}



}

/**
@brief     ResumeThread 기능을 동작할건지 여부 체크
@author    hhh
@date      2013.07.26
@return	   BOOL
*/
BOOL CReloadThread::StopAT_Suspend()
{
	DWORD dwDefalut = 0, dwRet = 0;
	dwRet = GetRegDWORDValue(HKEY_LOCAL_MACHINE, DBG_LOG_DEFAULT_KEY, L"ATSuspend", dwDefalut);
	if(dwRet == 1)
		return TRUE;
	else
		return FALSE;
}

//BOOL CReloadThread::ChangeFileName(CString _strChangeFileName)
//{
//	BOOL bResult = FALSE;
//	CString strFileFullpath, strTempName, strLog;
//	try
//	{
//		strFileFullpath.Format( _T("%s%s"), CPathInfo::GetClientInstallPath(), _strChangeFileName) ;
//
//		strTempName.Format( _T("%s_%s"), CPathInfo::GetClientInstallPath(), _strChangeFileName) ;  //_가 붙은 파일이름으로 변경
//		//이미 백업된 파일이 있다면 삭제 후 ReName한다.
//
//		UM_WRITE_LOG(strFileFullpath + _T("  ->") + strTempName);
//
//		CFile::Rename( strFileFullpath, strTempName);
//		bResult = TRUE;
//	}
//	catch (CFileException* pEx)
//	{
//		strLog.Format(_T("ChangeFileName Error : %d"), pEx->m_cause);
//		UM_WRITE_LOG(strLog);
//		pEx->Delete();
//		bResult = FALSE;
//	}
//	return bResult;
//}

//BOOL CReloadThread::KillTiorProcess(CString _strFIlePath)
//{
//	CString strLog = _T("");
//
//	HANDLE hToken;
//	OpenProcessToken(GetCurrentProcess(),
//		TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY,
//		&hToken);
//
//	SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);
//
//	//BOOL bRet = CProcess::KillProcess(_strFIlePath);	// 2017-09-15 sy.choi 프로세스가 실행중이 아닐때에도 TRUE 리턴하므로
//	BOOL bRet = CProcess::KillProcessByName(_strFIlePath);
//	strLog.Format(_T("Kill ret : %d"), bRet);
//	UM_WRITE_LOG(strLog);
//
//	SetPrivilege(hToken, SE_DEBUG_NAME, FALSE);
//	CloseHandle(hToken);
//	return bRet;
//}

//BOOL CReloadThread::DeleteService()
//{
//	if (FALSE == CheckVaildServiceName(TS_SERVICE_NAME)) {
//		return TRUE;
//	}
//	if (FALSE == ExistsService(TS_SERVICE_NAME)) {
//		return TRUE;
//	}
//	CImpersonator imp;
//	CString strServiceName = _T("");
//	strServiceName.Format(_T("%s%s"), CPathInfo::GetClientInstallPath(), WTIOR_AGENT_SERVICE_NAME);
//	if( imp.CreatProcessAndCompareStr(strServiceName,  "ok", _T("u")) )
//	{
//		imp.Free();
//		return TRUE;
//	}
//	else
//	{
//		imp.Free();
//		return FALSE;
//	}
//}
BOOL CReloadThread::SetPrivilege(
							  HANDLE hToken,   // 토큰 핸들
							  LPCTSTR Privilege,   // 활성/비활성화할 권한
							  BOOL bEnablePrivilege  // 권한 활성화 여부?
							  )
{
	DWORD dwErr = 0;
	TOKEN_PRIVILEGES tp;
	LUID luid;
	TOKEN_PRIVILEGES tpPrevious;
	DWORD cbPrevious=sizeof(TOKEN_PRIVILEGES);

	if (!LookupPrivilegeValue( NULL, Privilege, &luid ))
		return FALSE;

	// 현재의 권한 설정 얻기
	tp.PrivilegeCount   = 1;
	tp.Privileges[0].Luid   = luid;
	tp.Privileges[0].Attributes = 0;

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tp,
		sizeof(TOKEN_PRIVILEGES),
		&tpPrevious,
		&cbPrevious
		);

	dwErr = GetLastError();
	if (dwErr != ERROR_SUCCESS)
		return FALSE;

	// 이전의 권한 설정에 따라 권한 설정하기
	tpPrevious.PrivilegeCount       = 1;
	tpPrevious.Privileges[0].Luid   = luid;

	if (bEnablePrivilege) {
		tpPrevious.Privileges[0].Attributes |= (SE_PRIVILEGE_ENABLED);
	}
	else {
		tpPrevious.Privileges[0].Attributes ^= (SE_PRIVILEGE_ENABLED &
			tpPrevious.Privileges[0].Attributes);
	}

	AdjustTokenPrivileges(
		hToken,
		FALSE,
		&tpPrevious,
		cbPrevious,
		NULL,
		NULL
		);

	dwErr = GetLastError();
	if (dwErr != ERROR_SUCCESS)
		return FALSE;

	return TRUE;
}

#define MAXLEN_SERVICENAME              182        /**< 서비스 이름 최대 길이NT이상OS (NULL 종료문자를 제외한 최대 길이)      */
#define MAXLEN_NTSERVICENAME            80         /**< 서비스 이름 최대 길이 NT에서만(NULL 종료문자를 제외한 최대 길이)      */
BOOL CReloadThread::CheckVaildServiceName(const CString &_sServiceName)
{    
	if (_T("") == _sServiceName) 
		return false;

	CWinOsVersion OSVersion;
	if (osWinNT == OSVersion.GetProduct()) 
	{
		if (MAXLEN_NTSERVICENAME < _sServiceName.GetLength()) 
			return false;
	}
	else
	{
		if (MAXLEN_SERVICENAME < _sServiceName.GetLength()) 
			return false;
	}

	if (-1 != _sServiceName.Find(_T("/"))) 
		return false;    
	if (-1 != _sServiceName.Find(_T("\\")))
		return false;

	return true;
}

BOOL CReloadThread::ExistsService(const CString &_sSvcName)
{

	SC_HANDLE schService = NULL;
	SC_HANDLE schSCManager = NULL;

	if (!CheckVaildServiceName(_sSvcName)) 
		return false;    

	schSCManager = ::OpenSCManager(NULL, NULL, GENERIC_READ);
	if (NULL == schSCManager)
		return false;

	schService = ::OpenService(schSCManager, _sSvcName, SERVICE_QUERY_STATUS);        
	if (NULL == schService)    
	{
		::CloseServiceHandle(schSCManager);
		return false;
	}

	::CloseServiceHandle(schService);
	::CloseServiceHandle(schSCManager);

	return true;
}