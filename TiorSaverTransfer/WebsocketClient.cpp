#include "stdafx.h"
#include "WebsocketClient.h"
#include "TiorSaverTransfer.h"
#include "PathInfo.h"
#include "UtilsFile.h"
#include "ParamParser.h"

IMPLEMENT_DYNCREATE(CWebsocketClient, CWinThread)

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

extern CTiorSaverTransferApp theApp;
CWebsocketClient::CWebsocketClient(void)
{
	m_strUri = "";
	//theApp.m_bWSConnectDone = FALSE;
	//theApp.m_bWSOpen = FALSE;
	m_pClient = nullptr;
	//m_pLock = NULL;
	m_bWebsocketClientStart = FALSE;
	m_hConnectEvent = NULL;
	CreateConnectEvent();
	m_pLock = new websocketpp::lib::mutex;
}

CWebsocketClient::CWebsocketClient(CString _strUri)
{
	//theApp.m_bWSOpen = FALSE;
	//theApp.m_bWSConnectDone = FALSE;
	m_strUri = _strUri;
	m_pClient = nullptr;
	//m_pLock = NULL;
	m_bWebsocketClientStart = FALSE;
	m_hConnectEvent = NULL;
	CreateConnectEvent();
	m_pLock = new websocketpp::lib::mutex;
}

CWebsocketClient::~CWebsocketClient()
{
	m_bWebsocketClientStart = FALSE;
	SE_MemoryDelete(m_pClient);
	CloseHandle(this->m_hConnectEvent);
	SE_MemoryDelete(m_pLock);
}

BOOL CWebsocketClient::InitInstance()
{
	// TODO: ���⿡�� �� �����忡 ���� �ʱ�ȭ�� �����մϴ�.
	m_bWebsocketClientStart = FALSE;
	return TRUE;
}

int CWebsocketClient::ExitInstance()
{
	// TODO: ���⿡�� �� �����忡 ���� ������ �����մϴ�.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	m_bWebsocketClientStart = FALSE;
	ResetEvent(this->m_hConnectEvent);
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CWebsocketClient, CWinThread)
END_MESSAGE_MAP()

void CWebsocketClient::InitDomain(CString _strUri)
{
	m_strUri = _strUri;
}
// Handlers                                                 
void CWebsocketClient::on_open(websocketpp::connection_hdl hdl) {
	//theApp.m_bWSOpen = TRUE;
	scoped_lock guard(*m_pLock);
	std::string msg = "Hello";
	m_pClient->send(m_pClientHdl, msg, websocketpp::frame::opcode::text);
	//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Sent Message: " + msg);
	//CString strCountFile = _T("");
	//DWORD dwBytesWritten = 0;
	//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_OnOpen"));
	//CTime cTime = CTime::GetCurrentTime();
	//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
	DROP_TRACE_LOG(_T("TiorSaver_OnOpen"), _T(""));
	//MessageBox(NULL, strCount, _T("TiorSaver"), MB_OK);
}

void CWebsocketClient::on_fail(websocketpp::connection_hdl hdl) {
	//theApp.m_bWSOpen = FALSE;
	//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Connection Failed");
	//m_pClient->close(hdl, websocketpp::close::status::going_away, "");
	scoped_lock guard(*m_pLock);
	if (!m_pClientHdl.expired()) {
		m_pClientHdl.reset();
	}
	if (!hdl.expired()) {
		hdl.reset();
	}
	//theApp.m_bWSConnectDone = TRUE;
	//CString strCountFile = _T("");
	//CString strCount = _T("");
	//DWORD dwBytesWritten = 0;
	//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_OnFail"));
	DROP_TRACE_LOG(_T("TiorSaver_OnFail"), _T(""));
	Sleep(1000);
	this->SetConnectEvent();
}

void CWebsocketClient::on_message(websocketpp::connection_hdl hdl, message_ptr msg) {
	//theApp.m_bWSOpen = TRUE;
	//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Received Reply: " + msg->get_payload());
	//c->close(hdl, websocketpp::close::status::normal, "");
	scoped_lock guard(*m_pLock);
	m_strOnMessage = CA2CT(msg->get_payload().c_str());
	CParamParser parser;
	CString strClientID = _T("");
	CString strHello = _T("");
	strClientID = parser.ParserClientID(m_strOnMessage);
	if (-1 != m_strOnMessage.Find(_T("clientConnectError"))) {
		m_pClient->stop();
		this->SetConnectEvent();
		//theApp.m_bWSOpen = FALSE;
		//theApp.m_bWSConnectDone = TRUE;
	}
	if (!strClientID.IsEmpty()) {
		strHello = parser.CreateClientID(strClientID);
		CT2CA pszConvertedAnsiString(strHello);
		std::string msgHello(pszConvertedAnsiString);
		websocketpp::lib::error_code ec;
		m_pClient->send(m_pClientHdl, msgHello, websocketpp::frame::opcode::text, ec);
		if (ec) {
			//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Send Error: " + ec.message());
			//break;
			Sleep(1000);
			this->SetConnectEvent();
		} else {
			m_bWebsocketClientStart = TRUE;
		}
		//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Sent Message: " + msgHello);
	} else if (-1 != m_strOnMessage.Find(_T("policy"))){
		CString strToken = _T("");
		POLICY_SET* pPolicySet = new POLICY_SET;
		if (parser.ParserPolicySetForWebSocket(m_strOnMessage, strToken, pPolicySet)) {
			theApp.SetToken(strToken);
			if (NULL != pPolicySet) {
				if (NULL != pPolicySet) {
					if (pPolicySet->bIsPolicyExist) {
						theApp.ChangePolicy(pPolicySet);
					}
					if (NULL != pPolicySet->UpdateSet) {
						theApp.UpdateStart(pPolicySet->UpdateSet);
					}
				}
			}
		}
	} else if (-1 != m_strOnMessage.Find(_T("screen"))) {
		BOOL bStart = FALSE;
		if (parser.ParserThumnailAction(m_strOnMessage, bStart)) {
			theApp.PipeThumnailAction(bStart);
		}
	}
	CString strCount = _T("");
	strCount.Format(_T("[send]%s[recv]%s"), strHello, m_strOnMessage);
	DROP_TRACE_LOG(_T("TiorSaver_OnMSG"), strCount);
 }

void CWebsocketClient::on_close(websocketpp::connection_hdl hdl) {
	//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Connection Closed");
	//theApp.m_bWSConnectDone = TRUE;
	//m_pClient->close(hdl, websocketpp::close::status::going_away, "");
	scoped_lock guard(*m_pLock);
	if (!m_pClientHdl.expired()) {
		m_pClientHdl.reset();
	}
	if (!hdl.expired()) {
		hdl.reset();
	}
	
	//scoped_lock guard(*m_pLock);
	DROP_TRACE_LOG(_T("TiorSaver_OnClose"), _T(""));
	Sleep(1000);
	if (m_bWebsocketClientStart) {
		this->SetConnectEvent();
	}
}

#ifdef _TEST_CHOI_SSL_
context_tls_ptr CWebsocketClient::on_tls_init(websocketpp::connection_hdl hdl) {
	context_tls_ptr ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::sslv23);
	//ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::sslv23);

	try {
		ctx->set_options(boost::asio::ssl::context::default_workarounds |
			boost::asio::ssl::context::no_sslv2 |
			boost::asio::ssl::context::no_sslv3 |
			boost::asio::ssl::context::single_dh_use);
	}
	catch (std::exception& e) {
		std::cout << "Error in context pointer: " << e.what() << std::endl;
	}
	return ctx;
}
#endif

inline std::string to_string(const CString& cst)
{
	CT2CA pszConvertedAnsiString(cst);
	std::string strStd(pszConvertedAnsiString);
	return strStd;
}

int CWebsocketClient::Run() {
	while (TRUE)
	{
		DWORD dwResult = WaitForSingleObject(m_hConnectEvent, INFINITE);
		if (WAIT_OBJECT_0 == dwResult) {
			while (TRUE)
			{
				if (theApp.m_pReloadThread->GetStop())
					return -1;
				SE_MemoryDelete(m_pClient);
				m_pClient = new client_tls;
				//m_pClient->clear_access_channels(websocketpp::log::alevel::all);
				m_pClient->set_access_channels(websocketpp::log::alevel::app | websocketpp::log::alevel::connect);
				m_pClient->set_error_channels(websocketpp::log::elevel::all);

				// Initialize ASIO
				m_pClient->init_asio();

				// Register our handlers
				//m_pClient->set_socket_init_handler(bind(&CWebsocketClient::on_socket_init, this, ::_1));
#ifdef _TEST_CHOI_SSL_
				m_pClient->set_tls_init_handler(bind<context_tls_ptr>(&CWebsocketClient::on_tls_init, this, ::_1));
#endif
				m_pClient->set_message_handler(bind(&CWebsocketClient::on_message, this, ::_1, ::_2));
				m_pClient->set_open_handler(bind(&CWebsocketClient::on_open, this, ::_1));
				m_pClient->set_close_handler(bind(&CWebsocketClient::on_close, this, ::_1));
				m_pClient->set_fail_handler(bind(&CWebsocketClient::on_fail, this, ::_1));

				// Create a connection to the given URI and queue it for connection once
				// the event loop starts
				websocketpp::lib::error_code ec;
				//CStringA strUriA = theApp.Utf8_Encode(m_strUri);
				//CT2CA pszConvertedAnsiString(m_strUri);
				std::string sdstrUri = to_string(m_strUri);
				//std::string sdstrUri = "ws://52.78.113.115:3001/agent";
				connection_tls_ptr ClientConnection = m_pClient->get_connection(sdstrUri, ec);
				if (ec) {
					//m_pClient->get_alog().write(websocketpp::log::alevel::app, "Get Connection Error: " + ec.message());

					//CString strCountFile = _T("");
					//DWORD dwBytesWritten = 0;
					//strCountFile.Format(_T("%s%s"), CPathInfo::GetTransferLogFilePath(), _T("TiorSaver_OnError"));
					CString strCount(ec.message().c_str());
					//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
					DROP_TRACE_LOG(_T("TiorSaver_OnError"), strCount);
					//theApp.m_bWSOpen = FALSE;
					//theApp.m_bWSConnectDone = TRUE;
					Sleep(100);
				}
				if (NULL != ClientConnection) {
					m_pClientHdl = ClientConnection->get_handle();
					if (m_pClient->connect(ClientConnection)) {
						//m_start = std::chrono::high_resolution_clock::now();

						// Create a thread to run the ASIO io_service event loop
						//websocketpp::lib::thread asio_thread(&client_tls::run, m_pClient);
						// Create a thread to run the telemetry loop
						//asio_thread.join();
						try
						{
							m_pClient->run();
							break;
						}
						catch (boost::system::error_code ec)
						{
							CString strCount(ec.message().c_str());
							//WriteFileExample(strCountFile, strCount, dwBytesWritten, TRUE);
							DROP_TRACE_LOG(_T("TiorSaver_OnError"), strCount);
							//theApp.m_bWSOpen = FALSE;
							//theApp.m_bWSConnectDone = TRUE;
							Sleep(100);
						}
					}
				}
			}
		}
	}
	m_bWebsocketClientStart = FALSE;
	return 1;
}