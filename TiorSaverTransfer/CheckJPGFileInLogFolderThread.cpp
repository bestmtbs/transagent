/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.11.03	
********************************************************************************/
/**
@file       CLogSendThread.cpp
@brief     LogSendThread  구현 파일
@author	 jhlee
@date      create 2011.11.03
*/
// LogSendThread.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "CheckJPGFileInLogFolderThread.h"
#include "TiorSaverTransfer.h"
#include "Impersonator.h"
#include "UtilParse.h"
#include "UtilsFile.h"
#include "ParamParser.h"
#include "CmsDBManager.h"
#include "PathInfo.h"
#include "Crypto/Base64.h"

#include <yvals.h>

// CLogSendThread

IMPLEMENT_DYNCREATE(CCheckJPGFileInLogFolderThread, CWinThread)


#pragma warning(disable:4706)
/********************************************************************************
@class     CLogSendThread
@brief     로그 전송 
*********************************************************************************/


CCheckJPGFileInLogFolderThread::CCheckJPGFileInLogFolderThread()
{
	m_bCheckJPGFileInLogFolderThread  = FALSE;
	//m_nScanSleep = 5;
	//m_bSetServerIP = FALSE;
	//m_dwFileSize = 0;
	m_bSendThumbThreadStart = FALSE;
	m_bIsNewJPG = FALSE;
}

CCheckJPGFileInLogFolderThread::~CCheckJPGFileInLogFolderThread()
{
	m_bCheckJPGFileInLogFolderThread = FALSE;
}

BOOL CCheckJPGFileInLogFolderThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.
	return TRUE;
}

int CCheckJPGFileInLogFolderThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCheckJPGFileInLogFolderThread, CWinThread)
END_MESSAGE_MAP()


int CCheckJPGFileInLogFolderThread::Run()
{
	//UM_WRITE_LOG(_T("CLogSendThread::Run()"));
	m_bCheckJPGFileInLogFolderThread = TRUE;
	CStringArray arrList;
	CString strCmd = _T("");
	CString strQuery = _T("");
	CString strLog = _T("");
	arrList.RemoveAll();
	//CCmsDBManager dbManage;
	//dbManage.SelectAllQuery(_T("select * from DB_SEND_SCR_LIST;"), arrList);
	m_nMaxIdx = arrList.GetSize();
	m_nMaxIdx = 0;	// TODO: 스레드 관리 죽었을때 디비에 있는건 어찌해야해..
	//arrTemp.RemoveAll();
	strCmd.Format(_T("%sthumb.jpg"), CPathInfo::GetJPGFilePath());
	UM_WRITE_LOG(strQuery);
	
	while (TRUE)
	{
		m_bIsNewJPG = FALSE;
		strLog.Format(_T("[CCheckJPGFileInLogFolderThread] times"));
		UM_WRITE_LOG(strLog);
		FindJpgUpdate(strCmd/*, arrFiles, arrSizeOfFiles*/);
		if (m_bIsNewJPG) {
			SendThumb();
		}
		//CheckOffset(nCnt);
		Sleep(/*theApp.m_nScanDelay * 1000*/1000);
		if (!theApp.m_bIsCollecting) {
			//TerminateAllLogSizeThreads();
			//break;
			Sleep(/*theApp.m_nScanDelay * 1000*/1000);
		}
	}
	m_bCheckJPGFileInLogFolderThread = FALSE;
	return 0;
}

void CCheckJPGFileInLogFolderThread::FindJpgUpdate(CString _strPath/*, CStringArray& arrFiles, CUIntArray& arrSizeOfFiles*/)
{
	CString strQuery = _T("");
	//while (bFind)
	//{
	//	bFind = Finder.FindNextFile();
	//	if (Finder.IsDots())
	//		continue;

		//if (Finder.IsDirectory() && !Finder.IsDots())
		//{
		//	CString strFolder = Finder.GetFilePath();
		//	FindOldestWrittenFile(strFolder/*, arrFiles, arrSizeOfFiles*/);
		//} else if (!Finder.IsDots())
		//{
			//CTime ctLastWriteTime;
			//CString strFilePath = Finder.GetFilePath();
			//UINT nFileSize = Finder.GetLength();
			CString strLastWriteTime = _T("");
			CString strFilePath = _strPath;
			HANDLE hFile = CreateFile(strFilePath, GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (hFile == INVALID_HANDLE_VALUE) {
				CloseHandle(hFile);
				return;
			}
			FILETIME create_time, access_time, write_time, write_local;
			if (!GetFileTime(hFile, &create_time, &access_time, &write_time)) {
				CloseHandle(hFile);
				return;
			}
			SYSTEMTIME write_system_time, write_local_time;
			if (!FileTimeToLocalFileTime(&write_time, &write_local)) {
				CloseHandle(hFile);
				return;
			}
			FileTimeToSystemTime(&write_local, &write_system_time);
			SystemTimeToTzSpecificLocalTime(NULL, &write_system_time, &write_local_time);
			CTime ctLastWriteTime(write_local_time);
			//Finder.GetLastWriteTime(ctLastWriteTime);
			strLastWriteTime = ctLastWriteTime.Format(_T("%Y-%m-%d %H:%M:%S"));
			//if (0 == ExtractFileName(strFilePath).CompareNoCase(_T("thumb.jpg"))) {
				strQuery.Format(_T("select last_write_date from %s where full_path like '%%%s%%' and last_write_date < \"%s\""), DB_SEND_LOG_LIST, strFilePath, strLastWriteTime);
				CStringArray arrJPG;
				arrJPG.RemoveAll();
#ifdef _MGCHOI_TEST_
				if (theApp.m_pDbLog) {
					theApp.m_pDbLog->SelectQuery(strQuery, &arrJPG, _T("last_write_date"));
				}
#else	//#ifdef _MGCHOI_TEST_
				CCmsDBManager dbManageJPG(DB_SEND_LOG_LIST);
				dbManageJPG.SelectQuery(strQuery, &arrJPG, _T("last_write_date"));
				dbManageJPG.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
				if (0 < arrJPG.GetSize()) {
					strQuery = _T("");
					strQuery.Format(_T("update %s set last_write_date = '%s' where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, strLastWriteTime, strFilePath);
					UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
					if (theApp.m_pDbLog) {
						theApp.m_pDbLog->UpdateQuery(strQuery);
					}
#else	//#ifdef _MGCHOI_TEST_
					CCmsDBManager dbManage3(DB_SEND_LOG_LIST);
					dbManage3.UpdateQuery(strQuery);
					dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
					m_bIsNewJPG = TRUE;
					m_strJPG = strFilePath;
				} else {
			//} else {
			//arrFiles.Add(strFilePath);
			//arrSizeOfFiles.Add(nFileSize);
			//if (0 == ExtractFileName(strFilePath).CompareNoCase(_T("thumb.jpg"))) {
				CStringArray arrSendList;
				arrSendList.RemoveAll();
				strQuery = _T("");
				strQuery.Format(_T("select current_offset, file_size from %s where full_path like '%%%s%%'"), DB_SEND_LOG_LIST, strFilePath);
				UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
				if (theApp.m_pDbLog) {
					theApp.m_pDbLog->SelectQuery(strQuery, &arrSendList, _T("current_offset"), _T("file_size"));
				}
#else	//#ifdef _MGCHOI_TEST_
				CCmsDBManager dbManage3(DB_SEND_LOG_LIST);
				dbManage3.SelectQuery(strQuery, &arrSendList, _T("current_offset"), _T("file_size"));
				dbManage3.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
				if (0 >= arrSendList.GetSize()) {
					strQuery = _T("");
					strQuery.Format(_T("insert into %s (full_path, thread_idx, last_write_date) values ('%s', %d, '%s')"), DB_SEND_LOG_LIST, strFilePath, 0, strLastWriteTime);
					UM_WRITE_LOG(strQuery);
#ifdef _MGCHOI_TEST_
					if (theApp.m_pDbLog) {
						theApp.m_pDbLog->InsertQuery(strQuery);
					}
#else	//#ifdef _MGCHOI_TEST_
					CCmsDBManager dbManage2(DB_SEND_LOG_LIST);
					dbManage2.InsertQuery(strQuery);
					dbManage2.Free();
#endif	//#else	//#ifdef _MGCHOI_TEST_
					m_bIsNewJPG = TRUE;
					m_strJPG = strFilePath;
				}/* else {
					CStringArray arrData;
					CParseUtil::ParseStringToStringArray(_T("|"), arrSendList.GetAt(0), &arrData);
					if (nFileSize != _ttoi(arrData.GetAt(1))) {
						strQuery = _T("");
						strQuery.Format(_T("update %s set last_write_date = '%s' where full_path like '%%%s%%'"), DB_SEND_SCR_LIST, strLastWriteTime, strFilePath);
						CCmsDBManager dbManage3(DB_SEND_SCR_LIST);
						dbManage3.UpdateQuery(strQuery);
						UM_WRITE_LOG(strQuery);
						dbManage3.Free();
					}
				}*/
			//}
		}
				CloseHandle(hFile);
	//}
	//Finder.Close();
	//strQuery = _T("");
	//strQuery.Format(_T("delete from %s where thread_idx = %d"), DB_SEND_SCR_LIST, -1);
	//CCmsDBManager dbManage4(DB_SEND_SCR_LIST);
	//dbManage4.DeleteQuery(strQuery);
	//UM_WRITE_LOG(strQuery);
	//dbManage4.Free();
}

void CCheckJPGFileInLogFolderThread::SendThumb()
{
	if (m_bSendThumbThreadStart)
		return;
	m_bSendThumbThreadStart = TRUE;
	//HANDLE hThread;
	//DWORD dwUIThreadID = 0;

	//hThread= ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)SendThumbThread, this, 0, &dwUIThreadID);
	//CloseHandle(hThread);
	//CCheckJPGFileInLogFolderThread* pWnd = reinterpret_cast<CCheckJPGFileInLogFolderThread*>(lpParam);
	CString strThumbFile = /*pWnd->*/m_strJPG;
	if (strThumbFile.IsEmpty()) {
		/*pWnd->*/m_bSendThumbThreadStart = FALSE;
		//return FALSE;
		return;
	}
	BOOL bResult= FALSE;
	CFile cFile;
	CString strUrl = _T("");
	CParamParser parser;
	CString strParam = _T("");
	CString strReturn = _T("");
	CString strParameter = _T("");
	if (cFile.Open(strThumbFile, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone)) {
	strParam = parser.CreateKeyValueScreenRequest(strThumbFile);
	strUrl.Format(_T("%s/v1/trans/last_image"), theApp.m_pMemberData->GetServerHostURL());
	DWORD nBytesRead = 1;

	BYTE* pFileData = new BYTE[SEND_JPG_MAX_SIZE];
	memset(pFileData, 0x00, SEND_JPG_MAX_SIZE);
	nBytesRead = cFile.Read(pFileData, SEND_JPG_MAX_SIZE);
	if (0 < nBytesRead) {
		CBase64 base64;
		CString strEncodedFileData = _T("");
		CString strUrlEncodedData = _T("");
		strEncodedFileData = base64.base64encode(pFileData, nBytesRead);
		strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
		strParameter = strParam + strUrlEncodedData;
	} else {
		cFile.Close();
		SE_MemoryDelete(pFileData);
		/*pWnd->*/m_bSendThumbThreadStart = FALSE;
		//return FALSE;
		return;
	}
	SE_MemoryDelete(pFileData);
	int nErr = 0;
	/*if (theApp)*/	{
		strReturn = theApp.m_curl.CurlPost(strParameter, strUrl, theApp.GetToken());
		{
			CString strLog = _T("");
			strLog.Format(_T("[SendThumbThread] %s %s[line: %d, function: %s, file: %s]"), strParameter, strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
			UM_WRITE_LOG(strLog);
		}
		CString strTokenFromReturn = _T("");
		POLICY_SET* pPolicySet = new POLICY_SET;
		INT nParseResult = 0;
		nParseResult = parser.ParserPolicySet(strReturn, /*nCurrentOffset, */strTokenFromReturn, pPolicySet);

		if (1 == nParseResult) {
			theApp.SetToken(strTokenFromReturn);
			if (NULL != pPolicySet) {
				if (pPolicySet->bIsPolicyExist) {
					theApp.ChangePolicy(pPolicySet);
				}
			}
			bResult = TRUE;
		} else if (-1 == nParseResult) {
			// 2017-11-27 sy.choi
			theApp.m_pWnd->PostMessage(MSG_PAUSE_COMMUNICATE, NULL, NULL);
		}
		SE_MemoryDelete(pPolicySet);
		}
	}
	cFile.Close();
	/*pWnd->*/m_bSendThumbThreadStart = FALSE;
	//return TRUE;
	return;
}

//BOOL WINAPI SendThumbThread(LPVOID lpParam)
//{
//	CCheckJPGFileInLogFolderThread* pWnd = reinterpret_cast<CCheckJPGFileInLogFolderThread*>(lpParam);
//	CString strThumbFile = pWnd->m_strJPG;
//	if (strThumbFile.IsEmpty()) {
//		pWnd->m_bSendThumbThreadStart = FALSE;
//		return FALSE;
//	}
//	BOOL bResult= FALSE;
//	CFile cFile;
//	CString strUrl = _T("");
//	CParamParser parser;
//	CString strParam = _T("");
//	CString strReturn = _T("");
//	CString strParameter = _T("");
//	if (cFile.Open(strThumbFile, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone)) {
//	strParam = parser.CreateKeyValueScreenRequest(strThumbFile);
//	strUrl.Format(_T("%s/v1/trans/last_image.dsntech"), theApp.m_pMemberData->GetServerHostURL());
//	DWORD nBytesRead = 1;
//
//	BYTE* pFileData = new BYTE[SEND_JPG_MAX_SIZE];
//	memset(pFileData, 0x00, SEND_JPG_MAX_SIZE);
//	nBytesRead = cFile.Read(pFileData, SEND_JPG_MAX_SIZE);
//	if (0 < nBytesRead) {
//		CBase64 base64;
//		CString strEncodedFileData = _T("");
//		CString strUrlEncodedData = _T("");
//		strEncodedFileData = base64.base64encode(pFileData, nBytesRead);
//		strUrlEncodedData = CParseUtil::ReplaceSpecialCharacterForQuery(strEncodedFileData);
//		strParameter = strParam + strUrlEncodedData;
//	} else {
//		cFile.Close();
//		SE_MemoryDelete(pFileData);
//		pWnd->m_bSendThumbThreadStart = FALSE;
//		return FALSE;
//	}
//	SE_MemoryDelete(pFileData);
//	int nErr = 0;
//	strReturn = theApp.CurlPost(strParameter, strUrl, nErr);
//	{
//		CString strLog = _T("");
//		strLog.Format(_T("[SendThumbThread] %s %s[line: %d, function: %s, file: %s]"), strParameter, strReturn, __LINE__, __FUNCTIONW__, __FILEW__);
//		UM_WRITE_LOG(strLog);
//	}
//	CString strTokenFromReturn = _T("");
//	POLICY_SET* pPolicySet = new POLICY_SET;
//	if (parser.ParserPolicySet(strReturn, /*nCurrentOffset, */strTokenFromReturn, pPolicySet)) {
//		theApp.SetToken(strTokenFromReturn);
//		if (NULL != pPolicySet) {
//			if (pPolicySet->bIsPolicyExist) {
//				theApp.ChangePolicy(pPolicySet);
//			}
//		}
//		bResult = TRUE;
//	}
//	SE_MemoryDelete(pPolicySet);
//	}
//	cFile.Close();
//	pWnd->m_bSendThumbThreadStart = FALSE;
//	return TRUE;
//}