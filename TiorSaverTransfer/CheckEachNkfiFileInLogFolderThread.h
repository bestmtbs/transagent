/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.11.03	
********************************************************************************/
/**
@file       CLogSendThread.h
@brief     LogSendThread  정의 파일
@author   jhlee
@date      create 2011.11.03
*/
#pragma once

#include "../BuildEnv/common/ConnectParamInfo.h"
#include "CheckLogSizeThread.h"

// CLogSendThread

class CCheckEachNkfiFileInLogFolderThread : public CWinThread
{
	DECLARE_DYNCREATE(CCheckEachNkfiFileInLogFolderThread)

public:
	int m_nMaxIdx;
	//int m_nScanSleep;
	CCheckEachNkfiFileInLogFolderThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CCheckEachNkfiFileInLogFolderThread();


	BOOL m_bCheckEachNkfiFileInLogFolderThread;
	//BOOL m_bIsNewJPG;
	BOOL m_bSendThumbThreadStart;
	CString m_strJPG;
	//DWORD m_dwFileSize;
	void CheckOffset();
	//BOOL GetOffsetFromServer(CString _strTargetFile, UINT& _nCurrentOffset, int& Delay);
	//void FindOldestWrittenFile(CString _strPath/*, CStringArray& arrFiles, CUIntArray& arrSizeOfFiles*/);
	BOOL TerminateAllLogSizeThreads();

private:
//	void ClearForRenewDB(CString _strDBName, CString _strColumName, CStringArray& _arrStr)  //hhh: 추후 필요가능성 있음

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	//friend BOOL WINAPI SendThumbThread(LPVOID lpParam);

protected:
	DECLARE_MESSAGE_MAP()
	//BOOL GetVaccineModuleCheck(CString _strVaccineExe);

};