#include "stdafx.h"
#include "CpuUsageThread.h"
#include "TiorSaverTransfer.h"

#include <yvals.h>

IMPLEMENT_DYNCREATE(CCpuUsageThread, CWinThread)


#pragma warning(disable:4706)
/********************************************************************************
@class     CLogSendThread
@brief     로그 전송 
*********************************************************************************/

/**
@brief     생성자
@author   JHLEE
@date      2011.11.03
@note      초기화 작업
*/
CCpuUsageThread::CCpuUsageThread()
{
	m_bCpuUsageThread = FALSE;
	m_bCheckPolicy = FALSE;
}

/**
@brief     소멸자
@author   JHLEE
@date      2011.11.03
*/
CCpuUsageThread::~CCpuUsageThread()
{
	m_bCpuUsageThread = FALSE;
}

BOOL CCpuUsageThread::InitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 초기화를 수행합니다.

	return TRUE;
}

int CCpuUsageThread::ExitInstance()
{
	// TODO: 여기에서 각 스레드에 대한 정리를 수행합니다.
	//DBGLOG(L"[OfsAgnet] CLogSendThread::ExitInstance()");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CCpuUsageThread, CWinThread)
END_MESSAGE_MAP()


int CCpuUsageThread::Run()
{
	UM_WRITE_LOG(_T("CLogSendThread::Run()"));
	m_bCpuUsageThread = TRUE;
	CString strLog = _T("");

	while (TRUE)
	{
		INT nCurCPU = m_CpuUsage.GetCpuUsage();
		theApp.m_pMemberData->SetCpuUsage(nCurCPU);
		Sleep();
	}
	m_bCpuUsageThread = FALSE;
	//CTime tmSendTime = CTime::GetCurrentTime();
		
	UM_WRITE_LOG(_T(">>>> CCpuUsageThreadEnd.. <<<<"))
	return 0;
}