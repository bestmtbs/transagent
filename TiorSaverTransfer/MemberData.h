#pragma once
#ifdef _TIOR_TRANSFER_
#include "CpuUsage.h"
#endif
class CMemberData
{
public:
	CMemberData(void);
	~CMemberData(void);

	CString m_strDevice;
	CString m_strDeviceUUID;
	CString m_strVersion;
	int m_nDepartmentID;
	//CString m_strPCName;
	CString m_strAgentName;
	CString m_strPCIP;
	CString m_strProgram;
	CString m_strMAC;
	CString m_strLicenseCode;
	POLICY_SET* m_pPolicySet;
	CString m_strServerHostURL;
	CString m_strCorpName;
	CString m_strCollectType;
	CString m_strIgnore;
#ifdef _TIOR_TRANSFER_
	CCpuUsage m_CpuUsage;
#endif
	INT	m_nCpuUsage;

	BOOL		SetAgentInformation();
	void		ResetAgentInformation();
	CString		GetDeviceName();
	void		SetDeviceName(CString _strDeviceName);
	CString		GetDeviceUUID();
	void		SetDeviceUUID(CString _strDeviceUUID);
	CString		GetVersion();
	void		SetVersion(CString _strVersion);
	int			GetDepartmentID();
	void		SetDepartmentID(int _nDepartmentID);
	//CString GetPCName();
	//void	SetPCName(CString _strPCName);
	CString		GetAgentName();
	void		SetAgentName(CString _strAgentName);
	CString		GetPCAddress(BOOL _bIPorMAC);
	void		SetPCAddress(CString _strPCIP, BOOL _bIPorMAC);
	CString		GetProgram();
	void		SetProgram(CString _strProgram);
	CString		GetLicenseCode();
	void		SetLicenseCode(CString _strLicenseCode);
	CString		GetServerHostURL();
	BOOL		SetServerHostURL(CString _strServerHostURL);
	CString		GetCorpName();
	BOOL		SetCorpName(CString _strCorpName);
	CString		GetIgnore();
	CString		GetStrCollectType();
#ifdef _TIOR_TRANSFER_
	INT GetCpuUsage();
	VOID SetCpuUsage(INT _nCpuUsage);
#endif
};
