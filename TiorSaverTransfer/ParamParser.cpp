#include "StdAfx.h"
#include "ParamParser.h"
#include "../BuildEnv/common/json/JSON.h"
#include "MemberData.h"
#include "TiorSaverTransfer.h"
#include "UtilsFile.h"

#define	 HEADER			_T("header")
#define  SESSION			_T("Issession")
#define  CRYPTO			_T("Iscrypto")
#define  ENCRYPT			_T("Isencrypt")
#define  DEVICEID			_T("Deviceid")
#define  TRID				_T("Trid")
#define  MESSAGE			_T("message")
#define  CURRENT_OFFSET				_T("size")
#define  LOGFILE			_T("file")
#define	 TOKEN				_T("token")
#define  DATA		        _T("data")
#define  ACTION		        _T("action")
#define  METHOD		        _T("method")
#define  STATUS			_T("status")
#define  DEVICE			_T("device")
#define	 DEVICE_UUID		_T("device_uuid")
#define	 DEVICE_VERSION	_T("ver")
#define  EVENTLOG_FILENAME	_T("filename")
#define	 EVENTLOG_CONTENTS	_T("logs")
#define	 LICENSE_CODE _T("license_code")
#define	 AGENT_IP _T("agent_ip")

CParamParser::CParamParser(void)
{
}

CParamParser::~CParamParser(void)
{
}

CString CParamParser::CreateKeyValueRequest()
{
	//CString strReturn = _T("");
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(), AGENT_IP, theApp.m_pMemberData->GetPCAddress(TRUE));
	//strReturn.Format(_T("headers: { Authorization: Bearer %s} %s"), _strToken, strData);
	return strData;
}
CString CParamParser::CreateFileResetRequest(CString _strFileName)
{
	//CString strReturn = _T("");
	CString strData = _T("");
	CString strLogType = _T("");
	if ( 0 == ExtractFileExt(_strFileName).CompareNoCase(COLLECT_FILE_EXTENSION_LOG)) {
		strLogType = _T("event");
	} else {
		strLogType = _T("screen");
	}
	strData.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName, _T("log_type"), strLogType);
	DBGLOG(_T("[CreateKeyValueLogRequest] %s"), strData);
	return strData;
}
CString CParamParser::CreateKeyValueLogRequest(CString _strFileName)
{
	//CString strReturn = _T("");
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s&%s="), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName, EVENTLOG_CONTENTS);
	DBGLOG(_T("[CreateKeyValueLogRequest] %s"), strData);
	return strData;
}
CString CParamParser::CreateOffsetKeyValueRequest(CString _strFileName)
{
	//CString strReturn = _T("");
	CString strData = _T("");
	strData.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName);
	//strReturn.Format(_T("headers: { Authorization: Bearer %s} %s"), _strToken, strData);
	return strData;
}
CString CParamParser::CreateInstallKeyValueRequest(CString _strLicenseCode)
{
	CString strReturn = _T("");
	strReturn.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(), \
		LICENSE_CODE, _strLicenseCode);
	return strReturn;
}
CString CParamParser::CreateKeyValueScreenRequest(CString _strFileName)
{
	CString strReturn = _T("");
	strReturn.Format(_T("%s=%s&%s=%s&%s=%s&%s=%s&%s="),  DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		EVENTLOG_FILENAME, _strFileName, DATA);
	return strReturn;
}
INT CParamParser::ParserTotalPolicyIndex(CString _strRecvData, LOG_DATA* pIndex)
{
//	CString strResult = _T(""), strError = _T(""), strTempIndex=L"";
//
//	strError = GetErrorCheck(_strRecvData);
//
//	if( strError != _T("00000"))
//	{
//		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
//		return 0;
//	}
//
//	JSONValue *value = JSON::Parse(_strRecvData);
//
//	if( value != NULL )
//	{
//		JSONObject root, root2, root3;
//
//		if( value->IsObject() != false )
//		{
//			root = value->AsObject();
//
//			CString strBody = _T("");
//			if( root.find(BODY) != root.end() && root[BODY]->IsArray() )
//			{
//				JSONArray array = root[BODY]->AsArray();
//				for(UINT i=0; i<array.size(); i++)
//				{
//					CString strArr = _T("");
//					strArr.Format(_T("%s"), array[i]->Stringify().c_str());
//
//					if( !strArr.IsEmpty() )
//					{
//
//						JSONValue *value2 = JSON::Parse(strArr);
//						if( value2 != NULL )
//						{
//							root2 = value2->AsObject();
//
//							if( root2.find(RESULT) != root2.end() && root2[RESULT]->IsString() )
//							{
//								strResult.Format(_T("%s"), root2[RESULT]->AsString().c_str());
//							}
//
//							if( strResult == _T("5") )
//							{
//								delete value;
//								delete value2;
//
//								return 2;
//							}
//
//							if( strResult == _T("00000"))
//							{
//								if( root2.find(_T("plyIdx")) != root2.end() && root2[_T("plyIdx")]->IsNumber() )
//								{									
//									pIndex->iPolicyNameIndex = (UINT)root2[_T("plyIdx")]->AsNumber();
//								}
//							}
//							else
//							{
//								delete value;
//								delete value2;
//								return 0;
//							}
//							delete value2;
//						}//value2
//					}
//					else
//					{
//						delete value;
//						UM_WRITE_LOG(_T("Error - ParamParser::ParserPlyName body is null"))
//							return 0;
//					}
//				}
//			}
//			else
//			{
//				delete value;
//				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
//					return 0;
//			}
//		}
//		delete value;
//	}//value
	return 1;
}

INT CParamParser::GetErrorCheck(CString _strRecvData, CString strMessage)
{
	BOOL bStatus = 0;
	BOOL bIsInspect = FALSE;

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL ) {
		JSONObject root;

		if( value->IsObject() != false ) {
			root = value->AsObject();

			if(root.find(STATUS) != root.end() && root[STATUS]->IsBool()) {
				bStatus = root[STATUS]->AsBool();
				if (FALSE == bStatus) {
					if (root.find(MESSAGE) != root.end() && root[MESSAGE]->IsString()) {
						strMessage.Format(_T("%s"), root[MESSAGE]->AsString().c_str());
					}
					if( root.find(_T("inspect")) != root.end() && root[_T("inspect")]->IsBool() ) {
						bIsInspect = root[_T("inspect")]->AsBool();
						bStatus = -1;
					}
					if (root.find(_T("inspect_time")) != root.end() && root[_T("inspect_time")]->IsNumber()) {
						/*if (theApp)*/ {
							if (theApp.m_pWnd) {
								theApp.m_pWnd->m_nPauseCommSecond = (INT)root[_T("inspect_time")]->AsNumber();
							}
						}
					}
					if (root.find(_T("action")) != root.end() && root[_T("action")]->IsString()) {
						CString strAction = _T("");
						strAction.Format(_T("%s"), root[_T("action")]->AsString().c_str());
						if (0 == strAction.CompareNoCase(_T("delete"))) {
							theApp.StartUninstall();
							bStatus = -2;
						} else if (0 == strAction.CompareNoCase(_T("login"))) {
							theApp.VerifyLicense();
						} else {
							bStatus = -3;
						}
					}
				}
			}
		}
		delete value;
	}
	return bStatus;
}

INT CParamParser::ParserGetOffsetReturn(CString _strRecvData, INT& nCurrentOffset, CString& strToken, int& nDelay)
{
	if (_strRecvData.IsEmpty()) {
		return 0;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	INT nStatus = 0;
	INT bResult = 0;

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken = strMessage;
		return nStatus;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3, root5;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				//JSONValue *value2 = JSON::Parse(root[DATA]->AsString().c_str());
				root2 = root[DATA]->AsObject();
				if( root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString() ) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());

					if( root2.find(_T("file")) != root2.end() && root2[_T("file")]->IsObject() ) {
						root3 = root2[_T("file")]->AsObject();
						if(root3.find(_T("size")) != root3.end() && root3[_T("size")]->IsNumber())
						{
							nCurrentOffset = (int)(root3[_T("size")]->AsNumber());
							bResult = 1;
						}
						if (root3.find(_T("delay")) != root3.end() && root3[_T("delay")]->IsNumber()) {
							nDelay = (int)root3[_T("delay")]->AsNumber();
						}
					}
				} else {
					bResult = 0;
				}
				if( root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject())
				{
					root3 = root2[_T("policy")]->AsObject();							
					if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
						root5 = root3[_T("time")]->AsObject();
						if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
							CString strTimeFromServer = _T("");
							strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
							/*if (theApp)*/ {
								theApp.m_curl.m_strTimeFromServer = strTimeFromServer;
							}
						}
					}
				}
			} else {
				bResult = 0;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
		}
		delete value;
	}
	return bResult;
}

INT CParamParser::ParserOnlyToken(CString _strRecvData, /*UINT& nCurrentOffset, */CString& strToken)
{
	if (_strRecvData.IsEmpty()) {
		return 0;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	INT nStatus = 0;
	INT bResult = 0;

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken = strMessage;
		return nStatus;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3, root5;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				//JSONValue *value2 = JSON::Parse(root[DATA]->AsString().c_str());
				root2 = root[DATA]->AsObject();
				if(root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString()) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());

					//if( root2.find(_T("file")) != root2.end() && root2[_T("file")]->IsObject() ) {
					//	root3 = root2[_T("file")]->AsObject();
					//	if(root3.find(_T("size")) != root3.end() && root3[_T("size")]->IsNumber())
					//	{
					//		nCurrentOffset = (int)(root3[_T("size")]->AsNumber());
							bResult = 1;
					//	}
					//}
				} else {
					bResult = 0;
				}
				if( root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject())
				{
					root3 = root2[_T("policy")]->AsObject();							
					if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
						root5 = root3[_T("time")]->AsObject();
						if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
							CString strTimeFromServer = _T("");
							strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
							CString strLog = _T("");
							strLog.Format(_T("strTimeFromServer: %s"), strTimeFromServer);
							OutputDebugString(strLog);
							/*if (theApp)*/ {
								theApp.m_curl.m_strTimeFromServer = strTimeFromServer;
							}
						}
					}
				}
			} else {
				bResult = 0;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
		}
		delete value;
	}
	return bResult;
}

INT CParamParser::ParserIdentifyLicense(CString _strRecvData, CTypedPtrArray <CPtrArray, DEPARTMENT*>* arrDepartment, CString& strToken)
{
	if (_strRecvData.IsEmpty()) {
		return FALSE;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	INT nStatus = 0;
	INT bResult = 0;
	arrDepartment->RemoveAll();

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken = strMessage;
		return nStatus;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				//JSONValue *value2 = JSON::Parse(root[DATA]->AsString().c_str());
				root2 = root[DATA]->AsObject();
				if(root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString()) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());
					bResult = 1;

					if (root2.find(_T("department")) != root2.end() && root2[_T("department")]->IsArray()) {
						JSONArray subArray = root2[_T("department")]->AsArray();
						for( UINT i = 0; i < subArray.size(); i++)
						{
							DEPARTMENT* department = new DEPARTMENT;
							root3 = subArray[i]->AsObject();
							if (root3.find(_T("dep_id")) != root3.end() && root3[_T("dep_id")]->IsNumber()) {
								department->nDepartmentID = (int)root3[_T("dep_id")]->AsNumber();
							}
							if (root3.find(_T("dep_name")) != root3.end() && root3[_T("dep_name")]->IsString()) {
								department->strDepartmentName = root3[_T("dep_name")]->AsString().c_str();
							}
							arrDepartment->Add(department);
							//if (NULL != department) {
							//	delete department;
							//	department = NULL;
							//}
							SE_MemoryDelete(department);
						}
					}
				} else {
					bResult = 0;
				}
			} else {
				bResult = 0;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
		}
		delete value;
	}
	return bResult;
}

INT CParamParser::ParserPolicySet(CString _strRecvData, CString& strToken, POLICY_SET* pPolicySet)
{
	if (_strRecvData.IsEmpty()) {
		return FALSE;
	}
	CString strResult = _T(""), strTempIndex=L"";
	CString strMessage = _T("");
	INT nStatus = 0;
	INT bResult = 0;

	nStatus = GetErrorCheck(_strRecvData, strMessage);
	if (-1 == nStatus) {
		return -1;
	}
	if (-2 == nStatus)
	{
		pPolicySet->bIsPolicyExist = TRUE;
		pPolicySet->bDelete = TRUE;
		return 1;
	}
	if( nStatus != 1)
	{
		UM_WRITE_LOG(_T("CParamParser::ParserTotalPolicyIndex ERRORNUM"));
		strToken = strMessage;
		return nStatus;
	}

	JSONValue *value = JSON::Parse(_strRecvData);

	if( value != NULL )
	{
		JSONObject root, root2, root3, root4, root5, root6, root7;

		if( value->IsObject() != false )
		{
			root = value->AsObject();
			CString strBody = _T("");
			if( root.find(DATA) != root.end() && root[DATA]->IsObject())
			{
				root2 = root[DATA]->AsObject();
				if(root2.find(TOKEN) != root2.end() && root2[TOKEN]->IsString()) {
					strToken.Format(_T("%s"), root2[TOKEN]->AsString().c_str());
					bResult = 1;

					if (root2.find(_T("popup")) != root2.end() && root2[_T("popup")]->IsBool()) {
						pPolicySet->bNotice = root2[_T("popup")]->AsBool();
					}
					if( root2.find(_T("policy")) != root2.end() && root2[_T("policy")]->IsObject())
					{
						root3 = root2[_T("policy")]->AsObject();
						if (root3.find(_T("ignore")) != root3.end() && root3[_T("ignore")]->IsArray()) {
						theApp.m_bInitialized = TRUE;
							/*if (theApp)*/ {
								theApp.m_bInitialized = TRUE;
							}
							JSONArray subArray = root3[_T("ignore")]->AsArray();
							for (UINT i = 0; i < subArray.size(); i++)
							{
								CString strSubArr = _T("");
								strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
								pPolicySet->arrIgnore.Add(strSubArr);
							}
						}
						if (root3.find(_T("screen_size")) != root3.end() && root3[_T("screen_size")]->IsNumber()) {
							pPolicySet->dwSplitSize = root3[_T("screen_size")]->AsNumber();
						}
						if (root3.find(_T("trans_screen_size")) != root3.end() && root3[_T("trans_screen_size")]->IsNumber()) {
							theApp.m_nTransMaxScreenSize = root3[_T("trans_screen_size")]->AsNumber();
						}
						if (root3.find(_T("trans_log_line")) != root3.end() && root3[_T("trans_log_line")]->IsNumber()) {
							theApp.m_nTransMaxLogLines = root3[_T("trans_log_line")]->AsNumber();
						}
						if (root3.find(_T("browser")) != root3.end() && root3[_T("browser")]->IsArray()) {
							JSONArray subArray = root3[_T("browser")]->AsArray();
							for (UINT i = 0; i < subArray.size(); i++)
							{
								CString strSubArr = _T("");
								strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
								pPolicySet->arrBrowser.Add(strSubArr);
							}
						}
						if (root3.find(_T("agent")) != root3.end() && root3[_T("agent")]->IsObject()) {
							root4 = root3[_T("agent")]->AsObject();
							/*if (theApp)*/ {
								if (FALSE == theApp.m_bInitialized) {
									pPolicySet->bIsPolicyExist = FALSE;
								}
							}
							if (root4.size() <= 0) {
								pPolicySet->bIsPolicyExist = FALSE;
							}
							if (root4.find(_T("start")) != root4.end() && root4[_T("start")]->IsBool()) {
								pPolicySet->bStart = root4[_T("start")]->AsBool();
							}
							if (root4.find(_T("delete")) != root4.end() && root4[_T("delete")]->IsBool()) {
								pPolicySet->bDelete = root4[_T("delete")]->AsBool();
								if (pPolicySet->bDelete) {
									pPolicySet->bDelete = TRUE;
								}
							}
							if (root4.find(_T("collect")) != root4.end() && root4[_T("collect")]->IsArray()) {
								JSONArray subArray = root4[_T("collect")]->AsArray();
								for( UINT i = 0; i < subArray.size(); i++)
								{
									CString strSubArr = _T("");
									strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
									pPolicySet->arrCollect.Add(strSubArr);
								}
							}
							if (root4.find(_T("domain")) != root4.end() && root4[_T("domain")]->IsArray()) {
								JSONArray subArray = root4[_T("domain")]->AsArray();
								for( UINT i = 0; i < subArray.size(); i++)
								{
									CString strSubArr = _T("");
									strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
									pPolicySet->arrDomain.Add(strSubArr);
								}
							}
							if (root3.find(_T("time")) != root3.end() && root3[_T("time")]->IsObject()) {
								root5 = root3[_T("time")]->AsObject();
								if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
									CString strTimeFromServer = _T("");
									strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
									pPolicySet->TimeSet->strTimeFromServer = strTimeFromServer;
								}
								//if (root5.find(_T("timestamp")) != root5.end() && root5[_T("timestamp")]->IsNumber()) {
								//	pPolicySet->TimeSet->ctTimeFromServer = root5[_T("timestamp")]->AsNumber();
								//}
								if (root5.find(_T("scan_delay")) != root5.end() && root5[_T("scan_delay")]->IsNumber()) {
									 pPolicySet->TimeSet->nScanDelay = (int)root5[_T("scan_delay")]->AsNumber();
									 theApp.m_nScanDelay = pPolicySet->TimeSet->nScanDelay;
									 //if (NULL != theApp.m_pCheckEachLogFileInLogFolderThread) {
										// if (theApp.m_pCheckEachLogFileInLogFolderThread->m_bCheckEachLogFileInLogFolderThread) {
										//		 theApp.m_pCheckEachLogFileInLogFolderThread->m_nScanSleep = pPolicySet->TimeSet->nScanDelay;
										// }
									 //}
								}
								if (root5.find(_T("scan_date")) != root5.end() && root5[_T("scan_date")]->IsString()) {
									CString strScanDate = _T("");
									strScanDate.Format(_T("%s"), root5[_T("scan_date")]->AsString().c_str());
									int nYear = _wtoi(strScanDate.Left(4));
									int nMon = _wtoi(strScanDate.Mid(4,2));
									int nDay = _wtoi(strScanDate.Mid(6,2));
									pPolicySet->TimeSet->ctScanDate = CTime(nYear, nMon, nDay);
								}
								if (root5.find(_T("delete_date")) != root5.end() && root5[_T("delete_date")]->IsString()) {
									CString strDeleteDate = _T("");
									strDeleteDate.Format(_T("%s"), root5[_T("delte_date")]->AsString().c_str());
									int nYear = _wtoi(strDeleteDate.Left(4));
									int nMon = _wtoi(strDeleteDate.Mid(4, 2));
									int nDay = _wtoi(strDeleteDate.Mid(6, 2));
									pPolicySet->TimeSet->ctDeleteDate = CTime(nYear, nMon, nDay);
									/*if (theApp)*/
										theApp.m_strDeleteDate.Format(_T("%d-%d-%d"), nYear, nMon, nDay);
								}
								if (root5.find(_T("delete_delay")) != root5.end() && root5[_T("delete_delay")]->IsNumber()) {
									pPolicySet->TimeSet->nDeleteDelay = (int)root5[_T("delete_delay")]->AsNumber();
									/*if (theApp)*/
										theApp.m_nDeleteDelay = pPolicySet->TimeSet->nDeleteDelay;
								}
								if (root5.find(_T("policy_retry")) != root5.end() && root5[_T("policy_retry")]->IsNumber()) {
									pPolicySet->TimeSet->nPolicyRetry = (int)root5[_T("policy_retry")]->AsNumber();
									/*if (theApp)*/
										theApp.m_nPolicyRetry = pPolicySet->TimeSet->nPolicyRetry;
								}

							} else {
								pPolicySet->TimeSet = NULL;
							}
							if (root3.find(_T("update")) != root3.end() && root3[_T("update")]->IsObject()) {
								root6 = root3[_T("update")]->AsObject();
								if (root6.find(_T("reboot")) != root6.end() && root6[_T("reboot")]->IsBool()) {
									theApp.m_bUpdateAfterReboot = root6[_T("reboot")]->AsBool();
								}
								if (root6.find(_T("curr_version")) != root6.end() && root6[_T("curr_version")]->IsString()) {
									pPolicySet->UpdateSet->strCurrVersion.Format(_T("%s"), root6[_T("curr_version")]->AsString().c_str());
								}
								if (root6.find(_T("new_version")) != root6.end() && root6[_T("new_version")]->IsString()) {
									pPolicySet->UpdateSet->strNewVersion.Format(_T("%s"), root6[_T("new_version")]->AsString().c_str());
								}
								JSONObject root60, root61, root62, root63;
								if (root6.find(_T("files")) != root6.end() && root6[_T("files")]->IsObject()) {
									root60 = root6[_T("files")]->AsObject();
									if (root60.find(_T("path")) != root60.end() && root60[_T("path")]->IsArray()) {
										JSONArray subArray = root60[_T("path")]->AsArray();
										for( UINT i = 0; i < subArray.size(); i++)
										{
											CString strSubArr = _T("");
											strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
											pPolicySet->UpdateSet->arrPath.Add(strSubArr);
										}
									}
									if (root60.find(_T("version")) != root60.end() && root60[_T("version")]->IsArray()) {
										JSONArray subArray = root60[_T("version")]->AsArray();
										for( UINT i = 0; i < subArray.size(); i++)
										{
											CString strSubArr = _T("");
											strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
											pPolicySet->UpdateSet->arrVersion.Add(strSubArr);
										}
									}
									if (root60.find(_T("hash")) != root60.end() && root60[_T("hash")]->IsArray()) {
										JSONArray subArray = root60[_T("hash")]->AsArray();
										for( UINT i = 0; i < subArray.size(); i++)
										{
											CString strSubArr = _T("");
											strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
											pPolicySet->UpdateSet->arrHash.Add(strSubArr);
										}
									}
									if (root6.find(_T("message")) != root6.end() && root6[_T("message")]->IsString()) {
										pPolicySet->UpdateSet->strMessage.Format(_T("%s"), root6[_T("message")]->AsString().c_str());
									}
								} else {
									pPolicySet->UpdateSet = NULL;
								}
							} else {
								pPolicySet->UpdateSet = NULL;
							}
							if (root3.find(_T("disk")) != root3.end() && root3[_T("disk")]->IsObject()) {
								root7 = root3[_T("disk")]->AsObject();
								if (root7.find(_T("free")) != root7.end() && root7[_T("free")]->IsNumber()) {
									pPolicySet->nDiskFree = (UINT)root7[_T("free")]->AsNumber();
								}
							}
						} else
							pPolicySet->bIsPolicyExist = FALSE;
					} else {
					pPolicySet = NULL;
					}
				} else {
					bResult = 0;
				}
			} else {
				bResult = 0;
				UM_WRITE_LOG(_T("Error - ParamParser::v body is null"))
			}
		}
		delete value;
	}
	return bResult;
}

CString CParamParser::CreateAgentRegisterRequest(int _nDepartmentID, CString _strAgentName)
{
	CString strReturn = _T("");
	strReturn.Format(_T("%s=%s&%s=%s&%s=%s&%s=%d&%s=%s&%s=%s&%s=%s"), DEVICE, theApp.m_pMemberData->GetDeviceName(), DEVICE_UUID, theApp.m_pMemberData->GetDeviceUUID(), DEVICE_VERSION, theApp.m_pMemberData->GetVersion(),\
		_T("dep_id"), _nDepartmentID, _T("agent_name"), _strAgentName, _T("agent_ip"), theApp.m_pMemberData->GetPCAddress(TRUE), _T("program"), theApp.m_pMemberData->GetProgram());
	return strReturn;
}

CString CParamParser::CreateClientID(CString _strClientID)
{
	JSONObject root, root2;
	CString strReturn = _T("");
	root[_T("action")] = new JSONValue(_T("register"));

	//CString strSiteID = _T("");
	CString strDepartmentID = _T("");
	CString strUUID = _T("");
	CString strSiteID = _T("");

	strDepartmentID.Format(_T("%d"), theApp.m_pMemberData->GetDepartmentID());
	strUUID.Format(_T("%s"), theApp.m_pMemberData->GetDeviceUUID());
	strSiteID.Format(_T("%s"), theApp.m_pMemberData->GetCorpName());

	root2[_T("client_id")] = new JSONValue(_strClientID);
	root2[_T("sid")] = new JSONValue(strSiteID);
	root2[_T("uid")] = new JSONValue(strUUID);
	root2[_T("did")] = new JSONValue(strDepartmentID);
	root[_T("data")] = new JSONValue(root2);

	JSONValue *value = new JSONValue(root);
	strReturn.Format(_T("%s"), value->Stringify().c_str());
	delete value;

	return strReturn;
}

CString CParamParser::ParserClientID(CString _strData)
{
	CString strAction = _T("");
	CString strClientID = _T("");
	CString strMethod = _T("");
	JSONValue *value = JSON::Parse(_strData);

	if (value != NULL)
	{
		JSONObject root, root2;

		if (value->IsObject() != false)
		{
			root = value->AsObject();

			if (root.find(ACTION) != root.end() && root[ACTION]->IsString())
			{
				strAction = root[ACTION]->AsString().c_str();
				if (0 != strAction.CompareNoCase(_T("clientConnect"))) {
					delete value;
					return _T("");
				}
				//if (root.find(METHOD) != root.end() && root[METHOD]->IsString())
				//{
				//	strMethod = root[METHOD]->AsString().c_str();
				//	if (strMethod.CompareNoCase(_T("register"))) {
				//		delete value;
				//		return _T("");
				//	}
				//}
				if (root.find(DATA) != root.end() && root[DATA]->IsObject())
				{
					root2 = root[DATA]->AsObject();
					if (root2.find(_T("client_id")) != root2.end() && root2[_T("client_id")]->IsString())
						strClientID = root2[_T("client_id")]->AsString().c_str();
				}
			}
		}
		delete value;
	}
	return strClientID;
}

BOOL CParamParser::ParserPolicySetForWebSocket(CString _strRecvData, CString& strToken, POLICY_SET* pPolicySet)
{
	BOOL bResult = FALSE;
	CString strAction = _T("");
	CString strClientID = _T("");
	CString strMethod = _T("");
	JSONValue *value = JSON::Parse(_strRecvData);

	if (value != NULL)
	{
		JSONObject root, root2, root4, root5, root6;

		if (value->IsObject() != false)
		{
			root = value->AsObject();

			if (root.find(ACTION) != root.end() && root[ACTION]->IsString())
			{
				strAction = root[ACTION]->AsString().c_str();
				if (strAction.CompareNoCase(_T("agent"))) {
					delete value;
					return bResult;
				}
				if (root.find(METHOD) != root.end() && root[METHOD]->IsString())
				{
					strMethod = root[METHOD]->AsString().c_str();
					if (strMethod.CompareNoCase(_T("policy"))) {
						delete value;
						return bResult;
					}
					bResult = TRUE;

				}
				if (root.find(DATA) != root.end() && root[DATA]->IsObject())
				{
					root2 = root[DATA]->AsObject();
					root4 = root2[_T("policy")]->AsObject();
					if (root4.find(_T("start")) != root4.end() && root4[_T("start")]->IsBool()) {
						pPolicySet->bStart = root4[_T("start")]->AsBool();
						pPolicySet->bIsPolicyExist = TRUE;
					}
					if (root4.find(_T("delete")) != root4.end() && root4[_T("delete")]->IsBool()) {
						pPolicySet->bDelete = root4[_T("delete")]->AsBool();
						if (pPolicySet->bDelete) {
							pPolicySet->bStart = FALSE;
						}
						pPolicySet->bIsPolicyExist = TRUE;
					}
					if (root4.find(_T("collect")) != root4.end() && root4[_T("collect")]->IsArray()) {
						JSONArray subArray = root4[_T("collect")]->AsArray();
						for (UINT i = 0; i < subArray.size(); i++)
						{
							CString strSubArr = _T("");
							strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
							pPolicySet->arrCollect.Add(strSubArr);
						}
						pPolicySet->bIsPolicyExist = TRUE;
					}
					if (root4.find(_T("domain")) != root4.end() && root4[_T("domain")]->IsArray()) {
						JSONArray subArray = root4[_T("domain")]->AsArray();
						for (UINT i = 0; i < subArray.size(); i++)
						{
							CString strSubArr = _T("");
							strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
							pPolicySet->arrDomain.Add(strSubArr);
						}
						pPolicySet->bIsPolicyExist = TRUE;
					}
					if (root4.find(_T("time")) != root4.end() && root4[_T("time")]->IsObject()) {
						root5 = root4[_T("time")]->AsObject();
						if (root5.find(_T("server_time")) != root5.end() && root5[_T("server_time")]->IsString()) {
							CString strTimeFromServer = _T("");
							strTimeFromServer.Format(_T("%s"), root5[_T("server_time")]->AsString().c_str());
							pPolicySet->TimeSet->strTimeFromServer = strTimeFromServer;
						}
					}
					if (root4.find(_T("update")) != root4.end() && root4[_T("update")]->IsObject()) {
						root6 = root4[_T("update")]->AsObject();
						if (root6.find(_T("reboot")) != root6.end() && root6[_T("reboot")]->IsBool()) {
							theApp.m_bUpdateAfterReboot = root6[_T("reboot")]->AsBool();
						}
						if (root6.find(_T("curr_version")) != root6.end() && root6[_T("curr_version")]->IsString()) {
							pPolicySet->UpdateSet->strCurrVersion.Format(_T("%s"), root6[_T("curr_version")]->AsString().c_str());
						}
						if (root6.find(_T("new_version")) != root6.end() && root6[_T("new_version")]->IsString()) {
							pPolicySet->UpdateSet->strNewVersion.Format(_T("%s"), root6[_T("new_version")]->AsString().c_str());
						}
						JSONObject root60, root61, root62, root63;
						if (root6.find(_T("files")) != root6.end() && root6[_T("files")]->IsObject()) {
							root60 = root6[_T("files")]->AsObject();
							if (root60.find(_T("path")) != root60.end() && root60[_T("path")]->IsArray()) {
								JSONArray subArray = root60[_T("path")]->AsArray();
								for (UINT i = 0; i < subArray.size(); i++)
								{
									CString strSubArr = _T("");
									strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
									pPolicySet->UpdateSet->arrPath.Add(strSubArr);
								}
							}
							if (root60.find(_T("version")) != root60.end() && root60[_T("version")]->IsArray()) {
								JSONArray subArray = root60[_T("version")]->AsArray();
								for (UINT i = 0; i < subArray.size(); i++)
								{
									CString strSubArr = _T("");
									strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
									pPolicySet->UpdateSet->arrVersion.Add(strSubArr);
								}
							}
							if (root60.find(_T("hash")) != root60.end() && root60[_T("hash")]->IsArray()) {
								JSONArray subArray = root60[_T("hash")]->AsArray();
								for (UINT i = 0; i < subArray.size(); i++)
								{
									CString strSubArr = _T("");
									strSubArr.Format(_T("%s"), subArray[i]->Stringify().c_str());
									pPolicySet->UpdateSet->arrHash.Add(strSubArr);
								}
							}
							if (root6.find(_T("message")) != root6.end() && root6[_T("message")]->IsString()) {
								pPolicySet->UpdateSet->strMessage.Format(_T("%s"), root6[_T("message")]->AsString().c_str());
							}
						}
						else {
							pPolicySet->UpdateSet = NULL;
						}
					}
					else {
						pPolicySet->UpdateSet = NULL;
					}
				}
			}
		}
		delete value;
	}
	return bResult;
}

BOOL CParamParser::ParserThumnailAction(CString _strRecvData, BOOL& _bStart)
{
	BOOL bResult = FALSE;
	CString strAction = _T("");
	CString strClientID = _T("");
	CString strMethod = _T("");
	JSONValue *value = JSON::Parse(_strRecvData);

	if (value != NULL)
	{
		JSONObject root, root2;

		if (value->IsObject() != false)
		{
			root = value->AsObject();

			if (root.find(ACTION) != root.end() && root[ACTION]->IsString())
			{
				strAction = root[ACTION]->AsString().c_str();
				if (strAction.CompareNoCase(_T("agent"))) {
					delete value;
					return FALSE;
				}
				if (root.find(METHOD) != root.end() && root[METHOD]->IsString())
				{
					strMethod = root[METHOD]->AsString().c_str();
					if (strMethod.CompareNoCase(_T("screen"))) {
						delete value;
						return FALSE;
					}

				}
				if (root.find(DATA) != root.end() && root[DATA]->IsObject())
				{
					root2 = root[DATA]->AsObject();
					if (root2.find(_T("start")) != root2.end() && root2[_T("start")]->IsBool()) {
						_bStart = root2[_T("start")]->AsBool();
						bResult = TRUE;
					}
				}
			}
		}
		delete value;
	}
	return bResult;
}