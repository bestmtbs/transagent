#include "StdAfx.h"
#include "MemberData.h"
#include "CmsDBManager.h"
#include "PCInfo.h"
#ifdef _TIOR_TRANSFER_
#include "TiorSaverTransfer.h"
extern CTiorSaverTransferApp theApp;
#endif

CMemberData::CMemberData(void)
{
	m_strDevice = _T("");
	m_strDeviceUUID = _T("");
	m_strVersion = _T("");
	m_strAgentName = _T("");
	m_strPCIP = _T("");
	m_strProgram = _T("");
	m_strMAC = _T("");
	m_strLicenseCode = _T("");
	m_pPolicySet = new POLICY_SET;
	m_strServerHostURL = _T("");
	m_nDepartmentID = -1;
	m_strCollectType = _T("");
	m_strIgnore = _T("");
	m_nCpuUsage = -1;
}

CMemberData::~CMemberData(void)
{
	SE_MemoryDelete(m_pPolicySet);
}

void CMemberData::ResetAgentInformation()
{
	CString strQuery = _T("");
	CString strOSName = _T("");
	CStringArray arrData;
	int nCnt = 0;
	CPCInfo::GetOSName(strOSName);

	arrData.RemoveAll();

	strQuery.Format(_T("delete from %s"), DB_AGENT_INFO);
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->DeleteQuery(strQuery);
	}
#else
	CCmsDBManager dbManagerAgentInfo(DB_AGENT_INFO);
	dbManagerAgentInfo.DeleteQuery(strQuery);
	dbManagerAgentInfo.Free();
#endif

	strQuery = _T("");
	strQuery.Format(_T("delete from %s"), DB_STATE_POLICY);
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->DeleteQuery(strQuery);
	}
#else
	CCmsDBManager dbManageState(DB_STATE_POLICY);
	dbManageState.DeleteQuery(strQuery);
	dbManageState.Free();
#endif
	strQuery = _T("");
	strQuery.Format(_T("delete from %s"), DB_PC_INFO);

#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->DeleteQuery(strQuery);
	}
#else
	CCmsDBManager dbManagePC(DB_PC_INFO);
	dbManagePC.DeleteQuery(strQuery);
	dbManagePC.Free();
#endif

	strQuery = _T("");
	strQuery.Format(_T("delete from %s"), DB_SERVER_INFO);
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->DeleteQuery(strQuery);
	}
#else
	CCmsDBManager dbManageServer(DB_SERVER_INFO);
	dbManageServer.DeleteQuery(strQuery);
	dbManageServer.Free();
#endif
}

BOOL CMemberData::SetAgentInformation()
{
	BOOL bQueryResult = FALSE;
	CString strQuery = _T("");
	CString strOSName = _T("");
	CStringArray arrData;
	int nCnt = 0;
	CPCInfo::GetOSName(strOSName);

	arrData.RemoveAll();

	strQuery.Format(_T("insert into %s (device, device_uuid, ver, department_name, agent_name, department_id, license_code) values ('%s', '%s', '%s', '%s', '%s', %d, '%s')"), \
		DB_AGENT_INFO, GetDeviceName(), GetDeviceUUID(), GetVersion(), _T(""), GetAgentName(), GetDepartmentID(), GetLicenseCode());

#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->InsertQuery(strQuery);
	}
#else
	CCmsDBManager dbManager(DB_AGENT_INFO);
	bQueryResult = dbManager.InsertQuery(strQuery);
	dbManager.Free();
#endif
	if (FALSE == bQueryResult) {
		return FALSE;
	}
	SetCorpName(m_strCorpName);

	strQuery = _T("");
	strQuery.Format(_T("insert into %s (target_id) values (\"%s\")"), DB_STATE_POLICY, GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->InsertQuery(strQuery);
	}
#else
	CCmsDBManager dbManageState(DB_STATE_POLICY);
	bQueryResult = dbManageState.InsertQuery(strQuery);
	dbManageState.Free();
#endif
	if (FALSE == bQueryResult) {
		return FALSE;
	}
	strQuery = _T("");
	strQuery.Format(_T("insert into %s (device_uuid) values (\"%s\")"), DB_PC_INFO, GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->InsertQuery(strQuery);
	}
#else
	CCmsDBManager dbManagePCInfo(DB_STATE_POLICY);
	bQueryResult = dbManagePCInfo.InsertQuery(strQuery);
	dbManagePCInfo.Free();
#endif
	if (FALSE == bQueryResult) {
		return FALSE;
	}
	return TRUE;
}

CString CMemberData::GetDeviceName()
{
	if (m_strDevice.IsEmpty()) {
		m_strDevice = _T("agent");
	}
	return m_strDevice;
}
CString CMemberData::GetDeviceUUID()
{
	if (m_strDeviceUUID.IsEmpty()) {
		CPCInfo::GetPCID(m_strDeviceUUID);
	}
	return m_strDeviceUUID;
}
CString CMemberData::GetVersion()
{
	if (m_strVersion.IsEmpty()) {
		CString strQuery = _T("");
		CStringArray arrList;
		arrList.RemoveAll();
		strQuery.Format(_T("select ver from %s where device_uuid = \"%s\""), DB_AGENT_INFO, GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("ver"));
		}
#else
		CCmsDBManager dbManage(DB_AGENT_INFO);
		dbManage.SelectQuery(strQuery, &arrList, _T("ver"));
		dbManage.Free();
#endif
		if (0 < arrList.GetSize()) {
			if (arrList.GetAt(0).IsEmpty())	{
				return _T("1.0.0");
			}
			m_strVersion = arrList.GetAt(0);
		}
#ifdef _SETUP
		else {
			return _T("1.0.0");
		}
#endif		
	}
	return m_strVersion;
}
void	CMemberData::SetVersion(CString _strVersion)
{
	m_strVersion = _strVersion;
}
int CMemberData::GetDepartmentID()
{
	if (-1 == m_nDepartmentID) {
		CString strQuery = _T("");
		CStringArray arrList;
		arrList.RemoveAll();
		strQuery.Format(_T("select department_id from %s"), DB_AGENT_INFO);
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("department_id"));
		}
#else
		CCmsDBManager dbManage(DB_AGENT_INFO);
		dbManage.SelectQuery(strQuery, &arrList, _T("department_id"));
		dbManage.Free();
#endif
		if (0 < arrList.GetSize()) {
			m_nDepartmentID = _ttoi(arrList.GetAt(0));
		}
	}
	return m_nDepartmentID;
}
void	CMemberData::SetDepartmentID(int _nDepartmentID)
{
	m_nDepartmentID = _nDepartmentID;
}
//CString CMemberData::GetPCName()
//{
//	if (m_strPCName.IsEmpty()) {
//		CPCInfo::GetPCName(m_strPCName);
//	}
//	return m_strPCName;
//}
//void	CMemberData::SetPCName(CString _strPCName)
//{
//	m_strPCName = _strPCName;
//}

CString CMemberData::GetAgentName()
{
	if (m_strAgentName.IsEmpty()) {
		//CPCInfo::GetPCName(m_strAgentName);
	}
	return m_strAgentName;
}
void	CMemberData::SetAgentName(CString _strAgentName)
{
	m_strAgentName = _strAgentName;
}

CString CMemberData::GetPCAddress(BOOL _bIPorMAC)
{
	if (m_strPCIP.IsEmpty()) {
		CPCInfo::GetRealIPMAC(m_strPCIP, m_strMAC);
	}
	if (_bIPorMAC) {
		return m_strPCIP;
	} else {
		return m_strMAC;
	}
}
void	CMemberData::SetPCAddress(CString _strPCAddress, BOOL _bIPorMAC)
{
	m_strPCIP = m_strPCIP;
	m_strMAC = _strPCAddress;
}
CString CMemberData::GetProgram()
{
	if (m_strProgram.IsEmpty()) {
		m_strProgram = _T("empty");
	}
	return m_strProgram;
}

void	CMemberData::SetProgram(CString _strProgram)
{
	m_strProgram = _strProgram;
}

void	CMemberData::SetLicenseCode(CString _strLicenseCode)
{
	m_strLicenseCode = _strLicenseCode;
}

CString CMemberData::GetLicenseCode()
{
	if (m_strLicenseCode.IsEmpty()) {
		CString strQuery = _T("");
		CStringArray arrList;
		arrList.RemoveAll();
		strQuery.Format(_T("select license_code from %s"), DB_AGENT_INFO);
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("license_code"));
		}
#else
		CCmsDBManager dbManage(DB_AGENT_INFO);
		dbManage.SelectQuery(strQuery, &arrList, _T("license_code"));
		dbManage.Free();
#endif
		if (0 < arrList.GetSize()) {
			m_strLicenseCode = arrList.GetAt(0);
		}
	}
	return m_strLicenseCode;
}

CString CMemberData::GetServerHostURL()
{
	int nCnt = 0;
	CString strQuery = _T("");
	CStringArray arrList;
	if (m_strServerHostURL.IsEmpty()) {
		while (nCnt < 5)
		{
			arrList.RemoveAll();
			strQuery.Format(_T("select server_host_url from %s"), DB_SERVER_INFO);
#ifdef _TIOR_TRANSFER_
			if (theApp.m_pDbSetting) {
				theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("server_host_url"));
			}
#else
			CCmsDBManager dbManage(DB_SERVER_INFO);
			dbManage.SelectQuery(strQuery, &arrList, _T("server_host_url"));
			dbManage.Free();
#endif
			if (0 < arrList.GetSize()) {
				m_strServerHostURL = arrList.GetAt(0);
				break;
			} else {
				nCnt++;
			}
		}
	}
	return m_strServerHostURL;
}
BOOL CMemberData::SetServerHostURL(CString _strServerHostURL)
{
	BOOL bResult = TRUE;
	m_strServerHostURL = _strServerHostURL;
	if (!m_strServerHostURL.IsEmpty()) {
		CString strQuery = _T("");
		strQuery.Format(_T("insert into %s (server_host_url) values (\"%s\")"), DB_SERVER_INFO, _strServerHostURL);
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			if (!theApp.m_pDbSetting->InsertQuery(strQuery)) {
				bResult = FALSE;
			}
		} else {
			bResult = FALSE;
		}
#else
		CCmsDBManager dbManage(DB_SERVER_INFO);
		if (!dbManage.InsertQuery(strQuery)) {
			bResult = FALSE;
		}
		dbManage.Free();
#endif
	}
	return bResult;
}
CString CMemberData::GetCorpName() {
	if (m_strCorpName.IsEmpty()) {
		CString strQuery = _T("");
		CStringArray arrList;
		arrList.RemoveAll();
		strQuery.Format(_T("select corp_name from %s"), DB_AGENT_INFO);
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("corp_name"));
		}
#else
		CCmsDBManager dbManage(DB_AGENT_INFO);
		dbManage.SelectQuery(strQuery, &arrList, _T("corp_name"));
		dbManage.Free();
#endif
		if (0 < arrList.GetSize()) {
			m_strCorpName = arrList.GetAt(0);
	}
	}
	return m_strCorpName;
}
BOOL CMemberData::SetCorpName(CString _strCorpName) {
	CString strQuery = _T("");
	CString strOldCorpName = _T("");
	CStringArray arrList;
	arrList.RemoveAll();
	strQuery.Format(_T("select corp_name from %s"), DB_AGENT_INFO);
#ifdef _TIOR_TRANSFER_
	if (theApp.m_pDbSetting) {
		theApp.m_pDbSetting->SelectQuery(strQuery, &arrList, _T("corp_name"));
	}
#else
	CCmsDBManager dbManage(DB_AGENT_INFO);
	dbManage.SelectQuery(strQuery, &arrList, _T("corp_name"));
	dbManage.Free();
#endif
	if (0 < arrList.GetSize()) {
		strOldCorpName = arrList.GetAt(0);
	}
	if (0 != strOldCorpName.CompareNoCase(_strCorpName) && 0 < arrList.GetSize()) {
		strQuery = _T("");
		strQuery.Format(_T("update %s set corp_name = \"%s\" where device_uuid = \"%s\""), DB_AGENT_INFO, _strCorpName, GetDeviceUUID());
#ifdef _TIOR_TRANSFER_
		if (theApp.m_pDbSetting) {
			theApp.m_pDbSetting->UpdateQuery(strQuery);
		}
#else
		CCmsDBManager dbManage2(DB_AGENT_INFO);
		dbManage2.UpdateQuery(strQuery);
		dbManage2.Free();
#endif
	}
	m_strCorpName = _strCorpName;
	return TRUE;
}

CString CMemberData::GetIgnore()
{
	return m_strIgnore;
}
CString CMemberData::GetStrCollectType()
{
	return m_strCollectType;
}

#ifdef _TIOR_TRANSFER_
INT CMemberData::GetCpuUsage()
{
	m_nCpuUsage = m_CpuUsage.GetCpuUsage(_T("TiorSaverTransfer"));
	return m_nCpuUsage;
}

VOID CMemberData::SetCpuUsage(INT _nCpuUsage)
{
	m_nCpuUsage = _nCpuUsage;
}
#endif