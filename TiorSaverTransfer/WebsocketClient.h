#pragma once
#ifdef _TEST_CHOI_SSL_
#include <websocketpp/config/asio_client.hpp>
#else
#include <websocketpp/config/asio_no_tls_client.hpp>
#endif
//#include <websocketpp/config/asio.hpp>
#include <websocketpp/client.hpp>
//#include <websocketpp/config/core.hpp>
//#include <websocketpp/config/core_client.hpp>
//#include <websocketpp/config/debug_asio.hpp>
//#include <websocketpp/common/thread.hpp>
//#include <iostream>
//#include <chrono>

#pragma comment(lib, "libssl.lib")
//typedef websocketpp::client<websocketpp::config::asio_tls_client> client;
typedef websocketpp::lib::lock_guard<websocketpp::lib::mutex> scoped_lock;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

// pull out the type of messages sent by our config
#ifdef _TEST_CHOI_SSL_
typedef websocketpp::config::asio_tls_client::message_type::ptr message_ptr;
typedef websocketpp::lib::shared_ptr<boost::asio::ssl::context> context_tls_ptr;
#else
typedef websocketpp::config::asio_client::message_type::ptr message_ptr;
#endif

#ifdef _TEST_CHOI_SSL_
typedef websocketpp::client<websocketpp::config::asio_tls_client> client_tls;
typedef client_tls::connection_ptr connection_tls_ptr;
#else
typedef websocketpp::client<websocketpp::config::asio_client> client_tls;
typedef client_tls::connection_ptr connection_tls_ptr;
#endif

class CWebsocketClient : public CWinThread
{
	//typedef std::chrono::duration<int, std::micro> dur_type;
	DECLARE_DYNCREATE(CWebsocketClient)
public:
	CWebsocketClient(void);
	CWebsocketClient(CString _strUri);
	virtual ~CWebsocketClient(void);
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();
	
public:
	client_tls* m_pClient;
	CString m_strUri;
	//client::connection_ptr m_pClientConnection;
	websocketpp::connection_hdl m_pClientHdl;
	websocketpp::lib::mutex* m_pLock;
	//std::chrono::high_resolution_clock::time_point m_start;
	//std::chrono::high_resolution_clock::time_point m_socket_init;
	//std::chrono::high_resolution_clock::time_point m_tls_init;
	//std::chrono::high_resolution_clock::time_point m_open;
	//std::chrono::high_resolution_clock::time_point m_message;
	//std::chrono::high_resolution_clock::time_point m_close;

	CString m_strOnMessage; 
	BOOL m_bWebsocketClientStart;
	HANDLE m_hConnectEvent;
	void InitDomain(CString _strUri);
	void CreateConnectEvent()
	{
		m_hConnectEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	}
	void SetConnectEvent()
	{
		if (INVALID_HANDLE_VALUE !=m_hConnectEvent)
			::SetEvent(m_hConnectEvent);
	}
	//void on_socket_init(websocketpp::connection_hdl hdl, boost::asio::ssl::stream<boost::asio::ip::tcp::socket> & s) {
	//	boost::asio::ip::tcp::no_delay option(true);
	//	s.lowest_layer().set_option(option);
	//}
#ifdef _TEST_CHOI_SSL_
	context_tls_ptr on_tls_init(websocketpp::connection_hdl hdl);
#endif
	//context_ptr on_tls_init(websocketpp::connection_hdl) {
	//	m_tls_init = std::chrono::high_resolution_clock::now();
	//	context_ptr ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::tlsv1);

	//	try {
	//		ctx->set_options(boost::asio::ssl::context::default_workarounds |
	//			boost::asio::ssl::context::no_sslv2 |
	//			boost::asio::ssl::context::no_sslv3 |
	//			boost::asio::ssl::context::single_dh_use);
	//	}
	//	catch (std::exception& e) {
	//		std::cout << e.what() << std::endl;
	//	}
	//	return ctx;
	//}
public:
	void on_open(websocketpp::connection_hdl hdl);
	void on_fail(websocketpp::connection_hdl hdl);
	void on_message(websocketpp::connection_hdl hdl, message_ptr msg);
	void on_close(websocketpp::connection_hdl hdl);
	//void telemetry_loop();
protected:
	DECLARE_MESSAGE_MAP()
};