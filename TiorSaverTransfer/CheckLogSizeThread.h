/*******************************************************************************             
PROJECT  :    ITCMS                

PRODUCTION COMPANY : DSNTCH  digital solution & technology partner

URL : www.dsntech.com

DIVISION : Business Department Div

DATE : 2011.11.03	
********************************************************************************/
/**
@file       CLogSendThread.h
@brief     LogSendThread  정의 파일
@author   jhlee
@date      create 2011.11.03
*/
#pragma once

#include "../BuildEnv/common/ConnectParamInfo.h"

// CLogSendThread

class CCheckLogSizeThread : public CWinThread
{
	DECLARE_DYNCREATE(CCheckLogSizeThread)

public:
	CCheckLogSizeThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CCheckLogSizeThread();

	HANDLE m_hStopEvent;
	void CreateStopEvent()
	{
		m_hStopEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	}
	void SetStopEvent()
	{
		::SetEvent(m_hStopEvent);
	}
	HANDLE m_hResendEvent;
	void CreateResendEvent()
	{
		m_hResendEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	}
	void SetResendEvent()
	{
		::SetEvent(m_hResendEvent);
	}
	BOOL m_bCheckLogSizeThreadStart;
	BOOL m_bCheckLogSizeThreadSending;
	BOOL m_bSetServerIP;
	DWORD m_dwFileSize;
	UINT m_nCurrentOffset;
	//int m_nDelay;
	CString m_strTargetFile;
	CFile m_cScreenFile;
	CString m_strUrl;
	//CStdioFile m_cstdLogFile;
	UINT GetCurrentOffset();
	void SetCurrentOffset(UINT _nCurrentOffset);
	UINT GetFileSize();
	void SetFileSize(UINT _dwFileSize);
	CString GetTargetFile();
	void SetTargetFile(CString strTargetFile);
	INT ScreenFiles();
	INT LogFiles(CStdioFile& m_cstdLogFile);

private:
//	void ClearForRenewDB(CString _strDBName, CString _strColumName, CStringArray& _arrStr)  //hhh: 추후 필요가능성 있음

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();

protected:
	DECLARE_MESSAGE_MAP()

	BOOL GetVaccineModuleCheck(CString _strVaccineExe);

};


