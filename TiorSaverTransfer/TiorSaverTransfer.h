
// TiorSaverTransfer.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include <locale>
#include <ctime>
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#ifdef WIN32
#include <io.h>
#else
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include "curl\curl.h"
#include "pipe/C_SendAgent_S_CollectAgent/STOCPipeClient.h"
#pragma comment(lib, "wldap32.lib")
#pragma comment (lib, "crypt32")

#ifndef _DEBUG
#pragma comment (lib, "../BuildEnv/Lib/x86/libcurl.lib")
#else	// #ifdef _DEBUG
#pragma  comment (lib, "../BuildEnv/Lib/x86/libcurld.lib")
#endif	// #else	// #ifdef _DEBUG
#if LIBCURL_VERSION_NUM < 0x070c03
#error "upgrade your libcurl to no less than 7.12.3"
#endif
#include "resource.h"		// main symbols
#include "CheckLogSizeThread.h"
//#include "RandomDataGenerateThread.h"
#include "CheckEachLogFileInLogFolderThread.h"
#include "CheckEachNcviFileInLogFolderThread.h"
#include "CheckEachNkfiFileInLogFolderThread.h"
//#include "CheckJPGFileInLogFolderThread.h"
#include "CheckLogSizeThread.h"
#include "CheckPolicyThread.h"
#include "MemberData.h"
#include "TiorSaverTransferWnd.h"
//#include "MainFrm.h"
#include "Pipe/C_Update_S_Agent/PTOAPipeServer.h"
#include "Pipe/C_CollectAgent_S_SendAgent/CTOSPipeServer.h"
#include "WebsocketClient.h"
#include "TiorSaverReload.h"
#include "MiniDumpHelp.h"
#include "CurlTConnect.h"
#include "CmsDBManager.h"

// CTiorSaverTransferApp:
// See TiorSaverTransfer.cpp for the implementation of this class
//

class CTiorSaverTransferApp : public CWinAppEx
{
public:
	CTiorSaverTransferApp();
	virtual ~CTiorSaverTransferApp();

	//CHttpsTConnect* m_logSend;
	CMemberData* m_pMemberData;
	CTiorSaverTransferWnd* m_pWnd;
	CString m_strToken;
	BOOL m_bInitialized;	//2017-12-01 sy.choi initApi호출을 통해 무시할 확장자가 initialized되기 전까지 policy가 변경되어도 적용하지 않는다.(그럴 일은 없겠지만.)
	//CString m_strUrl;
	//BOOL m_bComplete;
	INT m_nRestartProcessType;
	MinidumpHelp m_Minidump;
	INT m_nSendType;
	BOOL m_bIsRandom;
	BOOL m_bIsCollecting;
	BOOL m_bIsFirstUser;
	BOOL m_bChangingPolicy;
	BOOL m_bIsDeleteRequestPipeStart;
	CString m_strNewVersion;
	INT	m_nScanDelay;
	int m_nDeleteDelay;
	CString m_strDeleteDate;
	INT m_nPolicyRetry;
	CSTOCPipeClient* m_pCSTOUCPipeClient;
	CSTOCPipeClient* m_pCSTOSCPipeClient;
	CCheckEachLogFileInLogFolderThread *m_pCheckEachLogFileInLogFolderThread;
	CCheckEachNcviFileInLogFolderThread *m_pCheckEachNcviFileInLogFolderThread;
	CCheckEachNkfiFileInLogFolderThread *m_pCheckEachNkfiFileInLogFolderThread;
	//CCheckJPGFileInLogFolderThread *m_pCheckJPGFileInLogFolderThread;
	CTypedPtrArray <CPtrArray, CCheckLogSizeThread*> m_arrLogSendingThreads;
	CTypedPtrArray <CPtrArray, CCheckLogSizeThread*> m_arrNCVISendingThreads;
	CTypedPtrArray <CPtrArray, CCheckLogSizeThread*> m_arrNKFISendingThreads;
	CCheckPolicyThread *m_pCheckPolicyThread;
	CWebsocketClient *m_pWebsocketClientThread;
	HANDLE	m_hPTOAPipeServer;
	HANDLE	m_hCTOSPipeServer;
	HANDLE	m_hWebSocketThread;
	BOOL m_bThreadWebSocketExit;
	CPTOAPipeServer* m_pPTOAPipeServer;
	CCTOSPipeServer* m_pCTOSPipeServer;
	BOOL m_bThreadPTOAPipeServerExit;
	BOOL m_bThreadCTOSPipeServerExit;
	//CString m_strTimeFromServer;
	BOOL m_bIsPipeStart;
	UINT m_nTempCurrentPos;
	BOOL m_bUninstallStart;
	CReloadThread* m_pReloadThread;
	CCurlTConnect m_curl;
	CStringArray m_arrDeleteList;
	INT m_nNcviArrayMaxSizeFull;
	INT m_nNkfiArrayMaxSizeFull;
	INT m_nMaxSendLimit;
	BOOL m_bDeleteOldFilesStart;
	CCmsDBManager* m_pDbSetting;
	INT m_nTransMaxScreenSize;
	INT m_nTransMaxLogLines;
	std::mutex* m_pTokenMutex;
	BOOL m_bSendThumbThreadStart;
	BOOL m_bUpdateAfterReboot;
#ifdef _MGCHOI_TEST_
	CCmsDBManager* m_pDbNcvi;
	CCmsDBManager* m_pDbNkfi;
	CCmsDBManager* m_pDbLog;
#endif // _MGCHOI_TEST_

	virtual BOOL InitInstance();
	BOOL GetOffsetFromServer(CString _strTargetFile, INT& _nCurrentOffset, int& _nDelay);
	BOOL PostResetOffsetToServer(CString _strTargetFile);
	//BOOL SendData(CString _strTargetFile, UINT& _nCurrentOffset);
	//BOOL SendBinaryData(CString _strTargetFile, UINT& _nCurrentOffset);
	//void RandomData(int _nCnt);
	void SetToken(CString _strToken);
	CString GetToken();
	//CStringA Utf8_Encode(CStringW strData);
	//CStringW Utf8_Decode(CStringA strData);
	BOOL SendInitScreenIdx();
	BOOL AddColumn(CString _strTableName, CString _strColumnName, CString _strDataType);
	BOOL TerminateFileTransfer();
	void SynchronziationTime(CString _strRecvServerTime);
	BOOL ComparePolicy(POLICY_SET* pPolicy, BOOL _bIsFirstStart = FALSE);
	BOOL ChangePolicy(POLICY_SET* pPolicySet, BOOL _bIsFirstStart = FALSE);
	BOOL SendPolicyChanged();
	BOOL InitAgent();	// 2017-09-13 sy.choi 업데이트하는 부분 여기있음
	void Write_Index(CString _strPath, CString _strHash, CString _strVersion);
	BOOL CallUpdate(CString _strVersion);
	void SetUpdateCompleteOnDB();
	void StartUninstall();
	void StartTray();
	void StartDeleteOldFiles();
	//void ForceDeleteMyself();
	void DeleteMyself(CString _strPath);
	BOOL ReleaseFileStatus(CString strFile, BYTE Attribute);
	BOOL MoveFile();
	BOOL CompareVersion(CString _strCurrVersion, CString _strNewVersion);
	INT GetUsrAgentSessionID();
	BOOL PipeThumnailAction(BOOL _bStart);
	BOOL PipeServerTime();
	BOOL PolicyRequest(POLICY_SET* pPolicySet);
	BOOL UpdateStart(UPDATE_SET* _pUpdateSet);
	//BOOL PipeResendPolicy();
	BOOL PipeDeleteFileRequest();
	BOOL AddDeleteFileRequestArray(CStringArray& _arrFilePath);
	BOOL RegisterStartProgram();
	VOID SendThumb(BYTE* pFileData, DWORD _dwSize);
	VOID VerifyLicense();

	DECLARE_MESSAGE_MAP()
	friend BOOL WINAPI StartPTOAPipeServerThread(LPVOID lpParam);
	friend BOOL WINAPI StartCTOSPipeServerThread(LPVOID lpParam);
	friend BOOL WINAPI StartUninstallThread(LPVOID lpParam);
	friend BOOL WINAPI StartDeleteOldFilesThread(LPVOID lpParam);
	friend BOOL WINAPI PipeServerTimeThread(LPVOID lpParam);
	friend BOOL WINAPI PipeDeleteFileRequestThread(LPVOID lpParam);
	//friend BOOL WINAPI PipeResendPolicyThread(LPVOID lpParam);
	//friend BOOL WINAPI StartWebSocketThread(LPVOID lpParam);
};

extern CTiorSaverTransferApp theApp;