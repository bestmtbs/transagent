#pragma once


class CCheckPolicyThread : public CWinThread
{
	DECLARE_DYNCREATE(CCheckPolicyThread)

public:
	CCheckPolicyThread();           // 동적 만들기에 사용되는 protected 생성자입니다.
	virtual ~CCheckPolicyThread();

	BOOL m_bCheckPolicy;
	BOOL m_bCheckPolicyThread;
	//DWORD m_dwFileSize;
	//BOOL PolicyRequest(POLICY_SET* pPolicy);
	//BOOL ApplyPolicy(POLICY_SET* pPolicy);
	//void	SendRecvEncFile_DecInfo();		//hhh:2014.04.15
	//void RenewDB();						//hhh:2012.11.30 
	//void CreateAndDeleteInfo_Renew();		//hhh: 2012.12.11

private:
//	void ClearForRenewDB(CString _strDBName, CString _strColumName, CStringArray& _arrStr)  //hhh: 추후 필요가능성 있음

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual int Run();

protected:
	DECLARE_MESSAGE_MAP()
};


